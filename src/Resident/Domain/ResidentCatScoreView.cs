using System;
using ProgramManagement.Domain.Examination;
using ProgramManagement.Domain.Program;

namespace ResidentManagement.Domain
{
    public class ResidentCatScoreView
    {
        public int UnitId { get; set; }
        public long ResidentUnitId { get; set; }
        public string Code { get; set; }
        public ExamType? ExamType { get; set; }
        public int ProgramEnrollmentId { get; set; }
        public long ResidentId { get; set; }
        public long ResidentCourseWorkId { get; set; }
        public string Name { get; set; }
        public int SemesterId { get; set; }
        public string Semester { get; set; }
        public int ExaminationId { get; set; }
        public int TimetableId { get; set; }
        public decimal? Score { get; set; }
        public bool HasFieldPlacementActivity { get; set; }
        public TimetableType TimeTableType { get; set; }
        
    }
}