﻿using FacultyManagement.Domain;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace ResidentManagement.Domain
{

    public enum AttendanceStatus
    {
        Present=1,
        Absent,
        Leave
    }
    public class ResidentUnitAttendance
    {
        public ResidentUnitAttendance()
        {

        }
        public ResidentUnitAttendance(long residentUnitId, int facultyId, DateTime attendanceDate,
            string createdBy, int timetableId, AttendanceStatus attendanceStatus)
        {
           
            ResidentUnitId = residentUnitId;
            FacultyId = facultyId;
            AttendanceDate = attendanceDate;
            CreatedBy = createdBy; 
            TimetableId = timetableId;
            DateCreated = DateTime.Now;
            AttendanceStatus = attendanceStatus;
        }
        public long Id { get; set; }
        public long ResidentUnitId { get; set; }
        public int FacultyId { get; set; }
        public DateTime AttendanceDate { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public AttendanceStatus AttendanceStatus { get; set; }
        public int TimetableId { get; set; }
        public virtual Faculty Faculty { get; set; }
        public virtual ResidentUnit ResidentUnit { get; set; }
        public virtual  Timetable Timetable { get; set; }

    }
}
