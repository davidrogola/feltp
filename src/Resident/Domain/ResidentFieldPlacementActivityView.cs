﻿using ProgramManagement.Domain.FieldPlacement;
using System;
using System.Collections.Generic;
using System.Text;

namespace ResidentManagement.Domain
{
    public class ResidentFieldPlacementActivityView
    {
        public ResidentFieldPlacementActivityView()
        {

        }

        public long Id { get; set; }
        public int FieldPlacementActivityId { get; set; }
        public ActivityType ActivityType { get; set; }
        public long ResidentUnitId { get; set; }
        public PlacementActivityStatus ProgressStatus { get; set; }
        public string Semester { get; set; }
        public int SemesterId { get; set; }
        public string ActivityName { get; set; }
        public long ResidentId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public int ProgramEnrollmentId { get; set; }

    }
}
