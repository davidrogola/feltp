﻿using Common.Models;
using ProgramManagement.Domain.FieldPlacement;
using System;
using System.ComponentModel.DataAnnotations;

namespace ResidentManagement.Domain
{
    public enum PlacementActivityStatus
    {
        NotStarted=0,
        Ongoing,
        Completed,
        [Display(Name = "Deliverable Pending Submission")]
        DeliverablePendingSubmission,
        Leave,
        Cancelled
    }
    public class ResidentFieldPlacementActivity 
    {
        public ResidentFieldPlacementActivity()
        {

        }

        public ResidentFieldPlacementActivity(long residentUnitId, int fieldPlacementActivityId, 
            string createdBy, PlacementActivityStatus progressStatus)
        {
            FieldPlacementActivityId = fieldPlacementActivityId;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
            ProgressStatus = progressStatus;
            ResidentUnitId = residentUnitId;

        }

        public long Id { get; set; }
        public int FieldPlacementActivityId { get; set; }
        public long ResidentUnitId { get; set; }
        public PlacementActivityStatus ProgressStatus { get; set; }
        public string EvalutedBy { get; set; }
        public DateTime ? DateEvaluated { get; set; }
        public DateTime ? DateCompleted { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public ResidentUnit ResidentUnit { get; set; }
        public virtual FieldPlacementActivity FieldPlacementActivity { get; set; }

        public void SetProgressStatus(PlacementActivityStatus status)
        {
            ProgressStatus = status;
        }
    }
}
