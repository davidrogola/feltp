﻿using Common.Models;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ResidentManagement.Domain
{
    public enum DeliverableStatus
    {
        Pending = 0,
        Submitted,
        Completed,
        Overdue
    }

    public enum ThesisDefenceStatus
    {
        [Display(Name = "Thesis Defence Pending")]
        Pending = 1,
        [Display(Name = "Thesis Succesfully Defended")]
        SuccesfullyDefended,
        [Display(Name = "Thesis Defence Failed")]
        DefenceFailed,
    }
    public class ResidentDeliverable
    {
        public ResidentDeliverable()
        {

        }

        public ResidentDeliverable(long residentFieldPlacementActivityId,
            int deliverableId, DeliverableStatus progressStatus, string createdBy)
        {
            ResidentFieldPlacementActivityId = residentFieldPlacementActivityId;
            DeliverableId = deliverableId;
            ProgressStatus = progressStatus;
            DateCreated = DateTime.Now;
            CreatedBy = createdBy;

        }

        public long Id { get; set; }
        public long ResidentFieldPlacementActivityId { get; set; }
        public int DeliverableId { get; set; }
        public DeliverableStatus ProgressStatus { get; set; }
        public string EvaluatedBy { get; set; }
        public DateTime? DateEvaluated { get; set; }
        public DateTime? DateCompleted { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public decimal? Score { get; set; }
        public ThesisDefenceStatus? ThesisDefenceStatus { get; set; }
        public virtual Deliverable Deliverable { get; set; }
        public virtual ResidentFieldPlacementActivity ResidentFieldPlacementActivity { get; set; }
        public virtual List<ResidentDeliverableCoauthor> CoAuthors { get; set; }

        public void Evaluate(string evaluatedBy)
        {
            ProgressStatus = DeliverableStatus.Completed;
            DateEvaluated = DateTime.Now;
            DateCompleted = DateTime.Now;
            EvaluatedBy = evaluatedBy;

        }

        public void SetProgressStatus(DeliverableStatus progressStatus)
        {
            ProgressStatus = progressStatus;
        }

        public void SetThesisDefenceStatus(ThesisDefenceStatus thesisDefenceStatus)
        {
            ThesisDefenceStatus = thesisDefenceStatus;
        }

        public void InputScore(decimal? score)
        {
            Score = score;
        }

    }
}
