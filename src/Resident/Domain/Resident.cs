﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ResidentManagement.Domain
{
    public class Resident : IAuditItem 
    {
        public Resident()
        {

        }

        public Resident(long applicantId, string registrationNumber, string createdBy)
        {
            ApplicantId = applicantId;
            RegistrationNumber = registrationNumber;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
        }

        public long Id { get; set; }
        public long ApplicantId { get; set; }
        public string RegistrationNumber { get; set; }

        public DateTime DateCreated { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? DateDeactivated { get; set; }

        public string DeactivatedBy { get; set; }

        public bool IsAlumni { get; set; }

        public virtual Applicant Applicant { get; set; }

        public void Deactivate(string deactivatedBy)
        {
            throw new NotImplementedException();
        }
        public void SetAlumni()
        {
            IsAlumni = true;
        }
    }
}
