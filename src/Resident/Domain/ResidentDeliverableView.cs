﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ResidentManagement.Domain
{
    public class ResidentDeliverableView
    {
        public string DeliverableName { get; set; }
        public int DeliverableId { get; set; }
        public int ProgramEnrollmentId { get; set; }
        public long ResidentId { get; set; }
        public long ResidentDeliverableId { get; set; }
        public long ResidentFieldPlacementActivityId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DeliverableStatus ProgressStatus { get; set; }
        public string Code { get; set; }
        public string Semester { get; set; }
        public int SemesterId { get; set; }
        public bool HasThesisDefence { get; set; }
        public ThesisDefenceStatus? ThesisDefenceStatus { get; set; }
        public long CourseId { get; set; }

    }
}
