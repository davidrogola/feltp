﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ResidentManagement.Domain
{
    public class ResidentUnitActivityDeliverableView
    {
        public ResidentUnitActivityDeliverableView()
        {

        }
        public long ResidentId { get; set; }
        public long ResidentFieldPlacementActivityId { get; set; }
        public int ProgramEnrollmentId { get; set; }
        public string UnitName { get; set; }
        public string ActivityName { get; set; }
        public long ResidentDeliverableId { get; set; }
        public string Deliverable { get; set; }
        public string Code { get; set; }
        public long ResidentUnitId { get; set; }
        public int SemesterId { get; set; }
        public string Semester { get; set; }
        public decimal ? Score { get; set; }
        public DeliverableStatus ProgressStatus { get; set; }
    }
}
