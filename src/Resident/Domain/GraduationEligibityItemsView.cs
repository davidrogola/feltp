﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ResidentManagement.Domain
{
    public class GraduationEligibityItemsView
    {
        public GraduationEligibityItemsView()
        {

        }
        public long Id { get; set; }
        public long GraduateId { get; set; }
        public decimal ExpectedValue { get; set; }
        public decimal ActualValue { get; set; }
        public OutcomeStatus OutcomeStatus { get; set; }
        public EligibilityType EligibiltyType { get; set; }
        public FailureReason FailureReason { get; set; }
        public DateTime DateCreated { get; set; }
        
        public string CreatedBy { get; set; }
        
        public DateTime? DateDeactivated { get; set; }
        
        public string DeactivatedBy { get; set; }
        
        public long? ResidentUnitId { get; set; }
        
        public long? ResidentCourseWorkId { get; set; }
                
        public string CourseName { get; set; }
        
        public string Semester { get; set; }
        
        public int ? SemesterId { get; set; }
        
        public string Code { get; set; }
        
    }
}
