﻿using Common.Models;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;
using ProgramManagement.Domain.Examination;

namespace ResidentManagement.Domain
{

    public enum CourseProgessStatus
    {
        NotStarted = 0,
        Ongoing,
        Compeleted,
        NotCompleted,
        Leave
    }
    public class ResidentCourseWork : IAuditItem
    {
        public ResidentCourseWork()
        {

        }

        public ResidentCourseWork(int enrollmentId, int programCourseId, CourseProgessStatus progressStatus, string createdBy
            , long residentId)
        {
            ProgramEnrollmentId = enrollmentId;
            ProgramCourseId = programCourseId;
            ProgressStatus = progressStatus;
            DateCreated = DateTime.Now;
            CreatedBy = createdBy;
            ProgressStatus = progressStatus;
            ResidentId = residentId;
        }

        public long Id { get; set; }
        public int ProgramEnrollmentId { get; set; }
        public int ProgramCourseId { get; set; }
        public long ResidentId { get; set; }
        public CourseProgessStatus ProgressStatus { get; set; }
        public int? TotalScore { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string EvaluatedBy { get; set; }
        public DateTime? DateEvaluated { get; set; }
        public DateTime? DateCompleted { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public virtual ProgramCourse ProgramCourse { get; set; }
        public virtual ProgramEnrollment ProgramEnrollment { get; set; }
        public virtual ExaminationScore ExaminationScore { get; set; }

        public void Deactivate(string deactivatedBy)
        {
            throw new NotImplementedException();
        }

        public void SetScore(int totalScore)
        {
            TotalScore = totalScore;
        }
    }
}
