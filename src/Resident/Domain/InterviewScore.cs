﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ResidentManagement.Domain
{
    public class InterviewScore
    {
        public InterviewScore()
        {

        }

        public InterviewScore(long offerApplicationId, int oralInterview, int writtenInterview)
        {
            ProgramOfferApplicationId = offerApplicationId;
            OralInterview = oralInterview;
            WrittenInterview = writtenInterview;
            Total = oralInterview + writtenInterview;

        }

        public long Id { get; set; }
        public long ProgramOfferApplicationId { get; set; }
        public int OralInterview { get; set; }
        public int WrittenInterview { get; set; }
        public int Total { get; set; }

    }
}
