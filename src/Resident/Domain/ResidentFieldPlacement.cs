﻿using ProgramManagement.Domain.FieldPlacement;
using System;
using System.Collections.Generic;
using System.Text;

namespace ResidentManagement.Domain
{
    public class ResidentFieldPlacement
    {
        public long Id { get; set; }
        public long ResidentFieldPlacementActivityId { get; set; }
        public long ResidentId { get; set; }
        public string ResidentName { get; set; }
        public string SiteName { get; set; }
        public string Supervisor { get; set; }
        public string ActivityName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Description { get; set; }
        public PlacementActivityStatus ProgressStatus { get; set; }
        public string PlacedBy { get; set; }
        public ActivityType ActivityType { get; set; }
        public string Faculty { get; set; }
    }
}
