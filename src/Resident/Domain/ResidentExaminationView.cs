﻿using System;
using System.Collections.Generic;
using System.Text;
using ProgramManagement.Domain.Examination;

namespace ResidentManagement.Domain
{
    public class ResidentExaminationView
    {        
        public int CourseId { get; set; }
        
        public string Code { get; set; }
        
        public ExamType ? ExamType { get; set; }
        
        public string CreatedBy { get; set; }
        public DateTime ? DateCreated { get; set; }
        public int ProgramEnrollmentId { get; set; }
        
        public long ResidentId { get; set; }
        public long ResidentCourseWorkId { get; set; }
        public string Name { get; set; }
        public int SemesterId { get; set; }
        
        public string Semester { get; set; }
        
        public int ExaminationId { get; set; }
        public decimal ? Score { get; set; }
    }
}
