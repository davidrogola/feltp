﻿using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace ResidentManagement.Domain
{
    public class PersonalStatementFile
    {
        public PersonalStatementFile()
        {

        }

        public PersonalStatementFile(long programOfferApplicationId, byte [] file,string fileName
            , string contentType)
        {
            File = file;
            ProgramOfferApplicationId = programOfferApplicationId;
            FileName = fileName;
            ContentType = contentType;
        }
        public int Id { get; set; }
        public byte[] File { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public long ProgramOfferApplicationId  { get; set; }
    }
}
