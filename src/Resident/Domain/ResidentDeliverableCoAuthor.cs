﻿using ResidentManagement.Domain.Ethics;
using System;
using System.Collections.Generic;
using System.Text;

namespace ResidentManagement.Domain
{
    public class ResidentDeliverableCoauthor
    {
        public ResidentDeliverableCoauthor()
        {

        }
        public ResidentDeliverableCoauthor(string authorName,long residentDeliverableId, int committeeId, string createdBy)
        {
            AuthorName = authorName;
            ResidentDeliverableId = residentDeliverableId;
            EthicsApprovalCommitteeId = committeeId;
            DateCreated = DateTime.Now;
            CreatedBy = createdBy;
            IsActive = true;
        }
        public int Id { get; set; }
        public string AuthorName { get; set; }
        public long ResidentDeliverableId { get; set; }
        public int EthicsApprovalCommitteeId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public DateTime ? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public virtual EthicsApprovalCommittee EthicsApprovalCommittee { get; set; }
        public virtual ResidentDeliverable ResidentDeliverable { get; set; }

    }
}
