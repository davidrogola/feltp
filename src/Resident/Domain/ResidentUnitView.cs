﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ResidentManagement.Domain
{
    public class ResidentUnitView
    {
        public int UnitId { get; set; }
        public long ResidentUnitId { get; set; }
        public int ExaminationId { get; set; }
        public string Code { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public int ProgramEnrollmentId { get; set; }
        public long ResidentId { get; set; }
        public long ResidentCourseWorkId { get; set; }
        public string Name { get; set; }
        
        public int SemesterId { get; set; }

        public bool HasFieldPlacementActivity { get; set; }
        
    }
}
