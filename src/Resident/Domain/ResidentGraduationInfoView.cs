﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ResidentManagement.Domain
{
    public class ResidentGraduationInfoView
    {
        public ResidentGraduationInfoView()
        {

        }

        public long Id { get; set; }
        public int GraduationRequirementId { get; set; }
        public long ResidentId { get; set; }
        public int ProgramEnrollmentId { get; set; }
        public QualificationStatus QualificationStatus { get; set; }
        public CompletionStatus CompletionStatus { get; set; }
        public DateTime ExpectedProgramEndDate { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string ResidentName { get; set; }
        public string ProgramName { get; set; }
        public string ProgramOffer { get; set; }
        public int ProgramOfferId { get; set; }
        public int ProgramId { get; set; }
        public string RegistrationNumber { get; set; }
    }
}
