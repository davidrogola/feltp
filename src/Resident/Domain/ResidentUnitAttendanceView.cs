﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ResidentManagement.Domain
{
    public class ResidentUnitAttendanceView
    {
        public ResidentUnitAttendanceView()
        {

        }
        public long Id { get; set; }
        public long ResidentUnitId { get; set; }
        public int FacultyId { get; set; }
        public DateTime AttendanceDate { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public AttendanceStatus AttendanceStatus { get; set; }
        public int TimetableId { get; set; }
        public string Trainer { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int SemesterScheduleItemId { get; set; }
        public long ResidentId { get; set; }
        public string ResidentName { get; set; }
        public string RegistrationNumber { get; set; }
        public string Unit { get; set; }
        public string Code { get; set; }
        public int UnitId { get; set; }
        public long ResidentCourseWorkId { get; set; }
        
    }
}
