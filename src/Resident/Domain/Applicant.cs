﻿using Common.Domain.Person;
using System;
using System.Collections.Generic;
using System.Text;

namespace ResidentManagement.Domain
{
    public class Applicant
    {
        public Applicant()
        {

        }

        public  Applicant(long personId, string createdBy)
        {
            PersonId = personId;
            DateCreated = DateTime.Now;
            CreatedBy = createdBy;

        }

        public long Id { get; set; }
        public long PersonId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public virtual Person Person { get; set; }

    }
}
