﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ResidentManagement.Domain
{

    public enum OutcomeStatus
    {
        [Display(Name = "Pending Evaluation")]
        PendingEvaluation =1,
        Passed,
        Failed
    }

    public enum EligibilityType
    {
        [Display(Name = "Number of Didactic Courses Completed")]
        DidacticUnitsAttendance =1,
        [Display(Name = "Number of Field Placement Activities Completed")]
        FieldActivitiesParticipated,
        [Display(Name = "Passmark Attained")]
        PassmarkAttained,
        [Display(Name = "Thesis Defended Succesfully")]
        ThesisDefenceSuccesful
        
    }

    public enum FailureReason
    {
        None,
        [Display(Name = "Didactic Attendance Not Attained")]
        DidacticUnitsAttendanceNotAttained,
        [Display(Name = "Passmark Not Attained")]
        PassmarkNotAttained,
        [Display(Name = "Activity Completion Pending")]
        FieldActivityAttendanceNotAttained,
        [Display(Name = "Thesis Defence Failed")]
        ThesisDefenceFailed
    }


    public class GraduateEligibilityItem 
    {
        public GraduateEligibilityItem()
        {

        }

        public GraduateEligibilityItem(long graduateId, decimal expectedValue, decimal actualValue, OutcomeStatus outcomeStatus,
            EligibilityType eligibilityType, FailureReason failureReason, string createdBy, long ? residentUnitId, long ? residentCourseWorkId)
        {
            GraduateId = graduateId;
            ExpectedValue = expectedValue;
            ActualValue = actualValue;
            OutcomeStatus = outcomeStatus;
            EligibiltyType = eligibilityType;
            FailureReason = failureReason;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
            ResidentUnitId = residentUnitId;
            ResidentCourseWorkId = residentCourseWorkId;
        }

        public long Id { get; set; }
        public long GraduateId { get; set; }
        public decimal ExpectedValue { get; set; }
        public decimal ActualValue { get; set; }
        public OutcomeStatus OutcomeStatus { get; set; }
        public EligibilityType EligibiltyType { get; set; }
        public FailureReason FailureReason { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public long ? ResidentUnitId { get; set; }
        
        public  long ? ResidentCourseWorkId { get; set; }
        public virtual  Graduate Graduate { get; set; }
        public virtual ResidentUnit ResidentUnit { get; set; }
        public virtual ResidentCourseWork ResidentCourseWork { get; set; }

        public void UpdateEligibilityExpectedValue(int newExpectedValue)
        {
            ExpectedValue = newExpectedValue;
        }



        public void Evaluate(decimal actualValue)
        {
            FailureReason failureReason = FailureReason.None;
            OutcomeStatus outcomeStatus = OutcomeStatus.Passed;
            if (actualValue < ExpectedValue)
            {
                failureReason = FailureReason.PassmarkNotAttained;
                outcomeStatus = OutcomeStatus.Failed;
            }

            OutcomeStatus = outcomeStatus;
            FailureReason = failureReason;
            ActualValue = actualValue;
        }

        public void EvaluateNonPassmarkEligiblityItem()
        {
            if(ActualValue < ExpectedValue)
            {
                ActualValue = ActualValue + 1;
                if (ActualValue >= ExpectedValue)
                    OutcomeStatus = OutcomeStatus.Passed;
            }
        }

        public void ReEvaluateNonPassmarkEligiblityItem()
        {
            if (ActualValue >= ExpectedValue)
                OutcomeStatus = OutcomeStatus.Passed;
        }



        public void AppendFinalOutcome()
        {

            if (ActualValue >= ExpectedValue)
            {
                OutcomeStatus = OutcomeStatus.Passed;
                FailureReason = FailureReason.None;
            }
            else
            {
                OutcomeStatus = OutcomeStatus.Failed;

                switch (EligibiltyType)
                {
                    case EligibilityType.DidacticUnitsAttendance:
                        break;
                    case EligibilityType.FieldActivitiesParticipated:                                                
                            FailureReason = FailureReason.FieldActivityAttendanceNotAttained;                        
                        break;
                    case EligibilityType.PassmarkAttained:                     
                            FailureReason = FailureReason.PassmarkNotAttained;                        
                        break;
                    case EligibilityType.ThesisDefenceSuccesful:
                        FailureReason = FailureReason.ThesisDefenceFailed;
                        break;
                    default:
                        break;
                }
            }
           
        }

    }
}
