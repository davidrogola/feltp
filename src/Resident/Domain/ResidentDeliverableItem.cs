﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ResidentManagement.Domain
{
    public enum SubmissionType : int
    {
        Initial,
        Revised,
        Final
    }

    public enum ProgrammaticArea
    {
        HIV = 1,
        TB,
        Malaria,
        [Display(Name = "Neglected Tropical Diseases")]
        Neglected_Tropical_Diseases,
        [Display(Name = "Zoonotic Diseases")]
        Zoonotic_Diseases,
        Immunization,
        Diarrhea,
        [Display(Name = "Maternal Child Health")]
        Maternal_Child_Health
    }

    public class ResidentDeliverableItem
    {
        public ResidentDeliverableItem()
        {

        }
        public ResidentDeliverableItem(string description, string title, byte[] file, string fileName, string contentType
            , DateTime dateSubmitted, string createdBy, long residentId, SubmissionType submissionType,
            long residentDeliverableId, ProgrammaticArea programmaticArea, string other)
        {
            Description = description;
            Title = title;
            File = file;
            FileName = fileName;
            ContentType = contentType;
            DateSubmitted = dateSubmitted;
            CreatedBy = createdBy;
            ResidentId = residentId;
            SubmissionType = submissionType;
            ResidentDeliverableId = residentDeliverableId;
            ProgrammaticArea = programmaticArea;
            Other = other;
        }

        public long Id { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public byte[] File { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateSubmitted { get; set; }
        public string CreatedBy { get; set; }
        public long ResidentId { get; set; }
        public SubmissionType SubmissionType { get; set; }
        public long ResidentDeliverableId { get; set; }
        public ProgrammaticArea ProgrammaticArea { get; set; }
        public virtual ResidentDeliverable ResidentDeliverable { get; set; }
        public virtual Resident Resident { get; set; }
        public string Other { get; set; }

        public void AddDeliverableFile(byte [] fileContents, string fileName, string contentType)
        {
            File = fileContents;
            FileName = fileName;
            ContentType = contentType;
        }
    }
}
