﻿using Common.Models;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace ResidentManagement.Domain
{
    public enum QualificationStatus
    {
        PendingEvaluation,
        Qualified,
        NotQualified
    }

    public enum CompletionStatus
    {
        Pending,
        Completed,
        Leave,
        Discontinued,
        NotCompleted
    }

    public class Graduate
    {
        public Graduate()
        {

        }

        public Graduate(int graduationRequirementId, string createdBy, long residentId,
            int enrollmentId, QualificationStatus status, DateTime programEndDate)
        {
            GraduationRequirementId = graduationRequirementId;
            CreatedBy = createdBy;
            ResidentId = residentId;
            ProgramEnrollmentId = enrollmentId;
            QualificationStatus = status;
            DateCreated = DateTime.Now;
            ExpectedProgramEndDate = programEndDate;

        }

        public long Id { get; set; }
        public int GraduationRequirementId { get; set; }
        public long ResidentId { get; set; }
        public int ProgramEnrollmentId { get; set; }
        public QualificationStatus QualificationStatus { get; set; }
        public CompletionStatus CompletionStatus { get; set; }
        public DateTime ExpectedProgramEndDate { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string EvaluatedBy { get; set; }
        public DateTime? DateEvaluated { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public virtual GraduationRequirement GraduationRequirement { get; set; }
        public virtual ProgramEnrollment ProgramEnrollment { get; set; }
        public virtual Resident Resident { get; set; }

        public void EvaluateGraduate(QualificationStatus qualificationStatus, CompletionStatus completionStatus,
            string evaluatedBy)
        {
            QualificationStatus = qualificationStatus;
            CompletionStatus = completionStatus;
            EvaluatedBy = evaluatedBy;
            DateEvaluated = DateTime.Now;

        }
        
    }
}
