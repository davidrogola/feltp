﻿using Common.Models;
using ProgramManagement.Domain.FieldPlacement;
using System;
using System.Collections.Generic;
using System.Text;

namespace ResidentManagement.Domain
{
  
    public class FieldPlacement 
    {
        public FieldPlacement()
        {

        }
        public FieldPlacement(int residentFieldPlacementActivityId, long residentId, DateTime startDate,
            DateTime endDate, string createdBy, PlacementActivityStatus status, string description, 
            int? siteId, int? supervisorId, int? facultyId)
        {
            ResidentFieldPlacementActivityId = residentFieldPlacementActivityId;
            ResidentId = residentId;
            StartDate = startDate;
            EndDate = endDate;
            ProgressStatus = status;
            Description = description;
            FieldPlacementSiteId = siteId;
            DateCreated = DateTime.Now;
            SupervisorId = supervisorId;
            CreatedBy = createdBy;
            FacultyId = facultyId;
        }

        public long Id { get; set; }
        public long ResidentFieldPlacementActivityId { get; set; }
        public long ResidentId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int? FieldPlacementSiteId { get; set; }
        public string Description { get; set; }
        public int? SupervisorId { get; set; }
        public PlacementActivityStatus ProgressStatus { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public int? FacultyId { get; set; }
        public virtual ResidentFieldPlacementActivity  ResidentPlacementActivity { get; set; }
        public virtual Resident Resident { get; set; }
        public virtual FieldPlacementSite FieldPlacementSite { get; set; }
        public virtual  FieldPlacementSiteSupervisor Supervisor { get; set; }
       
        public void SetProgressStatus(PlacementActivityStatus status)
        {
            ProgressStatus = status;
        }
        public void SetSite(int siteId)
        {
            FieldPlacementSiteId = siteId;
        }
 
    }

}

