﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ResidentManagement.Domain.Ethics
{
   
    public class EthicsApprovalCommittee
    {
        public EthicsApprovalCommittee()
        {

        }
        public EthicsApprovalCommittee(string name, string createdBy, int countryId)
        {
            Name = name;
            CreatedBy = createdBy;
            CountryId = countryId;
            DateCreated = DateTime.Now;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public int CountryId { get; set; }
        public DateTime DateCreated { get; set; }
        public virtual Country Country { get; set; }

    }
}
