﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ResidentManagement.Domain.Ethics
{
    public class EthicsApprovalItem
    {
        public EthicsApprovalItem()
        {

        }
        public EthicsApprovalItem(long ethicsApprovalId, int committeeId,string submittedBy, DateTime dateSubmitted,
            EthicsApprovalStatus status, long documentId)
        {
            EthicsApprovalId = ethicsApprovalId;
            EthicsApprovalCommitteeId = committeeId;
            SubmittedBy = submittedBy;
            SubmissionDate = dateSubmitted;
            DateCreated = DateTime.Now;
            ApprovalStatus = status;
            EthicsApprovalDocumentId = documentId;
        }
        public long Id { get; set; }
        public long EthicsApprovalId { get; set; }
        public int EthicsApprovalCommitteeId { get; set; }
        public long EthicsApprovalDocumentId { get; set; }
        public DateTime SubmissionDate { get; set; }
        public string SubmittedBy { get; set; }
        public EthicsApprovalStatus ApprovalStatus { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime ? DateEvaluated { get; set; }
        public string EvaluatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string Comment { get; set; }
        public virtual EthicsApprovalCommittee EthicsApprovalCommittee { get; set; }
        public virtual EthicsApprovalDocument EthicsApprovalDocument { get; set; }
        public virtual EthicsApproval EthicsApproval { get; set; }

        public void Evaluate(string evaluatedBy, string comment, string updatedBy, EthicsApprovalStatus status)
        {
            DateEvaluated = DateTime.Now;
            EvaluatedBy = evaluatedBy;
            Comment = comment;
            UpdatedBy = updatedBy;
            ApprovalStatus = status;
        }
    }
}
