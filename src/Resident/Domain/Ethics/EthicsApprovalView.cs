﻿using ProgramManagement.Domain.Event;
using System;
using System.Collections.Generic;
using System.Text;

namespace ResidentManagement.Domain.Ethics
{
    public class EthicsApprovalView
    {
        public EthicsApprovalView()
        {

        }
        public long Id { get; set; }
        public long ResidentDeliverableId { get; set; }
        public long ResidentId { get; set; }
        public int ProgramEventId { get; set; }
        public PaperCategory PaperCategory { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime SubmissionDueDate { get; set; }
        public bool EnableEmailNotification { get; set; }
        public bool EnableSmsNotification { get; set; }
        public EthicsApprovalStatus ApprovalStatus { get; set; }
        public int ProgramEnrollmentId { get; set; }
        public string Title { get; set; }
        public EventType EventTypeId { get; set; }
        public string ResidentName { get; set; }
        public string DeliverableName { get; set; }
    }
}
