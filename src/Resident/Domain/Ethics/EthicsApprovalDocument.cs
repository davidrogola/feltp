﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ResidentManagement.Domain.Ethics
{
    public enum DocumentVersion
    {
        Initial = 1,
        Revised,
        Final
    }
    public class EthicsApprovalDocument
    {
        public EthicsApprovalDocument()
        {

        }
        public EthicsApprovalDocument( byte[] document, string fileName, string contentType,
            string createdBy, DocumentVersion version, DateTime submissionDate)
        {
            Document = document;
            FileName = fileName;
            CreatedBy = createdBy;
            ContentType = contentType;
            Document = document;
            DateCreated = DateTime.Now;
            SubmissionDate = submissionDate;
            DocumentVersion = version;

        }
        public long Id { get; set; }
        public byte[] Document { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public DateTime SubmissionDate { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DocumentVersion DocumentVersion { get; set; }
    }
}
