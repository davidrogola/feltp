﻿using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ResidentManagement.Domain.Ethics
{
    public enum PaperCategory : int
    {
        Research = 1,
        [Display(Name = "Non Research")]
        Non_Research
    }

    public enum EthicsApprovalStatus
    {
        Approved = 1,
        [Display(Name = "Under Review")]
        Under_Review,
        Suspended,
        Rejected
    }

    public class EthicsApproval
    {
        public EthicsApproval()
        {

        }
        public EthicsApproval(long deliverableId, long residentId,int enrollmentId, int programEventId,
            PaperCategory paperCategory, DateTime submissionDueDate, string createdBy, bool emailNotification, bool smsNotification, EthicsApprovalStatus status)
        {
            ResidentDeliverableId = deliverableId;
            ResidentId = residentId;
            ProgramEventId = programEventId;
            PaperCategory = paperCategory;
            SubmissionDueDate = submissionDueDate;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
            EnableEmailNotification = emailNotification;
            EnableSmsNotification = smsNotification;
            ApprovalStatus = status;
            ProgramEnrollmentId = enrollmentId;
        }
        public long Id { get; set; }
        public long ResidentDeliverableId { get; set; }
        public long ResidentId { get; set; }
        public int ProgramEventId { get; set; }
        public PaperCategory PaperCategory { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime SubmissionDueDate { get; set; }
        public bool EnableEmailNotification { get; set; }
        public bool EnableSmsNotification { get; set; }
        public EthicsApprovalStatus ApprovalStatus { get; set; }
        public int ProgramEnrollmentId { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public string OverriddenBy { get; set; }
        public DateTime ? DateOverridden { get; set; }
        public EthicsApprovalStatus ? OriginalStatus { get; set; }
        public string OverrideReason { get; set; }
        public virtual ResidentDeliverable ResidentDeliverable { get; set; }
        public virtual List<EthicsApprovalItem> EthicsApprovalItems { get; set; }

        public void UpdateApproval(EthicsApprovalStatus status)
        {
            ApprovalStatus = status;
        }

        public void OverideApplicationStatus(string overridenBy,EthicsApprovalStatus originalStatus, string reason,EthicsApprovalStatus newStatus)
        {
            OverriddenBy = overridenBy;
            DateOverridden = DateTime.Now;
            OriginalStatus = originalStatus;
            OverrideReason = reason;
            ApprovalStatus = newStatus;
        }
    }
}
