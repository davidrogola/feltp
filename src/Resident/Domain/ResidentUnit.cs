﻿using ProgramManagement.Domain.Course;
using System;
using System.Collections.Generic;
using System.Text;

namespace ResidentManagement.Domain
{
    public class ResidentUnit
    {
        public ResidentUnit()
        {

        }

        public ResidentUnit(long residentCourseWorkId, int unitId, string createdBy)
        {
            ResidentCourseWorkId = residentCourseWorkId;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
            UnitId = unitId;
        }

        public long Id { get; set; }
        public long ResidentCourseWorkId { get; set; }
        public int UnitId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public virtual Unit Unit { get; set; }

    }
}
