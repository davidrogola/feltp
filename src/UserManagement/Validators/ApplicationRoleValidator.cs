﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UserManagement.Domain;
using UserManagement.ViewModels;

namespace UserManagement.Validators
{
    public class ApplicationRoleValidator : AbstractValidator<RoleViewModel>
    {
        ApplicationDbContext applicationDbContext;
        public ApplicationRoleValidator(ApplicationDbContext _applicationDbContext)
        {
            applicationDbContext = _applicationDbContext;

            RuleFor(x => x.RoleName).NotNull().WithMessage("Please enter the role name");

            RuleFor(x => x.Permissions).NotNull().WithMessage("Please select atleast one permission for the role");

            RuleFor(x => x.RoleName).Must((string name) =>
            {
                if (String.IsNullOrEmpty(name))
                    return true;
                bool exists = applicationDbContext.Roles.Where(x=>x.Name == name).Any();
                return !exists;
            }).WithMessage("Role name already exists");
        }
    }
}
