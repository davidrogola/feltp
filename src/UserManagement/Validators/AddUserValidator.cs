﻿using FluentValidation;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UserManagement.ConfigurationStrore.Entities;
using UserManagement.Domain;
using UserManagement.ViewModels;

namespace UserManagement.Validators
{
    public class RegisterUserValidator : AbstractValidator<RegisterViewModel>
    {
        ApplicationDbContext applicationDbContext;
        IConfiguration configuration;

        public RegisterUserValidator(ApplicationDbContext _applicationDbContext, IConfiguration _configuration)
        {
            configuration = _configuration;
            applicationDbContext = _applicationDbContext;

           
            RuleFor(x => x.Email).NotNull().WithMessage("Please enter the email address");

            RuleFor(x => x).Custom((model, context) =>
            {
               context.AddFailure("Please select atleast one role for the user");
            });
        }
    }

    public class RegisterSspUserModelValidator : AbstractValidator<RegisterSspUserModel>
    {
        IdentityServerDbContext identityServerDbContext;
        public RegisterSspUserModelValidator(IdentityServerDbContext _identityServerDbContext)
        {
            identityServerDbContext = _identityServerDbContext;
            RuleFor(x => x.Email).NotNull().WithMessage("Please enter the email address");

            RuleFor(x => x).Custom((model, context) =>
            {
                    if (string.IsNullOrEmpty(model.PhoneNumber))
                        context.AddFailure("Please enter your phone number");
                    else
                    {
                        var phoneNumberExists = identityServerDbContext.Users.Any(x => x.PhoneNumber == model.PhoneNumber);
                        if (phoneNumberExists)
                            context.AddFailure($"User with phone number {model.PhoneNumber} already exists");
                    }


                    if (string.IsNullOrEmpty(model.IdentificationNumber))
                        context.AddFailure("Please enter your identification number");
                    else
                    {
                        var idNumberExists = identityServerDbContext.Users.Any(x => x.IdentificationNumber == model.IdentificationNumber);
                        if (idNumberExists)
                            context.AddFailure($"User with Identification Number {model.IdentificationNumber} already exists");
                    }


                    if (string.IsNullOrEmpty(model.UserName))
                    {
                        context.AddFailure("Please enter your username");

                    }
                    else
                    {
                        var userNameExits = identityServerDbContext.Users.Any(x => x.UserName == model.UserName);
                        if (userNameExits)
                            context.AddFailure($"Username {model.UserName} already taken");
                    }               
            });
        }
    }
}
