﻿using FluentValidation;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using UserManagement.Domain;
using UserManagement.ViewModels;

namespace UserManagement.Validators
{
    public class ExistingUserActivationValidator : AbstractValidator<ExistingApplicantAccountActivationModel>
    {
        UserManager<ApplicationUser> userManager;
        public ExistingUserActivationValidator(UserManager<ApplicationUser> _userManager)
        {
            userManager = _userManager;

            RuleFor(x => x).Custom((model, context) =>
            {
                if (string.IsNullOrEmpty(model.EmailAddress))
                    context.AddFailure("Please enter your email address");

                if (string.IsNullOrEmpty(model.IdentificationNumber))
                    context.AddFailure("Please enter your identification number");

                if (!string.IsNullOrEmpty(model.EmailAddress))
                {
                    var user = userManager.FindByEmailAsync(model.EmailAddress).Result;
                    if (user == null)
                    {
                        context.AddFailure($"User with email address {model.EmailAddress} not found. Enter the email you applied with to a FELTP program");
                        return;
                    }                      
                    if (user.UserStatus != ApplicationUserStatus.UnConfirmed)
                        context.AddFailure("Your user account has already been activated. Please proceed to log in to the portal");

                    if(!string.IsNullOrEmpty(model.IdentificationNumber))
                    {
                        if (!string.Equals(user.IdentificationNumber, model.IdentificationNumber))
                            context.AddFailure($"Ensure that the identification number matches the user with email address '{model.EmailAddress}'");
                    }
                }

            });
        }
    }
}
