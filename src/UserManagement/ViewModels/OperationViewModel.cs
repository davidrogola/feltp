﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserManagement.ViewModels
{
    public class OperationViewModel
    {
        public int Id { get;  set; }
        public string Name { get;  set; }
        public string Comment { get;  set; }
        public int CreatedById { get; set; }
        public string CreatedBy { get; set; }
        public string DateCreated { get; set; }
    }
}
