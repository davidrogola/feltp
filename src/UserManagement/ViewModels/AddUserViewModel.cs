﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using UserManagement.Domain;

namespace UserManagement.ViewModels
{
    public class RegisterViewModel
    {
        public string UserName { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int ? CreatedByUserId { get; set; }
        public List<int> Roles { get; set; }
        public string CreatedBy { get; set; }
        public string IdentificationNumber { get; set; }
        public string ClientName { get; set; }
        public string CorrelationId { get; set; }
        public int IdentificationType { get; set; }

    }

    public class RegisterSspUserModel : RegisterViewModel
    {

    }

    public class UserDetailsModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string IdentificationNumber { get; set; }
        public int IdentificationType { get; set; }
        public string EmailAddress { get; set; }
        public string CreatedDate { get; set; }
        public string PhoneNumber { get; set; }
        public ApplicationUserStatus Status { get; set; }
        public string strStatus
        {
            get { return Status.ToString(); }
        }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }       
        public  DateTimeOffset ? LockoutEndDate { get; set; }
        public bool LockoutEnabled { get; set; }
        public string CorrelationId { get; set; }
    }

    public class UpdateUserCorrelationModel
    {
        public string CorrelationId { get; set; }
        public string UserId { get; set; }
    }

    public class ExistingApplicantAccountActivationModel
    {
        public string IdentificationNumber { get; set; }
        public string EmailAddress { get; set; }

    }



}
