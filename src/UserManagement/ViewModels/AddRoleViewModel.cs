﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace UserManagement.ViewModels
{
    public class RoleViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Enter the role name")]
        public string RoleName { get; set; }
        public int RoleId { get; set; }
        public List<int> Permissions { get; set; }
        public int CreatedById { get; set; }
        public string  CreatedBy { get; set; }
        public SelectList PermissionsSelectList { get; set; }

    }

    public class RolesList
    {
        public int Id { get; set; }
        public string RoleName { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }


    }

    public enum UserRoleStatus
    {
        Active,
        InActive
    }

    public class UserRoleViewModel
    {
        public string RoleName { get; set; }
        public string DateAddedToRole { get; set; }
        public string AddedToRoleBy { get; set; }
        public int RoleId { get; set; }
        public int UserId { get; set; }
        public string Status { get; set; }      
        public DateTime? DateRemovedFromRole { get; set; }
        public int? UserRemovedFromRoleById { get; set; }
    }

    public class RoleOperationsViewModel
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string Permission { get; set; }
        public string Description { get; set; }
    }

    public class AddUserToRoleViewModel
    {
        public int UserId { get; set; }
        public  List<int> RoleIds { get; set; }
        public List<SelectListItem> Roles { get; set; }

    }


}
