﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace UserManagement.ViewModels
{
    public class SetPasswordViewModel
    {      
        public string UserName { get; set; }

        public string Code { get; set; }

        [Required(ErrorMessage = "Password value is required")]
        [StringLength(100, ErrorMessage = "The password must be atleast {2} and at max {1} characters long", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm password value is required")]
        [DataType(DataType.Password)]
        [Compare("Password",ErrorMessage ="The password and confirmation password do not match")]
        public string ConfirmPassword { get; set; }
    }
}
