﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserManagement.Domain
{
    public class RolePermissionsView
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string Permission { get; set; }
        public string Description { get; set; }
    }
}
