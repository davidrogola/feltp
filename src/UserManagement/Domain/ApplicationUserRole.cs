﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace UserManagement.Domain
{
    public class ApplicationUserRole : IdentityUserRole<int>
    {
        public virtual DateTime DateAddedToRole { get; set; }
        public virtual int? UserAddedToRoleById { get; set; }
        public virtual DateTime? DateRemovedFromRole { get; set; }
        public virtual int? UserRemovedFromRoleById { get; set; }
        public virtual ApplicationUser UserAddedToRoleBy { get; set; }
        public virtual ApplicationUser UserRemovedFromRoleBy { get; set; }

        public void RemoveUserFromRole(int removedBy)
        {
            DateRemovedFromRole = DateTime.Now;
            UserRemovedFromRoleById = removedBy;
        }

        public void ActivateRole()
        {
            DateRemovedFromRole = null;
            UserRemovedFromRoleById = null;
        }


    }
}
