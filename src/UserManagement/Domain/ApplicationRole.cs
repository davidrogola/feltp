﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace UserManagement.Domain
{
    public class ApplicationRole : IdentityRole<int>
    {
        public virtual int? CreatedById { get; set; }
        public virtual DateTime DateCreated { get; set; }
        public virtual int? DecomissionedById { get; set; }
        public virtual DateTime? DateDecomissioned { get; set; }

        public virtual ApplicationUser CreatedBy { get; set; }
        public virtual ApplicationUser DecomissionedBy { get; set; }

        public ApplicationRole()
        {
        }

        public ApplicationRole(string name, int userId)
        {
            base.Name = name;
            DateCreated = DateTime.UtcNow;
            CreatedById = userId;
            NormalizedName = name.ToUpper();
        }

       public void Update(string name)
        {
            Name = name;
        }
    }
}
