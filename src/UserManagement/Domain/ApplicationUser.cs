﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace UserManagement.Domain
{
    public enum ApplicationUserStatus : int
    {
        UnConfirmed = 1,
        Confirmed = 2,
        DeActivated = 3
    }
    public class ApplicationUser : IdentityUser<int>
    {
        public void UpdateUserDetails(string userName, string email, string phoneNumber)
        {
            bool emailChanged = Email != email;
            bool phoneNoChanged = PhoneNumber != phoneNumber;
            Email = email;
            UserName = userName;
            PhoneNumber = phoneNumber;
            UserStatus = emailChanged == true ? ApplicationUserStatus.UnConfirmed : UserStatus;
            EmailConfirmed = !emailChanged;
            PhoneNumberConfirmed = !phoneNoChanged;

        }
        public virtual DateTime DateCreated { get; set; }
        public virtual int? CreatedById { get; set; }
        public virtual ApplicationUser CreatedBy { get; set; }
        public virtual DateTime LastPasswordChangeDate { get; set; }
        public virtual ApplicationUserStatus UserStatus { get; set; }
        public virtual bool TermsAccepted { get; set; }
        public virtual DateTime? DateTermsAccepted { get; set; }
        public virtual  int ? IdentificationType { get; set; }
        public virtual string IdentificationNumber { get; set; }
        public virtual string CorrelationId { get; set; }  // ApplicantId on Feltp Core DB

        public void SetCorrelationId(string applicantId)
        {
            CorrelationId = applicantId;
        }

        public void AcceptTerms()
        {
            TermsAccepted = true;
            DateTermsAccepted = DateTime.Now;
        }
    }
}
