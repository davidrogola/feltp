﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace UserManagement.Domain
{

    public class ApplicationUserClaim : IdentityUserClaim<int>
    {
    }

    public enum CustomClaimType
    {
        Role,
        Permission
    }
}
