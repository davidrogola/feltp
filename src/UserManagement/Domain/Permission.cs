﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace UserManagement.Domain
{
    public class Permission
    {
        public Permission()
        {

        }
        public Permission(string name, string comment, int createdBy)
        {
            Name = name;
            Comment = comment;
            CreatedById = createdBy;
            DateCreated = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int Id { get; protected set; }
        [Required, StringLength(100)]
        public virtual string Name { get; protected set; }
        [StringLength(256)]
        public virtual string Comment { get; protected set; }
        public int ? CreatedById { get; set; }
        public DateTime DateCreated { get; set; }
        public virtual ApplicationUser CreatedBy { get; set; }

    }
}
