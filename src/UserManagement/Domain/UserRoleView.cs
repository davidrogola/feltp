﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserManagement.Domain
{
    public class UserRoleView
    {
        public string RoleName { get; set; }
        public DateTime DateAddedToRole { get; set; }
        public string AddedToRoleBy { get; set; }
        public int RoleId { get; set; }
        public int UserId { get; set; }
        public DateTime ? DateRemovedFromRole { get; set; }
        public int ? UserRemovedFromRoleById { get; set; }
    }
}
