﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserManagement.Domain
{
    public class RolePermissionMapping
    {

        public RolePermissionMapping()
        {

        }

        public RolePermissionMapping(int roleId, int permissionId, string createdBy)
        {
            RoleId = roleId;
            PermissionId = permissionId;
            DateCreated = DateTime.Now;
            CreatedBy = createdBy;

        }

        
        public int Id { get; set; }
        public int RoleId { get; set; }
        public int PermissionId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public virtual Permission Permission { get; set; }
        public virtual ApplicationRole Role { get; set; }
    }
}
