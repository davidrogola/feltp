﻿
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;


namespace UserManagement.Domain
{
   public  class ApplicationDbContext : IdentityDbContext<ApplicationUser,ApplicationRole,int,ApplicationUserClaim,ApplicationUserRole,ApplicationUserLogIn,
       ApplicationRoleClaim,ApplicationUserToken>
    {

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> dbContextOptions) :
            base(dbContextOptions)
        {
           
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<ApplicationUser>().ToTable("aspnetusers").HasKey(x => x.Id);
            builder.Entity<ApplicationRole>().ToTable("aspnetroles").HasKey(x => x.Id);
            builder.Entity<ApplicationUserRole>().ToTable("aspnetuserroles");
            builder.Entity<ApplicationUserClaim>().ToTable("aspnetuserclaims"); 
            builder.Entity<ApplicationRoleClaim>().ToTable("aspnetroleclaims");
            builder.Entity<ApplicationUserLogIn>().ToTable("aspnetuserlogins");
            builder.Entity<ApplicationUserToken>().ToTable("aspnetusertokens");
            builder.Entity<Permission>().ToTable(nameof(Permission).ToLower()).HasKey(x => x.Id);
            builder.Entity<RolePermissionMapping>().ToTable(nameof(RolePermissionMapping).ToLower()).HasKey(x => x.Id);
            builder.Entity<UserRoleView>().ToTable(nameof(UserRoleView).ToLower()).HasKey(x=>x.UserId);
            builder.Entity<RolePermissionsView>().ToTable(nameof(RolePermissionsView).ToLower()).HasKey(x => x.RoleId);
            builder.Entity<EmailTemplate>().ToTable(nameof(EmailTemplate)).HasKey(x => x.Id);

        }


    }
}
