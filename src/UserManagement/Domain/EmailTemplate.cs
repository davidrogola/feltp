﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace UserManagement.Domain
{
    public enum EmailType
    {
        [Display(Name = "Password Reset")]
        PasswordReset = 1,
        [Display(Name = "Account Activation")]
        AccountActivation,
        [Display(Name = "Forgot Password")]
        ForgotPassword,
        [Display(Name = "Email Change")]
        EmailChange
    }
    public class EmailTemplate
    {

        public EmailTemplate()
        {
        }

        public EmailTemplate(EmailType emailType, string subject, string body)
        {
            EmailType = emailType;
            Subject = subject;
            Body = body;
        }

        public int Id { get; set; }
        public EmailType EmailType { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }

}
