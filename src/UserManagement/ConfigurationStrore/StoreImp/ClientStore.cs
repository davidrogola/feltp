﻿using IdentityServer4.Models;
using IdentityServer4.Stores;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagement.ConfigurationStrore.Entities;

namespace UserManagement.ConfigurationStrore.StoreImp
{
    public class ClientStore : IClientStore
    {
        IdentityServerDbContext storeContext;

        public ClientStore(IdentityServerDbContext _storeContext)
        {
            storeContext = _storeContext;
        }
        public Task<Client> FindClientByIdAsync(string clientId)
        {
            var client = storeContext.Clients.AsNoTracking().First(x => x.ClientId == clientId);
            client?.MapDataFromEntity();

            return Task.FromResult(client?.Client);
        }
    }
}
