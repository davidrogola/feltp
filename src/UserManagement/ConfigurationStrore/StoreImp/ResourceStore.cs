﻿using IdentityServer4.Models;
using IdentityServer4.Stores;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagement.ConfigurationStrore.Entities;

namespace UserManagement.ConfigurationStrore.StoreImp
{
    public class ResourceStore : IResourceStore
    {
        IdentityServerDbContext configurationStore;
        public ResourceStore(IdentityServerDbContext _configurationStore)
        {
            configurationStore = _configurationStore;
        }

        public Task<ApiResource> FindApiResourceAsync(string name)
        {
            var apiResource = configurationStore.ApiResources.SingleOrDefault(x=>x.ApiResourceName == name);
            apiResource ?.MapDataFromEntity();
            return Task.FromResult(apiResource ?.ApiResource);
        }

        public Task<IEnumerable<ApiResource>> FindApiResourcesByScopeAsync(IEnumerable<string> scopeNames)
        {
            if (scopeNames == null) throw new ArgumentNullException(nameof(scopeNames));

            var apiResources = new List<ApiResource>();

            var apiResourcesEntities = configurationStore.ApiResources.AsNoTracking()
                .Where(x => scopeNames.Contains(x.ApiResourceName)).AsEnumerable();

            foreach (var apiResourceEntity in apiResourcesEntities)
            {
                apiResourceEntity.MapDataFromEntity();

                apiResources.Add(apiResourceEntity.ApiResource);
            }

            return Task.FromResult(apiResources.AsEnumerable());
        }

        public Task<IEnumerable<IdentityResource>> FindIdentityResourcesByScopeAsync(IEnumerable<string> scopeNames)
        {
            if (scopeNames == null) throw new ArgumentNullException(nameof(scopeNames));

            var identityResources = new List<IdentityResource>();

            var identityResourcesEntities = configurationStore.IdentityResources.AsNoTracking()
                .Where(x => scopeNames.Contains(x.IdentityResourceName)).AsEnumerable();

            foreach (var identityResourceEntity in identityResourcesEntities)
            {
                identityResourceEntity.MapDataFromEntity();

                identityResources.Add(identityResourceEntity.IdentityResource);
            }

            return Task.FromResult(identityResources.AsEnumerable());
        }

        public Task<Resources> GetAllResourcesAsync()
        {
            var resources = new Resources();

            var identityResources = configurationStore.IdentityResources.AsNoTracking().AsEnumerable();
            var apiResources = configurationStore.ApiResources.AsNoTracking().AsEnumerable();

            var apiResourceList = new List<ApiResource>();
            var identityResourceList = new List<IdentityResource>();

            foreach (var identityResource in identityResources)
            {
                identityResource.MapDataFromEntity();
                identityResourceList.Add(identityResource.IdentityResource);
            }

            foreach (var apiResource in apiResources)
            {
                apiResource.MapDataFromEntity();
                apiResourceList.Add(apiResource.ApiResource);
            }

            return Task.FromResult(new Resources(identityResourceList, apiResourceList));

        }
    }
}
