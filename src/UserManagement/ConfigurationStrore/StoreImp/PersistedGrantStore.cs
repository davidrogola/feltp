﻿using IdentityServer4.Models;
using IdentityServer4.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagement.ConfigurationStrore.Entities;

namespace UserManagement.ConfigurationStrore.StoreImp
{
    public class PersistedGrantStore : IPersistedGrantStore
    {
        IdentityServerDbContext configurationStore;
        public PersistedGrantStore(IdentityServerDbContext _configurationStore)
        {
            configurationStore = _configurationStore;
        }
        public Task<IEnumerable<PersistedGrant>> GetAllAsync(string subjectId)
        {
            return Task.FromResult(configurationStore.PersistedGrants.Where(x => x.SubjectId == subjectId).AsEnumerable());
        }

        public Task<PersistedGrant> GetAsync(string key)
        {
            return Task.FromResult(configurationStore.PersistedGrants.FirstOrDefault(x => x.Key == key));
        }

        public Task RemoveAllAsync(string subjectId, string clientId)
        {
            var grants = configurationStore.PersistedGrants.Where(x => x.SubjectId == subjectId && x.ClientId == clientId).AsEnumerable();

            configurationStore.PersistedGrants.RemoveRange(grants);
            configurationStore.SaveChanges();

            return Task.FromResult(0);
        }

        public Task RemoveAllAsync(string subjectId, string clientId, string type)
        {
            var grants = configurationStore.PersistedGrants.Where(x => x.SubjectId == subjectId 
            && x.ClientId == clientId && x.Type == type).AsEnumerable();

            configurationStore.PersistedGrants.RemoveRange(grants);
            configurationStore.SaveChanges();

            return Task.FromResult(0);
        }

        public Task RemoveAsync(string key)
        {
            var grant = configurationStore.PersistedGrants.Where(x => x.Key == key).FirstOrDefault();
            configurationStore.PersistedGrants.Remove(grant);
            return Task.FromResult(0);
        }

        public Task StoreAsync(PersistedGrant grant)
        {
            configurationStore.PersistedGrants.Add(grant);
            configurationStore.SaveChanges();
            return Task.FromResult(0);
        }
    }
}
