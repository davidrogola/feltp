﻿using IdentityServer4.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using UserManagement.Domain;

namespace UserManagement.ConfigurationStrore.Entities
{
    public class IdentityServerDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int, ApplicationUserClaim, ApplicationUserRole, ApplicationUserLogIn, ApplicationRoleClaim, ApplicationUserToken>
    {
        public IdentityServerDbContext(DbContextOptions<IdentityServerDbContext> options) : base(options)
        { }

        public DbSet<ClientEntity> Clients { get; set; }
        public DbSet<ApiResourceEntity> ApiResources { get; set; }
        public DbSet<IdentityResourceEntity> IdentityResources { get; set; }

        public DbSet<PersistedGrant> PersistedGrants { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<ClientEntity>().HasKey(m => m.ClientId);
            builder.Entity<ClientEntity>().ToTable("clients");

            builder.Entity<ApiResourceEntity>().HasKey(m => m.ApiResourceName);
            builder.Entity<ApiResourceEntity>().ToTable("apiresources");

            builder.Entity<IdentityResourceEntity>().HasKey(m => m.IdentityResourceName);
            builder.Entity<IdentityResourceEntity>().ToTable("identityresources");

            builder.Entity<PersistedGrant>().HasKey(x => x.Key);
            builder.Entity<PersistedGrant>().ToTable("persistedgrants");

            base.OnModelCreating(builder);
        }
    }
}
