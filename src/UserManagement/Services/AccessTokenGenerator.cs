﻿using IdentityModel.Client;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement.Services
{

    public enum TokenGenerationMode
    {
        Client_Credentials,
        User_Access_Token
    }
    public class AccessTokenGenerator
    {
        readonly IConfiguration configuration;
        public AccessTokenGenerator(IConfiguration _configuration)
        {
            configuration = _configuration;
        }

        public async Task<string> GenerateAccessTokenUsingClientCredentails(string resourceName)
        {
            var discoveryClient = new DiscoveryClient(configuration["IdentityConfiguration:IdentityProvider-Endpoint"])
            {
                Policy = new DiscoveryPolicy()
                {
                    RequireHttps = false
                }
            };
            var discoveryResponse = await discoveryClient.GetAsync();
  
            if (discoveryResponse.IsError)
            {
                return String.Empty;
            }
            var clientInfo = configuration.GetSection("IdentityConfiguration:Client");

            var tokenClient = new TokenClient(discoveryResponse.TokenEndpoint, clientInfo["ClientId"], clientInfo["ClientSecret"]);

            var tokenResponse = await tokenClient.RequestClientCredentialsAsync(resourceName);

            return tokenResponse.AccessToken;
        }
    }
}
