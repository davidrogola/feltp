﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UserManagement.Domain;

namespace UserManagement.Services
{
    public class RolePermissionsLoader
    {
        ApplicationDbContext identityDbContext;

        public RolePermissionsLoader(ApplicationDbContext _identityDbContext)
        {
            identityDbContext = _identityDbContext;
        }

        public ConcurrentDictionary<string, List<string>> GetRolePermissionMap()
        {
            ConcurrentDictionary<string, List<string>> dict = new ConcurrentDictionary<string, List<string>>();

            var allRoles = identityDbContext.Roles.AsNoTracking().ToList();

            foreach (var role in allRoles)
            {
                var rolePermissions = identityDbContext.Set<RolePermissionMapping>()
                    .Where(x => x.RoleId == role.Id).Include(x => x.Permission).Select(x => x.Permission.Name.Trim())
                    .ToList();

                dict.GetOrAdd(role.Name, rolePermissions);

            }

            return dict;
        }
    }
}
