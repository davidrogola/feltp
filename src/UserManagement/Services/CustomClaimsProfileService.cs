﻿using IdentityModel;
using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using UserManagement.Domain;

namespace UserManagement.Services
{
    public class CustomClaimsProfileService : IProfileService
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IUserClaimsPrincipalFactory<ApplicationUser> claimsFactory;
        public CustomClaimsProfileService(UserManager<ApplicationUser> _userManager, 
            IUserClaimsPrincipalFactory<ApplicationUser> _claimsFactory)
        {
            userManager = _userManager;
            claimsFactory = _claimsFactory;
        }
        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var sub = context.Subject.GetSubjectId();
            var user = await userManager.FindByIdAsync(sub);
            if (user == null)
                return;
            var principal = await claimsFactory.CreateAsync(user);

            var claims = principal.Claims.ToList();
            claims.Add(new Claim(JwtClaimTypes.GivenName, user.UserName));

            if(!string.IsNullOrEmpty(user.IdentificationNumber))
            claims.Add(new Claim("IdentificationNumber", user.IdentificationNumber));

            if(!string.IsNullOrEmpty(user.CorrelationId))
                claims.Add(new Claim("ApplicantId", user.CorrelationId));

            context.IssuedClaims = claims;
            return;
        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            var sub = context.Subject.GetSubjectId();
            var user = await userManager.FindByIdAsync(sub);
            context.IsActive = user != null;
        }
    }
}
