﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using UserManagement.Domain;

namespace UserManagement.Services
{
    public static class RolePermissionsInstaller
    {
        public static void AddRolePermissionMapDictionary(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton(provider =>
             {
                 using (var serviceScope = provider.CreateScope())
                 {
                     var applicationDbContext = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
                     var rolePermissionsLoader = new RolePermissionsLoader(applicationDbContext);

                     return rolePermissionsLoader.GetRolePermissionMap();
                 }
               
             });
    }
    }

    
}
