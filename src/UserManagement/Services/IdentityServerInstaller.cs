﻿using IdentityModel;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace UserManagement.Services
{
    public static class IdentityServerInstaller
    {
        public static void AddIdentityServerConfiguration(this IServiceCollection services,
            IConfiguration configuration, string [] scopes)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = "Cookies";
                options.DefaultChallengeScheme = "oidc";
            }).AddCookie(options=> {
                options.Cookie.Name = "Cookies";
                options.ExpireTimeSpan = TimeSpan.FromMinutes(30);
            })
           .AddOpenIdConnect("oidc", options =>
           {
               options.SignInScheme = "Cookies";
               options.Authority = configuration["IdentityConfiguration:IdentityProvider-Endpoint"];
               options.ClientId = configuration["IdentityConfiguration:Client:ClientId"];
               options.ClientSecret = configuration["IdentityConfiguration:Client:ClientSecret"];
               options.ResponseType = "code id_token";
               foreach (var scope in scopes)
               {
                   options.Scope.Add(scope);
               }
               options.SaveTokens = true;
               options.GetClaimsFromUserInfoEndpoint = true;
               options.ClaimActions.Remove("amr");
               options.ClaimActions.DeleteClaim("sid");
               options.ClaimActions.DeleteClaim("idp");
               options.RequireHttpsMetadata = false;

               options.Events = new OpenIdConnectEvents()
               {
                   OnUserInformationReceived = async context =>
                   {
                       // IDS4 returns multiple claim values as JSON arrays, which break the authentication handler
                       if (context.User.TryGetValue(JwtClaimTypes.Role, out JToken role))
                       {
                           var claims = new List<Claim>();
                           if (role.Type != JTokenType.Array)
                           {
                               claims.Add(new Claim(JwtClaimTypes.Role, (string)role));
                           }
                           else
                           {
                               foreach (var userRole in role)
                                   claims.Add(new Claim(JwtClaimTypes.Role, (string)userRole));
                           }
                           var identity = context.Principal.Identity as ClaimsIdentity;
                           identity.AddClaims(claims);
                       }
                   }
               };

               options.TokenValidationParameters = new TokenValidationParameters
               {
                   RoleClaimType = JwtClaimTypes.Role,
                   NameClaimType = JwtClaimTypes.Name
               };
           });
        }
    }
}
