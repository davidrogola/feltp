﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace UserManagement.Services
{
    public class IdentityUserRoleFinder : IFindUserRole
    {
        HttpContext httpContext;
        IRoleInfo[] allRoles;
        public IdentityUserRoleFinder(HttpContext _httpContext, IRoleInfo[] _allRoles)
        {
            httpContext = _httpContext;
            allRoles = _allRoles;
        }

        public IRoleInfo[] FindSignedInUserRoles()
        {
            var user = httpContext.User;
            var userIdentity = (ClaimsIdentity)user.Identity;
            var roles = userIdentity.Claims
                .Where(c => c.Type == ClaimTypes.Role)
                .Select(c => c.Value)
                .ToList();
            return Array.FindAll(allRoles, c => roles.Contains(c.Name));
        }
    }
}
