﻿using IdentityServer4.Models;
using IdentityServer4.Stores;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UserManagement.ConfigurationStrore.Entities;
using UserManagement.ConfigurationStrore.StoreImp;

namespace UserManagement.Services
{
    public static class IdentityServerConfigStoreInstaller
    {
        public static void AddIdentityConfigurationStore(this IServiceCollection services, IConfiguration cofiguration)
        {
            services.AddDbContext<IdentityServerDbContext>
                (options => options.UseMySql(cofiguration.GetConnectionString("user-management")));

            services.AddTransient<IClientStore, ClientStore>();
            services.AddTransient<IResourceStore, ResourceStore>();
            services.AddTransient<IPersistedGrantStore, PersistedGrantStore>();
        }
    }

    public static class IdentityConfigurationStoreSeederInstaller
    {
        public static void SeedIdentityConfigurationStore(this IApplicationBuilder applicationBuilder, IConfiguration configuration, IdentityServerDbContext identityServerConfigStore)
        {
            var configStoreSeeder = new IdentityConfigurationStoreSeeder(identityServerConfigStore, configuration);
            configStoreSeeder.SeedDatabase();
        }
    }

    public class IdentityConfigurationStoreSeeder
    {
        IdentityServerDbContext configStore;
        IConfiguration configuration;
        public IdentityConfigurationStoreSeeder(IdentityServerDbContext _configStore, IConfiguration _configuration)
        {
            configStore = _configStore;
            configuration = _configuration;
        }

        public void SeedDatabase()
        {
            if (!configStore.ApiResources.Any())
                configStore.ApiResources.AddRange(BuildApiResourceEntityList(IdentityConfiguration.GetApiResources().ToList()));

            if (!configStore.Clients.Any())
                configStore.Clients.AddRange(BuildClientEntityList(IdentityConfiguration.GetClients(configuration).ToList()));

            if (!configStore.IdentityResources.Any())
                configStore.IdentityResources.AddRange(BuildIdentityResourceEntityList(IdentityConfiguration.GetIdentityResources().ToList()));

            configStore.SaveChanges();
        }

        private IEnumerable<ClientEntity> BuildClientEntityList(List<Client> clients)
        {
            var clientEntities = new List<ClientEntity>();
            foreach (var client in clients)
            {
                var clientEntity = new ClientEntity
                {
                    Client = client
                };

                clientEntity.AddDataToEntity();
                clientEntities.Add(clientEntity);
            }
            return clientEntities;
        }

        private IEnumerable<IdentityResourceEntity> BuildIdentityResourceEntityList(List<IdentityResource> identityResources)
        {
            var identityResourceEntities = new List<IdentityResourceEntity>();

            foreach (var identityResource in identityResources)
            {
                var idResourceEntity = new IdentityResourceEntity
                {
                    IdentityResource = identityResource
                };

                idResourceEntity.AddDataToEntity();
                identityResourceEntities.Add(idResourceEntity);
            }

            return identityResourceEntities;
        }

        private IEnumerable<ApiResourceEntity> BuildApiResourceEntityList(List<ApiResource> apiResources)
        {
            var apiResourceEntities = new List<ApiResourceEntity>();

            foreach (var apiResource in apiResources)
            {
                var apiResourceEntity = new ApiResourceEntity
                {
                    ApiResource = apiResource
                };

                apiResourceEntity.AddDataToEntity();
                apiResourceEntities.Add(apiResourceEntity);
            }

            return apiResourceEntities;
        }
    }
}

