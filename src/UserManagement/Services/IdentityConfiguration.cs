﻿using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace UserManagement.Services
{
    public static class IdentityConfiguration
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
               new IdentityResources.OpenId(),
               new IdentityResources.Profile(),
               new IdentityResources.Address(),
               new IdentityResource("roles","Your Role(s)",new List<string>{"role"}),
               new IdentityResource("IdentificationNumber","Identification Number",new [] {"IdentificationNumber"}),
               new IdentityResource("ApplicantId","Registered Applicant Id",new [] {"ApplicantId"})
               
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>()
            {
                new ApiResource("ResidentManagementAPI","Resident Management API",new List<string>{ "roles" }),
                new ApiResource("ProgramManagementAPI","Program Management API",new List<string>{ "roles" }),
                new ApiResource("UserManagementAPI","User Management API",new List<string>{ "roles" }),
                new ApiResource("LookUpManagementAPI","LookUp Management API",new List<string>{ "roles" })
            };

        }

        public static IEnumerable<Client> GetClients(IConfiguration configuration)
        {
            var clients = new List<Client>()
            {
                                
                new Client
                {
                    ClientId = "feltp-ssp-client",
                    ClientName = "FELTP SSP",
                    AllowedGrantTypes = GrantTypes.HybridAndClientCredentials,
                    RedirectUris = new List<string>()
                    {
                        $"{configuration["Client-EndPoints:Feltp-Ssp"]}/signin-oidc"
                    },
                    PostLogoutRedirectUris = new List<string>
                    {
                         $"{configuration["Client-EndPoints:Feltp-Ssp"]}/signout-callback-oidc"
                    },
                    AllowedScopes =
                        {
                           IdentityServerConstants.StandardScopes.OpenId,
                           IdentityServerConstants.StandardScopes.Profile,
                           IdentityServerConstants.StandardScopes.Address,
                           "roles",
                           "ResidentManagementAPI",
                           "ProgramManagementAPI",
                           "UserManagementAPI",
                           "IdentificationNumber",
                           "ApplicantId",
                           "LookUpManagementAPI"
                        },
                        ClientSecrets =
                        {

                              new Secret("secret".Sha256())
                        },
                        RequireConsent = false,
                        AlwaysSendClientClaims = true,
                        AlwaysIncludeUserClaimsInIdToken = true
                }
            };
            return clients;
        }
    }

    public static class ApiResourceName
    {
        public const string ResidentManagementAPI = "ResidentManagementAPI";
        public const string ProgramManagementAPI = "ProgramManagementAPI";
        public const string UserManagementAPI = "UserManagementAPI";
        public const string LookUpManagementAPI = "LookUpManagementAPI";
    }
}
