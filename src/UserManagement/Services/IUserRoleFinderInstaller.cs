﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UserManagement.Domain;

namespace UserManagement.Services
{
    public static class UserRoleFinderInstaller
    {
        public static void AddUserRoleFinder(this IServiceCollection services)
        {
            services.AddScoped<IFindUserRole>(provider =>
            {
                var identityContext = provider.GetService<IdentityDbContext>();
                var httpContext = provider.GetService<IHttpContextAccessor>();
                var userRoles = identityContext.Set<ApplicationRole>()
                .AsNoTracking().Select(c => new {  c.Id,  c.Name })
                    .ToArray();

                var roles = Array.ConvertAll(userRoles, d => (IRoleInfo)new RoleInfo(d.Name, d.Id));

                return new IdentityUserRoleFinder(httpContext.HttpContext, roles);
            });

        }
    }
}
