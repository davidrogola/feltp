﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace UserManagement.Services
{
    public static class AuthenticationServiceInstaller
    {
        public static void AddUserAuthenticationService(this IServiceCollection services)
        {
            services.AddScoped<IUserAuthenticationService, UserAuthenticationService>();
        }
    }
}
