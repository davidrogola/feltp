﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserManagement.Services
{
    public interface IFindUserRole
    {
        IRoleInfo [] FindSignedInUserRoles();
    }

    public interface IRoleInfo
    {
        string Name { get; set; }
        int RoleId { get; set; }

    }

    public class RoleInfo : IRoleInfo
    {
        public RoleInfo(string name, int roleId)
        {
            Name = name;
            RoleId = roleId;
        }
        public string Name { get; set; }
        public int RoleId { get; set; }
    }


}
