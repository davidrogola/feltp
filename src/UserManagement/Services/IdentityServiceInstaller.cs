﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using UserManagement.Domain;
using static UserManagement.Services.CookieSharingKeyDirectoryFinder;

namespace UserManagement.Services
{
    public static class IdentityServiceInstaller
    {
        public static void AddIdentityServices(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            serviceCollection.Configure<CookiePolicyOptions>(options =>
            {
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            serviceCollection.AddDbContext<ApplicationDbContext>
               (options => options.UseMySql(configuration.GetConnectionString("user-management")));

            serviceCollection.AddIdentity<ApplicationUser, ApplicationRole>()
                 .AddEntityFrameworkStores<ApplicationDbContext>()
                 .AddDefaultTokenProviders();

            serviceCollection.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = false;
                options.Password.RequiredUniqueChars = 6;

                // Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                // User settings
                options.User.RequireUniqueEmail = true;
            });

            serviceCollection.AddDataProtection()
             .PersistKeysToFileSystem(GetKeyRingDirInfo(configuration["KeyRingFolder"]))
             .SetApplicationName("Feltp.Web.UAT");

            serviceCollection.ConfigureApplicationCookie(options =>
            {
                options.Cookie.Name = "Feltp.Web.SharedCookie";
                options.Cookie.HttpOnly = true;
                options.LoginPath = "/Account/Login";
                options.LogoutPath = "/Account/Logout";
                options.AccessDeniedPath = "/Account/AccessDenied";
                options.ExpireTimeSpan = TimeSpan.FromMinutes(Convert.ToInt16(configuration["CookieExpireTimeSpan"]));
                options.SlidingExpiration = true;
                options.ReturnUrlParameter = "returnUrl";
            });
            serviceCollection.AddAuthorization(options =>
            {
                options.DefaultPolicy = options.DefaultPolicy = new AuthorizationPolicyBuilder("Identity.Application")
                .RequireAuthenticatedUser().Build();

            });

        }
    }
}
