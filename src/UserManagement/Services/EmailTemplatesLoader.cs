﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UserManagement.Domain;

namespace UserManagement.Services
{
    public class EmailTemplatesLoader
    {
        ApplicationDbContext applicationDbContext;
        public EmailTemplatesLoader(ApplicationDbContext _applicationDbContext)
        {
            applicationDbContext = _applicationDbContext;
        }

        public ConcurrentDictionary<EmailType, EmailTemplateViewModel> LoadEmailTemplates()
        {
            var templateDict = new ConcurrentDictionary<EmailType, EmailTemplateViewModel>();

            var emailTemplates = applicationDbContext.Set<EmailTemplate>().AsNoTracking().ToList();

            foreach (var template in emailTemplates)
            {
                templateDict.GetOrAdd(template.EmailType, new EmailTemplateViewModel
                {
                    Body = template.Body,
                    Subject = template.Subject
                });
            }

            return templateDict;
        }
    }

    public class EmailTemplateViewModel
    {
        public string Subject { get; set; }
        public string Body { get; set; }

    }

    public static class EmailTemplatesDictionaryInstaller
    {
        public static void AddEmailTemplates(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton(provider =>
            {
                using (var serviceScope = provider.CreateScope())
                {
                    var dbContext = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
                    var emailTemplatesLoader = new EmailTemplatesLoader(dbContext);

                    return emailTemplatesLoader.LoadEmailTemplates();
                }

            });
        }
    }

}
