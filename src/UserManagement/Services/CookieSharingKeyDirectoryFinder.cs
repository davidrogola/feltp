﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace UserManagement.Services
{
    public static class CookieSharingKeyDirectoryFinder
    {
        public static DirectoryInfo GetKeyRingDirInfo(string webContentRootPath)
        {

            var keyRingDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Feltp", "data-protection");
#if !DEBUG
              keyRingDir = Path.Combine(webContentRootPath, "Feltp", "data-protection");

#endif

            if (!Directory.Exists(keyRingDir))
                return Directory.CreateDirectory(keyRingDir);

            return new DirectoryInfo(keyRingDir);
        }
    }
}

