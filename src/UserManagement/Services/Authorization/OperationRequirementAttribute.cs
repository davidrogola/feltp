﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc.Filters;
using UserManagement.Domain;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace UserManagement.Services.Authorization
{
    public class OperationAttribute : TypeFilterAttribute
    {   
        public OperationAttribute(string Name) : base(typeof(OperationRequirementFilter))
        {
            Arguments = new object[] { new Claim(CustomClaimType.Role.ToString(), Name) };
        }
    }

    public class OperationRequirementFilter : IAuthorizationFilter
    {
        readonly Claim claim;
        ConcurrentDictionary<string, List<string>> rolePermissionMap;
        public OperationRequirementFilter(Claim _claim, ConcurrentDictionary<string, List<string>> _rolePermissionMap)
        {
            claim = _claim;
            rolePermissionMap = _rolePermissionMap;
        }
        public void OnAuthorization(AuthorizationFilterContext context)
        {         
            var userRoleClaims = context.HttpContext.User.Claims.Where(x => x.Type.ToLower() == claim.Type.ToLower()).Select(x=>x.Value).ToList();

            var rolePermissions = rolePermissionMap.Where(x => userRoleClaims.Contains(x.Key)).SelectMany(x => x.Value)
                .Distinct().Select(x=>x.ToLowerInvariant());


            var hasPermission = rolePermissions.Contains(claim.Value.ToLowerInvariant().Trim());
            if (!hasPermission)
                context.Result = new ForbidResult();
        }
    }
}
