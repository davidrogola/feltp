﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UserManagement.Domain;
using UserManagement.ViewModels;

namespace UserManagement.Services
{

    public interface IUserAuthenticationService
    {
        Task<LogInResponse> SignInUser(LogInViewModel model);
        void SignOutUser();
    }

    public class UserAuthenticationService : IUserAuthenticationService
    {
        SignInManager<ApplicationUser> signInManager;
        public UserAuthenticationService(SignInManager<ApplicationUser> _signInManager)
        {
            signInManager = _signInManager;
        }

        public async Task<LogInResponse> SignInUser(LogInViewModel model)
        {
            ApplicationUser user = null;
            SignInResult signInResult = null;
            if (model.UserNameOrEmail.IndexOf('@') != -1)
            {
                user = await signInManager.UserManager.FindByEmailAsync(model.UserNameOrEmail);
            }
            else
            {
                user = await signInManager.UserManager.FindByNameAsync(model.UserNameOrEmail);
            }
            if (user == null)
            {
                return new LogInResponse()
                {
                    ApplicationUser = null,
                    SignInResult = SignInResult.Failed
                };
            }           
            
            if (string.IsNullOrEmpty(user.SecurityStamp))
            {
                await signInManager.UserManager.UpdateSecurityStampAsync(user);
            }

            signInResult = await signInManager.PasswordSignInAsync(user.UserName, model.Password, true, lockoutOnFailure: true);

            if (signInResult.Succeeded && user.AccessFailedCount > 0)
                await signInManager.UserManager.ResetAccessFailedCountAsync(user);

            return new LogInResponse()
            {
                ApplicationUser = user,
                SignInResult = signInResult
            };
        }

        public void SignOutUser()
        {
            signInManager.SignOutAsync();
        }
    }

    public class LogInResponse
    {
        public SignInResult SignInResult { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

    }
}
