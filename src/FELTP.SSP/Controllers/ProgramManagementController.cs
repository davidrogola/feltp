﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Application.Common.Services;
using Application.ProgramManagement.Queries;
using UserManagement.Services;
using Newtonsoft.Json;
using Application.Common.Models;
using FELTP.SSP.Models;
using X.PagedList;
using DataTables.AspNet.Core;
using DataTables.AspNet.AspNetCore;
using Application;
using System.Text.RegularExpressions;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using ResidentManagement.Domain;

namespace FELTP.SSP.Controllers
{
    public class ProgramManagementController : Controller
    {
        HttpRequestHelper httpRequestHelper;
        IMediator mediator;

        public ProgramManagementController(HttpRequestHelper _httpRequestHelper)
        {
            httpRequestHelper = _httpRequestHelper;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ProgramList()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> ProgramList(IDataTablesRequest request)
        {
            var programs = new List<ProgramViewModel>();
            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ProgramManagementAPI, "Program/GetPrograms");

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var apiResponseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                var apiResponseModel = JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

                if (apiResponseModel.Success)
                {
                    programs = JsonConvert.DeserializeObject<List<ProgramViewModel>>(apiResponseModel.ResponseInfo)
                        .OrderByDescending(x => x.DateCreated).ToList();

                   var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)? programs
                 : programs.Where(_item => _item.Name.Contains(request.Search.Value));
                    var dataPage = filteredData.Skip(request.Start).Take(request.Length);

                    var response = DataTablesResponse.Create(request, programs.Count(), filteredData.Count(), dataPage);
                    return new DataTablesJsonResult(response, true);
                }
            }
            return new DataTablesJsonResult(DataTablesResponse.Create(request, 0, 0, new List<GetProgramCoursesList>()));
        }

        public async Task<IActionResult> ProgramDetails(int id)
        {
            var programDetails = new ProgramViewModel();

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ProgramManagementAPI,
                $"Program/GetProgramById/{id}");

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var apiResponseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                var apiResponseModel = JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);
                if (apiResponseModel.Success)
                    programDetails = JsonConvert.DeserializeObject<ProgramViewModel>(apiResponseModel.ResponseInfo);
            }
            return View(programDetails);
        }

        [HttpPost]
        public async Task<IActionResult> GetProgramCourses(int? programId, IDataTablesRequest request)
        {
            var programsCourseList = new List<ProgramCoursesViewModel>();
            var Id = programId;
            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ProgramManagementAPI,
              $"Program/GetProgramCourses/{Id}");


            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var apiResponseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                var apiResponseModel = JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

                if (apiResponseModel.Success)
                {
                    programsCourseList = JsonConvert.DeserializeObject<List<ProgramCoursesViewModel>>(apiResponseModel.ResponseInfo)
                      .OrderByDescending(x => x.Id).ToList();
                    var activePrograms = programsCourseList.Where(x => x.Status == Status.Active.ToString());

                    var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                        ? activePrograms
                        : activePrograms.Where(_item => _item.ProgramName.ToLower().Contains(request.Search.Value));

                    var dataPage = filteredData.Skip(request.Start).Take(request.Length);

                    var response = DataTablesResponse.Create(request, activePrograms.Count(), filteredData.Count(), dataPage);
                    return new DataTablesJsonResult(response, true);
                }
            }

            return new DataTablesJsonResult(DataTablesResponse.Create(request, 0, 0, new List<GetProgramCoursesList>()));
        }

        [HttpPost]
        public async Task<IActionResult> GetProgramResourcesByProgramId(int? programId, IDataTablesRequest request)
        {
            var programResource = new List<ProgramResourceViewModel>();
            var Id = programId;
            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ProgramManagementAPI,
              $"Program/GetProgramResources/{Id}");


            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var apiResponseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                var apiResponseModel = JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

                if (apiResponseModel.Success)
                {
                    programResource = JsonConvert.DeserializeObject<List<ProgramResourceViewModel>>(apiResponseModel.ResponseInfo)
                        .OrderByDescending(x => x.DateCreated).ToList();

                    var filteredData = String.IsNullOrWhiteSpace(request.Search.Value) ? programResource
                  : programResource.Where(_item => _item.ResourceTitle.Contains(request.Search.Value));
                    var dataPage = filteredData.Skip(request.Start).Take(request.Length);

                    var response = DataTablesResponse.Create(request, programResource.Count(), filteredData.Count(), dataPage);
                    return new DataTablesJsonResult(response, true);
                }
            }

            return new DataTablesJsonResult(DataTablesResponse.Create(request, 0, 0, new List<ProgramResourceViewModel>()));
        }

       // [Authorize]
        public IActionResult GetProgramResourcesFiles(int? Id)
        {

            var resourcefile = new GetProgramResourceFile();
            resourcefile.resourceId = Id;
            return PartialView(resourcefile);
        }

        [HttpPost]
        public async Task<IActionResult> ViewProgramResourcesFiles(int? Id, IDataTablesRequest request)
        {
            var programResourceFiles = new List<ProgramResourceFilesViewModel>();
            
            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ProgramManagementAPI,
              $"Program/GetProgramResourcesFilesByResourceId/{Id}");

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var apiResponseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                var apiResponseModel = JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

                if (apiResponseModel.Success)
                {
                    programResourceFiles = JsonConvert.DeserializeObject<List<ProgramResourceFilesViewModel>>(apiResponseModel.ResponseInfo)
                        .OrderByDescending(x => x.DateCreated).ToList();

                    var filteredData = String.IsNullOrWhiteSpace(request.Search.Value) ? programResourceFiles
                  : programResourceFiles.Where(_item => _item.ContentType.Contains(request.Search.Value));
                    var dataPage = filteredData.Skip(request.Start).Take(request.Length);

                    var response = DataTablesResponse.Create(request, programResourceFiles.Count(), filteredData.Count(), dataPage);
                    return new DataTablesJsonResult(response, true);
                }
            }

            return new DataTablesJsonResult(DataTablesResponse.Create(request, 0, 0, new List<ProgramResourceFilesViewModel>()));
        }
        
        public async Task<IActionResult> DownLoadResourceFile(int Id)
        {
            var resourceFiles = new ProgramResourceFilesViewModel();

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ProgramManagementAPI,
             $"Program/GetProgramResourcesFilesById/{Id}");

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var apiResponseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                var apiResponseModel = JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);
                if (apiResponseModel.Success)
                 resourceFiles = JsonConvert.DeserializeObject<ProgramResourceFilesViewModel>(apiResponseModel.ResponseInfo);
                return File(resourceFiles.FileContent, resourceFiles.ContentType, resourceFiles.FileName);
            }

      
            return File(resourceFiles.FileContent, resourceFiles.ContentType, resourceFiles.FileName);
       
        }

        public async Task<IActionResult> GetResidentFieldPlacementSites()
        {
            return View();
        }
    }
}