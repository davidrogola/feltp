﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Application.Common.Models;
using Application.Common.Services;
using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using Application.ProgramManagement.QueryHandlers;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using FELTP.SSP.Models;
using IdentityModel;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProgramManagement.Domain.Program;
using UserManagement.Services;
using UserManagement.ViewModels;
using X.PagedList;

namespace FELTP.SSP.Controllers
{
    public class ProgramOfferController : Controller
    {
        HttpRequestHelper httpRequestHelper;    

        public ProgramOfferController(HttpRequestHelper _httpRequestHelper)
        {
            httpRequestHelper = _httpRequestHelper;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> _ViewProgramOffers(int? page)
        {
            var pageNumber = page == null || page <= 0 ? 1 : page.Value;
            var pageSize = 3;

            var programOffers = new List<ProgramOfferViewModel>();

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ProgramManagementAPI, "ProgramOffer/GetProgramOffers");

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var apiResponseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);

                var apiResponseModel = JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

                if (apiResponseModel.Success)
                {
                    programOffers = JsonConvert.DeserializeObject<List<ProgramOfferViewModel>>(apiResponseModel.ResponseInfo)
                        .OrderByDescending(x => x.DateCreated).ToList();

                    var pagedProgramOffers = programOffers.Skip((pageNumber - 1) * pageSize).Take(pageSize);

                    var pagedProgramOfferViewModel = new PagedProgramOfferViewModel()
                    {
                        ProgramOffers = programOffers.ToPagedList(pageNumber, pageSize)
                    };

                    return PartialView(pagedProgramOfferViewModel);
                }

            }

            return PartialView(new PagedProgramOfferViewModel
            {
                ProgramOffers = new StaticPagedList<ProgramOfferViewModel>(programOffers, pageNumber, pageSize, programOffers.Count)
            });
        }

        public async Task<IActionResult> _ViewProgramOfferRequirements(int id)
        {
            var programOfferRequirements = new ProgramOfferRequirementsViewModel();

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ProgramManagementAPI,
                $"ProgramOffer/GetProgramOfferEligiblityRequirements/{id}");

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var apiResponseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                var apiResponseModel = JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);
                if (apiResponseModel.Success)
                    programOfferRequirements = JsonConvert.DeserializeObject<ProgramOfferRequirementsViewModel>(apiResponseModel.ResponseInfo);

            }
            return PartialView(programOfferRequirements);
        }

        public async Task<IActionResult> ViewProgramOfferDetails(int Id)
        {

            ProgramOfferViewModel programOfferDetail = await GetProgramOfferDetails(Id);
            return View(programOfferDetail);
        }


        private async Task<ProgramOfferViewModel> GetProgramOfferDetails(int offerId)
        {
            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ProgramManagementAPI,
               $"ProgramOffer/GetProgramOfferById/{offerId}");

            ProgramOfferViewModel programOfferDetail = null;

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var apiResponseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                var apiResponseModel = JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);
                if (apiResponseModel.Success)
                    programOfferDetail = JsonConvert.DeserializeObject<ProgramOfferViewModel>(apiResponseModel.ResponseInfo);
            }

            return programOfferDetail;
        }

        public async Task<IActionResult> _ViewProgramOfferLocations(int offerId)
        {
            var programLocations = new List<ProgramLocationViewModel>();

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ProgramManagementAPI,
                $"ProgramOffer/GetProgramOfferLocations/{offerId}");

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var apiResponseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                var apiResponse = JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);
                if (apiResponse.Success)
                    programLocations = JsonConvert.DeserializeObject<List<ProgramLocationViewModel>>(apiResponse.ResponseInfo);
            }

            return PartialView(programLocations);
        }

        [Authorize]
        public async Task<IActionResult> Apply(int Id)
        {
            var identificationNumber = User.FindFirstValue("IdentificationNumber");
            var applicantId = User.FindFirstValue("ApplicantId");
            long personId = 0;

            if (string.IsNullOrEmpty(applicantId))
            {
                var access_token = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);

                var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ResidentManagementAPI, $"Person/GetPersonByIdentificationNumber?idNumber={identificationNumber}", TokenGenerationMode.User_Access_Token, access_token);
                if (!httpResponseMessage.IsSuccessStatusCode)
                    return RedirectToAction("RegisterProfile", "Applicant");

                var responseContent = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                if (String.IsNullOrEmpty(responseContent))
                    return RedirectToAction("RegisterProfile", "Applicant");

                var personInfo = JsonConvert.DeserializeObject<PersonViewModel>(responseContent);
                personId = personInfo.BioDataInfo.Id;
            }

            var programOfferDetail = await GetProgramOfferDetails(Id);
            var model = new AddNewProgramOfferApplicationCommand
            {
                PersonId = personId,
                ApplicantId = !string.IsNullOrEmpty(applicantId) ? Convert.ToInt64(applicantId) : default(long),
                ProgramOfferId = Id,
                ProgramOfferDetail = programOfferDetail,
                ProgramId = programOfferDetail.ProgramId
            };

            return View(model);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Apply(AddNewProgramOfferApplicationCommand model, IFormFile StatementFile)
        {
            if (!ModelState.IsValid)
                return View(model);

            var accessToken = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);

            const string errorMessage = "An error occured while processing your request. Please try again";

            model.StatementFileCommand = null;
            var httpMessageResponse = await httpRequestHelper.SendPostJsonRequestAsync(ApiResourceName.ProgramManagementAPI,
                "ProgramOffer/AddProgramOfferApplication", JsonConvert.SerializeObject(model), TokenGenerationMode.User_Access_Token, accessToken);

            if (httpMessageResponse.IsSuccessStatusCode)
            {
                var responseString = await httpMessageResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
                var apiResponse = JsonConvert.DeserializeObject<ApiResponse>(responseString);
                if (apiResponse.Success)
                {
                    var addProgramOfferResponse = JsonConvert.DeserializeObject<AddProgramOfferApplicationResponse>(apiResponse.ResponseInfo);
                    if (string.IsNullOrEmpty(User.FindFirstValue("ApplicantId")))
                    {
                        var userCorrelationModel = new UpdateUserCorrelationModel
                        {
                            CorrelationId = addProgramOfferResponse.ApplicantId.ToString(),
                            UserId = User.FindFirstValue(JwtClaimTypes.Subject)
                        };
                       var response = await httpRequestHelper.SendPostJsonRequestAsync(ApiResourceName.UserManagementAPI, "UserManagement/SetUserCorrelationValue", JsonConvert.SerializeObject(userCorrelationModel));
                    }

                    await SendPersonalStatementFile(StatementFile, addProgramOfferResponse.ProgramOfferApplicationId, accessToken);
                    return RedirectToAction(nameof(ProgramApplications), new { Id = addProgramOfferResponse.ApplicantId });
                }

                if (apiResponse.Errors != null)
                    apiResponse.Errors.ForEach(error =>
                    {
                        ModelState.AddModelError(error.ErrorCode, error.ErrorMessage);
                    });


            }
            ModelState.AddModelError("Error", errorMessage);
            return View(model);
        }

        private async Task SendPersonalStatementFile(IFormFile formFile, long programOfferApplicationId, string access_token)
        {
            if (formFile == null)
                return;
            using (var content = new MultipartFormDataContent())
            {
                content.Add(new StreamContent(formFile.OpenReadStream())
                {
                    Headers =
                    {
                        ContentLength = formFile.Length,
                        ContentType = new MediaTypeHeaderValue(formFile.ContentType)
                    }
                }, formFile.Name, formFile.FileName);

                content.Add(new StringContent(programOfferApplicationId.ToString()), "ProgramOfferApplicationId");

                var response = await httpRequestHelper.SendPostRequestAsync(ApiResourceName.ProgramManagementAPI, 
                    "ProgramOffer/AddApplicantPersonalStatementFile",content,TokenGenerationMode.User_Access_Token,access_token);
            }
        }


        public IActionResult ProgramApplications()
        {
            var applicantId = User.FindFirstValue("ApplicantId");
            return View(nameof(ProgramApplications), applicantId);
        }


        [HttpPost]
        public async Task<IActionResult> GetApplicantProgramOfferApplications(string Id, IDataTablesRequest request)
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ProgramManagementAPI,
                $"ProgramOffer/GetProgramOfferApplicationsByApplicantId/{Id}", TokenGenerationMode.User_Access_Token, accessToken);

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var apiResponseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);

                var programApplications = JsonConvert.DeserializeObject<List<ProgramOfferApplicationViewModel>>(apiResponseString);

                var filteredData = string.IsNullOrWhiteSpace(request.Search.Value)
                    ? programApplications
                    : programApplications.Where(_item => _item.Program.Contains(request.Search.Value));

                var dataPage = filteredData.Skip(request.Start).Take(request.Length);

                var response = DataTablesResponse.Create(request, programApplications.Count(), filteredData.Count(), dataPage);

                return new DataTablesJsonResult(response, true);
            }
            return new DataTablesJsonResult(DataTablesResponse.Create(request, 0, 0, new List<AcademicDetail>()));

        }

        public async Task<IActionResult> ApplicationDetails(long Id)
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);

            ProgramOfferApplicationViewModel applicationModel = null;
            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ProgramManagementAPI,
                $"ProgramOffer/GetProgramOfferApplicationById/{Id}", TokenGenerationMode.User_Access_Token, accessToken);
            if(httpResponseMessage.IsSuccessStatusCode)
            {
                var responseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                applicationModel = JsonConvert.DeserializeObject<ProgramOfferApplicationViewModel>(responseString);

                if(applicationModel != null)
                {
                    var unwantedStatuses = new ProgressStage[] { ProgressStage.FailedEvaluation, ProgressStage.WaitingList };

                    var progressIndicatorsDict = applicationModel.ProgressIndicator.ProgressStatusDict;

                    var invalidProgressStatuses = applicationModel.ProgressIndicator.InvalidStages;

                    var validStatuses = invalidProgressStatuses == null ? progressIndicatorsDict.OrderBy(x => x.Key)
                        .ToList() : progressIndicatorsDict.Where(x => !invalidProgressStatuses.Contains(x.Key)).ToList();

                    var currentStatus = (int)applicationModel.ProgressIndicator.CurrentStatus;

                    validStatuses = validStatuses.Where(x => (int)x.Key <= currentStatus).ToList();

                    if (applicationModel.ProgressIndicator.IsFinal)
                        validStatuses.Add(new KeyValuePair<ProgressStage, string>(ProgressStage.ApplicationClosed, "Application Closed"));

                    var totalStatusCount = validStatuses.Count();
                    var expectedTotalCount = progressIndicatorsDict.Where(x => !unwantedStatuses.Contains(x.Key)).Count();
                    var width = applicationModel.ProgressIndicator.IsFinal ? 100 / totalStatusCount : 100 / expectedTotalCount;

                    applicationModel.ProgressBuilder = new ProgressBuilder
                    {
                        ValidStatuses = validStatuses,
                        Width = (int)width
                    };
                }
            }
            return View(applicationModel);
        }


    }
}