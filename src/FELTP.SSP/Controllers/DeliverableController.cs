﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Application.Common.Models;
using Application.Common.Services;
using Application.ResidentManagement.Commands;
using Application.ResidentManagement.Queries;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using IdentityModel;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using UserManagement.Services;

namespace FELTP.SSP.Controllers
{
    public class DeliverableController : Controller
    {
        HttpRequestHelper httpRequestHelper;
        public DeliverableController(HttpRequestHelper _httpRequestHelper)
        {
            httpRequestHelper = _httpRequestHelper;
        }

        [HttpGet]
        public async Task<IActionResult> GetResidentActivityDeliverables(long activityId)
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ResidentManagementAPI,
                $"ApplicantManagement/GetResidentDeliverablesByActivityId/{activityId.ToString()}",
                TokenGenerationMode.User_Access_Token, accessToken);

            var residentDeliverables = new List<ResidentDeliverableViewModel>();

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var apiResponseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                if (apiResponseString == null)
                    return Json(residentDeliverables);
                residentDeliverables = JsonConvert.DeserializeObject<List<ResidentDeliverableViewModel>>(apiResponseString);
            }
            return Json(residentDeliverables);

        }

        public async Task<IActionResult> SubmitDeliverable(int programEnrollmentId, long residentId, int semesterId)
        {
            var model = new AddResidentDeliverableItemCommand
            {
                ResidentDeliverables = await GetDeliverablesDropdown(programEnrollmentId, semesterId),
                ResidentId = residentId
            };

            return PartialView(model);
        }

        [HttpPost]
        public async Task<IActionResult> SubmitDeliverable(AddResidentDeliverableItemCommand model, IFormFile DeliverableFile)
        {
            if (DeliverableFile == null)
            {
                ModelState.AddModelError("File", "Please select the file to upload");
                model.ResidentDeliverables = await GetDeliverablesDropdown(model.ProgramEnrollmentId, model.SemesterId);
                return PartialView(model);
            }

            model.CreatedBy = User.FindFirst(JwtClaimTypes.Name)?.Value;
            model.DateCreated = DateTime.Now;
            model.StrDateSubmitted = DateTime.Now.ToShortDateString();

            var accessToken = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);

            var httpResponseMessage = await httpRequestHelper.SendPostJsonRequestAsync(ApiResourceName.ResidentManagementAPI,
                "ApplicantManagement/SubmitResidentDeliverable", JsonConvert.SerializeObject(model), TokenGenerationMode.User_Access_Token, accessToken);

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var responseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                var apiResponse = JsonConvert.DeserializeObject<ApiResponse>(responseString);
                if (apiResponse.Success)
                {
                    var deliverableResponse = JsonConvert.DeserializeObject<SubmitDeliverableResponse>(apiResponse.ResponseInfo);
                    await SendDeliverableFileItem(DeliverableFile, deliverableResponse.DeliverableItemId, accessToken);

                    return PartialView("DeliverableResult", deliverableResponse);
                }

                if (!string.IsNullOrEmpty(apiResponse.Message))
                    ModelState.AddModelError("Response", apiResponse.Message);

                if (apiResponse.Errors != null)
                {
                    foreach (var error in apiResponse.Errors)
                    ModelState.AddModelError(error.ErrorCode, error.ErrorMessage);

                }
            }

            model.ResidentDeliverables = await GetDeliverablesDropdown(model.ProgramEnrollmentId, model.SemesterId);
            ModelState.AddModelError("Error", "An error occured while processing your request please try again later");
            return PartialView(model);
        }

        private async Task SendDeliverableFileItem(IFormFile deliverableFile, long residentDeliverableItemId, string access_token)
        {
            if (deliverableFile == null)
                return;
            using (var content = new MultipartFormDataContent())
            {
                content.Add(new StreamContent(deliverableFile.OpenReadStream())
                {
                    Headers =
                    {
                        ContentLength = deliverableFile.Length,
                        ContentType = new MediaTypeHeaderValue(deliverableFile.ContentType)
                    }
                }, deliverableFile.Name, deliverableFile.FileName);

                content.Add(new StringContent(residentDeliverableItemId.ToString()), "ResidentDeliverableItemId");

                await httpRequestHelper.SendPostRequestAsync(ApiResourceName.ResidentManagementAPI,
                    "ApplicantManagement/SubmitDeliverableFile", content, TokenGenerationMode.User_Access_Token, access_token);
            }
        }


        private async Task<List<SelectListItem>> GetDeliverablesDropdown(int enrollmentId, int semesterId)
        {
            var deliverables = await GetResidentDeliverable(enrollmentId, semesterId);
            var selectList = deliverables.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()

            }).ToList();

            return selectList;
        }

        public async Task<List<ResidentDeliverableViewModel>> GetResidentDeliverable(int enrollmentId, int semesterId)
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);

            var residentDeliverables = new List<ResidentDeliverableViewModel>();

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ResidentManagementAPI,
               $"ApplicantManagement/GetResidentDeliverables?Id={enrollmentId.ToString()}&semesterId={semesterId}",
               TokenGenerationMode.User_Access_Token, accessToken);

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var responseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                if (string.IsNullOrEmpty(responseString))
                    return residentDeliverables;

                residentDeliverables = JsonConvert.DeserializeObject<List<ResidentDeliverableViewModel>>(responseString);

            }

            return residentDeliverables;
        }

        public IActionResult ViewSubmissions(long deliverableId, string name)
        {

            return PartialView(new ResidentDeliverableSubmissionViewModel
            {
                ResidentDeliverableId = deliverableId,
                DeliverableName = name
            });
        }

        [HttpPost]
        public async Task<IActionResult> GetResidentDeliverableSubmissions(long deliverableId, IDataTablesRequest request)
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ResidentManagementAPI,
               $"ApplicantManagement/GetResidentDeliverableSubmissions/{deliverableId}",
               TokenGenerationMode.User_Access_Token, accessToken);

            var defaultResponse = new DataTablesJsonResult(DataTablesResponse
               .Create(request, 0, 0, new List<ResidentDeliverableSubmissionViewModel>()));

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var apiResponseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                if (apiResponseString == null)
                    return defaultResponse;

                var submissions = JsonConvert.DeserializeObject<List<ResidentDeliverableSubmissionViewModel>>(apiResponseString);

                var filteredData = string.IsNullOrWhiteSpace(request.Search.Value)
                    ? submissions
                    : submissions.Where(_item => _item.Description.ToLower().Contains(request.Search.Value.ToLower()));

                var dataPage = filteredData.Skip(request.Start).Take(request.Length);

                var response = DataTablesResponse.Create(request, submissions.Count(), filteredData.Count(), dataPage);

                return new DataTablesJsonResult(response, true);
            }
            return defaultResponse;
        }

        public async Task<IActionResult> DownloadSubmission(long submissionId)
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);
            var deliverableSubmission = new ResidentDeliverableSubmissionViewModel();

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ResidentManagementAPI,
               $"ApplicantManagement/GetDeliverableSubmission/{submissionId}",TokenGenerationMode.User_Access_Token, accessToken);

            if(httpResponseMessage.IsSuccessStatusCode)
            {
                var stringContent = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                if (!string.IsNullOrEmpty(stringContent))
                {
                    deliverableSubmission = JsonConvert.DeserializeObject<ResidentDeliverableSubmissionViewModel>(stringContent);
                    if (deliverableSubmission.FileName == null)
                        return NoContent();

                    return File(deliverableSubmission.File, deliverableSubmission.ContentType, deliverableSubmission.FileName);
                }
            }
            return NoContent();
        }


    }
}