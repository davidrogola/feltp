﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Application.Common.Services;
using Application.ProgramManagement.Queries;
using Application.ResidentManagement.Queries;
using Application.ResidentManagement.QueryHandlers;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProgramManagement.Domain.Program;
using UserManagement.Services;

namespace FELTP.SSP.Controllers
{

    [Authorize]
    public class CourseController : Controller
    {
        HttpRequestHelper httpRequestHelper;
        public CourseController(HttpRequestHelper _httpRequestHelper)
        {
            httpRequestHelper = _httpRequestHelper;
        }

        public async Task<IActionResult> ViewCourseInformation()
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);
            var applicantId = User.FindFirstValue("ApplicantId");
            ResidentCourseDetailViewModel courseInformation = null;

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ResidentManagementAPI,
                $"ApplicantManagement/GetApplicantCourseInformation/{applicantId}", TokenGenerationMode.User_Access_Token, accessToken);

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var responseContent = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                if (!string.IsNullOrEmpty(responseContent))
                {
                    courseInformation = JsonConvert.DeserializeObject<ResidentCourseDetailViewModel>(responseContent);
                }
            }
            return View(courseInformation);
        }


        [HttpPost]
        public async Task<IActionResult> GetResidentCourseWork(long Id, int? semesterId, IDataTablesRequest request)
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);

            var defaultResponse = new DataTablesJsonResult(DataTablesResponse.Create(request, 0, 0, new List<ResidentCourseWorkViewModel>()));
            string strSemesterId = semesterId.HasValue ? semesterId.Value.ToString() : null;


            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ResidentManagementAPI,
                $"ApplicantManagement/GetResidentCourseWork?Id={Id.ToString()}&semesterId={strSemesterId}", TokenGenerationMode.User_Access_Token, accessToken);

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var apiResponseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                if (apiResponseString == null)
                    return defaultResponse;

                var residentCourseWork = JsonConvert.DeserializeObject<List<ResidentCourseWorkViewModel>>(apiResponseString);


                var filteredData = string.IsNullOrWhiteSpace(request.Search.Value)
                    ? residentCourseWork
                    : residentCourseWork.Where(_item => _item.Course.ToLower().Contains(request.Search.Value.ToLower()));

                var dataPage = filteredData.Skip(request.Start).Take(request.Length);

                var response = DataTablesResponse.Create(request, residentCourseWork.Count(), filteredData.Count(), dataPage);

                return new DataTablesJsonResult(response, true);
            }
            return defaultResponse;

        }

      
        public IActionResult GetProgramResourcesFiles(int? Id)
        {

            var resourcefile = new GetProgramResourceFile
            {
                resourceId = Id
            };
            return PartialView(resourcefile);
        }

        [HttpPost]
        public async Task<IActionResult> GetResidentFieldPlacementActivities(long Id, int? semesterId, IDataTablesRequest request)
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);

            string strSemesterId = semesterId.HasValue ? semesterId.Value.ToString() : null;

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ResidentManagementAPI,
                $"ApplicantManagement/GetResidentFieldPlacementActivities?Id={Id}&semesterId={strSemesterId}",
                TokenGenerationMode.User_Access_Token, accessToken);

            var defaultResponse = new DataTablesJsonResult(DataTablesResponse
                .Create(request, 0, 0, new List<ResidentFieldPlacementActivityViewModel>()));

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var apiResponseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                if (apiResponseString == null)
                    return defaultResponse;

                var fieldPlacementActivity = JsonConvert.DeserializeObject<List<ResidentFieldPlacementActivityViewModel>>(apiResponseString);


                var filteredData = string.IsNullOrWhiteSpace(request.Search.Value)
                    ? fieldPlacementActivity
                    : fieldPlacementActivity.Where(_item => _item.Activity.ToLower().Contains(request.Search.Value.ToLower()));

                var dataPage = filteredData.Skip(request.Start).Take(request.Length);

                var response = DataTablesResponse.Create(request, fieldPlacementActivity.Count(), filteredData.Count(), dataPage);

                return new DataTablesJsonResult(response, true);
            }
            return defaultResponse;

        }

        [HttpPost]
        public async Task<IActionResult> GetResidentExaminationScores(long Id, int? semesterId, IDataTablesRequest request)
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);

            string strSemesterId = semesterId.HasValue ? semesterId.Value.ToString() : null;

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ResidentManagementAPI,
                $"ApplicantManagement/GetResidentExaminationScores?Id={Id.ToString()}&semesterId={strSemesterId}",
                TokenGenerationMode.User_Access_Token, accessToken);

            var defaultResponse = new DataTablesJsonResult(DataTablesResponse
                .Create(request, 0, 0, new List<ResidentExaminationScoreViewModel>()));

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var apiResponseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                if (apiResponseString == null)
                    return defaultResponse;

                var examinationScores = JsonConvert.DeserializeObject<List<ResidentExaminationScoreViewModel>>(apiResponseString);

                var filteredData = string.IsNullOrWhiteSpace(request.Search.Value)
                    ? examinationScores
                    : examinationScores.Where(_item => _item.Name.ToLower().Contains(request.Search.Value.ToLower()));

                var dataPage = filteredData.Skip(request.Start).Take(request.Length);

                var response = DataTablesResponse.Create(request, examinationScores.Count(), filteredData.Count(), dataPage);

                return new DataTablesJsonResult(response, true);
            }
            return defaultResponse;

        }

        public IActionResult ViewUnitsList(int Id, string course)
        {

            return PartialView(new ResidentUnitViewModel { ResidentCourseWorkId = Id, Course = course });
        }


        [HttpPost]
        public async Task<IActionResult> GetResidentUnitsList(long Id, IDataTablesRequest request)
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ResidentManagementAPI,
                $"ApplicantManagement/GetResidentUnits?courseworkId={Id.ToString()}",
                TokenGenerationMode.User_Access_Token, accessToken);

            var defaultResponse = new DataTablesJsonResult(DataTablesResponse.Create(request, 0, 0, new List<ResidentUnitViewModel>()));

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var apiResponseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                if (apiResponseString == null)
                    return defaultResponse;

                var residentUnits = JsonConvert.DeserializeObject<List<ResidentUnitViewModel>>(apiResponseString);

                var filteredData = string.IsNullOrWhiteSpace(request.Search.Value)
                    ? residentUnits
                    : residentUnits.Where(_item => _item.Unit.ToLower().Contains(request.Search.Value.ToLower()));

                var dataPage = filteredData.Skip(request.Start).Take(request.Length);

                var response = DataTablesResponse.Create(request, residentUnits.Count(), filteredData.Count(), dataPage);

                return new DataTablesJsonResult(response, true);
            }
            return defaultResponse;

        }

        public async Task<IActionResult> GetTimetableList(int Id, int timetableType, int? semesterId, IDataTablesRequest request)
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);

            string strSemesterId = semesterId.HasValue ? semesterId.Value.ToString() : null;

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ProgramManagementAPI,
                $"ProgramOffer/GetTimetableListByOfferId?Id={Id}&semesterId={strSemesterId}&type={timetableType}",
                TokenGenerationMode.User_Access_Token, accessToken);

            var defaultResponse = new DataTablesJsonResult(DataTablesResponse
                .Create(request, 0, 0, new List<TimetableViewModel>()));

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var apiResponseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                if (apiResponseString == null)
                    return defaultResponse;

                var timetableViewModel = JsonConvert.DeserializeObject<List<TimetableViewModel>>(apiResponseString);


                var filteredData = string.IsNullOrWhiteSpace(request.Search.Value)
                    ? timetableViewModel
                    : timetableViewModel.Where(_item => _item.UnitName.ToLower().Contains(request.Search.Value.ToLower()));

                var dataPage = filteredData.Skip(request.Start).Take(request.Length);

                var response = DataTablesResponse.Create(request, timetableViewModel.Count(), filteredData.Count(), dataPage);

                return new DataTablesJsonResult(response, true);
            }
            return defaultResponse;
        }
     

    }
}