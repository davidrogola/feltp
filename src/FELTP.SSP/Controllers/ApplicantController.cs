﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Application.Common.Models;
using Application.Common.Services;
using Application.ProgramManagement.QueryHandlers;
using Application.ResidentManagement.Queries;
using Common.Domain.Person;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using FELTP.SSP.Models;
using IdentityModel;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using UserManagement.Services;
using UserManagement.ViewModels;

namespace FELTP.SSP.Controllers
{
    [Authorize]
    public class ApplicantController : Controller
    {
        HttpRequestHelper httpRequestHelper;

        public ApplicantController(HttpRequestHelper _httpRequestHelper)
        {
            httpRequestHelper = _httpRequestHelper;
        }

        public async Task<IActionResult> RedirectProfile()
        {
            var identificationNumber = User.FindFirstValue("IdentificationNumber");
            var applicantId = User.FindFirstValue("ApplicantId");

            if (!string.IsNullOrEmpty(applicantId))
                return RedirectToAction("ApplicantProfile");

            var accessToken = await HttpContext.GetTokenAsync("access_token");

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ResidentManagementAPI,
                $"Person/GetPersonByIdentificationNumber?idNumber={identificationNumber}", TokenGenerationMode.User_Access_Token, accessToken);

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var stringResponse = await httpResponseMessage.Content.ReadAsStringAsync();
                if (string.IsNullOrEmpty(stringResponse))
                    return RedirectToAction(nameof(RegisterProfile));

                var personInfo = JsonConvert.DeserializeObject<PersonViewModel>(stringResponse);

                HttpContext.Session.SetString("PersonId", personInfo.BioDataInfo.Id.ToString());
                return RedirectToAction("ProfileInfo");
            }

            return RedirectToAction(nameof(RegisterProfile));
        }

        public async Task<IActionResult> ApplicantProfile()
        {
            var access_token = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);

            var applicantId = User.FindFirstValue("ApplicantId");
            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ResidentManagementAPI,
                $"ApplicantManagement/GetApplicantById/{applicantId}", TokenGenerationMode.User_Access_Token, access_token);

            if (!httpResponseMessage.IsSuccessStatusCode)
                return View(new ApplicantViewModel());

            var responseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
            if (string.IsNullOrEmpty(responseString))
                View(new ApplicantViewModel());

            var applicant = JsonConvert.DeserializeObject<ApplicantViewModel>(responseString);
            HttpContext.Session.SetString("PersonId", applicant.Person.BioDataInfo.Id.ToString());

            return View(applicant);
        }

        private async Task<UserDetailsModel> GetUserDetails()
        {
            try
            {
                var userId = User.FindFirstValue(JwtClaimTypes.Subject);

                var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.UserManagementAPI,
                    $"UserManagement/GetUserDetails/{userId}");
                if (!httpResponseMessage.IsSuccessStatusCode)
                    return null;

                var responseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                var apiResponse = JsonConvert.DeserializeObject<ApiResponse>(responseString);

                var userDetails = JsonConvert.DeserializeObject<UserDetailsModel>(apiResponse.ResponseInfo);
                return userDetails;
            }
            catch (Exception ex)
            {

                return null;
            }

        }

        public async Task<IActionResult> RegisterProfile()
        {
            var userDetails = await GetUserDetails();
            var personCommand = new AddPersonCommand
            {
                BioDataInfo = new BioDataInformation
                {
                    IdentificationNumber = userDetails?.IdentificationNumber,
                    IdentificationType = (IdentificationType)userDetails?.IdentificationType,
                },
                ContactInfo = new ContactInfo
                {
                    EmailAddress = userDetails?.EmailAddress,
                    PrimaryMobileNumber = userDetails?.PhoneNumber
                }
            };

            return View(new PersonModel { Person = personCommand });
        }

        [HttpPost]
        public async Task<IActionResult> RegisterProfile(PersonModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            string errorMessage = "An error occured while processing your request.Please try again";
            var accessToken = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);

            var httpMessageResponse = await httpRequestHelper.SendPostJsonRequestAsync(ApiResourceName.ResidentManagementAPI, "Person/AddPerson", JsonConvert.SerializeObject(model.Person), TokenGenerationMode.User_Access_Token, accessToken);

            if (!httpMessageResponse.IsSuccessStatusCode)
            {
                ModelState.AddModelError("Error", errorMessage);
                return View(model);
            }

            var responseString = await httpMessageResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
            var apiResponse = JsonConvert.DeserializeObject<ApiResponse>(responseString);

            if (apiResponse.Success)
            {
                var responseInfo = JsonConvert.DeserializeObject<dynamic>(apiResponse.ResponseInfo);

                long personId = Convert.ToInt64(responseInfo["PersonId"].ToString());

                if (personId != default(long))
                {
                    HttpContext.Session.SetString("PersonId", personId.ToString());
                    return RedirectToAction(nameof(AcademicInfoSetUp));
                }
            }

            if (apiResponse.Errors.Any())
            {
                apiResponse.Errors.ForEach(x =>
                {
                    ModelState.AddModelError(x.ErrorCode, x.ErrorMessage);
                });
            }

            ModelState.AddModelError("Error", errorMessage);
            return View(model);
        }

        public async Task<IActionResult> AcademicInfoSetUp()
        {
            var selectListTuple = await BuildAcademicInfoSelectLists();
            var personId = HttpContext.Session.GetString("PersonId");

            var model = new AddPersonAcademicInfoCommand
            {
                AcademicInfo = new AcademicDetail
                {
                    PersonId = Convert.ToInt64(personId),
                    UniversitySelectList = selectListTuple.Item2,
                    UniversityCoursesSelectList = selectListTuple.Item1
                },
            };

            return View(model);
        }

        public async Task<IActionResult> _AcademicInfoSetUp()
        {
            var selectListTuple = await BuildAcademicInfoSelectLists();
            var personId = HttpContext.Session.GetString("PersonId");

            var model = new AddPersonAcademicInfoCommand
            {
                AcademicInfo = new AcademicDetail
                {
                    PersonId = Convert.ToInt64(personId),
                    UniversitySelectList = selectListTuple.Item2,
                    UniversityCoursesSelectList = selectListTuple.Item1
                },
            };
            return PartialView(model);
        }

        [HttpPost]
        public async Task<IActionResult> _AcademicInfoSetUp(AddPersonAcademicInfoCommand model)
        {
            var actionResult = await SendAcademicInfoRequest(model, true);
            return actionResult;
        }

        [HttpPost]
        public async Task<IActionResult> AcademicInfoSetUp(AddPersonAcademicInfoCommand model)
        {
            var actionResult = await SendAcademicInfoRequest(model);
            return actionResult;
        }

        private async Task<IActionResult> SendAcademicInfoRequest(AddPersonAcademicInfoCommand model, bool isPartial=false)
        {
            if (!ModelState.IsValid)
            {
                model = await RebuildAcademicInfoModel(model);
                if (isPartial)
                    return PartialView(model);
                return View(model);
            }

            var accessToken = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);

            var httpResponseMessage = await httpRequestHelper.SendPostJsonRequestAsync(ApiResourceName.ResidentManagementAPI, "Person/AddPersonAcademicInfo", JsonConvert.SerializeObject(model), TokenGenerationMode.User_Access_Token, accessToken);

            string errorMessage = "An error occured while processing your request.Please try again";

            if (!httpResponseMessage.IsSuccessStatusCode)
            {
                ModelState.AddModelError("Error", errorMessage);
                model = await RebuildAcademicInfoModel(model);
                if (isPartial)
                    return PartialView(model);
                return View(model);
            }

            var apiResponseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);

            var apiResponseModel = JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (apiResponseModel.Success)
            {
                if (isPartial)
                    return PartialView("AcademicInfoAdded", model.AcademicInfo.PersonId.ToString());

                return RedirectToAction(nameof(AcademicProfileInfo));
            }

            if (apiResponseModel.Errors.Any())
            {
                foreach (var error in apiResponseModel.Errors)
                {
                    ModelState.AddModelError(error.ErrorCode, error.ErrorMessage);

                }
                model = await RebuildAcademicInfoModel(model);
            }
            if (isPartial)
                return PartialView(model);
            return View(model);
        }

        public async Task<AddPersonAcademicInfoCommand> RebuildAcademicInfoModel(AddPersonAcademicInfoCommand model)
        {
            var selectListTuple = await BuildAcademicInfoSelectLists();
            model.AcademicInfo.UniversityCoursesSelectList = selectListTuple.Item1;
            model.AcademicInfo.UniversitySelectList = selectListTuple.Item2;

            return model;
        }

        private async Task<Tuple<SelectList, List<SelectListItem>>> BuildAcademicInfoSelectLists()
        {
            var universityResponseString = await SendGetRequest("University/GetUniversitySelectList", ApiResourceName.LookUpManagementAPI);

            var coursesResponseString = await SendGetRequest("University/GetUniversityCourses", ApiResourceName.LookUpManagementAPI);

            var universitySelectList = !string.IsNullOrEmpty(universityResponseString) ? JsonConvert.DeserializeObject<List<SelectListItem>>(universityResponseString) : new List<SelectListItem>();

            var courseViewModel = !String.IsNullOrEmpty(coursesResponseString) ? JsonConvert.DeserializeObject<List<AcademicCourseViewModel>>(coursesResponseString) :
               new List<AcademicCourseViewModel>();

            return new Tuple<SelectList, List<SelectListItem>>(new SelectList(courseViewModel, "Id", "CourseName", null, "EducationLevel"), universitySelectList);
        }

        private async Task<string> SendGetRequest(string url, string resourceName)
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(resourceName, url, TokenGenerationMode.User_Access_Token, accessToken);

            if (!httpResponseMessage.IsSuccessStatusCode)
                return string.Empty;
            var response = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);

            return response;
        }


        public async Task<IActionResult> ProfileInfo()
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);
            var personId = HttpContext.Session.GetString("PersonId");

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ResidentManagementAPI,
                $"Person/GetPersonById?Id={personId}",TokenGenerationMode.User_Access_Token,accessToken);

            if (!httpResponseMessage.IsSuccessStatusCode)
                return View(new PersonViewModel());

            var responseContent = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);

            if(string.IsNullOrEmpty(responseContent))
                return View(new PersonViewModel());

            var personViewModel = JsonConvert.DeserializeObject<PersonViewModel>(responseContent);

            return View(personViewModel);
        }

        public IActionResult AcademicProfileInfo()
        {
            var personId = HttpContext.Session.GetString("PersonId");
            return View(nameof(AcademicProfileInfo), personId);
        }

        [HttpPost]
        public async Task<IActionResult> GetPersonAcademicInfo(string Id, IDataTablesRequest request)
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ResidentManagementAPI,
                $"Person/GetPersonAcademicInfo?Id={Convert.ToInt64(Id)}", TokenGenerationMode.User_Access_Token, accessToken);

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var apiResponseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);

                var academicInfo = JsonConvert.DeserializeObject<List<AcademicDetail>>(apiResponseString);

                var filteredData = string.IsNullOrWhiteSpace(request.Search.Value)
                    ? academicInfo
                    : academicInfo.Where(_item => _item.CourseName.Contains(request.Search.Value));

                var dataPage = filteredData.Skip(request.Start).Take(request.Length);

                var response = DataTablesResponse.Create(request, academicInfo.Count(), filteredData.Count(), dataPage);

                return new DataTablesJsonResult(response, true);
            }
            return new DataTablesJsonResult(DataTablesResponse.Create(request, 0, 0, new List<AcademicDetail>()));

        }
        private async Task<List<SelectListItem>> GetMinistrySelectListItems()
        {
            var access_token = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.LookUpManagementAPI, "Ministry/GetMinistrySelectList", TokenGenerationMode.User_Access_Token, access_token);
            if (!httpResponseMessage.IsSuccessStatusCode)
                return new List<SelectListItem>();

            var responseContent = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
            if (string.IsNullOrEmpty(responseContent))
                return new List<SelectListItem>();

            var selectListItem = JsonConvert.DeserializeObject<List<SelectListItem>>(responseContent);

            return selectListItem;
        }

        public async Task<AddPersonEmploymentInfoCommand> InitializeEmploymentInfoModel(long personId)
        {
            return new AddPersonEmploymentInfoCommand
            {
                EmploymentInfo = new EmploymentDetail
                {
                    MinistrySelectListItems = await GetMinistrySelectListItems(),
                    PersonId = personId
                }
            };
        }

        public async Task<IActionResult> EmploymentInfoSetUp()
        {
            var personId = HttpContext.Session.GetString("PersonId");

            var addEmploymentInfoCommand = await InitializeEmploymentInfoModel(Convert.ToInt64(personId));

            return View(addEmploymentInfoCommand);
        }

        public async Task<IActionResult> _EmploymentInfoSetUp()
        {
            var personId = HttpContext.Session.GetString("PersonId");
            var addEmploymentInfoCommand = await InitializeEmploymentInfoModel(Convert.ToInt64(personId));

            return PartialView(addEmploymentInfoCommand);
        }

        [HttpPost]
        public async Task<IActionResult> EmploymentInfoSetUp(AddPersonEmploymentInfoCommand model)
        {
            var response = await SendEmploymentInfoCommandRequest(model);

            return response;
        }

        [HttpPost]
        public async Task<IActionResult> _EmploymentInfoSetUp(AddPersonEmploymentInfoCommand model)
        {
            var response = await SendEmploymentInfoCommandRequest(model, isPartial: true);

            return response;
        }




        public async Task<IActionResult> SendEmploymentInfoCommandRequest(AddPersonEmploymentInfoCommand model, bool isPartial = false)
        {
            if (!ModelState.IsValid)
            {
                model.EmploymentInfo.MinistrySelectListItems = await GetMinistrySelectListItems();
                if (isPartial)
                    return PartialView();
                return View(model);
            }
            string errorMessage = "An error occured while processing your request.Please try again";

            var access_token = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);

            var httpResponseMessage = await httpRequestHelper.SendPostJsonRequestAsync(ApiResourceName.ResidentManagementAPI, "Person/AddPersonEmploymentHistory",JsonConvert.SerializeObject(model),TokenGenerationMode.User_Access_Token, access_token);

            if(!httpResponseMessage.IsSuccessStatusCode)
            {
                ModelState.AddModelError("Error", errorMessage);
                model.EmploymentInfo.MinistrySelectListItems = await GetMinistrySelectListItems();
                if (isPartial)
                    return PartialView(model);
                return View(model);
            }

            var apiResponseContent = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);

            var apiResponse = JsonConvert.DeserializeObject<ApiResponse>(apiResponseContent);
            if (apiResponse.Success)
            {
                if (isPartial)
                    return PartialView("EmploymentInfoAdded",  model.EmploymentInfo.PersonId.ToString());
                return RedirectToAction(nameof(EmploymentInfoProfile), new { Id = model.EmploymentInfo.PersonId });
            }

            if (apiResponse.Errors.Any())
            {
                foreach (var error in apiResponse.Errors)
                {
                    ModelState.AddModelError(error.ErrorCode, error.ErrorMessage);

                }
                model.EmploymentInfo.MinistrySelectListItems = await GetMinistrySelectListItems();
            }

            if (isPartial)
                return PartialView(model);
            return View(model);

        }

        public IActionResult EmploymentInfoProfile()
        {
            var personId = HttpContext.Session.GetString("PersonId");

            return View(nameof(EmploymentInfoProfile), personId);
        }


        [HttpPost]
        public async Task<IActionResult> GetPersonEmploymentInfo(string Id, IDataTablesRequest request)
        {
           var accessToken = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ResidentManagementAPI,
                $"Person/GetPersonEmploymentInfo?Id={Id}", TokenGenerationMode.User_Access_Token, accessToken);

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var apiResponseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);

                var employmentInfo = JsonConvert.DeserializeObject<List<EmploymentDetail>>(apiResponseString);

                var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                    ? employmentInfo
                    : employmentInfo.Where(_item => _item.EmployerName.Contains(request.Search.Value));

                var dataPage = filteredData.Skip(request.Start).Take(request.Length);

                var response = DataTablesResponse.Create(request, employmentInfo.Count(), filteredData.Count(), dataPage);

                return new DataTablesJsonResult(response, true);
            }
            return new DataTablesJsonResult(DataTablesResponse.Create(request, 0, 0, new List<AcademicDetail>()));

        }


    }
}