﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Common.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using UserManagement.Services;

namespace FELTP.SSP.Controllers
{
    public class LookUpController : Controller
    {
        HttpRequestHelper httpRequestHelper;
        string lookUpResource = "LookUpManagementAPI";
        public LookUpController(HttpRequestHelper _httpRequestHelper)
        {
            httpRequestHelper = _httpRequestHelper;
        }


        public async Task<IActionResult> GetCountryDropDown()
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token");
            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(lookUpResource,
                "LocationInfo/GetCountrySelectList", TokenGenerationMode.Client_Credentials, accessToken);

            if (!httpResponseMessage.IsSuccessStatusCode)
                return Json(new List<SelectListItem>());

            var responseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);

            var countrySelectList = JsonConvert.DeserializeObject<List<SelectListItem>>(responseString);

            return Json(countrySelectList);
        }

        public async Task<IActionResult> GetCountyDropDown(int countryId)
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token");

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(lookUpResource,
                $"LocationInfo/GetCountySelectList?countryId={countryId}", TokenGenerationMode.Client_Credentials, accessToken);

            if (!httpResponseMessage.IsSuccessStatusCode)
                return Json(new List<SelectListItem>());

            var responseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);

            var countySelectList = JsonConvert.DeserializeObject<List<SelectListItem>>(responseString);

            return Json(countySelectList);
        }


        public async Task<IActionResult> GetSubCountyDropDown(int countyId)
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token");

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(lookUpResource,
                $"LocationInfo/GetSubCountySelectList?countyId={countyId}", TokenGenerationMode.Client_Credentials, accessToken);

            if (!httpResponseMessage.IsSuccessStatusCode)
                return Json(new List<SelectListItem>());

            var responseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);

            var subCountySelectList = JsonConvert.DeserializeObject<List<SelectListItem>>(responseString);

            return Json(subCountySelectList);
        }

        public async Task<IActionResult> GetCadres()
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token");

            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(lookUpResource,
                "Cadre/GetCadreSelectList", TokenGenerationMode.Client_Credentials, accessToken);

            if (!httpResponseMessage.IsSuccessStatusCode)
                return Json(new List<SelectListItem>());

            var responseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);

            var cadres = JsonConvert.DeserializeObject<List<SelectListItem>>(responseString);

            return Json(cadres);
        }


    }
}