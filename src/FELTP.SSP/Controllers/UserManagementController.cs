﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Common.Models;
using Application.Common.Notifications;
using Application.Common.Services;
using Application.ProgramManagement.Queries;
using Common.Domain.Messaging;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using UserManagement.Services;
using UserManagement.ViewModels;

namespace FELTP.SSP.Controllers
{
    public class UserManagementController : Controller
    {
        HttpRequestHelper httpRequestHelper;
        IConfiguration configuration;
        IMediator mediator;

        public UserManagementController(IMediator _mediator, HttpRequestHelper _httpRequestHelper,
            IConfiguration _configuration)
        {
            httpRequestHelper = _httpRequestHelper;
            configuration = _configuration;
            mediator = _mediator;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterSspUserModel userModel)
        {
            userModel.ClientName = configuration["IdentityConfiguration:Client:ClientName"];

            if (!ModelState.IsValid)
                return View(userModel);

            var httpResponseMessage = await httpRequestHelper.SendPostJsonRequestAsync(ApiResourceName.UserManagementAPI,
                "UserManagement/CreateNewUser", JsonConvert.SerializeObject(userModel));

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var stringResponse = await httpResponseMessage.Content.ReadAsStringAsync();

                var apiResponse = JsonConvert.DeserializeObject<ApiResponse>(stringResponse);

                if (apiResponse.Success)
                {
                    var createdUserInfo = JsonConvert.DeserializeObject<dynamic>(apiResponse.ResponseInfo);

                    var callbackUrl = Url.Action("ActivateAccount", "UserManagement", new { token = createdUserInfo["EmailConfirmationToken"].ToString(), userName = createdUserInfo["UserName"].ToString() }, HttpContext.Request.Scheme, $"{HttpContext.Request.Host.Host}:{HttpContext.Request.Host.Port}");

                    await mediator.Publish(new UserCreatedNotification()
                    {
                        CallBackUrl = callbackUrl,
                        EmailAddress = userModel.Email,
                        Subject = "FELTP Self Service Portal Account Activation",
                        UserName = createdUserInfo["UserName"],
                        EmailTemplateBody = createdUserInfo["EmailBody"],
                        MessageType = MessageTypeEnum.AccountActivation
                    });
                    await SetUserCorrelationValue(userModel.IdentificationNumber, createdUserInfo["UserId"].ToString());

                    return RedirectToAction(nameof(AccountCreated));
                }

                if (apiResponse.Errors.Any())
                {
                    foreach (var error in apiResponse.Errors)
                        ModelState.AddModelError(error.ErrorCode, error.ErrorMessage);
                }
            }

            ModelState.AddModelError("Error", "An error occured while processing this request.Please try again.");
            return View(userModel);
        }

        private async Task SetUserCorrelationValue(string identificationNumber, string userId)
        {
            var httpResponseMessage = await httpRequestHelper.SendGetRequestAsync(ApiResourceName.ResidentManagementAPI, $"ApplicantManagement/GetApplicantByIdentificationNumber?idNumber={identificationNumber}");
            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var responseContent = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                if (string.IsNullOrEmpty(responseContent))
                    return;

                var applicantInfo = JsonConvert.DeserializeObject<ProgramOfferApplicationViewModel>(responseContent);

                var applicantId = applicantInfo?.ApplicantId;

                if (applicantId.HasValue)
                {
                    var userCorrelationModel = new UpdateUserCorrelationModel
                    {
                        CorrelationId = applicantId.Value.ToString(),
                        UserId = userId
                    };
                    var response = await httpRequestHelper.SendPostJsonRequestAsync(ApiResourceName.UserManagementAPI, "UserManagement/SetUserCorrelationValue", JsonConvert.SerializeObject(userCorrelationModel));
                }

            }

        }

        public IActionResult ActivateAccount(string token, string userName)
        {

            return View(new SetPasswordViewModel { UserName = userName, Code = token });
        }

        [HttpPost]
        public async Task<IActionResult> ActivateAccount(SetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return View();

            var httpResponseMessage = await httpRequestHelper.SendPostJsonRequestAsync(ApiResourceName.UserManagementAPI, "UserManagement/ActivateEmail", JsonConvert.SerializeObject(model));
            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var stringResponse = await httpResponseMessage.Content.ReadAsStringAsync();
                var apiResponse = JsonConvert.DeserializeObject<ApiResponse>(stringResponse);

                if (apiResponse.Success)
                {
                    await HttpContext.SignOutAsync("Cookies");
                    await HttpContext.SignOutAsync("oidc");

                    return RedirectToAction(nameof(AccountActivated));
                }

                if (apiResponse.Errors.Any())
                {
                    foreach (var error in apiResponse.Errors)
                        ModelState.AddModelError(error.ErrorCode, error.ErrorMessage);
                    return View(model);
                }
            }
            ModelState.AddModelError("ErrorOccured", "An error occured while processing your request.Please try again later");
            return View(model);
        }

        public IActionResult ExistingApplicantRegistration()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ExistingApplicantRegistration(ExistingApplicantAccountActivationModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var httpMessageResponse = await httpRequestHelper.SendPostJsonRequestAsync(ApiResourceName.UserManagementAPI, "UserManagement/GenerateExistingUserAccountActivationToken", JsonConvert.SerializeObject(model));

            if (httpMessageResponse.IsSuccessStatusCode)
            {
                var responseContent = await httpMessageResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
                var apiResponse = JsonConvert.DeserializeObject<ApiResponse>(responseContent);
                if (apiResponse.Success)
                {
                    var userResponseInfo = JsonConvert.DeserializeObject<dynamic>(apiResponse.ResponseInfo);

                    var callbackUrl = Url.Action("ActivateAccount", "UserManagement", new { token = userResponseInfo["ActivationToken"].ToString(), userName = userResponseInfo["UserName"].ToString() }, HttpContext.Request.Scheme, $"{HttpContext.Request.Host.Host}:{HttpContext.Request.Host.Port}");

                    await mediator.Publish(new UserCreatedNotification()
                    {
                        CallBackUrl = callbackUrl,
                        EmailAddress = model.EmailAddress,
                        Subject = "FELTP Self Service Portal Account Activation",
                        UserName = userResponseInfo["UserName"].ToString(),
                        EmailTemplateBody = userResponseInfo["EmailTemplate"].ToString(),
                    });
                    return RedirectToAction(nameof(AccountCreated));
                }

                apiResponse.Errors.ForEach(error =>
                {
                    ModelState.AddModelError(error.ErrorCode, error.ErrorMessage);
                });
            }
            ModelState.AddModelError("Error", "An error occured while processing your request. Please try again");
            return View(model);
        }

        public IActionResult AccountCreated()
        {
            return View();
        }

        public IActionResult AccountActivated()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult ForgotPassword(string email)
        {
            return View(new ForgotPasswordViewModel { Email = email });
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var httpMessageResponse = await httpRequestHelper.SendPostJsonRequestAsync(ApiResourceName.UserManagementAPI,
                "UserManagement/GenerateResetPasswordToken", JsonConvert.SerializeObject(model));
            if (httpMessageResponse.IsSuccessStatusCode)
            {
                var responseContent = await httpMessageResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
                if (!string.IsNullOrEmpty(responseContent))
                {
                    var apiResponse = JsonConvert.DeserializeObject<ApiResponse>(responseContent);
                    if (apiResponse.Success)
                    {
                        var responseBody = JsonConvert.DeserializeObject<dynamic>(apiResponse.ResponseInfo);

                        var passwordToken = responseBody["PasswordResetToken"].ToString();
                        var userName = responseBody["UserName"].ToString();
                        var email = responseBody["EmailAddress"].ToString();


                        var callbackUrl = Url.Action("ResetPassword", "UserManagement", new { token = passwordToken, userName }, HttpContext.Request.Scheme, $"{HttpContext.Request.Host.Host}:{HttpContext.Request.Host.Port}");

                        await mediator.Publish(new ForgotPasswordNotification()
                        {
                            CallBackUrl = callbackUrl,
                            EmailAddress = model.Email,
                            UserName = userName,
                            Subject = "FELTP Forgot Password",
                            MessageType = MessageTypeEnum.ForgotPassword
                        });

                        return RedirectToAction(nameof(ForgotPasswordResult));
                    }
                    ModelState.AddModelError("Result", apiResponse.Message);
                }
            }

            ModelState.AddModelError("Error", "Password reset request failed. Please try again");
            return View(model);
        }



        public IActionResult ForgotPasswordResult()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult ResetPassword(string token, string userName)
        {

            return View(new ResetPasswordViewModel { UserName = userName, Code = token });
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return View();

            var httpMessageResponse = await httpRequestHelper.SendPostJsonRequestAsync(ApiResourceName.UserManagementAPI,
                "UserManagement/ResetUserPassword", JsonConvert.SerializeObject(model));
            if (httpMessageResponse.IsSuccessStatusCode)
            {
                var responseContent = await httpMessageResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
                if (!string.IsNullOrEmpty(responseContent))
                {
                    var apiResponse = JsonConvert.DeserializeObject<ApiResponse>(responseContent);
                    if (apiResponse.Success)
                    {
                        var responseBody = JsonConvert.DeserializeObject<dynamic>(apiResponse.ResponseInfo);
                        var userName = responseBody["UserName"].ToString();
                        var email = responseBody["EmailAddress"].ToString();

                        var callBackUrl = Url.Action("RedirectProfile", "Applicant", new { }, HttpContext.Request.Scheme, $"{HttpContext.Request.Host.Host}:{HttpContext.Request.Host.Port}");

                        await mediator.Publish(new ResetPasswordNotification()
                        {
                            CallBackUrl = callBackUrl,
                            EmailAddress = email,
                            UserName = userName,
                            Subject = "FELTP Password Reset Succesful",
                            MessageType = MessageTypeEnum.PasswordReset
                        });

                        return RedirectToAction("RedirectProfile", "Applicant");
                    }

                    if (apiResponse.Errors != null)
                    {
                        foreach (var error in apiResponse.Errors)
                        {
                            ModelState.AddModelError(error.ErrorCode, error.ErrorMessage);
                        }
                    }
                }
            }

            ModelState.AddModelError("Error", "Password reset request failed. Please try again");
            return View(model);
        }

        public async Task SignOut()
        {
            await HttpContext.SignOutAsync("Cookies");
            await HttpContext.SignOutAsync("oidc");
        }
    }
}