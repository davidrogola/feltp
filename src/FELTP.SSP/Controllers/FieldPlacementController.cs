using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Application.Common.Models;
using Application.Common.Services;
using Application.ProgramManagement.Commands;
using Application.ResidentManagement.Queries;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProgramManagement.Domain.FieldPlacement;
using UserManagement.Services;

namespace FELTP.SSP.Controllers
{
    public class FieldPlacementController : Controller
    {
       private readonly HttpRequestHelper _httpRequestHelper;
        public FieldPlacementController(HttpRequestHelper httpRequestHelper)
        {
            _httpRequestHelper = httpRequestHelper;
        }

        public IActionResult ResidentFieldPlacementSites()
        {
            var applicantId = User.FindFirstValue("ApplicantId");
            return View(new GetResidentFieldPlacementSites{ApplicantId = Convert.ToInt64(applicantId)});
        }
        
        
        [HttpPost]
        public async Task<IActionResult> GetResidentFieldPlacementSites(long applicantId, IDataTablesRequest request)
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);
            var httpResponseMessage = await _httpRequestHelper.SendGetRequestAsync(ApiResourceName.ResidentManagementAPI,
                $"FieldPlacement/GetResidentFieldPlacementSites/{applicantId}",
                TokenGenerationMode.User_Access_Token, accessToken);

            var defaultResponse = new DataTablesJsonResult(DataTablesResponse
                .Create(request, 0, 0, new List<ResidentFieldPlacementInfoViewModel>()));

            if (!httpResponseMessage.IsSuccessStatusCode) 
                return defaultResponse;
            var apiResponseString = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
            if (apiResponseString == null)
                return defaultResponse;

            var submissions = JsonConvert.DeserializeObject<List<ResidentFieldPlacementInfoViewModel>>(apiResponseString);

            var filteredData = string.IsNullOrWhiteSpace(request.Search.Value)
                ? submissions
                : submissions.Where(_item => _item.Description.ToLower().Contains(request.Search.Value.ToLower()));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);

            var response = DataTablesResponse.Create(request, submissions.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public IActionResult ReviewSite(int id, string siteName)
        {
            return PartialView(new AddFieldPlacementEvaluationCommand{FieldPlacementSiteId = id,SiteName = siteName});
        }

        [HttpPost]
        public async Task<IActionResult> ReviewSite(AddFieldPlacementEvaluationCommand model)
        {
            model.ApplicantId = Convert.ToInt64(User.FindFirstValue("ApplicantId"));

            var accessToken = await HttpContext.GetTokenAsync("access_token").ConfigureAwait(false);

            var httpMessageResponse = await _httpRequestHelper.SendPostJsonRequestAsync(ApiResourceName.ResidentManagementAPI, "Review/EvaluateFieldPlacementSite", 
                JsonConvert.SerializeObject(model), TokenGenerationMode.User_Access_Token, accessToken);

            if (!httpMessageResponse.IsSuccessStatusCode)
            {
                ModelState.AddModelError("Error", "An error occured while reviewing placement site");
                return PartialView(model);
            }

            var responseString = await httpMessageResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
            var apiResponse = JsonConvert.DeserializeObject<FieldPlacementEvaluationResponse>(responseString);
            return PartialView("ReviewSiteResult",apiResponse);
        }

       
        
    }
}