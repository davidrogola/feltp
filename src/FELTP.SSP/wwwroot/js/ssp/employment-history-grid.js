﻿$(document).ready(function () {

    var personId = $("#PersonId").val();
    var loaderpath = $("#loader-input-path").val();
    var table = $('#employment_history_table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/Applicant/GetPersonEmploymentInfo",
            "type": "POST",
            "data": { Id: personId }
        },
        columns: [
            {
                name: 'PersonnelNumber',
                data: 'personnelNumber',
                title: "Personnel Number",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'WorkStation',
                data: 'workStation',
                title: "Work Station",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'Designation',
                data: 'designation',
                title: "Designation",
                sortable: true,
                searchable: true
            },
            {
                name: 'DurationInPosition',
                data: "durationInPosition",
                title: "Duration In Position (Years)",
                sortable: true,
                searchable: true
            },
            {
                name: 'EmployerName',
                data: "employerName",
                title: "Employer Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'YearOfEmployment',
                data: "yearOfEmployment",
                title: "Year Of Employment",
                sortable: true,
                searchable: true
            }
        ],
        language: {
            "emptyTable": "No Records found, Please click on <b>Add New</b> button.",
            processing: loaderpath
        },
        processing: true


    });


    $('#btn-add-employment-info').on("click", function (event) {
        event.preventDefault();
        $("#employmentinfo-profile-spinner").show();
        var url = $(this).attr("data-url");
        $.get(url, function (data) {
            $('#add-employment-info-container').html(data);
            $(".selectpicker").selectpicker("refresh");
            $('#add-employment-info-modal').modal('show');
            $("#employmentinfo-profile-spinner").hide();
        });
    });

    $("#add-employment-info-modal").on("click", "#add-employment-info-btn", function (event) {
        $("#add-employmentinfo-spinner").show();
        var form = $('form#employment-info-form');
        var action = $(form).attr('action');
        event.preventDefault();
        $.post(action, $(form).serialize(), function (data) {
            $("#add-employmentinfo-spinner").hide();
            $("#add-employment-info-container").html(data);
            $(".selectpicker").selectpicker("refresh");
            $("#add-employmentinfo-spinner").hide();
            toggle_hidden_employmentinfo_elements();
        }, 'html');
    });

});

$(document).ready(function () {

    $('#EmploymentInfo_EmployedByMinistry').on("change", function () {
        toggle_ministry_div(this.checked)
    });

    $('#EmploymentInfo_IsCurrentEmployer').on("change", function () {
        toggle_currentemployer_div(this.checked)
    });

    toggle_hidden_employmentinfo_elements();
});

function toggle_ministry_div(checked) {
    if (checked) {
        $(".ministry-dropdown-div").show();
        $(".employer-name-div").hide();
    }
    else {
        $(".employer-name-div").show();
        $(".ministry-dropdown-div").hide();
    }
}

function toggle_currentemployer_div(checked) {
    if (checked) {
        $(".employment-end-date-div").hide();
    }
    else {
        $(".employment-end-date-div").show();
    }
}

function toggle_hidden_employmentinfo_elements() {
    if ($("#EmploymentInfo_EmployedByMinistry").length > 0) {
        var employedByMinistry = document.getElementById("EmploymentInfo_EmployedByMinistry").checked;
        toggle_ministry_div(employedByMinistry);
    }

    if ($("#EmploymentInfo_IsCurrentEmployer").length > 0) {
        var isCurrentEmployer = document.getElementById("EmploymentInfo_IsCurrentEmployer").checked;
        toggle_currentemployer_div(isCurrentEmployer);
    }

}


$('#add-employment-info-modal').on("change", "#EmploymentInfo_EmployedByMinistry", function () {
    toggle_ministry_div(this.checked)
});

$('#add-employment-info-modal').on("change", "#EmploymentInfo_IsCurrentEmployer", function () {
    toggle_currentemployer_div(this.checked)
});