﻿function get_deliverable(data) {
    var result = '<div class="slider" style="margin-left:60px">'
    result += '<table id="resident-deliverable-list" class="table table-bordered table-striped" cellspacing="0">';
    result += '<thead><th>Code</th><th>Deliverable</th><th>Status</th><th>Actions</th></thead>';
    if (data.length > 0) {
        $.each(data, function (i, item) {
            result += '<tr">' +
                '<td>' + item.code + '</td>' +
                '<td>' + item.name + '</td>' +
                '<td>' + item.progressStatus + '</td>' +
                '<td>' + '<a href="/Deliverable/ViewSubmissions?deliverableId=' + item.id + '&name=' + item.name + '" class="view-deliverable-submissions">View Submissions </a> ' + '</td>' +
                '</tr>';
        });
    }
    else {
        result += '<tr">' +
            '<td>' + 'Deliverables not found' + '</td>' +
            '<td>' + 'N/A' + '</td>' +
            '<td>' + 'N/A' + '</td>' +
            '<td>' + 'N/A' + '</td>' +
            '</tr>';
    }

    result += '</table>'
    result += '</div>';
    return result;
};

$('#resident-activities-list-table').on("click", ".view-deliverable-submissions", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#resident-coursework-container').html(data);
        $('#resident-coursework-modal').modal('show');
    });
});

$(document).ready(function () {
    var programId = $("#ProgramId").val();
    var enrollmentId = $("#ProgramEnrollemntId").val();
    var programOfferId = $("#ProgramOfferId").val();
    getResidentCourses(enrollmentId, null);
    getResidentActivities(enrollmentId, null);
    getResidentExaminationScores(enrollmentId, null);
    getUnitTimetable(programOfferId, null);
    getExamTimetable(programOfferId, null);
    getProgramResources(programId);

});

$("#SemesterId").change(function () {
    var semesterId = $(this).val();
    $("#SelectedSemester").val(semesterId);

    var enrollmentId = $("#ProgramEnrollemntId").val();
    var offerId = $("#ProgramOfferId").val();
   

    getResidentExaminationScores(enrollmentId, semesterId);
    getResidentCourses(enrollmentId, semesterId);
    getResidentActivities(enrollmentId, semesterId);
    getUnitTimetable(offerId, semesterId);
    getExamTimetable(offerId, semesterId);
    
});

function getResidentCourses(enrollmentId, semesterId) {
    $('#resident-courses-list-table').dataTable({
        "serverSide": true,
        "proccessing": true,
        "destroy": true,
        "ajax": {
            "url": "/Course/GetResidentCourseWork",
            "type": "POST",
            "data": { Id: enrollmentId, semesterId: semesterId }
        },
        columns: [
            {
                name: 'id',
                data: 'residentCourseWorkId',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'coursetype',
                data: "courseType",
                title: "Course Type",
                sortable: true,
                searchable: true
            },
            {
                name: 'semester',
                data: "semester",
                title: "Semester",
                sortable: true,
                searchable: true
            },
            {
                name: 'course',
                data: "course",
                title: "Course",
                sortable: true,
                searchable: true
            },
            {
                name: 'status',
                data: 'progressStatus',
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                name: 'datecreated',
                data: 'dateCreated',
                title: "Date Created",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                "title": "Actions",
                "data": { "residentCourseWorkId": "residentCourseWorkId", "course": "course" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a class="view-resident-units" href="/Course/ViewUnitsList?id=' + data.residentCourseWorkId + '&course=' + data.course + '"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-list-alt"></i> View Units</button> </a>';
                }
            }
        ]
    });
}

$('#resident-courses-list-table').on("click", ".view-resident-units", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#resident-coursework-container').html(data);
        $('#resident-coursework-modal').modal('show');
    });
});

$('#resident-coursework-modal').on('show.bs.modal', function () {

    var courseWorkId = $("#ResidentCourseWorkId").val();

    $('#resident-unit-list').dataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/Course/GetResidentUnitsList",
            "type": "POST",
            "data": { Id: courseWorkId }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'code',
                data: 'code',
                title: "Code",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'unit',
                data: "unit",
                title: "Unit",
                sortable: true,
                searchable: true
            },
            {
                name: 'createdBy',
                data: 'createdBy',
                title: "Created By",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'datecreated',
                data: 'dateCreated',
                title: "Date Created",
                sortable: true,
                searchable: true,
                visible: false

            }
        ]
    });
});

// ben
function getProgramResources(programId) {
    $('#resident-resources-list-table').dataTable({
        "serverSide": true,
        "proccessing": true,
        "destroy": true,
        "ajax": {
            "url": "/programmanagement/GetProgramResourcesByProgramId",
            "type": "POST",
            "data": { programId }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'programId',
                data: 'programId',
                title: "Program Id",
                sortable: true,
                visible: false
            },
            {
                name: 'resourcesType',
                data: 'resourcesType',
                title: 'Resources Type',
                sortable: false,
                searchable: true
            },
            {
                name: 'resourceTitle',
                data: 'resourceTitle',
                title: "Resource Title",
                sortable: false,
                searchable: true
            },

            {
                name: 'resourceDescription',
                data: 'resourceDescription',
                title: "Resource Description",
                visible: false
            },




            {
                "title": "Actions",
                "data": { "id": "id", "programId": "programId" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/Course/GetProgramResourcesFiles?Id=' + data.id + '" class="ProgramCourseFiles" data-toggle="tooltip" title="Files"><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i>  View Files </button></a> ';

                }
            }
        ]
    });
}

$("#resident-resources-list-table").on("click", ".ProgramCourseFiles", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#viewFilesContainer').html(data);
        $('#viewResourceFilesModal').modal('show');
    });
});

$('#viewResourceFilesModal').on('show.bs.modal', function () {

    var resourceId = $("#resourceId").val();
    $('#resource-files-tables').dataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/ProgramManagement/ViewProgramResourcesFiles",
            "type": "POST",
            "data": { Id: resourceId }
        },
        columns: [

            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'resourceId',
                data: 'resourceId',
                title: "resource id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'fileName',
                data: 'fileName',
                title: "File Name",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'contentType',
                data: 'contentType',
                title: "Content Type",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'dateCreated',
                data: 'dateCreated',
                title: "Date Created",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'fileContent',
                data: 'fileContent',
                title: "File ",
                sortable: false,
                searchable: false,
                visible: false
            },

            {
                "title": "Actions",
                "data": { "id": "id" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/ProgramManagement/DownLoadResourceFile?Id=' + data.id + '" class="downLoadFile" data-toggle="tooltip" title="Download"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-download"></i> Download Resource File </button></a> ';
                }
            }
        ]
    });
});


function getResidentActivities(enrollmentId, semesterId) {
    var residentActivityTable = $('#resident-activities-list-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "destroy": true,
        "ajax": {
            "url": "/Course/GetResidentFieldPlacementActivities",
            "type": "POST",
            "data": { Id: enrollmentId, semesterId: semesterId }
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "width": "5%",
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'semester',
                data: "semester",
                title: "Semester",
                sortable: true,
                searchable: true
            },
            {
                name: 'activity',
                data: "activity",
                title: "Activity",
                sortable: true,
                searchable: true
            },
            {
                name: 'status',
                data: 'progressStatus',
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                name: 'datecreated',
                data: 'dateCreated',
                title: "Date Created",
                sortable: true,
                searchable: true,
                visible: false

            }
        ]
    });

    $('#resident-activities-list-table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = residentActivityTable.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            var rowData = row.data();
            $.getJSON("/Deliverable/GetResidentActivityDeliverables", { activityId: rowData.id }, function (data) {
                row.child(get_deliverable(data), 'no-padding').show();
                tr.addClass('shown');
                $('div.slider', row.child()).slideDown();
            });

        }
    });
}

function getResidentExaminationScores(enrollmentId, semesterId) {

    var residentexamTable = $('#resident-examinationscores-list').DataTable({
        "serverSide": true,
        "proccessing": true,
        "destroy": true,
        "ajax": {
            "url": "/Course/GetResidentExaminationScores",
            "type": "POST",
            "data": {Id: enrollmentId, semesterId: semesterId }
        },
        columns: [
            {
                name: 'residentCourseWorkId',
                data: 'residentCourseWorkId',
                title: "residentCourseWorkId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'residentUnitId',
                data: 'residentUnitId',
                title: "residentUnitId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'hasFieldPlacementActivity',
                data: 'hasFieldPlacementActivity',
                title: "hasFieldPlacementActivity",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'id',
                data: 'residentCourseWorkId',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'semester',
                data: "semester",
                title: "Semester",
                sortable: true,
                searchable: true
            },
            {
                name: 'code',
                data: "code",
                title: "Code",
                sortable: true,
                searchable: true
            },
            {
                name: 'unitName',
                data: 'name',
                title: "Unit Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'score',
                data: 'score',
                title: "Score",
                sortable: true,
                searchable: true
            },
            {
                name: 'dateCreated',
                data: 'dateCreated',
                title: "Date Created",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'createdBy',
                data: 'createdBy',
                title: "Created By",
                sortable: true,
                searchable: true,
                visible: false
            }
        ]
    });
}

$('.submit-resident-deliverable').on("click", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    event.preventDefault();

    var semesterId = $("#SelectedSemester").val();
    var enrollmentId = $("#ProgramEnrollemntId").val();
    var residentId = $("#ResidentId").val();
    if (semesterId == 0) {
        alert("Please select semester before submitting deliverables");
        return;
    }
    $.get(url, { programEnrollmentId: enrollmentId, residentId: residentId, semesterId: semesterId }, function (data) {
        $('#resident-coursework-container').html(data);
        $('#resident-coursework-modal').modal('show');
        $('.selectpicker').selectpicker();
    });
});


$("#resident-coursework-container").on('change',"#ProgrammaticAreaFound", function () {
        if (this.checked) {
            $("#programmaticarea-textarea-div").show();
            $("#programmaticarea-dropdown-div").hide();
        }
        else {
            $("#programmaticarea-dropdown-div").show();
            $("#programmaticarea-textarea-div").hide();
        }
    });

$("#resident-coursework-modal").on("click", "#btn-submit-deliverable", function (e) {
    var form = $('form#submit-resident-deliverable-form');
    var action = $(form).attr('action');
    var method = $(form).attr('method');
    e.preventDefault();

    var fileupload = $("#DeliverableFile").get(0);
    var files = fileupload.files;
    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append('DeliverableFile', files[i]);
    }

    // You can update the jquery selector to use a css class if you want
    $("input[type='text'").each(function (x, y) {
        data.append($(y).attr("name"), $(y).val());
    });

    $("select").each(function (x, y) {
        data.append($(y).attr("name"), $(y).val());
    });

    $("input[type='hidden'").each(function (x, y) {
        data.append($(y).attr("name"), $(y).val());
    });
    $("input[type='checkbox'").each(function (x, y) {
        data.append($(y).attr("name"), $(y).val());
    });

    $.ajax({
        type: method,
        url: action,
        contentType: false,
        processData: false,
        data: data
    }).done(function (res) {
        var checked = $("#ProgrammaticAreaFound").val();
        if (checked) {
            $("#programmaticarea-textarea-div").show();
            $("#programmaticarea-dropdown-div").hide();
        }
        else {
            $("#programmaticarea-dropdown-div").show();
            $("#programmaticarea-textarea-div").hide();
        }

        $("#resident-coursework-container").html(res);
        $('.selectpicker').selectpicker();
    }).fail(function (xhr, b, error) {
        alert(error);
    });

});

$('#resident-coursework-modal').on('show.bs.modal', function () {

    var deliverableId = $("#ResidentDeliverableId").val();

    $('#resident-deliverable-submissions-list').dataTable({
        "serverSide": true,
        "proccessing": true,
        "dom": 'rt<"bottom"i<"clear">>',
        "ajax": {
            "url": "/Deliverable/GetResidentDeliverableSubmissions",
            "type": "POST",
            "data": { deliverableId: deliverableId }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'title',
                data: "title",
                title: "Title",
                sortable: true,
                searchable: true
            },
            {
                name: 'programmaticArea',
                data: "programmaticArea",
                title: "Programmatic Area",
                sortable: true,
                searchable: true
            },
            {
                name: 'submissionType',
                data: 'submissionType',
                title: "Submission Type",
                sortable: true,
                searchable: true
            },
            {
                name: 'dateSubmitted',
                data: 'dateSubmitted',
                title: "Date Submitted",
                sortable: true,
                searchable: true
            },
            {
                name: 'CreatedBy',
                data: 'createdBy',
                title: "CreatedBy",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                "title": "Actions",
                "data": { "id": "id" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/Deliverable/DownloadSubmission?submissionId=' + data.id + '">Download</a>';
                }
            }
        ]
    });
});

$("#resident-examinationscores-list").on("click", ".view-resident-deliverable-score", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#resident-modules-container').html(data);
        $('#resident-modules-modal').modal('show');
    });
});

function getUnitTimetable(programOfferId, semesterId) {

  var ut = $('#unit-timetable-list').DataTable({
        "serverSide": true,
        "proccessing": true,
        "destroy": true,
        "ajax": {
            "url": "/Course/GetTimetableList",
            "type": "POST",
            "data": { Id: programOfferId, semesterId: semesterId, timetableType: 1 }

        },
        buttons: [
            {
                extend: 'pdf',
                text: '<span class ="glyphicon glyphicon-download-alt"></span> Download Timetable',
                exportOptions: {
                    columns: ':visible'
                }
            }],
      initComplete: function () {
            ut.buttons().container()
                .appendTo($('#unit-timetable-list_wrapper .col-sm-6:eq(0)'));
            let buttons = document.getElementsByClassName("dt-button");
            for (let button of buttons) {
                button.classList.remove("dt-button");
                button.classList.add("btn");
                button.classList.add("btn-default");
                button.classList.add("btn-md");
            }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'unit',
                data: 'unitName',
                title: "Unit",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'startDate',
                data: 'startDate',
                title: "Date",
                sortable: false,
                searchable: true,
                visible: true
            },
            {
                name: 'startTime',
                data: 'startTime',
                title: "Start Time",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'endTime',
                data: 'endTime',
                title: "End Time",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'duration',
                data: 'duration',
                title: "Duration",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'faculty',
                data: 'faculty',
                title: "Faculty",
                sortable: true,
                searchable: true,
                visible: true
            },
            {
                name: 'createdBy',
                data: 'createdBy',
                title: "Created By",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "DateCreated",
                sortable: true,
                searchable: false,
                visible: false
            }
        ]
    });
}

function getExamTimetable(programOfferId, semesterId) {

  var et = $('#examination-timetable-list').DataTable({
        "serverSide": true,
        "proccessing": true,
       "destroy": true,
        "ajax": {
            "url": "/Course/GetTimetableList",
            "type": "POST",
            "data": { Id: programOfferId, semesterId: semesterId, timetableType: 2 }

        },
        buttons: [
            {
                extend: 'pdf',
                text: '<span class ="glyphicon glyphicon-download-alt"></span> Download Timetable',
                exportOptions: {
                    columns: ':visible'
                }
            }],
      initComplete: function () {
            et.buttons().container()
                .appendTo($('#examination-timetable-list_wrapper .col-sm-6:eq(0)'));
            let buttons = document.getElementsByClassName("dt-button");
            for (let button of buttons) {
                button.classList.remove("dt-button");
                button.classList.add("btn");
                button.classList.add("btn-default");
                button.classList.add("btn-md");
            }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'unit',
                data: 'examinationName',
                title: "Unit",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'startDate',
                data: 'startDate',
                title: "Date",
                sortable: false,
                searchable: true,
                visible: true
            },
            {
                name: 'startTime',
                data: 'startTime',
                title: "Start Time",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'endTime',
                data: 'endTime',
                title: "End Time",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'duration',
                data: 'duration',
                title: "Duration",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'faculty',
                data: 'faculty',
                title: "Faculty",
                sortable: true,
                searchable: true,
                visible: true
            },
            {
                name: 'createdBy',
                data: 'createdBy',
                title: "Created By",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "DateCreated",
                sortable: true,
                searchable: false,
                visible: false
            }
        ]
  });

}



