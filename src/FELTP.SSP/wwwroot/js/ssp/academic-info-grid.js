﻿$(document).ready(function () {

    var personId = $("#PersonId").val();
    var loaderpath = $("#loader-input-path").val();
    var table = $('#academic_info_table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/Applicant/GetPersonAcademicInfo",
            "type": "POST",
            "data": { Id: personId }
        },
        columns: [
            {
                name: 'strEducationLevel',
                data: "strEducationLevel",
                title: "Education Level",
                sortable: true,
                searchable: true
            },
            {
                name: 'courseName',
                data: "courseName",
                title: "Course Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'university',
                data: "university",
                title: "University",
                sortable: true,
                searchable: true
            },
            {
                name: 'yearOfGraduation',
                data: 'yearOfGraduation',
                title: "Graduation Year",
                sortable: true,
                searchable: true
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Add New</b> button.",
            processing: loaderpath
        },
        processing: true


    });


    $('#btn-add-academic-info').on("click", function (event) {
        event.preventDefault();
        $("#academic-profile-spinner").show();
        var url = $(this).attr("data-url");
        $.get(url, function (data) {
            $('#add-academic-info-container').html(data);
            $(".selectpicker").selectpicker("refresh");
            $('#add-academic-info-modal').modal('show');
            $("#academic-profile-spinner").hide();
        });
    });

});