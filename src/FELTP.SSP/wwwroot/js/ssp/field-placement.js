$(document).ready(function () {
    var applicantId = $("#ApplicantId").val();
    var loaderpath = $("#loader-input-path").val();

    $('#resident-field-placement-sites-table').dataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/FieldPlacement/GetResidentFieldPlacementSites",
            "type": "POST",
            "data": { applicantId: applicantId }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'siteName',
                data: 'siteName',
                title: "Site Name",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'supervisor',
                data: 'supervisor',
                title: "Supervisor",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'startDate',
                data: 'startDate',
                title: "Start Date",
                sortable: true,
                searchable: true
            },
            {
                name: 'endDate',
                data: "endDate",
                title: "End Date",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": { "id": "id" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a class="review-site" href="/FieldPlacement/ReviewSite?id=' + data.id + '&siteName=' +data.siteName+'"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicons-new-window"></i> Review </button></a>';

                }
            }
        ],
        language: {
            processing: loaderpath
        },
        processing: true

    });
});

$('#resident-field-placement-sites-table').on("click", ".review-site", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#review-field-placement-container').html(data);
        $('#review-field-placement-modal').modal('show');
        $('.selectpicker').selectpicker();
    });
});


$('#review-field-placement-modal').on("click", "#btn-submit-review", function (event) {
    var form = $('form#review-site-form');
    var action = $(form).attr('action');
    event.preventDefault();
    $.post(action, $(form).serialize(), function (data) {
        $("#review-field-placement-container").html(data);
        $('.selectpicker').selectpicker();
    }, 'html');

});
