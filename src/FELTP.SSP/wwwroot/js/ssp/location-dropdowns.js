﻿$(document).ready(function () {

    var url = "/LookUp/GetCountryDropDown";
    $.getJSON(url, {}, function (data) {     
        var selectedValue;
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, comp) {
            items += "<option value='" + comp.value + "'>" + comp.text + "</option>";
            if (comp.text.includes("Kenya"))
                selectedValue = comp.value;
        });
        $("#Person_AddressInfo_CountryId").html(items).selectpicker("refresh");;
        $("#Person_AddressInfo_CountryId").val(selectedValue);
        $("#Person_AddressInfo_CountryId").change();
    });
});

$("#Person_AddressInfo_CountryId").change(function () {
    var countryId = $("#Person_AddressInfo_CountryId").val();
    var url = "/LookUp/GetCountyDropDown";

    $.getJSON(url, { countryId: countryId }, function (data) {
        var items = "<option value='0'> Select County </option>";
        $.each(data, function (i, county) {
            items += "<option value='" + county.value + "'>" + county.text + "</option>"
        });
        $("#Person_AddressInfo_CountyId").html(items).selectpicker("refresh");;
    });

})



$("#Person_AddressInfo_CountyId").change(function () {
    var countyId = $("#Person_AddressInfo_CountyId").val();

    var url = "/LookUp/GetSubCountyDropDown";
    $.getJSON(url, { countyId: countyId }, function (data) {
        var items = "<option value='0'> Select Sub-County</option>";
        $.each(data, function (i, county) {
            items += "<option value='" + county.value + "'>" + county.text + "</option>"
        });
        $("#Person_AddressInfo_SubCountyId").html(items).selectpicker("refresh");;
    });
})