﻿$(document).ready(function () {
    $.get('/ProgramOffer/_ViewProgramOffers', function (data) {
        $("#program-offers-div").html(data);
    }, 'html');

});

$(document).ready(function () {
    var programOfferId = $("#ProgramOfferId").val();
    $.get('/ProgramOffer/_ViewProgramOfferLocations', { offerId: programOfferId }, function (data) {
        $("#program-offer-locations-div").html(data);
    }, 'html');

});


$(document).ready(function () {
    var programOfferId = $("#ProgramOfferId").val();
    $.get('/ProgramOffer/_ViewProgramOfferRequirements', { Id: programOfferId }, function (data) {
        $("#program-offer-requirements-div").html(data);
    }, 'html');

});


$(document).ready(function () {
    var applicantId = $("#ApplicantId").val();
    if (applicantId != null) {
        if ($("#program-applications-div").length > 0) {
            $("#program-applications-div").show();
        }
    }
    else {
        $("#program-applications-div").hide();
    }
    var loaderpath = $("#loader-input-path").val();

    $('#program-offer-applications-table').dataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/ProgramOffer/GetApplicantProgramOfferApplications",
            "type": "POST",
            "data": { Id: applicantId }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'programOfferId',
                data: 'programOfferId',
                title: "programOfferId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'applicantId',
                data: 'applicantId',
                title: "applicantId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'program',
                data: 'program',
                title: "Program",
                sortable: true,
                searchable: true
            },
            {
                name: 'programofferName',
                data: "programOfferName",
                title: "Offer Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'status',
                data: "qualificationStatus",
                title: "Qualification Status",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'applicationStatus',
                data: "applicationStatus",
                title: "Application Status",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'approvedByCounty',
                data: "approvedByCounty",
                title: "County Approved",
                sortable: true,
                searchable: true
            },
            {
                name: 'dateApplied',
                data: "dateApplied",
                title: "Date Applied",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": { "id": "id" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a class="view-program-offer" href="/ProgramOffer/ApplicationDetails?id=' + data.id + '"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicons-new-window"></i> View Details </button></a>';

                }
            }
        ],
        language: {
            processing: loaderpath
        },
        processing: true

    });
});