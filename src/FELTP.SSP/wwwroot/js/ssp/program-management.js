﻿

function format_program(d) {
    return '<div class="slider" style="margin-left:60px">' +
        '<b>Code:</b> ' + d.code + '<br>' +
        '<b>Tier:</b> ' + d.programTier + '<br>' +
        '<b>Name: </b>' + d.name + '<br>' +
        '<b>Duration:</b> ' + d.duration + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}
$(document).ready(function () {
    var table = $('#program-list-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/programmanagement/ProgramList",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "width": "5%",
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'code',
                data: 'code',
                title: "Code",
                sortable: false,
                searchable: false
            },
            {
                name: 'programtier',
                data: 'programTier',
                title: "Tier",
                sortable: true,
                searchable: true
            },
            {
                name: 'name',
                data: "name",
                title: "Program",
                sortable: true,
                searchable: true
            },
            {
                name: 'description',
                data: "description",
                title: "Description",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'duration',
                data: 'duration',
                title: "Duration",
                sortable: true,
                searchable: true
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "DateCreated",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'createdby',
                data: 'createdBy',
                title: "CreatedBy",
                //sortable: true,
                searchable: false,
                visible: false
            },
            {
                "title": "Actions",
                "data": { "id": "id", "name": "name" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a class="view-details" href="/ProgramManagement/ProgramDetails?id=' + data.id + '"><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i> View Details</button></a> ';

                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });

    // Add event listener for opening and closing details
    $('#program-list-table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format_program(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });
});

$('#program-list-table').on("click", ".view-program-courses", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#program-courses-container').html(data);
        $('#program-courses-modal').modal('show');
    });
});
$(document).ready(function () {
    var programId = $("#ProgramId").val()
    table = $('#program-courses-list-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/programmanagement/GetProgramCourses",
            "type": "POST",
            "data": { programId: programId }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'programId',
                data: 'programId',
                title: "programId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'coursetype',
                data: 'category',
                title: "CourseType",
                sortable: true,
                searchable: true
            },
            {
                name: 'semester',
                data: "semester",
                title: "Semester",
                sortable: true,
                searchable: true
            },
            {
                name: 'coursename',
                data: "courseName",
                title: "Semester Title",
                sortable: true,
                searchable: true
            },
            {
                name: 'durationinweeks',
                data: 'durationInWeeks',
                title: "Duration (Weeks)",
                sortable: true,
                searchable: true
            },

        ]
    });

});


$(document).ready(function () {
    var programId = $("#ProgramId").val()
    var id = $("#id").val()
    var fileContent = $("#fileContent").val()
    table = $('#program-resource-list-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/programmanagement/GetProgramResourcesByProgramId",
            "type": "POST",
            "data": { programId: programId }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'programId',
                data: 'programId',
                title: "Program Id",
                sortable: true,
                visible: false
            },
            {
                name: 'resourcesType',
                data: 'resourcesType',
                title: 'Resources Type',
                sortable: false,
                searchable: true
            },
            {
                name: 'resourceTitle',
                data: 'resourceTitle',
                title: "Resource Title",
                sortable: false,
                searchable: true
            },
          
            {
                name: 'resourceDescription',
                data: 'resourceDescription',
                title: "Resource Description",
                visible: false
            },
      
        


            {
                "title": "Actions",
                "data": { "id": "id", "programId": "programId" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/ProgramManagement/GetProgramResourcesFiles?Id=' + data.id + '" class="ProgramCourseFiles" data-toggle="tooltip" title="Files"><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i>  View Files </button></a> ';

                }
            }
        ]
    });

});




$('#viewResourceFilesModal').on('show.bs.modal', function () {

    var resourceId = $("#resourceId").val();
    $('#resource-files-tables').dataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/ProgramManagement/ViewProgramResourcesFiles",
            "type": "POST",
            "data": { Id: resourceId }
        },
        columns: [

            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'resourceId',
                data: 'resourceId',
                title: "resource id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'fileName',
                data: 'fileName',
                title: "File Name",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'contentType',
                data: 'contentType',
                title: "Content Type",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'dateCreated',
                data: 'dateCreated',
                title: "Date Created",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'fileContent',
                data: 'fileContent',
                title: "File ",
                sortable: false,
                searchable: false,
                visible: false
            },

            {
                "title": "Actions",
                "data": { "id": "id" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/ProgramManagement/DownLoadResourceFile?Id=' + data.id + '" class="downLoadFile" data-toggle="tooltip" title="Download"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-download"></i> Download Resource File </button></a> ';
                }
            }
        ]
    });
});



function format_resource_type(d) {
    return '<div class="slider" style="margin-left:70px">' +
        '<b>Full name: </b>' + d.name + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}



function format_resource(d) {
    return '<div class="slider" style="margin-left:60px">' +
        '<b>Resource Type:</b> ' + d.resourceType + '<br>' +
        '<b>Resource Title:</b> ' + d.resourceTitle + '<br>' +
        '<b>Resource Description:</b> ' + d.resourceDescription + '<br>' +
        '<b>Source: </b>' + d.source + '<br>' +
        '<b>Source Contact Details:</b> ' + d.sourceContactDetails + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}

function format_program_tier(d) {
    return '<div class="slider" style="margin-left:60px">' +
        '<b>Full name: </b>' + d.name + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}

function format(d) {
    return '<div class="slider" style="margin-left:70px">' +
        '<b>Program: </b>' + d.program + '<br>' +
        '<b>Program offer: </b>' + d.programOffer + '<br>' +
        '<b>Course: </b>' + d.programCourse + '<br>' +
        '<b>Satrt Date: </b>' + d.startDate + '<br>' +
        '<b>End Date: </b>' + d.endDate + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}


