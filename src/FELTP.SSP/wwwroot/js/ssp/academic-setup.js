﻿$(document).ready(function () {
    $('#AcademicInfo_UniversityNotFound').on("change", function () {
        toggle_university_div(this.checked)
    });

    $('#AcademicInfo_CourseNotFound').on("change", function () {
        toggle_course_div(this.checked)
    });

    toggle_hidden_academic_elements();
   
});

function toggle_hidden_academic_elements()
{
    if ($("#AcademicInfo_CourseNotFound").length > 0) {
        var course_notfound = document.getElementById("AcademicInfo_CourseNotFound").checked;
        toggle_course_div(course_notfound);
    }

    if ($("#AcademicInfo_CourseNotFound").length > 0) {
        var universityNotFound = document.getElementById("AcademicInfo_CourseNotFound").checked;
        toggle_university_div(universityNotFound);
    }

}

$("#AcademicInfo_UniversityCourseId").change(function () {
    var educationLevel = $("#AcademicInfo_UniversityCourseId option:selected").parent().attr("label");
    $("#AcademicInfo_StrEducationLevel").val(educationLevel);
});


function toggle_university_div(checked)
{
    if (checked) {
        $(".university-textarea-div").show();
        $(".university-dropdown-div").hide();
    }
    else {
        $(".university-dropdown-div").show();
        $(".university-textarea-div").hide();
    }
}

function toggle_course_div(checked)
{
    if (checked) {
        $(".course-textarea-div").show();
        $(".academic-course-dropdown-div").hide();
    }
    else {
        $(".academic-course-dropdown-div").show();
        $(".course-textarea-div").hide();
    }
}

$("#add-academic-info-container").on("change", "#AcademicInfo_UniversityCourseId", function () {
    var educationLevel = $("#AcademicInfo_UniversityCourseId option:selected").parent().attr("label");
    $("#AcademicInfo_StrEducationLevel").val(educationLevel);
});


$("#add-academic-info-modal").on("click", "#add-academic-info-btn", function (event) {
    $("#add-academic-info-spinner").show();
    var form = $('form#academic-info-form');
    var action = $(form).attr('action');
    event.preventDefault();
    $.post(action, $(form).serialize(), function (data) {
        $("#add-academic-info-spinner").hide();
        $("#add-academic-info-container").html(data);
        $(".selectpicker").selectpicker("refresh");
        toggle_hidden_academic_elements();
    }, 'html');
});


$('#add-academic-info-modal').on("change","#AcademicInfo_UniversityNotFound", function () {
    toggle_university_div(this.checked)
});

$('#add-academic-info-modal').on("change", "#AcademicInfo_CourseNotFound",function () {
    toggle_course_div(this.checked)
});

