﻿$(document).ready(function () {
    var resourceId = $("#resourceId").val();
    $('#resource-files-tables').dataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/ProgramManagement/ViewProgramResourcesFiles",
            "type": "POST",
            "data": { Id: resourceId }
        },
        columns: [
         
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'resourceId',
                data: 'resourceId',
                title: "resource id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'fileName',
                data: 'fileName',
                title: "File Name",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'contentType',
                data: 'contentType',
                title: "Content Type",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'dateCreated',
                data: 'dateCreated',
                title: "Date Created",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'fileContent',
                data: 'fileContent',
                title: "File ",
                sortable: false,
                searchable: false,
                visible: false
            },
          
            {
                "title": "Actions",
                "data": { "id": "id"},
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/ProgramManagement/DownLoadResourceFile?Id=' + data.id + '" class="downLoadFile" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i> Download Resource File </button></a> ';
                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });

});



function format_course_type(d) {
    return '<div class="slider" style="margin-left:60px">' +
        '<b>Full name: </b>' + d.name + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}


function format(d) {
    return '<div class="slider" style="margin-left:70px">' +
        '<b>Full name: </b>' + d.name + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}





