﻿using Application.ProgramManagement.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace FELTP.SSP.Models
{
    public class PagedProgramOfferViewModel
    {
        public IPagedList<ProgramOfferViewModel> ProgramOffers { get; set; }

    }
}
