﻿using Application.ProgramManagement.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace FELTP.SSP.Models
{
    public class PagedProgramViewModel
    {
        public IEnumerable<ProgramViewModel> ProgramList { get; set; }
    }
}
