﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Common.Models;
using Application.Common.Models.Queries;
using Application.ResidentManagement.Commands;
using Application.ResidentManagement.Queries;
using Application.ResidentManagement.QueryHandlers;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using static Application.Common.Services.ApiErrorModelBuilder;


namespace ResidentManagementAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [Authorize]
    public class ApplicantManagementController : Controller
    {
        IMediator mediator;
        public ApplicantManagementController(IMediator _mediator)
        {
            mediator = _mediator;
        }

        [HttpGet("{Id}")]
        public async Task<object> GetApplicantById(string Id)
        {
            if (string.IsNullOrEmpty(Id))
                return NoContent();

            var applicant = await mediator.Send(new GetApplicantQuery { Id = Convert.ToInt64(Id) });

            return Ok(applicant);
        }

        public async Task<object> GetApplicantByIdentificationNumber(string idNumber)
        {
            if (string.IsNullOrEmpty(idNumber))
                return NoContent();

            var applicant = await mediator.Send(new GetApplicantByIdNumber { IdentificationNumber = idNumber });

            return Ok(applicant);
        }

        [HttpGet("{Id}")]
        public async Task<object> GetApplicantCourseInformation(string Id)
        {
            if (string.IsNullOrEmpty(Id))
                return NoContent();

            var courseInformation = await mediator.Send(new GetApplicantCourseInformation { ApplicantId = Convert.ToInt64(Id) });

            return Ok(courseInformation);
        }

        [HttpGet]
        public async Task<object> GetResidentCourseWork(string Id, string semesterId)
        {
            if (string.IsNullOrEmpty(Id))
                return NoContent();

            var residentCourseWork = await mediator.Send(new GetResidentCourseWork
            {
                EnrollmentId = Convert.ToInt32(Id),
                SemesterId = string.IsNullOrEmpty(semesterId) ? null : (int?)Convert.ToInt32(semesterId)
            });

            return Ok(residentCourseWork);

        }

        [HttpGet]
        public async Task<object> GetResidentFieldPlacementActivities(string Id, string semesterId)
        {
            if (string.IsNullOrEmpty(Id))
                return NoContent();

            var residentActivities = await mediator.Send(new GetResidentFieldPlacementActivities
            {
                ProgramEnrollmentId = Convert.ToInt32(Id),
                SemesterId = string.IsNullOrEmpty(semesterId) ? null : (int?)Convert.ToInt32(semesterId)
            });

            return Ok(residentActivities);
        }

        [HttpGet]
        public async Task<object> GetResidentDeliverables(string Id, string semesterId)
        {
            if (string.IsNullOrEmpty(Id))
                return NoContent();

            var residentDeliverables = await mediator.Send(new GetResidentDeliverables
            {
                ProgramEnrollmentId = Convert.ToInt32(Id),
                SemesterId = string.IsNullOrEmpty(semesterId) ? null : (int?)Convert.ToInt32(semesterId)
            });

            return Ok(residentDeliverables);
        }

        [HttpGet("{Id}")]
        public async Task<object> GetResidentDeliverablesByActivityId(string Id)
        {
            if (string.IsNullOrEmpty(Id))
                return NoContent();

            var deliverables = await mediator.Send(new GetResidentDeliverables { ResidentFieldPlacementActivityId = Convert.ToInt64(Id) });

            return Ok(deliverables);
        }

        [HttpGet]
        public async Task<object> GetResidentExaminationScores(string Id, string semesterId)
        {
            if (string.IsNullOrEmpty(Id))
                return NoContent();

            var examinationScores = await mediator.Send(new GetResidentExaminationScore
            {
                EnrollmentId = Convert.ToInt32(Id),
                SemesterId = string.IsNullOrEmpty(semesterId) ? null : (int?)Convert.ToInt32(semesterId)
            });

            return Ok(examinationScores);
        }

        [HttpGet]
        public async Task<object> GetResidentUnits(string courseworkId)
        {
            if (string.IsNullOrEmpty(courseworkId))
                return NoContent();

            var residentUnits = await mediator.Send(new GetResidentUnits { ResidentCourseWorkId = Convert.ToInt64(courseworkId) });

            return Ok(residentUnits);

        }

        [HttpPost]
        public async Task<object> SubmitResidentDeliverable([FromBody]AddResidentDeliverableItemCommand model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Validation failed when submitting resident deliverable",
                    null, false, BuildModelFromModelStateErrors(ModelState.Values.SelectMany(x => x.Errors)));

            var response = await mediator.Send(model);

            return new ApiResponse(response.Message, JsonConvert.SerializeObject(response), response.IsSuccessful);

        }

        [HttpPost]
        public async Task<object> SubmitDeliverableFile()
        {
            var form = HttpContext.Request.Form;
            var deliverableItemExists = form.TryGetValue("ResidentDeliverableItemId", out StringValues formValues);

            if (form.Files.Count == 0 && !deliverableItemExists)
                return Ok(new ApiResponse("An error occured while processing statement file upload", null, false));

            await mediator.Send(new AddDeliverableItemFileRequest
            {
                DeliverableItemId = Convert.ToInt64(formValues[0]),
                DeliverableFile = form.Files[0]
            });

            return Ok(new ApiResponse("Delivarable file processed succesfully",
                JsonConvert.SerializeObject(new { Completed = true, DeliverableItemId = formValues[0] }), true));
        }

        [HttpGet("{Id}")]
        public async Task<object> GetResidentDeliverableSubmissions(long Id)
        {
            var submissions = await mediator.Send(new GetResidentDeliverableSubmissions()
            {
                ResidentDeliverableId = Id
            });
            return Ok(submissions);
        }

        [HttpGet("{Id}")]
        public async Task<object> GetDeliverableSubmission(long Id)
        {
            var submission = await mediator.Send(new GetDeliverableSubmission() { SubmissionId = Id });
            return Ok(submission);
        }


    }
}