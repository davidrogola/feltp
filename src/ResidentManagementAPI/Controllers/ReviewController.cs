using System.Threading;
using System.Threading.Tasks;
using Application.ProgramManagement.Commands;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProgramManagement.Domain.FieldPlacement;

namespace ResidentManagementAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [Authorize]
    public class ReviewController : Controller
    {
        private readonly IMediator _mediator;

        public ReviewController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<object> EvaluateFieldPlacementSite([FromBody]AddFieldPlacementEvaluationCommand command)
        {
            var response = await _mediator.Send(command, CancellationToken.None);
            if (response.Status)
                return Ok(response);
            return BadRequest(response);
        }
        
        
        
    }
}