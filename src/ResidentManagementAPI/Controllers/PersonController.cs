﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Common.Models;
using Application.Common.Models.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using static Application.Common.Services.ApiErrorModelBuilder;


namespace ResidentManagementAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [Authorize]
    public class PersonController : Controller
    {
        IMediator mediator;
        public PersonController(IMediator _mediator)
        {
            mediator = _mediator;
        }

        [HttpPost]
        public async Task<object> AddPerson([FromBody]AddPersonCommand model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Validation failed when creating person details", null, false, BuildModelFromModelStateErrors(ModelState.Values.SelectMany(x => x.Errors)));

            var response = await mediator.Send(model);

            return new ApiResponse("Person created succesfully", JsonConvert.SerializeObject(new { PersonId = response }), true);

        }

        [HttpPost]
        public async Task<object> AddPersonAcademicInfo([FromBody] AddPersonAcademicInfoCommand model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Validation failed when adding person academic details", null, false, BuildModelFromModelStateErrors(ModelState.Values.SelectMany(x => x.Errors)));

            var response = await mediator.Send(model);

            return new ApiResponse("Person academic details added succesfully", JsonConvert.SerializeObject(new { PersonId = response }), true);
        }

        [HttpPost]
        public async Task<object> AddPersonEmploymentHistory([FromBody]AddPersonEmploymentInfoCommand model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Validation failed when adding person employment details", null, false, BuildModelFromModelStateErrors(ModelState.Values.SelectMany(x => x.Errors)));

            var response = await mediator.Send(model);

            return new ApiResponse("Person employment details added succesfully", JsonConvert.SerializeObject(new { PersonId = response }), true);
        }

        [HttpGet]
        public async Task<object> GetPersonAcademicInfo(long Id)
        {
            var academicInfo = await mediator.Send(new GetPersonAcademicDetail { PersonId = Id });

            return Ok(academicInfo);
        }

        [HttpGet]
        public async Task<object> GetPersonByIdentificationNumber(string idNumber)
        {
            if (String.IsNullOrEmpty(idNumber))
                return NoContent();

            var person = await mediator.Send(new GetPersonQuery { IdNumber = idNumber });

            return Ok(person);
        }

        [HttpGet]
        public async Task<object> GetPersonById(long Id)
        {
            if (Id == default(long))
                return NoContent();

            var person = await mediator.Send(new GetPersonQuery { PersonId = Id });

            return Ok(person);
        }

        [HttpGet]
        public async Task<object> GetPersonEmploymentInfo(long Id)
        {
            if (Id == default(long))
                return NoContent();

            var employmentHistory = await mediator.Send(new GetPersonEmploymentHistory { PersonId = Id });

            return Ok(employmentHistory);
        }

    }
}