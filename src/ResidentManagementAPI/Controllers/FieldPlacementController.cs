using System;
using System.Threading;
using System.Threading.Tasks;
using Application.ResidentManagement.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ResidentManagementAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [Authorize]
    public class FieldPlacementController : Controller
    {
        private readonly IMediator _mediator;
        public FieldPlacementController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("{Id}")]
        public async Task<object> GetResidentFieldPlacementSites(long Id)
        {
            Console.WriteLine("ApplicantId "+Id);
            var fieldPlacementSites = await _mediator.Send(new GetResidentFieldPlacementSites {ApplicantId = Id},
                CancellationToken.None);
            
            return Ok(fieldPlacementSites);
        }
    }
}