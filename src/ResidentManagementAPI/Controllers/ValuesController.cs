﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UserManagement.Domain;
using UserManagement.ViewModels;

namespace ResidentManagementAPI.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
       
        [HttpGet]
        public string Get()
        {
            return "Resident Management API is running and receiving user requests";
        }
    }
}
