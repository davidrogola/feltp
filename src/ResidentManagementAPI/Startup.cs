﻿using Application.Common;
using Application.Common.Interface;
using Application.Common.Services;
using Application.Common.Validators;
using Application.MapperProfiles.ResidentManagement;
using Application.ResidentManagement.Queries;
using AutoMapper;
using FELTP.Infrastructure;
using FluentValidation.AspNetCore;
using IdentityServer4.AccessTokenValidation;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MySql.Data.MySqlClient;
using System;
using UserManagement.Domain;
using static Application.Common.Services.IdentityConfig;

namespace ResidentManagementAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(Configuration);

            services.AddFeltpPersistanceStores(Configuration);
            services.AddUnitOfWorkImplementations();
            services.AddScoped<IPersonService, PersonService>();

            var connectionString = Configuration.GetConnectionString("feltp");
            services.AddSingleton<Func<MySqlConnection>>(() => new MySqlConnection(connectionString));

            services.AddMvcCore(options => {

                options.Filters.Add(typeof(WebApiCustomExceptionFilter));
            })
            .AddFluentValidation(x=>x.RegisterValidatorsFromAssemblyContaining<AddPersonCommandValidator>())
            .AddAuthorization()
            .AddJsonFormatters();

            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .AddIdentityServerAuthentication(options =>
                {
                    options.ApiName = "ResidentManagementAPI";
                    options.Authority = GetIdentityServerEndPoint(Configuration);
                    options.RequireHttpsMetadata = false;
                    options.ApiSecret = "secret";
                });
            services.AddAutoMapper(typeof(ResidentCourseWorkProfile).Assembly);
            services.AddMediatR(typeof(SearchApplicantQuery).Assembly);
            services.AddEmailSender();
            services.AddUserNotifierService();
            SerilogConfiguration.BootstrapLogger(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
