﻿using System;
using MediatR;
using AutoMapper;
using DataTables.AspNet.AspNetCore;
using FluentValidation.AspNetCore;
using MySql.Data.MySqlClient;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;

using UserManagement.Services;
using Application.Common.ActivityMonitor.Installers;
using Application.MapperProfiles.ProgramManagement;
using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Validators;
using Application.Common.SequenceGenerator;
using Application.ResidentManagement.Services;
using UserManagement.Domain;
using Application.Common.Interface;
using Application.Common.Services;
using FELTP.Web.Services;


using Hangfire;
using Hangfire.MySql.Core;
using System.Data;
using Application.Common.ActivityMonitor;
using Application.ProgramManagement.Services;
using Application.Common;
using Microsoft.AspNetCore.Authentication;
using Microsoft.IdentityModel.Tokens;
using IdentityModel;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Security.Claims;
using Application.Messaging;
using FELTP.Infrastructure.Task;

namespace FELTP.Web
{
    public class Startup
    {
        public Startup(IHostingEnvironment hostingEnvironment)
        {
            var builder = new ConfigurationBuilder()
                               .SetBasePath(hostingEnvironment.ContentRootPath)
                               .AddJsonFile("appsettings.json")
                               .AddEnvironmentVariables();
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(Configuration);

            services.AddFeltpPersistanceStores(Configuration);
            services.AddUnitOfWorkImplementations();
            services.AddScoped<IPersonService, PersonService>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            var connectionString = Configuration.GetConnectionString("feltp");
            services.AddSingleton<Func<MySqlConnection>>(() => new MySqlConnection(connectionString));
        
            services.AddIdentityServices(Configuration);
            services.AddUserAuthenticationService();

            services.AddMvc(config=> 
            {
                config.Filters.Add(typeof(GlobalExceptionLogger));
            })
            .AddFluentValidation(fv =>
             {
                 fv.RegisterValidatorsFromAssemblyContaining<AddProgramCommandValidator>();
                 fv.ConfigureClientsideValidation();
             }).AddJsonOptions(options => options.SerializerSettings.ContractResolver = 
             new DefaultContractResolver());

            //add hilo sequence generator services
            services.AddServerHiloSequenceGenerator();
            services.AddSequenceCodeGenerator();
            services.AddEligiltyGeneratorServices();
            
//             services.AddHangfire(x=>x.UseStorage(
//                 new MySqlStorage(Configuration.GetConnectionString("hangfire"),
//                  new MySqlStorageOptions
//                  {
//                      TransactionIsolationLevel = IsolationLevel.ReadCommitted,
//                        QueuePollInterval = TimeSpan.FromSeconds(15),
//                        JobExpirationCheckInterval = TimeSpan.FromHours(1),
//                        CountersAggregateInterval = TimeSpan.FromMinutes(5),
//                        PrepareSchemaIfNecessary = true,
//                        DashboardJobListLimit = 50000,
//                        TransactionTimeout = TimeSpan.FromMinutes(1)
//                   })));

            services.AddMediatR(typeof(AddProgramCommand).Assembly);
            services.AddAutoMapper(typeof(ProgramProfile).Assembly);
  
            services.RegisterDataTables();
            services.AddAntiforgery(x => x.SuppressXFrameOptionsHeader = true);
            services.AddSelectListLoaders();
            services.AddGraduandEligibiltyService();
            services.AddGraduandListGeneratorService();
            services.AddProgramActivityMonitors();
            services.AddResidentCourseProgressUpdator();
            services.AddScoped<IProgamActivityMonitoringServiceLoader, ProgramActivityMonitoringServicesLoader>();
            services.AddScoped<ExamScoreConsolidationService>();
            services.AddResidentCourseGeneratorService();
            SerilogConfiguration.BootstrapLogger(Configuration);
            services.AddRolePermissionMapDictionary();
            services.AddEmailSender();
            services.AddUserNotifierService();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,IServiceProvider serviceProvider, ApplicationDbContext dbContext)
        {
            //dbContext.Database.EnsureCreated();
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            
       
            app.UseStaticFiles();

            app.UseAuthentication();
            
            //app.UseHangfireServer();

//            app.UseHangfireDashboard("/hangfire", new DashboardOptions
//            {
//                Authorization = new[] { new HangfireDashboardAuthorizationFilter() }
//            });
            
            //ConfigureCronJobs(serviceProvider);


            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

        }

        public void ConfigureCronJobs(IServiceProvider serviceProvider)
        {
            var configuration = serviceProvider.GetService<IConfiguration>();

            var cronExpression = configuration["CronJobExecutionTimeline"];

            RecurringJob.AddOrUpdate(() => serviceProvider.GetRequiredService<IProgamActivityMonitoringServiceLoader>().Execute(),cronExpression);

            RecurringJob.AddOrUpdate(() => serviceProvider.GetRequiredService<IResidentCourseworkGenerator>().GenerateResidentCoursework(), cronExpression);
        }

    }
}
