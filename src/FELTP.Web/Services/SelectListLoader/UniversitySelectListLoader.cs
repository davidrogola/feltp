﻿using Application.LookUp.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FELTP.Web.Services
{
    public class UniversitySelectListLoader : ISelectListLoader
    {
        IMediator mediator;

        public UniversitySelectListLoader(IMediator mediator)
        {
            this.mediator = mediator;
        }
        public bool CanLoad(SelectItemType type)
        {
            bool canLoad = type == SelectItemType.University ? true : false;
            return canLoad;
        }

        public async Task<SelectList> GetSelectListAsync(object Id, string selected = null)
        {
            if (!CanLoad(SelectItemType.University))
                throw new ArgumentException("Select list loader not found");
            var universities = await mediator.Send(new GetUniversities() { Id = (int?)Id });

            var selectList = new SelectList(universities.OrderBy(x => x.Id), "Id", "Name", selected, "University");

            return selectList;
        }

        public async Task<List<SelectListItem>> GetSelectListItemsAsync(object Id)
        {
            if (!CanLoad(SelectItemType.University))
                throw new ArgumentException("Select list loader not found");

            var universityList = await mediator.Send(new GetUniversities() { Id = (int?)Id });

            var selectListItems = universityList.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return selectListItems;
        }
    }
}
