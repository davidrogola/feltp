﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application;
using Application.FacultyManagement.Queries;
using Application.ProgramManagement.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FELTP.Web.Services
{
    public class FacultySelectListLoader : ISelectListLoader
    {
        IMediator mediator;

        public FacultySelectListLoader(IMediator mediator)
        {
            this.mediator = mediator;
        }
        public bool CanLoad(SelectItemType type)
        {
            bool canLoad = type == SelectItemType.Faculty ? true : false;
            return canLoad;
        }

        public async Task<SelectList> GetSelectListAsync(object Id, string selected = null)
        {
            if (!CanLoad(SelectItemType.Faculty))
                throw new ArgumentException("Select list loader not found");
            var faculties = await mediator.Send(new GetFacultyListQuery() { });

            var selectList = new SelectList(faculties.Where(x => x.Status == Status.Active.ToString())
                .OrderBy(x => x.Id), "Id", "Name", selected, "Faculty");
            return selectList;
        }

        public async Task<List<SelectListItem>> GetSelectListItemsAsync(object Id)
        {
            if (!CanLoad(SelectItemType.Faculty))
                throw new ArgumentException("Select list loader not found");

            var facultyList = await mediator.Send(new GetFacultyListQuery() { });

            var selectListItems = facultyList.Where(x => x.Status == Status.Active.ToString())
                .Select(x => new SelectListItem
                {
                    Text = $"{ x.Person.BioDataInfo.FirstName} {x.Person.BioDataInfo.LastName}",
                    Value = x.Id.ToString()
                }).ToList();

            return selectListItems;
        }
    }
}
