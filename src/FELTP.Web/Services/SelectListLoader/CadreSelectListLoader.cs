﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.FacultyManagement.Queries;
using Application.ProgramManagement.Queries;
using Application.ResidentManagement.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FELTP.Web.Services
{
    public class CadreSelectListLoader : ISelectListLoader
    {
        IMediator mediator;

        public CadreSelectListLoader(IMediator mediator)
        {
            this.mediator = mediator;
        }
        public bool CanLoad(SelectItemType type)
        {
            bool canLoad = type == SelectItemType.Cadre ? true : false;
            return canLoad;
        }

        public async Task<SelectList> GetSelectListAsync(object Id,string selected = null)
        {
            if (!CanLoad(SelectItemType.Cadre))
                throw new ArgumentException("Select list loader not found");
            var cadres = await mediator.Send(new GetCadreList() { });

            var selectList = new SelectList(cadres.OrderBy(x => x.Id), "Id", "Name", selected, "Cadre");

            return selectList;
        }

        public async Task<List<SelectListItem>> GetSelectListItemsAsync(object Id)
        {
            if (!CanLoad(SelectItemType.Cadre))
                throw new ArgumentException("Select list loader not found");

            var cadreList = await mediator.Send(new GetCadreList() { });

            var selectListItems = cadreList.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return selectListItems;
        }
    }
}
