﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.ProgramManagement.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FELTP.Web.Services
{
    public class CategorySelectListLoader : ISelectListLoader
    {
         IMediator mediator;

        public CategorySelectListLoader(IMediator mediator)
        {
            this.mediator = mediator;
        }
        public  bool CanLoad(SelectItemType type)
        {
            bool canLoad = type == SelectItemType.Category ? true : false;
            return canLoad;
        }

        public  async Task<SelectList> GetSelectListAsync(object Id, string selected = null)
        {
            if (!CanLoad(SelectItemType.Category))
                throw new ArgumentException("Select list loader not found");
            var categories = await mediator.Send(new GetCategoryList() { Id = (int ?) Id });

            var selectList = new SelectList(categories.OrderBy(x => x.CourseTypeId), "Id", "Name", selected, "CourseType");

            return selectList;
        }

        public  async Task<List<SelectListItem>> GetSelectListItemsAsync(object Id)
        {
            if (!CanLoad(SelectItemType.Program))
                throw new ArgumentException("Select list loader not found");

            var categoryList = await mediator.Send(new GetCategoryList() { Id = (int?)Id });

            var selectListItems = categoryList.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return selectListItems;
        }
    }
}
