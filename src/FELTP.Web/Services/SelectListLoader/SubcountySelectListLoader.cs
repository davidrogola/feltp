﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.LookUp.Queries;
using Application.ProgramManagement.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FELTP.Web.Services
{
    public class SubcountySelectListLoader : ISelectListLoader
    {
        IMediator mediator;

        public SubcountySelectListLoader(IMediator mediator)
        {
            this.mediator = mediator;
        }
        public bool CanLoad(SelectItemType type)
        {
            bool canLoad = type == SelectItemType.Subcounty ? true : false;
            return canLoad;
        }

        public async Task<SelectList> GetSelectListAsync(object Id, string selected = null)
        {
            if (!CanLoad(SelectItemType.Subcounty))
                throw new ArgumentException("Select list loader not found");
            var Subcounties = await mediator.Send(new GetSubCounties() { CountyId = (int?)Id });

            var selectList = new SelectList(Subcounties, "Id", "Name", selected, "County");

            return selectList;
        }

        public async Task<List<SelectListItem>> GetSelectListItemsAsync(object Id)
        {
            if (!CanLoad(SelectItemType.Subcounty))
                throw new ArgumentException("Select list loader not found");

            var SubcountyList = await mediator.Send(new GetSubCounties() { CountyId = (int?)Id });

            var selectListItems = SubcountyList.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return selectListItems;
        }
    }
}
