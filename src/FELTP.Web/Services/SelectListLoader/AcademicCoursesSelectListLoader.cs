﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.ProgramManagement.QueryHandlers;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FELTP.Web.Services.SelectListLoader
{
    public class AcademicCoursesSelectListLoader : ISelectListLoader
    {
        IMediator mediator;
        public AcademicCoursesSelectListLoader(IMediator _mediator)
        {
            mediator = _mediator;
        }
        public bool CanLoad(SelectItemType type)
        {
            return type == SelectItemType.AcademicCourse;
        }

        public async Task<SelectList> GetSelectListAsync(object Id, string selected = null)
        {
            var academicCourseGrading = await mediator.Send(new GetAcademicCoursesQuery { });

            var academicCourseGradingModel = academicCourseGrading.Select(x => new
            {
                x.Id,
                x.CourseName,
                x.EducationLevel,
                EducationLevelToCourseMapping = $"{x.EducationLevel} : {x.Id}"
            }).ToList();

            return new SelectList(academicCourseGradingModel, "EducationLevelToCourseMapping", "CourseName",null,"EducationLevel");
        }

        public async Task<List<SelectListItem>> GetSelectListItemsAsync(object Id)
        {
            var academicCourseGrading = await mediator.Send(new GetAcademicCoursesQuery { });

            var selectListItem = academicCourseGrading.Select(x => new SelectListItem
            {
                Text = x.CourseName,
                Value = x.Id.ToString()
            }).ToList();

            return selectListItem;
        }
    }
}
