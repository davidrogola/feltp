﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.ProgramManagement.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FELTP.Web.Services.SelectListLoader
{
    public class FieldPlacementSelectListLoader : ISelectListLoader
    {
        IMediator mediator;
        public FieldPlacementSelectListLoader(IMediator _mediator)
        {
            mediator = _mediator;
        }
        public bool CanLoad(SelectItemType type)
        {
            return type == SelectItemType.FieldPlacementActivity;
        }

        public async Task<SelectList> GetSelectListAsync(object Id, string selected = null)
        {
            if (!CanLoad(SelectItemType.FieldPlacementActivity))
                throw new ArgumentException("Select list loader not found");
            var fieldPlacementActivities = await mediator.Send(new GetFieldPlacementActivityList());

            var selectList = new SelectList(fieldPlacementActivities, "Id", "Name", selected,null);
            return selectList;

        }

        public async Task<List<SelectListItem>> GetSelectListItemsAsync(object Id)
        {
            if (!CanLoad(SelectItemType.FieldPlacementActivity))
                throw new ArgumentException("Select list loader not found");

            var countyList = await mediator.Send(new GetFieldPlacementActivityList());

            var selectListItems = countyList.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return selectListItems;
        }
    }
}
