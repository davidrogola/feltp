﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.LookUp.Queries;
using Application.ProgramManagement.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FELTP.Web.Services
{
    public class CountySelectListLoader : ISelectListLoader
    {
        IMediator mediator;

        public CountySelectListLoader(IMediator mediator)
        {
            this.mediator = mediator;
        }
        public bool CanLoad(SelectItemType type)
        {
            bool canLoad = type == SelectItemType.County ? true : false;
            return canLoad;
        }

        public async Task<SelectList> GetSelectListAsync(object Id, string selected = null)
        {
            if (!CanLoad(SelectItemType.County))
                throw new ArgumentException("Select list loader not found");
            var counties = await mediator.Send(new GetCounties() { CountryId = (int?)Id });

            var selectList = new SelectList(counties, "Id", "Name", selected, "Country");

            return selectList;
        }

        public async Task<List<SelectListItem>> GetSelectListItemsAsync(object Id)
        {
            if (!CanLoad(SelectItemType.County))
                throw new ArgumentException("Select list loader not found");

            var countyList = await mediator.Send(new GetCounties() { CountryId = (int?)Id });

            var selectListItems = countyList.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return selectListItems;
        }
    }
}
