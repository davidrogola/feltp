﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FELTP.Web.Services
{
    public static class SelectListLoaderInstaller
    {
        public static void AddSelectListLoaders(this IServiceCollection service)
        {
            var type = typeof(ISelectListLoader);
            var selectListLoaders = Assembly.GetAssembly(typeof(ISelectListLoader)).GetTypes()
                .Where(x => type.IsAssignableFrom(x) && !x.IsInterface).ToList();

            foreach (var loader in selectListLoaders)
            {
                service.AddScoped(type, loader);
            }
        }
    }
}
