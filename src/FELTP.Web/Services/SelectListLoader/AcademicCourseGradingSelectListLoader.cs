﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.ProgramManagement.QueryHandlers;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FELTP.Web.Services.SelectListLoader
{
    public class AcademicCourseGradingSelectListLoader : ISelectListLoader
    {
        IMediator mediator;
        public AcademicCourseGradingSelectListLoader(IMediator _mediator)
        {
            mediator = _mediator;
        }
        public bool CanLoad(SelectItemType type)
        {
            return type == SelectItemType.AcademicCourseGrading;
        }

        public async Task<SelectList> GetSelectListAsync(object Id, string selected = null)
        {
            var courseGradings = await mediator.Send(new GetAcademicCourseGradingQuery { });

            return new SelectList(courseGradings.Select(x=>new
            {
                x.Id,
                x.EducationLevel,
                x.GradeName,
                EducationLevelToGradeMapping = $"{x.EducationLevel} : {x.Id}"
            }).ToList(), "EducationLevelToGradeMapping", "GradeName", null, "EducationLevel");
        }

        public async Task<List<SelectListItem>> GetSelectListItemsAsync(object Id)
        {
            var courseGradings = await mediator.Send(new GetAcademicCourseGradingQuery { });

            return courseGradings.Select(x => new SelectListItem
            {
                Text = x.GradeName,
                Value = x.Id.ToString()
            }).ToList();
        }
    }
}
