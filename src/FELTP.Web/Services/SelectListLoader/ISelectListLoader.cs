﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FELTP.Web.Services
{
    public enum SelectItemType
    {
        ProgramTier,
        Program,
        Category,
        CourseType,
        ResourceType,
        Resource,
        Country,
        County,
        Subcounty,
        Role,
        University,
        Ministry,
        Cadre,
        Faculty,
        Deliverable,
        ProgramLocation,
        FieldPlacementActivity,
        AcademicCourse,
        AcademicCourseGrading
    }

    public interface ISelectListLoader
    {
        bool CanLoad(SelectItemType type);
        Task<SelectList> GetSelectListAsync(object Id, string selected = null);
        Task<List<SelectListItem>> GetSelectListItemsAsync(object Id);
    }

}
