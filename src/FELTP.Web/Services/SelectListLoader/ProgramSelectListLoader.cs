﻿using Application.ProgramManagement.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FELTP.Web.Services
{
    public class ProgramSelectListLoader : ISelectListLoader
    {
        private readonly IMediator mediator;

        public ProgramSelectListLoader(IMediator mediator)
        {
            this.mediator = mediator;
        }

        public  bool CanLoad(SelectItemType type)
        {
            bool canLoad = type == SelectItemType.Program ? true : false;
            return canLoad;
        }

        public  async Task<SelectList> GetSelectListAsync(object Id,string selected=null)
        {
            if (!CanLoad(SelectItemType.Program))
                throw new ArgumentException("Select list loader not found");

            var programs = await mediator.Send(new GetProgramList() { ProgramTierId = (int ?) Id });

            var selectList = new SelectList(programs.OrderBy(x => x.ProgramTierId), "Id", "Name", selected, "ProgramTier");

            return selectList;
        }   

        public  async Task<List<SelectListItem>> GetSelectListItemsAsync(object Id)
        {
            if (!CanLoad(SelectItemType.Program))
                throw new ArgumentException("Select list loader not found");

            var programs = await mediator.Send(new GetProgramList() { ProgramTierId = (int?)Id });

            var selectListItems = programs.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return selectListItems;
        }
    }
}
