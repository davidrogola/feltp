﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.FacultyManagement.Queries;
using Application.ProgramManagement.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FELTP.Web.Services
{
    public class ProgramLocationSelectListLoader : ISelectListLoader
    {
        IMediator mediator;

        public ProgramLocationSelectListLoader(IMediator mediator)
        {
            this.mediator = mediator;
        }
        public bool CanLoad(SelectItemType type)
        {
            bool canLoad = type == SelectItemType.ProgramLocation ? true : false;
            return canLoad;
        }

        public async Task<SelectList> GetSelectListAsync(object Id,string selected = null)
        {
            if (!CanLoad(SelectItemType.ProgramLocation))
                throw new ArgumentException("Select list loader not found");
            var programLocations = await mediator.Send(new GetProgramOfferLocationQuery() { Id = (int?)Id });

            var selectList = new SelectList(programLocations.OrderBy(x => x.Id), "Id", "Name", selected, "ProgramOffer");

            return selectList;
        }

        public async Task<List<SelectListItem>> GetSelectListItemsAsync(object Id)
        {
            if (!CanLoad(SelectItemType.ProgramLocation))
                throw new ArgumentException("Select list loader not found");

            var programLocationList = await mediator.Send(new GetProgramOfferLocationQuery() { Id = (int?)Id });

            var selectListItems = programLocationList.Select(x => new SelectListItem
            {
                Text = x.Location,
                Value = x.Id.ToString()
            }).ToList();

            return selectListItems;
        }
    }
}
