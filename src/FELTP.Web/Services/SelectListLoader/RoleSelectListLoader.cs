﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.FacultyManagement.Queries;
using Application.ProgramManagement.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FELTP.Web.Services
{
    public class RoleSelectListLoader : ISelectListLoader
    {
        IMediator mediator;

        public RoleSelectListLoader(IMediator mediator)
        {
            this.mediator = mediator;
        }
        public bool CanLoad(SelectItemType type)
        {
            bool canLoad = type == SelectItemType.Role ? true : false;
            return canLoad;
        }

        public async Task<SelectList> GetSelectListAsync(object Id,string selected = null)
        {
            if (!CanLoad(SelectItemType.Role))
                throw new ArgumentException("Select list loader not found");
            var roles = await mediator.Send(new GetFacultyRoleList() { Id = (int?)Id });

            var selectList = new SelectList(roles.OrderBy(x => x.Id), "Id", "Name", selected, "Role");

            return selectList;
        }

        public async Task<List<SelectListItem>> GetSelectListItemsAsync(object Id)
        {
            if (!CanLoad(SelectItemType.Role))
                throw new ArgumentException("Select list loader not found");

            var RoleList = await mediator.Send(new GetFacultyRoleList() { Id = (int?)Id });

            var selectListItems = RoleList.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return selectListItems;
        }
    }
}
