﻿using Application.ProgramManagement.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FELTP.Web.Services
{
    public class DeliverableSelectListLoader : ISelectListLoader
    {
        private readonly IMediator mediator;

        public DeliverableSelectListLoader(IMediator mediator)
        {
            this.mediator = mediator;
        }

        public  bool CanLoad(SelectItemType type)
        {
            bool canLoad = type == SelectItemType.Deliverable ? true : false;
            return canLoad;
        }

        public  async Task<SelectList> GetSelectListAsync(object Id,string selected=null)
        {
            if (!CanLoad(SelectItemType.Deliverable))
                throw new ArgumentException("Select list loader not found");

            var deliverables = await mediator.Send(new GetDeliverableList() {  });

            var selectList = new SelectList(deliverables.OrderBy(x => x.Id), "Id", "Name", selected, "Deliverable");

            return selectList;
        }   

        public  async Task<List<SelectListItem>> GetSelectListItemsAsync(object Id)
        {
            if (!CanLoad(SelectItemType.Deliverable))
                throw new ArgumentException("Select list loader not found");

            var deliverables = await mediator.Send(new GetDeliverableList() {  });

            var selectListItems = deliverables.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return selectListItems;
        }
    }
}
