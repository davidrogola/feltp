﻿using Application.LookUp.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FELTP.Web.Services
{
    public class MinistrySelectListLoader : ISelectListLoader
    {
        IMediator mediator;

        public MinistrySelectListLoader(IMediator mediator)
        {
            this.mediator = mediator;
        }
        public bool CanLoad(SelectItemType type)
        {
            bool canLoad = type == SelectItemType.Ministry ? true : false;
            return canLoad;
        }

        public async Task<SelectList> GetSelectListAsync(object Id, string selected = null)
        {
            if (!CanLoad(SelectItemType.Ministry))
                throw new ArgumentException("Select list loader not found");
            var ministries = await mediator.Send(new GetMinistries() { Id = (int?)Id });

            var selectList = new SelectList(ministries.OrderBy(x => x.Id), "Id", "Name", selected, "Ministry");

            return selectList;
        }

        public async Task<List<SelectListItem>> GetSelectListItemsAsync(object Id)
        {
            if (!CanLoad(SelectItemType.Ministry))
                throw new ArgumentException("Select list loader not found");

            var ministryList = await mediator.Send(new GetMinistries() { Id = (int?)Id });

            var selectListItems = ministryList.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return selectListItems;
        }
    }
}
