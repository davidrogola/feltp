﻿using Application.LookUp.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FELTP.Web.Services
{
    public class CountrySelectListLoader : ISelectListLoader
    {
        IMediator mediator;

        public CountrySelectListLoader(IMediator mediator)
        {
            this.mediator = mediator;
        }
        public bool CanLoad(SelectItemType type)
        {
            bool canLoad = type == SelectItemType.Country ? true : false;
            return canLoad;
        }

        public async Task<SelectList> GetSelectListAsync(object Id, string selected = null)
        {
            if (!CanLoad(SelectItemType.Country))
                throw new ArgumentException("Select list loader not found");
            var countries = await mediator.Send(new GetCountries() { });

            var selectList = new SelectList(countries.OrderBy(x => x.Id), "Id", "Name", selected, "Country");

            return selectList;
        }

        public async Task<List<SelectListItem>> GetSelectListItemsAsync(object Id)
        {
            if (!CanLoad(SelectItemType.Country))
                throw new ArgumentException("Select list loader not found");

            var countryList = await mediator.Send(new GetCountries() {  });

            var selectListItems = countryList.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return selectListItems;
        }
    }
}
