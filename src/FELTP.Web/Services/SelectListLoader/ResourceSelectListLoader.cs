﻿using Application.ProgramManagement.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FELTP.Web.Services
{
    public class ResourceSelectListLoader : ISelectListLoader
    {
        private readonly IMediator mediator;

        public ResourceSelectListLoader(IMediator mediator)
        {
            this.mediator = mediator;
        }

        public bool CanLoad(SelectItemType type)
        {
            bool canLoad = type == SelectItemType.Resource ? true : false;
            return canLoad;
        }

        public async Task<SelectList> GetSelectListAsync(object Id, string selected = null)
        {
            if (!CanLoad(SelectItemType.Resource))
                throw new ArgumentException("Select list loader not found");

            var resources = await mediator.Send(new GetResourceList() { ResourceTypeId = (int?)Id });

            var selectList = new SelectList(resources.OrderBy(x => x.ResourceTypeId), "Id", "Name", selected, "ResourceType");

            return selectList;
        }

        public async Task<List<SelectListItem>> GetSelectListItemsAsync(object Id)
        {
            if (!CanLoad(SelectItemType.Resource))
                throw new ArgumentException("Select list loader not found");

            var resources = await mediator.Send(new GetResourceList() { ResourceTypeId = (int?)Id });

            var selectListItems = resources.Select(x => new SelectListItem
            {
                Text = x.ResourceTitle,
                Value = x.Id.ToString()
            }).ToList();

            return selectListItems;
        }
    }
}
