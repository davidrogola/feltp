﻿using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FELTP.Web.Services
{
    public class GlobalExceptionLogger : IExceptionFilter
    {
        ILogger logger = Log.ForContext<GlobalExceptionLogger>();

        public void OnException(ExceptionContext context)
        {
            logger.Error(context.Exception, $"An error occured during processing of a request on action {context.ActionDescriptor.DisplayName} with route data {context.HttpContext.Request.Path.ToString()}");
        }
    }
}
