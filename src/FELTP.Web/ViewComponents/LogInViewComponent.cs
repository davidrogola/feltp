﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FELTP.Web.ViewComponents
{
    public class LogInViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View("LogIn");
        }
    }
}
