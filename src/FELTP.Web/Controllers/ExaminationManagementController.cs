﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application;
using Application.Common.Services;
using Application.FacultyManagement.Queries;
using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using Application.ResidentManagement.Commands;
using Application.ResidentManagement.Queries;
using Application.ResidentManagement.QueryHandlers;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProgramManagement.Domain.Examination;
using ProgramManagement.Domain.Program;
using UserManagement.Services.Authorization;

namespace FELTP.Web.Controllers
{
    [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
    [Authorize]
    public class ExaminationManagementController : Controller
    {
        IMediator mediator;
        public ExaminationManagementController(IMediator _mediator)
        {
            mediator = _mediator;
        }

        [Operation("Add Examination Schedule")]
        public async Task<IActionResult> AddExaminationSchedule(int scheduleId, int programId, int semesterId)
        {

            var model = new AddSemesterScheduleItemCommand()
            {
               // ProgramCourses = await GetProgramCourses(programId, semesterId, ScheduleItemType.Examination),
                ScheduleItemType = ScheduleItemType.Examination,
                SemesterScheduleId = scheduleId,
                ProgramId = programId,
                SemesterId = semesterId,
                SemesterSchedule = await GetSemesterSchedule(scheduleId),
            };

            return PartialView(model);
        }

        [HttpPost]
        [Operation("Add Examination Schedule")]
        public async Task<IActionResult> AddExaminationSchedule(AddSemesterScheduleItemCommand model)
        {
            if (!ModelState.IsValid)
            {
                //model.ProgramCourses = await GetProgramCourses(model.ProgramId, model.SemesterId, ScheduleItemType.Examination);
                model.SemesterSchedule = await GetSemesterSchedule(model.SemesterScheduleId);
                return PartialView(model);
            }

            model.CreatedBy = User.Identity.Name;

            var result = await mediator.Send(model);

            if (result.CreatedSuccesfully)
                return PartialView("ScheduleResult", result);

            ModelState.AddModelError("Response", result.Message);
            return PartialView(model);
        }

        [Operation("Add Examination Schedule")]
        public async Task<IActionResult> CreateExaminationTimetable(int Id, int courseId)
        {
            var scheduleItems = await mediator.Send(new GetSemesterScheduleItemList { Id = Id });

            var scheduleItem = scheduleItems.SingleOrDefault();
            
            var model = new AddScheduleItemTimetableCommand
            {
                StartDate = scheduleItem.StartDate.ToLocalDate().ToShortDateString(),
                EndDate = scheduleItem.EndDate.ToLocalDate().ToShortDateString(),
                EndDateLimit = scheduleItem.EndDate.ToLocalDate().AddDays(1).ToShortDateString(),
                TimetableItem = await GetCourseExaminations(scheduleItem.SemesterId),
                ScheduleItemId = Id,
                ScheduleItemName = scheduleItem.Name,
                FacultySelectList = await GetFacultyDropDown(),
                SemesterScheduleId = scheduleItem.SemesterScheduleId,
                TimetableType = TimetableType.Examination,
                ProgramCourseId = courseId
            };
            return View(model);
        }
        
        [HttpPost]
        [Operation("Add Examination Schedule")]
        public async Task<IActionResult> CreateExaminationTimetable(AddScheduleItemTimetableCommand model)
        {
            if (!ModelState.IsValid)
            {
                model.FacultySelectList = await GetFacultyDropDown();
                return View(model);
            }              

            model.TimetableType = TimetableType.Examination;
            model.CreatedBy = User.Identity.Name;

            var result = await mediator.Send(model);

            return RedirectToAction(nameof(CreateExaminationTimetable), new { Id = model.ScheduleItemId, CourseId = model.ProgramCourseId });
        }
        
        [Operation("Add Examination Schedule")]
        public async Task<IActionResult> CreateCatTimetable(int Id, int courseId)
        {
            var scheduleItems = await mediator.Send(new GetSemesterScheduleItemList { Id = Id });

            var scheduleItem = scheduleItems.SingleOrDefault();

            var model = new AddScheduleItemTimetableCommand
            {
                StartDate = scheduleItem.StartDate.ToLocalDate().ToShortDateString(),
                EndDate = scheduleItem.EndDate.ToLocalDate().ToShortDateString(),
                EndDateLimit = scheduleItem.EndDate.ToLocalDate().AddDays(1).ToShortDateString(),
                TimetableItem = await GetCourseExaminations(scheduleItem.SemesterId),
                ScheduleItemId = Id,
                ScheduleItemName = scheduleItem.Name,
                FacultySelectList = await GetFacultyDropDown(),
                SemesterScheduleId = scheduleItem.SemesterScheduleId,
                ProgramCourseId = courseId,
            };
            return View(model);
        }
        
        [HttpPost]
        [Operation("Add Examination Schedule")]
        public async Task<IActionResult> CreateCatTimetable(AddScheduleItemTimetableCommand model)
        {
            if (!ModelState.IsValid)
            {
                model.FacultySelectList = await GetFacultyDropDown();
                return View(model);
            }              
            model.CreatedBy = User.Identity.Name;

            var result = await mediator.Send(model);

            return RedirectToAction(nameof(CreateCatTimetable), new { Id = model.ScheduleItemId, CourseId = model.ProgramCourseId });
        }

        [Operation("View Course Examination")]
        private async Task<TimetableItemCommand> GetCourseExaminations(int semesterId)
        {
            var programCourses = await mediator.Send(new GetProgramCoursesList { SemesterId = semesterId });

            var courseIds = programCourses.Select(x => x.CourseId).ToList();
            var examinations = await mediator.Send(new GetExaminationListQuery());

            var courseExams = examinations.Where(x => x.CourseId != null && courseIds.Contains(x.CourseId.Value));
            
            var catSelectList = new List<SelectListItem>()
            {
                new SelectListItem {Text = "Select CAT", Value = "0"},
                new SelectListItem {Text = "CAT I", Value = ExamType.CatOne.ToString()},
                new SelectListItem {Text = "CAT II", Value = ExamType.CatTwo.ToString()},
                new SelectListItem {Text = "CAT III", Value = ExamType.CatThree.ToString()}
            };

            return new TimetableItemCommand { 

                 Exams = courseExams.Select(x=>new SelectListItem
                 {
                     Text = x.Name,
                     Value = x.Id.ToString()
                 }).ToList(),
                 Cats = catSelectList
            };


        }

        private async Task<List<SelectListItem>> GetFacultyDropDown()
        {
            var faculties = await mediator.Send(new GetFacultyForDropDownDisplayQuery());
            return faculties;
        }

        public async Task<IActionResult> ViewExaminationTimetable(int id, int semesterScheduleId)
        {
            var scheduleItem = await mediator.Send(new GetSemesterScheduleItemList { Id = id });

            return View(new TimetableViewModel
            {
                ScheduleItemName = scheduleItem.SingleOrDefault()?.Name,
                SemesterScheduleItemId = id,
                SemesterScheduleId = semesterScheduleId
            });
        }

        public async Task<IActionResult> _ViewExaminationTimetableSchedule(int examinationId, int semesterScheduleItemId, string examinationName)
        {
            var scheduleItem = await mediator.Send(new GetSemesterScheduleItemList { Id = semesterScheduleItemId });

            return PartialView(new TimetableViewModel
            {
                ScheduleItemName = scheduleItem.SingleOrDefault()?.Name,
                SemesterScheduleItemId = semesterScheduleItemId,
                SemesterScheduleId = scheduleItem.SingleOrDefault().SemesterScheduleId,
                ExaminationId = examinationId,
                ExaminationName = examinationName
            });
        }

        public async Task<IActionResult> GetExamTimetableList(IDataTablesRequest request, int scheduleItemId, int examinationId)
        {
            var timetable = await mediator.Send(new GetTimetableListQuery
            {
                SemesterScheduleItemId = scheduleItemId
            });

            var examSchedules = timetable.Where(x => x.ExaminationId == examinationId).ToList();

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? examSchedules
                : examSchedules.Where(_item => _item.ExaminationName.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, examSchedules.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> GetFullExamTimetableList(IDataTablesRequest request, int scheduleItemId)
        {
            var timetable = await mediator.Send(new GetTimetableListQuery
            {
                SemesterScheduleItemId = scheduleItemId
            });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? timetable
                : timetable.Where(_item => _item.ExaminationName.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, timetable.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        [HttpPost]
        public async Task<IActionResult> GetCatTimetableByScheduleItem(IDataTablesRequest request, int scheduleItemId)
        {
            var timetable = await mediator.Send(new GetTimetableListQuery
            {
                SemesterScheduleItemId = scheduleItemId,
                TimetableTypes = new List<TimetableType> {TimetableType.CatOne,TimetableType.CatTwo, TimetableType.CatThree}
            });

            var dataPage = timetable.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, timetable.Count(), timetable.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }
        
        public async Task<IActionResult> GetExamTimetableByScheduleItem(IDataTablesRequest request, int scheduleItemId)
        {
            var timetable = await mediator.Send(new GetTimetableListQuery
            {
                SemesterScheduleItemId = scheduleItemId
            });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? timetable
                : timetable.Where(_item => _item.UnitName.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, timetable.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> GetExamsInTimetable(IDataTablesRequest request, int scheduleItemId)
        {
            var timetable = await mediator.Send(new GetTimetableListQuery
            {
                SemesterScheduleItemId = scheduleItemId
            });

            var exams = timetable.GroupBy(x => x.ExaminationId)
                .Select(u => u.FirstOrDefault()).ToList();

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? exams
                : exams.Where(_item => _item.UnitName.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, exams.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        private async Task<SelectList> GetProgramCourses(int programId, int semesterId, ScheduleItemType scheduleType)
        {
            IEnumerable<ProgramCoursesViewModel> coursesInSemester = null;

            var programCourses = await mediator.Send(new GetProgramCoursesList { ProgramId = programId });

            var activeCourses = programCourses.Where(x => x.Status == Status.Active.ToString()).ToList();

            switch (scheduleType)
            {
                case ScheduleItemType.Examination:
                    coursesInSemester = activeCourses.Where(x => x.SemesterId == semesterId
                    && x.CourseType == CourseTypeEnum.Didactic.ToString());
                    break;
                default:
                    coursesInSemester = activeCourses.Where(x => x.SemesterId == semesterId);
                    break;
            }
            return new SelectList(coursesInSemester, "Id", "CourseName");
        }

        private async Task<SemesterScheduleDates> GetSemesterSchedule(int Id)
        {
            var result = await mediator.Send(new GetSemesterScheduleList { Id = Id });

            var schedule = result.SingleOrDefault();

            return new SemesterScheduleDates
            {
                StartDate = schedule.EstimatedStartDate,
                EndDate = schedule.EstimatedEndDate,
                Semester = schedule.Semester
            };
        }

        [Operation("Input Examination Scores")]
        public async Task<IActionResult> InputExaminationScores(int enrollmentId, int semesterId, long residentId, int offerId)
        {
            var examinationDetailResponse = await mediator.Send(new GetResidentExamDetail
            {
                ProgramEnrollmentId = enrollmentId,
                ProgramOfferId = offerId,
                SemesterId = semesterId
            });

            var model = new InputResidentExaminationScoreCommand()
            {
                ExamScheduleDescription = examinationDetailResponse.IsSuccessful ? examinationDetailResponse.ExamScheduleDescription.Split("(")[0] :null,
                ExamDetailResponse = examinationDetailResponse,
                EnrollmentId = enrollmentId,
                SemesterScheduleItemId = semesterId,
                ResidentId = residentId,
            };

            return PartialView(model);
        }

        [HttpPost]
        [Operation("Input Examination Scores")]
        public async Task<IActionResult> InputExaminationScores(InputResidentExaminationScoreCommand command)
        {
            if (!ModelState.IsValid)
                return PartialView(command);

            command.CreatedBy = User.Identity.Name;

            var result = await mediator.Send(command);

            if (result.IsSuccessful)
                return PartialView("InputScoresResult", result);

            ModelState.AddModelError("Response", result.Message);
            return PartialView(command);
        }
        
        
        
        [Operation("Input Examination Scores")]
        public async Task<IActionResult> InputCatScores(int enrollmentId, int semesterId, long residentId, int offerId)
        {
            var catScoreDetailsResponse = await GetResidentCatScoreDetails(enrollmentId, offerId, semesterId);

            var model = new InputResidentExaminationScoreCommand()
            {
                ExamScheduleDescription = catScoreDetailsResponse.IsSuccessful ? catScoreDetailsResponse.ExamScheduleDescription.Split("(")[0] :null,
                ExamDetailResponse = catScoreDetailsResponse,
                EnrollmentId = enrollmentId,
                SemesterScheduleItemId = semesterId,
                ResidentId = residentId,
                SemesterId =  semesterId,
                ProgramOfferId = offerId
            };

            return PartialView(model);
        }
        
        [Operation("Input Examination Scores")]
        [HttpPost]
        public async Task<IActionResult> InputCatScores(InputResidentExaminationScoreCommand command)
        {
            if (!ModelState.IsValid)
            {
                command.ExamDetailResponse =
                    await GetResidentCatScoreDetails(command.EnrollmentId, command.ProgramOfferId, command.SemesterId);
                return PartialView(command);
            }
            
            command.IsCat = true;
            command.CreatedBy = User.Identity.Name;
          
            var result = await mediator.Send(command);

            if (result.IsSuccessful)
                return PartialView("InputScoresResult", result);

            ModelState.AddModelError("Response", result.Message);
            return PartialView(command);
        }

        private async Task<ResidentExamDetailResponse> GetResidentCatScoreDetails(int enrollmentId, int offerId, 
            int semesterId)
        {
           return await mediator.Send(new GetResidentCatScoreDetails
            {
                ProgramEnrollmentId = enrollmentId,
                ProgramOfferId = offerId,
                SemesterId = semesterId,
            });
        }

        public async Task<IActionResult> GetResidentExaminationScores(IDataTablesRequest request, int programEnrollmentId, int ? semesterId)
        {
            var examinations = await mediator.Send(new GetResidentExaminationScore
            {
                EnrollmentId = programEnrollmentId,
                SemesterId = semesterId
            });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? examinations
                : examinations.Where(_item => _item.Name.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, examinations.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }
        

    }
}