﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Common.Models.Queries;
using Application.LookUp.Queries;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FELTP.Web.Controllers
{
    [Authorize]
    public class LookUpController : Controller
    {
        IMediator mediator;
        public LookUpController(IMediator _mediator)
        {
            mediator = _mediator;
        }
        

        public async Task<IActionResult> GetCountries()
        {
            var countries = await mediator.Send(new GetCountries() { });

            var selectItems = countries.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return Json(selectItems);
        }

        public async Task<IActionResult> GetCounties(int ? countryId)
        {
            var counties = await mediator.Send(new GetCounties() { CountryId = countryId });

            var selectItems = counties.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return Json(selectItems);
        }

        public async Task<IActionResult> GetSubCounties(int ? countyId)
        {
            var counties = await mediator.Send(new GetSubCounties() { CountyId = countyId });

            var selectItems = counties.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return Json(selectItems);
        }

        [HttpPost]
        public async Task<IActionResult> GetPersonAcademicHistory(long personId,IDataTablesRequest request)
        {
            var academicHistory = await mediator.Send(new GetPersonAcademicDetail() { PersonId = personId });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? academicHistory
                : academicHistory.Where(_item => _item.Other.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, academicHistory.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        [HttpPost]
        public async Task<IActionResult> GetPersonEmploymentHistory(long personId, IDataTablesRequest request)
        {
            var employmentHistory = await mediator.Send(new GetPersonEmploymentHistory() { PersonId = personId });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? employmentHistory
                : employmentHistory.Where(_item => _item.PersonnelNumber.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, employmentHistory.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> GetUniversities()
        {
            var universities = await mediator.Send(new GetUniversities() { });

            var selectItems = universities.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return Json(selectItems);
        }

        public async Task<IActionResult> GetMinistries()
        {
            var ministries = await mediator.Send(new GetMinistries() { });

            var selectItems = ministries.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return Json(selectItems);
        }
    }
}