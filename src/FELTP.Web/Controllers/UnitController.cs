﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using FELTP.Web.Services;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using UserManagement.Services.Authorization;

namespace FELTP.Web.Controllers
{
    [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
    [Authorize]
    public class UnitController : Controller
    {
        private readonly IMediator mediator;
        IEnumerable<ISelectListLoader> selectListLoaders;

        public UnitController(IMediator _mediator, IEnumerable<ISelectListLoader> _selectListLoaders)
        {
            mediator = _mediator;
            selectListLoaders = _selectListLoaders;
        }
        public IActionResult Index()
        {
            return View();
        }

        [Operation("Add Unit")]
        public IActionResult AddUnit()
        {
            return View();
        }

        [HttpPost]
        [Operation("Add Unit")]
        public async Task<IActionResult> AddUnit(AddUnitCommand model)
        {
            if (!ModelState.IsValid)
                return View();

            model.CreatedBy = User.Identity.Name;
            var unitId = await mediator.Send(model);
            
            return RedirectToAction(nameof(UnitList));
        }

        public async Task<IActionResult> EditUnit(int Id)
        {
            var unit = await mediator.Send(new GetUnit { Id = Id });
            var editUnitModel = new UpdateUnitCommand
            {
                Id = unit.Id,
                Code = unit.Code,
                Name = unit.Name,
                Description = unit.Description,
                CreatedBy = unit.CreatedBy,
                HasFieldPlacementActivity = unit.HasActivity,
                FieldPlacementActivities = await GetSelectListLoader(SelectItemType.FieldPlacementActivity).GetSelectListItemsAsync(null),
                FieldPlacementActivityId = unit.FieldPlacementActivityIds
            };

            return PartialView(editUnitModel);
        }
        [HttpPost]
        public async Task<IActionResult> EditUnit(UpdateUnitCommand command)
        {
            if (!ModelState.IsValid)
                return PartialView(command);

            command.UpdatedBy = User.Identity.Name;
            var result = await mediator.Send(command);

           if(result.IsSuccessful)
            return PartialView("UnitResponse",result);

            ModelState.AddModelError("Response", result.Message);
            return PartialView(command);
        }

        [Operation("View Unit List")]
        public IActionResult UnitList()
        {
            return View();
        }

        [HttpPost]
        [Operation("View Unit List")]
        public async Task<IActionResult> GetUnits(IDataTablesRequest request)
        {
            var units = await mediator.Send(new GetUnitList());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? units
                : units.Where(_item => _item.Name.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, units.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public async Task<JsonResult> GetUnits(bool hasActivity)
        {
            var units = await mediator.Send(new GetUnitList() { HasActivity = hasActivity, IsDropDownRequest = true });
            var unitsListItems = units.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return Json(unitsListItems);
        }

        public async Task<JsonResult> GetUnitFieldPlacementActivityAndDeliverables(int unitId)
        {
            var result = await mediator.Send(new GetUnitFieldPlacementActivityWithDeliverables { UnitId = unitId });

            return Json(result);
        }

        [Operation("View Unit List")]
        public  async Task<IActionResult> ViewDetails(int Id)
        {
            var unit = await mediator.Send(new GetUnit { Id = Id });

            return PartialView(unit);
        }

        [Operation("View Unit List")]
        public async Task<IActionResult> ViewUnitActivities(int Id)
        {
            var unit = await mediator.Send(new GetUnit { Id = Id });

            return PartialView(unit);
        }


        [HttpPost]
        [Operation("View Unit List")]
        public async Task<IActionResult> GetUnitActivities(IDataTablesRequest request, int unitId)
        {
            var activities = await mediator.Send(new GetUnitFieldPlacementActivities { UnitId = unitId });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? activities
                : activities.Where(_item => _item.Name.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, activities.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        private ISelectListLoader GetSelectListLoader(SelectItemType selectItemType)
        {
            return selectListLoaders.SingleOrDefault(x => x.CanLoad(selectItemType));
        }

    }
}