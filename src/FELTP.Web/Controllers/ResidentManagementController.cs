﻿using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using Application.ResidentManagement.Commands;
using Application.ResidentManagement.Queries;
using ProgramManagement.Domain.Program;
using Microsoft.AspNetCore.Authorization;
using FELTP.Web.Services;
using ResidentManagement.Domain;
using Newtonsoft.Json;
using UserManagement.Services.Authorization;
using Application.Common.Models;
using Application.Common.Models.Queries;

namespace FELTP.Web.Controllers
{
    [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
    [Authorize]
    public class ResidentManagementController : Controller
    {
        IMediator mediator;
        ISelectListLoader programSelectListLoader;
        IEnumerable<ISelectListLoader> selectListLoaders;
        public ResidentManagementController(IMediator _mediator, IEnumerable<ISelectListLoader> _selectListLoaders)
        {
            mediator = _mediator;
            selectListLoaders = _selectListLoaders;
            programSelectListLoader = selectListLoaders.Where(x => x.CanLoad(SelectItemType.Program)).SingleOrDefault();
        }

        public IActionResult DashBoard()
        {
            return View();
        }

        [Operation("Register New Applicant")]
        public async Task<IActionResult> RegisterNewApplicant(int Id)
        {
            var offerDetail = await mediator.Send(new GetProgramOfferById { OfferId = Id });

            var response = offerDetail.ValidateProgramOfferApplicationDuration();
            if (true)
            {
                var applicantCommand = new RegisterNewApplicantCommand()
                {
                    OfferName = offerDetail.Name,
                    ProgramName = offerDetail.ProgramName,
                    ProgramOfferId = offerDetail.Id
                };
                return View(applicantCommand);
            }

        }

        [HttpPost]
        [Operation("Register New Applicant")]
        public async Task<IActionResult> RegisterNewApplicant(RegisterNewApplicantCommand command, IFormFile statementFile)
        {
            if (!ModelState.IsValid)
                return View(command);

            if (command.PersonalStatementSubmitted)
            {
                if (statementFile == null)
                {
                    ModelState.AddModelError("StatementFile", "Please upload a personal statement file");
                    return View(command);
                }

                using (var memoryStream = new MemoryStream())
                {
                    statementFile.CopyTo(memoryStream);
                    command.File = new StatementFile(memoryStream.ToArray(), statementFile.ContentType,
                        statementFile.FileName);
                }
            }
            command.CreatedBy = User.Identity.Name;

            var applicantId = await mediator.Send(command, CancellationToken.None);

            if (applicantId == default(long))
                return View("Error");

            return RedirectToAction("ProgramOfferApplicants", "ProgramManagement", new { id = command.ProgramOfferId });
        }


        public async Task<IActionResult> ApplicantDetails(long applicantId, int offerId)
        {
            var applicantViewModel = await mediator.Send(new GetApplicantQuery { Id = applicantId, ProgramOfferId = offerId });
            applicantViewModel.IsApplicantPartial = true;

            return View(applicantViewModel);
        }

        [Operation("Edit Applicant Bio Data")]
        public async Task<IActionResult> _EditApplicantBioData(int applicantId, int programOfferId)
        {
            var applicant = await mediator.Send(new GetApplicantQuery() { Id = applicantId, ProgramOfferId = programOfferId });

            var updateApplicantCommand = new UpdateApplicantBioDataCommand
            {
                UpdatePersonBioData = new UpdatePersonBioDataCommand
                {
                    BioDataInfo = applicant.Person.BioDataInfo,
                    Cadres = await GetSelectListLoader(SelectItemType.Cadre).GetSelectListItemsAsync(null),
                },
                ApplicantId = applicantId,
                ProgramOfferId = programOfferId
            };

            return PartialView(updateApplicantCommand);
        }

        private ISelectListLoader GetSelectListLoader(SelectItemType selectItemType)
        {
            return selectListLoaders.Where(x => x.CanLoad(selectItemType)).SingleOrDefault();
        }

        [HttpPost]
        public async Task<IActionResult> _EditApplicantBioData(UpdateApplicantBioDataCommand command)
        {
            var id = await mediator.Send(command);

            return RedirectToAction(nameof(ApplicantDetails), new { applicantId = command.ApplicantId, offerId = command.ProgramOfferId });
        }

        [Operation("Edit Applicant Contact Info")]
        public async Task<IActionResult> _EditApplicantContactInfo(long personId, int applicantId, int programOfferId)
        {

            var person = await mediator.Send(new GetPersonQuery() { PersonId = personId });

            var model = new UpdatePersonContactInfoCommand
            {
                ContactInfo = person.ContactInfo,
                AddressInfo = person.AddressInfo,
                Countries = await GetSelectListLoader(SelectItemType.Country).GetSelectListItemsAsync(null),
                Counties = await GetSelectListLoader(SelectItemType.County).GetSelectListItemsAsync(person.AddressInfo.CountryId),
                Subcounties = await GetSelectListLoader(SelectItemType.Subcounty).GetSelectListItemsAsync(person.AddressInfo.CountyId),
                PersonId = personId,
                CorrelationId = applicantId,
                ProgramOfferId = programOfferId
            };

            return PartialView(model);
        }


        [HttpPost]
        public async Task<IActionResult> _EditApplicantContactInfo(UpdatePersonContactInfoCommand command)
        {
            var id = await mediator.Send(command);

            return RedirectToAction(nameof(ApplicantDetails), new { applicantId = command.CorrelationId, offerId = command.ProgramOfferId });

        }

        public IActionResult ApplicantTransition(int id)
        {
            return PartialView(new AddResidentCommand { ApplicantId = id });
        }

        [HttpPost]
        public async Task<IActionResult> ApplicantTransition(AddResidentCommand model)
        {
            model.CreatedBy = User.Identity.Name;
            var residentId = await mediator.Send(model);
            if (residentId == default(long))
                return View();

            return RedirectToAction(nameof(ResidentDetails), new { applicantId = model.ApplicantId });
        }

        [Operation("View Applicants List")]
        public async Task<IActionResult> ApplicantList()
        {
            var model = new FilterResident()
            {
                ProgramSelectList = await programSelectListLoader.GetSelectListAsync(null)
            };
            return View(model);
        }


        [HttpPost]
        [Operation("View Applicants List")]
        public async Task<IActionResult> GetApplicants(IDataTablesRequest request, int? Id, GetBy getBy)
        {
            var applicants = await mediator.Send(new GetApplicantListQuery() { GetApplicantBy = getBy, Id = Id });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? applicants
                : applicants.Where(_item => _item.IdentificationNumber.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, applicants.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        [Operation("View Applicants List")]
        public async Task<IActionResult> ResidentList()
        {
            var model = new FilterResident()
            {
                ProgramSelectList = await programSelectListLoader.GetSelectListAsync(null)

            };

            return View(model);
        }

        [HttpPost]
        [Operation("View Applicants List")]
        public async Task<IActionResult> GetResidents(IDataTablesRequest request, int? Id, GetBy getBy)
        {
            var residents = await mediator.Send(new GetResidentListQuery() { Id = Id, GetResidentBy = getBy });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? residents
                : residents.Where(_item => _item.IdentificationNumber.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, residents.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }


        public async Task<IActionResult> ResidentDetails(long applicantId, int offerId)
        {
            var resident = await mediator.Send(new GetApplicantQuery { Id = applicantId, ProgramOfferId = offerId });

            return View(resident);

        }

        [Operation("Enroll Resident")]
        public async Task<IActionResult> EnrollResident(long Id, long residentId, int offerId)
        {
            var offerDetails = await mediator.Send(new GetProgramOfferApplicationByApplicantId
            {
                ApplicantId = Id
            });
            string message = String.Empty;

            var programOfferPendingEnrollment = offerDetails
                .FirstOrDefault(x => x.ApplicantId == Id && x.ProgramOfferId == offerId);

            if (programOfferPendingEnrollment == null)
            {
                message = "Resident has no program application pending enrollment";
                return PartialView("EnrollmentPartialInfo", message);
            }

            var previousProgramEnrollment = offerDetails.FirstOrDefault(x => x.ProgramId == programOfferPendingEnrollment.ProgramId 
            && x.EnrollmentStatus == EnrollmentStatus.Enrolled);

            if(previousProgramEnrollment != null)
            {
                message = $"Applicant is already enrolled to the program {previousProgramEnrollment.Program} with offer {previousProgramEnrollment.ProgramOfferName}";
                return PartialView("EnrollmentPartialInfo", message);
            }

            var qualificationStatus = (ProgramQualificationStatus)Enum.Parse(typeof(ProgramQualificationStatus), programOfferPendingEnrollment.QualificationStatus);

            switch (qualificationStatus)
            {
                case ProgramQualificationStatus.NotQualified:
                    message = "Resident has not qualified for the program";
                    break;
                case ProgramQualificationStatus.Pending_Evaluation:
                    message = "Applicant program application has not been evaluated. Please evaluate the application before enrollment";
                    break;
            }

            if(!String.IsNullOrEmpty(message))
                return PartialView("EnrollmentPartialInfo", message);


            var programOffer = await mediator.Send(new GetProgramOfferById
            {
                OfferId = programOfferPendingEnrollment.ProgramOfferId
            });

            return PartialView(new EnrollResidentModel()
            {
                EnrollResidentCommand = new EnrollResidentCommand
                {
                    ProgramOfferApplicationId = programOfferPendingEnrollment.Id,
                    ApplicantId = Id,
                    ResidentId = residentId,
                    ProgramOfferId = programOfferPendingEnrollment.ProgramOfferId,
                    ProgramId = programOffer.ProgramId
                },
                Name = programOfferPendingEnrollment.Name,
                RegistrationNumber = programOfferPendingEnrollment.RegistrationNumber,

                ProgramOfferDetail = programOffer,

            });
        }

        [HttpPost]
        [Operation("Enroll Resident")]
        public async Task<IActionResult> EnrollResident(EnrollResidentModel model)
        {
            if (!ModelState.IsValid)
                return PartialView(model);
            model.EnrollResidentCommand.CreatedBy = User.Identity.Name;

            var enrollmentResponse = await mediator.Send(model.EnrollResidentCommand);

            return PartialView("EnrollResidentResult", enrollmentResponse);
        }

        public async Task<IActionResult> CourseDetails(long id, int offerId)
        {
            var model = await mediator.Send(new GetResidentCourseDetails { ResidentId = id, ProgramOfferId = offerId });
            if (model != null)
                model.ResidentId = id;

            return View(model);
        }

        [Operation("View Resident Course Work")]
        public async Task<IActionResult> GetResidentCourseWork(int programEnrollmentId, int? semesterId, IDataTablesRequest request)
        {
            var residentCourseWork = await mediator.Send(new GetResidentCourseWork()
            {
                EnrollmentId = programEnrollmentId,
                SemesterId = semesterId
            });

            var filteredData = string.IsNullOrWhiteSpace(request.Search.Value)
                ? residentCourseWork
                : residentCourseWork.Where(_item => _item.Course.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, residentCourseWork.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        [Operation("View Resident Field Placement Activities")]
        public async Task<IActionResult> GetResidentFieldPlacementActivities(int programEnrollmentId, int? semesterId, IDataTablesRequest request)
        {
            var residentCourseActivities = await mediator.Send(new GetResidentFieldPlacementActivities()
            {
                ProgramEnrollmentId = programEnrollmentId,
                SemesterId = semesterId
            });

            var filteredData = string.IsNullOrWhiteSpace(request.Search.Value)
                ? residentCourseActivities
                : residentCourseActivities.Where(_item => _item.Activity.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, residentCourseActivities.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public async Task<JsonResult> GetResidentActivitiesDropDown(int programEnrollmentId)
        {
            var residentCourseActivities = await mediator.Send(new GetResidentFieldPlacementActivities()
            {
                ProgramEnrollmentId = programEnrollmentId
            });

            var activities = residentCourseActivities.Select(x => new SelectListItem
            {
                Text = x.Activity,
                Value = x.Id.ToString()
            }).ToList();

            return Json(activities);
        }

        public IActionResult ResidentUnitsList(long Id, string course)
        {
            return PartialView(new ResidentUnitViewModel
            {
                ResidentCourseWorkId = Id,
                Course = course
            });
        }

        [Operation("View Resident Units List")]
        public async Task<IActionResult> GetResidentUnits(long residentCourseWorkId, IDataTablesRequest request)
        {
            var residentModules = await mediator.Send(new GetResidentUnits()
            {
                ResidentCourseWorkId = residentCourseWorkId
            });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? residentModules
                : residentModules.Where(_item => _item.Unit.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, residentModules.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public IActionResult ResidentDeliverablesList(long Id, string activity)
        {
            return PartialView(new ResidentDeliverableViewModel()
            {
                Activity = activity,
                ResidentFieldPlacementActivityId = Id
            });
        }

        [Operation("View Resident Deliverables List")]
        public async Task<IActionResult> GetResidentDeliverables(long? residentActivityId, int? enrollmentId, IDataTablesRequest request)
        {
            var residentDeliverables = await mediator.Send(new GetResidentDeliverables()
            {
                ResidentFieldPlacementActivityId = residentActivityId,
                ProgramEnrollmentId = enrollmentId
            });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? residentDeliverables
                : residentDeliverables.Where(_item => _item.Name.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, residentDeliverables.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        [Operation("View Resident Course History")]
        public async Task<IActionResult> GetResidentCourseHistory(long residentId, IDataTablesRequest request)
        {
            var courseHistory = await mediator.Send(new GetResidentCourseHistory
            {
                ResidentId = residentId
            });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
               ? courseHistory
               : courseHistory.Where(_item => _item.Program.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, courseHistory.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);

        }

        [Operation("Add Cadre")]
        public IActionResult _AddCadre()
        {
            return PartialView();
        }

        [HttpPost]
        [Operation("Add Cadre")]
        public async Task<IActionResult> _AddCadre(AddCadreCommand model)
        {
            if (!ModelState.IsValid)
                return View();
            var cadreId = await mediator.Send(model);

            return RedirectToAction(nameof(CadreList));
        }

        [Operation("View Cadre List")]
        public IActionResult CadreList()
        {
            return View();
        }

        [Operation("Edit Cadre")]
        public async Task<IActionResult> _EditCadre(int id)
        {
            var Cadre = await mediator.Send(new GetCadreList() { Id = id });

            return PartialView(Cadre.FirstOrDefault());
        }

        [HttpPost]
        [Operation("Edit Cadre")]
        public async Task<IActionResult> _EditCadre(UpdateCadreCommand command)
        {
            var id = await mediator.Send(command);

            return RedirectToAction(nameof(CadreList));
        }

        public ActionResult _DeactivateCadre(int id)
        {
            return PartialView("_DeactivateCadre");

        }

        [HttpPost]
        public async Task<IActionResult> _DeactivateCadre(DeactivateCadreCommand command)
        {
            if (!ModelState.IsValid)
            {
                command = await SetDeactivateCadreCommandModel(command.Id);
                return View(command);
            }

            command.DeactivatedBy = User.Identity.Name;

            var id = await mediator.Send(command);

            return RedirectToAction(nameof(CadreList));
        }

        private async Task<DeactivateCadreCommand> SetDeactivateCadreCommandModel(int cadreId)
        {
            var deactivateCadreCommand = new DeactivateCadreCommand();

            var cadreList = await mediator.Send(new GetCadreList { Id = cadreId });

            var cadre = cadreList.SingleOrDefault();

            return deactivateCadreCommand;
        }

        [HttpPost]
        [Operation("View Cadre List")]
        public async Task<IActionResult> GetCadres(IDataTablesRequest request)
        {
            var cadres = await mediator.Send(new GetCadreList());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? cadres
                : cadres.Where(_item => _item.Name.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, cadres.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public async Task<List<SelectListItem>> GetCadres()
        {
            var cadres = await mediator.Send(new GetCadreList());
            var selectListItems = cadres.Select(
                x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                }).ToList();

            return selectListItems;
        }


        [Operation("Add Resident Course Attendance")]
        public async Task<IActionResult> AddResidentCourseAttendance()
        {
            var model = new AddResidentCourseAttendanceCommand
            {
                Programs = await programSelectListLoader.GetSelectListAsync(null)

            };

            return View(model);
        }

        [HttpPost]
        [Operation("Add Resident Course Attendance")]
        public async Task<IActionResult> AddResidentCourseAttendance(AddResidentCourseAttendanceCommand command)
        {

            if (!ModelState.IsValid)
            {
                command.Programs = await programSelectListLoader.GetSelectListAsync(null);
                return View(command);
            }
            command.CreatedBy = User.Identity.Name;
            var result = await mediator.Send(command, CancellationToken.None);

            return RedirectToAction(nameof(ResidentCourseAttendanceResult), new { Id = result.SemesterScheduleItemId, unitId = result.UnitId,
                submitted = result.Success });
        }


        public async Task<IActionResult> ResidentCourseAttendanceResult(int Id, int unitId, bool submitted)
        {
            var unitInformation = await mediator.Send(new GetUnit { Id = unitId });

            return View(new ResidentCourseAttendanceResponse
            {
                Code = unitInformation?.Code,
                UnitName = unitInformation?.Name,
                UnitId = unitId,
                SemesterScheduleItemId = Id,
                Success = submitted,
                Message = submitted ? "Residents attendance information updated sucessfully" :
                  "An error occured while processing your request please try again later"
            });

        }

        [HttpPost]
        [Operation("View Resident Course Attendance List")]
        public async Task<IActionResult> GetResidentCourseAttendance(long Id,int ? unitId, IDataTablesRequest request, 
            GetTimetableBy getBy = GetTimetableBy.CourseWorkId)
        {
            var unitAttendanceDetails = await mediator.Send(new GetResidentCourseAttendance() { Id = Id, GetBy = getBy });

            if (unitId.HasValue)
                unitAttendanceDetails = unitAttendanceDetails.Where(x => x.UnitId == unitId.Value).ToList();

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? unitAttendanceDetails
                : unitAttendanceDetails.Where(_item => _item.ResidentName.ToLower().Contains(request.Search.Value.ToLower()));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, unitAttendanceDetails.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }


        public IActionResult ResidentCourseAttendance(long Id, string course)
        {
            return PartialView(new ResidentUnitAttendanceViewModel
            {
                ResidentCourseWorkId = Id,
                Course = course,

            });
        }

        [Operation("View Enrolled Residents List")]
        public async Task<IActionResult> EnrolledResidentsList(int programOfferId)
        {
            var enrolledResidents = await mediator.Send(new GetEnrolledResidents
            {
                ProgramOfferId = programOfferId
            });
            return PartialView(enrolledResidents);

        }

        [HttpPost]
        [Operation("View Residents By Offer")]
        public async Task<IActionResult> GetResidentsByOffer(int programOfferId, IDataTablesRequest request)
        {
            var residents = await mediator.Send(new GetEnrolledResidents()
            {
                ProgramOfferId = programOfferId
            });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? residents
                : residents.Where(_item => _item.ResidentName.ToLower().Contains(request.Search.Value.ToLower()));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, residents.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public async Task<JsonResult> GetEnrolledResidentsDropDown(int programOfferId)
        {
            var residents = await mediator.Send(new GetEnrolledResidents()
            {
                ProgramOfferId = programOfferId
            });

            var residentsDropdown = residents.Select(x => new SelectListItem
            {
                Text = x.ResidentName,
                Value = x.EnrollmentId.ToString()
            }).ToList();

            return Json(residentsDropdown);
        }

        [Operation("View Applicant Program Offers")]
        public async Task<IActionResult> GetApplicantProgramOffers(IDataTablesRequest request, long applicantId)
        {
            var programOfferApplicants = await mediator.Send(new GetProgramOfferApplicationByApplicantId()
            {
                ApplicantId = applicantId
            });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? programOfferApplicants
                : programOfferApplicants.Where(_item => _item.Program.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);

            var response = DataTablesResponse.Create(request, programOfferApplicants.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        [Operation("Download Statement File")]
        public async Task<IActionResult> DownloadStatementFile(long id)
        {
            var statementFile = await mediator.Send(new GetApplicantStatementFile() { ProgramOfferApplicationId = id });
            if (statementFile == null || (statementFile.FileContents == null && statementFile.FileName == null))
                return Content("File Not Found");

            return File(statementFile.FileContents, statementFile.ContentType, statementFile.FileName);
        }

        [Operation("View Applicant Evaluation")]
        public async Task<IActionResult> ApplicantEvaluation(long id, long applicantId, bool readMode = false)
        {
            var evaluationModel = await mediator.Send(new GetApplicantProgramOfferEvaluationDetails
            {
                ProgramOfferApplicationId = id,
                ApplicantId = applicantId,
            });
            if (readMode)
            {
                return PartialView("ApplicantEvaluationDetails", evaluationModel);
            }

            return PartialView(evaluationModel);
        }

        [HttpPost]
        [Operation("View Applicant Evaluation")]
        public async Task<IActionResult> ApplicantEvaluation(ApplicantEvaluationCommand model)
        {
            model.EvaluatedBy = User.Identity.Name;

            var evaluationResult = await mediator.Send(model);
            if (evaluationResult != null)
            {
                switch (evaluationResult.EvaluationOutCome)
                {
                    case ProgramQualificationStatus.Pending_Evaluation:
                        return RedirectToAction(nameof(ApplicantDetails), new { applicantId = evaluationResult.ApplicantId, offerId = evaluationResult.ProgramOfferId });
                    case ProgramQualificationStatus.Qualified:
                        return RedirectToAction(nameof(ResidentDetails), new { applicantId = evaluationResult.ApplicantId, offerId = evaluationResult.ProgramOfferId });
                    case ProgramQualificationStatus.NotQualified:
                        return RedirectToAction(nameof(ApplicantDetails), new { applicantId = evaluationResult.ApplicantId, offerId = evaluationResult.ProgramOfferId });

                    default:
                        break;
                }
            }
            return PartialView(model);
        }

        [Operation("Filter Applicants")]
        public async Task<IActionResult> FilterApplicants()
        {
            var model = new ShortListApplicantCommand
            {
                ProgramSelectList = await programSelectListLoader.GetSelectListAsync(null),
                ApplicationStatus = ApplicationStatus.Pending

            };
            return View(model);
        }

        [HttpPost]
        [Operation("Filter Applicants")]
        public IActionResult FilterApplicants(ShortListApplicantCommand model)
        {

            return View("FilterApplicantsResult", model);
        }

        [HttpPost]
        [Operation("Short List Applicant")]
        public async Task<IActionResult> ShortListApplicants(ShortListApplicantCommand model)
        {
            if (string.IsNullOrEmpty(model.ProgramOfferApplicationIdsJson))
            {
                ModelState.AddModelError("OfferId", "Please select atleast one applicant for shortlisting");
                return View("FilterApplicantsResult", model);
            }

            model.ProgramOfferApplicationIds = JsonConvert.DeserializeObject<List<long>>(model.ProgramOfferApplicationIdsJson);
            model.ShortListedBy = User.Identity.Name;
            model.DateShortListed = DateTime.Now;

            var result = await mediator.Send(model);

            if (result.CompletedSuccessfully)
            {
                TempData["Message"] = result.Message;
                ModelState.Clear();
            }
            else
            {
                ModelState.AddModelError("UpdateFailed", result.Message);
            }

            return View("FilterApplicantsResult", model);
        }

        [Operation("Search Applicant")]
        public async Task<IActionResult> SearchApplicant(int Id)
        {
            var offerDetail = await mediator.Send(new GetProgramOfferById { OfferId = Id });

            var response = offerDetail.ValidateProgramOfferApplicationDuration();

            if(response.ApplicationsOpen)
              return View(new SearchApplicantQuery { OfferId = Id });

            return View("ApplicationResult", response);
        }

        [HttpPost]
        [Operation("View Applicant Search Results")]
        public IActionResult SearchApplicantResult(SearchApplicantQuery model)
        {
            if (!ModelState.IsValid)
                return View(nameof(SearchApplicant), model);

            return View(model);
        }

        [HttpPost]
        [Operation("View Search Applicant Details")]
        public async Task<IActionResult> SearchApplicantDetails(string identificationNo, IDataTablesRequest request)
        {
            var applicantResult = await mediator.Send(new SearchApplicantQuery()
            {
                IdentificationNumber = identificationNo
            });
            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
          ? applicantResult
          : applicantResult.Where(_item => _item.IdentificationNumber.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, applicantResult.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);

        }

        [Operation("Add Program Application")]
        public async Task<IActionResult> AddNewProgramApplication(long Id, long? residentId)
        {
            var offerApplications = await mediator.Send(new GetProgramOfferApplicationByApplicantId { ApplicantId = Id });

            var pendingAppication = offerApplications.FirstOrDefault(x => x.ApplicationStatus == ApplicationStatus.Pending);

            var model = new AddNewProgramOfferApplicationCommand
            {
                ApplicantId = Id,
                ProgramSelectItem = await programSelectListLoader.GetSelectListAsync(null),
                IsValid = false,
                ResidentId = residentId ?? residentId.Value

            };

            if (pendingAppication != null)
            {
                model.Message = $"Applicant has a pending program offer application for {pendingAppication.ProgramOfferName}";
                return PartialView(model);
            }


            if (residentId.HasValue)
            {
                var courseHistory = await mediator.Send(new GetResidentCourseHistory { ResidentId = residentId.Value });

                var unwantedProgramStatus = new[]
                {
                    CompletionStatus.Discontinued.ToString(),
                    CompletionStatus.NotCompleted.ToString(),
                    CompletionStatus.Pending.ToString(),
                    CompletionStatus.Leave.ToString()
                }.ToList();

                bool completionPending = courseHistory
                    .Any(x => unwantedProgramStatus.Contains(x.CompletionStatus));

                if (completionPending)
                {
                    model.Message = "The applicant cannot apply for another program before completion of the current program";
                    return PartialView(model);
                }

            }
            model.IsValid = true;

            return PartialView(model);
        }

        [HttpPost]
        [Operation("Add Program Application")]
        public async Task<IActionResult> AddNewProgramApplication(AddNewProgramOfferApplicationCommand model)
        {
            if (!ModelState.IsValid)
            {
                model.ProgramSelectItem = await programSelectListLoader.GetSelectListAsync(null);
                return PartialView(model);
            }

            model.CreatedBy = User.Identity.Name;
            bool personalStatementSubmitted = model.StatementFileCommand.StatementFile != null;

            var result = await mediator.Send(model);

            if (result.Success && personalStatementSubmitted)
            {
                model.StatementFileCommand.ProgramOfferApplicationId = result.ProgramOfferApplicationId;
                await mediator.Send(model);
            }
                
            var message = result.Success ? "An error occured while creating a new program offer application" :
                                        "Program offer application created successfuly";

            return PartialView("ProgramApplicationResult", new Tuple<bool, string>(result.Success, message));
        }

        [Operation("Generate Resident Courses")]
        public async Task<IActionResult> GenerateResidentCourseWork(long Id,int offerId, int enrollmentId)
        {
            await mediator.Send(new GenerateResidentCourseWorkCommand
            {
                ProgramEnrollmentId = enrollmentId,
                ProgramOfferId = offerId,
                ResidentId = Id
            });

            return PartialView(nameof(GenerateResidentCourseWork),"Resident course details generation request is being processed");
        }

    }
}