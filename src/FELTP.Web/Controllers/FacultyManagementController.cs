﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Models;
using Application.Common.Models.Queries;
using Application.FacultyManagement.Commands;
using Application.FacultyManagement.Queries;
using Application.ProgramManagement.Queries;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using FELTP.Web.Services;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProgramManagement.Domain.Program;
using UserManagement.Services.Authorization;

namespace FELTP.Web.Controllers
{
    [Authorize]
    [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
    public class FacultyManagementController : Controller
    {
        IMediator mediator;
        IEnumerable<ISelectListLoader> selectListLoaders;

        public FacultyManagementController(IMediator _mediator, IEnumerable<ISelectListLoader> _selectListLoaders)
        {
             mediator = _mediator;
             selectListLoaders = _selectListLoaders;            
        }
        public IActionResult DashBoard()
        {
            return View();
        }

        [Operation("Add Faculty Role")]
        public IActionResult _AddFacultyRole()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Operation("Add Faculty Role")]
        public async Task<IActionResult> _AddFacultyRole(AddFacultyRoleCommand model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var roleId = await mediator.Send(model);
            return RedirectToAction(nameof(FacultyRoleList));

        }

        [Operation("Edit Faculty Role")]
        public async Task<IActionResult> _EditFacultyRole(int id)
        {
            var facultyrole = await mediator.Send(new GetFacultyRoleList() { Id = id });

            return PartialView(facultyrole.FirstOrDefault());
        }

        [HttpPost]
        [Operation("Edit Faculty Role")]
        public async Task<IActionResult> _EditFacultyRole(UpdateFacultyRoleCommand command)
        {
            var id = await mediator.Send(command);

            return RedirectToAction(nameof(FacultyRoleList));
        }

        [Operation("View Faculty Role Details")]
        public async Task<IActionResult> _DetailsFacultyRole(int id)
        {
            var facultyroledet = await mediator.Send(new GetFacultyRoleList() { Id = id });

            return PartialView(facultyroledet.FirstOrDefault());
        }

        public ActionResult _DeactivateFacultyRole(int id)
        {
            return PartialView("_DeactivateFacultyRole");

        }

        [HttpPost]
        public async Task<IActionResult> _DeactivateFacultyRole(DeactivateFacultyRoleCommand command)
        {
            if (!ModelState.IsValid)
            {
                command = await SetDeactivateFacultyRoleCommandModel(command.Id);
                return View(command);
            }

            command.DeactivatedBy = User.Identity.Name;

            var id = await mediator.Send(command);

            return RedirectToAction(nameof(FacultyRoleList));
        }

        private async Task<DeactivateFacultyRoleCommand> SetDeactivateFacultyRoleCommandModel(int roleId)
        {
            var deactivateFacultyRoleCommand = new DeactivateFacultyRoleCommand();

            var roleList = await mediator.Send(new GetFacultyRoleList { Id = roleId });

            var role = roleList.SingleOrDefault();

            return deactivateFacultyRoleCommand;
        }

        [Operation("View Faculty Role List")]
        public IActionResult FacultyRoleList()
        {
            return View();
        }

        [HttpPost]
        [Operation("View Faculty Role List")]
        public async Task<IActionResult> GetFacultyRoles(IDataTablesRequest request)
        {
            var facultyroles = await mediator.Send(new GetFacultyRoleList());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? facultyroles
                : facultyroles.Where(_item => _item.Name.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, facultyroles.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> GetFacultyRoleDropDown()
        {
            var roles = await mediator.Send(new GetFacultyRoleList());
            var selectListItems = roles.Select(
                x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                }).ToList();

            return Json(selectListItems);
        }

        [Operation("Add Faculty")]
        public ActionResult AddFaculty()
        {
            return View();
        }

        [HttpPost]
        [Operation("Add Faculty")]
        public async Task<IActionResult> AddFaculty(AddFacultyCommand command)
        {
            if (!ModelState.IsValid)
                return View(command);

            command.CreatedBy = User.Identity.Name;

            var facultyId = await mediator.Send(command, CancellationToken.None);

            if (facultyId == default(int))
                return View(command);

            return RedirectToAction(nameof(FacultyDetails), new { id = facultyId });
        }

        [Operation("Edit Faculty Bio Data")]
        public async Task<IActionResult> _EditFacultyBioData(int facultyId)
        {
            var faculty = await mediator.Send(new GetFacultyQuery() {  Id = facultyId });


            var updateFacultyCommand = new UpdateFacultyBioDataCommand
            {
                UpdatePersonBioData = new UpdatePersonBioDataCommand
                {
                    BioDataInfo = faculty.Person.BioDataInfo,
                    Cadres = await GetSelectListLoader(SelectItemType.Cadre).GetSelectListItemsAsync(null),
                },
                Roles = await GetSelectListLoader(SelectItemType.Role).GetSelectListItemsAsync(null),
                RoleId = faculty.Roles.Select(x => x.Id).ToList(),
                StaffNumber = faculty.StaffNumber,
                FacultyId = facultyId,
            };

            return PartialView(updateFacultyCommand);
        }


        [HttpPost]
        public async Task<IActionResult> _EditFacultyBioData(UpdateFacultyBioDataCommand command)
        {
           var id = await mediator.Send(command);

            return RedirectToAction(nameof(FacultyDetails), new { Id = command.FacultyId });
        }

        [Operation("Edit Faculty Contact Info")]
        public async Task<IActionResult> _EditFacultyContactInfo(long personId, int facultyId )
        {

            var person = await mediator.Send(new GetPersonQuery() { PersonId = personId });

            var model = new UpdatePersonContactInfoCommand
            {
                ContactInfo = person.ContactInfo,
                AddressInfo = person.AddressInfo,
                Countries = await GetSelectListLoader(SelectItemType.Country).GetSelectListItemsAsync(null),
                Counties = await GetSelectListLoader(SelectItemType.County).GetSelectListItemsAsync(person.AddressInfo.CountryId),
                Subcounties = await GetSelectListLoader(SelectItemType.Subcounty).GetSelectListItemsAsync(person.AddressInfo.CountyId),
                PersonId = personId,
                CorrelationId =facultyId
            };              

            return PartialView(model);
        }




        [HttpPost]
        public async Task<IActionResult> _EditFacultyContactInfo(UpdatePersonContactInfoCommand command)
        {
           var id = await mediator.Send(command);

            return RedirectToAction(nameof(FacultyDetails), new { Id = command.CorrelationId });

        }
        private async Task<List<SelectListItem>> GetProgramTierSelectList(string selected = "Select")
        {
            var programTiers = await mediator.Send(new GetProgramTierList());
            var tiers = programTiers.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString(),
                Selected = x.Name == selected
            }).ToList();

            return tiers;
        }

        public async Task<IActionResult> GetAssignedFacultyRoles(int facultyId)
        {
            var facultyRoles = await mediator.Send(new GetAssignedFacultyRoles { FacultyId = facultyId });
            return Json(facultyRoles.Select(x=>x.Id).ToList());
        }

        [Operation("View Faculty Details")]
        public async Task<IActionResult> FacultyDetails(int id)
        {
            var facultyViewModel = await mediator.Send(new GetFacultyQuery { Id = id });

            return View(facultyViewModel);
        }

        [Operation("View Faculty List")]
        public ActionResult FacultyList()
        {
            return View();
        }

        public async Task<IActionResult> GetFacultyDropDown()
        {
            var faculties = await mediator.Send(new GetFacultyForDropDownDisplayQuery { });

            return Json(faculties);
        }

        [HttpPost]
        [Operation("View Faculty List")]
        public async Task<IActionResult> GetFaculties(IDataTablesRequest request)
        {
            var faculties = await mediator.Send(new GetFacultyListQuery());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? faculties
                : faculties.Where(_item => _item.Person.BioDataInfo.IdentificationNumber.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, faculties.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }
        
        [Operation("View Faculty Examination Schedule")]
        public async Task<IActionResult> GetFacultyExaminationSchedule(IDataTablesRequest request, int facultyId)
        {
            var facultySchedule = await mediator.Send(new GetFacultyExaminationSchedule() { FacultyId = facultyId });

            var examinationSchedule = facultySchedule.Where(x => x.TimeTableType == TimetableType.Examination.ToString());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? examinationSchedule
                : examinationSchedule.Where(_item => _item.ExaminationName.ToLower().Contains(request.Search.Value.ToLower()));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, examinationSchedule.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        [Operation("View Faculty Course Schedule")]
        public async Task<IActionResult> GetFacultyCourseSchedule(IDataTablesRequest request, int facultyId)
        {
            var facultySchedule = await mediator.Send(new GetFacultyExaminationSchedule() { FacultyId = facultyId });
            var courseSchedule = facultySchedule.Where(x => x.TimeTableType == TimetableType.Unit.ToString());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? courseSchedule
                : courseSchedule.Where(_item => _item.UnitName.ToLower().Contains(request.Search.Value.ToLower()));
            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, courseSchedule.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        private ISelectListLoader GetSelectListLoader(SelectItemType selectItemType)
        {
            return selectListLoaders.Where(x => x.CanLoad(selectItemType)).SingleOrDefault();
        }

        [Operation("Activate Faculty")]
        public async Task<IActionResult> ActivateFaculty(int facultyId)
        {
            var faculty = await mediator.Send(new GetFacultyQuery { Id = facultyId });

            return PartialView(new ActivateFacultyCommand
            {
                FacultyId = facultyId,
                PersonnelNumber = faculty.StaffNumber,
                Name = faculty.Person.BioDataInfo.Name
            });
        }

        [HttpPost]
        [Operation("Activate Faculty")]
        public async Task<IActionResult> ActivateFaculty(ActivateFacultyCommand model)
        {
            if (!ModelState.IsValid)
                return PartialView(model);

            model.ActivatedBy = User.Identity.Name;
            var result = await mediator.Send(model);

            return PartialView("FacultyActivationDeActivationResult", result);
        }


        [Operation("Deactivate Faculty")]
        public async Task<IActionResult> DeactivateFaculty(int facultyId)
        {
            var faculty = await mediator.Send(new GetFacultyQuery { Id = facultyId });
            return PartialView(new DeactivateFacultyCommand
            {
                FacultyId = facultyId,
                PersonnelNumber = faculty.StaffNumber,
                Name = faculty.Person.BioDataInfo.Name
            });
        }

        [HttpPost]
        [Operation("Deactivate Faculty")]
        public async Task<IActionResult> DeactivateFaculty(DeactivateFacultyCommand model)
        {
            if (!ModelState.IsValid)
                return PartialView(model);

            model.DeactivatedBy = User.Identity.Name;
            var result = await mediator.Send(model);

            return PartialView("DeactivateFacultyResult",result);
        }

    }
}