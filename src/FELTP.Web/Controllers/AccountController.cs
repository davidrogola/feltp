using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UserManagement.Domain;
using UserManagement.Services;
using UserManagement.ViewModels;

namespace FELTP.Web.Controllers
{
    public class AccountController : Controller
    {
        IUserAuthenticationService userAuthenticationService;
        public AccountController(IUserAuthenticationService _userAuthenticationService)
        {
            userAuthenticationService = _userAuthenticationService;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Login(string ReturnUrl)
        {
            return View(new LogInViewModel() { ReturnUrl = ReturnUrl });
        }

        [HttpPost]
        public async Task<IActionResult> Login(LogInViewModel loginModel)
        {

            if (!ModelState.IsValid)
            {
                loginModel.Password = string.Empty;
                return View(loginModel);
            }

            var signInResult = await userAuthenticationService.SignInUser(loginModel);

            if (signInResult.ApplicationUser == null)
            {
                ModelState.AddModelError("InvalidUserNameOrPassword", "Invalid user name or password");
                return View(loginModel);
            }
            if (signInResult.ApplicationUser.UserStatus == ApplicationUserStatus.UnConfirmed)
            {
                ModelState.AddModelError("EmailConfirmation", "Please confirm your email before log in");
                return View(loginModel);
            }

            if (signInResult.SignInResult.Succeeded)
            {
                if (Url.IsLocalUrl(loginModel.ReturnUrl))
                {
                    return Redirect(loginModel.ReturnUrl);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else if (signInResult.SignInResult.IsLockedOut)
            {
                return View("AccountLockedOut");
            }
            else
            {
                ModelState.AddModelError("InvalidUserNameOrPassword", "Invalid user name or password");
                loginModel.Password = string.Empty;
                return View(loginModel);
            }
        }


        public IActionResult AccessDenied()
        {
            return View();
        }

        public IActionResult Logout()
        {
            userAuthenticationService.SignOutUser();
            return RedirectToAction("Login");
        }
    }
}