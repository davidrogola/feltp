﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application;
using Application.Common.Services;
using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using Application.ProgramManagement.QueryHandlers;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using FELTP.Web.Services;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using UserManagement.Services.Authorization;

namespace FELTP.Web.Controllers
{
    [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
    [Authorize]
    public class CourseManagementController : Controller
    {
        IMediator mediator;
        IEnumerable<ISelectListLoader> selectListLoaders;

        public CourseManagementController(IMediator _mediator, IEnumerable<ISelectListLoader> _selectListLoaders)
        {
            mediator = _mediator;
            selectListLoaders = _selectListLoaders;
        }

        public IActionResult DashBoard()
        {
            return View();
        }

        [Operation("Add Course")]
        public async Task<IActionResult> AddCourse()
        {
            var categorySelectItem = await GetSelectListLoader(SelectItemType.Category).GetSelectListAsync(null);

            var addCourse = new AddCourseCommand()
            {
                CategorySelectList = categorySelectItem
            };
            return View(addCourse);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Operation("Add Course")]
        public async Task<IActionResult> AddCourse(AddCourseCommand model)
        {
            if (!ModelState.IsValid)
            {
                model.CategorySelectList = await GetSelectListLoader(SelectItemType.Category).GetSelectListAsync(null);
                return View(model);
            }

            model.CreatedBy = User.Identity.Name;
            int id = await mediator.Send(model);

            return RedirectToAction(nameof(CourseList));
        }

        [Operation("Edit Course")]
        public async Task<IActionResult> EditCourse(int id)
        {
            var updateCourseCommand = await SetEditCourseCommandModel(id);

            return PartialView(updateCourseCommand);
        }

        [HttpPost]
        [Operation("Edit Course")]
        public async Task<IActionResult> EditCourse(UpdateCourseCommand command)
        {
            if (!ModelState.IsValid)
            {
                command = await SetEditCourseCommandModel(command.Id);
                return View(command);
            }

            command.UpdatedBy = User.Identity.Name;

            var id = await mediator.Send(command);

            return PartialView("EditCourseResult");
        }

        private async Task<UpdateCourseCommand> SetEditCourseCommandModel(int courseId)
        {
            var updateCourseCommand = new UpdateCourseCommand();

            var courseList = await mediator.Send(new GetCourseList { Id = courseId });

            var course = courseList.SingleOrDefault();
            if (course != null)
            {
                updateCourseCommand.Name = course.Name;
                updateCourseCommand.CategorySelectList = await GetCategorySelectList(course.CourseTypeId, course.Category);
                updateCourseCommand.CategoryId = course.CategoryId;
            }
            var courseUnits = await mediator.Send(new GetCourseUnitsList { Id = courseId });

            if (courseUnits.Any())
            {
                ModelState.Clear();
                var selectedUnits = courseUnits.Where(x => x.Status == Status.Active.ToString()).Select(x => x.UnitId);

                var allUnits = await mediator.Send(new GetUnitList { });

                var filteredUnits = allUnits.Where(x => x.HasActivity == courseUnits.FirstOrDefault().HasActivity);

                var unitSelectList = filteredUnits
                    .Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString(),
                    }).ToList();

                updateCourseCommand.UnitSelectList = unitSelectList;
                updateCourseCommand.UnitId = selectedUnits.ToList();
                updateCourseCommand.Id = course.Id;

            }
            return updateCourseCommand;
        }
        public async Task<JsonResult> GetCategories(int? courseTypeId)
        {
            var categories = await GetSelectListLoader(SelectItemType.Category).GetSelectListAsync(courseTypeId);

            return Json(categories);
        }

        private async Task<SelectList> GetCategorySelectList(int? typeId, string selected = "Select")
        {
            var categories = await mediator.Send(new GetCategoryList() { CourseTypeId = typeId });

            var selectList = new SelectList(categories.OrderBy(x => x.CourseTypeId), "Id", "Name", selected, "CourseType");

            return selectList;
        }

        [Operation("View Course List")]
        public IActionResult CourseList()
        {

            return View();
        }

        [HttpPost]
        [Operation("View Course List")]
        public async Task<IActionResult> GetCourses(IDataTablesRequest request)
        {
            var courses = await mediator.Send(new GetCourseList());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? courses
                : courses.Where(_item => _item.Name.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);

            var response = DataTablesResponse.Create(request, courses.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }
        public IActionResult _ViewCourseUnit(int Id, string name)
        {
            return PartialView(new CourseUnitViewModel { CourseId = Id, Course = name });
        }


        public async Task<JsonResult> GetCourseTypes()
        {
            var courseTypes = await mediator.Send(new GetCourseTypeList());
            var selectListItems = courseTypes.Select(
                x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                }).ToList();

            return Json(selectListItems);
        }

        public async Task<JsonResult> GetCourses(int? categoryId)
        {
            var courseListQuery = new GetCourseList() { CategoryId = categoryId };

            var courses = await mediator.Send(courseListQuery);
            var coursesListItem = courses.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return Json(coursesListItem);
        }

        public IActionResult AddCourseUnit()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddCourseUnit(AddCourseUnitCommand unit)
        {
            if (!ModelState.IsValid)
                return View(unit);

            var courseUnitId = await mediator.Send(unit);

            return RedirectToAction(nameof(CourseUnitsList));
        }

        public async Task<IActionResult> GetCourseUnits(int? courseId)
        {
            var courseUnits = await mediator.Send(new GetCourseUnitsList { Id = courseId });

            var selectItems = courseUnits.Select(x => new SelectListItem
            {
                Text = x.Unit,
                Value = x.UnitId.ToString()
            });

            return Json(selectItems);
        }

        [HttpPost]
        public async Task<IActionResult> GetCourseUnits(int? courseId, IDataTablesRequest request)
        {
            var courseModules = await mediator.Send(new GetCourseUnitsList() { Id = courseId });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? courseModules
                : courseModules.Where(_item => _item.Course.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);

            var response = DataTablesResponse.Create(request, courseModules.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> GetProgramCoursesCount(int programId)
        {
            var programCourses = await mediator.Send(new GetProgramCoursesList { ProgramId = programId });

            var courseIds = programCourses.Select(x => x.CourseId).Distinct().ToArray();

            var didacticCoursesCount = programCourses.Count(x => x.CourseType == CourseTypeEnum.Didactic.ToString());
            var fieldPlacementCourseCount =
                programCourses.Count(x => x.CourseType == "Field Placement");

            return Json(new { DidacticCourseCount = didacticCoursesCount, FieldPlacementCourseCount = fieldPlacementCourseCount });
        }


        public IActionResult CourseUnitsList()
        {
            return View();
        }

        public ActionResult _AddCourseType()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Operation("Add Course Type")]
        public async Task<ActionResult> _AddCourseType(AddCourseTypeCommand addCourseType)
        {
            if (!ModelState.IsValid)
            {
                return View(addCourseType);
            }
            int CourseTypeId = await mediator.Send(addCourseType);
            return RedirectToAction(nameof(CourseTypeList));
        }

        [Operation("Edit Course Type")]
        public async Task<IActionResult> _EditCourseType(int id)
        {
            var CourseType = await mediator.Send(new GetCourseTypeList() { Id = id });

            return PartialView(CourseType.FirstOrDefault());
        }

        [HttpPost]
        [Operation("Edit Course Type")]
        public async Task<IActionResult> _EditCourseType(UpdateCourseTypeCommand command)
        {
            var id = await mediator.Send(command);

            return RedirectToAction(nameof(CourseTypeList));
        }

        [Operation("View Course Type Details")]
        public async Task<IActionResult> _DetailsCourseType(int id)
        {
            var CourseType = await mediator.Send(new GetCourseTypeList() { Id = id });

            return PartialView(CourseType.FirstOrDefault());
        }

        [Operation("Deactivate Course Type")]
        public ActionResult _DeleteCourseType(int id)
        {
            return PartialView("_DeleteCourseType");

        }

        [Operation("View Course Type List")]
        public IActionResult CourseTypeList()
        {
            return View();
        }

        [HttpPost]
        [Operation("View Course Type List")]
        public async Task<IActionResult> GetCourseTypes(IDataTablesRequest request)
        {
            var coursetypes = await mediator.Send(new GetCourseTypeList());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? coursetypes
                : coursetypes.Where(_item => _item.Name.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, coursetypes.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        [Operation("Add Category")]
        public ActionResult _AddCategory()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Operation("Add Category")]
        public async Task<ActionResult> _AddCategory(AddCategoryCommand addCategory)
        {
            if (!ModelState.IsValid)
            {
                return View(addCategory);
            }

            int CategoryId = await mediator.Send(addCategory);
            return RedirectToAction(nameof(CategoryList));
        }

        [Operation("Edit Category")]
        public async Task<IActionResult> _EditCategory(int id)
        {
            var command = await SetEditCategoryCommandModel(id);

            return PartialView(command);
        }

        [HttpPost]
        [Operation("Edit Category")]
        public async Task<IActionResult> _EditCategory(UpdateCategoryCommand command)
        {
            if (!ModelState.IsValid)
            {
                command = await SetEditCategoryCommandModel(command.Id);
                return View(command);
            }

            command.CreatedBy = User.Identity.Name;
            var id = await mediator.Send(command);

            return RedirectToAction(nameof(CategoryList));
        }

        private async Task<UpdateCategoryCommand> SetEditCategoryCommandModel(int categoryId)
        {
            var categoryList = await mediator.Send(new GetCategoryList { Id = categoryId });

            var command = categoryList.Select(x => new UpdateCategoryCommand
            {
                Id = x.Id,
                Name = x.Name,
                CourseType = x.CourseType,
                CourseTypeId = x.CourseTypeId,
                CreatedBy = User.Identity.Name

            }).SingleOrDefault();

            command.Categories = await GetCategotySelectList(command.CourseType);

            return command;
        }

        private async Task<List<SelectListItem>> GetCategotySelectList(string selected = "Select")
        {
            var categories = await mediator.Send(new GetCourseTypeList());
            var cats = categories.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString(),
                Selected = x.Name == selected
            }).ToList();

            return cats;
        }

        [Operation("View Category Details")]
        public async Task<IActionResult> _DetailCategory(int id)
        {
            var Categorydet = await mediator.Send(new GetCategoryList() { Id = id });

            return PartialView(Categorydet.FirstOrDefault());
        }

        [Operation("Deactivate Category")]
        public ActionResult DeleteCategory(int id)
        {
            return PartialView("_DeleteCategory");

        }

        [HttpPost]
        [Operation("View Category List")]
        public async Task<IActionResult> GetCategories(IDataTablesRequest request)
        {
            var categories = await mediator.Send(new GetCategoryList());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? categories
                : categories.Where(_item => _item.Name.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, categories.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        [Operation("View Category List")]
        public ActionResult CategoryList()
        {
            return View();
        }

        public async Task<IActionResult> GetProgramCourse(int id)
        {
            var programCourses = await mediator.Send(new GetProgramCoursesList { Id = id });
            return Json(programCourses.SingleOrDefault());
        }

        public IActionResult CalculateEndDate(string strStartDate, int duration)
        {
            var startDate = strStartDate.ToLocalDate();
            var endDate = startDate.CalculateEndDate(duration);
            return Json(new { EndDate = endDate.ToString("dd/MM/yyyy")});

        }

        public async Task<IActionResult> CoursesInSemester(int programId, int semesterId)
        {
            var programCourses = await mediator.Send(new GetProgramCoursesList { ProgramId = programId });

            var coursesInSemester = programCourses.Where(x => x.SemesterId == semesterId && x.Status == Status.Active.ToString())
                .Select(x => new SemesterCourseInfomation
                {
                    Semester = x.Semester,
                    DurationInWeeks = x.DurationInWeeks,
                    CourseTitle = x.CourseName,
                    TotalDuration = +x.DurationInWeeks
                }).ToList();

            return PartialView(coursesInSemester);
        }

        public async Task<IActionResult> _GetFilteredAcademicGradesDropdown(string educationLevels)
        {
            if (String.IsNullOrEmpty(educationLevels))
            {
                return PartialView(new AddProgramOfferCommand()
                {
                    AcademicRequirementDetails = new AddProgramAcademicRequirementCommand()
                });
            }             

            var eductaionLevelsArr = educationLevels.Split(",").Select(x => x.Trim());

            var academicGrades = await mediator.Send(new GetAcademicCourseGradingQuery());

            var selectedGrades = academicGrades.Where(x => eductaionLevelsArr.Contains(x.EducationLevel)).ToList();

            var model = new AddProgramOfferCommand()
            {
                AcademicRequirementDetails = new AddProgramAcademicRequirementCommand()
                {
                    CourseGrades = new SelectList(selectedGrades.Select(x => new
                    {
                        x.Id,
                        x.EducationLevel,
                        x.GradeName,
                        EducationLevelToGradeMapping = $"{x.EducationLevel} : {x.Id}"
                    }), "EducationLevelToGradeMapping", "GradeName", null, "EducationLevel")
                }
            };
            return PartialView(model);
        }

        public async Task<List<SelectListItem>> GetUniversityCoursesDropDown()
        {
            var coursesSelectItems = await GetSelectListLoader(SelectItemType.AcademicCourse).GetSelectListItemsAsync(null);

            return coursesSelectItems;
        }

        private ISelectListLoader GetSelectListLoader(SelectItemType selectItemType)
        {
            return selectListLoaders.Where(x => x.CanLoad(selectItemType)).SingleOrDefault();
        }
    }
}