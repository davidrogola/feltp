﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using FELTP.Web.Services;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Application;
using UserManagement.Services.Authorization;

namespace FELTP.Web.Controllers
{
    public class CollaboratorController : Controller
    {
        IMediator mediator;
        ISelectListLoader programSelectListLoader;
        IEnumerable<ISelectListLoader> selectListLoaders;

        public CollaboratorController(IMediator _mediator, IEnumerable<ISelectListLoader> _selectListLoaders)
        {
            mediator = _mediator;
            selectListLoaders = _selectListLoaders;
            programSelectListLoader = selectListLoaders.Where(x => x.CanLoad(SelectItemType.Program)).SingleOrDefault();
        }
        public IActionResult Index()
        {
            return View();
        }

        [Operation("Add Collaborator")]
        public async Task<ActionResult> _AddCollaborator()
        {
            var addCollaborator = new AddCollaboratorCommand()
            {
                ProgramSelectList = await programSelectListLoader.GetSelectListAsync(null)
            };
            return PartialView(addCollaborator);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Operation("Add Collaborator")]
        public async Task<ActionResult> _AddCollaborator(AddCollaboratorCommand addCollaborator)
        {
            if (!ModelState.IsValid)
            {
                return View(addCollaborator);
            }

            addCollaborator.CreatedBy = User.Identity.Name;
            int CollaboratorId = await mediator.Send(addCollaborator);
            return RedirectToAction(nameof(CollaboratorList));
        }

        [Operation("Edit Collaborator")]
        public async Task<IActionResult> _EditCollaborator(int id)
        {
            var collaborator = await mediator.Send(new GetCollaboratorList() { Id = id });

            var model = collaborator.Select(x => new UpdateCollaboratorCommand
            {
                Id = x.Id,
                Name = x.Name,
                ProgramId = x.ProgramId,
                CollaboratorTypeId = x.CollaboratorTypeId,
            }).SingleOrDefault();
            model.Programs = await GetSelectListLoader(SelectItemType.Program).GetSelectListItemsAsync(null);

            return PartialView(model);

        }

        [HttpPost]
        [Operation("Edit Collaborator")]
        public async Task<IActionResult> _EditCollaborator(UpdateCollaboratorCommand command)
        {
            command.UpdatedBy = User.Identity.Name;
            var id = await mediator.Send(command);
            return RedirectToAction(nameof(CollaboratorList));
        }

       public async Task<JsonResult> GetPrograms(int? Id)
        {
            var programs = await programSelectListLoader.GetSelectListAsync(Id);

            return Json(programs);
        }

        private async Task<SelectList> GetProgramSelectList(int? id, string selected = "Select")
        {
            var programs = await mediator.Send(new GetProgramList() { Id = id });

            var selectList = new SelectList(programs.OrderBy(x => x.Id), "Id", "Name", selected, "Program");

            return selectList;
        }

        [Operation("Edit Collaborator")]
        public async Task<IActionResult> _CollaboratorDetail(int id)
        {
            var Collaboratordet = await mediator.Send(new GetCollaboratorList() { Id = id });

            return View(Collaboratordet.FirstOrDefault());
        }

        [Operation("Deactivate Collaborator")]
        public ActionResult _Deactivate(int id)
        {
            return PartialView("_Deactivate");

        }

        [HttpPost]
        [Operation("View Collaborator List")]
        public async Task<IActionResult> GetCollaborators(IDataTablesRequest request)
        {
            var Collaborators = await mediator.Send(new GetCollaboratorList());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? Collaborators
                : Collaborators.Where(_item => _item.Name.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, Collaborators.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        [Operation("View Collaborator List")]
        public ActionResult CollaboratorList()
        {
            return View();
        }


        public IActionResult _ViewProgramCollaborator(int Id, string name)
        {
            return PartialView(new ProgramCollaboratorViewModel { CollaboratorId = Id, CollaboratorName = name });
        }

        public async Task<IActionResult> GetProgramCollaborators(int? collaboratorId)
        {
            var programCollaborators = await mediator.Send(new GetProgramCollaboratorList { Id = collaboratorId, GetBy = GetCollaboratorBy.CollaboratorId });

            var selectItems = programCollaborators.Select(x => new SelectListItem
            {
                Text = x.CollaboratorName,
                Value = x.Id.ToString()
            });

            return Json(selectItems);
        }
        [HttpPost]
        public async Task<IActionResult> GetProgramCollaborators(int? collaboratorId, IDataTablesRequest request)
        {
            var programCollaborators = await mediator.Send(new GetProgramCollaboratorList() { GetBy = GetCollaboratorBy.CollaboratorId, Id = collaboratorId });

            var activePrograms = programCollaborators.Where(x => x.Status == Status.Active.ToString());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? activePrograms
                : activePrograms.Where(_item => _item.CollaboratorName.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, programCollaborators.Count(), filteredData.Count(), dataPage);
            return new DataTablesJsonResult(response, true);
        }

        private ISelectListLoader GetSelectListLoader(SelectItemType selectItemType)
        {
            return selectListLoaders.SingleOrDefault(x => x.CanLoad(selectItemType));
        }
    }
}