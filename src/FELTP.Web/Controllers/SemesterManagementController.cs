﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application;
using Application.Common.Services;
using Application.FacultyManagement.Queries;
using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using Application.ProgramManagement.QueryHandlers;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using FELTP.Web.Services;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProgramManagement.Domain.Program;
using UserManagement.Services.Authorization;

namespace FELTP.Web.Controllers
{
    [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
    [Authorize]
    public class SemesterManagementController : Controller
    {
        IMediator mediator;
        ISelectListLoader programSelectListLoader;
        IEnumerable<ISelectListLoader> selectListLoaders;

        public SemesterManagementController(IMediator _mediator, IEnumerable<ISelectListLoader> _selectListLoaders)
        {
            mediator = _mediator;
            selectListLoaders = _selectListLoaders;
            programSelectListLoader = selectListLoaders.FirstOrDefault(x => x.CanLoad(SelectItemType.Program));
        }

        [Operation("Add Semester")]
        public IActionResult _AddSemester()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Operation("Add Semester")]
        public async Task<ActionResult> _AddSemester(AddSemesterCommand addSemester)
        {
            if (!ModelState.IsValid)
            {
                return View(addSemester);
            }

            int semesterId = await mediator.Send(addSemester);
            return RedirectToAction(nameof(SemesterList));
        }

        [Operation("Edit Semester")]
        public async Task<IActionResult> _EditSemester(int id)
        {
            var semester = await mediator.Send(new GetSemesterList() {Id = id});

            return PartialView(semester.FirstOrDefault());
        }

        [HttpPost]
        [Operation("Edit Semester")]
        public async Task<IActionResult> _EditSemester(UpdateSemesterCommand command)
        {
            var id = await mediator.Send(command);

            return PartialView("EditSemesterResult", "Semester details updated succesfully");
        }

        [Operation("View Semester Details")]
        public async Task<IActionResult> _DetailsSemester(int id)
        {
            var semester = await mediator.Send(new GetSemesterList() {Id = id});

            return PartialView(semester.FirstOrDefault());
        }


        [Operation("View Semester List")]
        public IActionResult SemesterList()
        {
            return View();
        }

        [HttpPost]
        [Operation("View Semester List")]
        public async Task<IActionResult> GetSemesters(IDataTablesRequest request)
        {
            var semesters = await mediator.Send(new GetSemesterList());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? semesters
                : semesters.Where(_item => _item.Name.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, semesters.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public async Task<JsonResult> GetSemesters()
        {
            var programTiers = await mediator.Send(new GetSemesterList());
            var tiers = programTiers.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return Json(tiers);
        }

        [Operation("Deactivate Semester")]
        public ActionResult _DeactivateSemester(int id)
        {
            return PartialView("_DeactivateSemester");
        }

        [HttpPost]
        [Operation("Deactivate Semester")]
        public async Task<IActionResult> _DeactivateSemester(DeactivateSemesterCommand command)
        {
            if (!ModelState.IsValid)
            {
                command = await SetDeactivateSemesterCommandModel(command.Id);
                return View(command);
            }

            command.DeactivatedBy = User.Identity.Name;

            var id = await mediator.Send(command);

            return RedirectToAction(nameof(SemesterList));
        }

        private async Task<DeactivateSemesterCommand> SetDeactivateSemesterCommandModel(int semesterId)
        {
            var deactivateSemesterCommand = new DeactivateSemesterCommand();

            var semesterList = await mediator.Send(new GetSemesterList {Id = semesterId});

            var semester = semesterList.SingleOrDefault();

            return deactivateSemesterCommand;
        }

        public async Task<IActionResult> CreateSemesterSchedule()
        {
            var model = new AddSemesterScheduleCommand
            {
                ProgramSelectList = await programSelectListLoader.GetSelectListAsync(null)
            };

            return View(model);
        }

        [HttpPost]
        [Operation("Add Semester Schedule")]
        public async Task<IActionResult> CreateSemesterSchedule(AddSemesterScheduleCommand model)
        {
            if (!ModelState.IsValid)
            {
                model.ProgramSelectList = await programSelectListLoader.GetSelectListAsync(null);
                return View(model);
            }

            model.CreatedBy = User.Identity.Name;

            var result = await mediator.Send(model);

            if (result.CreatedSuccesfully)
                return RedirectToAction(nameof(SemesterScheduleList));

            ModelState.AddModelError("Response", result.Message);

            return View(model);
        }

        [Operation("View Semester Schedule")]
        public IActionResult SemesterScheduleList()
        {
            return View();
        }

        [Operation("View Semester Schedule")]
        public async Task<IActionResult> GetSemesterScheduleList(IDataTablesRequest request)
        {
            var scheduleList = await mediator.Send(new GetSemesterScheduleList { });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? scheduleList
                : scheduleList.Where(_item => _item.Description.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, scheduleList.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> GetSemesterScheduleItemList(IDataTablesRequest request, int semesterScheduleId)
        {
            var scheduleList = await mediator.Send(new GetSemesterScheduleItemList
            {
                SemesterScheduleId = semesterScheduleId
            });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? scheduleList
                : scheduleList.Where(_item => _item.Name.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, scheduleList.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }


        [Operation("View Semester Schedule Details")]
        public async Task<IActionResult> SemesterScheduleDetails(int id)
        {
            var scheduleList = await mediator.Send(new GetSemesterScheduleList {Id = id});

            return View(scheduleList.SingleOrDefault());
        }

        [Operation("Add Unit")]
        public async Task<IActionResult> AddExaminationSchedule(int scheduleId, int programId, int semesterId)
        {
            var model = new AddSemesterScheduleItemCommand()
            {
                ProgramCourses = await GetProgramCourses(programId, semesterId, ScheduleItemType.Examination),
                ScheduleItemType = ScheduleItemType.Examination,
                SemesterScheduleId = scheduleId,
                ProgramId = programId,
                SemesterId = semesterId,
                SemesterSchedule = await GetSemesterSchedule(scheduleId),
            };

            return PartialView(model);
        }

        public async Task<IActionResult> CompleteSemesterRequest(int semesterScheduleId)
        {
            var semesterScheduleDetails = await mediator.Send(new GetSemesterScheduleList {Id = semesterScheduleId});

            var semesterSchedule = semesterScheduleDetails.SingleOrDefault();

            var completeSemesterModel = new CompleteSemesterCommand()
            {
                ScheduleName = semesterSchedule.Description,
                StartDate = semesterSchedule.EstimatedStartDate,
                EndDate = semesterSchedule.EstimatedEndDate,
                SemesterScheduleId = semesterScheduleId
            };

            return PartialView(completeSemesterModel);
        }

        [HttpPost]
        public async Task<IActionResult> CompleteSemesterRequest(CompleteSemesterCommand model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(model);
            }

            model.RequestedBy = User.Identity.Name;
            model.DateRequested = DateTime.Now;
            var result = await mediator.Send(model);

            if (result.CreatedSuccesfully)
                return PartialView("ScheduleResult", result);

            ModelState.AddModelError("Response", result.Message);
            return PartialView(model);
        }

        private async Task<SelectList> GetProgramCourses(int programId, int semesterId, ScheduleItemType scheduleType)
        {
            IEnumerable<ProgramCoursesViewModel> coursesInSemester = null;

            var programCourses = await mediator.Send(new GetProgramCoursesList {ProgramId = programId});

            var activeCourses = programCourses.Where(x => x.Status == Status.Active.ToString());

            switch (scheduleType)
            {
                case ScheduleItemType.Examination:
                case ScheduleItemType.Cat:
                    coursesInSemester = activeCourses.Where(x => x.SemesterId == semesterId 
                                                                 && x.CourseType == CourseTypeEnum.Didactic.ToString());
                    break;
                default:
                    coursesInSemester = activeCourses.Where(x => x.SemesterId == semesterId);
                    break;
            }

            return new SelectList(
                coursesInSemester.Select(x => new {CourseName = $"{x.CourseName} ({x.CourseType})", x.Id}).ToList(),
                "Id", "CourseName");
        }

        private async Task<SemesterScheduleDates> GetSemesterSchedule(int Id)
        {
            var result = await mediator.Send(new GetSemesterScheduleList {Id = Id});

            var schedule = result.SingleOrDefault();

            return new SemesterScheduleDates
            {
                StartDate = schedule.EstimatedStartDate,
                EndDate = schedule.EstimatedEndDate,
                Semester = schedule.Semester
            };
        }

        private async Task<List<SelectListItem>> GetFacultyDropDown()
        {
            var faculties = await mediator.Send(new GetFacultyForDropDownDisplayQuery());
            return faculties;
        }


        [Operation("Add Course Schedule")]
        public async Task<IActionResult> AddCourseSchedule(int scheduleId, int programId, int semesterId)
        {
            var model = new AddSemesterScheduleItemCommand()
            {
                ProgramCourses = await GetProgramCourses(programId, semesterId, ScheduleItemType.Course),
                ScheduleItemType = ScheduleItemType.Course,
                SemesterScheduleId = scheduleId,
                ProgramId = programId,
                SemesterId = semesterId,
                SemesterSchedule = await GetSemesterSchedule(scheduleId),
            };

            return PartialView(model);
        }

        [HttpPost]
        [Operation("Add Course Schedule")]
        public async Task<IActionResult> AddCourseSchedule(AddSemesterScheduleItemCommand model)
        {
            if (!ModelState.IsValid)
            {
                model.ProgramCourses =
                    await GetProgramCourses(model.ProgramId, model.SemesterId, ScheduleItemType.Course);
                model.SemesterSchedule = await GetSemesterSchedule(model.SemesterScheduleId);
                return PartialView(model);
            }

            model.CreatedBy = User.Identity.Name;

            var result = await mediator.Send(model);

            if (result.CreatedSuccesfully)
                return PartialView("ScheduleResult", result);

            ModelState.AddModelError("Response", result.Message);
            return PartialView(model);
        }

        public async Task<IActionResult> AddCatSchedule(int scheduleId, int programId, int semesterId)
        {
            var model = new AddSemesterScheduleItemCommand()
            {
                ScheduleItemType = ScheduleItemType.Cat,
                SemesterScheduleId = scheduleId,
                ProgramId = programId,
                SemesterId = semesterId,
                SemesterSchedule = await GetSemesterSchedule(scheduleId),
            };

            return PartialView(model);
        }
        
        [HttpPost]
        public async Task<IActionResult> AddCatSchedule(AddSemesterScheduleItemCommand model)
        {
            if (!ModelState.IsValid)
            {
                model.SemesterSchedule = await GetSemesterSchedule(model.SemesterScheduleId);
                return PartialView(model);
            }

            model.CreatedBy = User.Identity.Name;

            var result = await mediator.Send(model);

            if (result.CreatedSuccesfully)
                return PartialView("ScheduleResult", result);

            ModelState.AddModelError("Response", result.Message);
            return PartialView(model);
        }
        

        public IActionResult SemesterSchedulePendingApproval()
        {
            return View();
        }


        public async Task<IActionResult> GetSemesterSchedulePendingApproval(IDataTablesRequest request)
        {
            var scheduleList = await mediator.Send(new GetSemesterScheduleList());

            var scheduleListPendingApproval = scheduleList.Where(x => x.CompletionRequestDate != null)
                .Where(x => x.Status != ProgressStatus.Completed.ToString())
                .Where(x => x.ApprovalStatus == ApprovalStatus.Pending)
#if !DEBUG
                .Where(c => !c.CompletionRequestedBy.Equals(User.Identity.Name))
#endif
                .ToList();

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? scheduleListPendingApproval
                : scheduleListPendingApproval.Where(_item => _item.Description.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, scheduleListPendingApproval.Count(), filteredData.Count(),
                dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> ApproveSemesterCompletionRequest(int Id)
        {
            var result = await mediator.Send(new GetSemesterScheduleList {Id = Id});

            var schedule = result.SingleOrDefault();
            var model = new ApproveSemesterCompletionCommand
            {
                ScheduleName = schedule.Description,
                StartDate = schedule.EstimatedStartDate,
                EndDate = schedule.EstimatedEndDate,
                RequestReason = schedule.CompletionRequestReason,
                RequestedBy = schedule.CompletionRequestedBy,
                SemesterScheduleId = schedule.Id
            };
            return PartialView(model);
        }

        [HttpPost]
        public async Task<IActionResult> ApproveSemesterCompletionRequest(ApproveSemesterCompletionCommand model)
        {
            if (!ModelState.IsValid)
                return PartialView(model);

            model.ApprovedBy = User.Identity.Name;
            var result = await mediator.Send(model);

            if (result.CreatedSuccesfully)
                return PartialView("SemesterCompletionResult", result);

            ModelState.AddModelError("Response", result.Message);
            return PartialView(model);
        }

        [Operation("Add Course Schedule Timetable")]
        public async Task<IActionResult> CreateCourseTimetable(int Id, int courseId)
        {
            var scheduleItems = await mediator.Send(new GetSemesterScheduleItemList {Id = Id});

            var scheduleItem = scheduleItems.SingleOrDefault();


            var model = new AddScheduleItemTimetableCommand
            {
                StartDate = scheduleItem.StartDate,
                EndDate = scheduleItem.EndDate,
                EndDateLimit = scheduleItem.EndDate.ToLocalDate().AddDays(1).ToString("dd/MM/yyyy"),
                TimetableItem = await GetCourseTimetableItem(courseId),
                ScheduleItemId = Id,
                ScheduleItemName = scheduleItem.Name,
                FacultySelectList = await GetFacultyDropDown(),
                SemesterScheduleId = scheduleItem.SemesterScheduleId,
                TimetableType = TimetableType.Unit,
                ProgramCourseId = courseId
            };
            return View(model);
        }

        [HttpPost]
        [Operation("Add Course Schedule Timetable")]
        public async Task<IActionResult> CreateCourseTimetable(AddScheduleItemTimetableCommand model)
        {
            if (!ModelState.IsValid)
            {
                model.FacultySelectList = await GetFacultyDropDown();
                model.TimetableItem = await GetCourseTimetableItem(model.ProgramCourseId);
                return View(model);
            }

            model.TimetableType = TimetableType.Unit;
            model.CreatedBy = User.Identity.Name;

            var result = await mediator.Send(model);

            return RedirectToAction(nameof(CreateCourseTimetable),
                new {Id = result.ScheduleItemId, courseId = model.ProgramCourseId});
        }


        private async Task<TimetableItemCommand> GetCourseTimetableItem(int courseId)
        {
            return new TimetableItemCommand
            {
                Units = await GetCourseUnitsSelectList(courseId)
            };
        }

        private async Task<List<SelectListItem>> GetCourseUnitsSelectList(int courseId)
        {
            var programCourse = await mediator.Send(new GetProgramCoursesList {Id = courseId});

            var courseUnits =
                await mediator.Send(new GetCourseUnitsList {Id = programCourse.SingleOrDefault().CourseId});

            var unitDropDown = courseUnits.Select(x => new SelectListItem
            {
                Text = x.UnitName,
                Value = x.UnitId.ToString()
            }).ToList();

            return unitDropDown;
        }


        [Operation("View Course Schedule Timetable Details")]
        public async Task<IActionResult> ViewCourseTimetable(int id, int semesterScheduleId)
        {
            var scheduleItem = await mediator.Send(new GetSemesterScheduleItemList {Id = id});

            return View(new TimetableViewModel
            {
                ScheduleItemName = scheduleItem.SingleOrDefault().Name,
                SemesterScheduleItemId = id,
                SemesterScheduleId = semesterScheduleId
            });
        }


        [Operation("View Course Schedule Timetable Details")]
        public async Task<IActionResult> GetCourseTimetableList(IDataTablesRequest request, int scheduleItemId,
            int unitId)
        {
            var timetable = await mediator.Send(new GetTimetableListQuery
            {
                SemesterScheduleItemId = scheduleItemId
            });

            var unitSchedules = timetable.Where(x => x.UnitId == unitId).ToList();

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? unitSchedules
                : unitSchedules.Where(_item => _item.UnitName.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, unitSchedules.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }
        //todo: Mwasi Add role for viewing full timetable details
        //[Operation("View Full Timetable Details")]
        public async Task<IActionResult> GetFullTimetableList(IDataTablesRequest request, int scheduleItemId)
        {
            var timetable = await mediator.Send(new GetTimetableListQuery
            {
                SemesterScheduleItemId = scheduleItemId
            });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? timetable
                : timetable.Where(_item => _item.UnitName.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, timetable.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> GetCourseTimetableByScheduleItem(IDataTablesRequest request,
            int scheduleItemId)
        {
            var timetable = await mediator.Send(new GetTimetableListQuery
            {
                SemesterScheduleItemId = scheduleItemId
            });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? timetable
                : timetable.Where(_item => _item.UnitName.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, timetable.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> GetUnitsInTimetable(IDataTablesRequest request, int scheduleItemId)
        {
            var timetable = await mediator.Send(new GetTimetableListQuery
            {
                SemesterScheduleItemId = scheduleItemId
            });

            var units = timetable.GroupBy(x => x.UnitId)
                .Select(u => u.FirstOrDefault()).ToList();

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? units
                : units.Where(_item => _item.UnitName.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, units.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> GetUnitsInTimetableDropDown(int scheduleItemId)
        {
            var timetable = await mediator.Send(new GetTimetableListQuery
            {
                SemesterScheduleItemId = scheduleItemId
            });

            var units = timetable.GroupBy(x => x.UnitId)
                .Select(u => u.FirstOrDefault()).ToList();

            return Json(units.Select(x => new SelectListItem
            {
                Text = x.UnitName,
                Value = x.UnitId.ToString()
            }));
        }

        public async Task<IActionResult> _ViewCourseTimetableSchedule(int unitId, int semesterScheduleItemId,
            string unitName)
        {
            var scheduleItem = await mediator.Send(new GetSemesterScheduleItemList {Id = semesterScheduleItemId});

            return PartialView(new TimetableViewModel
            {
                ScheduleItemName = scheduleItem.SingleOrDefault().Name,
                SemesterScheduleItemId = semesterScheduleItemId,
                SemesterScheduleId = scheduleItem.SingleOrDefault().SemesterScheduleId,
                UnitId = unitId,
                UnitName = unitName
            });
        }

        public async Task<IActionResult> GetSemesterScheduleDropDown(int programOfferId)
        {
            var result = await mediator.Send(new GetProgramOfferSemesterSchedule {ProgramOfferId = programOfferId});

            var selectDropDown = result.Where(s=>s.Status == ProgressStatus.InProgress.ToString())
                .Select(x => new SelectListItem
            {
                Text = x.Description,
                Value = x.Id.ToString()
            }).ToList();

            return Json(selectDropDown);
        }

        public async Task<IActionResult> GetSemesterScheduleItemDropDown(int semesterScheduleId)
        {
            var semesterScheduleItems = await mediator.Send(new GetSemesterScheduleItemList
            {
                SemesterScheduleId = semesterScheduleId
            });

            var courseScheduleSelectList = semesterScheduleItems
                .Where(x => x.ScheduleItemType == ScheduleItemType.Course.ToString())
                .Select(x => new SelectListItem
                {
                    Text = $"{x.Name}-{x.CourseType}",
                    Value = x.Id.ToString()
                }).ToList();

            return Json(courseScheduleSelectList);
        }

        public async Task<IActionResult> GetUnitAttedanceDates(int unitId, int scheduleItemId)
        {
            var timetable = await mediator.Send(new GetTimetableListQuery
            {
                SemesterScheduleItemId = scheduleItemId
            });

            var attendanceDates = timetable.Where(x => x.UnitId == unitId).Select(x => new {Date = x.StartDate})
                .Distinct().ToList();

            var selectList = attendanceDates.Select(x => new SelectListItem {Text = x.Date, Value = x.Date});

            return Json(selectList);
        }

        public async Task<IActionResult> GetUnitTimeSlotsForDate(int unitId, int scheduleItemId, string attendanceDate)
        {
            var timetable = await mediator.Send(new GetTimetableListQuery
            {
                SemesterScheduleItemId = scheduleItemId
            });

            var unitSchedules = timetable.Where(x => x.UnitId == unitId && x.StartDate == attendanceDate).ToList();

            var attendanceSchedules = unitSchedules
                .Select(x => new SelectListItem
                {
                    Text = $"{x.Faculty} ({x.StartTime}-{x.EndTime})",
                    Value = $" {x.FacultyId.ToString()}-{x.Id.ToString()}"
                }).ToList();

            return Json(attendanceSchedules);
        }

        public async Task<IActionResult> GetProgramSemesters(int programId)
        {
            var programCourses = await mediator.Send(new GetProgramCoursesList {ProgramId = programId});

            var semestersSelectList = programCourses.OrderBy(x => x.SemesterId)
                .GroupBy(x => x.SemesterId)
                .Select(x => x.FirstOrDefault())
                .Select(x => new SelectListItem
                {
                    Value = x.SemesterId.ToString(),
                    Text = x.Semester
                }).ToList();

            return Json(semestersSelectList);
        }


        public async Task<IActionResult> AdjustSemesterSchedule(int semesterScheduleId)
        {
            var result = await mediator.Send(new GetSemesterScheduleList {Id = semesterScheduleId});

            var schedule = result.SingleOrDefault();

            var programOffer = await mediator.Send(new GetProgramOfferById {OfferId = schedule.ProgramOfferId});


            var model = new InitiateSemesterScheduleChangeRequest
            {
                ScheduleName = schedule.Description,
                StartDate = schedule.EstimatedStartDate,
                EndDate = schedule.EstimatedEndDate,
                RequestedBy = schedule.CompletionRequestedBy,
                SemesterScheduleId = schedule.Id,
                ProgramOfferId = schedule.ProgramOfferId,
                ProgramOffer = schedule.ProgramOffer,
                Semester = schedule.Semester,
                OfferStartDate = programOffer.StrStartDate,
                OfferEndDate = programOffer.StrEndDate,
                Duration = await GetSemesterDuration(programOffer.ProgramId, schedule.SemesterId),
                Status = (ProgressStatus) Enum.Parse(typeof(ProgressStatus), schedule.Status)
            };
            return PartialView(model);
        }

        [HttpPost]
        public async Task<IActionResult> AdjustSemesterSchedule(InitiateSemesterScheduleChangeRequest model)
        {
            if (!ModelState.IsValid)
                return PartialView(model);

            model.RequestedBy = User.Identity.Name;
            var result = await mediator.Send(model);

            return PartialView("AdjustSemesterScheduleResponse", result);
        }

        public IActionResult SemesterScheduleChangeRequests()
        {
            return View();
        }


        public async Task<IActionResult> SemesterScheduleChangeRequestPendingApproval(IDataTablesRequest request)
        {
            var semesterScheduleChangeRequests = await mediator.Send(new GetSemesterScheduleChangeRequestQuery { });

            var requestPendingApproval = semesterScheduleChangeRequests
                .Where(x => x.ApprovalStatus == ApprovalStatus.Pending.ToString())
#if !DEBUG
                .Where(c => !c.RequestedBy.Equals(User.Identity.Name))
#endif
                .AsEnumerable();


            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? requestPendingApproval
                : requestPendingApproval.Where(_item =>
                    _item.ScheduleName.ToLowerInvariant().Contains(request.Search.Value.ToLowerInvariant()));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response =
                DataTablesResponse.Create(request, requestPendingApproval.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        private async Task<int> GetSemesterDuration(int programId, int semesterId)
        {
            var programCourses = await mediator.Send(new GetProgramCoursesList {ProgramId = programId});

            var totalDuration = programCourses
                .Where(x => x.SemesterId == semesterId && x.Status == Status.Active.ToString())
                .Sum(x => x.DurationInWeeks);

            return totalDuration;
        }

        public async Task<IActionResult> ApproveSemesterScheduleChangeRequest(int changeRequestId)
        {
            var scheduleChangeRequestList = await mediator.Send(new GetSemesterScheduleChangeRequestQuery
                {Id = changeRequestId});
            var scheduleChangeRequest = scheduleChangeRequestList.SingleOrDefault();

            var approveSemesterScheduleChangeRequest = new ApproveSemesterChangeRequest()
            {
                StartDate = scheduleChangeRequest.StartDate,
                EndDate = scheduleChangeRequest.EndDate,
                ChangeRequestId = scheduleChangeRequest.Id,
                ScheduleName = scheduleChangeRequest.ScheduleName,
                OriginalStartDate = scheduleChangeRequest.OriginalStartDate,
                OriginalEndDate = scheduleChangeRequest.OriginalEndDate,
                SemesterScheduleId = scheduleChangeRequest.SemesterScheduleId,
                Comment = scheduleChangeRequest.Comment,
                RequestedBy = scheduleChangeRequest.RequestedBy
            };

            return PartialView(approveSemesterScheduleChangeRequest);
        }

        [HttpPost]
        public async Task<IActionResult> ApproveSemesterScheduleChangeRequest(ApproveSemesterChangeRequest model)
        {
            if (!ModelState.IsValid)
                return PartialView(model);

            model.ApprovedBy = User.Identity.Name;
            var result = await mediator.Send(model);

            return PartialView("AdjustSemesterScheduleResponse", result);
        }

        public async Task<IActionResult> _EditTimetable(int id)
        {
            var model = await LoadTimetableItemModel(id);
            return PartialView(model);
        }

        [HttpPost]
        public async Task<IActionResult> _EditTimetable(UpdateScheduleItemTimetableCommand model)
        {
            if (!ModelState.IsValid)
            {
                model = await LoadTimetableItemModel(model.Id);
                return PartialView(model);
            }

            var result = await mediator.Send(model);

            return PartialView("_EditTimetableResult", result);
        }

        private async Task<UpdateScheduleItemTimetableCommand> LoadTimetableItemModel(int id)
        {
            var timetableItem = await mediator.Send(new GetTimetableItemQuery {Id = id});

            var model = new UpdateScheduleItemTimetableCommand()
            {
                Date = timetableItem.StartDate,
                StartTime = timetableItem.StartTime,
                EndTime = timetableItem.EndTime,
                TimetableType = timetableItem.TimeTableType,
                ExaminationId = timetableItem.ExaminationId,
                UnitId = timetableItem.UnitId,
                FacultyId = timetableItem.FacultyId,
                UnitName = timetableItem.UnitId.HasValue ? timetableItem.UnitName : timetableItem.ExaminationName,
                StartDate = timetableItem.ScheduleStartDate,
                EndDate = timetableItem.ScheduleEndDate,
                FacultySelectList = await GetSelectListLoader(SelectItemType.Faculty).GetSelectListItemsAsync(null),
                Code = timetableItem.Code,
                ScheduleItemId = timetableItem.SemesterScheduleItemId,
                SemesterScheduleId = timetableItem.SemesterScheduleId
            };
            return model;
        }


        private ISelectListLoader GetSelectListLoader(SelectItemType selectItemType)
        {
            return selectListLoaders.Where(x => x.CanLoad(selectItemType)).SingleOrDefault();
        }
    }
}