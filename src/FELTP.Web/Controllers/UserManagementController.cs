﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Common.Notifications;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UserManagement.Domain;
using UserManagement.Services.Authorization;
using UserManagement.ViewModels;
using System.Security.Claims;
using Common.Domain.Messaging;

namespace FELTP.Web.Controllers
{
    [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
    [Authorize]
    public class UserManagementController : Controller
    {
        UserManager<ApplicationUser> userManager;
        SignInManager<ApplicationUser> signInManager;
        IMediator mediator;
        ApplicationDbContext identityDbContext;


        public UserManagementController(UserManager<ApplicationUser> _userManager,
            SignInManager<ApplicationUser> _signInManager, IMediator _mediator,
            ApplicationDbContext _identityDbContext)
        {
            userManager = _userManager;
            signInManager = _signInManager;
            mediator = _mediator;
            identityDbContext = _identityDbContext;

        }

        public IActionResult DashBoard()
        {
            return View();
        }

        [Operation("Add User")]
        public IActionResult AddNewUser()
        {
            return View();
        }

        [HttpPost]
        [Operation("Add User")]
        public async Task<IActionResult> AddNewUser(RegisterViewModel addUser)
        {
            if (!ModelState.IsValid)
                return View();
            var currentUserId = userManager.GetUserId(User);

            var createdByUserId = (int?)Convert.ToInt32(currentUserId);

            var emailSplit = addUser.Email.Split("@");

            var user = new ApplicationUser()
            {
                UserName = emailSplit[0],
                Email = addUser.Email,
                PhoneNumber = addUser.PhoneNumber,
                CreatedById = createdByUserId == 0 ? null : createdByUserId,
                DateCreated = DateTime.Now,
                LastPasswordChangeDate = DateTime.Now,
                LockoutEnabled = false,
                UserStatus = ApplicationUserStatus.UnConfirmed,
            };

            var createUserResult = await userManager.CreateAsync(user);

            if (createUserResult.Succeeded)
            {

                var code = await userManager.GenerateEmailConfirmationTokenAsync(user);

                if (addUser.Roles != null)
                {
                    var userRoles = addUser.Roles.Select(x => new ApplicationUserRole
                    {
                        DateAddedToRole = DateTime.Now,
                        RoleId = x,
                        UserAddedToRoleById = createdByUserId,
                        UserId = user.Id,
                    }).ToList();

                    identityDbContext.Set<ApplicationUserRole>().AddRange(userRoles);
                    identityDbContext.SaveChanges();

                    var roleClaims = identityDbContext.Set<ApplicationRole>().AsNoTracking()
                        .Where(x => addUser.Roles.Contains(x.Id))
                        .Select(x => new Claim("Role", x.Name)).ToList();

                    await userManager.AddClaimsAsync(user, roleClaims);

                }

                var callbackUrl = Url.Action("ActivateEmail", "UserManagement", new { token = code, userName = user.UserName }, HttpContext.Request.Scheme, $"{HttpContext.Request.Host.Host}:{HttpContext.Request.Host.Port}");

                await mediator.Publish(new UserCreatedNotification()
                {
                    CallBackUrl = callbackUrl,
                    EmailAddress = addUser.Email,
                    Subject = "FELTP Account Activation",
                    UserName = user.UserName,
                    MessageType = MessageTypeEnum.AccountActivation
                });

                return RedirectToAction(nameof(ListUsers));

            }
            foreach (var error in createUserResult.Errors)
                ModelState.AddModelError(error.Code, error.Description);

            return View();
        }

        [Operation("View User Details")]
        public IActionResult ViewUserDetails(int Id)
        {
            var user = identityDbContext.Set<ApplicationUser>().AsNoTracking()
                .Where(x => x.Id == Id).Select(x => new UserDetailsModel
                {
                    Id = x.Id,
                    CreatedBy = x.CreatedBy.UserName,
                    CreatedDate = x.DateCreated.ToShortDateString(),
                    EmailAddress = x.Email,
                    UserName = x.UserName,
                    PhoneNumber = x.PhoneNumber,
                    Status = x.UserStatus,
                    LockoutEnabled = x.LockoutEnabled,
                    LockoutEndDate = x.LockoutEnd,
                    IsActive = true
                }).SingleOrDefault();

            if (user.LockoutEndDate.HasValue)
            {
                var maxDate = DateTime.MaxValue.Date;
                var lockoutDate = user.LockoutEndDate.Value.Date;
                if (user.LockoutEnabled == true && lockoutDate == maxDate)
                    user.IsActive = false;
            }


            return View(user);
        }

        [AllowAnonymous]
        public IActionResult ActivateEmail(string token, string userName)
        {

            return View(new SetPasswordViewModel { UserName = userName, Code = token });
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ActivateEmail(SetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return View();

            var user = await userManager.FindByNameAsync(model.UserName);
            if (user == null)
            {
                ModelState.AddModelError("usernotfound", "User Not Found");
                return View(model);
            }

            var result = await userManager.ConfirmEmailAsync(user, model.Code);

            if (!result.Succeeded)
            {
                ModelState.AddModelError("InvalidCode", "Invalid user token code");
                return View(model);
            }

            user.PasswordHash = userManager.PasswordHasher.HashPassword(user, model.Password);
            user.UserStatus = ApplicationUserStatus.Confirmed;

            await userManager.UpdateAsync(user);
            return RedirectToAction("LogIn", "Account");
        }


        [HttpPost]
        [Operation("View User List")]
        public IActionResult GetUsers(IDataTablesRequest request)
        {
            var users = identityDbContext.Set<ApplicationUser>().AsNoTracking().AsQueryable()
                .Select(x => new UserDetailsModel
                {
                    CreatedBy = x.CreatedBy.UserName,
                    UserName = x.UserName,
                    EmailAddress = x.Email,
                    PhoneNumber = x.PhoneNumber,
                    CreatedDate = x.DateCreated.ToShortDateString(),
                    Status = x.UserStatus,
                    Id = x.Id
                }).ToList();

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? users
                : users.Where(_item => _item.EmailAddress.Contains(request.Search.Value));


            var dataPage = filteredData.Skip(request.Start).Take(request.Length);

            var response = DataTablesResponse.Create(request, users.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        [Operation("View User List")]
        public IActionResult ListUsers()
        {
            return View();
        }

        [Operation("Edit User")]
        public IActionResult _EditUserDetails(string userName)
        {
            var model = identityDbContext.Users
                .Where(p => p.UserName.Equals(userName))
                .Select(p =>
                    new EditUserModel()
                    {
                        UserName = p.UserName,
                        Email = p.Email,
                        PhoneNumber = p.PhoneNumber,
                    }).Single();
            return PartialView(model);
        }

        [HttpPost]
        [Operation("Edit User")]
        public async Task<IActionResult> _EditUserDetails(EditUserModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var user = await signInManager.UserManager.FindByNameAsync(model.UserName);

            var code = await userManager.GenerateEmailConfirmationTokenAsync(user);
            var emailSplit = model.Email.Split("@");

            bool emailChanged = user.Email != model.Email;
            bool emailConfirmed = user.EmailConfirmed;

            user.UpdateUserDetails(emailSplit[0], model.Email, model.PhoneNumber);

            var result = await signInManager.UserManager.UpdateAsync(user);

            if (result.Succeeded && emailChanged)
            {
                if (emailConfirmed)
                {
                    var callbackUrl = Url.Action("EmailChange", "UserManagement", new { token = code, userName = user.UserName }, HttpContext.Request.Scheme, $"{HttpContext.Request.Host.Host}:{HttpContext.Request.Host.Port}");

                    await mediator.Publish(new EmailChangeNotification()
                    {
                        CallBackUrl = callbackUrl,
                        EmailAddress = user.Email,
                        Subject = "FELTP Account Email Change",
                        UserName = user.UserName,
                        MessageType = MessageTypeEnum.EmailChange
                    });
                }
                else
                {
                    var callbackUrl = Url.Action("ActivateEmail", "UserManagement", new { token = code, userName = user.UserName }, HttpContext.Request.Scheme, $"{HttpContext.Request.Host.Host}:{HttpContext.Request.Host.Port}");

                    await mediator.Publish(new UserCreatedNotification()
                    {
                        CallBackUrl = callbackUrl,
                        EmailAddress = user.Email,
                        Subject = "FELTP Account Activation",
                        UserName = user.UserName,
                        MessageType = MessageTypeEnum.AccountActivation
                    });
                }
            }

            return RedirectToAction("ViewUserDetails", new { user.Id });
        }

        [Operation("Deactivate User")]
        public IActionResult DeactivateUser(string userName)
        {
            return PartialView("DeactivateUser", userName);
        }

        [HttpPost]
        [Operation("Deactivate User")]
        public async Task<IActionResult> DeactivateUserAccount(string userName)
        {
            var user = await userManager.FindByNameAsync(userName);

            await userManager.SetLockoutEnabledAsync(user, true);

            await userManager.SetLockoutEndDateAsync(user, DateTime.MaxValue);

            return RedirectToAction("ViewUserDetails", new { user.Id });
        }


        public IActionResult ReActivateUser(string userName)
        {
            return PartialView("ReActivateUser", userName);
        }

        [HttpPost]
        public async Task<IActionResult> ReActivateUserAccount(string userName)
        {
            var user = await userManager.FindByNameAsync(userName);

            await userManager.SetLockoutEnabledAsync(user, false);

            await userManager.SetLockoutEndDateAsync(user, DateTime.UtcNow);

            return RedirectToAction("ViewUserDetails", new { user.Id });
        }

        [AllowAnonymous]
        public IActionResult ResetPassword(string token, string userName)
        {

            return View(new ResetPasswordViewModel { UserName = userName, Code = token });
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return View();

            var user = await userManager.FindByNameAsync(model.UserName);
            if (user == null)
            {
                ModelState.AddModelError("usernotfound", "User Not Found");
                return View(model);
            }

            var result = await userManager.ResetPasswordAsync(user, model.Code, model.Password);

            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(error.Code, error.Description);

                }
                return View(model);
            }

            var callBackUrl = Url.Action("Login", "Account", new { }, HttpContext.Request.Scheme, $"{HttpContext.Request.Host.Host}:{HttpContext.Request.Host.Port}");

            await mediator.Publish(new ResetPasswordNotification()
            {
                CallBackUrl = callBackUrl,
                EmailAddress = user.Email,
                UserName = user.UserName,
                Subject = "FELTP Forgot Password",
                MessageType = MessageTypeEnum.PasswordReset
            });


            return RedirectToAction("LogIn", "Account");
        }



        [AllowAnonymous]
        public IActionResult ForgotPassword(string email)
        {
            return View(new ForgotPasswordViewModel { Email = email });
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var user = await signInManager.UserManager.FindByEmailAsync(model.Email);

            if (user == null)
            {
                ModelState.AddModelError("UserNotFound", $"User details with email address {model.Email} not found");
                return View(model);
            }

            if (!(await signInManager.UserManager.IsEmailConfirmedAsync(user)))
            {
                ModelState.AddModelError("EmailNotConfirmed", $"User email address is not confirmed");
                return View(model);
            }

            var token = userManager.GeneratePasswordResetTokenAsync(user).Result;

            var callbackUrl = Url.Action("ResetPassword", "UserManagement", new { token, userName = user.UserName }, HttpContext.Request.Scheme, $"{HttpContext.Request.Host.Host}:{HttpContext.Request.Host.Port}");

            await mediator.Publish(new ForgotPasswordNotification()
            {
                CallBackUrl = callbackUrl,
                EmailAddress = model.Email,
                UserName = user.UserName,
                Subject = "FELTP Forgot Password",
                MessageType = MessageTypeEnum.ForgotPassword
            });

            return RedirectToAction(nameof(ForgotPasswordResult));

        }

        [AllowAnonymous]
        public IActionResult ForgotPasswordResult()
        {
            return View();
        }

        [AllowAnonymous]
        public async Task<IActionResult> EmailChange(string token, string userName)
        {
            var user = await userManager.FindByNameAsync(userName);
            if (user == null)
            {
                ModelState.AddModelError("usernotfound", "User Not Found");
                return View("EmailChangeFailure");
            }

            var result = await userManager.ConfirmEmailAsync(user, token);

            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(error.Code, error.Description);
                }
                return View("EmailChangeFailure");
            }

            user.UserStatus = ApplicationUserStatus.Confirmed;
            user.EmailConfirmed = true;
            await userManager.UpdateAsync(user);
            return RedirectToAction("EmailChangeSuccess");
        }

        [AllowAnonymous]
        public IActionResult EmailChangeSuccess()
        {
            return View();
        }

    }
}
