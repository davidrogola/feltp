﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using FELTP.Web.Services;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Application;
using UserManagement.Services.Authorization;

namespace FELTP.Web.Controllers
{
    public class EventManagementController : Controller
    {
        IMediator mediator;
        ISelectListLoader programSelectListLoader;
        IEnumerable<ISelectListLoader> selectListLoaders;

        public EventManagementController(IMediator _mediator, IEnumerable<ISelectListLoader> _selectListLoaders)
        {
            mediator = _mediator;
            selectListLoaders = _selectListLoaders;
            programSelectListLoader = selectListLoaders.Where(x => x.CanLoad(SelectItemType.Program)).SingleOrDefault();
        }
        public IActionResult Index()
        {
            return View();
        }

        [Operation("Add Event")]
        public async Task <ActionResult> _AddEvent()

        {
            var addEvent = new AddEventCommand()
            {
                ProgramSelectList = await programSelectListLoader.GetSelectListAsync(null)
            };
            return PartialView(addEvent);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Operation("Add Event")]
        public async Task<ActionResult> _AddEvent(AddEventCommand addEvent)
        {
            if (!ModelState.IsValid)
            {
                return View(addEvent);
            }

            addEvent.CreatedBy = User.Identity.Name;
            int eventId = await mediator.Send(addEvent);
            return RedirectToAction(nameof(EventList));
        }

        [Operation("Edit Event")]
        public async Task<IActionResult> _EditEvent(int id)
        {
            var updateEventCommand = await SetEditEventCommandModel(id);

            return PartialView(updateEventCommand);
        }

        [HttpPost]
        [Operation("Edit Event")]
        public async Task<IActionResult> _EditEvent(UpdateEventCommand command)
        {
            if (!ModelState.IsValid)
            {
                command = await SetEditEventCommandModel(command.Id);
                return PartialView(command);
            }

            command.UpdatedBy = User.Identity.Name;

            var id = await mediator.Send(command);
            if (id != default(int))
                return PartialView("EditEventResult", "Event details updated succesfully");

            ModelState.AddModelError("Response", "An error occured while update event details");
            return PartialView(command);
          
        }

        private async Task<UpdateEventCommand> SetEditEventCommandModel(int eventId)
        {
            var eventList = await mediator.Send(new GetEventList { Id = eventId });

            var eventDetail = eventList.SingleOrDefault();
            if (eventDetail == null)
                return new UpdateEventCommand();

            var programEvent = await mediator.Send(new GetProgramEventList { GetBy = GetEventBy.EventId, Id = eventId });
         
            var selectedPrograms = programEvent.Where(x => x.Status == Status.Active.ToString()).Select(x => x.ProgramId);

            List<SelectListItem> programSelectList = await programSelectListLoader.GetSelectListItemsAsync(null);


            var updateEventCommand = new UpdateEventCommand()
            {
                ProgramSelectList = programSelectList,
                ProgramId = selectedPrograms.ToList(),
                Title = eventDetail.Title,
                Id = eventDetail.Id,
                Theme = eventDetail.Theme,
                EventTypeId = eventDetail.EventTypeId,
            };

            ModelState.Clear();
            return updateEventCommand;
        }
        public async Task<JsonResult> GetPrograms(int? Id)
        {
            var programs = await programSelectListLoader.GetSelectListAsync(Id);

            return Json(programs);
        }

        private async Task<SelectList> GetProgramSelectList(int? id, string selected = "Select")
        {
            var programs = await mediator.Send(new GetProgramList() { Id = id });

            var selectList = new SelectList(programs.OrderBy(x => x.Id), "Id", "Name", selected, "Program");

            return selectList;
        }

        [Operation("View Events Details")]
        public async Task<IActionResult> _EventDetail(int id)
        {
            var Eventdet = await mediator.Send(new GetEventList() { Id = id });

            return View(Eventdet.FirstOrDefault());
        }

        [Operation("Deactivate Event")]
        public ActionResult _Deactivate(int id)
        {
            return PartialView("_Deactivate");

        }

        [HttpPost]
        [Operation("View Events List")]
        public async Task<IActionResult> GetEvents(IDataTablesRequest request)
        {
            var Events = await mediator.Send(new GetEventList());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? Events
                : Events.Where(_item => _item.Title.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, Events.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        [Operation("View Events List")]
        public ActionResult EventList()
        {
            return View();
        }

        public async Task<IActionResult> _AddEventSchedule(int id, int programId, int eventId)
        {
            var model = new AddEventScheduleCommand
            {
                ProgramEventId = id,
                ProgramId = programId,
                ProgramSelectList = await programSelectListLoader.GetSelectListAsync(null),
                ProgramEvents = await GetEventsById(id),
                EventId = eventId
            };

            return PartialView(model);
        }

        [HttpPost]
        public async Task<IActionResult> _AddEventSchedule(AddEventScheduleCommand model)
        {
            if (!ModelState.IsValid)
            {
                model.ProgramSelectList = await programSelectListLoader.GetSelectListAsync(null);
                return PartialView(model);
            }

            model.CreatedBy = User.Identity.Name;

            var id = await mediator.Send(model);

            return RedirectToAction(nameof(_EventDetail),new { Id = model.EventId});
        }

        private async Task<List<SelectListItem>> GetEventsById(int id)
        {
            var events = await mediator.Send(new GetProgramEventList { Id = id, GetBy = GetEventBy.Id });

            var eventSelectList = events.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.EventTitle
            }).ToList();

            return eventSelectList;
        }

        public IActionResult EventScheduleList()
        {
            return View();
        }

        public async Task<IActionResult> GetEventSchedule(int? programEventId)
        {
            var eventSchedule = await mediator.Send(new GetEventScheduleList { ProgramEventId = programEventId });

            var activeSchedule = eventSchedule.FirstOrDefault();

            return Json(activeSchedule);
        }

        [HttpPost]
        public async Task<IActionResult> GetEventSchedule(IDataTablesRequest request, int? programEventId)
        {
            var eventSchedule = await mediator.Send(new GetEventScheduleList() { ProgramEventId = programEventId });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? eventSchedule
                : eventSchedule.Where(_item => _item.Location.Contains(request.Search.Value));
            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, eventSchedule.Count(), filteredData.Count(), dataPage);
            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> _EventScheduleDetails(int id)
        {
            var programEvent = await mediator.Send(new GetProgramEventList() { Id = id, GetBy = GetEventBy.Id });

            return PartialView(programEvent.FirstOrDefault());
        }
        public ActionResult DeleteEventSchedule(int id)
        {
            return PartialView("_DeleteEventSchedule");

        }

        public async Task<IActionResult> GetProgramEvents(int? programId)
        {
            var programEvents = await mediator.Send(new GetProgramEventList { Id = programId, GetBy = GetEventBy.ProgramId });

            var selectItems = programEvents.Select(x => new SelectListItem
            {
                Text = x.EventTitle,
                Value = x.Id.ToString()
            });

            return Json(selectItems);
        }
        [HttpPost]
        public async Task<IActionResult> GetProgramEvents(int? eventId, IDataTablesRequest request)
        {
            var programs = await mediator.Send(new GetProgramEventList() { GetBy = GetEventBy.EventId, Id = eventId });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? programs
                : programs.Where(_item => _item.ProgramName.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, programs.Count(), filteredData.Count(), dataPage);
            return new DataTablesJsonResult(response, true);
        }

    }
}