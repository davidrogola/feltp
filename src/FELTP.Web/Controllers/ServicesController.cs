using System.Threading;
using Application.Common.ActivityMonitor;
using Microsoft.AspNetCore.Mvc;

namespace FELTP.Web.Controllers
{
    public class ServicesController : Controller
    {
        private readonly IProgamActivityMonitoringServiceLoader _programActivitySvcLoader;
        public ServicesController(IProgamActivityMonitoringServiceLoader programActivitySvcLoader)
        {
            _programActivitySvcLoader = programActivitySvcLoader;
        }
        // GET
        public IActionResult Trigger()
        {
            return View();
        }
        
        [HttpPost]
        public IActionResult TriggerActivityMonitoringService()
        {
            _programActivitySvcLoader.Execute();
            return View("Trigger");
        }
        
        
        
    }
}