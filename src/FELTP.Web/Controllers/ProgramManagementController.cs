﻿using Application;
using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using Application.ProgramManagement.QueryHandlers;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using FELTP.Web.Services;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserManagement.Services;
using UserManagement.Services.Authorization;
using System.IO;

namespace FELTP.Web.Controllers
{
    [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
    [Authorize]
    public class ProgramManagementController : Controller
    {

        IMediator mediator;
        IEnumerable<ISelectListLoader> selectListLoaders;
        public ProgramManagementController(IMediator _mediator, IEnumerable<ISelectListLoader> _selectListLoaders)
        {
            mediator = _mediator;
            selectListLoaders = _selectListLoaders;
        }


        public IActionResult DashBoard()
        {
            return View();
        }


        [Operation("Add Program")]
        public ActionResult _AddProgram()
        {

            return PartialView();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Operation("Add Program")]
        public async Task<ActionResult> _AddProgram(AddProgramCommand addProgramCommand)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(addProgramCommand);
            }

            addProgramCommand.CreatedBy = User.Identity.Name;

            var programId = await mediator.Send(addProgramCommand);

            return RedirectToAction(nameof(ProgramList));
        }

        [Operation("View Program List")]
        public ActionResult ProgramList()
        {
            return View();
        }

        [Operation("Edit Program")]
        public async Task<IActionResult> _Editprogram(int id)
        {
            var command = await SetEditProgramCommandModel(id);

            return PartialView(command);
        }

        [HttpPost]
        [Operation("Edit Program")]
        public async Task<IActionResult> _EditProgram(UpdateProgramCommand command)
        {
            if (!ModelState.IsValid)
            {
                command = await SetEditProgramCommandModel(command.Id);
                return View(command);
            }

            command.CreatedBy = User.Identity.Name;
            var id = await mediator.Send(command);

            return RedirectToAction(nameof(ProgramList));
        }

        private async Task<UpdateProgramCommand> SetEditProgramCommandModel(int ProgramId)
        {
            var programList = await mediator.Send(new GetProgramList { Id = ProgramId });

            var command = programList.Select(x => new UpdateProgramCommand
            {
                Id = x.Id,
                Code = x.Code,
                Name = x.Name,
                Description = x.Description,
                HasMonthlySeminors = x.HasMonthlySeminors,
                ProgramTier = x.ProgramTier,
                ProgramTierId = x.ProgramTierId,
                CreatedBy = User.Identity.Name

            }).SingleOrDefault();

            command.ProgramTiers = await GetProgramTierSelectList(command.ProgramTier);

            return command;
        }

        private async Task<List<SelectListItem>> GetProgramTierSelectList(string selected = "Select")
        {
            var programTiers = await mediator.Send(new GetProgramTierList());
            var tiers = programTiers.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString(),
                Selected = x.Name == selected
            }).ToList();

            return tiers;
        }


        public async Task<IActionResult> ProgramDetails(int id)
        {
            var programdet = await mediator.Send(new GetProgramList() { Id = id });

            return View(programdet.FirstOrDefault());
        }


        [Operation("Deactivate Program")]
        public ActionResult Delete(int id)
        {
            return PartialView("_DeleteProgram");

        }

        public async Task<JsonResult> GetProgramTiers()
        {
            var tiers = await GetProgramTierSelectList();
            return Json(tiers);
        }

        public async Task<JsonResult> GetPrograms(int? programTierId)
        {
            var programs = await GetSelectListLoader(SelectItemType.Program).GetSelectListAsync(programTierId);

            return Json(programs);
        }

        public async Task<JsonResult> GetCategories(int? courseTypeId)
        {

            var categories = await GetSelectListLoader(SelectItemType.Category).GetSelectListAsync(null);

            return Json(categories);
        }


        [HttpPost]
        [Operation("View Program List")]
        public async Task<IActionResult> GetPrograms(IDataTablesRequest request)
        {
            var programs = await mediator.Send(new GetProgramList());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? programs
                : programs.Where(_item => _item.Name.Contains(request.Search.Value));


            var dataPage = filteredData.Skip(request.Start).Take(request.Length);


            var response = DataTablesResponse.Create(request, programs.Count(), filteredData.Count(), dataPage);
            return new DataTablesJsonResult(response, true);
        }

        [Operation("View Program List")]
        public ActionResult ProgramTierList()
        {
            return View();
        }

        [Operation("Add Program Course")]
        public async Task<IActionResult> _AddProgramCourse(int programId)
        {

            var addProgramCourse = new AddProgramCourseCommand()
            {
                ProgramId = programId,
                ProgramSelectList = await GetSelectListLoader(SelectItemType.Program).GetSelectListAsync(null),
                CategorySelectList = await GetSelectListLoader(SelectItemType.Category).GetSelectListAsync(null)
            };
            return PartialView(addProgramCourse);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Operation("Add Program Course")]
        public async Task<IActionResult> _AddProgramCourse(AddProgramCourseCommand model)
        {
            if (!ModelState.IsValid)
            {
                model.ProgramSelectList = await GetSelectListLoader(SelectItemType.Program).GetSelectListAsync(null);
                model.CategorySelectList = await GetSelectListLoader(SelectItemType.Category).GetSelectListAsync(null);
                return View(model);
            }
            model.CreatedBy = User.Identity.Name;
            int id = await mediator.Send(model);
            return RedirectToAction(nameof(ProgramDetails), new { Id = model.ProgramId });
        }

        [Operation("View Program Course List")]
        public IActionResult ProgramCoursesList()
        {
            return View();
        }


        public async Task<IActionResult> GetProgramCourses(int? programId)
        {
            var programCourses = await mediator.Send(new GetProgramCoursesList { ProgramId = programId });

            var selectItems = programCourses.Select(x => new SelectListItem
            {
                Text = $"{ x.CourseName} ({x.Semester})",
                Value = x.Id.ToString()
            });

            return Json(selectItems);
        }

        [Operation("View Program Course Details")]
        public IActionResult _ProgramCourses(int programId, string name)
        {
            var model = new ProgramCoursesViewModel() { ProgramId = programId, ProgramName = name };
            return PartialView(model);
        }

        [HttpPost]
        [Operation("View Program Course List")]
        public async Task<IActionResult> GetProgramCourses(int? programId, IDataTablesRequest request)
        {
            var programs = await mediator.Send(new GetProgramCoursesList() { ProgramId = programId });

            var activePrograms = programs.Where(x => x.Status == Status.Active.ToString());
            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? activePrograms
                : activePrograms.Where(_item => _item.ProgramName.ToLower().Contains(request.Search.Value));


            var dataPage = filteredData.Skip(request.Start).Take(request.Length);

            var response = DataTablesResponse.Create(request, activePrograms.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public ActionResult _RemoveProgramCourse(int id, int programId)
        {
            return PartialView("_RemoveProgramCourse", new DeactivateProgramCourseCommand { Id = id, ProgramId = programId });

        }

        [HttpPost]
        public async Task<IActionResult> _RemoveProgramCourse(DeactivateProgramCourseCommand command)
        {
            if (!ModelState.IsValid)
            {
                command = await SetDeactivateProgramCourseCommandModel(command.Id);
                return View(command);
            }

            command.DeactivatedBy = User.Identity.Name;

            var id = await mediator.Send(command);

            return RedirectToAction(nameof(ProgramDetails), new { Id = command.ProgramId });
        }

        private async Task<DeactivateProgramCourseCommand> SetDeactivateProgramCourseCommandModel(int courseId)
        {
            var deactivateProgramCourseCommand = new DeactivateProgramCourseCommand();

            var courseList = await mediator.Send(new GetProgramCoursesList { Id = courseId });

            var course = courseList.SingleOrDefault();

            return deactivateProgramCourseCommand;
        }


        [Operation("View Program Competency")]
        public IActionResult ProgramCompetenciesList()
        {
            return View();
        }

        [Operation("View Program Competency")]
        public async Task<IActionResult> GetProgramCompetencies(IDataTablesRequest request)
        {
            var programCompetencies = await mediator.Send(new GetProgramCompetencyList());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? programCompetencies
                : programCompetencies.Where(_item => _item.Program.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);


            var response = DataTablesResponse.Create(request, programCompetencies.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public async Task<AddProgramAcademicRequirementCommand> BuildProgramAcademicRequirementCommandModel()
        {

            var academicRequirementDetails = new AddProgramAcademicRequirementCommand
            {
                AcademicCourses = await GetSelectListLoader(SelectItemType.AcademicCourse).GetSelectListAsync(null),
                CourseGrades = await GetSelectListLoader(SelectItemType.AcademicCourseGrading).GetSelectListAsync(null)
            };

            return academicRequirementDetails;
        }
        public async Task<AddProgramOfferCommand> BuildProgramOfferCommandModel()
        {
            var programOfferCommand = new AddProgramOfferCommand()
            {
                ProgramSelectList = await GetSelectListLoader(SelectItemType.Program).GetSelectListAsync(null),
                AcademicRequirementDetails = await BuildProgramAcademicRequirementCommandModel()
            };
            return programOfferCommand;
        }


        [Operation("Add Program Offer")]
        public async Task<ActionResult> AddProgramOffer()
        {
            var addProgramOfferCommand = await BuildProgramOfferCommandModel();

            return View(addProgramOfferCommand);
        }


        [HttpPost]
        [Operation("Add Program Offer")]
        public async Task<IActionResult> AddProgramOffer(AddProgramOfferCommand model)
        {
            if (!ModelState.IsValid)
            {
                model.ProgramSelectList = await GetSelectListLoader(SelectItemType.Program).GetSelectListAsync(null);
                model.AcademicRequirementDetails = await BuildProgramAcademicRequirementCommandModel();
                return View(model);
            }

            model.CreatedBy = User.Identity.Name;

            var offerId = await mediator.Send(model);

            return RedirectToAction(nameof(ProgramOfferList));
        }

        public async Task<IActionResult> _EditProgramOffer(int id)
        {
            var programOfer = await mediator.Send(new GetProgramOfferById() { OfferId = id });

            var updateProgramOfferCommand = new UpdateProgramOfferCommand
            {
                Id = programOfer.Id,
                Name = programOfer.Name,
                ProgramId = programOfer.ProgramId,
                FacultyId = programOfer.FacultyId,
                ProgramLocationId = programOfer.ProgramLocationId,
                StrApplicationStartDate = programOfer.StrApplicationStartDate,
                StrApplicationEndDate = programOfer.StrApplicationEndDate,
                StrStartDate = programOfer.StrStartDate,
                ProgramLocations = await GetSelectListLoader(SelectItemType.ProgramLocation).GetSelectListItemsAsync(null),
                Programs = await GetSelectListLoader(SelectItemType.Program).GetSelectListItemsAsync(null),
                Faculties = await GetSelectListLoader(SelectItemType.Faculty).GetSelectListItemsAsync(null)
            };

            return PartialView(updateProgramOfferCommand);

        }

        [HttpPost]
        public async Task<IActionResult> _EditProgramOffer(UpdateProgramOfferCommand command)
        {
            command.CreatedBy = User.Identity.Name;
            var id = await mediator.Send(command);
            return RedirectToAction(nameof(ProgramOfferList));
        }

        public async Task<IActionResult> ViewOfferRequirements(int Id)
        {
            var programOfferGraduationRequirements = await mediator.Send(new GetProgramOfferRequirementsQuery { ProgramOfferId = Id });

            return View(programOfferGraduationRequirements);
        }


        [Operation("Add Program Location")]
        public IActionResult _AddProgramLocation()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Operation("Add Program Location")]
        public async Task<IActionResult> _AddProgramLocation(AddProgramLocationCommand model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var id = await mediator.Send(model);

            return RedirectToAction(nameof(ProgramLocationList));
        }

        [Operation("View Program Location")]
        public IActionResult ProgramLocationList()
        {
            return View();
        }

        public async Task<IActionResult> _EditProgramLocation(int id)
        {

            var programLocation = await mediator.Send(new GetProgramLocationQuery() { Id = id });
            var model = new UpdateProgramLocationCommand
            {
                Id = programLocation.Id,
                Road = programLocation.Road,
                Location = programLocation.Location,
                PostalAddress = programLocation.PostalAddress,
                Building = programLocation.Building,
                CountryId = programLocation.CountryId,
                CountyId = programLocation.CountyId,
                SubCountyId = programLocation.SubCountyId,
                Countries = await GetSelectListLoader(SelectItemType.Country).GetSelectListItemsAsync(null),
                Counties = await GetSelectListLoader(SelectItemType.County).GetSelectListItemsAsync(programLocation.CountryId),
                Subcounties = await GetSelectListLoader(SelectItemType.Subcounty).GetSelectListItemsAsync(programLocation.CountyId)
            };
            return PartialView(model);
        }

        [HttpPost]
        public async Task<IActionResult> _EditProgramLocation(UpdateProgramLocationCommand command)
        {
            var id = await mediator.Send(command);

            return RedirectToAction(nameof(ProgramLocationList));

        }
        public IActionResult _DeactivateProgramLocation(int id)
        {
            var model = new DeactivateProgramLocationCommand {Id = id, DeactivatedBy = User.Identity.Name};
         
            return PartialView(model);

        }

        [HttpPost]
        public async Task<IActionResult> _DeactivateProgramLocation(DeactivateProgramLocationCommand command)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(command);
            }

            command.DeactivatedBy = User.Identity.Name;

            var id = await mediator.Send(command);

            return RedirectToAction(nameof(ProgramLocationList));
        }


        public IActionResult _ProgramOfferLocationList(int id, string name)
        {
            return PartialView(new ProgramOfferViewModel() { ProgramOfferId = id, Name = name });
        }

        public async Task<IActionResult> GetProgramOfferLocations(IDataTablesRequest request, int? programOfferId)
        {
            var programOfferLocations = await mediator.Send(new GetProgramOfferLocationQuery() { ProgramOfferId = programOfferId });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? programOfferLocations
                : programOfferLocations.Where(_item => _item.Location.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);


            var response = DataTablesResponse.Create(request, programOfferLocations.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        [HttpPost]
        public async Task<IActionResult> GetProgramLocations(IDataTablesRequest request)
        {
            var programLocations = await mediator.Send(new GetProgramOfferLocationQuery());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? programLocations
                : programLocations.Where(_item => _item.Location.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, programLocations.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }


        public async Task<IActionResult> GetProgramLocationDropDown()
        {
            var locations = await mediator.Send(new GetProgramOfferLocationQuery { });
            var selectItems = locations.Select(x => new SelectListItem
            {
                Text = x.Location,
                Value = x.Id.ToString()
            });
            return Json(selectItems);
        }

        [Operation("View Program Offer List")]
        public IActionResult ProgramOfferList()
        {
            return View();
        }

        [Operation("View Program Offer List")]
        public async Task<IActionResult> GetProgramOfferList(IDataTablesRequest request)
        {
            var programOffers = await mediator.Send(new GetProgramOfferList());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? programOffers
                : programOffers.Where(_item => _item.Name.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);


            var response = DataTablesResponse.Create(request, programOffers.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }


        public async Task<IActionResult> GetProgramOffers(int? programId)
        {
            var programOffers = await mediator.Send(new GetProgramOfferList { ProgramId = programId });

            var selectItems = programOffers.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            });

            return Json(selectItems);
        }

        [Operation("View Program Offer Details")]
        public async Task<IActionResult> _ProgramOfferDetails(int programOfferId, bool jsonFormat = false)
        {
            var programOffer = await mediator.Send(new GetProgramOfferById
            {
                OfferId = programOfferId
            });

            if (jsonFormat)
                return Json(programOffer);

            return PartialView(programOffer);

        }

        public async Task<IActionResult> ProgramOfferDetails(int id)
        {
            var programOffer = await mediator.Send(new GetProgramOfferById { OfferId = id });
            return View(programOffer);
        }

        public async Task<IActionResult> ProgramOfferApplicants(int id)
        {
            var programOffer = await mediator.Send(new GetProgramOfferById
            {
                OfferId = id
            });

            return View(programOffer);
        }

        [Operation("View Program Offer Applicants List")]
        public async Task<IActionResult> GetProgramOfferApplicants(IDataTablesRequest request, int programOfferId,
            ApplicationStatus status = ApplicationStatus.All)
        {
            var programOfferApplicants = await mediator.Send(new GetProgramOfferApplicationsQuery()
            {
                ProgramOfferId = programOfferId,
                ApplicationStatus = status

            });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? programOfferApplicants.OrderByDescending(x=>x.Id)
                : programOfferApplicants.Where(_item => _item.IdentificationNumber.Contains(request.Search.Value)).OrderByDescending(x=>x.Id);


            var dataPage = filteredData.Skip(request.Start).Take(request.Length).ToList();

            var response = DataTablesResponse.Create(request, programOfferApplicants.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }


        [Operation("Add Resource")]
        public async Task<IActionResult> _AddResource()
        {
            var programSelectItem = await GetSelectListLoader(SelectItemType.Program).GetSelectListAsync(null);

            var model = new AddResourceCommand
            {
                ProgramSelectList = programSelectItem
            };
            model.ResourseTypes = await GetResourceTypes();
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Operation("Add Resource")]
        public async Task<IActionResult> _AddResource(AddResourceCommand model)
        {
            if (!ModelState.IsValid)
            {
                model.ResourseTypes = await GetResourceTypes();
                model.ProgramSelectList = await GetSelectListLoader(SelectItemType.Program).GetSelectListAsync(null);
                return View(model);
            }

            model.CreatedBy = User.Identity.Name;
            int resourceId = await mediator.Send(model);

            var resourcefile = new AddResourceFileCommand();
            resourcefile.ResourceId = resourceId;
        
            foreach (var file in model.UploadedFile)
            {
                if (file != null)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        file.CopyTo(memoryStream);
                        resourcefile.FileContent = memoryStream.ToArray();
                        resourcefile.ContentType = file.ContentType;
                        resourcefile.FileName = file.FileName;
                    }
                    resourcefile.CreatedBy = User.Identity.Name;
                    var result = await mediator.Send(resourcefile);
                }
            }
            return RedirectToAction(nameof(ResourceList));
        }
      
        public async Task<IActionResult> _EditResource(int id)
        {
            var resource = await mediator.Send(new GetResource { ResourceId = id });
            var editResource = new EditResourceCommand
            {
                ProgramId = resource.ProgramId,
                SourceContactDetails = resource.SourceContactDetails,
                ResourceDescription = resource.ResourceDescription,
                ResourceTitle = resource.ResourceTitle,
                ResourceTypeId = resource.ResourceTypeId,
                Id = resource.Id,
                Source = resource.Source,
                CreatedBy = resource.CreatedBy,
                ProgramSelectList = await GetSelectListLoader(SelectItemType.Program).GetSelectListAsync(null),
                ResourseTypes = await GetResourceTypes()
            };

            return PartialView(editResource);
        }

        [HttpPost]
        public async Task<IActionResult> _EditResource(EditResourceCommand model)
        {
            if (!ModelState.IsValid)
            {
                model.ProgramSelectList = await GetSelectListLoader(SelectItemType.Program).GetSelectListAsync(null);
                model.ResourseTypes = await GetResourceTypes();
                return PartialView(model);
            }

            model.UpdatedBy = User.Identity.Name;
            var result = await mediator.Send(model);
            try
            {
                var resourcefile = new EditResourceFileCommand();
                resourcefile.ResourceId = model.Id;

                foreach (var file in model.UploadedFile)
                {
                    if (file != null)
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            file.CopyTo(memoryStream);
                            resourcefile.FileContent = memoryStream.ToArray();
                            resourcefile.ContentType = file.ContentType;
                            resourcefile.FileName = file.FileName;
                        }
                        resourcefile.CreatedBy = User.Identity.Name;
                        var documentuploadresult = await mediator.Send(resourcefile);
                    }
                }
            }
            catch(Exception ex)
            {

            }
            return PartialView("_EditResourceResult",result);
        }


        [Operation("View Resource List")]
        public IActionResult ResourceList()
        {

            return View();
        }

        [HttpPost]
        [Operation("View Resource List")]
        public async Task<IActionResult> GetResources(IDataTablesRequest request)
        {
            var resources = await mediator.Send(new GetResourceList());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? resources
                : resources.Where(_item => _item.ResourceTitle.Contains(request.Search.Value));


            var dataPage = filteredData.Skip(request.Start).Take(request.Length);


            var response = DataTablesResponse.Create(request, resources.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }


        public async Task<List<SelectListItem>> GetResourceTypes()
        {
            var resourceTypes = await mediator.Send(new GetResourceTypeList());
            var selectListItems = resourceTypes.Select(
                x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                }).ToList();

            return selectListItems;
        }
        public IActionResult _AddResourceType()
        {
            return PartialView();
        }

        [HttpPost]
        public async Task<IActionResult> _AddResourceType(AddResourceTypeCommand model)
        {
            if (!ModelState.IsValid)
                return View();
            var resourcetypeid = await mediator.Send(model);

            return RedirectToAction(nameof(ResourceTypeList));
        }

        public async Task<IActionResult> _EditResourceType(int id)
        {
            var resourcetype = await mediator.Send(new GetResourceTypeList() { Id = id });

            return PartialView(resourcetype.FirstOrDefault());
        }

        [HttpPost]
        [Operation("Edit Resource Type")]
        public async Task<IActionResult> _EditResourceType(UpdateResourceTypeCommand command)
        {
            var id = await mediator.Send(command);

            return RedirectToAction(nameof(ResourceTypeList));
        }

        [Operation("View Resource Type Details")]
        public async Task<IActionResult> _DetailsResourceType(int id)
        {
            var resourcetypedet = await mediator.Send(new GetResourceTypeList() { Id = id });

            return PartialView(resourcetypedet.FirstOrDefault());
        }

        [Operation("Deactivate Resource Type")]
        public ActionResult _DeactivateResourceType(int id)
        {
            return PartialView("_DeactivateResourceType");

        }

        [HttpPost]
        public async Task<IActionResult> _DeactivateResourceType(DeactivateResourceTypeCommand command)
        {
            if (!ModelState.IsValid)
            {
                command = await SetDeactivateResourceTypeCommandModel(command.Id);
                return View(command);
            }

            command.DeactivatedBy = User.Identity.Name;

            var id = await mediator.Send(command);

            return RedirectToAction(nameof(ResourceTypeList));
        }

        private async Task<DeactivateResourceTypeCommand> SetDeactivateResourceTypeCommandModel(int typeId)
        {
            var deactivateResourceTypeCommand = new DeactivateResourceTypeCommand();

            var typeList = await mediator.Send(new GetResourceTypeList { Id = typeId });

            var type = typeList.SingleOrDefault();

            return deactivateResourceTypeCommand;
        }

        [Operation("View Resource Type List")]
        public IActionResult ResourceTypeList()
        {
            return View();
        }

        [HttpPost]
        [Operation("View Resource Type List")]
        public async Task<IActionResult> GetResourceTypes(IDataTablesRequest request)
        {
            var resourcetypes = await mediator.Send(new GetResourceTypeList());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? resourcetypes
                : resourcetypes.Where(_item => _item.Name.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, resourcetypes.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public async Task<JsonResult> GetResources(int? resourceTypeId)
        {
            var resourceTypes = await mediator.Send(new GetResourceList() { ResourceTypeId = resourceTypeId });

            var types = resourceTypes.Select(x => new SelectListItem
            {
                Text = x.ResourceTitle,
                Value = x.Id.ToString()
            }).ToList();

            return Json(types);
        }

        [Operation("Add Program Tier")]
        public ActionResult _AddProgramTier()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Operation("Add Program Tier")]
        public async Task<ActionResult> _AddProgramTier(AddProgramTierCommand addProgramTier)
        {
            if (!ModelState.IsValid)
            {
                return View(addProgramTier);
            }
            int programtierId = await mediator.Send(addProgramTier);
            return RedirectToAction(nameof(ProgramTierList));
        }

        [Operation("Edit Program Tier")]
        public async Task<IActionResult> _EditProgramTier(int id)
        {
            var programtier = await mediator.Send(new GetProgramTierList() { Id = id });

            return PartialView(programtier.FirstOrDefault());
        }

        [HttpPost]
        [Operation("Edit Program Tier")]
        public async Task<IActionResult> _EditProgramTier(UpdateProgramTierCommand command)
        {
            var id = await mediator.Send(command);

            return RedirectToAction(nameof(ProgramTierList));
        }

        [Operation("View Program Tier Details")]
        public async Task<IActionResult> _DetailsProgramTier(int id)
        {
            var programtier = await mediator.Send(new GetProgramTierList() { Id = id });

            return PartialView(programtier.FirstOrDefault());
        }

        [Operation("Decativate Program Tier")]
        public ActionResult _DeleteProgramTier(int id)
        {
            return PartialView("_DeleteProgramTier");

        }

        [HttpPost]
        [Operation("Decativate Program Tier")]
        public async Task<IActionResult> _DeleteProgramTier(DeactivateProgramTierCommand command)
        {
            if (!ModelState.IsValid)
            {
                command = await SetDeactivateProgramTierCommandModel(command.Id);
                return View(command);
            }

            command.UpdatedBy = User.Identity.Name;

            var id = await mediator.Send(command);

            return RedirectToAction(nameof(ProgramTierList));
        }

        private async Task<DeactivateProgramTierCommand> SetDeactivateProgramTierCommandModel(int tierId)
        {
            var deactivateProgramTierCommand = new DeactivateProgramTierCommand();

            var tierList = await mediator.Send(new GetProgramTierList { Id = tierId });

            var tier = tierList.SingleOrDefault();

            return deactivateProgramTierCommand;
        }


        [HttpPost]
        [Operation("View Program Tier List")]
        public async Task<IActionResult> GetProgramTiers(IDataTablesRequest request)
        {
            var programtiers = await mediator.Send(new GetProgramTierList());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? programtiers
                : programtiers.Where(_item => _item.Name.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, programtiers.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }


        public async Task<IActionResult> AddProgramResource()
        {
            var programLoader = selectListLoaders.Where(x => x.CanLoad(SelectItemType.Program)).SingleOrDefault();

            var programResourceCommand = new AddProgramResourceCommand()
            {
                ProgramSelectList = await programLoader.GetSelectListAsync(null)
            };

            return View(programResourceCommand);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddProgramResource(AddProgramResourceCommand model)
        {
            if (!ModelState.IsValid)
            {
                var programLoader = selectListLoaders.Where(x => x.CanLoad(SelectItemType.Program)).SingleOrDefault();
                var programSelectItem = await programLoader.GetSelectListAsync(null);
                model.ProgramSelectList = programSelectItem;
                return View(model);
            }

            int id = await mediator.Send(model);

            return RedirectToAction(nameof(ProgramResourcesList));
        }

        public IActionResult _ViewProgramResources(int resourceId, string title)
        {
            return PartialView(new ResourceViewModel { Id = resourceId, ResourceTitle = title });
        }

        public IActionResult ProgramResourcesList()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> GetProgramResources(IDataTablesRequest request, int ? resourceId)
        {
            var programresources = await mediator.Send(new GetProgramResourcesList() { ResourceId = resourceId });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? programresources
                : programresources.Where(_item => _item.ProgramName.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, programresources.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public async Task<JsonResult> GetProgramListDropdown()
        {
            var programs = await mediator.Send(new GetProgramList { });
            var programsSelectList = programs.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return Json(programsSelectList);
        }
        private ISelectListLoader GetSelectListLoader(SelectItemType selectItemType)
        {
            return selectListLoaders.Where(x => x.CanLoad(selectItemType)).SingleOrDefault();
        }

        public async Task<JsonResult> GetProgramDeliverablesWithThesisDefenceCount(int programId)
        {
            var deliverables = await mediator.Send(new GetProgramDeliverables { ProgramId = programId });

            var thesisDefenceDeliverablesCount = deliverables.Where(x => x.HasThesisDefence == true).Count();

            return Json(new { thesisDefenceDeliverablesCount });
        }
        public IActionResult ViewProgramResourcesFiles(int? resourceId)
        {

            var resourcefile = new GetProgramResourceFile();
            resourcefile.resourceId = resourceId;
            return PartialView(resourcefile);
        }

        [HttpPost]
        public async Task<IActionResult> ViewProgramResourcesFiles(int? Id, IDataTablesRequest request)
        {
            
            var programresourcefiles = await mediator.Send(new GetProgramResourceFile() { resourceId = Id });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value) ? programresourcefiles
                  : programresourcefiles.Where(_item => _item.ContentType.Contains(request.Search.Value));
            var dataPage = filteredData.Skip(request.Start).Take(request.Length);

            var response = DataTablesResponse.Create(request, programresourcefiles.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> DownLoadResourceFile(int Id)
        {
            var resourceFiles = await mediator.Send(new GetProgramResourceFileById() { Id=Id });
            if(resourceFiles!=null)
                return File(resourceFiles.FileContent, resourceFiles.ContentType, resourceFiles.FileName);
            return null;

        }

        [HttpPost]
        public async Task<IActionResult> GetProgramResourcesByProgramId(int? programId, IDataTablesRequest request)
        {
            var programResource = await mediator.Send(new GetProgramResourcesByProgramId() { programId = programId });
            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value) ? programResource
                   : programResource.Where(_item => _item.ResourceTitle.Contains(request.Search.Value));
            var dataPage = filteredData.Skip(request.Start).Take(request.Length);

            var response = DataTablesResponse.Create(request, programResource.Count(), filteredData.Count(), dataPage);
            return new DataTablesJsonResult(response, true);
        }
        
        

    }
}