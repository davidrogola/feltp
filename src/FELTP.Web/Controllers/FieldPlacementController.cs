﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using Application.FacultyManagement.Queries;
using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using Application.ResidentManagement.Commands;
using Application.ResidentManagement.Queries;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using FELTP.Web.Services;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProgramManagement.Domain.FieldPlacement;
using UserManagement.Services.Authorization;

namespace FELTP.Web.Controllers
{
    [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
    [Authorize]
    public class FieldPlacementController : Controller
    {
        IMediator mediator;
        IEnumerable<ISelectListLoader> selectListLoaders;

        public FieldPlacementController(IMediator _mediator, IEnumerable<ISelectListLoader> _selectListLoaders)
        {
            mediator = _mediator;
            selectListLoaders = _selectListLoaders;
        }
        public IActionResult Index()
        {
            return View();
        }

        [Operation("Add Field Activity")]
        public IActionResult AddFieldPlacementActivity()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Operation("Add Field Activity")]
        public async Task<IActionResult> AddFieldPlacementActivity(AddFieldPlacementActivityCommand model)
        {
            if (!ModelState.IsValid)
                return View();
            var activityId = await mediator.Send(model);
            return RedirectToAction(nameof(FieldPlacementActivityList));
        }

        public async Task<IActionResult> _EditFieldPlacementActivity(int id)
        {
            var fieldPlacementActivity = await mediator.Send(new GetFieldPlacementActivityList() { Id = id });

            var model = fieldPlacementActivity.Select(x => new UpdateFieldPlacementActivityCommand
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                ActivityType = x.ActivityType,
                DeliverableId = x.DeliverableId

            }).SingleOrDefault();
            model.Deliverables = await GetSelectListLoader(SelectItemType.Deliverable).GetSelectListItemsAsync(null);

            return PartialView(model);
        }

        [HttpPost]
        public async Task<IActionResult> _EditFieldPlacementActivity(UpdateFieldPlacementActivityCommand command)
        {
            command.UpdatedBy = User.Identity.Name;
            var id = await mediator.Send(command);
            return RedirectToAction(nameof(FieldPlacementActivityList));

        }


        [Operation("View Field Activities")]
        public IActionResult FieldPlacementActivityList()
        {
            return View();
        }

        [Operation("View Field Activities")]
        public async Task<IActionResult> GetFieldPlacementActivities(IDataTablesRequest request)
        {

            var fieldPlacementActivity = await mediator.Send(new GetFieldPlacementActivityList());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? fieldPlacementActivity
                : fieldPlacementActivity.Where(_item => _item.Name.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, fieldPlacementActivity.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        [Operation("Add Field Placement Activity Deliverable")]
        public IActionResult AddFieldPlacementActivityDeliverable()
        {

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Operation("Add Field Placement Activity Deliverable")]
        public async Task<IActionResult> AddFieldPlacementActivityDeliverable(AddFieldPlacementActivityDeliverableCommand model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var activityId = await mediator.Send(model);
            return RedirectToAction(nameof(FieldPlacementActivityDeliverableList));
        }

        [HttpPost]
        [Operation("View Field Placement Activity Deliverable")]
        public async Task<IActionResult> GetFieldPlacementActivityDeliverables(int activityId, IDataTablesRequest request)
        {
            var fieldPlacementActivityDeliverable =
               await mediator.Send(new GetFieldPlacementActivityDeliverablesList()
               {
                   FieldPlacementActivityId = activityId
               });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? fieldPlacementActivityDeliverable
                : fieldPlacementActivityDeliverable.Where(_item => _item.FieldPlacementActivity.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, fieldPlacementActivityDeliverable.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        [Operation("View Activity Deliverable")]
        public IActionResult _ViewActivityDeliverable(int id, string name)
        {
            var model = new FieldPlacementActivityDeliverablesViewModel
            {
                FieldPlacementActivityId = id,
                FieldPlacementActivity = name
            };
            return PartialView(model);
        }

        [Operation("View Field Placement Activity Deliverable")]
        public IActionResult FieldPlacementActivityDeliverableList()
        {

            return View();
        }


        public async Task<JsonResult> GetFieldPlacementActivitiesDropDown()
        {
            var fieldPlacementActivities = await mediator.Send(new GetFieldPlacementActivityList());
            var activitiesList = fieldPlacementActivities.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return Json(activitiesList);
        }




        public async Task<JsonResult> FieldPlacementActivityDeliverables(int? fieldPlacementActivityId)
        {
            var fieldPlacementActivities = await mediator.Send(new GetFieldPlacementActivityDeliverablesList()
            {
                FieldPlacementActivityId = fieldPlacementActivityId
            });

            var activitiesList = fieldPlacementActivities.Select(x => new
            {
                DeliverableName = x.Deliverable,
                DelivarableId = x.DeliverableId
            }).ToList();

            return Json(activitiesList);
        }

        [Operation("Add Field Site")]
        public IActionResult _AddFieldPlacementSite()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Operation("Add Field Site")]
        public async Task<IActionResult> _AddFieldPlacementSite(AddFieldPlacementSiteCommand model)
        {
            if (!ModelState.IsValid)
                return View(model);

            model.CreatedBy = User.Identity.Name;
            var siteId = await mediator.Send(model);

            return RedirectToAction(nameof(FieldPlacementSiteList));
        }

        public async Task<IActionResult> _EditFieldPlacementSite(int id)
        {
            var fieldPlacementSite = await mediator.Send(new GetFieldPlacementSiteList() { Id = id });

            var model = fieldPlacementSite.Select(x => new UpdateFieldPlacementSiteCommand
            {
                Id = x.Id,
                Name = x.Name,
                Location = x.Location,
                Building = x.Building,
                Level = x.Level,
                PostalAddress = x.PostalAddress,
                CountryId = x.CountryId,
                CountyId = x.CountyId,
                SubCountyId = x.SubCountyId,
                FacultyId = x.FacultyId

            }).SingleOrDefault();
            model.Faculties = await GetSelectListLoader(SelectItemType.Faculty).GetSelectListItemsAsync(null);
            model.Countries = await GetSelectListLoader(SelectItemType.Country).GetSelectListItemsAsync(null);
            model.Counties = await GetSelectListLoader(SelectItemType.County).GetSelectListItemsAsync(null);
            model.Subcounties = await GetSelectListLoader(SelectItemType.Subcounty).GetSelectListItemsAsync(null);
            return PartialView(model);
        }

        [HttpPost]
        public async Task<IActionResult> _EditFieldPlacementSite(UpdateFieldPlacementSiteCommand command)
        {
            command.CreatedBy = User.Identity.Name;
            var id = await mediator.Send(command);
            return RedirectToAction(nameof(FieldPlacementSiteList));

        }

        [Operation("View Field Site Details")]
        public async Task<IActionResult> _DetailsFieldPlacementSite(int id)
        {
            var facultyroledet = await mediator.Send(new GetFieldPlacementSiteList() { Id = id });

            return PartialView(facultyroledet.FirstOrDefault());
        }

        [Operation("Deactivate Field Site")]
        public ActionResult Delete(int id)
        {
            return PartialView("_DeleteFieldPlacementSite");

        }

        [Operation("View Field Sites")]
        public IActionResult FieldPlacementSiteList()
        {
            return View();
        }

        public async Task<List<SelectListItem>> GetFieldPlacementSites()
        {
            var fieldPlacementSites = await mediator.Send(new GetFieldPlacementSiteList());
            var selectListItems = fieldPlacementSites.Select(

                x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()

                }).ToList();
            return selectListItems;

        }

        [HttpPost]
        [Operation("View Field Sites")]
        public async Task<IActionResult> GetFieldPlacementSites(IDataTablesRequest request)
        {
            var fieldPlacementSite = await mediator.Send(new GetFieldPlacementSiteList());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? fieldPlacementSite
                : fieldPlacementSite.Where(_item => _item.Name.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, fieldPlacementSite.Count(), filteredData.Count(), dataPage);
            return new DataTablesJsonResult(response, true);
        }

        [Operation("Initiate Resident Field Placement")]
        public async Task<IActionResult> InitiateResidentFieldPlacement(int programEnrollmentId, long residentId, int semesterId, int programOfferId)
        {
            var semesterSchedule = await GetSemesterScheduleDates(programOfferId, semesterId);

            var model = new AddResidentFieldPlacementCommand()
            {
                SiteSupervisors = await GetFieldSiteSupervisor(),
                ResidentFieldPlacementActivities = await GetResidentFieldPlacementActivity(programEnrollmentId, semesterId),
                ResidentId = residentId,
                ProgramEnrollmentId = programEnrollmentId,
                SemesterStartDate = semesterSchedule.Item1,
                SemesterEndDate = semesterSchedule.Item2
            };

            return PartialView(model);
        }

        [HttpPost]
        [Operation("Initiate Resident Field Placement")]
        public async Task<IActionResult> InitiateResidentFieldPlacement(AddResidentFieldPlacementCommand model)
        {
            if (!ModelState.IsValid)
            {
                model.SiteSupervisors = await GetFieldSiteSupervisor();
                model.ResidentFieldPlacementActivities = await GetResidentFieldPlacementActivity(model.ProgramEnrollmentId, model.SemesterId);
                return PartialView(model);
            }
            model.CreatedBy = User.Identity.Name;

            var result = await mediator.Send(model);

            if (result.IsSuccessful)
                return PartialView("FieldPlacementResult", result);

            ModelState.AddModelError("Response", result.Message);
            return PartialView(model);
        }

        private async Task<Tuple<string,string>> GetSemesterScheduleDates(int programOfferId,int semesterId)
        {
            var programOfferSemesterSchedules = await mediator.Send(new GetProgramOfferSemesterSchedule { ProgramOfferId = programOfferId });

            var selectedSemesterSchedule = programOfferSemesterSchedules.Where(x => x.SemesterId == semesterId).FirstOrDefault();

            return new Tuple<string, string>(selectedSemesterSchedule.EstimatedStartDate, selectedSemesterSchedule.EstimatedEndDate);
        }

        private async Task<SelectList> GetResidentFieldPlacementActivity(int programEnrollmentId, int semesterId)
        {
            var activities = await mediator.Send(new GetResidentFieldPlacementActivities
            {
                ProgramEnrollmentId = programEnrollmentId,
                SemesterId = semesterId
            });

            return new SelectList(activities, "Id", "Activity", "Select", "ActivityType");
        }


        private async Task<SelectList> GetFieldSiteSupervisor()
        {
            var supervisors = await mediator.Send(new GetFieldSiteSupervisor { });

            return new SelectList(supervisors, "SupervisorId", "SupervisorName", "Select", "SiteName");
        }

        public async Task<IActionResult> GetFacultyDropDown()
        {
            var faculties = await mediator.Send(new GetFacultyForDropDownDisplayQuery { });

            return Json(faculties);
        }


        [Operation("View Resident Field Placement Info")]
        public async Task<IActionResult> ResidentFieldPlacementInfo(long residentActivityId)
        {
            var fieldplacementDetails = await mediator.Send(new GetResidentPlacementInformation
            {
                ResidentFieldPlacementActivityId = residentActivityId
            });

            return PartialView(fieldplacementDetails);
        }

        [HttpPost]
        public async Task<IActionResult> GetFieldPlacementSiteSupervisors(int facultyId, IDataTablesRequest request)
        {
            var fieldPlacementSitesupervisor = await mediator.Send(new GetFieldSiteSupervisor());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? fieldPlacementSitesupervisor
                : fieldPlacementSitesupervisor.Where(_item => _item.SiteName.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, fieldPlacementSitesupervisor.Count(), filteredData.Count(), dataPage);
            return new DataTablesJsonResult(response, true);
        }
        private ISelectListLoader GetSelectListLoader(SelectItemType selectItemType)
        {
            return selectListLoaders.Where(x => x.CanLoad(selectItemType)).SingleOrDefault();
        }

    }
}