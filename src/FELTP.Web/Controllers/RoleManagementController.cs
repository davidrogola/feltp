﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using UserManagement.Domain;
using UserManagement.ViewModels;

namespace FELTP.Web.Controllers
{
    [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
    [Authorize]
    public class RoleManagementController : Controller
    {
        private readonly RoleManager<ApplicationRole> roleManager;
        private readonly UserManager<ApplicationUser> userManager;
        ApplicationDbContext identityDbContext;
        ConcurrentDictionary<string, List<string>> rolePermissionMap;

        public RoleManagementController(RoleManager<ApplicationRole> _roleManager,
            UserManager<ApplicationUser> _userManager, ApplicationDbContext _identityDbContext,
            ConcurrentDictionary<string, List<string>> rolePermissionMap)
        {
            roleManager = _roleManager;
            userManager = _userManager;
            identityDbContext = _identityDbContext;
            this.rolePermissionMap = rolePermissionMap;
        }

        public IActionResult AddRole()
        {
            var addRoleView = new RoleViewModel()
            {
                PermissionsSelectList = GetOperationsSelectList()
            };

            return View(addRoleView);
        }

        [HttpPost]
        public IActionResult AddRole(RoleViewModel model)
        {
            if (!ModelState.IsValid)
            {
                model.PermissionsSelectList = GetOperationsSelectList();
                return View(model);
            }

            var createdById = Convert.ToInt32(userManager.GetUserId(User));
            var applicationRole = new ApplicationRole(model.RoleName, createdById);

            identityDbContext.Set<ApplicationRole>().Add(applicationRole);
            identityDbContext.SaveChanges();


            var rolePermissionMappings = model.Permissions
                 .Select(x => new RolePermissionMapping
                 {
                     RoleId = applicationRole.Id,
                     PermissionId = x,
                     CreatedBy = User.Identity.Name,
                     DateCreated = DateTime.Now
                 }).ToList();

            identityDbContext.Set<RolePermissionMapping>().AddRange(rolePermissionMappings);
            identityDbContext.SaveChanges();

            var permissionNames = identityDbContext.Set<Permission>().AsNoTracking()
                .Where(x => model.Permissions.Contains(x.Id)).Select(x => x.Name).ToList();

            rolePermissionMap.GetOrAdd(model.RoleName, permissionNames);

            return RedirectToAction(nameof(RolesList));
        }

        private SelectList GetOperationsSelectList()
        {
            var permissions = identityDbContext.Set<Permission>()
                .AsNoTracking().AsQueryable().Select(x => new OperationViewModel
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();
            return new SelectList(permissions, "Id", "Name");
        }

        public IActionResult RolesList()
        {
            return View();
        }

        [HttpPost]
        public IActionResult GetRoles(IDataTablesRequest request)
        {
            var roles = identityDbContext.Set<ApplicationRole>().AsNoTracking()
                .AsQueryable().Select(x => new RolesList
                {
                    CreatedBy = x.CreatedBy.UserName,
                    DateCreated = x.DateCreated.ToShortDateString(),
                    Id = x.Id,
                    RoleName = x.Name
                }).ToList();

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? roles
                : roles.Where(_item => _item.RoleName.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, roles.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        private List<SelectListItem> GetRolesSelectList()
        {
            var roles = identityDbContext.Set<ApplicationRole>().AsNoTracking()
              .AsQueryable().Select(x => new SelectListItem
              {
                  Text = x.Name,
                  Value = x.Id.ToString()
              }).ToList();

            return roles;
        }

        public IActionResult GetRolesDropDown()
        {
            return Json(GetRolesSelectList());
        }

        public IActionResult AddPermission()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddPermission(OperationViewModel model)
        {
            if (!ModelState.IsValid)
                return View();
            var createdBy = userManager.GetUserId(User);

            var permission = new Permission(model.Name, model.Comment, Convert.ToInt32(createdBy));
            identityDbContext.Set<Permission>().Add(permission);
            identityDbContext.SaveChanges();
            return RedirectToAction(nameof(PermissionsList));
        }

        public IActionResult PermissionsList()
        {
            return View();
        }

        public IActionResult GetPermissions(IDataTablesRequest request)
        {
            var operations = identityDbContext.Set<Permission>().AsNoTracking()
                .AsQueryable().Select(x => new OperationViewModel
                {
                    CreatedBy = x.CreatedBy.UserName != null ? x.CreatedBy.UserName : "System",
                    DateCreated = x.DateCreated.ToShortDateString(),
                    Id = x.Id,
                    Name = x.Name
                }).ToList();

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? operations
                : operations.Where(_item => _item.Name.ToLowerInvariant().Contains(request.Search.Value.ToLowerInvariant()));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, operations.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public IActionResult GetUserRoles(IDataTablesRequest request, int userId)
        {

            var userRoles = identityDbContext.Set<UserRoleView>().AsNoTracking()
              .Where(x => x.UserId == userId)
              .Select(x => new UserRoleViewModel
              {
                  AddedToRoleBy = x.AddedToRoleBy,
                  DateAddedToRole = x.DateAddedToRole.ToShortDateString(),
                  UserId = x.UserId,
                  RoleId = x.RoleId,
                  RoleName = x.RoleName,
                  Status = x.DateRemovedFromRole == null ? UserRoleStatus.Active.ToString()
                  : UserRoleStatus.InActive.ToString()
              }).ToList();

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? userRoles
                : userRoles.Where(_item => _item.RoleName.ToLowerInvariant().Contains(request.Search.Value.ToLowerInvariant()));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, userRoles.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }


        public IActionResult RolePermissions(int Id, string roleName)
        {
            return PartialView(new RoleOperationsViewModel { RoleId = Id, RoleName = roleName });
        }

        public IActionResult GetRolePermissions(IDataTablesRequest request, int roleId)
        {
            var operations = identityDbContext.Set<RolePermissionsView>().AsNoTracking()
              .Where(x => x.RoleId == roleId)
              .Select(x => new RoleOperationsViewModel
              {
                  Description = x.Description,
                  Permission = x.Permission,
                  RoleId = x.RoleId,
                  RoleName = x.RoleName
              }).ToList();

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? operations
                : operations.Where(_item => _item.Permission.ToLowerInvariant().Contains(request.Search.Value.ToLowerInvariant()));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, operations.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public IActionResult DeActivateUserRole(int id, int roleId)
        {
            var userRole = identityDbContext.Set<UserRoleView>().Where(x => x.RoleId == roleId && x.UserId == id)
                .Select(x => new UserRoleViewModel
                {
                    UserId = x.UserId,
                    RoleId = x.RoleId,
                    RoleName = x.RoleName
                }).SingleOrDefault();

            if (userRole == null)
                Content("User role not found");

            return PartialView(userRole);
        }

        [HttpPost]
        public async Task<IActionResult> DeactivateUserRole(UserRoleView roleView)
        {
            var userRole = identityDbContext.Set<ApplicationUserRole>().Where(x => x.UserId == roleView.UserId &&
            x.RoleId == roleView.RoleId).SingleOrDefault();

            var currentUserId = Convert.ToInt16(userManager.GetUserId(User));

            var user = await userManager.FindByIdAsync(roleView.UserId.ToString());
            userRole.RemoveUserFromRole(currentUserId);

            identityDbContext.Set<ApplicationUserRole>().Attach(userRole);
            identityDbContext.Entry(userRole).State = EntityState.Modified;

            identityDbContext.SaveChanges();

            var claims = identityDbContext.Set<ApplicationRole>().AsNoTracking()
                       .Where(x => x.Id == roleView.RoleId)
                       .Select(x => new Claim(CustomClaimType.Role.ToString(), x.Name)).ToList();

            await userManager.RemoveClaimsAsync(user, claims);

            return RedirectToAction("ViewUserDetails", "UserManagement", new { Id = roleView.UserId });
        }

        public IActionResult AddUserToRole(int userId)
        {
            var userRoles = identityDbContext.Set<ApplicationUserRole>()
                .Where(x => x.UserId == userId)
                .Select(x => x.RoleId.ToString()).ToList();

            var roleSelectList = GetRolesSelectList();

            var allRoles = roleSelectList.Select(x => x.Value).ToList();

            var newRoles = allRoles.Except(userRoles);

            var model = new AddUserToRoleViewModel
            {
                Roles = roleSelectList.Where(x => newRoles.Contains(x.Value)).ToList(),
                UserId = userId
            };

            return PartialView(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddUserToRole(AddUserToRoleViewModel model)
        {
            var createdBy = userManager.GetUserId(User);

            var user = await userManager.FindByIdAsync(model.UserId.ToString());
            if (user == null)
                return Content("User not found");

            var userRoles = model.RoleIds.Select(x => new ApplicationUserRole
            {
                DateAddedToRole = DateTime.Now,
                RoleId = x,
                UserAddedToRoleById = Convert.ToInt16(createdBy),
                UserId = model.UserId,
            }).ToList();

            identityDbContext.Set<ApplicationUserRole>().AddRange(userRoles);
            identityDbContext.SaveChanges();

            var claims = identityDbContext.Set<ApplicationRole>().AsNoTracking()
                .Where(x => model.RoleIds.Contains(x.Id))
                .Select(x => new Claim(CustomClaimType.Role.ToString(), x.Name)).ToList();

            await userManager.AddClaimsAsync(user, claims);

            return RedirectToAction("ViewUserDetails", "UserManagement", new { Id = model.UserId });
        }


        public IActionResult ActivateUserRole(int id, int roleId)
        {
            var userRole = identityDbContext.Set<UserRoleView>().Where(x => x.RoleId == roleId && x.UserId == id)
                .Select(x => new UserRoleViewModel
                {
                    UserId = x.UserId,
                    RoleId = x.RoleId,
                    RoleName = x.RoleName
                }).SingleOrDefault();

            if (userRole == null)
                Content("User role not found");

            return PartialView(userRole);
        }

        [HttpPost]
        public async Task<IActionResult> ActivateUserRole(UserRoleView model)
        {
            var userRole = identityDbContext.UserRoles.Where(x => x.UserId == model.UserId && x.RoleId == model.RoleId).SingleOrDefault();

            if (userRole == null)
                return Content("User role not found");

            userRole.ActivateRole();
            identityDbContext.Set<ApplicationUserRole>().Attach(userRole);
            identityDbContext.Entry(userRole).State = EntityState.Modified;
            identityDbContext.SaveChanges();

            var user = await userManager.FindByIdAsync(model.UserId.ToString());

            var claims = identityDbContext.Set<ApplicationRoleClaim>().AsNoTracking()
                .Where(x => x.RoleId == model.RoleId)
                .Select(x => new Claim(x.ClaimType, x.ClaimValue)).ToList();

            await userManager.AddClaimsAsync(user, claims);

            return RedirectToAction("ViewUserDetails", "UserManagement", new { Id = model.UserId });
        }

        public IActionResult EditRole(int Id, string roleName)
        {
            var selectedOperations = identityDbContext.Set<RolePermissionMapping>()
                .Where(x => x.RoleId == Id).ToList();

            var editRoleModel = new RoleViewModel
            {
                Permissions = selectedOperations.Select(x => x.PermissionId).ToList(),
                RoleName = roleName,
                PermissionsSelectList = GetOperationsSelectList(),
                RoleId = Id
            };

            return PartialView(editRoleModel);
        }

        [HttpPost]
        public async Task<IActionResult> EditRole(RoleViewModel model)
        {
            var rolePermissionMappings = identityDbContext.Set<RolePermissionMapping>()
                .Where(x => x.RoleId == model.RoleId).ToList();

            var role = await roleManager.FindByIdAsync(model.RoleId.ToString());
            if (role == null)
                return Content("Role not found");

            role.Update(model.RoleName);
            await roleManager.UpdateAsync(role);

            var existingPermissions = rolePermissionMappings.Select(x => x.PermissionId).ToList();

            var newPermissions = model.Permissions?.Except(existingPermissions);

            if (newPermissions.Any())
            {

                var roleMappings = newPermissions.Select(x => new RolePermissionMapping
                {
                    RoleId = role.Id,
                    CreatedBy = User.Identity.Name,
                    PermissionId = x,
                    DateCreated = DateTime.Now
                }).ToList();

                identityDbContext.Set<RolePermissionMapping>().AddRange(roleMappings);
                identityDbContext.SaveChanges();

                var newPermissionNames = identityDbContext.Set<Permission>()
                    .Where(x => newPermissions.Contains(x.Id)).Select(x => x.Name).ToList();

                rolePermissionMap.AddOrUpdate(role.Name, newPermissionNames, (key, addValue) =>
                {
                    addValue.AddRange(newPermissionNames);
                    return addValue;
                });

            }


            var removedPermissions = model.Permissions == null ? existingPermissions : existingPermissions.Except(model.Permissions);

            if (removedPermissions.Any())
            {
                var removedRoleMappings = identityDbContext.Set<RolePermissionMapping>()
                    .Where(x => removedPermissions.ToList().Contains(x.PermissionId))
                    .Include(x => x.Permission)
                    .ToList();

                var removedPemissionNames = removedRoleMappings.Select(x => x.Permission.Name).ToList();

                identityDbContext.Set<RolePermissionMapping>().RemoveRange(removedRoleMappings);
                identityDbContext.SaveChanges();

                rolePermissionMap.AddOrUpdate(role.Name, removedPemissionNames, (key, addValue) =>
                {
                    addValue.RemoveAll(x => removedPemissionNames.Contains(x));
                    return addValue;
                });
            }

            return RedirectToAction(nameof(RolesList));
        }
    }
}