﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.ResidentManagement.Queries;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using MediatR;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FELTP.Web.Controllers
{
    public class AlumniManagementController : Controller
    {
        private readonly IMediator _mediator;
        public AlumniManagementController(IMediator mediator)
        {
            _mediator = mediator;
        }

        public IActionResult AlumniList()
        {      
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> GetAlumniList(IDataTablesRequest request)
        {
            var alumni = await _mediator.Send(new GetAlumniQuery());

            var filteredData = string.IsNullOrWhiteSpace(request.Search.Value)
                ? alumni
                : alumni.Where(_item => _item.RegistrationNumber.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, alumni.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }
    }
}
