﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using Application.ProgramManagement.Services;
using Application.ResidentManagement.Queries;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using FELTP.Web.Services;
using Hangfire;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using UserManagement.Services.Authorization;

namespace FELTP.Web.Controllers
{
    [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
    [Authorize]
    public class GraduationManagementController : Controller
    {
        IMediator mediator;
        ISelectListLoader programSelectListLoader;
        IEnumerable<ISelectListLoader> selectListLoaders;
        IValidateGraduandEligibilty graduandEligibiltyValidator;

        public GraduationManagementController(IMediator _mediator,
            IEnumerable<ISelectListLoader> _selectListLoaders, 
            IValidateGraduandEligibilty _graduandEligibiltyValidator)
        {
            mediator = _mediator;
            selectListLoaders = _selectListLoaders;
            programSelectListLoader = selectListLoaders.Where(x=>x.CanLoad(SelectItemType.Program)).SingleOrDefault();
            graduandEligibiltyValidator = _graduandEligibiltyValidator;
        }
        public IActionResult Index()
        {
            return View();
        }

        [Operation("Add Graduation Requirement")]
        public async Task<IActionResult> AddNewGraduationRequirement()
        {
            var model = new AddGraduationRequirementCommand
             {
                ProgramSelectList = await programSelectListLoader.GetSelectListAsync(null)
             };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Operation("Add Graduation Requirement")]
        public async Task<IActionResult> AddNewGraduationRequirement(AddGraduationRequirementCommand model)
        {
            if (!ModelState.IsValid)
            {
                model.ProgramSelectList = await programSelectListLoader.GetSelectListAsync(null);
                return View(model);
            }

            model.CreatedBy = User.Identity.Name;
            var graduationRequirementId = await mediator.Send(model);

            return RedirectToAction(nameof(GraduationRequirementList));
        }

        [Operation("View Graduation Requirements List")]
        public IActionResult GraduationRequirementList()
        {
            return View();
        }

        [HttpPost]
        [Operation("View Graduation Requirements List")]
        public async Task<IActionResult> GetGraduationRequirements(IDataTablesRequest request)
        {
            var graduationRequirements = await mediator.Send(new GetGraduationRequirementList());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? graduationRequirements
                : graduationRequirements.Where(_item => _item.Program.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);

            var response = DataTablesResponse.Create(request, graduationRequirements.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }


        public async Task<IActionResult> ResidentGraduationList()
        {
            var model = new FilterResident()
            {
                ProgramSelectList = await programSelectListLoader.GetSelectListAsync(null)
            };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> GetResidentGraduationList(IDataTablesRequest request, int programOfferId)
        {
            var graduationList = await mediator.Send(new GetResidentGraduationList() { ProgramOfferId = programOfferId });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? graduationList
                : graduationList.Where(_item => _item.ProgramName.ToLower().Contains(request.Search.Value.ToLower()));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);

            var response = DataTablesResponse.Create(request, graduationList.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> ResidentGraduationDetails(long graduateId)
        {
            var graduationDetails = await mediator.Send(new GetResidentGraduateEligibiltyItems { GraduateId = graduateId });

            return View(graduationDetails);
        }

        public async Task<IActionResult> _EditGraduationRequirement(int id)
        {
            var graduationRequirementList = await mediator.Send(new GetGraduationRequirementList { Id = id });

            var graduationRequirement = graduationRequirementList.SingleOrDefault();

            var updateGraduationRequirement = new UpdateGraduationRequirementCommand
            {
                NumberOfActivitiesRequired = graduationRequirement.NumberOfActivitiesRequired,
                NumberOfDidacticUnitsRequired = graduationRequirement.NumberOfDidacticModulesRequired,
                NumberOfThesisDefenceDeliverables = graduationRequirement.NumberOfThesisDefenceDeliverables,
                PassingGrade = graduationRequirement.PassingGrade,
                ProgramId = graduationRequirement.ProgramId,
                ProgramSelectList = await programSelectListLoader.GetSelectListAsync(null)
            };

            return PartialView(updateGraduationRequirement);
        }

        [HttpPost]
        [Operation("Add Graduation Requirement")]
        public async Task<IActionResult> _EditGraduationRequirement(UpdateGraduationRequirementCommand command)
        {
            if (!ModelState.IsValid)
            {
                command.ProgramSelectList = await programSelectListLoader.GetSelectListAsync(null);
                return PartialView(command);
            }

            BackgroundJob.Schedule(() => graduandEligibiltyValidator.UpdateGraduandsEligiblityCriteria(command),TimeSpan.FromSeconds(30));

            return PartialView("_EditGraduationRequirementResult");
        }

    }
}