﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.ResidentManagement.Commands;
using Application.ResidentManagement.Queries;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using FELTP.Web.Services;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using ResidentManagement.Domain.Ethics;
using UserManagement.Services.Authorization;

namespace FELTP.Web.Controllers
{
    public class EthicsManagementController : Controller
    {
        IEnumerable<ISelectListLoader> selectListLoader;
        ISelectListLoader programSelectListLoader;
        IMediator mediator;
        public EthicsManagementController(IEnumerable<ISelectListLoader> _selectListLoader, IMediator _mediator)
        {
            mediator = _mediator;
            selectListLoader = _selectListLoader;
            programSelectListLoader = selectListLoader.FirstOrDefault(x => x.CanLoad(SelectItemType.Program));
        }
        public IActionResult Index()
        {
            return View();
        }

        [Operation("Add Ethics Approval")]
        public async Task<IActionResult> EthicsApprovalForm()
        {
            var model = new AddEthicsApprovalCommand()
            {
                ProgramSelectList = await programSelectListLoader.GetSelectListAsync(null),

            };
            return View(model);
        }

        [HttpPost]
        [Operation("Add Ethics Approval")]
        public async Task<IActionResult> EthicsApprovalForm(AddEthicsApprovalCommand model, List<EthicsApprovalItemCommand> EthicsApprovalItems)
        {
            model.EthicsApprovalItems = EthicsApprovalItems;
            model.CreatedBy = User.Identity.Name;
            if (!ModelState.IsValid)
            {
                model.ProgramSelectList = await programSelectListLoader.GetSelectListAsync(null);
                return View(model);
            }

            var result = await mediator.Send(model);

            return RedirectToAction(nameof(EthicsApprovalList));
        }

        [Operation("Add Ethics Approval Committee")]      
        public async Task<IActionResult> EthicsApprovalCommitteePartial(long? residentDeliverableId)
        {

            var coAuthors = await mediator.Send(new GetResidentDeliverableCoauthors()
            {
                ResidentDeliverableId = residentDeliverableId
            });
            if (coAuthors.Any())
            {
                var ethicsCommitteeModel = coAuthors.Select(x => new EthicsApprovalItemCommand
                {
                    Author = x.AuthorName,
                    CommitteeName = x.CommitteeName,
                    CommitteeId = x.CommitteeId
                }).ToList();
                return PartialView(ethicsCommitteeModel);
            }

            var committees = await mediator.Send(new GetEthicsApprovalCommittee());
            var model = committees.Select(x => new EthicsApprovalItemCommand
            {
                CommitteeId = x.Id,
                CommitteeName = x.Name
            }).ToList();

            return PartialView(model);
        }

        [Operation("View Ethics Approval List")]
        public IActionResult EthicsApprovalList()
        {
            return View();
        }

        [Operation("View Ethics Approval List")]
        public async Task<IActionResult> GetEthicsApprovalList(IDataTablesRequest request)
        {
            var ethicsApprovals = await mediator.Send(new GetEthicsApprovalQuery());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? ethicsApprovals
                : ethicsApprovals.Where(_item => _item.ResidentName.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);

            var response = DataTablesResponse.Create(request, ethicsApprovals.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        [Operation("View Ethics Approval Details")]
        public async Task<IActionResult> EthicsApprovalDetails(long Id)
        {
            var ethicsApproval = await mediator.Send(new GetEthicsApprovalQuery() { Id = Id });
            return View(ethicsApproval.SingleOrDefault());
        }

        public async Task<IActionResult> GetCommitteeByEthicsApprovalId(IDataTablesRequest request,
            long ethicsApprovalId)
        {
            var ethicsApprovalItems = await mediator.Send(new GetEthicsApprovalItems() { EthicsApprovalId = ethicsApprovalId });

            var distictCommitees = ethicsApprovalItems.GroupBy(x => x.CommitteeId)
                .Select(x => x.FirstOrDefault())
                .Select(x => new { x.Committee, x.CommitteeId, x.EthicsApprovalId, x.DateCreated });

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? distictCommitees
                : distictCommitees.Where(_item => _item.Committee.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);

            var response = DataTablesResponse.Create(request, distictCommitees.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        [Operation("View Ethics Approval Submissions")]
        public async Task<IActionResult> ViewEthicsApprovalSubmissions(long id, int committeeId)
        {
            var ethicsApproval = await mediator.Send(new GetEthicsApprovalQuery { Id = id });

            var committees = await mediator.Send(new GetEthicsApprovalCommittee { Id = committeeId });

            var submissionViewModel = new EthicsSubmissionViewModel
            {
                Committee = committees.SingleOrDefault().Name,
                CommitteeId = committeeId,
                DeliverableName = ethicsApproval.SingleOrDefault().DeliverableName,
                EthicsApprovalId = id,
                EventTypeId = ethicsApproval.SingleOrDefault().EventTypeId,
                SubmissionDueDate = ethicsApproval.SingleOrDefault().SubmissionDueDate,
                Title = ethicsApproval.SingleOrDefault().Title
            };

            return View(submissionViewModel);
        }

        [Operation("View Ethics Approval Committee Submissions")]
        public async Task<IActionResult> GetEthicsApprovalCommitteeSubmissions(IDataTablesRequest request, long id,
            int committeeId)
        {
            var ethicsApprovalItems = await mediator.Send(new GetEthicsApprovalItems() { EthicsApprovalId = id });

            ethicsApprovalItems = ethicsApprovalItems.Where(x => x.EthicsApprovalCommitteeId == committeeId).ToList();

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? ethicsApprovalItems
                : ethicsApprovalItems.Where(_item => _item.Committee.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);

            var response = DataTablesResponse.Create(request, ethicsApprovalItems.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        [Operation("Download Submission Document")]
        public async Task<IActionResult> DownloadSubmissionDocument(long id)
        {
            var documents = await mediator.Send(new GetEthicsApprovalDocumentQuery { Id = id });

            var doc = documents.SingleOrDefault();

            return File(doc.Document, doc.ContentType, doc.FileName);

        }

        [Operation("Evaluate Ethics Approval Submission")]
        public async Task<IActionResult> _EvaluateEthicsApprovalSubmission(long id)
        {
            var ethicsApprovalItem = await mediator.Send(new GetEthicsApprovalItems { EthicsApprovalItemId = id });

            var item = ethicsApprovalItem.SingleOrDefault();

            var model = new EvaluateEthicsApprovalSubmissionCommand()
            {
                ApprovalStatus = (EthicsApprovalStatus)Enum.Parse(typeof(EthicsApprovalStatus), item.ApprovalStatus),
                EthicsApprovalItemId = item.Id,
                EthicsApprovalCommitteeId = item.EthicsApprovalCommitteeId,
                EthicsApprovalId = item.EthicsApprovalId
            };
            return PartialView(model);
        }

        [HttpPost]
        [Operation("Evaluate Ethics Approval Submission")]
        public async Task<IActionResult> _EvaluateEthicsApprovalSubmission(EvaluateEthicsApprovalSubmissionCommand model)
        {
            if (!ModelState.IsValid)
                return PartialView(model);

            model.EvaluatedBy = User.Identity.Name;
            model.UpdatedBy = User.Identity.Name;

            var result = await mediator.Send(model);

            return RedirectToAction(nameof(ViewEthicsApprovalSubmissions),
                new { id = model.EthicsApprovalId, committeeId = model.EthicsApprovalCommitteeId });
        }

        [Operation("Override Ethics Approval Outcome")]
        public async Task<IActionResult> OverrideEthicsApprovalOutcome(long id)
        {
            var ethicsApproval = await mediator.Send(new GetEthicsApprovalQuery { Id = id });

            var approvalModel = ethicsApproval.SingleOrDefault();

            var ethicsApprovalOutcome = new OverrideEthicsApprovalOutcomeCommand
            {
                ApprovalStatus = (EthicsApprovalStatus)Enum.Parse(typeof(EthicsApprovalStatus), approvalModel.ApprovalStatus),
                Deliverable = approvalModel.DeliverableName,
                EthicsApprovalId = approvalModel.Id,
                EventTypeId = approvalModel.EventTypeId,
                Title = approvalModel.Title,
                Message = approvalModel.ApprovalStatus == EthicsApprovalStatus.Approved.ToString() ?
                "Ethics approval status is approved hence cannot be altered" : String.Empty
            };
            return PartialView(ethicsApprovalOutcome);
        }

        [HttpPost]
        [Operation("Override Ethics Approval Outcome")]
        public async Task<IActionResult> OverrideEthicsApprovalOutcome(OverrideEthicsApprovalOutcomeCommand model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(model);
            }
            model.OverridenBy = User.Identity.Name;
            var result = await mediator.Send(model);

            return RedirectToAction(nameof(EthicsApprovalDetails), new { id = model.EthicsApprovalId });
        }


    }
}