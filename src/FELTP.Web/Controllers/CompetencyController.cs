﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application;
using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using FELTP.Web.Services;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using UserManagement.Services.Authorization;

namespace FELTP.Web.Controllers
{
    [Authorize]
    public class CompetencyController : Controller
    {
        IMediator mediator;
        IEnumerable<ISelectListLoader> selectListLoaders;

        public CompetencyController(IMediator _mediator, IEnumerable<ISelectListLoader> _selectListLoaders)
        {
            mediator = _mediator;
            selectListLoaders = _selectListLoaders;
        }

        [Operation("Add Competency")]
        public ActionResult _AddCompetency()
        {
            return PartialView();
        }

        [HttpPost]
        [Operation("Add Competency")]
        public async Task<ActionResult> _AddCompetency(AddCompetencyCommand addCompetency)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(addCompetency);
            }
            addCompetency.CreatedBy = User.Identity.Name;
            int competencyId = await mediator.Send(addCompetency);

            if(competencyId != default(int))              
            return PartialView("CompetencyResult", "Competency created successfully");

            ModelState.AddModelError("Response", "An error occured while creating competency");
            return PartialView(addCompetency);

           
        }

        public async Task<IActionResult> _EditCompetency(int id)
        {
            var competency = await mediator.Send(new GetCompetencyList() { Id = id });

            var model = competency.Select(x => new UpdateCompetencyCommand
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                DurationInWeeks = x.DurationInWeeks,
                DeliverableId = x.DeliverableId

            }).SingleOrDefault();
            model.Deliverables = await GetSelectListLoader(SelectItemType.Deliverable).GetSelectListItemsAsync(null);

            return PartialView(model);
        }

        [HttpPost]
        public async Task<IActionResult> _EditCompetency(UpdateCompetencyCommand command)
        {
            command.CreatedBy = User.Identity.Name;
            var id = await mediator.Send(command);
            return RedirectToAction(nameof(CompetencyList));

        }

        [HttpPost]
        [Operation("View Competency List")]
        public async Task<IActionResult> GetCompetencies(IDataTablesRequest request)
        {
            var competencies = await mediator.Send(new GetCompetencyList());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? competencies
                : competencies.Where(_item => _item.Name.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, competencies.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        [Operation("View Competency List")]
        public ActionResult CompetencyList()
        {
            return View();
        }

        [Operation("Edit Competency")]
        public IActionResult Edit(int id)
        {
            return PartialView("_EditCompetency");
        }

        [Operation("Deactivate Competency")]
        public IActionResult Delete(int id)
        {
            return PartialView("_DeleteCompetency");

        }

        public IActionResult AddCompetencyDeliverable()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddCompetencyDeliverable(AddCompetencyDeliverableCommand model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var id = await mediator.Send(model);
            return RedirectToAction(nameof(CompetencyDeliverableList));
        }

        public async Task<JsonResult> GetCompetencyDropdown()
        {
            var comptencies = await mediator.Send(new GetCompetencyList());
            var selectListItems = comptencies.Select(
                x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                }).ToList();

            return Json(selectListItems);
        }

        [Operation("View Competency Deliverable")]
        public IActionResult _ViewCompetencyDeliverable(int Id, string name)
        {
            return PartialView(new CompetencyDeliverableViewModel { CompetencyId = Id, Competency = name });
        }



        [HttpPost]
        [Operation("View Competency Deliverable")]
        public async Task<IActionResult> GetCompetencyDeliverables(int ? competencyId,IDataTablesRequest request)
        {
            var competencydeliverables = await mediator.Send(new GetCompetencyDeliverableList() { CompetencyId = competencyId  });

            var activeDeliverables = competencydeliverables.Where(x => x.Status == Status.Active.ToString());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? activeDeliverables
                : activeDeliverables.Where(_item => _item.Competency.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, competencydeliverables.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        [Operation("View Competency Deliverable")]
        public ActionResult CompetencyDeliverableList()
        {
            return View();
        }

        private ISelectListLoader GetSelectListLoader(SelectItemType selectItemType)
        {
            return selectListLoaders.Where(x => x.CanLoad(selectItemType)).SingleOrDefault();
        }
    }
}