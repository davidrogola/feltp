﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using Application.ResidentManagement.Commands;
using Application.ResidentManagement.Queries;
using AutoMapper.Configuration.Conventions;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Persistance.LookUpRepo.UnitOfWork;
using ResidentManagement.Domain;
using UserManagement.Services.Authorization;

namespace FELTP.Web.Controllers
{
    public class DeliverableController : Controller
    {
        IMediator mediator;

        public DeliverableController(IMediator _mediator)
        {
            mediator = _mediator;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Operation("Add Deliverable")]
        public ActionResult _AddDeliverable()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Operation("Add Deliverable")]
        public async Task<ActionResult> _AddDeliverable(AddDeliverableCommand addDeliverable)
        {
            if (!ModelState.IsValid)
                return View(addDeliverable);
            addDeliverable.CreatedBy = User.Identity.Name;
            int deliverableId = await mediator.Send(addDeliverable);
            return RedirectToAction(nameof(DeliverableList));
        }

        [Operation("Edit Deliverable")]
        public async Task<IActionResult> _EditDeliverable(int id)
        {
            var Deliverable = await mediator.Send(new GetDeliverableList() {Id = id});

            return PartialView(Deliverable.FirstOrDefault());
        }

        [HttpPost]
        [Operation("Edit Deliverable")]
        public async Task<IActionResult> _EditDeliverable(UpdateDeliverableCommand command)
        {
            var id = await mediator.Send(command);

            return RedirectToAction(nameof(DeliverableList));
        }

        [Operation("View Deliverable Details")]
        public async Task<IActionResult> _DetailsDeliverable(int id)
        {
            var Deliverable = await mediator.Send(new GetDeliverableList() {Id = id});

            return PartialView(Deliverable.FirstOrDefault());
        }

        [Operation("Deactivate Deliverable")]
        public ActionResult _DeactivateDeliverable(int id)
        {
            var model = new DeactivateDeliverableCommand
            {
                Id = id,
                DeactivatedBy = User.Identity.Name
            };
            return PartialView("_DeactivateDeliverable",model);
        }
        
        [Operation("Deactivate Deliverable")]
        [HttpPost]
        public async Task<IActionResult> _DeactivateDeliverable(DeactivateDeliverableCommand command)
        {
            await mediator.Send(command);
            return RedirectToAction(nameof(DeliverableList));
        }

        [Operation("View Deliverable List")]
        public ActionResult DeliverableList()
        {
            return View();
        }

        [HttpPost]
        [Operation("View Deliverable List")]
        public async Task<IActionResult> GetDeliverables(IDataTablesRequest request)
        {
            var deliverables = await mediator.Send(new GetDeliverableList());

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? deliverables
                : deliverables.Where(_item => _item.Name.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, deliverables.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        [Operation("View Resident Deliverable")]
        public async Task<List<SelectListItem>> GetResidentDeliverable(int programEnrollmentId, int? semesterId,
            string status = null)
        {
            var deliverables = await mediator.Send(new GetResidentDeliverables
                {ProgramEnrollmentId = programEnrollmentId, SemesterId = semesterId});
            if (!String.IsNullOrEmpty(status))
            {
                deliverables = deliverables.Where(x => x.ProgressStatus == status).ToList();
            }

            var selectList = deliverables.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return selectList;
        }


        public async Task<JsonResult> GetResidentDeliverablesDropDown(int programEnrollmentId, int? semesterId,
            string status = null)
        {
            var selectList = await GetResidentDeliverable(programEnrollmentId, semesterId, status);
            return Json(selectList);
        }

        public async Task<JsonResult> GetDeliverablesListDropdown()
        {
            var deliverables = await mediator.Send(new GetDeliverableList { });
            var deliverablesSelectList = deliverables.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return Json(deliverablesSelectList);
        }

        [Operation("Submit Resident Deliverable")]
        public async Task<IActionResult> SubmitResidentDeliverable(int programEnrollmentId, long residentId,
            int semesterId, int programOfferId)
        {
            var model = new AddResidentDeliverableItemCommand
            {
                ResidentDeliverables = await GetResidentDeliverable(programEnrollmentId, semesterId),
                ResidentId = residentId,
                ProgramOfferId = programOfferId,
                CoAuthors = await GetEthicsApprovalCommittee()
            };

            return PartialView(model);
        }

        [HttpPost]
        [Operation("Submit Resident Deliverable")]
        public async Task<IActionResult> SubmitResidentDeliverable(AddResidentDeliverableItemCommand model,
            IFormFile DeliverableFile)
        {
            if (DeliverableFile == null)
                ModelState.AddModelError("Error", "Please select the file to upload");

            if (!ModelState.IsValid)
            {
                model.ResidentDeliverables = await GetResidentDeliverable(model.ProgramEnrollmentId, model.SemesterId);
                return PartialView(model);
            }

            model.CreatedBy = User.Identity.Name;
            model.DateCreated = DateTime.Now;

            var result = await mediator.Send(model);

            if (result.IsSuccessful)
            {
                var deliverableFileItem = new AddDeliverableItemFileRequest
                {
                    DeliverableFile = DeliverableFile,
                    DeliverableItemId = result.DeliverableItemId
                };

                await mediator.Send(deliverableFileItem);

                return PartialView("SubmitDeliverableResult", result);
            }

            ModelState.AddModelError("Response", result.Message);
            return PartialView(model);
        }


        [Operation("View Ethics Approval committee")]
        private async Task<List<AddCoauthorCommand>> GetEthicsApprovalCommittee()
        {
            var committees = await mediator.Send(new GetEthicsApprovalCommittee());
            var command = committees.Select(x => new AddCoauthorCommand
            {
                Committee = x.Name,
                CommitteeId = x.Id
            }).ToList();
            return command;
        }

        [Operation("View Resident Deliverable Submissions")]
        public IActionResult ViewSubmissions(long deliverableId, string name)
        {
            return PartialView(new ResidentDeliverableSubmissionViewModel
            {
                ResidentDeliverableId = deliverableId,
                DeliverableName = name
            });
        }

        [HttpPost]
        [Operation("View Resident Deliverable Submissions")]
        public async Task<IActionResult> GetResidentDeliverableSubmissions(long deliverableId,
            IDataTablesRequest request)
        {
            var submissions = await mediator.Send(new GetResidentDeliverableSubmissions()
            {
                ResidentDeliverableId = deliverableId
            });
            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? submissions
                : submissions.Where(_item => _item.Title.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, submissions.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        [Operation("Download Submission")]
        public async Task<IActionResult> DownloadSubmission(int submissionId)
        {
            var submissionFile = await mediator.Send(new GetDeliverableSubmission {SubmissionId = submissionId});

            return File(submissionFile.File, submissionFile.ContentType, submissionFile.FileName);
        }

        [Operation("Input Resident Deliverable Scores")]
        public async Task<IActionResult> InputResidentDeliverableScores(int enrollmentId, int semesterId,
            long residentId)
        {
            var deliverables = await mediator.Send(new GetResidentUnitActivityDeliverableQuery
            {
                ProgramEnrollmentId = enrollmentId,
                SemesterId = semesterId,
            });

            var model = new AddResidentDeliverableScoreCommand
            {
                ProgramEnrollmentId = enrollmentId,
                SemesterId = semesterId,
                ResidentDeliverables = deliverables
                    .Where(x => x.ProgressStatus == DeliverableStatus.Completed.ToString())
                    .Select(x => new ResidentDeliverableDetail
                    {
                        ActivityName = x.ActivityName,
                        Code = x.Code,
                        Deliverable = x.Deliverable,
                        ResidentDeliverableId = x.ResidentDeliverableId,
                        ResidentFieldPlacementActivityId = x.ResidentFieldPlacementActivityId,
                        ResidentUnitId = x.ResidentUnitId,
                        UnitName = x.UnitName,
                        Score = x.Score.HasValue ? Math.Floor(x.Score.Value) : (decimal?) null,
                        Submitted = x.Score.HasValue,
                        StrScore = x.Score.HasValue ? x.Score.Value.ToString() : null
                    }).ToList(),
                ResidentId = residentId
            };

            return PartialView(model);
        }

        [HttpPost]
        [Operation("Input Resident Deliverable Scores")]
        public async Task<IActionResult> InputResidentDeliverableScores(AddResidentDeliverableScoreCommand model)
        {
            if (!ModelState.IsValid)
                return PartialView(model);

            model.CreatedBy = User.Identity.Name;
            model.DateCreated = DateTime.Now;

            var result = await mediator.Send(model);
            if (result.IsSuccessful)
                return PartialView("ResidentDeliverableScoreResult", result);

            ModelState.AddModelError("Response", result.Message);
            return PartialView(model);
        }

        public async Task<IActionResult> GetResidentDeliverables(long residentActivityId)
        {
            var deliverableList = await mediator.Send(new GetResidentDeliverables
                {ResidentFieldPlacementActivityId = residentActivityId});

            return Json(deliverableList);
        }

        public IActionResult ResidentUnitDeliverables(long residentUnitId, string unitName)
        {
            return PartialView(new ResidentUnitActivityDeliverableViewModel()
                {UnitName = unitName, ResidentUnitId = residentUnitId});
        }

        public async Task<IActionResult> GetResidentUnitDeliverables(long residentUnitId, IDataTablesRequest request)
        {
            var unitDeliverables = await mediator.Send(new GetResidentUnitDeliverables()
            {
                ResidentUnitId = residentUnitId
            });
            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value)
                ? unitDeliverables
                : unitDeliverables.Where(_item => _item.Deliverable.Contains(request.Search.Value));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            var response = DataTablesResponse.Create(request, unitDeliverables.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        public async Task<IActionResult> GetResidentDeliverableById(long Id)
        {
            var deliverable = await mediator.Send(new GetResidentDeliverable {Id = Id});

            return Json(deliverable);
        }

        public IActionResult SubmitDeliverableFeedback(int deliverableId, long residentId, string name)
        {
            return PartialView(new SubmitDeliverableEvaluationCommand
                {DeliverableName = name, Id = deliverableId, ResidentId = residentId});
        }
        
        [HttpPost]
        public async Task<IActionResult> SubmitDeliverableFeedback(SubmitDeliverableEvaluationCommand command)
        {
            if (!ModelState.IsValid)
                return PartialView(command);
            command.Faculty = User.Identity.Name; 
            
            var message = await mediator.Send(command);
            return PartialView("SubmitDeliverableFeedbackResult",message);
        }
        
        
    }
}