﻿$(document).ready(function () {
    getCountries()
});

function getCountries ()
{
    var url = "/LookUp/GetCountries";
    $.getJSON(url, {}, function (data) {
        var items = "";
        var selectedValue ;
        $.each(data, function (i, comp) {
            items += "<option value='" + comp.Value + "'>" + comp.Text + "</option>";
            if (comp.Text.includes("Kenya"))
                selectedValue = comp.Value;
        });
        $("#CountryId").html(items).selectpicker("refresh");
        $("#CountryId").val(selectedValue);
        $("#CountryId").change();
        $("#CountryId").selectpicker("refresh")

        $("#Person_AddressInfo_CountryId").html(items).selectpicker("refresh");
        $("#Person_AddressInfo_CountryId").val(selectedValue);
        $("#Person_AddressInfo_CountryId").change();
        $("#Person_AddressInfo_CountryId").selectpicker("refresh")
     
    });
}

$(document).ready(function () {
    $('#PersonalStatementSubmitted').change(function () {
        if (this.checked) {
            $("#personal-statement-div").show();
        }
        else {
            $("#personal-statement-div").hide();
        }
    })
});

$(document).ready(function () {
    $('#UniversityFound').change(function () {
        if (this.checked) {
            $("#university-textarea-div").show();
            $("#university-dropdown-div").hide();
        }
        else {
            $("#university-dropdown-div").show();
            $("#university-textarea-div").hide();
        }
    })
});

$('#createProgramLocationModal').on('show.bs.modal', function () {
    getCountries()
});

$('#createFieldPlacementSiteModal').on('show.bs.modal', function () {
    getCountries()
});