﻿
$(document).ready(function () {
    $('#alumni-list-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "destroy": true,
        "ajax": {
            "url": "/AlumniManagement/GetAlumniList",
            "type": "POST",
        },
        columns: [
            {
                name: 'Id',
                data: 'id',
                title: "id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'applicantId',
                data: 'applicantId',
                title: "applicantId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'residentId',
                data: 'residentId',
                title: "residentId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'program',
                data: "programName",
                title: "Program",
                sortable: true,
                searchable: true
            },
            {
                name: 'programOfferId',
                data: "programOfferId",
                title: "programOfferId",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'programOffer',
                data: "programOffer",
                title: "programOffer",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'registrationNumber',
                data: "registrationNumber",
                title: "Registration Number",
                sortable: true,
                searchable: true
            },
            {
                name: 'residentName',
                data: "residentName",
                title: "Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'completionDate',
                data: "expectedProgramEndDate",
                title: "Graduation Date",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                "title": "Actions",
                "data": { "applicantId": "applicantId", "programOfferId": "programOfferId", "id": "id" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a target="_blank"   href="/GraduationManagement/ResidentGraduationDetails?graduateId=' + data.id + ' ">View Details </a>';
                }
            }
        ]
    });
})
