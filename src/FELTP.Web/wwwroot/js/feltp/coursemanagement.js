﻿function format_course(d) {
    return '<div class="slider" style="margin-left:60px">' +
        '<b>Code:</b > ' + d.code + '<br>' +
        '<b>Semester Tilte:</b > ' + d.name + '<br>' +
        '<b>Course Type:</b > ' + d.category + '<br>' +
        '<b>Created By:</b > ' + d.createdBy + '<br>' +
        '<b>Date Created:</b > ' + d.dateCreated + '<br>' +
        '<b>Status: </b >' + d.status + '<br>'
}

$(document).ready(function () {
    var table = $('#course-list-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/CourseManagement/GetCourses",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "width": "5%",
                "orderable": false,
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'code',
                data: 'code',
                title: "Code",
                sortable: false,
                searchable: false
            },
            {
                name: 'name',
                data: "name",
                title: "Course Title",
                sortable: true,
                searchable: true
            },          
            {
                name: 'category',
                data: 'category',
                title: "Session Type",
                sortable: true,
                searchable: true
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "Date Created",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'createdby',
                data: 'createdBy',
                title: "Created By",
                //sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'status',
                data: 'status',
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": { "id": "id", "name": "name" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a class="edit-unit" href="/CourseManagement/EditCourse?id=' + data.id + '"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i> Edit</button> </a>' + ' | '
                        + '<a class="view-units" href="/CourseManagement/_ViewCourseUnit?id=' + data.id + '&name=' + data.name + '"><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i> View Topics</button> </a>'
                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });
    $('#course-list-table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format_course(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }

    });
});

$('#course-list-table').on("click", ".view-units", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#course-unit-container').html(data);
        $('#course-unit-modal').modal('show');
    });
});

$('#course-list-table').on("click", ".edit-unit", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#course-unit-container').html(data);
        $('#course-unit-modal').modal('show');
        $('.selectpicker').selectpicker();
    });
});

$("#course-unit-modal").on("click", "#btn-edit-course", function (event) {
    var form = $('form#update-course-form');
    var action = $(form).attr('action');
    event.preventDefault();
    $.post(action, $(form).serialize(), function (data) {
        $("#course-unit-container").html(data);
    }, 'html');
});

$('#course-unit-modal').on('show.bs.modal', function () {
    var courseId = $("#CourseId").val();
    $('#course-units-list-table').dataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/CourseManagement/GetCourseUnits",
            "type": "POST",
            "data": { courseId: courseId }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'code',
                data: 'code',
                title: "Code",
                sortable: false,
                searchable: false
            },
            {
                name: 'unit',
                data: 'unit',
                title: "Unit",
                sortable: false,
                searchable: false
            },
            {
                name: 'hasActivity',
                data: 'hasActivity',
                title: "Has Activity",
                sortable: true,
                searchable: true
            },
            {
                name: 'status',
                data: 'status',
                title: "Status",
                sortable: true,
                searchable: true
            }
        ]
    });
});
function format_course_type(d) {
    return '<div class="slider" style="margin-left:60px">' +
        '<b>Full name: </b>' + d.name + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}
$(document).ready(function () {
    var table = $('#course-type-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/CourseManagement/GetCourseTypes",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "width": "5%",
                "orderable": false,
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'name',
                data: "name",
                title: "Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "Date Created",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'createdby',
                data: 'createdBy',
                title: "Created By",
                //sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'status',
                data: 'status',
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": "id",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/CourseManagement/_EditCourseType?id=' + data + '" class="editCourseType" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i> Edit</button></a> | <a href="/CourseManagement/_DeleteCourseType?id=' + data + '" class="deleteCourseType" data-toggle="tooltip" title="Deactivate"><button class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-ban-circle"></i> Deactivate</button></a>';
                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });
    // Add event listener for opening and closing details
    $('#course-type-table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format_program_tier(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });
    $('#course-type-table').on("click", ".editCourseType", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#editCourseTypeContainer').html(data);
            $('#editCourseTypeModal').modal('show');
        });

    });

    $('#course-type-table').on("click", ".detailsCourseType", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#detailsCourseTypeContainer').html(data);
            $('#detailsCourseTypeModal').modal('show');
        });

    });

    $('#course-type-table').on("click", ".deleteCourseType", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#deleteCourseTypeContainer').html(data);
            $('#deleteCourseTypeModal').modal('show');
        });

    });
});
$("#btnCreateCourseType").on("click", function () {

    var url = $(this).data("url");
    var CourseTypeurl = "/CourseManagement/GetCourseTypes";

    $.get(url, function (data) {
        $('#createCourseTypeContainer').html(data);
        $('#createCourseTypeModal').modal('show');
    });

});

function format(d) {
    return '<div class="slider" style="margin-left:70px">' +
        '<b>Full name: </b>' + d.name + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}
$(document).ready(function () {
    var table = $('#category-list').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/CourseManagement/GetCategories",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "width": "5%",
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'name',
                data: "name",
                title: "Category",
                sortable: true,
                searchable: true
            },
            {
                name: 'courseType',
                data: "courseType",
                title: "Course Type",
                sortable: true,
                searchable: true
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "Date Created",
                sortable: true,
                searchable: true
            },
            {
                name: 'createdby',
                data: 'createdBy',
                title: "Created By",
                //sortable: true,
                searchable: true
            },
            {
                name: 'status',
                data: 'status',
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": "id",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/CourseManagement/_EditCategory?id=' + data + '" class="editCategory" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i> Edit</button></a> | <a href="/CourseManagement/Delete?id=' + data + '" class="deleteCategory"><button class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-ban-circle"></i> Deactivate</button></a>';
                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Add New</b> button."
        }
    });
    // Add event listener for opening and closing details
    $('#category-list tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });
    $('#category-list').on("click", ".editCategory", function (event) {
        var courseTypeurl = "/CourseManagement/GetCourseTypes";

        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#editCategoryContainer').html(data);
            $('#editCategoryModal').modal('show');
            $('.selectpicker').selectpicker();
        });
        $.getJSON(courseTypeurl, {}, function (data) {
            var items = "<option value='0'> Select </option>";
            $.each(data, function (i, type) {
                items += "<option value='" + type.Value + "'>" + type.Text + "</option>"
            });
            $("#CourseTypeId").html(items).selectpicker("refresh");

        });

    });

    $('#category-list').on("click", ".detailCategory", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#detailCategoryContainer').html(data);
            $('#detailCategoryModal').modal('show');
            $('.selectpicker').selectpicker();
        });
        $.getJSON(courseTypeurl, {}, function (data) {
            var items = "<option value='0'> Select </option>";
            $.each(data, function (i, type) {
                items += "<option value='" + type.Value + "'>" + type.Text + "</option>"
            });
            $("#CourseTypeId").html(items).selectpicker("refresh");

        });

    });

    $('#category-list').on("click", ".deleteCategory", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#deleteCategoryContainer').html(data);
            $('#deleteCategoryModal').modal('show');
        });

    });
});

$("#btnCreateCategory").on("click", function () {

    var url = $(this).data("url");
   

    $.get(url, function (data) {
        $('#createCategoryContainer').html(data);
        $('#createCategoryModal').modal('show');
        $('.selectpicker').selectpicker();
        getcoursetypes();
    });
   
  
});

function getcoursetypes()
{
    var courseTypeurl = "/CourseManagement/GetCourseTypes";
    $.getJSON(courseTypeurl, {}, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, type) {
            items += "<option value='" + type.Value + "'>" + type.Text + "</option>"
        });
        $("#CourseTypeId").html(items).selectpicker("refresh");

    });

}




