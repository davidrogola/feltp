﻿function format(d) {
    return '<div class="slider" style="margin-left:50px">' +
        'Name: ' + d.name + '<br>' +
        'Description: ' + d.description + '<br>' +
        'Created By: ' + d.createdBy + '<br>' +
        'Date Created: ' + d.dateCreated + '<br>' +
        'Status: ' + d.status + '<br>'
}
$(document).ready(function () {
    var table = $("#competency").DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/competency/GetCompetencies",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'name',
                data: "name",
                title: "Name",
                sortable: true,
                searchable: true
            },

            {
                name: 'description',
                data: "description",
                title: "Description",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'durationInWeeks',
                data: "durationInWeeks",
                title: "Duration (Weeks)",
                sortable: true,
                searchable: true
            },
            {
                name: 'createdby',
                data: 'createdBy',
                title: "CreatedBy",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'status',
                data: "status",
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": { "id": "id", "name": "name" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a class="editCompetency" href="/Competency/_EditCompetency?id=' + data.id + '"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i> Edit </button></a>' + ' | '
                        + '<a class="view-deliverables" href="/Competency/_ViewCompetencyDeliverable?id=' + data.id + '&name=' + data.name + '"><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i> View Deliverables</button></a>';
                       
                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Add New</b> button."
        }
    });

    // Add event listener for opening and closing details
    $('#competency tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }

    });
    $('#competency').on("click", ".editCompetency", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#editCompetencyContainer').html(data);
            $('#editCompetencyModal').modal('show');
            $('.selectpicker').selectpicker();

        });

    });

    $('#competency').on("click", ".deleteCompetency", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#deleteCompetencyContainer').html(data);
            $('#deleteCompetencyModal').modal('show');
        });

    });

});

function getDeliverables() {
    var deliverableurl = "/Deliverable/GetDeliverablesListDropdown";

    $.getJSON(deliverableurl, {}, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, tier) {
            items += "<option value='" + tier.Value + "'>" + tier.Text + "</option>"
        });
        $("#DeliverableId").html(items).selectpicker("refresh");

    });
}


$("#btnCreateCompetency").on("click", function () {

    var url = $(this).data("url");
   

    $.get(url, function (data) {
        $('#createCompetencyContainer').html(data);
        $('#createCompetencyModal').modal('show');
        $('.selectpicker').selectpicker();
        getDeliverables();

    });
}); 

$('#competency').on("click", ".view-deliverables", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#competency-deliverable-container').html(data);
        $('#competency-deliverable-modal').modal('show');
    });
});

$('#createCompetencyModal').on("click", "#btn-submit-compentency", function (event) {
    var form = $('form#add-competency-form');
    var action = $(form).attr('action');
    event.preventDefault();
    $.post(action, $(form).serialize(), function (data) {
        $("#createCompetencyContainer").html(data);
        getDeliverables();
        $('.selectpicker').selectpicker();
    }, 'html');

});


$('#competency-deliverable-modal').on('show.bs.modal', function () {

    var competencyId = $("#CompetencyId").val();
    $('#competency-deliverable').dataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/Competency/GetCompetencyDeliverables",
            "type": "POST",
            "data": { competencyId: competencyId }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'code',
                data: "code",
                title: "Code",
                sortable: true,
                searchable: true
            },
            {
                name: 'deliverable',
                data: "deliverable",
                title: "Name",
                sortable: true,
                searchable: true
            },

            {
                name: 'description',
                data: "description",
                title: "Description",
                sortable: true,
                searchable: true
            },
            {
                name: 'durationInWeeks',
                data: "duration",
                title: "Duration (Weeks)",
                sortable: true,
                searchable: true
            }
        ]
    });
});



