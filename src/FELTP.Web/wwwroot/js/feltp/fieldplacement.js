﻿function format_fieldactivities(d) {
    return '<div class="slider" style="margin-left:60px">' +
        '<b>Name:</b > ' + d.name + '<br>' +
        '<b>Description:</b > ' + d.description + '<br>' +
        '<b>Created By:</b > ' + d.createdBy + '<br>' +
        '<b>Date Created:</b > ' + d.dateCreated + '<br>' +
        '<b>Status: </b >' + d.status + '<br>'
}

$(document).ready(function () {
    var table = $('#fieldplacementactivity-list-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/FieldPlacement/GetFieldPlacementActivities",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "width": "5%",
                "orderable": false,
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'name',
                data: "name",
                title: "Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'description',
                data: "description",
                title: "Description",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'status',
                data: "status",
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": { "id": "id", "name": "name" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a class="edit-fieldplacement-activity" href="/FieldPlacement/_EditFieldPlacementActivity?id=' + data.id + '"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i> Edit </button> </a>' + ' | '
                        + '<a class="view-placement-deliverables" href="/FieldPlacement/_ViewActivityDeliverable?id=' + data.id + '&name=' + data.name + '"><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i> View Deliverables</button></a>'
                }
            }
        ]
    });


    $('#fieldplacementactivity-list-table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format_fieldactivities(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }

    });
});

$('#fieldplacementactivity-list-table').on("click", ".view-placement-deliverables", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#fieldplacement-deliverable-container').html(data);
        $('#fieldplacement-deliverable-modal').modal('show');
    });
});

$('#fieldplacementactivity-list-table').on("click", ".edit-fieldplacement-activity", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#fieldPlacementActivitycontainer').html(data);
        $('#fieldPlacementActivitymodal').modal('show');
        $('.selectpicker').selectpicker();

    });
});


$('#fieldplacement-deliverable-modal').on('show.bs.modal', function () {

    var activityId = $("#FieldPlacementActivityId").val();
    $('#field-placement-deliverable').dataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/FieldPlacement/GetFieldPlacementActivityDeliverables",
            "type": "POST",
            "data": { activityId: activityId }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'deliverable',
                data: "deliverable",
                title: "Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'status',
                data: "status",
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "DateCreated",
                sortable: true,
                searchable: true,
                visible: false

            },
            {
                name: 'createdby',
                data: 'createdBy',
                title: "CreatedBy",
                sortable: true,
                searchable: true,
                visible: true
            }
        ]
    });
});



function format_sites(d) {
    return '<div class="slider" style="margin-left:60px">' +
        '<b>Name: </b >' + d.name + '<br>' +
        '<b>Country: </b >' + d.country + '<br>' +
        '<b>Venue: </b >' + d.location + '<br>' +
        '<b>Building: </b >' + d.building + '<br>' +
        '<b>Created By: </b >' + d.createdBy + '<br>' +
        '<b>Date Created: </b >' + d.dateCreated + '<br>' +
        '<b>Status: </b >' + d.status + '<br>'
}
$(document).ready(function () {
    var table = $('#fieldplacementsite-list').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/FieldPlacement/GetFieldPlacementSites",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "width": "5%",
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'name',
                data: "name",
                title: "Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'location',
                data: "location",
                title: "Venue",
                sortable: true,
                searchable: true
            },
            {
                name: 'building',
                data: "building",
                title: "Building",
                sortable: true,
                searchable: true
            },
            {
                name: 'status',
                data: "status",
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": "id",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/FieldPlacement/_EditFieldPlacementSite?id=' + data + '" class="editFieldPlacementSite" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i> Edit</button></a> | <a href="/FieldPlacement/Delete?id=' + data + '" class="deleteFieldPlacementSite"  data-toggle="tooltip" title="Deactivate"><button class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-ban-circle"></i> Deactivate</button></a>';
                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });

    // Add event listener for opening and closing details
    $('#fieldplacementsite-list tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format_sites(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });
    $('#fieldplacementsite-list').on("click", ".editFieldPlacementSite", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#editFieldPlacementSiteContainer').html(data);
            $('#editFieldPlacementSiteModal').modal('show');
            $('.selectpicker').selectpicker();
        });

    });

    $('#fieldplacementsite-list').on("click", ".detailsFieldPlacementSite", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#detailsFieldPlacementSiteContainer').html(data);
            $('#detailsFieldPlacementSiteModal').modal('show');
            $('.selectpicker').selectpicker();
        });

    });

    $('#fieldplacementsite-list').on("click", ".deleteFieldPlacementSite", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#deleteFieldPlacementSiteContainer').html(data);
            $('#deleteFieldPlacementSiteModal').modal('show');
        });

    });
});

function getfacultydropdown() {
    var facultyUrl = "/FacultyManagement/GetFacultyDropDown";
    $.getJSON(facultyUrl, {}, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, comp) {
            items += "<option value='" + comp.Value + "'>" + comp.Text + "</option>"
        });
        $("#Faculty").html(items).selectpicker("refresh");

    });

};


$("#btnCreateFieldPlacementSite").on("click", function () {

    var url = $(this).data("url"); 

    $.get(url, function (data) {
        $('#createFieldPlacementSiteContainer').html(data);
        $('#createFieldPlacementSiteModal').modal('show');
        $('.selectpicker').selectpicker();
        getfacultydropdown();
    });

   
});


$('.initiate-fieldplacement').on("click", function (event) {

    event.preventDefault();
    var url = $(this).attr("href");
    var semesterId = $("#SemesterId").val();
    var enrollmentId = $("#ProgramEnrollemntId").val();
    var residentId = $("#ResidentId").val();
    var programOfferId = $("#ProgramOfferId").val();
    if (semesterId == 0) {
        alert("Please select semester before initiating a field placement");
        return;
    }
    $.get(url, { programEnrollmentId: enrollmentId, semesterId: semesterId, residentId: residentId, programOfferId: programOfferId }, function (data) {
        $('#resident-fieldplacement-container').html(data);
        $('#resident-fieldplacement-modal').modal('show');
        $('.selectpicker').selectpicker();
        getfacultydropdown();
    });
});

$('#resident-fieldplacement-container').on("change", "#SupervisorId", function () {
    var fieldSiteName = $('#SupervisorId :selected').parent().attr('label');
    $("#SiteName").val(fieldSiteName);
    console.log($("#SiteName").val());
});

$('#resident-fieldplacement-container').on("change", "#ResidentFieldPlacementActivityId", function () {
    var activityType = $('#ResidentFieldPlacementActivityId :selected').parent().attr('label');
    $("#ActivityType").val(activityType);
    if (activityType == "Field") {
        $('#supervisor-div').show();
        $('#faculty-div').hide();
    }
    else if (activityType == "Non-Field") {
        $('#faculty-div').show();
        $('#supervisor-div').hide();
    }
    else
    {
        $('#supervisor-div').hide();
        $('#faculty-div').hide();
    }
    
});

