﻿function format_deliverable(d) {
    return '<div class="slider" style="margin-left:70px">' +
        '<b>Code: </b>' + d.code + '<br>' +
        '<b>Name: </b>' + d.name + '<br>' +
        '<b>Description: </b>' + d.description + '<br>' +
        '<b>Has Thesis Defence: </b>' + d.hasThesisDefence + '<br>' +
        '<b>Duration in Weeks: </b>' + d.durationInWeeks + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}
$(document).ready(function () {
    var table = $('#deliverable').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/deliverable/GetDeliverables",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "width": "5%",
                "orderable": false,
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'code',
                data: "code",
                title: "Code",
                sortable: true,
                searchable: true
            },
            {
                name: 'name',
                data: "name",
                title: "Name",
                sortable: true,
                searchable: true
            },

            {
                name: 'description',
                data: "description",
                title: "Description",
                sortable: true,
                searchable: true,
                visible: false

            },
            {
                name: 'durationInWeeks',
                data: "durationInWeeks",
                title: "Duration (Weeks)",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "DateCreated",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'createdby',
                data: 'createdBy',
                title: "CreatedBy",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'status',
                data: "status",
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": "id",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/Deliverable/_EditDeliverable?id=' + data + '" class="editDeliverable" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i></button></a> | <a href="/Deliverable/_DetailsDeliverable?id=' + data + '" class="detailsDeliverable" data-toggle="tooltip" title="Details"><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i></button></a> | <a href="/Deliverable/_DeactivateDeliverable?id=' + data + '" class="deleteDeliverable" data-toggle="tooltip" title="Deactivate"><button class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-ban-circle"></i></button></a>';
                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Add New</b> button."
        }
    });
    // Add event listener for opening and closing details
    $('#deliverable tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format_deliverable(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });
    $('#deliverable').on("click", ".editDeliverable", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#editDeliverableContainer').html(data);
            $('#editDeliverableModal').modal('show');
            $('.selectpicker').selectpicker();
        });

    });

    $('#deliverable').on("click", ".detailsDeliverable", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#detailsDeliverableContainer').html(data);
            $('#detailsDeliverableModal').modal('show');
            $('.selectpicker').selectpicker();
        });

    });

    $('#deliverable').on("click", ".deleteDeliverable", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#deleteDeliverableContainer').html(data);
            $('#deleteDeliverableModal').modal('show');
        });

    });
});


$("#btnCreateDeliverable").on("click", function () {

    var url = $(this).data("url");

    $.get(url, function (data) {
        $('#createDeliverableContainer').html(data);
        $('#createDeliverableModal').modal('show');
    });

});

$('.submit-resident-deliverable').on("click", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    event.preventDefault();

    var semesterId = $("#SelectedSemester").val();
    var enrollmentId = $("#ProgramEnrollemntId").val();
    var residentId = $("#ResidentId").val();
    var offerId = $("#ProgramOfferId").val();
    if (semesterId == 0) {
        alert("Please select semester before submitting deliverables");
        return;
    }

    $.get(url, { programEnrollmentId: enrollmentId, residentId: residentId, semesterId: semesterId, programOfferId: offerId }, function (data) {
        $('#submit-residentdeliverable-container').html(data);
        $('#submit-residentdeliverable-modal').modal('show');
        $('#committee-approval-table').hide();
        $('#toggle-approval-commitee-div').hide();
        $('.selectpicker').selectpicker();
    });
});


$('#submit-residentdeliverable-container').on("change", "#SubmittedDeliverableId", function () {
    var deliverableId = $('#SubmittedDeliverableId').val();
    $("#ResidentDeliverableId").val(deliverableId);
});

$('#submit-residentdeliverable-container').on("click", "#toggle-approval-commitee-div", function () {

    $(".hide-approval-committee").show();
    $("#hide-approval-commitee-div").show();
    $("#toggle-approval-commitee-div").hide();
});

$('#submit-residentdeliverable-container').on("click", "#hide-approval-commitee-div", function () {
    $(".hide-approval-committee").hide();
    $("#hide-approval-commitee-div").hide();
    $("#toggle-approval-commitee-div").show();
});

$("#submit-residentdeliverable-modal").on('show.bs.modal', function () {
    $('#ProgrammaticAreaFound').change(function () {
        if (this.checked) {
            $("#programmaticarea-textarea-div").show();
            $("#programmaticarea-dropdown-div").hide();
        }
        else {
            $("#programmaticarea-dropdown-div").show();
            $("#programmaticarea-textarea-div").hide();
        }
    })
});

$('#submit-residentdeliverable-container').on("change", "#SubmissionType", function () {
    var submissionType = $('#SubmissionType').val();
    if (submissionType == 2) // Final submission
    {
        $("#ethics-approval-div").show();

        var hasThesisDefence = $("#HasThesisDefence").val();
        console.log(hasThesisDefence);
        if (hasThesisDefence == "true")
            $("#thesis-defence-div").show();
        else
            $("#thesis-defence-div").hide();
    }
    else
    {
        $("#ethics-approval-div").hide();
        $("#thesis-defence-div").hide();

    }    
});

$('#submit-residentdeliverable-container').on("change", "#ResidentDeliverableId", function () {
    var url = "/Deliverable/GetResidentDeliverableById";
    var deliverableId = $("#ResidentDeliverableId").val();
    var submissionType = $('#SubmissionType').val();

    $.getJSON(url, { Id: deliverableId }, function (data) {
        if (data.HasThesisDefence == true && submissionType == 2) {
            $("#thesis-defence-div").show();
        }
        else
        {
            $("#thesis-defence-div").hide();
        }
        console.log(data.HasThesisDefence);
       $("#HasThesisDefence").val(data.HasThesisDefence);
    });
});

$("#submit-residentdeliverable-modal").on('show.bs.modal', function () {
    $('#HasEthicalApproval').change(function () {
        if (this.checked) {
            $("#committee-approval-table").show();
            $('#toggle-approval-commitee-div').show();
            $(".hide-approval-committee").hide();
        }
        else {
            $("#committee-approval-table").hide();
            $('#toggle-approval-commitee-div').hide();
            $("#hide-approval-commitee-div").hide();
        }
    })

});
