﻿
$('#ProgramOfferId').change(function () {
    var offerId = $("#ProgramOfferId").val();
    var url = '/ResidentManagement/GetEnrolledResidentsDropDown';
    $.getJSON(url, { programOfferId: offerId }, function (data) {
        var items = "<option value='0'> Select Resident </option>";
        $.each(data, function (i, offer) {
            items += "<option value='" + offer.Value + "'>" + offer.Text + "</option>"
        });
        $("#ProgramEnrollmentId").html(items).selectpicker("refresh");
    });
   
}); 

$("#ProgramId").change(function () {
    var url = "/ProgramManagement/GetProgramOffers";
    var programId = $("#ProgramId").val();
    $.getJSON(url, { programId: programId }, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, offer) {
            items += "<option value='" + offer.Value + "'>" + offer.Text + "</option>"
        });
        $("#ProgramOfferId").html(items).selectpicker("refresh");

    });
});

$("#ProgramId").change(function () {
    var url = "/EventManagement/GetProgramEvents";
    var ProgramId = $("#ProgramId").val();
    $.getJSON(url, { ProgramId: ProgramId }, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, event) {
            items += "<option value='" + event.Value + "'>" + event.Text + "</option>"
        });
        $("#ProgramEventId").html(items).selectpicker("refresh");
    });
});

$("#ProgramEventId").change(function () {
    var url = "/ProgramManagement/GetEventSchedule";
    var eventId = $("#ProgramEventId").val();
    $.getJSON(url, { programEventId: eventId }, function (data) {
        $("#SubmissionExpiryDate").val(data.StartDate);
        $("#StrSubmissionExpiryDate").val(data.StartDate);
        $("#submission-expiry-date-div").show();
    });
});

$("#ProgramEnrollmentId").change(function () {
    var enrollmentId = $("#ProgramEnrollmentId").val();
    var url = '/Deliverable/GetResidentDeliverablesDropDown';
    $.getJSON(url, { programEnrollmentId: enrollmentId, status:"Completed"}, function (data) {
        var items = "<option value='0'> Select Deliverable </option>";
        $.each(data, function (i, offer) {
            items += "<option value='" + offer.Value + "'>" + offer.Text + "</option>"
        });
        $("#ResidentDeliverableId").html(items).selectpicker("refresh");
    });

});

$("#ResidentDeliverableId").change(function () {
    var deliverableId = $("#ResidentDeliverableId").val();
    var url = '/EthicsManagement/EthicsApprovalCommitteePartial';
    $.get(url, { residentDeliverableId: deliverableId }, function (data) {
        $("#ethics-approval-committee-div").html(data);
        $("#ethics-approval-committee-div").show();
        $('.selectpicker').selectpicker();
        $('#toggle-approval-commitee-div').show();
        $(".hide-approval-committee").hide();
    });

});

$('#ethics-approval-committee-div').on("click","#toggle-approval-commitee-div",function () {
    $(".hide-approval-committee").show();
    $("#hide-approval-commitee-div").show();
    $("#toggle-approval-commitee-div").hide();
});

$('#ethics-approval-committee-div').on("click", "#hide-approval-commitee-div", function () {
    $(".hide-approval-committee").hide();
    $("#hide-approval-commitee-div").hide();
    $("#toggle-approval-commitee-div").show();
});

function format(d) {
    return '<div class="slider" style="margin-left:60px">' +
        '<b>Resident Name:</b> ' + d.residentName + '<br>' +
        '<b>Deliverable:</b> ' + d.deliverableName + '<br>' +
        '<b>Approval Status: </b>' + d.approvalStatus + '<br>' +
        '<b>Created By:</b> ' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Event Type: </b>' + d.eventTypeId + '<br>' +
        '<b>Event Title: </b>' + d.title + '<br>' +
        '<b>Submission Due Date: </b>' + d.submissionDueDate + '<br>'
    '<b>Enable Email Notification: </b>' + d.enableEmailNotification + '<br>' +
        '<b>Enable Sms Notification: </b>' + d.enableSmsNotification + '<br>';
     
}

$(document).ready(function () {
    var table = $('#ethics-approval-list-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/EthicsManagement/GetEthicsApprovalList",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "width": "5%",
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'residentId',
                data: 'residentId',
                title: "residentId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'residentName',
                data: 'residentName',
                title: "Resident",
                sortable: false,
                searchable: false
            },
            {
                name: 'Deliverable',
                data: 'deliverableName',
                title: "Deliverable",
                sortable: true,
                searchable: true
            },
            {
                name: 'eventName',
                data: "title",
                title: "Event Title",
                sortable: true,
                searchable: false,
                visible: true
            },
            {
                name: 'ApprovalStatus',
                data: "approvalStatus",
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                name: 'createdBy',
                data: 'createdBy',
                title: "Created By",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "DateCreated",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'submissionDueDate',
                data: 'submissionDueDate',
                title: "Submission DueDate",
                //sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'enableEmailNotification',
                data: "enableEmailNotification",
                title: "Enable Email Notification",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'enableSmsNotification',
                data: 'enableSmsNotification',
                title: "Enable Sms Notification",
                //sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'eventTypeId',
                data: 'eventTypeId',
                title: "EventType",
                //sortable: true,
                searchable: false,
                visible: false
            },
            {
                "title": "Actions",
                "data": { "id": "id", "residentId": "residentId" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/EthicsManagement/EthicsApprovalDetails?id=' + data.id +' ">View Details </a>';
                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });
    // Add event listener for opening and closing details
    $('#ethics-approval-list-table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });
});


$(document).ready(function () {
    var ethicsApprovalId = $("#EthicsApprovalId").val();
    var table = $('#ethics-approval-committee-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "dom": 'rt<"bottom"i<"clear">>',
        "ajax": {
            "url": "/EthicsManagement/GetCommitteeByEthicsApprovalId",
            "type": "POST",
            "data": { ethicsApprovalId: ethicsApprovalId }

        },
        columns: [
            {
                name: 'committeeId',
                data: 'committeeId',
                title: "committeeId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'ethicsApprovalId',
                data: 'ethicsApprovalId',
                title: "ethicsApprovalId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'committeeName',
                data: "committee",
                title: "Committee",
                sortable: true,
                searchable: true
            },
            {
                name: 'dateCreated',
                data: "dateCreated",
                title: "Date Created",
                sortable: true,
                searchable: true,
                visible: true
            },
            {
                "title": "Actions",
                "data": { "ethicsApprovalId": "ethicsApprovalId", "committeeId": "committeeId" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a  href="/EthicsManagement/ViewEthicsApprovalSubmissions?id=' + data.ethicsApprovalId + '&committeeId=' + data.committeeId + '">View Submissions </a>'
                }
            }
        ]
    });
});

$(document).ready(function () {

    var ethicsApprovalId = $("#EthicsApprovalId").val();
    var committeeId = $("#CommitteeId").val();

    var table = $('#ethics-approval-submissions').DataTable({
        "serverSide": true,
        "proccessing": true,
        "dom": 'rt<"bottom"i<"clear">>',
        "ajax": {
            "url": "/EthicsManagement/GetEthicsApprovalCommitteeSubmissions",
            "type": "POST",
            "data": { id: ethicsApprovalId, committeeId: committeeId }

        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'committeeId',
                data: 'committeeId',
                title: "committeeId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'ethicsApprovalId',
                data: 'ethicsApprovalId',
                title: "ethicsApprovalId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'ethicsApprovalDocumentId',
                data: 'ethicsApprovalDocumentId',
                title: "ethicsApprovalDocumentId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'documentVersion',
                data: "documentVersion",
                title: "Document Version",
                sortable: true,
                searchable: true
            },
            {
                name: 'ApprovalStatus',
                data: "approvalStatus",
                title: "Approval Status",
                sortable: true,
                searchable: true,
                visible: true
            },
            {
                name: 'submissionDate',
                data: "submissionDate",
                title: "Submission Date",
                sortable: true,
                searchable: true
            },
            {
                name: 'submittedBy',
                data: "submittedBy",
                title: "Submitted By",
                sortable: true,
                searchable: true,
                visible: true
            },
            {
                name: 'comment',
                data: "comment",
                title: "Comment",
                sortable: true,
                searchable: true,
                visible: true
            },
            {
                "title": "Actions",
                "data": { "ethicsApprovalDocumentId": "ethicsApprovalDocumentId", "id": "id" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a class="evaluate-approval-submission"  href="/EthicsManagement/_EvaluateEthicsApprovalSubmission?id=' + data.id + '">Evaluate </a>' + ' | '
                        + '<a  href="/EthicsManagement/DownloadSubmissionDocument?id=' + data.ethicsApprovalDocumentId + '">Download Document </a>';
                }
            }
        ]
    });
});

$('#ethics-approval-submissions').on("click", ".evaluate-approval-submission", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#evaluate-ethics-approval-submission-container').html(data);
        $('.selectpicker').selectpicker();
        $('#evaluate-ethics-approval-submission-modal').modal('show');
    });
});

$('.override-ethics-approval-outcome').on("click", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#override-ethics-approval-submissions-container').html(data);
        $('.selectpicker').selectpicker();
        $('#override-ethics-approval-submissions-modal').modal('show');
    });
});
