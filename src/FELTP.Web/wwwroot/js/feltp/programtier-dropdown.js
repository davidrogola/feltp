﻿$(document).ready(function () {
    var url = "/ProgramManagement/GetProgramTiers";
    $.getJSON(url, {}, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, tier) {
            items += "<option value='" + tier.Value + "'>" + tier.Text + "</option>"
        });
        $("#ProgramTierId").html(items).selectpicker("refresh");

    });
});

