﻿$(document).ready(function () {
    var url = "/CourseManagement/GetCategories";
    $.getJSON(url, {}, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, coursetype) {
            items += "<option value='" + coursetype.Value + "'>" + coursetype.Text + "</option>"
        });
        $("#CategoryId").html(items).selectpicker("refresh");

    });
});