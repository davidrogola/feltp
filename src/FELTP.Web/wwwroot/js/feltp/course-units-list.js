﻿
$(document).ready(function () {
    $('#course-units-list-table').dataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/CourseManagement/GetCourseUnits",
            "type": "POST"
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'course',
                data: "course",
                title: "Course Title",
                sortable: true,
                searchable: true
            },
            {
                name: 'unit',
                data: 'unit',
                title: "Topic",
                sortable: false,
                searchable: false
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "Date Created",
                sortable: true,
                searchable: true
            },
            {
                name: 'createdby',
                data: 'createdBy',
                title: "Created By",
                //sortable: true,
                searchable: true
            },
            {
                name: 'status',
                data: 'status',
                title: "Status",
                sortable: true,
                searchable: true
            }
        ]
    });
});

