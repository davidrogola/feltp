﻿$(document).ready(function () {
    var url = "/FieldPlacement/GetFieldPlacementActivitiesDropDown";
    $.getJSON(url, {}, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, activity) {
            items += "<option value='" + activity.Value + "'>" + activity.Text + "</option>"
        });
        $("#FieldPlacementActivityId").html(items).selectpicker("refresh");
        $("#FieldPlacementActivities").html(items).selectpicker("refresh");


    });
});

$(document).ready(function () {
    $('#HasFieldPlacementActivity').change(function () {
        if (this.checked) {
            $("#field-placement-activity-div").show();
        }
        else
        {
            $("#field-placement-activity-div").hide();
        }
    })
});

$("#units-details-modal").on("change", "#HasFieldPlacementActivity", function (e) {
        if (this.checked) {
            $("#field-placement-activity-div").show();
        }
        else {
            $("#field-placement-activity-div").hide();
        }
});

