﻿$("#CourseTypeId").change(function () {
    var url = "/CourseManagement/GetCourses";
    var courseTypeId = $("#CourseTypeId").val();
    $.getJSON(url, { courseTypeId: courseTypeId }, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, course) {
            items += "<option value='" + course.Value + "'>" + course.Text + "</option>"
        });
        $("#CourseId").html(items).selectpicker("refresh");

    });
});


function getCourses()
{
    var url = "/CourseManagement/GetCourses";
    var categoryId = $("#CategoryId").val();
    $.getJSON(url, { categoryId: categoryId }, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, course) {
            items += "<option value='" + course.Value + "'>" + course.Text + "</option>"
        });
        $("#CourseId").html(items).selectpicker("refresh");

    });

}

$('#createProgramCourseModal').on('show.bs.modal', function () {
    $("#CategoryId").change(function () {
        getCourses();
    });  
});


$(document).ready(function () {
    var url = "/CourseManagement/GetUniversityCoursesDropDown";
    $.getJSON(url, {}, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, tier) {
            items += "<option value='" + tier.Value + "'>" + tier.Text + "</option>"
        });
        $("#AcademicCourseId").html(items).selectpicker("refresh");
    });
});

$('#AcademicRequirementDetails_AcademicCourseToEducationLevelMapping').on('hidden.bs.select', function (e) {
    var selectedAcademicCourseValues = $(this).val();
    var distinctEducationLevels = [];
    for (var counter = 0; counter < selectedAcademicCourseValues.length; counter++) {
        var educationLevelToCourseMapping = selectedAcademicCourseValues[counter];
        var splitMappingArr = educationLevelToCourseMapping.split(":");

        var educationLevel = splitMappingArr[0];
        var educationLevelExists = distinctEducationLevels.indexOf(educationLevel) > -1;

        if (!educationLevelExists) {
            distinctEducationLevels.push(educationLevel);
        }
    }

    $.get('/CourseManagement/_GetFilteredAcademicGradesDropdown', { educationLevels: distinctEducationLevels.toString() }, function (data) {
        $("#academic-requirement-grading-div").html(data);
        $('#AcademicRequirementDetails_CourseGradingToEducationLevelMapping').selectpicker("refresh");
    }, 'html');
});



