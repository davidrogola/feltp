﻿$(document).ready(function () {
    var url = "/CourseManagement/GetCourseTypes";
    $.getJSON(url, {}, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, coursetype) {
            items += "<option value='" + coursetype.Value + "'>" + coursetype.Text + "</option>"
        });
        $("#CourseTypeId").html(items).selectpicker("refresh");

    });
});