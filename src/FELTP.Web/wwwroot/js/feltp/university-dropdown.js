﻿$(document).ready(function () {
    var url = "/LookUp/GetUniversities";
    $.getJSON(url, {}, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, tier) {
            items += "<option value='" + tier.Value + "'>" + tier.Text + "</option>"
        });
        $("#Person_AcademicDetail_UniversityId").html(items).selectpicker("refresh");

    });
});

$('.graduation-info').on('click', function () {
    var selectedValue = $('input[name=radio-graduation-info]:checked').val();

    if (selectedValue == 'GraduateofFELTP') {
        $("#Person_AcademicDetail_GraduateofFELTP").val(true);
        $("#Person_AcademicDetail_GraduateofEIS").val(false);
    }
    else {
        $("#Person_AcademicDetail_GraduateofFELTP").val(false);
        $("#Person_AcademicDetail_GraduateofEIS").val(true);
    }
});
