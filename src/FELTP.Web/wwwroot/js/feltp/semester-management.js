﻿function getSemesters() {
    var url = "/SemesterManagement/GetSemesters";
    $.getJSON(url, {}, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, semester) {
            items += "<option value='" + semester.Value + "'>" + semester.Text + "</option>"
        });
        $("#SemesterId").html(items).selectpicker("refresh");

    });
}

$(document).ready(function () {
    getSemesters()
});

$('#createProgramCourseModal').on('show.bs.modal', function () {
    getSemesters();
});

function getProgramSemesters(programId) {
    var url = "/SemesterManagement/GetProgramSemesters";
    $.getJSON(url, { programId: programId }, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, semester) {
            items += "<option value='" + semester.Value + "'>" + semester.Text + "</option>"
        });
        $("#SemesterId").html(items).selectpicker("refresh");

    });
}


function format(d) {
    return '<div class="slider" style="margin-left:60px">' +
        '<b>Schedule Name:</b> ' + d.description + '<br>' +
        '<b>Program Name:</b> ' + d.program + '<br>' +
        '<b>Program Offer:</b> ' + d.programOffer + '<br>' +
        '<b>Start Date: </b>' + d.estimatedStartDate + '<br>' +
        '<b>End Date: </b>' + d.estimatedEndDate + '<br>' +
        '<b>Status: </b>' + d.status + '<br>' +
        '<b>Created By:</b> ' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>';

}




$(document).ready(function () {
    var table = $('#semester-schedule-list-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/SemesterManagement/GetSemesterScheduleList",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "width": "5%",
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'programOfferId',
                data: 'programOfferId',
                title: "programOfferId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'semesterId',
                data: 'semesterId',
                title: "semesterId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'Description',
                data: 'description',
                title: "Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'estimatedStartDate',
                data: "estimatedStartDate",
                title: "Start Date",
                sortable: true,
                searchable: false,
                visible: true
            },
            {
                name: 'estimatedEndDate',
                data: "estimatedEndDate",
                title: "End Date",
                sortable: true,
                searchable: false,
                visible: true
            },
            {
                name: 'status',
                data: "status",
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                name: 'createdBy',
                data: 'createdBy',
                title: "Created By",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "DateCreated",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'program',
                data: "program",
                title: "Program",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'ProgramOffer',
                data: 'programOffer',
                title: "Program Offer",
                //sortable: true,
                searchable: false,
                visible: false
            },
            {
                "title": "Actions",
                "data": { "id": "id" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/SemesterManagement/SemesterScheduleDetails?id=' + data.id + ' "><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i> View Details</button></a>';
                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });
    // Add event listener for opening and closing details
    $('#semester-schedule-list-table').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });
});

function format_completionrequest(d) {
    return '<div class="slider" style="margin-left:60px">' +
        '<b>Schedule Name:</b> ' + d.description + '<br>' +
        '<b>Program Name:</b> ' + d.program + '<br>' +
        '<b>Program Offer:</b> ' + d.programOffer + '<br>' +
        '<b>Start Date: </b>' + d.estimatedStartDate + '<br>' +
        '<b>End Date: </b>' + d.estimatedEndDate + '<br>' +
        '<b>Status: </b>' + d.status + '<br>' +
        '<b>Created By:</b> ' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Requested By: </b>' + d.completionRequestedBy + '<br>' +
        '<b>Date Requested:</b> ' + d.completionRequestDate + '<br>' +
        '<b>Comment: </b>' + d.completionRequestReason + '<br>';

}

$(document).ready(function () {
    var table = $('#semester-schedule-pendingcompletion-list-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/SemesterManagement/GetSemesterSchedulePendingApproval",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "width": "5%",
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'programOfferId',
                data: 'programOfferId',
                title: "programOfferId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'semesterId',
                data: 'semesterId',
                title: "semesterId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'Description',
                data: 'description',
                title: "Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'estimatedStartDate',
                data: "estimatedStartDate",
                title: "Start Date",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'estimatedEndDate',
                data: "estimatedEndDate",
                title: "End Date",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'status',
                data: "status",
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                name: 'completionRequestedBy',
                data: 'completionRequestedBy',
                title: "Requested By",
                sortable: true,
                searchable: true,
                visible: true
            },
            {
                name: 'completionRequestReason',
                data: "completionRequestReason",
                title: "Comment",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'completionRequestDate',
                data: "completionRequestDate",
                title: "Date Requested",
                sortable: true,
                searchable: false,
                visible: true
            },
            {
                name: 'dateCreated',
                data: "dateCreated",
                title: "Date Created",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'createdBy',
                data: "createdBy",
                title: "Date Created",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'ProgramOffer',
                data: 'programOffer',
                title: "Program Offer",
                //sortable: true,
                searchable: false,
                visible: false
            },
            {
                "title": "Actions",
                "data": { "id": "id" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a class="approve-semester-completion" href="/SemesterManagement/ApproveSemesterCompletionRequest?id=' + data.id + ' "> <button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon glyphicon-ok"></i> Approve</button> </a>';
                }
            }
        ],
        "language": {
            "emptyTable": "No Records found"
        }
    });

    // Add event listener for opening and closing details
    $('#semester-schedule-pendingcompletion-list-table').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format_completionrequest(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });
});


$(document).ready(function () {

    var scheduleId = $("#SemesterScheduleId").val();
    var table = $('#semester-curiculum-schedule-list').DataTable({
        "serverSide": true,
        "proccessing": true,
        "dom": 'rt<"bottom"i<"clear">>',
        "ajax": {
            "url": "/SemesterManagement/GetSemesterScheduleItemList",
            "type": "POST",
            "data": { semesterScheduleId: scheduleId }

        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'scheduleItemType',
                data: 'scheduleItemType',
                title: "Type",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'programCourseId',
                data: 'programCourseId',
                title: "programCourseId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'name',
                data: 'name',
                title: "Name",
                sortable: false,
                searchable: true,
                visible: true
            },
            {
                name: 'startDate',
                data: 'startDate',
                title: "Start Date",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'endDate',
                data: 'endDate',
                title: "End Date",
                sortable: true,
                searchable: true
            },
            {
                name: 'status',
                data: "status",
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                name: 'createdBy',
                data: 'createdBy',
                title: "Created By",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "DateCreated",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'timetableCreated',
                data: "timetableCreated",
                title: "timetableCreated",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'semesterScheduleId',
                data: "semesterScheduleId",
                title: "semesterScheduleId",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                "title": "Actions",
                "data": { "id": "id", "programCourseId": "programCourseId", "timetableCreated": "timetableCreated", "semesterScheduleId": "semesterScheduleId", "scheduleItemType": "ScheduleItemType" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    var link = '<a href="/ExaminationManagement/CreateExaminationTimetable?id=' + data.id + '&courseId=' + data.programCourseId + ' "><button class="btn btn-primary btn-xs"> Create Timetable</button></a>';

                    if (data.scheduleItemType == "Course")
                        link = '<a href="/SemesterManagement/CreateCourseTimetable?id=' + data.id + '&courseId=' + data.programCourseId + ' "><button class="btn btn-primary btn-xs"> Create Timetable</button></a>';

                    if (data.timetableCreated == true && data.scheduleItemType == "Examination")
                        link = '<a href="/ExaminationManagement/CreateExaminationTimetable?id=' + data.id + '&courseId=' + data.programCourseId + ' "><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-plus"></i> Add Timetable</button></a>' + ' |  '
                        + '<a href="/ExaminationManagement/ViewExaminationTimetable?id=' + data.id + '&semesterScheduleId=' + data.semesterScheduleId + ' "><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i> View Schedule</button> </a>';

                    else if (data.timetableCreated == true && data.scheduleItemType == "Course")
                        link = '<a href="/SemesterManagement/CreateCourseTimetable?id=' + data.id + '&courseId=' + data.programCourseId + ' "><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-plus"></i> Add Timetable</button></a>' + ' | '
                        + '<a href="/SemesterManagement/ViewCourseTimetable?id=' + data.id + '&semesterScheduleId=' + data.semesterScheduleId + ' "><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i> View Timetable</button> </a>';
                    else if (data.scheduleItemType == "Cat")
                        link = '<a href="/ExaminationManagement/CreateCatTimetable?id=' + data.id + '&courseId=' + data.programCourseId + ' "><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-plus"></i> Add Timetable</button></a>' + ' | '
                            + '<a href="/ExaminationManagement/ViewExaminationTimetable?id=' + data.id + '&semesterScheduleId=' + data.semesterScheduleId + ' "><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i> View Timetable</button> </a>';
                    return link;
                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });

});

$('.add-examination-link').click(function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#semester-schedule-container').html(data);
        $('#semester-schedule-modal').modal('show');
        $('.selectpicker').selectpicker();
    });
});

$('.add-cat-schedule-link').click(function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#semester-schedule-container').html(data);
        $('#semester-schedule-modal').modal('show');
        $('.selectpicker').selectpicker();
    });
});

$('.add-course-schedule-link').click(function (event) {

    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#semester-schedule-container').html(data);
        $('#semester-schedule-modal').modal('show');
        $('.selectpicker').selectpicker();
    });
});

$('.add-cat-schedule-link').click(function (event) {

    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#semester-schedule-container').html(data);
        $('#semester-schedule-modal').modal('show');
        $('.selectpicker').selectpicker();
    });
});

$('.complete-semester-link').click(function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#semester-schedule-container').html(data);
        $('#semester-schedule-modal').modal('show');
        $('.selectpicker').selectpicker();
    });
});

$('#semester-schedule-pendingcompletion-list-table').on("click", ".approve-semester-completion", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#semester-schedule-completion-container').html(data);
        $('#semester-schedule-completion-modal').modal('show');
        $('.selectpicker').selectpicker();
    });

});

$("#semester-schedule-completion-modal").on("click", "#btn-approve-completion-request", function (e) {
    var form = $('form#approve-semester-completion-form');
    var action = $(form).attr('action');
    e.preventDefault();
    $.post(action, $(form).serialize(), function (data) {
        $("#semester-schedule-completion-container").html(data);
        $('.selectpicker').selectpicker();
    }, 'html');
});


$('#semester-schedule-container').on("change", "#ProgramCourseId", function (event) {
    var url = "/CourseManagement/GetProgramCourse";
    var programCourseId = $("#ProgramCourseId").val();
    $.getJSON(url, { Id: programCourseId }, function (data) {
        $("#Duration").val(data.DurationInWeeks);

    });

});

$('#semester-schedule-container').on("dp.change", "#StrStartDate", function (e) {
    var date = e.date._d.toLocaleDateString("fr-FR");
    var url = "/CourseManagement/CalculateEndDate";
    $("#StrEndDate").val("");
    var duration = $("#Duration").val();
  
    $.getJSON(url, { strStartDate: date, duration: duration }, function (data) {
        var newDate = data.EndDate;
        console.log(newDate);
        $("#StrEndDate").val(data.EndDate);

    });
});


$("#semester-schedule-modal").on("click", "#btn-complete-semester-request", function (e) {
    var form = $('form#complete-semester-form');
    var action = $(form).attr('action');
    e.preventDefault();
    $.post(action, $(form).serialize(), function (data) {
        $("#semester-schedule-container").html(data);
    }, 'html');


});

$("#semester-schedule-modal").on("click", "#create-examination-btn", function (e) {
    var form = $('form#create-examination-form');
    var action = $(form).attr('action');
    e.preventDefault();
    $.post(action, $(form).serialize(), function (data) {
        $("#semester-schedule-container").html(data);
        $('.selectpicker').selectpicker();
    }, 'html');


});


$("#semester-schedule-modal").on("click", "#create-course-schedule-btn", function (e) {
    var form = $('form#create-course-schedule-form');
    var action = $(form).attr('action');
    e.preventDefault();
    $.post(action, $(form).serialize(), function (data) {
        $("#semester-schedule-container").html(data);
        $('.selectpicker').selectpicker();
    }, 'html');


});

$("#semester-schedule-modal").on("click", "#create-cat-schedule-btn", function (e) {
    var form = $('form#create-cat-schedule-form');
    var action = $(form).attr('action');
    e.preventDefault();
    $.post(action, $(form).serialize(), function (data) {
        $("#semester-schedule-container").html(data);
        $('.selectpicker').selectpicker();
    }, 'html');


});

$(document).ready(function () {

    var scheduleId = $("#ScheduleItemId").val();
    var table = $('#exam-timetable-details-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "dom": 'rt<"bottom"i<"clear">>',
        "ajax": {
            "url": "/ExaminationManagement/GetExamTimetableByScheduleItem",
            "type": "POST",
            "data": { scheduleItemId: scheduleId }

        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'examinationName',
                data: 'examinationName',
                title: "Examination",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'startDate',
                data: 'startDate',
                title: "Date",
                sortable: false,
                searchable: true,
                visible: true
            },
            {
                name: 'startTime',
                data: 'startTime',
                title: "Start Time",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'endTime',
                data: 'endTime',
                title: "End Time",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'duration',
                data: 'duration',
                title: "Duration",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'faculty',
                data: 'faculty',
                title: "Faculty",
                sortable: true,
                searchable: true,
                visible: true
            },
            {
                name: 'createdBy',
                data: 'createdBy',
                title: "Created By",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "DateCreated",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                "title": "Actions",
                "data": { "id": "id" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a class="edit-timetable" href="/SemesterManagement/_EditTimeTable?Id=' + data.id + '"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i> Edit</button></a>';

                }
            }

        ]
    });

});


$(document).ready(function () {

    var scheduleId = $("#ScheduleItemId").val();
    var table = $('#cat-timetable-details-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "dom": 'rt<"bottom"i<"clear">>',
        "ajax": {
            "url": "/ExaminationManagement/GetCatTimetableByScheduleItem",
            "type": "POST",
            "data": { scheduleItemId: scheduleId }

        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'examinationName',
                data: 'examinationName',
                title: "Examination",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'examType',
                data: 'timeTableType',
                title: "Type",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'startDate',
                data: 'startDate',
                title: "Date",
                sortable: false,
                searchable: true,
                visible: true
            },
            {
                name: 'startTime',
                data: 'startTime',
                title: "Start Time",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'endTime',
                data: 'endTime',
                title: "End Time",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'duration',
                data: 'duration',
                title: "Duration",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'faculty',
                data: 'faculty',
                title: "Faculty",
                sortable: true,
                searchable: true,
                visible: true
            },
            {
                name: 'createdBy',
                data: 'createdBy',
                title: "Added By",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "DateCreated",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                "title": "Actions",
                "data": { "id": "id" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a class="edit-timetable" href="/SemesterManagement/_EditTimeTable?Id=' + data.id + '"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i> Edit</button></a>';

                }
            }

        ]
    });

});
$(document).ready(function () {

    var scheduleId = $("#SemesterScheduleItemId").val();
    var table = $('#exam-timetable-schedule-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/ExaminationManagement/GetFullExamTimetableList",
            "type": "POST",
            "data": { scheduleItemId: scheduleId }

        },
        buttons: [
            {
                extend: 'pdf',
                text: '<span class ="glyphicon glyphicon-download-alt"></span> Download Timetable',
                exportOptions: {
                    columns: ':visible'
                }
            }],
        initComplete: function () {
            table.buttons().container()
                .appendTo($('#exam-timetable-schedule-table_wrapper .col-sm-6:eq(0)'));
            let buttons = document.getElementsByClassName("dt-button");
            for (let button of buttons) {
                button.classList.remove("dt-button");
                button.classList.add("btn");
                button.classList.add("btn-default");
                button.classList.add("btn-md");
            }
        },
        columns: [
            {
                name: 'examinationId',
                data: 'examinationId',
                title: "examinationId",
                sortable: false,
                searchable: false,
                visible: false
            },
             {
                 name: 'semesterScheduleItemId',
                 data: 'semesterScheduleItemId',
                 title: "semesterScheduleItemId",
                 sortable: false,
                 searchable: false,
                 visible: false
             },
            {
                name: 'code',
                data: 'code',
                title: "Code",
                sortable: false,
                searchable: true,
                visible: true
            },
            {
                name: 'examinationName',
                data: 'examinationName',
                title: "Exam",
                sortable: false,
                searchable: true,
                visible: true
            },
            {
                name: 'startDate',
                data: 'startDate',
                title: "Date",
                sortable: false,
                searchable: true,
                visible: true
            },
            {
                name: 'startTime',
                data: 'startTime',
                title: "Start Time",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'endTime',
                data: 'endTime',
                title: "End Time",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'duration',
                data: 'duration',
                title: "Duration",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'faculty',
                data: 'faculty',
                title: "Faculty",
                sortable: true,
                searchable: true,
                visible: true
            },
            {
                name: 'createdBy',
                data: 'createdBy',
                title: "Created By",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "DateCreated",
                sortable: true,
                searchable: false,
                visible: false
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });

});

$('#exam-timetable-schedule-table').on("click", ".edit-timetable", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#editExamTimetablecontainer').html(data);
        $('#editExamTimetableModal').modal('show');
    });
});

$('#exam-timetable-schedule-table').on("click", ".view-timetable-details", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#viewExamTimetableontainer').html(data);
        $('#viewExamTimetablemodal').modal('show');
    });
});


$('#viewExamTimetablemodal').on('show.bs.modal', function () {
    var scheduleId = $("#SemesterScheduleItemId").val();
    var examinationId = $("#ExaminationId").val();
    var table = $('#exam-timetable-schedule').DataTable({
        "serverSide": true,
        "proccessing": true,
        "dom": 'rt<"bottom"i<"clear">>',
        "ajax": {
            "url": "/ExaminationManagement/GetExamTimetableList",
            "type": "POST",
            "data": { scheduleItemId: scheduleId, examinationId: examinationId }

        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'startDate',
                data: 'startDate',
                title: "Date",
                sortable: false,
                searchable: true,
                visible: true
            },
            {
                name: 'startTime',
                data: 'startTime',
                title: "Start Time",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'endTime',
                data: 'endTime',
                title: "End Time",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'duration',
                data: 'duration',
                title: "Duration",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'faculty',
                data: 'faculty',
                title: "Faculty",
                sortable: true,
                searchable: true,
                visible: true
            },
            {
                name: 'createdBy',
                data: 'createdBy',
                title: "Created By",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "DateCreated",
                sortable: true,
                searchable: false,
                visible: false
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });
});

$(document).ready(function () {

    var scheduleId = $("#ScheduleItemId").val();
    var table = $('#course-timetable-details-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "dom": 'rt<"bottom"i<"clear">>',
        "ajax": {
            "url": "/SemesterManagement/GetCourseTimetableByScheduleItem",
            "type": "POST",
            "data": { scheduleItemId: scheduleId }

        },
        buttons: [
            {
                extend: 'pdf',
                text: '<span class ="glyphicon glyphicon-download-alt"></span> Download Timetable',
                exportOptions: {
                    columns: ':visible'
                }
            }],
        initComplete: function () {
            table.buttons().container()
                .appendTo($('#course-timetable-details-table_wrapper .col-sm-6:eq(0)'));
            let buttons = document.getElementsByClassName("dt-button");
            for (let button of buttons) {
                button.classList.remove("dt-button");
                button.classList.add("btn");
                button.classList.add("btn-default");
                button.classList.add("btn-md");
            }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'unit',
                data: 'unitName',
                title: "Unit",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'startDate',
                data: 'startDate',
                title: "Date",
                sortable: false,
                searchable: true,
                visible: true
            },
            {
                name: 'startTime',
                data: 'startTime',
                title: "Start Time",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'endTime',
                data: 'endTime',
                title: "End Time",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'duration',
                data: 'duration',
                title: "Duration",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'faculty',
                data: 'faculty',
                title: "Faculty",
                sortable: true,
                searchable: true,
                visible: true
            },
            {
                name: 'createdBy',
                data: 'createdBy',
                title: "Created By",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "DateCreated",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                "title": "Actions",
                "data": { "id": "id"},
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a class="edit-timetable" href="/SemesterManagement/_EditTimeTable?Id=' + data.id + '"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i> Edit</button></a>';                 

                }
            }

        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });

});

$(document).ready(function () {

    var scheduleId = $("#SemesterScheduleItemId").val();
    var table = $('#course-timetable-schedule-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/SemesterManagement/GetFullTimetableList",
            "type": "POST",
            "data": { scheduleItemId: scheduleId }

        },
        buttons: [
            {
                extend: 'pdf',
                text: '<span class ="glyphicon glyphicon-download-alt"></span> Download Timetable',
                exportOptions: {
                    columns: ':visible'
                }
            }],
        initComplete: function () {
            table.buttons().container()
                .appendTo($('#course-timetable-schedule-table_wrapper .col-sm-6:eq(0)'));
            let buttons = document.getElementsByClassName("dt-button");
            for (let button of buttons) {
                button.classList.remove("dt-button");
                button.classList.add("btn");
                button.classList.add("btn-default");
                button.classList.add("btn-md");
            }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'code',
                data: 'code',
                title: "Code",
                sortable: false,
                searchable: true,
                visible: true
            },
            {
                name: 'unit',
                data: 'unitName',
                title: "Unit",
                sortable: false,
                searchable: true,
                visible: true
            },
            {
                name: 'startDate',
                data: 'startDate',
                title: "Date",
                sortable: false,
                searchable: true,
                visible: true
            },
            {
                name: 'startTime',
                data: 'startTime',
                title: "Start Time",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'endTime',
                data: 'endTime',
                title: "End Time",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'duration',
                data: 'duration',
                title: "Duration",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'faculty',
                data: 'faculty',
                title: "Faculty",
                sortable: true,
                searchable: true,
                visible: true
            },
            {
                name: 'createdBy',
                data: 'createdBy',
                title: "Created By",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "DateCreated",
                sortable: true,
                searchable: false,
                visible: false
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });

});

$('#course-timetable-details-table').on("click", ".edit-timetable", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    load_edit_modal(url);
});

$('#exam-timetable-details-table').on("click", ".edit-timetable", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    load_edit_modal(url);
});

function load_edit_modal(url) {
    $.get(url, function (data) {
        $('#edit_timetable_container').html(data);
        $('#edit_timetable_modal').modal('show');
        $('.selectpicker').selectpicker();
    });
}

$("#edit_timetable_modal").on("click", "#update-timetable-btn", function (e) {
    var form = $('form#update-timetable-form');
    var action = $(form).attr('action');
    e.preventDefault();
    $.post(action, $(form).serialize(), function (data) {
        $("#edit_timetable_container").html(data);
        $('.selectpicker').selectpicker();
    }, 'html');
});

$('#course-timetable-schedule-table').on("click", ".view-timetable-details", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#viewCourseTimetableontainer').html(data);
        $('#viewCourseTimetablemodal').modal('show');
    });
});


$('#viewCourseTimetablemodal').on('show.bs.modal', function () {
    var scheduleId = $("#SemesterScheduleItemId").val();
    var unitId = $("#UnitId").val();
    var table = $('#course-timetable-schedule').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/SemesterManagement/GetCourseTimetableList",
            "type": "POST",
            "data": { scheduleItemId: scheduleId, unitId: unitId }

        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'startDate',
                data: 'startDate',
                title: "Date",
                sortable: false,
                searchable: true,
                visible: true
            },
            {
                name: 'startTime',
                data: 'startTime',
                title: "Start Time",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'endTime',
                data: 'endTime',
                title: "End Time",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'duration',
                data: 'duration',
                title: "Duration",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'faculty',
                data: 'faculty',
                title: "Faculty",
                sortable: true,
                searchable: true,
                visible: true
            },
            {
                name: 'createdBy',
                data: 'createdBy',
                title: "Created By",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "DateCreated",
                sortable: true,
                searchable: false,
                visible: false
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });
});



function format_semester(d) {
    return '<div class="slider" style="margin-left:60px">' +
        '<b>Full name: </b>' + d.name + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}
$(document).ready(function () {
    var table = $('#semester-list').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/SemesterManagement/GetSemesters",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "width": "5%",
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'name',
                data: "name",
                title: "Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "Date Created",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'createdby',
                data: 'createdBy',
                title: "Created By",
                //sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'status',
                data: 'status',
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": "id",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a class="edit-semester-details" href="/SemesterManagement/_EditSemester?id=' + data + '"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i> Edit</button></a>' + ' | '
                    + '<a class="decativate-semester" href="/SemesterManagement/_DeactivateSemester?id=' + data + '"><button class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-ban-circle"></i> Deactivate</button></a>';
                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });
    // Add event listener for opening and closing details
    $('#semester-list tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format_semester(row.data()), 'no-padding').show();
            tr.addClass('shown');
            $('div.slider', row.child()).slideDown();
        }
    });
    $('#semester-list').on("click", ".edit-semester-details", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#editSemesterContainer').html(data);
            $('#editSemesterModal').modal('show');
        });
    });

    $('#semester-list').on("click", ".view-semester-details", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#editSemesterContainer').html(data);
            $('#editSemesterModal').modal('show');
        });
    });

    $('#editSemesterModal').on("click", "#btn-update-semester-details", function (event) {
        var form = $('form#update-semester-details');
        var action = $(form).attr('action');
        event.preventDefault();
        $.post(action, $(form).serialize(), function (data) {
            $("#editSemesterContainer").html(data);
        }, 'html');

    });

    $('#semester-list').on("click", ".decativate-semester", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#deactivateSemesterContainer').html(data);
            $('#deactivateSemesterModal').modal('show');
        });

    });
});


$("#btnCreateSemester").on("click", function () {
    var url = $(this).data("url");
    $.get(url, function (data) {
        $('#createSemesterContainer').html(data);
        $('#createSemesterModal').modal('show');
    });
});

$('#SemesterId').change(function (event) {
    var url = "/CourseManagement/CoursesInSemester";
    var programId = $("#ProgramId").val();
    var semesterId = $(this).val();

    $.get(url, { programId: programId, semesterId: semesterId }, function (data) {
        $("#semester-courses-div").html(data);
        $("#semester-courses-div").show();
        $("#SemesterEndDate").val("");
    });
});

$('#TimetableItem_UnitId').change(function (event) {
    var unitName = $("#TimetableItem_UnitId option:selected").text();
    $("#TimetableItem_UnitName").val(unitName);
});

$('#TimetableItem_FacultyId').change(function (event) {
    var faculty = $("#TimetableItem_FacultyId option:selected").text();
    $("#TimetableItem_FacultyName").val(faculty);
});

$(document).ready(function () {
    $('#SemesterId').prop('disabled', true);
    $('#SemesterId').selectpicker('refresh');
});

$('#ProgramId').change(function (event) {
    $('#SemesterId').prop('disabled', false);
    $('#SemesterId').selectpicker('refresh');
});

$('#StrEstimatedStartDate').on("dp.change", function (e) {
    var date = e.date._d.toLocaleDateString("fr-FR");
    var url = "/CourseManagement/CalculateEndDate";

    var totalDuration = $("#TotalSemesterDuration").val();
    if (totalDuration <= 0)
        $("#btn-create-semester-schedule").attr("disabled", true);
    else
        $("#btn-create-semester-schedule").attr("disabled", false);

    $.getJSON(url, { strStartDate: date, duration: totalDuration }, function (data) {
        $("#SemesterEndDate").val(data.EndDate);
        $("#StrEstimatedEndDate").val(data.EndDate);

    });
});

$('.adjust-semester-date-link').on("click", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#semester-schedule-container').html(data);
        $('#semester-schedule-modal').modal('show');
    });
});

$("#ProgramOfferId").change(function () {
    var url = "/ProgramManagement/_ProgramOfferDetails";
    var offerId = $("#ProgramOfferId").val();

    $.get(url, { programOfferId: offerId, jsonFormat: true }, function (data) {
        $("#OfferStartDate").val(data.StrStartDate);
        $("#OfferEndDate").val(data.StrEndDate);

        $('#StrEstimatedStartDate').data("DateTimePicker").minDate(data.StrStartDate);
        $('#StrEstimatedStartDate').data("DateTimePicker").maxDate(data.StrEndDate);

        $(".offer-details-div").show();

    });
});

$('#semester-schedule-modal').on("click", "#btn-update-semester-details", function (event) {
    var form = $('form#edit-semester-schedule-form');
    var action = $(form).attr('action');
    event.preventDefault();
    $.post(action, $(form).serialize(), function (data) {
        $("#semester-schedule-container").html(data);
    }, 'html');

});

$('#semester-schedule-modal').on("dp.change", "#StrStartDate", function (e) {
    var date = e.date._d.toLocaleDateString("fr-FR");
    var url = "/CourseManagement/CalculateEndDate";

    var semesterDuration = $("#Duration").val();
    if (semesterDuration <= 0)
        $("#btn-update-semester-details").attr("disabled", true);
    else
        $("#btn-update-semester-details").attr("disabled", false);

    $.getJSON(url, { strStartDate: date, duration: semesterDuration }, function (data) {
        $("#EndDate").val(data.EndDate);
    });
});

$(document).ready(function () {
    var table = $('#semester-schedule-change-request').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/SemesterManagement/SemesterScheduleChangeRequestPendingApproval",
            "type": "POST",
        },
        columns: [
             {
                 "class": "details-control",
                 "orderable": false,
                 "width": "5%",
                 "data": null,
                 "defaultContent": ""
             },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'scheduleName',
                data: 'scheduleName',
                title: "Schedule Name",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'startDate',
                data: 'startDate',
                title: "Start Date",
                sortable: false,
                searchable: true,
                visible: true
            },
            {
                name: 'endDate',
                data: 'endDate',
                title: "End Date",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'originalStartDate',
                data: 'originalStartDate',
                title: "Original StartDate",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'originalEndDate',
                data: 'originalEndDate',
                title: "Original EndDate",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'programOffer',
                data: 'programOffer',
                title: "Program Offer",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'program',
                data: 'program',
                title: "Program",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'duration',
                data: 'duration',
                title: "Duration",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'requestedBy',
                data: 'requestedBy',
                title: "Requested By",
                sortable: true,
                searchable: true,
                visible: true
            },
            {
                name: 'dateRequested',
                data: "dateRequested",
                title: "Date Requested",
                sortable: true,
                searchable: false,
                visible: false
            },
             {
                "title": "Actions",
                "data": { "id": "id" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a class="approve-semester-adjustment" href="/SemesterManagement/ApproveSemesterScheduleChangeRequest?changeRequestId=' + data.id + ' "> <button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon glyphicon-ok"></i> Approve</button> </a>';
                }
            }
        ]
    });

    $('#semester-schedule-change-request tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format_approval_details(row.data()), 'no-padding').show();
            tr.addClass('shown');
            $('div.slider', row.child()).slideDown();
        }
    });

    function format_approval_details(d) {
        return '<div class="slider" style="margin-left:60px">' +
            '<b>Program: </b>' + d.program + '<br>' +
            '<b>Program Offer: </b>' + d.programOffer + '<br>' +
            '<b>Schedule Name: </b>' + d.scheduleName + '<br>' +
            '<b>Current Start Date: </b>' + d.originalStartDate + '<br>' +
            '<b>Current End Date: </b>' + d.originalEndDate + '<br>' +
            '<b>New Start Date: </b>' + d.startDate + '<br>' +
            '<b>New End Date: </b>' + d.endDate + '<br>' +
            '<b>Semester Duration(Weeks) : </b>' + d.duration + '<br>' +
            '<b>Requested By: </b>' + d.requestedBy + '<br>' +
            '<b>Date Requested: </b>' + d.dateRequested + '<br>' 
    }

    $('#semester-schedule-change-request').on('click', '.approve-semester-adjustment', function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#approve-schedule-change-container').html(data);
            $('#approve-schedule-change-modal').modal('show');
            $(".selectpicker").selectpicker();
        });
    });

    $('#approve-schedule-change-modal').on("click", "#btn-approve-semester-completion-request", function (event) {
        var form = $('form#approve-semester-changerequest-form');
        var action = $(form).attr('action');
        event.preventDefault();
        $.post(action, $(form).serialize(), function (data) {
            $("#approve-schedule-change-container").html(data);
            $(".selectpicker").selectpicker();

        }, 'html');
    });

});
