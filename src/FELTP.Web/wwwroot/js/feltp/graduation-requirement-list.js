﻿
$(document).ready(function () {

    function format_graduationdetails(d) {
        return '<div class="slider" style="margin-left:60px">' +
            '<b>Program:</b> ' + d.program + '<br>' +
            '<b>Didactic Units:</b> ' + d.numberOfDidacticModulesRequired + '<br>' +
            '<b>Activities Required:</b> ' + d.numberOfActivitiesRequired + '<br>' +
            '<b>No. of Thesis Defence Deliverables Required:</b> ' + d.numberOfThesisDefenceDeliverables + '<br>' +
            '<b>Passing Grade:</b> ' + d.passingGrade + '<br>' +
            '<b>Created By: </b>' + d.createdBy + '<br>' +
            '<b>Date Created: </b>' + d.dateCreated + '<br>';
    }

   var graduationRequirement = $('#graduation-requirement-list-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/GraduationManagement/GetGraduationRequirements",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "width": "5%",
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'program',
                data: "program",
                title: "Program",
                sortable: true,
                searchable: true
            },
            {
                name: 'NumberOfDidacticModulesRequired',
                data: 'numberOfDidacticModulesRequired',
                title: "Didactic Topics",
                sortable: false,
                searchable: false
            },
            {
                name: 'NumberOfActivitiesRequired',
                data: 'numberOfActivitiesRequired',
                title: "Activities Required",
                sortable: true,
                searchable: true
            },
            {
                name: 'numberOfThesisDefenceDeliverables',
                data: 'numberOfThesisDefenceDeliverables',
                title: "Thesis Defence Deliverables",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'passingGrade',
                data: 'passingGrade',
                title: "Passing Grade",
                sortable: true,
                searchable: true
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "Date Created",
                sortable: true,
                searchable: true,
                visible: false

            },
            {
                name: 'createdby',
                data: 'createdBy',
                title: "Created By",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                "title": "Actions",
                "data": { "id": "id" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a class="edit-graduation-details" href="/GraduationManagement/_EditGraduationRequirement?id=' + data.id + '"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i> Edit</button></a>'
                }
            }
        ]
    });

    $('#graduation-requirement-list-table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = graduationRequirement.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format_graduationdetails(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
   });


    $('#graduation-requirement-list-table').on("click", ".edit-graduation-details", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#graduation-requirement-container').html(data);
            $('#graduation-requirement-modal').modal('show');
            $("#ProgramId").selectpicker();
            var thesisDefenceDeliverables = $("#NumberOfThesisDefenceDeliverables").val();
            var programId = $("#ProgramId").val();
            if (thesisDefenceDeliverables == 0)
            getThesisDefenceDeliverables(programId);
        });
    });

});

$("#ProgramOfferId").change(function () {
    var offerId = $(this).val();
    getResidentGraduationInformation(offerId);
});


function getResidentGraduationInformation(offerId) {
    var table = $('#resident-graduation-list-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "destroy": true,
        "ajax": {
            "url": "/GraduationManagement/GetResidentGraduationList",
            "type": "POST",
            "data": { programOfferId: offerId }
        },
        columns: [
            {
                name: 'Id',
                data: 'id',
                title: "id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'applicantId',
                data: 'applicantId',
                title: "applicantId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'residentId',
                data: 'residentId',
                title: "residentId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'program',
                data: "programName",
                title: "Program",
                sortable: true,
                searchable: true
            },
            {                                                         
                name: 'programOfferId',
                data: "programOfferId",
                title: "programOfferId",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'programOffer',
                data: "programOffer",
                title: "programOffer",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'registrationNumber',
                data: "registrationNumber",
                title: "Registration Number",
                sortable: true,
                searchable: true
            },
            {
                name: 'residentName',
                data: "residentName",
                title: "Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'qualificationStatus',
                data: "qualificationStatus",
                title: "Qualification Status",
                sortable: true,
                searchable: true
            },
            {
                name: 'completionStatus',
                data: "completionStatus",
                title: "Completion Status",
                sortable: true,
                searchable: true
            },
            {
                name: 'completionDate',
                data: "expectedProgramEndDate",
                title: "Completion Date",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                "title": "Actions",
                "data": { "applicantId": "applicantId", "programOfferId": "programOfferId","id":"id" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a target="_blank"   href="/GraduationManagement/ResidentGraduationDetails?graduateId=' + data.id  + ' ">View Details </a>';
                }
            }
        ]
    });
}

$("#graduation-requirement-modal").on("click", "#btn-edit-graduation-requirements", function (e) {
    var form = $('form#edit-graduation-requirement-form');
    var action = $(form).attr('action');
    e.preventDefault();
    $.post(action, $(form).serialize(), function (data) {
        $("#graduation-requirement-container").html(data);
    }, 'html');

});


