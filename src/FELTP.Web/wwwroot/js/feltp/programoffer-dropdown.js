﻿
$('#ProgramId').change(function () {
    var url = "/ProgramManagement/GetProgramOffers";
    var programId = $("#ProgramId").val();
    var programName = $("#ProgramId option:selected").text();
    $.getJSON(url, { programId: programId }, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, offer) {
            items += "<option value='" + offer.Value + "'>" + offer.Text + "</option>"
        });
        $("#ProgramOfferId").html(items).selectpicker("refresh");
        $("#ProgramName").val(programName);
    });
});

$('#ProgramId').change(function () {
    var programId = $("#ProgramId").val();

    getProgramSemesters(programId);
});

$('#ProgramOfferId').change(function () {
    var offer = $("#ProgramOfferId option:selected").text();
    $("#ProgramOffer").val(offer);
});

$('#enroll-resident-container').on("change", "#ProgramOfferId", function () {
    var url = "/ProgramManagement/_ProgramOfferDetails";
    var offerId = $("#ProgramOfferId").val();
    $.get(url, { programOfferId: offerId }, function (data) {

        $("#offer-details-div").html(data);

    });
});

function getProgramSemesters(programId) {
    var url = "/SemesterManagement/GetProgramSemesters";
    $.getJSON(url, { programId: programId }, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, semester) {
            items += "<option value='" + semester.Value + "'>" + semester.Text + "</option>"
        });
        $("#SemesterId").html(items).selectpicker("refresh");

    });
}



