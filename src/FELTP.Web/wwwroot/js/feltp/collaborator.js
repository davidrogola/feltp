﻿function format_collaborator(d) {
    return '<div class="slider" style="margin-left:60px">' +
        '<b>Collaborator Type: </b>' + d.collaboratorType + '<br>' +
        '<b>Full name: </b>' + d.name + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}
$(document).ready(function () {
    var table = $('#collaborator-list').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/Collaborator/GetCollaborators",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "data": null,
                "width": "5%",
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'collaboratorType',
                data: "collaboratorType",
                title: "Collaborator Type",
                sortable: true,
                searchable: true
            },
            {
                name: 'name',
                data: "name",
                title: "Name",
                sortable: true,
                searchable: true
            },           
            {
                name: 'status',
                data: 'status',
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": { "id": "id" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a class="editCollaborator" href="/Collaborator/_EditCollaborator?id=' + data.id + '"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i> Edit </button></a>' + ' | '
                        + '<a class="view-programs" href="/Collaborator/_ViewProgramCollaborator?id=' + data.id + '&name=' + data.name + '"><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i> View Programs</button></a>';
                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });
    // Add collaborator listener for opening and closing details
    $('#collaborator-list tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format_collaborator(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });

    $('#collaborator-list').on("click", ".editCollaborator", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#editCollaboratorContainer').html(data);
            $('#editCollaboratorModal').modal('show');
            $('.selectpicker').selectpicker();
        });

    });


    $('#collaborator-list').on("click", ".deactivateCollaborator", function (collaborator) {
        collaborator.prcollaboratorDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#deactivateCollaboratorContainer').html(data);
            $('#deactivateCollaboratorModal').modal('show');
        });

    });
});

$("#btnCreateCollaborator").on("click", function () {
    var url = $(this).data("url");

    $.get(url, function (data) {
        $('#createCollaboratorContainer').html(data);
        $('#createCollaboratorModal').modal('show');
        $('.selectpicker').selectpicker();

    });

});

$('#collaborator-list').on("click", ".view-programs", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#program-collaborator-container').html(data);
        $('#program-collaborator-modal').modal('show');
    });
});

$('#program-collaborator-modal').on('show.bs.modal', function () {

    var collaboratorId = $("#CollaboratorId").val();
    $('#program-collaborator').dataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/Collaborator/GetProgramCollaborators",
            "type": "POST",
            "data": { collaboratorId: collaboratorId }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },

            {
                name: 'programName',
                data: "programName",
                title: "Program",
                sortable: true,
                searchable: true
            }
             
        ]
    });
});