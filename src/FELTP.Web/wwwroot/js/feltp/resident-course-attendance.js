﻿
function format(d) {
    return '<div class="slider" style="margin-left:70px">' +
        '<b>Full name: </b>' + d.name + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}
$(document).ready(function () {
    var table = $('#fieldplacementsite-list').dataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/FieldPlacement/GetFieldPlacementSites",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'name',
                data: "name",
                title: "Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'location',
                data: "location",
                title: "Location",
                sortable: true,
                searchable: true
            },
            {
                name: 'building',
                data: "building",
                title: "Building",
                sortable: true,
                searchable: true
            },
            {
                name: 'status',
                data: "status",
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": "id",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/FieldPlacement/_EditFieldPlacementSite?id=' + data + '" class="editFieldPlacementSite"><button class="btn btn-default btn-xs"><i class="glyphicon glyphicon-edit"></i></button></a> | <a href="/FieldPlacement/_DetailsFieldPlacementSite?id=' + data + '" class="detailsFieldPlacementSite"><button class="btn btn-default btn-xs"><i class="glyphicon glyphicon-list-alt"></i></button></a> | <a href="/FieldPlacement/Delete?id=' + data + '" class="deleteFieldPlacementSite"><button class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></button></a>';
                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });
});