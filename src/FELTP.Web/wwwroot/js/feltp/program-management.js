﻿

function format_program(d) {
    return '<div class="slider" style="margin-left:60px">' +
        '<b>Code:</b> ' + d.code + '<br>' +
        '<b>Tier:</b> ' + d.programTier + '<br>' +
        '<b>Name: </b>' + d.name + '<br>' +
        '<b>Duration:</b> ' + d.duration + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}
$(document).ready(function () {
    var table = $('#program-list-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/programmanagement/GetPrograms",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "width": "5%",
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'code',
                data: 'code',
                title: "Code",
                sortable: false,
                searchable: false
            },
            {
                name: 'programtier',
                data: 'programTier',
                title: "Tier",
                sortable: true,
                searchable: true
            },
            {
                name: 'name',
                data: "name",
                title: "Program",
                sortable: true,
                searchable: true
            },
            {
                name: 'description',
                data: "description",
                title: "Description",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'duration',
                data: 'duration',
                title: "Duration",
                sortable: true,
                searchable: true
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "DateCreated",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'createdby',
                data: 'createdBy',
                title: "CreatedBy",
                //sortable: true,
                searchable: false,
                visible: false
            },
            {
                "title": "Actions",
                "data": { "id": "id", "name": "name" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/ProgramManagement/_EditProgram?id=' + data.id + '" class="editProgram" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i> Edit </button></a> | <a class="view-details" href="/ProgramManagement/ProgramDetails?id=' + data.id + '"><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i> View Courses</button></a> ';

                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });

    // Add event listener for opening and closing details
    $('#program-list-table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format_program(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });
    $('#program-list-table').on("click", ".editProgram", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#editProgramContainer').html(data);
            $('#editProgramModal').modal('show');
            $('.selectpicker').selectpicker();
        });

    });
    $('#program-list-table').on("click", ".deleteProgram", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#deleteProgramContainer').html(data);
            $('#deleteProgramModal').modal('show');
        });

    });
    $('#Input').on('keyup', function () {
        alert("bree");
        table.search(this.value).draw();
    });
});

$('#program-list-table').on("click", ".view-program-courses", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#program-courses-container').html(data);
        $('#program-courses-modal').modal('show');
    });
});







$('#program-courses-modal').on('show.bs.modal', function () {

    var programId = $("#ProgramId").val();

    $('#program-courses-list-table').dataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/programmanagement/GetProgramCourses",
            "type": "POST",
            "data": { programId: programId }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'coursetype',
                data: 'category',
                title: "CourseType",
                sortable: true,
                searchable: true
            },
            {
                name: 'semester',
                data: "semester",
                title: "Semester",
                sortable: true,
                searchable: true
            },
            {
                name: 'coursename',
                data: "courseName",
                title: "Course Title",
                sortable: true,
                searchable: true
            },
            {
                name: 'durationinweeks',
                data: 'durationInWeeks',
                title: "Duration (Weeks)",
                sortable: true,
                searchable: true
            }
        ]
    });
});
$(document).ready(function () {
    var programId = $("#ProgramId").val()
    table = $('#program-courses-list-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/programmanagement/GetProgramCourses",
            "type": "POST",
            "data": { programId: programId }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'programId',
                data: 'programId',
                title: "programId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'coursetype',
                data: 'category',
                title: "CourseType",
                sortable: true,
                searchable: true
            },
            {
                name: 'semester',
                data: "semester",
                title: "Semester",
                sortable: true,
                searchable: true
            },
            {
                name: 'coursename',
                data: "courseName",
                title: "Course Title",
                sortable: true,
                searchable: true
            },
            {
                name: 'durationinweeks',
                data: 'durationInWeeks',
                title: "Duration (Weeks)",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": { "id": "id", "programId": "programId" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/ProgramManagement/_RemoveProgramCourse?id=' + data.id + '&programId=' + data.programId + '" class="removeProgramCourse" data-toggle="tooltip" title="Remove"><button class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-ban-circle"></i> Remove </button></a> ';

                }
            }
        ]
    });

});


function format_resource_type(d) {
    return '<div class="slider" style="margin-left:70px">' +
        '<b>Full name: </b>' + d.name + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}
$(document).ready(function () {
    var table = $('#resource-type').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/programmanagement/GetResourceTypes",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "width": "5%",
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'name',
                data: "name",
                title: "Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "Date Created",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'createdby',
                data: 'createdBy',
                title: "Created By",
                //sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'status',
                data: 'status',
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": "id",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/ProgramManagement/_EditResourceType?id=' + data + '" class="editResourceType"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i> Edit</button></a> | <a href="/ProgramManagement/_DeactivateResourceType?id=' + data + '" class="deactivateResourceType"><button class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-ban-circle"></i> Deactivate</button></a>';
                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });
    // Add event listener for opening and closing details
    $('#resource-type tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format_resource_type(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });


    $('#resource-type').on("click", ".editResourceType", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#editResourceTypeContainer').html(data);
            $('#editResourceTypeModal').modal('show');
        });

    });

    $('#resource-type').on("click", ".detailsResourceType", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#detailsResourceTypeContainer').html(data);
            $('#detailsResourceTypeModal').modal('show');
        });

    });

    $('#resource-type').on("click", ".deactivateResourceType", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#deactivateResourceTypeContainer').html(data);
            $('#deactivateResourceTypeModal').modal('show');
        });

    });
});
$("#btnCreateResourceType").on("click", function () {

    var url = $(this).data("url");

    $.get(url, function (data) {
        $('#createResourceTypeContainer').html(data);
        $('#createResourceTypeModal').modal('show');
    });

});


function format_resource(d) {
    return '<div class="slider" style="margin-left:60px">' +
        '<b>Resource Type:</b> ' + d.resourceType + '<br>' +
        '<b>Resource Title:</b> ' + d.resourceTitle + '<br>' +
        '<b>Resource Description:</b> ' + d.resourceDescription + '<br>' +
        '<b>Source: </b>' + d.source + '<br>' +
        '<b>Source Contact Details:</b> ' + d.sourceContactDetails + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}

$(document).ready(function () {
    var programId = $("#ProgramId").val()
    var id = $("#id").val()
    var fileContent = $("#fileContent").val()
    table = $('#program-resource-list-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/programmanagement/GetProgramResourcesByProgramId",
            "type": "POST",
            "data": { programId: programId }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'programId',
                data: 'programId',
                title: "Program Id",
                sortable: true,
                visible: false
            },
            {
                name: 'resourcesType',
                data: 'resourcesType',
                title: 'Resources Type',
                sortable: false,
                searchable: true
            },
            {
                name: 'resourceTitle',
                data: 'resourceTitle',
                title: "Resource Title",
                sortable: false,
                searchable: true
            },

            {
                name: 'resourceDescription',
                data: 'resourceDescription',
                title: "Resource Description",
                visible: false
            },


        ]
    });

});

$(document).ready(function () {
    var table = $('#resource-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/programmanagement/GetResources",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "width": "5%",
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'resourceType',
                data: "resourceType",
                title: "Resource Type",
                sortable: true,
                searchable: true
            },
            {
                name: 'resourceTitle',
                data: "resourceTitle",
                title: "Resource Title",
                sortable: true,
                searchable: true
            },
            {
                name: 'resourceDescription',
                data: "resourceDescription",
                title: "Resource Description",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'source',
                data: "source",
                title: "Source",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'sourcecontactdetails',
                data: "sourceContactDetails",
                title: "Source Contact Details",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "Date Created",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'createdby',
                data: 'createdBy',
                title: "Created By",
                //sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'status',
                data: 'status',
                title: "Status",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                "title": "Actions",
                "data": { "id": "id", "resourceTitle":"resourceTitle" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/ProgramManagement/_EditResource?id=' + data.id + '" class="editResource"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i> Edit </button></a>'
                        + '| <a class="ProgramCourseFiles" href="/ProgramManagement/ViewProgramResourcesFiles?resourceId=' + data.id + '&title=' + data.resourceTitle +
                          '"><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i> View Files</button></a>';

                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }

    });
    $("#resource-table").on("click", ".ProgramCourseFiles", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#viewFilesContainer').html(data);
            $('#viewResourceFilesModal').modal('show');
        });
    });
    $('#viewResourceFilesModal').on('show.bs.modal', function () {

        var resourceId = $("#resourceId").val();
        $('#resource-files-tables').dataTable({
            "serverSide": true,
            "proccessing": true,
            "ajax": {
                "url": "/ProgramManagement/ViewProgramResourcesFiles",
                "type": "POST",
                "data": { Id: resourceId }
            },
            columns: [

                {
                    name: 'id',
                    data: 'id',
                    title: "Id",
                    sortable: false,
                    searchable: false,
                    visible: false
                },
                {
                    name: 'resourceId',
                    data: 'resourceId',
                    title: "resource id",
                    sortable: false,
                    searchable: false,
                    visible: false
                },
                {
                    name: 'fileName',
                    data: 'fileName',
                    title: "File Name",
                    sortable: false,
                    searchable: false,
                    visible: true
                },
                {
                    name: 'contentType',
                    data: 'contentType',
                    title: "Content Type",
                    sortable: false,
                    searchable: false,
                    visible: false
                },
                {
                    name: 'dateCreated',
                    data: 'dateCreated',
                    title: "Date Created",
                    sortable: false,
                    searchable: false,
                    visible: true
                },
                {
                    name: 'fileContent',
                    data: 'fileContent',
                    title: "File ",
                    sortable: false,
                    searchable: false,
                    visible: false
                },

                {
                    "title": "Actions",
                    "data": { "id": "id" },
                    "searchable": false,
                    "sortable": false,
                    "render": function (data, type, full, meta) {
                        return '<a href="/ProgramManagement/DownLoadResourceFile?Id=' + data.id + '" class="downLoadFile" data-toggle="tooltip" title="Download"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-download"></i> Download Resource File </button></a> ';
                    }
                }
            ]
        });
    });


    // Add collaborator listener for opening and closing details
    $('#resource-table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format_resource(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });
    $('#resource-table').on("click", ".editResource", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#editResourceContainer').html(data);
            $('#editResourceModal').modal('show');
            $('.selectpicker').selectpicker();
        });
    });

    $("#editResourceModal").on("click", "#edit-resource-btn", function (e) {
        var form = $('form#edit-resource-form');
        var action = $(form).attr('action');
        e.preventDefault();
        $.post(action, $(form).serialize(), function (data) {
            $("#editResourceContainer").html(data);
            $('.selectpicker').selectpicker();
        }, 'html');


    });
 



    $('#resource-table').on("click", ".deactivateResource", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#deactivateResourceContainer').html(data);
            $('#deactivateResourceModal').modal('show');
        });

    });
});

$('#editResourceModal').on('show.bs.modal', function () {
    var resourceId = $("#ResourceId").val();

    $('#program-resources-list-table').dataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/ProgramManagement/GetProgramResources",
            "type": "POST",
            "data": { resourceId: resourceId }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'programtier',
                data: 'programTier',
                title: "Program Tier",
                sortable: true,
                searchable: true
            },
            {
                name: 'programname',
                data: "programName",
                title: "Program Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "Date Created",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'createdby',
                data: 'createdBy',
                title: "Created By",
                sortable: true,
                searchable: true,
                visible: true
            },
            {
                name: 'status',
                data: 'status',
                title: "Status",
                sortable: true,
                searchable: true
            }
        ]
    });
});

$("#btnCreateResource").on("click", function () {
    var url = $(this).data("url");

    $.get(url, function (data) {
        $('#createResourceContainer').html(data);
        $('#createResourceModal').modal('show');
        $('.selectpicker').selectpicker();

    });

});





$(document).ready(function () {
    $('#program-resources-list-table').dataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/programmanagement/GetProgramResources",
            "type": "POST"
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'programname',
                data: "programName",
                title: "Program Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'programtier',
                data: 'programTier',
                title: "Program Tier",
                sortable: true,
                searchable: true
            },
            {
                name: 'resourcetype',
                data: 'resourceType',
                title: "Resource Type",
                sortable: true,
                searchable: true
            },
            {
                name: 'resourcename',
                data: "resourceName",
                title: "Resource Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "Date Created",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'createdby',
                data: 'createdBy',
                title: "Created By",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'status',
                data: 'status',
                title: "Status",
                sortable: true,
                searchable: true
            }
        ]
    });
});

function format_program_tier(d) {
    return '<div class="slider" style="margin-left:60px">' +
        '<b>Full name: </b>' + d.name + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}
$(document).ready(function () {
    var table = $('#program-tier').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/programmanagement/GetProgramTiers",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "width": "5%",
                "orderable": false,
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'name',
                data: "name",
                title: "Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "Date Created",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'createdby',
                data: 'createdBy',
                title: "Created By",
                //sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'status',
                data: 'status',
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": "id",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/ProgramManagement/_EditProgramTier?id=' + data + '" class="editProgramTier" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i> Edit </button></a> | <a href="/ProgramManagement/_DeleteProgramTier?id=' + data + '" class="deleteProgramTier" data-toggle="tooltip" title="Deactivate"><button class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-ban-circle"></i> Deactivate</button></a>';
                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });

    // Add event listener for opening and closing details
    $('#program-tier tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format_program_tier(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });
    $('#program-tier').on("click", ".editProgramTier", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#editProgramTierContainer').html(data);
            $('#editProgramTierModal').modal('show');
        });

    });

    $('#program-tier').on("click", ".detailsProgramTier", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#detailsProgramTierContainer').html(data);
            $('#detailsProgramTierModal').modal('show');
        });

    });

    $('#program-tier').on("click", ".deleteProgramTier", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#deleteProgramTierContainer').html(data);
            $('#deleteProgramTierModal').modal('show');
        });

    });
});
$("#btnCreateProgramTier").on("click", function () {

    var url = $(this).data("url");
    var programtierurl = "/ProgramManagement/GetProgramTiers";

    $.get(url, function (data) {
        $('#createProgramTierContainer').html(data);
        $('#createProgramTierModal').modal('show');
    });

});



$("#btnCreateProgram").on("click", function () {

    var url = $(this).data("url");
    var programtierurl = "/ProgramManagement/GetProgramTiers";

    $.get(url, function (data) {
        $('#createProgramContainer').html(data);

        $('#createProgramModal').modal('show');
        $('.selectpicker').selectpicker();

        $.getJSON(programtierurl, {}, function (data) {
            var items = "<option value='0'> Select </option>";
            $.each(data, function (i, tier) {
                items += "<option value='" + tier.Value + "'>" + tier.Text + "</option>"
            });
            $("#ProgramTierId").html(items).selectpicker("refresh");

        });
    });

});

function format(d) {
    return '<div class="slider" style="margin-left:70px">' +
        '<b>Program: </b>' + d.program + '<br>' +
        '<b>Program offer: </b>' + d.programOffer + '<br>' +
        '<b>Course: </b>' + d.programCourse + '<br>' +
        '<b>Satrt Date: </b>' + d.startDate + '<br>' +
        '<b>End Date: </b>' + d.endDate + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}
$(document).ready(function () {
    var facultyId = $("#FacultyId").val();
    var table = $('#course-schedule-list').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/ProgramManagement/GetCourseSchedule",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'name',
                data: "name",
                title: "Schedule Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'programoffer',
                data: "programOffer",
                title: "Program Offer",
                sortable: true,
                searchable: true
            },
            {
                name: 'programcourse',
                data: "programCourse",
                title: "Course",
                sortable: true,
                searchable: true
            },
            {
                name: 'startdate',
                data: "startDate",
                title: "Start Date",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'enddate',
                data: 'endDate',
                title: "End date",
                //sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'status',
                data: 'status',
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": "id",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/ProramManagement/_EditCourseSchedule?id=' + data + '" class="editCourseSchedule"><button class="btn btn-default btn-xs"><i class="glyphicon glyphicon-edit"></i></button></a> | <a href="/ProgramManagement/_DetailsCourseSchedule?id=' + data + '" class="detailsCourseSchedule"><button class="btn btn-default btn-xs"><i class="glyphicon glyphicon-list-alt"></i></button></a> | <a href="/ProgramManagement/Delete?id=' + data + '" class="deleteCourseSchedule"><button class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></button></a>';
                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });
    // Add event listener for opening and closing details
    $('#course-schedule tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });
    $('#course-schedule').on("click", ".editCourseSchedule", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#editCourseScheduleContainer').html(data);
            $('#editCourseScheduleModal').modal('show');
            $('.selectpicker').selectpicker();
        });

    });

    $('#course-schedule').on("click", ".detailsCourseSchedule", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#detailsCourseScheduleContainer').html(data);
            $('#detailsCourseScheduleModal').modal('show');
        });

    });

    $('#course-schedule').on("click", ".deleteCourseSchedule", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#deleteCourseScheduleContainer').html(data);
            $('#deleteCourseScheduleodal').modal('show');
        });

    });
});

$("#btnCreateProgramCourse").on("click", function () {
    var url = $(this).data("url");
    var programId = $("#ProgramId").val();
    $.get(url, function (data) {
        $('#createProgramCourseContainer').html(data);
        $('#createProgramCourseModal').modal('show');
        $('.selectpicker').selectpicker();
    });
});

$('#program-courses-list-table').on("click", ".removeProgramCourse", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#removeProgramCourseContainer').html(data);
        $('#removeProgramCourseModal').modal('show');
    });

});
