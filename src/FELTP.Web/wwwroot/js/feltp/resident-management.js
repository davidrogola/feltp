﻿function format_cadre(d) {
    return '<div class="slider" style="margin-left:70px">' +
        '<b>Full name: </b>' + d.name + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}
$(document).ready(function () {
    var table = $('#cadre-list').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/residentmanagement/GetCadres",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "width": "5%",
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'name',
                data: "name",
                title: "Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "Date Created",
                sortable: true,
                searchable: true
            },
            {
                name: 'createdby',
                data: 'createdBy',
                title: "Created By",
                //sortable: true,
                searchable: true
            },
            {
                name: 'status',
                data: 'status',
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": "id",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/ResidentManagement/_EditCadre?id=' + data + '" class="editCadre" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i></button></a> | <a href="/ResidentManagement/_DetailsCadre?id=' + data + '" class="detailsCadre" data-toggle="tooltip" title="Details"><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i></button></a> | <a href="/ResidentManagement/DeactivateCadre?id=' + data + '" class="deactivateCadre" data-toggle="tooltip" title="Deactivate"><button class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-ban-circle"></i></button></a>';
                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });

    // Add event listener for opening and closing details
    $('#cadre-list tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format_cadre(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });
    $('#cadre-list').on("click", ".editCadre", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#editCadreContainer').html(data);
            $('#editCadreModal').modal('show');
            $('.selectpicker').selectpicker();
        });

    });

    $('#cadre-list').on("click", ".detailsCadre", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#detailsCadreContainer').html(data);
            $('#detailsCadreModal').modal('show');
            $('.selectpicker').selectpicker();
        });

    });

    $('#cadre-list').on("click", ".deleteCadre", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#deleteCadreContainer').html(data);
            $('#deleteCadreModal').modal('show');
        });

    });
});

$("#btnCreateCadre").on("click", function () {
    var url = $(this).data("url");
    $.get(url, function (data) {
        $('#createCadreContainer').html(data);
        $('#createCadreModal').modal('show');
    });

});

$("#btnUpdateApplicantBioData").on("click", function (event) {
    var url = $(this).data("url");
    event.preventDefault();

    $.get(url, function (data) {
        $('#updateApplicantBioDataContainer').html(data);
        $('#updateApplicantBioDataModal').modal('show');
        $('.selectpicker').selectpicker();
    });

});

$("#btnUpdateApplicantContactInfo").on("click", function (event) {

    var url = $(this).data("url");
    event.preventDefault();

    $.get(url, function (data) {
        $('#updateApplicantContactInfoContainer').html(data);
        $('#updateApplicantContactInfoModal').modal('show');
        $('.selectpicker').selectpicker();
    });

});



$("#generate-resident-coursework").on("click", function (event) {
    var url = $(this).data("url");
    event.preventDefault();
    $.get(url, function (data) {
        $('#generate-resident-coursework-container').html(data);
        $('#generate-resident-coursework-modal').modal('show');
    });

});



