﻿function get_deliverable(data) {
    console.log(data);
         var result = '<div class="slider" style="margin-left:60px">'
         result += '<table id="resident-deliverable-list" class="table table-bordered table-striped" cellspacing="0">';
         result += '<thead><th>Code</th><th>Deliverable</th><th>Status</th><th>Actions</th></thead>';
         if (data.length > 0) {
             $.each(data, function (i, item) {
                 result += '<tr">' +
                     '<td>' + item.Code + '</td>' +
                     '<td>' + item.Name + '</td>' +
                     '<td>' + item.ProgressStatus + '</td>' +
                     '<td>' + '<a href="/Deliverable/ViewSubmissions?deliverableId=' + item.Id + '&name=' + item.Name + '" class="view-deliverable-submissions">View Submissions </a> ' + '|'+ '<a href="/Deliverable/SubmitDeliverableFeedback?deliverableId=' + item.Id + '&name=' + item.Name + '&residentId='+item.ResidentId + '" class="submit-deliverable-feedback">Submit Feedback </a> ' + '</td>' +
                     '</tr>';
             });
         }
         else
         {
             result += '<tr">' +
                 '<td>' + 'Deliverables not found' + '</td>' +
                 '<td>' + 'N/A' + '</td>' +
                 '<td>' + 'N/A' + '</td>' +
                 '<td>' + 'N/A' + '</td>' +
                 '</tr>';
         }
      
        result += '</table>'
        result += '</div>';
        return result;
};
$('#resident-activities-list-table').on("click", ".view-deliverable-submissions", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#resident-deliverable-submission-container').html(data);
        $('#resident-deliverable-submission-modal').modal('show');
    });
});

$('#resident-activities-list-table').on("click", ".submit-deliverable-feedback", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#resident-deliverable-submission-container').html(data);
        $('#resident-deliverable-submission-modal').modal('show');
    });
});


$(document).ready(function () {

    var enrollmentId = $("#ProgramEnrollemntId").val();

    getResidentCourses(enrollmentId, null);
    getResidentActivities(enrollmentId, null);
    getResidentExaminationScores(enrollmentId, null);

});

$("#SemesterId").change(function () {
    var semesterId = $(this).val();
    $("#SelectedSemester").val(semesterId);
    var enrollmentId = $("#ProgramEnrollemntId").val();
    getResidentExaminationScores(enrollmentId, semesterId);
    getResidentCourses(enrollmentId, semesterId);
    getResidentActivities(enrollmentId, semesterId);
});

function format_examination(d) {
    return '<div class="slider" style="margin-left:60px">' +
        '<b>Semester:</b> ' + d.semester + '<br>' +
        '<b>Code:</b> ' + d.code + '<br>' +
        '<b>Unit Name:</b> ' + d.name + '<br>' +
        '<b>Score:</b> ' + d.score + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>';
}
function getResidentCourses(enrollmentId,semesterId)
{ 
    $('#resident-courses-list-table').dataTable({
        "serverSide": true,
        "proccessing": true,
        "destroy": true,
        "ajax": {
            "url": "/ResidentManagement/GetResidentCourseWork",
            "type": "POST",
            "data": { programEnrollmentId: enrollmentId, semesterId: semesterId }
        },
        columns: [
            {
                name: 'id',
                data: 'residentCourseWorkId',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'coursetype',
                data: "courseType",
                title: "Course Type",
                sortable: true,
                searchable: true
            },
            {
                name: 'semester',
                data: "semester",
                title: "Semester",
                sortable: true,
                searchable: true
            },
            {
                name: 'course',
                data: "course",
                title: "Course",
                sortable: true,
                searchable: true
            },
            {
                name: 'status',
                data: 'progressStatus',
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                name: 'datecreated',
                data: 'dateCreated',
                title: "Date Created",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                "title": "Actions",
                "data": { "residentCourseWorkId": "residentCourseWorkId", "course": "course" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a class="view-resident-module" href="/ResidentManagement/ResidentUnitsList?id=' + data.residentCourseWorkId + '&course=' + data.course + '"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-list-alt"></i> View Units</button> </a>' + '| ' + '<a class="view-resident-attendance" href="/ResidentManagement/ResidentCourseAttendance?id=' + data.residentCourseWorkId + '&course=' + data.course + '"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-list-alt"></i> View Attendance</button> </a>';
                }
            }
        ]
    });
}

function getResidentActivities(enrollmentId,semesterId) {
    var residentActivityTable = $('#resident-activities-list-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "destroy": true,
        "ajax": {
            "url": "/ResidentManagement/GetResidentFieldPlacementActivities",
            "type": "POST",
            "data": { programEnrollmentId: enrollmentId, semesterId: semesterId }
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "width": "5%",
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'semester',
                data: "semester",
                title: "Semester",
                sortable: true,
                searchable: true
            },
            {
                name: 'activity',
                data: "activity",
                title: "Activity",
                sortable: true,
                searchable: true
            },
            {
                name: 'status',
                data: 'progressStatus',
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                name: 'datecreated',
                data: 'dateCreated',
                title: "Date Created",
                sortable: true,
                searchable: true,
                visible: false

            },
            {
                "title": "Actions",
                "data": { "id": "id", "activity": "activity" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/FieldPlacement/ResidentFieldPlacementInfo?residentActivityId=' + data.id + '" class="view-resident-placement"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-list-alt"></i> View Placement</button> </a>';
                }
            }
        ]
    });

    $('#resident-activities-list-table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = residentActivityTable.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            var rowData = row.data();
            var data;
            $.getJSON("/Deliverable/GetResidentDeliverables", { residentActivityId: rowData.id }, function (data) {
                row.child(get_deliverable(data), 'no-padding').show();
                tr.addClass('shown');
                $('div.slider', row.child()).slideDown();
            });

        }
    });
}

function getResidentExaminationScores(enrollmentId,semesterId) {

    var residentexamTable = $('#resident-examinationscores-list').DataTable({
        "serverSide": true,
        "proccessing": true,
        "destroy": true,
        "ajax": {
            "url": "/ExaminationManagement/GetResidentExaminationScores",
            "type": "POST",
            "data": { programEnrollmentId: enrollmentId, semesterId: semesterId }
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "width": "5%",
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'residentCourseWorkId',
                data: 'residentCourseWorkId',
                title: "residentCourseWorkId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'residentUnitId',
                data: 'residentUnitId',
                title: "residentUnitId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'hasFieldPlacementActivity',
                data: 'hasFieldPlacementActivity',
                title: "hasFieldPlacementActivity",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'id',
                data: 'residentCourseWorkId',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'semester',
                data: "semester",
                title: "Semester",
                sortable: true,
                searchable: true
            },
            {
                name: 'code',
                data: "code",
                title: "Code",
                sortable: true,
                searchable: true
            },
            {
                name: 'courseName',
                data: 'name',
                title: "Course Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'score',
                data: 'score',
                title: "Score",
                sortable: true,
                searchable: true
            },
            {
                name: 'dateCreated',
                data: 'dateCreated',
                title: "Date Created",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'createdBy',
                data: 'createdBy',
                title: "Created By",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                "title": "Actions",
                "data": { "residentCourseWorkId": "residentCourseWorkId", "residentUnitId": "residentUnitId", "hasFieldPlacementActivity": "hasFieldPlacementActivity", "score": "score", "name": "name" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    if (data.hasFieldPlacementActivity == true && data.score != null) {
                        return '<a class="view-resident-deliverable-score" href="/Deliverable/ResidentUnitDeliverables?residentUnitId=' + data.residentUnitId + '&unitName=' + data.name + '">View Deliverable Scores </a>';
                    }
                    else {
                        return 'N/A'
                    }

                }
            }
        ]
    });

    $('#resident-examinationscores-list tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = residentexamTable.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format_examination(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });
}

$('#resident-courses-list-table').on("click", ".view-resident-module", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#resident-modules-container').html(data);
        $('#resident-modules-modal').modal('show');
    });
});

$('#resident-activities-list-table').on("click", ".view-resident-placement", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#resident-fieldplacement-container').html(data);
        $('#resident-fieldplacement-modal').modal('show');
    });
});


$('#resident-courses-list-table').on("click", ".view-resident-attendance", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#resident-attendance-container').html(data);
        $('#resident-attendance-modal').modal('show');
    });
});


$(".submit-residentexamination-scores").on("click", function (event) {
    event.preventDefault();
 
    var semesterId = $("#SemesterId").val();
    var enrollmentId = $("#ProgramEnrollemntId").val();
    var residentId = $("#ResidentId").val();
    var offerId = $("#ProgramOfferId").val();
    if (semesterId == 0)
    {
        alert("Please select semester before submitting examination scores");
        return;
    }

    var url = $(this).attr("href");
    $.get(url, { enrollmentId: enrollmentId, semesterId: semesterId, residentId: residentId, offerId: offerId }, function (data) {
        $('#resident-examinationscore-submission-container').html(data);
        $('#resident-examinationscore-submission-modal').modal('show');
    });
});

$(".submit-resident-cat-scores").on("click", function (event) {
    event.preventDefault();

    var semesterId = $("#SemesterId").val();
    var enrollmentId = $("#ProgramEnrollemntId").val();
    var residentId = $("#ResidentId").val();
    var offerId = $("#ProgramOfferId").val();
    if (semesterId == 0)
    {
        alert("Please select semester before submitting cat scores");
        return;
    }

    var url = $(this).attr("href");
    $.get(url, { enrollmentId: enrollmentId, semesterId: semesterId, residentId: residentId, offerId: offerId }, function (data) {
        $('#resident-cat-score-submission-container').html(data);
        $('#resident-catscore-submission-modal').modal('show');
    });
});

$(".submit-residentdeliverable-scores").on("click", function (event) {
    event.preventDefault();

    var semesterId = $("#SemesterId").val();
    var enrollmentId = $("#ProgramEnrollemntId").val();
    var residentId = $("#ResidentId").val();
    var programOfferId = $("#ProgramOfferId").val();
    if (semesterId == 0) {
        alert("Please select semester before submitting deliverable scores");
        return;
    }

    var url = $(this).attr("href");
    $.get(url, { enrollmentId: enrollmentId, semesterId: semesterId, residentId: residentId, programOfferId: programOfferId }, function (data) {
        $('#resident-examinationscore-submission-container').html(data);
        $('#resident-examinationscore-submission-modal').modal('show');
    });
});


$("#resident-examinationscore-submission-modal").on("click", "#input-examination-score-btn", function (e) {
    var form = $('form#input-resident-examination-score-form');
    var action = $(form).attr('action');
    e.preventDefault();
    $.post(action, $(form).serialize(), function (data) {
        $("#resident-examinationscore-submission-container").html(data);
    }, 'html');

});

$("#resident-catscore-submission-modal").on("click", "#input-cat-score-btn", function (e) {
    var form = $('form#input-resident-cat-score-form');
    var action = $(form).attr('action');
    e.preventDefault();
    $.post(action, $(form).serialize(), function (data) {
        $("#resident-cat-score-submission-container").html(data);
    }, 'html');

});


$("#resident-examinationscore-submission-modal").on("click", "#input-resident-score-btn", function (e) {
    var form = $('form#input-resident-deliverable-score-form');
    var action = $(form).attr('action');
    e.preventDefault();
    $.post(action, $(form).serialize(), function (data) {
        $("#resident-examinationscore-submission-container").html(data);
    }, 'html');

});

$("#resident-fieldplacement-modal").on("click", "#btn-initiate-placement", function (e) {
    var form = $('form#initiate-resident-placement-div');
    var action = $(form).attr('action');
    e.preventDefault();
    $.post(action, $(form).serialize(), function (data) {
        $("#resident-fieldplacement-container").html(data);
        $('.selectpicker').selectpicker();
    }, 'html');

});

$("#resident-deliverable-submission-modal").on("click", "#btn-submit-del-evaluations", function (e) {
    var form = $('form#submit-resident-evaluation-div');
    var action = $(form).attr('action');
    e.preventDefault();
    $.post(action, $(form).serialize(), function (data) {
        $("#resident-deliverable-submission-container").html(data);}, 'html');

});


$("#submit-residentdeliverable-modal").on("click", "#btn-submit-deliverable", function (e) {
    var form = $('form#submit-resident-deliverable-form');
    var action = $(form).attr('action');
    var method = $(form).attr('method');
    e.preventDefault();

    var fileupload = $("#DeliverableFile").get(0);
    var files = fileupload.files;
    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append('DeliverableFile', files[i]);
    }

    // You can update the jquery selector to use a css class if you want
    $("input[type='text'").each(function (x, y) {
        data.append($(y).attr("name"), $(y).val());
    });

    $("select").each(function (x, y) {
        data.append($(y).attr("name"), $(y).val());
    });

    $("input[type='hidden'").each(function (x, y) {
        data.append($(y).attr("name"), $(y).val());
    });
    $("input[type='checkbox'").each(function (x, y) {
        data.append($(y).attr("name"), $(y).val());
    });

    $.ajax({
        type: method,
        url: action,
        contentType: false,
        processData: false,
        data: data
    }).done(function (res) {
        $("#submit-residentdeliverable-container").html(res);
        $('.selectpicker').selectpicker();
        var submissionType = $('#SubmissionType').val();
        if (submissionType == 2) // Final submission
        {
            $("#committee-approval-table").show();
            $('#toggle-approval-commitee-div').show();
            $(".hide-approval-committee").hide();
        }
        else {
            $("#committee-approval-table").hide();
            $('#toggle-approval-commitee-div').hide();
            $("#hide-approval-commitee-div").hide();
        }
    }).fail(function (xhr, b, error) {
        alert(error);
    });

});
$('#resident-modules-modal').on('show.bs.modal', function () {

    var courseWorkId = $("#ResidentCourseWorkId_UnitList").val();

    $('#resident-module-list').dataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/ResidentManagement/GetResidentUnits",
            "type": "POST",
            "data": { residentCourseWorkId: courseWorkId }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'code',
                data: 'code',
                title: "Code",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'unit',
                data: "unit",
                title: "Unit",
                sortable: true,
                searchable: true
            },
            {
                name: 'createdBy',
                data: 'createdBy',
                title: "Created By",
                sortable: true,
                searchable: true
            },
            {
                name: 'datecreated',
                data: 'dateCreated',
                title: "Date Created",
                sortable: true,
                searchable: true,
                visible: false

            }
        ]
    });
});


$('#resident-deliverable-submission-modal').on('show.bs.modal', function () {

    var deliverableId = $("#ResidentDeliverableId").val();

    $('#resident-deliverable-submissions-list').dataTable({
        "serverSide": true,
        "proccessing": true,
        "dom": 'rt<"bottom"i<"clear">>',
        "ajax": {
            "url": "/Deliverable/GetResidentDeliverableSubmissions",
            "type": "POST",
            "data": { deliverableId: deliverableId }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'title',
                data: "title",
                title: "Title",
                sortable: true,
                searchable: true
            },
            {
                name: 'programmaticArea',
                data: "programmaticArea",
                title: "Programmatic Area",
                sortable: true,
                searchable: true
            },
            {
                name: 'submissionType',
                data: 'submissionType',
                title: "Submission Type",
                sortable: true,
                searchable: true
            },
            {
                name: 'dateSubmitted',
                data: 'dateSubmitted',
                title: "Date Submitted",
                sortable: true,
                searchable: true
            },
            {
                name: 'CreatedBy',
                data: 'createdBy',
                title: "CreatedBy",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": { "id": "id" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/Deliverable/DownloadSubmission?submissionId=' + data.id + '">Download</a>';
                }
            }
        ]
    });
});



$('#resident-attendance-modal').on('show.bs.modal', function () {
    var courseWorkId = $("#ResidentCourseWorkId_AttendanceList").val();

$('#residents-course-attendance').dataTable({
    "serverSide": true,
    "proccessing": true,
    "dom": 'rt<"bottom"i<"clear">>',
    "ajax": {
        "url": "/ResidentManagement/GetResidentCourseAttendance",
        "type": "POST",
        "data": { Id: courseWorkId }
        
    },
    columns: [
        {
            name: 'id',
            data: 'residentCourseWorkId',
            title: "Id",
            sortable: false,
            searchable: false,
            visible: false
        },
        {
            name: 'code',
            data: 'code',
            title: "Code",
            sortable: false,
            searchable: false,
            visible: true
        },
        {
            name: 'unit',
            data: 'unit',
            title: "Unit",
            sortable: false,
            searchable: false,
            visible: true
        },
        {
            name: 'status',
            data: 'status',
            title: "Attendance Status",
            sortable: true,
            searchable: true
        },
        {
            name: 'attendanceDate',
            data: "attendanceDate",
            title: "Attendance Date",
            sortable: true,
            searchable: true
        },
        {
            name: 'startTime',
            data: "startTime",
            title: "Start Time",
            sortable: true,
            searchable: true
        },
        {
            name: 'endTime',
            data: "endTime",
            title: "End Time",
            sortable: true,
            searchable: true
        },
        {
            name: 'trainer',
            data: "trainer",
            title: "Trainer",
            sortable: true,
            searchable: true
        }
    ]
    });
});

$("#resident-examinationscores-list").on("click",".view-resident-deliverable-score", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#resident-modules-container').html(data);
        $('#resident-modules-modal').modal('show');
    });
});

$('#resident-modules-modal').on('show.bs.modal', function () {

    var residentUnitId = $("#ResidentUnitId").val();

    $('#resident-deliverable-scores-list').dataTable({
        "serverSide": true,
        "proccessing": true,
        "dom": 'rt<"bottom"i<"clear">>',
        "ajax": {
            "url": "/Deliverable/GetResidentUnitDeliverables",
            "type": "POST",
            "data": { residentUnitId: residentUnitId }
        },
        columns: [
            {
                name: 'residentDeliverableId',
                data: 'residentDeliverableId',
                title: "residentDeliverableId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'code',
                data: "code",
                title: "Code",
                sortable: true,
                searchable: true
            },
            {
                name: 'deliverable',
                data: "deliverable",
                title: "Deliverable",
                sortable: true,
                searchable: true
            },
            {
                name: 'score',
                data: 'score',
                title: "Score",
                sortable: true,
                searchable: true
            },
            {
                name: 'progressStatus',
                data: 'progressStatus',
                title: "Status",
                sortable: true,
                searchable: true
            }
        ]
    });
});



