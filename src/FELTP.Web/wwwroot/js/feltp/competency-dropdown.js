﻿$(document).ready(function () {
    var url = "/Competency/GetCompetencyDropdown";
    $.getJSON(url, {}, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, comp) {
            items += "<option value='" + comp.Value + "'>" + comp.Text + "</option>"
        });
        $("#CompetencyId").html(items).selectpicker("refresh");

    });
});