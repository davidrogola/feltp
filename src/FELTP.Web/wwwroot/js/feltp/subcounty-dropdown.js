﻿$("#Person_AddressInfo_CountyId").change(function () {
    var countyId = $("#Person_AddressInfo_CountyId").val();
    getSubCounties(countyId);
});


function getSubCounties(countyId) {
    var url = "/LookUp/GetSubCounties";
    $.getJSON(url, { countyId: countyId }, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, county) {
            items += "<option value='" + county.Value + "'>" + county.Text + "</option>"
        });
        $("#SubCountyId").html(items).selectpicker("refresh");
        $("#Person_AddressInfo_SubCountyId").html(items).selectpicker("refresh");
        $("#AddressInfo_SubCountyId").html(items).selectpicker("refresh");
    });
}

$('#createProgramLocationModal').on('show.bs.modal', function () {
    $("#CountyId").change(function () {
        var countyId = $("#CountyId").val();
        getSubCounties(countyId);
    });
});

$('#createFieldPlacementSiteModal').on('show.bs.modal', function () {
    $("#CountyId").change(function () {
        var countyId = $("#CountyId").val();
        getSubCounties(countyId)
    });
});

$('#editProgramLocationModal').on('show.bs.modal', function () {
    $("#CountyId").change(function () {
        var countyId = $("#CountyId").val();
        getSubCounties(countyId)
    });
});


$('#updateApplicantContactInfoModal').on('show.bs.modal', function () {
    $("#AddressInfo_CountyId").change(function () {
        var countyId = $("#AddressInfo_CountyId").val();
        getSubCounties(countyId)
    });
});

$('#updateFacultyContactInfoModal').on('show.bs.modal', function () {
    $("#AddressInfo_CountyId").change(function () {
        var countyId = $("#AddressInfo_CountyId").val();
        getSubCounties(countyId)
    });
});