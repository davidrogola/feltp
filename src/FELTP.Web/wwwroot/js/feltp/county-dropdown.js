﻿$("#Person_AddressInfo_CountryId").change(function () {
    var countryId = $("#Person_AddressInfo_CountryId").val();
    getCounties(countryId);
});

function getCounties(countryId) {
    var url = "/LookUp/GetCounties";
    $.getJSON(url, { countryId: countryId }, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, county) {
            items += "<option value='" + county.Value + "'>" + county.Text + "</option>"
        });
        $("#Person_AddressInfo_CountyId").html(items).selectpicker("refresh");
        $("#CountyId").html(items).selectpicker("refresh");
        $("#AddressInfo_CountyId").html(items).selectpicker("refresh");

    });

}

$('#createProgramLocationModal').on('show.bs.modal', function () {
    $("#CountryId").change(function () {
        var countryId = $("#CountryId").val();
        getCounties(countryId);
    });
});

$('#createFieldPlacementSiteModal').on('show.bs.modal', function () {
    $("#CountryId").change(function () {
        var countryId = $("#CountryId").val();
        getCounties(countryId);
    });
});


$('#editProgramLocationModal').on('show.bs.modal', function () {
    $("#CountryId").change(function () {
        var countryId = $("#CountryId").val();
        getCounties(countryId);
    });
});

$('#updateApplicantContactInfoModal').on('show.bs.modal', function () {
    $("#AddressInfo_CountryId").change(function () {
        var countryId = $("#AddressInfo_CountryId").val();
        getCounties(countryId);
    });
});


$('#updateFacultyContactInfoModal').on('show.bs.modal', function () {
    $("#AddressInfo_CountryId").change(function () {
        var countryId = $("#AddressInfo_CountryId").val();
        getCounties(countryId);
    });
});


