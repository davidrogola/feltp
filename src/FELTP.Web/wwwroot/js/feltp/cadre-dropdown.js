﻿$(document).ready(function () {
    getCadres(null);
});

function getCadres(selectedIndex) {
    var url = "/ResidentManagement/GetCadres";
    $.getJSON(url, {}, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, comp) {
            if (comp.Value == selectedIndex)
                items += "<option value='" + comp.Value + "' selected=" + 'selected' + ">" + comp.Text + "</option>"
            else
                items += "<option value='" + comp.Value + "'>" + comp.Text + "</option>";

        });
        $("#Person_BioDataInfo_CadreId").html(items).selectpicker("refresh");
        $("#CadreId").html(items).selectpicker("refresh");
    });
}

$('#updateFacultyModal').on('show.bs.modal', function () {
    var cadreId = $("#PersonCadreId").val();
    getCadres(cadreId);
});

