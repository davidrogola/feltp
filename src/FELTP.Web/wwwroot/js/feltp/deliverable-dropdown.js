﻿$(document).ready(function () {
    var url = "/Deliverable/GetDeliverablesListDropdown";
    $.getJSON(url, {}, function (data) {
        var items = " ";
        $.each(data, function (i, deliverable) {
            items += "<option value='" + deliverable.Value + "'>" + deliverable.Text + "</option>"
        });
        $("#DeliverableId").html(items).selectpicker("refresh");

    });
});