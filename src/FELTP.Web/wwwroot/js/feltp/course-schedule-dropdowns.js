﻿$("#btnCreateCourseSchedule").on("click", function () {

    var url = $(this).data("url");
    var programtierurl = "/ProgramManagement/GetProgramTiers";
    var programurl = "/ProgramManagement/GetPrograms";
    $.get(url, function (data) {
        $('#createCourseScheduleContainer').html(data);
        $('#createCourseScheduleModal').modal('show');
        $('.selectpicker').selectpicker();

        $.getJSON(programtierurl, {}, function (data) {
            var items = "<option value='0'> Select </option>";
            $.each(data, function (i, tier) {
                items += "<option value='" + tier.Value + "'>" + tier.Text + "</option>"
            });
            $("#ProgramTierId").html(items).selectpicker("refresh");

        });
        $("#ProgramTierId").change(function () {
            var programTierId = $("#ProgramTierId").val();
            var url = "/ProgramManagement/GetPrograms";
            $('.selectpicker').selectpicker();
            $.getJSON(url, { programTierId: programTierId }, function (data) {
                var items = "<option value='0'> Select </option>";
                $.each(data, function (i, tier) {
                    items += "<option value='" + tier.Value + "'>" + tier.Text + "</option>"
                });
                $("#ProgramId").html(items).selectpicker("refresh");

            });
        });
        $("#ProgramId").change(function () {
            var url = "/ProgramManagement/GetProgramOffers";
            var programId = $("#ProgramId").val();
            $.getJSON(url, { programId: programId }, function (data) {
                var items = "<option value='0'> Select </option>";
                $.each(data, function (i, offer) {
                    items += "<option value='" + offer.Value + "'>" + offer.Text + "</option>"
                });
                $("#ProgramOfferId").html(items).selectpicker("refresh");

            });
        });
        $("#ProgramId").change(function () {
            var url = "/ProgramManagement/GetProgramCourses";
            var ProgramId = $("#ProgramId").val();
            $.getJSON(url, { ProgramId: ProgramId }, function (data) {
                var items = "<option value='0'> Select </option>";
                $.each(data, function (i, course) {
                    items += "<option value='" + course.Value + "'>" + course.Text + "</option>"
                });
                $("#ProgramCourseId").html(items).selectpicker("refresh");
            });
        });
        $(document).ready(function () {
            var url = "/FacultyManagement/GetFacultyDropDown";
            $.getJSON(url, {}, function (data) {
                var items = "<option value='0'> Select </option>";
                $.each(data, function (i, comp) {
                    items += "<option value='" + comp.Value + "'>" + comp.Text + "</option>"
                });
                $("#FacultyId").html(items).selectpicker("refresh");

            });
        });
    });
});


