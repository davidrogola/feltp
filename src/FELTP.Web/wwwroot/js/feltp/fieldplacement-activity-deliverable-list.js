﻿$(document).ready(function () {
    $('#fieldplacementactivity-deliverable-list-table').dataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/FieldPlacement/GetFieldPlacementActivityDeliverables",
            "type": "POST"
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'fieldplacementactivity',
                data: "fieldPlacementActivity",
                title: "Field Activity",
                sortable: true,
                searchable: true
            },
            {
                name: 'deliverable',
                data: "deliverable",
                title: "Deliverable",
                sortable: true,
                searchable: true
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "DateCreated",
                sortable: true,
                searchable: true
            },
            {
                name: 'createdby',
                data: 'createdBy',
                title: "CreatedBy",
                sortable: true,
                searchable: true
            }
        ]
    });
});