﻿function format_unit(d) {
    return '<div class="slider" style="margin-left:60px">' +
        '<b>Code:</b> ' + d.code + '<br>' +
        '<b>Name: </b>' + d.name + '<br>' +
        '<b>Description:</b> ' + d.description + '<br>' +
        '<b>Has Activity:</b> ' + d.hasActivity + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}
$(document).ready(function () {
    var table = $('#units-list-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/Unit/GetUnits",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "width": "5%",
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'code',
                data: "code",
                title: "Code",
                sortable: true,
                searchable: true
            },
            {
                name: 'name',
                data: "name",
                title: "Name",
                sortable: true,
                searchable: true
            },

            
            {
                name: 'description',
                data: "description",
                title: "Description",
                sortable: true,
                searchable: true,
                visible :false
            },
            {
                name: 'status',
                data: "status",
                title: "Status",
                sortable: true,
                searchable: true,
                visible :false
            },
            {
                name: 'hasActivity',
                data: "hasActivity",
                title: "hasActivity",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                "title": "Actions",
                "data": { "id": "id", "name": "name", "hasActivity": "hasActivity" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {

                    var link =  '<a class="edit-unit-details" href="/Unit/EditUnit?id=' + data.id + '"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i> Edit </button></a>' + ' | '
                        + '<a class="view-unit-details" href="/Unit/ViewDetails?id=' + data.id + '"><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i> View</button></a>';
                    if (data.hasActivity == true)
                        link +='|'+ '<a class="view-unit-activities" href="/Unit/ViewUnitActivities?id=' + data.id + '"><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i> Activities</button></a>';
                    return link;

                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });
    // Add event listener for opening and closing details
    $('#units-list-table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format_unit(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });


    $('#units-list-table').on("click", ".view-unit-details", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#units-details-container').html(data);
            $('#units-details-modal').modal('show');
        });

    });



    $('#units-list-table').on("click", ".view-unit-activities", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#units-details-container').html(data);
            $('#units-details-modal').modal('show');
        });

    });
});

$('#units-details-modal').on('show.bs.modal', function () {
    var unitId = $("#UnitId").val();
    $('#unit-activity-list-table').dataTable({
        "serverSide": true,
        "proccessing": true,
        "dom": 'rt<"bottom"i<"clear">>',
        "ajax": {
            "url": "/Unit/GetUnitActivities",
            "type": "POST",
            "data": { unitId: unitId }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'strActivityType',
                data: 'strActivityType',
                title: "Activity Type",
                sortable: false,
                searchable: false
            },
            {
                name: 'name',
                data: 'name',
                title: "Activity Name",
                sortable: false,
                searchable: false
            },
            {
                name: 'status',
                data: 'status',
                title: "Status",
                sortable: false,
                searchable: false
            }
        ]
    });
});

$("#units-details-modal").on("click", "#btn-update-unit", function (e) {
    var form = $('form#edit-unit-details-form');
    var action = $(form).attr('action');
    e.preventDefault();
    $.post(action, $(form).serialize(), function (data) {
        $("#units-details-container").html(data);
    }, 'html');

});

$('#units-list-table').on("click", ".edit-unit-details", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#units-details-container').html(data);
        $('#units-details-modal').modal('show');
        $("#FieldPlacementActivityId").selectpicker("refresh");
    });
});