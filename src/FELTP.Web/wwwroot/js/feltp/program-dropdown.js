﻿$('#enroll-resident-container').on("change", "#ProgramTierId", function () {
    var programTierId = $("#ProgramTierId").val();
    var url = "/ProgramManagement/GetPrograms";

    $.getJSON(url, { programTierId: programTierId }, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, tier) {
            items += "<option value='" + tier.Value + "'>" + tier.Text + "</option>"
        });
        $("#ProgramId").html(items).selectpicker("refresh");

    });
});

$("#ProgramId").change(function () {
    var programId = $("#ProgramId").val();
    var url = "/CourseManagement/GetProgramCoursesCount";
    $.getJSON(url, { programId: programId }, function (data) {
        $("#NumberOfDidacticUnitsRequired").val(data.DidacticCourseCount);
        $("#NumberOfActivitiesRequired").val(data.FieldPlacementCourseCount);
    });
});

$("#ProgramId").change(function () {
    var programId = $("#ProgramId").val();
    getThesisDefenceDeliverables(programId);
});

function getThesisDefenceDeliverables(programId) {
    var url = "/ProgramManagement/GetProgramDeliverablesWithThesisDefenceCount";
    $.getJSON(url, { programId: programId }, function (data) {
        $("#NumberOfThesisDefenceDeliverables").val(data.thesisDefenceDeliverablesCount);
    });
}


$(document).ready(function () {
    var url = "/ProgramManagement/GetProgramLocationDropDown";
    $.getJSON(url, {}, function (data) {
        var items = " ";
        $.each(data, function (i, tier) {
            items += "<option value='" + tier.Value + "'>" + tier.Text + "</option>"
        });
        $("#ProgramLocationId").html(items).selectpicker("refresh");

    });
});

$('#add-program-application-container').on("change", "#ProgramId", function () {
    var url = "/ProgramManagement/GetProgramOffers";
    var programId = $("#ProgramId").val();
    $.getJSON(url, { programId: programId }, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, offer) {
            items += "<option value='" + offer.Value + "'>" + offer.Text + "</option>"
        });
        $("#ProgramOfferId").html(items).selectpicker("refresh");

    });
});



