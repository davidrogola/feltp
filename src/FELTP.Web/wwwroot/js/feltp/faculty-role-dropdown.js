﻿$(document).ready(function () {
    var url = "/FacultyManagement/GetFacultyRoleDropDown";
    $.getJSON(url, {}, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, tier) {
            items += "<option value='" + tier.Value + "'>" + tier.Text + "</option>"
        });
        $("#Roles").html(items).selectpicker("refresh");

    });
});

