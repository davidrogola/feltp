﻿function format_event(d) {
    return '<div class="slider" style="margin-left:70px">' +
        '<b>Full name: </b>' + d.name + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}
$(document).ready(function () {
    var table = $('#event-list').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/Eventmanagement/GetEvents",
            "type": "POST"
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'theme',
                data: "theme",
                title: "Theme",
                sortable: true,
                searchable: true
            },
            {
                name: 'title',
                data: "title",
                title: "Title",
                sortable: true,
                searchable: true
            },
            {
                name: 'eventType',
                data: "eventType",
                title: "Event Type",
                sortable: true,
                searchable: true
            },
            {
                name: 'status',
                data: 'status',
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": { "id": "id" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/EventManagement/_EditEvent?id=' + data.id + '" class="editEvent"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i> Edit </button></a> | <a class="view-details" href="/EventManagement/_EventDetail?id=' + data.id +'"><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i> View Details</button></a>' ;

                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });
    // Add event listener for opening and closing details
    $('#event-list tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format_event(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });
    $('#event-list').on("click", ".editEvent", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        var programurl = "/ProgramManagement/GetProgramListDropdown";
        $.get(url, function (data) {
            $('#editEventContainer').html(data);
            $('#editEventModal').modal('show');
            $('.selectpicker').selectpicker();
        });
        $.getJSON(programurl, {}, function (data) {
            var items = "<option value='0'> Select </option>";
            $.each(data, function (i, tier) {
                items += "<option value='" + tier.Value + "'>" + tier.Text + "</option>"
            });
            $("#ProgramId").html(items).selectpicker("refresh");

        });

    });


    $("#editEventModal").on("click", "#btn-update-event", function (e) {
        var form = $('form#update-program-event-form');
        var action = $(form).attr('action');
        e.preventDefault();
        $.post(action, $(form).serialize(), function (data) {
            $("#editEventContainer").html(data);
            $('.selectpicker').selectpicker();
        }, 'html');

    });

    $('#event-list').on("click", ".detailEvent", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#detailEventContainer').html(data);
            $('#detailEventModal').modal('show');
            $('.selectpicker').selectpicker();
        });

    });

    $('#event-list').on("click", ".deactivateEvent", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#deactivateEventContainer').html(data);
            $('#deactivateEventModal').modal('show');
        });

    });
});

$("#btnCreateEvent").on("click", function () {
    var url = $(this).data("url");

    $.get(url, function (data) {
        $('#createEventContainer').html(data);
        $('#createEventModal').modal('show');
        $('.selectpicker').selectpicker();

    });

});

function format(d) {
    return '<div class="slider" style="margin-left:70px">' +
        '<b>Event: </b>' + d.programEvent + '<br>' +
        '<b>Satrt Date: </b>' + d.startDate + '<br>' +
        '<b>End Date: </b>' + d.endDate + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}
$(document).ready(function () {
    var table = $('#event-schedule').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/ProgramManagement/GetEventSchedule",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "data": null,
                "width": "5%",
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'programEvent',
                data: "programEvent",
                title: "Event",
                sortable: true,
                searchable: true
            },
            {
                name: 'location',
                data: "location",
                title: "Location",
                sortable: true,
                searchable: true
            },
            {
                name: 'startdate',
                data: "startDate",
                title: "Start Date",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'enddate',
                data: 'endDate',
                title: "End date",
                //sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'status',
                data: 'status',
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": "id",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/ProramManagement/_EditEventSchedule?id=' + data + '" class="editEventSchedule"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i></button></a> | <a href="/ProgramManagement/_DetailsEventSchedule?id=' + data + '" class="detailsEventSchedule"><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i></button></a> | <a href="/ProgramManagement/Delete?id=' + data + '" class="deleteEventSchedule"><button class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-ban-circle"></i></button></a>';
                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });
    // Add event listener for opening and closing details
    $('#event-schedule tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });
    $('#event-schedule').on("click", ".editEventSchedule", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#editEventScheduleContainer').html(data);
            $('#editEventScheduleModal').modal('show');
            $('.selectpicker').selectpicker();
        });

    });

    $('#event-schedule').on("click", ".detailsEventSchedule", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#detailsEventScheduleContainer').html(data);
            $('#detailsEventScheduleModal').modal('show');
        });

    });

    $('#event-schedule').on("click", ".deleteEventSchedule", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#deleteEventScheduleContainer').html(data);
            $('#deleteEventScheduleodal').modal('show');
        });

    });
});


$(document).ready(function () {
    var eventId = $("#EventId").val();
    var table = $('#program-event-list').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/EventManagement/GetProgramEvents",
            "type": "POST",
            "data": { eventId: eventId }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'programId',
                data: 'programId',
                title: "programId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'eventId',
                data: 'eventId',
                title: "eventId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'programName',
                data: "programName",
                title: "Program",
                sortable: true,
                searchable: true
            },
            {
                name: 'status',
                data: 'status',
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": { "id": "id", "programName": "programName", "programId": "programId", "eventId": "eventId"},
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return ' <a class="view-schedule" href="/EventManagement/_EventScheduleDetails?Id=' + data.id + '&programName=' + data.programName + '"><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i> View Schedule</button></a>'
                        + ' <a class="add-schedule" href="/EventManagement/_AddEventSchedule?Id=' + data.id + '&programId=' + data.programId + '&eventId=' + data.eventId+'"><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i> Add Schedule </button></a>';

                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });
    // Add event listener for opening and closing details
    $('#event-list tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format_event(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });


    $('#event-list').on("click", ".deactivateEvent", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#deactivateEventContainer').html(data);
            $('#deactivateEventModal').modal('show');
        });

    });
});

$('#program-event-list').on("click", ".view-schedule", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#program-schedule-container').html(data);
        $('#program-schedule-modal').modal('show');
    });
});

$("#program-event-list").on("click", ".add-schedule", function (event) {
    event.preventDefault();
    var url_string = $(this).attr("href");
    var programParameter = url_string.split('&');
    var parogramParamSplit = programParameter[1].split('=');
    var programId = parogramParamSplit[1];

    $.get(url_string, function (data) {
        $('#createEventScheduleContainer').html(data);
        $('#createEventScheduleModal').modal('show');     
        $('.selectpicker').selectpicker();
    });
});

function getProgramEvents(programId) {
    var url = "/EventManagement/GetProgramEvents";
    $.getJSON(url, { ProgramId: programId }, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, event) {
            items += "<option value='" + event.Value + "'>" + event.Text + "</option>"
        });
        $("#ProgramEventId").html(items).selectpicker("refresh");
    });
}
    



$('#program-schedule-modal').on('show.bs.modal', function () {

    var eventId = $("#ProgramEventId").val();

    var table = $('#event-schedule').DataTable({
        "serverSide": true,
        "proccessing": true,
        "dom": 'rt<"bottom"i<"clear">>',
        "ajax": {
            "url": "/EventManagement/GetEventSchedule",
            "type": "POST",
            "data": { programEventId: eventId}
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'location',
                data: "location",
                title: "Location",
                sortable: true,
                searchable: true
            },
            {
                name: 'startdate',
                data: "startDate",
                title: "Start Date",
                sortable: true,
                searchable: true,
                visible: true
            },
            {
                name: 'enddate',
                data: 'endDate',
                title: "End date",
                //sortable: true,
                searchable: true,
                visible: true
            },
            {
                name: 'status',
                data: 'status',
                title: "Status",
                sortable: true,
                searchable: true
            },
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });
});
