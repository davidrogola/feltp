﻿$(document).ready(function () {
    var residentId = $("#ResidentId").val();

    $('#resident-course-history-list-table').dataTable({
        "serverSide": true,
        "proccessing": true,
        "dom": 'rt<"bottom"i<"clear">>',
        "ajax": {
            "url": "/ResidentManagement/GetResidentCourseHistory",
            "type": "POST",
            "data": { residentId: residentId }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'Tier',
                data: 'tier',
                title: "Tier",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'program',
                data: 'program',
                title: "Program",
                sortable: true,
                searchable: true
            },
            {
                name: 'completionstatus',
                data: "completionStatus",
                title: "Completion Status",
                sortable: true,
                searchable: true
            },
            {
                name: 'EnrollmentDate',
                data: "enrollmentDate",
                title: "Enrollment Date",
                sortable: true,
                searchable: true
            },
            {
                name: 'ExpectedProgramEndDate',
                data: "expectedProgramEndDate",
                title: "Program EndDate",
                sortable: true,
                searchable: true
            }
        ]
    });
});

$(document).ready(function () {
    var personId = $("#PersonId").val();
    $('#employment-history-list-table').dataTable({
        "serverSide": true,
        "proccessing": true,
         "dom": 'rt<"bottom"i<"clear">>',
         "ajax": {
             "url": "/LookUp/GetPersonEmploymentHistory",
             "type": "POST",
             "data": { personId: personId }
         },
        columns: [
            {
                name: 'PersonnelNumber',
                data: 'personnelNumber',
                title: "Personnel Number",
                sortable: false,                          
                searchable: false,
                visible: true
            },
            {
                name: 'WorkStation',
                data: 'workStation',
                title: "Work Station",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'Designation',
                data: 'designation',
                title: "Designation",
                sortable: true,
                searchable: true
            },
            {
                name: 'DurationInPosition',
                data: "durationInPosition",
                title: "Duration In Position (Years)",
                sortable: true,
                searchable: true
            },
            {
                name: 'EmployerName',
                data: "employerName",
                title: "Employer Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'YearOfEmployment',
                data: "yearOfEmployment",
                title: "Year Of Employment",
                sortable: true,
                searchable: true
            }
        ]
    });
});




$(document).ready(function () {
    var personId = $("#PersonId").val();
    $('#academic-history-list-table').dataTable({
        "serverSide": true,
        "proccessing": true,
        "dom": 'rt<"bottom"i<"clear">>',
        "ajax": {
            "url": "/LookUp/GetPersonAcademicHistory",
            "type": "POST",
            "data": { personId: personId }
        },
        columns: [
            {
                name: 'strEducationLevel',
                data: 'strEducationLevel',
                title: "Education Level",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'University',
                data: 'university',
                title: "University Name",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'courseName',
                data: 'courseName',
                title: "Course",
                sortable: true,
                searchable: true
            },
            {
                name: 'yearOfGraduation',
                data: "yearOfGraduation",
                title: "Year of Graduation",
                sortable: true,
                searchable: true
            }
        ]
    });
});