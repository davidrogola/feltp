﻿$("#ProgramOfferId").change(function () {
    var offerId = $(this).val();
    getApplicantsDataTable(offerId, 2); // load residents by program offer
});

function getApplicantsDataTable(Id, getBy)
{
    var table = $('#applicants-list-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "destroy": true,
        "ajax": {
            "url": "/ResidentManagement/GetApplicants",
            "type": "POST",
            "data": { Id: Id, getBy: getBy }         
        },
        columns: [
            {
                name: 'applicantId',
                data: 'applicantId',
                title: "applicantId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'program',
                data: "program",
                title: "Program",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'programOfferId',
                data: "programOfferId",
                title: "programOfferId",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'registrationNumber',
                data: "registrationNumber",
                title: "Registration Number",
                sortable: true,
                searchable: true
            },
            {
                name: 'name',
                data: "name",
                title: "Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'idNumber',
                data: 'identificationNumber',
                title: "Id Number",
                sortable: true,
                searchable: true
            },
            {
                name: 'evaluatedBy',
                data: "evaluatedBy",
                title: "Evaluated By",
                sortable: true,
                searchable: true
            },
            {
                name: 'dateEvaluated',
                data: "dateEvaluated",
                title: "Date Registered",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                "title": "Actions",
                "data": { "applicantId": "applicantId", "programOfferId":"programOfferId" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a target="_blank"   href="/ResidentManagement/ApplicantDetails?applicantId=' + data.applicantId + '&offerId=' + data.programOfferId + ' "><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i> View Details</button></a>';
                }
            }
        ]
    });
}

$('.transition-resident').on("click",function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    var programtierurl = "/ProgramManagement/GetProgramTiers";
    $.get(url, function (data) {
        $('#editProgramContainer').html(data);
        $('#editProgramModal').modal('show');
    });

});


$(document).ready(function () {
    var idNumber = $("#IdentificationNumber").val();
    $('#applicants-search-list-table').dataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/ResidentManagement/SearchApplicantDetails",
            "type": "POST",
            "data": { identificationNo: idNumber }
        },
        columns: [
            {
                name: 'applicantId',
                data: 'applicantId',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'name',
                data: "name",
                title: "Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'idNumber',
                data: 'identificationNumber',
                title: "Id Number",
                sortable: true,
                searchable: true
            },
            {
                name: 'cadre',
                data: "cadre",
                title: "Cadre",
                sortable: true,
                searchable: true
            },
            {
                name: 'createdBy',
                data: "createdBy",
                title: "Created By",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": "applicantId",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a target ="_blank" href="/ResidentManagement/ApplicantDetails?applicantId=' + data + '">View Details </a>';
                }
            }
        ]
    });
});

