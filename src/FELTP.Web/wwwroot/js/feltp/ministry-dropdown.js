﻿$(document).ready(function () {
    var url = "/LookUp/GetMinistries";
    $.getJSON(url, {}, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, tier) {
            items += "<option value='" + tier.Value + "'>" + tier.Text + "</option>"
        });
        $("#Person_EmploymentDetail_MinistryId").html(items).selectpicker("refresh");

    });
});
