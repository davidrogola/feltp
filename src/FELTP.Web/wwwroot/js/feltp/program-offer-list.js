﻿function format_program_offer(d) {
    return '<div class="slider" style="margin-left:60px">' +
        '<b>Name: </b>' + d.name + '<br>' +
        '<b>Program Name:</b> ' + d.programName + '<br>' +
        '<b>Offer Name: </b>' + d.name + '<br>' +
        '<b>Program Start Date:</b> ' + d.strStartDate + '<br>' +
        '<b>Program End Date: </b>' + d.strEndDate + '<br>' +
        '<b>Application Start Date: </b>' + d.strApplicationStartDate + '<br>' +
        '<b>Application End Date: </b>' + d.strApplicationEndDate + '<br>'+
        '<b>Faculty:</b> ' + d.faculty + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>'        
}
$(document).ready(function () {
    var table =  $('#program-offer-list-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/programmanagement/GetProgramOfferList",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "width": "5%",
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'name',
                data: "name",
                title: "Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'programname',
                data: "programName",
                title: "Program",
                sortable: true,
                searchable: true,
                visible:false
            },
            {
                name: 'startdate',
                data: 'strStartDate',
                title: "Program Start Date",
                sortable: true,
                searchable: true
            },
            {
                name: 'enddate',
                data: 'strEndDate',
                title: "Program End Date",
                sortable: true,
                searchable: true
            },
            {
                name: 'faculty',
                data: "faculty",
                title: "Faculty",
                sortable: true,
                searchable: true
            },
            {
                name: 'datecreated',
                data: 'dateCreated',
                title: "Date Created",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'createdBy',
                data: 'createdBy',
                title: "Created By",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'strApplicationStartDate',
                data: 'strApplicationStartDate',
                title: "Application Start Date",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'strApplicationEndDate',
                data: 'strApplicationEndDate',
                title: "Application End Date",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                "title": "Actions",
                "data": { "id": "id", "name": "name" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a class="view-program-offer" href="/ProgramManagement/ProgramOfferDetails?id=' + data.id + '"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicons-new-window"></i> View Details </button></a>'
                      
                }
            }
        ]
    });
    $('#program-offer-list-table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format_program_offer(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });

    $('#collaborator-list').on("click", ".editCollaborator", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#editCollaboratorContainer').html(data);
            $('#editCollaboratorModal').modal('show');
            $('.selectpicker').selectpicker();
        });

    });
});


$('.edit-program-offer-link').click(function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#programoffer-locations-container').html(data);
        $('#programoffer-locations-modal').modal('show');
        $('.selectpicker').selectpicker();
    });
});

$('#program-offer-list-table').on("click", ".view-programoffer-location", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#programoffer-locations-container').html(data);
        $('#programoffer-locations-modal').modal('show');
    });
});


$(document).ready(function () {
    var offerId = $("#ProgramOfferId").val();
    $('#program-locationlist-table').dataTable({
        "serverSide": true,
        "proccessing": true,
        "dom": 'rt<"bottom"i<"clear">>',
        "ajax": {
            "url": "/programmanagement/GetProgramLocations",
            "type": "POST",
            "data": { programOfferId: offerId }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'location',
                data: "location",
                title: "Location",
                sortable: true,
                searchable: true
            },
            {
                name: 'country',
                data: "country",
                title: "Country",
                sortable: true,
                searchable: true
            },
            {
                name: 'county',
                data: "county",
                title: "County",
                sortable: true,
                searchable: true
            },
            {
                name: 'building',
                data: 'building',
                title: "Building",
                sortable: true,
                searchable: true
            },
            {
                name: 'road',
                data: 'road',
                title: "Road",
                sortable: true,
                searchable: true
            }
        ]
    });
});