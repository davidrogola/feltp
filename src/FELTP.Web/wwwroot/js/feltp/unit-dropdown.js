﻿$("#CategoryId").change(function () {

  var categoryName =  $("#CategoryId option:selected").text();
    var hasActivity = true;
    if (categoryName.toLowerCase().indexOf("didactic") >=0) // didactic course types
    {
        hasActivity = false;
    }
    var url = "/Unit/GetUnits";
    $.getJSON(url, { hasActivity: hasActivity }, function (data) {
        var items = " ";
        $.each(data, function (i, module) {
            items += "<option value='" + module.Value + "'>" + module.Text + "</option>"
        });
        $("#UnitId").html(items).selectpicker("refresh");

    });
});