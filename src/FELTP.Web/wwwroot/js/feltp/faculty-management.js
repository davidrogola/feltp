﻿function format(d) {
    return '<div class="slider" style="margin-left:70px">' +
        '<b>Full name: </b>' + d.name + '<br>' +
        '<b>Created By: </b>' + d.createdBy + '<br>' +
        '<b>Date Created: </b>' + d.dateCreated + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}
$(document).ready(function () {
    var table = $('#faculty-role-list').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/FacultyManagement/GetFacultyRoles",
            "type": "POST",

        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "data": null,
                "width": "5%",
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'name',
                data: "name",
                title: "Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "Date Created",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'createdby',
                data: 'createdBy',
                title: "Created By",
                //sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'status',
                data: 'status',
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": "id",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/FacultyManagement/_EditFacultyRole?id=' + data + '" class="editFacultyRole" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i> Edit</button></a> | <a href="/FacultyManagement/_DetailsFacultyRole?id=' + data + '" class="detailsFacultyRole" data-toggle="tooltip" title="Details"><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i> View</button></a> | <a href="/FacultyManagement/_DeactivateFacultyRole?id=' + data + '" class="deactivateFacultyRole" data-toggle="tooltip" title="Deactivate"><button class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-ban-circle"></i> Deactivate</button></a>';
                }
            }
        ],
        "language": {
            "emptyTable": "No Records found, Please click on <b>Create New</b> button."
        }
    });
    // Add event listener for opening and closing details
    $('#faculty-role-list tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });
    $('#faculty-role-list').on("click", ".editFacultyRole", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#editFacultyRoleContainer').html(data);
            $('#editFacultyRoleModal').modal('show');
            $('.selectpicker').selectpicker();
        });

    });

    $('#faculty-role-list').on("click", ".detailsFacultyRole", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#detailsFacultyRoleContainer').html(data);
            $('#detailsFacultyRoleModal').modal('show');
            $('.selectpicker').selectpicker();
        });

    });

    $('#faculty-role-list').on("click", ".deactivateFacultyRole", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#deactivateFacultyRoleContainer').html(data);
            $('#deactivateFacultyRoleModal').modal('show');
        });

    });
});


$("#btnCreateFacultyRole").on("click", function () {
    var url = $(this).data("url");
    $.get(url, function (data) {
        $('#createFacultyRoleContainer').html(data);
        $('#createFacultyRoleModal').modal('show');
    });

});

function format_faculty(d) {
    return '<div class="slider" style="margin-left:60px">' +
        '<b>Staff Number:</b> ' + d.staffNumber + '<br>' +
        '<b>Name: </b>' + d.person.bioDataInfo.name + '<br>' +
        '<b>ID Number:</b> ' + d.person.bioDataInfo.identificationNumber + '<br>' +
        '<b>Registered By: </b>' + d.createdBy + '<br>' +
        '<b>Date Registered: </b>' + d.dateCreated + '<br>' +
        '<b>Status: </b>' + d.status + '<br>'
}
$(document).ready(function () {
    var table = $('#faculty-list').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/FacultyManagement/GetFaculties",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "width": "5%",
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'staffNumber',
                data: "staffNumber",
                title: "Staff Number",
                sortable: true,
                searchable: true
            },
            {
                name: 'name',
                data: "person.bioDataInfo.name",
                title: "Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'idNumber',
                data: 'person.bioDataInfo.identificationNumber',
                title: "Id Number",
                sortable: true,
                searchable: true
            },
            {
                name: 'createdBy',
                data: "createdBy",
                title: "Registered By",
                sortable: true,
                searchable: true
            },
            {
                name: 'dateCreated',
                data: "dateCreated",
                title: "Date Registered",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": "id",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/FacultyManagement/FacultyDetails?id=' + data + '" target="_blank"  class="view-applicant-details" data-toggle="tooltip" title="View Faculty"><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i> View Details</button></a>';
                }
            }
        ]
    });

    $('#faculty-list tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format_faculty(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });

});

$("#btnUpdateFacultyBioData").on("click", function (event) {
    var url = $(this).data("url");
    event.preventDefault();
    $.get(url, function (data) {
        $('#updateFacultyBioDataContainer').html(data);
        $('#updateFacultyBioDataModal').modal('show');
        $('.selectpicker').selectpicker();
    });

});

$("#btnUpdateFacultyContactInfo").on("click", function (event) {
    var url = $(this).data("url");
    event.preventDefault();
    $.get(url, function (data) {
        $('#updateFacultyContactInfoContainer').html(data);
        $('#updateFacultyContactInfoModal').modal('show');
        $('.selectpicker').selectpicker();
    });

});


$(document).ready(function () {

    var facultyId = $("#FacultyId").val();
    var table = $('#examination-schedule-list-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "dom": 'rt<"bottom"i<"clear">>',
        "ajax": {
            "url": "/FacultyManagement/GetFacultyExaminationSchedule",
            "type": "POST",
            "data": { facultyId: facultyId }

        },
        columns: [
    {
        name: 'id',
        data: 'id',
        title: "Id",
        sortable: false,
        searchable: false,
        visible: false
    },
    {
        name: 'examinationName',
        data: 'examinationName',
        title: "Examination",
        sortable: false,
        searchable: false,
        visible: true
    },
    {
        name: 'startDate',
        data: 'startDate',
        title: "Date",
        sortable: false,
        searchable: true,
        visible: true
    },
    {
        name: 'startTime',
        data: 'startTime',
        title: "Start Time",
        sortable: false,
        searchable: false,
        visible: true
    },
    {
        name: 'endTime',
        data: 'endTime',
        title: "End Time",
        sortable: false,
        searchable: false,
        visible: true
    },
    {
        name: 'duration',
        data: 'duration',
        title: "Duration",
        sortable: false,
        searchable: false,
        visible: true
    },
     {
         name: 'createdBy',
         data: 'createdBy',
         title: "Created By",
         sortable: true,
         searchable: true,
         visible: false
     },
    {
        name: 'datecreated',
        data: "dateCreated",
        title: "DateCreated",
        sortable: true,
        searchable: false,
        visible: false
    }
        ],

        "language": {
            "emptyTable": "No Records found"
        }
    });

});

$(document).ready(function () {

    var facultyId = $("#FacultyId").val();
    var table = $('#course-schedule-list-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "dom": 'rt<"bottom"i<"clear">>',
        "ajax": {
            "url": "/FacultyManagement/GetFacultyCourseSchedule",
            "type": "POST",
            "data": { facultyId: facultyId }

        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'code',
                data: 'code',
                title: "Code",
                width : "5%",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'unit',
                data: 'unitName',
                title: "Unit",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'startDate',
                data: 'startDate',
                title: "Date",
                sortable: false,
                searchable: true,
                visible: true
            },
            {
                name: 'startTime',
                data: 'startTime',
                title: "Start Time",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'endTime',
                data: 'endTime',
                title: "End Time",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'duration',
                data: 'duration',
                title: "Duration",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'createdBy',
                data: 'createdBy',
                title: "Created By",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "DateCreated",
                sortable: true,
                searchable: false,
                visible: false
            }
        ],

        "language": {
            "emptyTable": "No Records found"
        }
    });

});

$("#btnDeactivateFaculty").on("click", function (event) {
    var url = $(this).data("url");
    event.preventDefault();

    $.get(url, function (data) {
        $('#updateFacultyContactInfoContainer').html(data);
        $('#updateFacultyContactInfoModal').modal('show');
    });
});

$('#updateFacultyContactInfoModal').on("click", "#deactivate-faculty-btn", function (event) {
    var form = $('form#deactivate-faculty-form');
    var action = $(form).attr('action');
    event.preventDefault();
    $.post(action, $(form).serialize(), function (data) {
        $("#updateFacultyContactInfoContainer").html(data);
    }, 'html');
});


$("#btnActivateFaculty").on("click", function (event) {
    var url = $(this).data("url");
    event.preventDefault();

    $.get(url, function (data) {
        $('#updateFacultyContactInfoContainer').html(data);
        $('#updateFacultyContactInfoModal').modal('show');
    });
});

$('#updateFacultyContactInfoModal').on("click", "#activate-faculty-btn", function (event) {
    var form = $('form#activate-faculty-form');
    var action = $(form).attr('action');
    event.preventDefault();
    $.post(action, $(form).serialize(), function (data) {
        $("#updateFacultyContactInfoContainer").html(data);
    }, 'html');
});