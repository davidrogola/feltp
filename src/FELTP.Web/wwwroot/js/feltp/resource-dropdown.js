﻿$("#ResourceTypeId").change(function () {
    var resourceTypeId = $("#ResourceTypeId").val();
    var url = "/ProgramManagement/GetResources";
    $.getJSON(url, { resourceTypeId: resourceTypeId }, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, tier) {
            items += "<option value='" + tier.Value + "'>" + tier.Text + "</option>"
        });
        $("#ResourceId").html(items).selectpicker("refresh");

    });
});

