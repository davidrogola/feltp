﻿$(document).ready(function () {
    var url = "/ProgramManagement/GetResourceTypes";
    $.getJSON(url, {}, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, tier) {
            items += "<option value='" + tier.Value + "'>" + tier.Text + "</option>"
        });
        $("#ResourceTypeId").html(items).selectpicker("refresh");

    });
});