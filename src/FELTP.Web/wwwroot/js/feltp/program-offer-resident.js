﻿$("#ProgramId").change(function () {
    var url = "/ProgramManagement/GetProgramOffers";
    var programId = $("#ProgramId").val();
    $.getJSON(url, { programId: programId }, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, offer) {
            items += "<option value='" + offer.Value + "'>" + offer.Text + "</option>"
        });
        $("#ProgramOfferId").html(items).selectpicker("refresh");

    });
});

$("#ProgramOfferId").change(function () {
    var url = "/SemesterManagement/GetSemesterScheduleDropDown";
    var programOfferId = $("#ProgramOfferId").val();
    $.getJSON(url, { programOfferId: programOfferId }, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, offer) {
            items += "<option value='" + offer.Value + "'>" + offer.Text + "</option>"
        });
        $("#SemesterScheduleId").html(items).selectpicker("refresh");

    });
});

$("#SemesterScheduleId").change(function () {
    var url = "/SemesterManagement/GetSemesterScheduleItemDropDown";
    var semesterScheduleId = $("#SemesterScheduleId").val();
    $.getJSON(url, { semesterScheduleId: semesterScheduleId }, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, offer) {
            items += "<option value='" + offer.Value + "'>" + offer.Text + "</option>"
        });
        $("#CourseScheduleId").html(items).selectpicker("refresh");

    });
});

$("#CourseScheduleId").change(function () {
    var url = "/SemesterManagement/GetUnitsInTimetableDropDown";
    var scheduleItemId = $("#CourseScheduleId").val();
    $.getJSON(url, { scheduleItemId: scheduleItemId }, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, offer) {
            items += "<option value='" + offer.Value + "'>" + offer.Text + "</option>"
        });
        $("#UnitId").html(items).selectpicker("refresh");

    });
});

$("#UnitId").change(function () {
    var url = "/SemesterManagement/GetUnitAttedanceDates";
    var unitId = $("#UnitId").val();
    var scheduleItemId = $("#CourseScheduleId").val();

    $.getJSON(url, { scheduleItemId: scheduleItemId, unitId: unitId }, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, offer) {
            items += "<option value='" + offer.Value + "'>" + offer.Text + "</option>"
        });
        $("#StrAttendanceDate").html(items).selectpicker("refresh");

    });
});

$("#StrAttendanceDate").change(function () {
    var url = "/SemesterManagement/GetUnitTimeSlotsForDate";
    var unitId = $("#UnitId").val();
    var scheduleItemId = $("#CourseScheduleId").val();
    var attendanceDate = $("#StrAttendanceDate").val();


    $.getJSON(url, { scheduleItemId: scheduleItemId, unitId: unitId, attendanceDate: attendanceDate }, function (data) {
        var items = "<option value='0'> Select </option>";
        $.each(data, function (i, offer) {
            items += "<option value='" + offer.Value + "'>" + offer.Text + "</option>"
        });
        $("#StrFacultyTimetableItem").html(items).selectpicker("refresh");

    });
});


var table;

$("#ProgramOfferId").change(function () {
    var url = "/ResidentManagement/GetResidentsByOffer";

    var offerId = $("#ProgramOfferId").val();
    table = $('#enrolled-residents-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "destroy": true,
        "ajax": {
            "url": "/ResidentManagement/GetResidentsByOffer",
            "type": "POST",
            "data": { programOfferId: offerId }
        },
        columns: [
            {
                name: 'residentId',
                data: 'residentId',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'registrationNumber',
                data: "registrationNumber",
                title: "FELTP Number",
                sortable: true,
                searchable: true
            },
            {
                name: 'residentName',
                data: "residentName",
                title: "Resident Name",
                sortable: true,
                searchable: true,
            },
            {
                "title": "Attendance",
                "data": "residentId",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<label class="radio-inline">' + '<input type="radio" checked value="Present"' + 'name=' + "optradio-" + data +' >Present</label>' +
                        '<label class="radio-inline">' + '<input type="radio" value="Absent" ' + 'name=' + "optradio-" + data + '>Absent</label>' +
                        '<label class="radio-inline">' + '<input type="radio" value="Leave" ' + 'name=' + "optradio-" + data + ' >Leave</label>';
                       
                }
            }
        ]
    });
    $("#residents-div").show();
});



$('#resident-attendance-frm').on('submit', function (e) {

    var radios = $('#enrolled-residents-table').find("input:radio:checked").prop('checked', false);
    var dict = {};
    radios.each(function (index) {
        var status = $(this).attr('value');
        var residentId = $(this).attr('name').split('-')[1];
        dict[residentId] = status;

    });
    $("#Status").val(JSON.stringify(dict));
    console.log(JSON.stringify(dict))
});



$(document).ready(function () {
    var scheduleItemId = $("#SemesterScheduleItemId").val();
    var unitId = $("#UnitId").val();

$('#attendance-marking-results-table').dataTable({
    "serverSide": true,
    "proccessing": true,
    "ajax": {
        "url": "/ResidentManagement/GetResidentCourseAttendance",
        "type": "POST",
        "data": { Id: scheduleItemId, getBy: 2, unitId: unitId } // value 2 represents getting the value by semesterscheduleId

    },
    columns: [
        {
            name: 'id',
            data: 'residentCourseWorkId',
            title: "Id",
            sortable: false,
            searchable: false,
            visible: false
        },
        {
            name: 'registrationNumber',
            data: 'registrationNumber',
            title: "Reg Number",
            sortable: false,
            searchable: false,
            visible: false
        },
        {
            name: 'residentName',
            data: 'residentName',
            title: "Resident Name",
            sortable: false,
            searchable: false,
            visible: true
        },
        {
            name: 'code',
            data: 'code',
            title: "Code",
            sortable: false,
            searchable: false,
            visible: false
        },
        {
            name: 'unit',
            data: 'unit',
            title: "Unit",
            sortable: false,
            searchable: false,
            visible: false
        },
        {
            name: 'status',
            data: 'status',
            title: "Attendance Status",
            sortable: true,
            searchable: true
        },
        {
            name: 'attendanceDate',
            data: "attendanceDate",
            title: "Attendance Date",
            sortable: true,
            searchable: true
        },
        {
            name: 'startTime',
            data: "startTime",
            title: "Start Time",
            sortable: true,
            searchable: true
        },
        {
            name: 'endTime',
            data: "endTime",
            title: "End Time",
            sortable: true,
            searchable: true
        },
        {
            name: 'trainer',
            data: "trainer",
            title: "Trainer",
            sortable: true,
            searchable: true
        }
    ]
});
});
