﻿function format_program_location(d) {
    return '<div class="slider" style="margin-left:60px">' +
        '<b>Venue:</b> ' + d.location + '<br>' +
        '<b>Country:</b> ' + d.country + '<br>' +
        '<b>County: </b>' + d.county + '<br>' +
        '<b>Subcounty:</b> ' + d.subCounty + '<br>' +
        '<b>Building:</b> ' + d.building + '<br>' +
        '<b>Road:</b> ' + d.road + '<br>'
}
$(document).ready(function () {
    var table = $('#program-locationlist-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/programmanagement/GetProgramLocations",
            "type": "POST"
        },
        columns: [
            {
                "class": "details-control",
                "orderable": false,
                "width": "5%",
                "data": null,
                "defaultContent": ""
            },
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,                                             
                visible: false
            },
            {
                name: 'location',
                data: "location",
                title: "Venue",
                sortable: true,
                searchable: true
            },
            {
                name: 'country',
                data: "country",
                title: "Country",
                sortable: true,
                searchable: true
            },
            {
                name: 'county',
                data: "county",
                title: "County",
                sortable: true,
                searchable: true
            },
            {
                name: 'building',
                data: 'building',
                title: "Building",
                sortable: true,
                searchable: true
            },
            {
                name: 'road',
                data: 'road',
                title: "Road",
                sortable: true,
                searchable: true
            },
            {
                    "title": "Actions",
            "data": { "id": "id", "name": "name" },
            "searchable": false,
            "sortable": false,
            "render": function (data, type, full, meta) {
                return '<a class="edit-location-details" href="/ProgramManagement/_EditProgramLocation?id=' + data.id + '"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i> Edit </button></a>' + ' | '
                    + '<a class="deactivate-location" href="/ProgramManagement/_DeactivateProgramLocation?id=' + data.id + '"><button class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-ban-circle"></i> Deactivate</button></a>';

            }
        }
    ],
    "language": {
        "emptyTable": "No Records found, Please click on <b>Create New</b> button."
    }


});
    // Add event listener for opening and closing details
    $('#program-locationlist-table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp(function () {
                row.child.hide();
                tr.removeClass('shown');
            });
        }
        else {
            // Open this row
            row.child(format_program_location(row.data()), 'no-padding').show();
            tr.addClass('shown');

            $('div.slider', row.child()).slideDown();
        }
    });

    $('#program-locationlist-table').on("click", ".edit-location-details", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#editProgramLocationContainer').html(data);
            $('#editProgramLocationModal').modal('show');
        });

    });

    $('#program-locationlist-table').on("click", ".deactivate-location", function (event) {
        event.preventDefault();
        var url = $(this).attr("href");
        $.get(url, function (data) {
            $('#deactivateProgramLocationContainer').html(data);
            $('#deactivateProgramLocationModal').modal('show');
        });

    });
});

$("#btnCreateProgramLocation").on("click", function () {
    var url = $(this).data("url");
    $.get(url, function (data) {
        $('#createProgramLocationContainer').html(data);
        $('#createProgramLocationModal').modal('show');
        $('.selectpicker').selectpicker();
    });

});

$('#createProgramLocationModal').on('show.bs.modal', function () {
    $('.selectpicker').selectpicker();
});

$('#editProgramLocationModal').on('show.bs.modal', function () {
    $('.selectpicker').selectpicker();
});