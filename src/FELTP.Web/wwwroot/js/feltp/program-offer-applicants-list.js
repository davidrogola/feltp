﻿$(document).ready(function () {
    var offerId = $("#ProgramOfferId").val();

    $('#program-applicants-list-table').dataTable({
  
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/ProgramManagement/GetProgramOfferApplicants",
            "type": "POST",
            "data": { programOfferId: offerId }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'programOfferId',
                data: 'programOfferId',
                title: "programOfferId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'applicantId',
                data: 'applicantId',
                title: "applicantId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'name',
                data: "name",
                title: "Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'idNumber',
                data: 'identificationNumber',
                title: "Id Number",
                sortable: true,
                searchable: true
            },
            {
                name: 'cadre',
                data: "cadre",
                title: "Cadre",
                sortable: true,
                searchable: true
            },
            {
                name: 'status',
                data: "strApplicationStatus",
                title: "Status",
                sortable: true,
                searchable: true
            },
            {
                name: 'dateApplied',
                data: "dateApplied",
                title: "Date Applied",
                sortable: true,
                searchable: true
            },
            {
                "title": "Actions",
                "data": { "id": "id", "applicantId": "applicantId", "programOfferId":"programOfferId" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a target="_blank"   href="/ResidentManagement/ApplicantDetails?applicantId=' + data.applicantId + '&offerId=' + data.programOfferId + ' ">View Details </a>';
                }
            }
        ]
    });
});

$(document).ready(function () {
    var applicantId = $("#Id").val();

    $('#application-history-list-table').dataTable({
        "serverSide": true,
        "proccessing": true,
        "dom": 'rt<"bottom"i<"clear">>',
        "ajax": {
            "url": "/ResidentManagement/GetApplicantProgramOffers",
            "type": "POST",
            "data": { applicantId: applicantId }
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'programOfferId',
                data: 'programOfferId',
                title: "programOfferId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'applicantId',
                data: 'applicantId',
                title: "applicantId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'programofferName',
                data: "programOfferName",
                title: "Offer Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'program',
                data: 'program',
                title: "Program",
                sortable: true,
                searchable: true
            },
            {
                name: 'status',
                data: "qualificationStatus",
                title: "Qualification Status",
                sortable: true,
                searchable: true
            },
            {
                name: 'strApplicationStatus',
                data: "strApplicationStatus",
                title: "Application Status",
                sortable: true,
                searchable: true
            },
            {
                name: 'applicationStatus',
                data: "applicationStatus",
                title: "applicationStatus",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                "title": "Actions",
                "data": { "id": "id", "applicantId": "applicantId", "applicationStatus": "applicationStatus", "qualificationStatus": "qualificationStatus" },
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    var action = '<a class="view-program-evaluation" href="/ResidentManagement/ApplicantEvaluation?id=' + data.id + '&applicantId=' + data.applicantId + '&readMode=' + true + '"><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i> View Details</button></a>';
                    if (data.applicationStatus == 1 && data.qualificationStatus =="Pending_Evaluation")
                        action = '<a class="view-program-evaluation" href="/ResidentManagement/ApplicantEvaluation?id=' + data.id + '&applicantId=' + data.applicantId + '"><button class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-list-alt"></i> Evaluate</button> </a>';
                    return action;
                    //application status 1 = Shortlisted
                }
            }
        ]
    });
});

$(document).ready(function () {
    var offerId = $("#ProgramOfferId").val();
    var status = $("#ApplicationStatus").val();

  var datatable =  $('#shortlist-applicants-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/ProgramManagement/GetProgramOfferApplicants",
            "type": "POST",
            "data": { programOfferId: offerId, status: status }
        },
      'columnDefs': [
          {
              'targets': 0,
              'checkboxes': {
                  'selectRow': true,
              }
          }
      ],
      'select': {
          'style': 'multi'
      },
      'order': [[1, 'asc']],
        columns: [       
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: true
            },
            {
                name: 'programOfferId',
                data: 'programOfferId',
                title: "programOfferId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'applicantId',
                data: 'applicantId',
                title: "applicantId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'name',
                data: "name",
                title: "Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'idNumber',
                data: 'identificationNumber',
                title: "Id Number",
                sortable: true,
                searchable: true
            },
            {
                name: 'cadre',
                data: "cadre",
                title: "Cadre",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'status',
                data: "strApplicationStatus",
                title: "Application Status",
                sortable: true,
                searchable: true
            },
            {
                name: 'dateApplied',
                data: "dateApplied",
                title: "Date Applied",
                sortable: true,
                searchable: true
            }
        ]
    });


    $('#short-list-applicants-frm').on('submit', function (e) {
        var rows_selected = datatable.column(0).checkboxes.selected();
        var offerapplications = [];

        $.each(rows_selected, function (index, rowId) {
            // Create a hidden element 
            offerapplications.push(rowId);
        });

        console.log(offerapplications);
        $("#ProgramOfferApplicationIdsJson").val(JSON.stringify(offerapplications));          
    });

});



$('#application-history-list-table').on("click", ".view-program-evaluation", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#view-applicant-offer-container').html(data);
        $('#view-applicant-offer-modal').modal('show');
        $("#EvaluationStatus").selectpicker("refresh");
    });

});

$('.enroll-resident').on("click", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#enroll-resident-container').html(data);
        $('#enroll-resident-modal').modal('show');
        
    });

});

$("#enroll-resident-modal").on("click", "#enroll-applicant-btn", function (e) {
    e.preventDefault();
    var form = $('form#enroll-resident-form');
    var action = $(form).attr('action');
    e.preventDefault();
    $.post(action, $(form).serialize(), function (data) {
        $("#enroll-resident-container").html(data);
    }, 'html');
});


$('.add-new-applicant-application').on("click", function (event) {
    event.preventDefault();
    var url = $(this).attr("href");
    $.get(url, function (data) {
        $('#add-program-application-container').html(data);
        $('#add-program-application-modal').modal('show');
        $(".selectpicker").selectpicker("refresh");
    });

});

$("#add-program-application-modal").on("click","#add-program-offer-btn",function (e) {
    e.preventDefault();

    var form = $('form#add-program-application-form');
    var action = $(form).attr('action');
    var method = $(form).attr('method');

    var data = new FormData();

        var fileupload = $("#StatementFile").get(0);
        var files = fileupload.files;
        for (var i = 0; i < files.length; i++) {
            data.append('StatementFile', files[i]);
        }

    // You can update the jquery selector to use a css class if you want
    $("input[type='text'").each(function (x, y) {
        data.append($(y).attr("name"), $(y).val());
    });

    $("select").each(function (x, y) {
        data.append($(y).attr("name"), $(y).val());
    });

    $("input[type='hidden'").each(function (x, y) {
        data.append($(y).attr("name"), $(y).val());
    });
    $("input[type='checkbox'").each(function (x, y) {
        data.append($(y).attr("name"), $(y).val());
    });

    $.ajax({
        type: method,
        url: action,
        contentType: false,
        processData: false,
        data: data
    }).done(function (res) {
        $("#add-program-application-container").html(res);
        $('.selectpicker').selectpicker();
    }).fail(function (xhr, b, error) {
        alert(error);
    });

});



