﻿
$("#ProgramOfferId").change(function () {
    var offerId = $(this).val();
    getResidentsDataTable(offerId, 2); // load residents by program offer
});


function getResidentsDataTable(Id,getBy) {
   var residents = $('#residents-list-table').DataTable({
        "serverSide": true,
        "proccessing": true,
        "destroy": true,
        "ajax": {
            "url": "/ResidentManagement/GetResidents",
            "type": "POST",
            "data": { Id: Id, getBy: getBy}
        },
        columns: [
            {
                name: 'applicantId',
                data: 'applicantId',
                title: "applicantId",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'program',
                data: "program",
                title: "Program",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'programOfferId',
                data: "programOfferId",
                title: "programOfferId",
                sortable: true,
                searchable: true,
                visible: false
            },
            {
                name: 'registrationNumber',
                data: "registrationNumber",
                title: "Registration Number",
                sortable: true,
                searchable: true
            },
            {
                name: 'name',
                data: "name",
                title: "Name",
                sortable: true,
                searchable: true
            },
            {
                name: 'idNumber',
                data: 'identificationNumber',
                title: "Id Number",
                sortable: true,
                searchable: true
            },
            {
                name: 'evaluatedBy',
                data: "evaluatedBy",
                title: "Registered By",
                sortable: true,
                searchable: true
            },
            {
                name: 'dateEvaluated',
                data: "dateEvaluated",
                title: "Date Registered",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                "title": "Actions",
                "data": { "applicantId": "applicantId", "programOfferId":"programOfferId"},
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a target="_blank"   href="/ResidentManagement/ResidentDetails?applicantId=' + data.applicantId + '&offerId=' + data.programOfferId + ' "><button class="btn btn-info btn-xs"><i class="glyphicon glyphicon-list-alt"></i> View Details</button></a>';
                }
            }
        ]
    });
}