﻿$(document).ready(function () {
    $('#program-competency-list-table').dataTable({
        "serverSide": true,
        "proccessing": true,
        "ajax": {
            "url": "/ProgramManagement/GetProgramCompetencies",
            "type": "POST"
        },
        columns: [
            {
                name: 'id',
                data: 'id',
                title: "Id",
                sortable: false,
                searchable: false,
                visible: false
            },
            {
                name: 'program',
                data: "program",
                title: "Program",
                sortable: true,
                searchable: true
            },
            {
                name: 'competency',
                data: 'competency',
                title: "Competency",
                sortable: true,
                searchable: true
            },
            {
                name: 'deliverables',
                data: 'deliverables',
                title: "Deliverables",
                sortable: false,
                searchable: false
            },           
            {
                name: 'datecreated',
                data: "dateCreated",
                title: "DateCreated",
                sortable: true,
                searchable: false,
                visible: false
            },
            {
                name: 'createdby',
                data: 'createdBy',
                title: "CreatedBy",
                //sortable: true,
                searchable: false,
                visible: false
            }
        ]
    });
});