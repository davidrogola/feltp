ALTER VIEW residentexaminationview
AS
Select  s.Id AS SemesterId,s.Name as Semester,es.ExamType,  e.Id AS ExaminationId,
 e.CourseId,c.Code,e.Name,pe.ResidentId,es.Score,rs.ProgramEnrollmentId,es.DateCreated
 ,es.CreatedBy,rs.Id AS ResidentCourseWorkId
 from examinationscore es 
 inner join examination e on e.Id = es.ExaminationId
 inner join course c on c.Id = e.CourseId
 inner join residentcoursework rs on rs.Id = es.ResidentCourseWorkId
 inner join programcourse pc on pc.Id = rs.ProgramCourseId
 inner join semester s on s.Id = pc.SemesterId
 inner join programenrollment pe ON pe.Id = rs.ProgramEnrollmentId
 inner join programoffer po on po.Id = pe.ProgramOfferId
 group by es.Id

 
 
