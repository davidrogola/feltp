ALTER  VIEW `residentdeliverableview`
AS SELECT
   `d`.`Name` AS `DeliverableName`,
   `rcw`.`ProgramEnrollmentId` AS `ProgramEnrollmentId`,
   `rcw`.`ResidentId` AS `ResidentId`,
   `rd`.`Id` AS `ResidentDeliverableId`,
   `rd`.`ResidentFieldPlacementActivityId` AS `ResidentFieldPlacementActivityId`,
   `d`.`Id` AS `DeliverableId`,
   `rd`.`DateCreated` AS `DateCreated`,
   `rd`.`ProgressStatus` AS `ProgressStatus`,
   `rd`.`CreatedBy` AS `CreatedBy`,
   `d`.`Code` AS `Code`,
   `s`.`Id` AS `SemesterId`,
   `s`.`Name` AS `Semester`,
   `d`.`HasThesisDefence` AS `HasThesisDefence`,
   `rd`.`ThesisDefenceStatus` AS `ThesisDefenceStatus`,
   `pc`.`CourseId` AS `CourseId`
   FROM ((((((`residentdeliverable` `rd` join `residentfieldplacementactivity` `rf` 
   on(`rd`.`ResidentFieldPlacementActivityId` = `rf`.`Id`)) join `residentunit` `ru`
   on(`ru`.`Id` = `rf`.`ResidentUnitId`)) join `deliverable` `d` on(`d`.`Id` = `rd`.`DeliverableId`)) 
   join `residentcoursework` `rcw` on(`ru`.`ResidentCourseWorkId` = `rcw`.`Id`)) join `programcourse`
   `pc` on(`pc`.`Id` = `rcw`.`ProgramCourseId`)) join `semester` `s` on(`s`.`Id` = `pc`.`SemesterId`));