ALTER table graduateeligibilityitem add ResidentCourseWorkId bigint null;
alter table graduateeligibilityitem add constraint fk_graudateeligibilityitem_course_work_id foreign key
(ResidentCourseWorkId)  references residentcoursework(id)