ALTER VIEW GraduationEligibityItemsView
AS
SELECT gi.*,c.Name AS CourseName,c.Code,s.Id AS SemesterId, s.Name AS Semester FROM graduateeligibilityitem gi 
LEFT JOIN residentcoursework rsc ON rsc.Id = gi.ResidentCourseWorkId
LEFT JOIN programcourse pc ON pc.Id  = rsc.ProgramCourseId
LEFT JOIN semester s ON s.Id = pc.SemesterId
LEFT JOIN course c ON c.Id = pc.CourseId;


