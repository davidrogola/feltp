﻿ALTER VIEW programofferapplicants AS
SELECT po.Id,po.ApplicantId,po.ProgramOfferId,po.DateApplied,po.DateCreated, 
p.FirstName ,p.LastName,p.IdentificationNumber,c.Name as Cadre,pof.Name as ProgramOfferName, pog.Name as Program
,po.ApprovedByCounty,po.PersonalStatementSubmitted,po.EnrollmentStatus,r.RegistrationNumber,po.Comment,
po.EvaluatedBy,po.DateEvaluated,po.QualificationStatus,po.ApplicationStatus,po.DateShortListed,po.ShortListedBy
FROM programofferapplication po 
INNER JOIN applicant ap on ap.Id= po.ApplicantId
INNER JOIN person p on ap.PersonId = p.Id
LEFT JOIN  cadre c on c.Id = p.CadreId
LEFT JOIN resident r ON ap.Id = r.ApplicantId
INNER JOIN programoffer pof ON pof.Id = po.ProgramOfferId
INNER JOIN program pog ON pog.Id = pof.ProgramId

