CREATE VIEW residentcatscoreview
AS
Select un.Id as UnitId,un.Code,un.Name,ru.Id as ResidentUnitId,tm.TimeTableType, es.`ExamType`,rsw.`ResidentId`,
rsw.Id as ResidentCourseWorkId,es.`Score`,s.Id as SemesterId, s.`Name` as Semester,ex.Id as ExaminationId,un.`HasFieldPlacementActivity`,rsw.`ProgramEnrollmentId`,tm.Id as TimeTableId,es.Id as ExamScoreId 
 from `programenrollment` pr inner join residentcoursework rsw on pr.Id = rsw.`ProgramEnrollmentId`
inner join programcourse pc on pc.Id = rsw.`ProgramCourseId`
inner join semester s on s.Id = pc.`SemesterId`
inner join residentunit ru on rsw.Id = ru.`ResidentCourseWorkId`
inner join unit un on ru.`UnitId` = un.Id
inner join examination ex on ex.UnitId = un.Id
inner join timetable tm on tm.`ExaminationId` = ex.Id
left join `examinationparticipation` ep on ep.`TimetableId` = tm.Id
left join examinationscore es on es.`ExaminationParticipationId` = ep.Id