ALTER VIEW `residentexaminationview`
AS SELECT
   `s`.`Id` AS `SemesterId`,
   `s`.`Name` AS `Semester`,
   `e`.`Id` AS `ExaminationId`,
   `es`.`ExamType`,
   `ru`.`UnitId` AS `UnitId`,
   `ru`.`Code` AS `Code`,
   `ru`.`Name` AS `Name`,
   `ru`.`ResidentId` AS `ResidentId`,
   `es`.`Score` AS `Score`,
   `ru`.`ProgramEnrollmentId` AS `ProgramEnrollmentId`,
   `ru`.`ResidentUnitId` AS `ResidentUnitId`,
   `es`.`DateCreated` AS `DateCreated`,
   `es`.`CreatedBy` AS `CreatedBy`,
   `ru`.`ResidentCourseWorkId` AS `ResidentCourseWorkId`,
   `ru`.`HasFieldPlacementActivity` AS `HasFieldPlacementActivity`
FROM (((((((`programoffer` `po` join `programenrollment` `pe` on(`pe`.`ProgramOfferId` = `po`.`Id`)) join `residentunitview` `ru` on(`ru`.`ProgramEnrollmentId` = `pe`.`Id`)) join `examination` `e` on(`e`.`UnitId` = `ru`.`UnitId`)) left join `examinationscore` `es` on(`es`.`ResidentUnitId` = `ru`.`ResidentUnitId`)) join `residentcoursework` `rc` on(`rc`.`Id` = `ru`.`ResidentCourseWorkId`)) join `programcourse` `pc` on(`pc`.`Id` = `rc`.`ProgramCourseId`)) join `semester` `s` on(`s`.`Id` = `pc`.`SemesterId`)) group by `ru`.`ResidentUnitId`;