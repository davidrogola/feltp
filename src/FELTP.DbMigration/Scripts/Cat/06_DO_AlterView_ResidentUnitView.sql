
ALTER VIEW residentunitview 
AS
SELECT ru.Id as ResidentUnitId, ru.UnitId,u.Code,rsc.ResidentId,ru.DateCreated,ru.CreatedBy,rsc.ProgramEnrollmentId
,ru.ResidentCourseWorkId,u.Name,po.Id as ProgramOfferId,u.HasFieldPlacementActivity, pc.SemesterId, ex.Id as ExaminationId
FROM residentunit ru 
INNER JOIN unit u ON ru.UnitId = u.Id
INNER JOIN examination ex ON ex.UnitId = u.Id
INNER JOIN residentcoursework rsc ON rsc.Id = ru.ResidentCourseWorkId
INNER JOIN programcourse pc ON pc.Id = rsc.ProgramCourseId
INNER JOIN programenrollment pe ON pe.Id = rsc.ProgramEnrollmentId
INNER JOIN programoffer po ON po.Id = pe.ProgramOfferId
