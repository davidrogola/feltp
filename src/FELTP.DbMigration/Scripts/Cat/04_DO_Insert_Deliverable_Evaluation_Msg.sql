INSERT INTO messagetemplate(Id,MessageTypeId,Subject,Body)
VALUES(11, 11,
'Deliverable Evaluation Feedback', 
'Dear @Model.ResidentName, <br />
 <p></p>
 <p></p>
 This is to notify you that faculty @Model.Faculty has reviewed your deliverable @Model.Deliverable for the field activity @Model.FieldActivity on date<br/> @Model.DateEvaluated  as follows: <b> @Model.Message </b>. 
 <p></p>
 <p></p>
 Best Regards,
 <p></p>
 <b>KFELTP Team </b>

')