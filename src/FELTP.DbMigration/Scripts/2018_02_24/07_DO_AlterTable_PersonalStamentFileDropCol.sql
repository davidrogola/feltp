﻿ALTER TABLE personalstatementfile DROP FOREIGN KEY FK_PersonalStatementFile_Applicant_Id;

ALTER TABLE personalstatementfile DROP COLUMN ApplicantId;