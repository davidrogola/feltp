﻿CREATE TABLE `interviewscore` (
  `Id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `OralInterview` INT(11) NOT NULL,
  `WrittenInterview` INT(11) NOT NULL,
  `Total` INT(11) NOT NULL,
  `ProgramOfferApplicationId` BIGINT(20) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_InterviewScore_ProgramOfferApplicant_idx` (`ProgramOfferApplicationId`),
  CONSTRAINT `FK_InterviewScore_ProgramOfferApplicationId` FOREIGN KEY (`ProgramOfferApplicationId`) REFERENCES `programofferapplication` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
