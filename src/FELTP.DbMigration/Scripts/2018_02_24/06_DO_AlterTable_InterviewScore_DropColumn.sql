﻿ALTER TABLE interviewscore DROP FOREIGN KEY FK_InterviewScore_ApplicantId;

ALTER TABLE interviewscore DROP COLUMN ApplicantId;