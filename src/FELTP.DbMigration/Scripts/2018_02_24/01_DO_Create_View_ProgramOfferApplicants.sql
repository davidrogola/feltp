﻿CREATE VIEW programofferapplicants AS
SELECT po.Id,po.ApplicantId,po.ProgramOfferId,po.Status,po.DateApplied,po.DateCreated, 
p.FirstName ,p.LastName,p.IdentificationNumber,c.Name as Cadre
FROM programofferapplication po 
INNER JOIN applicant ap on ap.Id= po.ApplicantId
INNER JOIN person p on ap.PersonId = p.Id
LEFT JOIN  cadre c on c.Id = p.CadreId

