﻿CREATE VIEW feltpusermanagement.RoleOperationsView AS
SELECT rc.ClaimValue as Operation, o.Comment as Description, rc.RoleId, rs.Name as RoleName
FROM feltpusermanagement.aspnetroleclaims rc 
INNER JOIN feltpusermanagement.aspnetroles rs on rs.Id = rc.RoleId
LEFT JOIN feltpusermanagement.operation o ON rc.ClaimValue = o.Name
