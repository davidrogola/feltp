﻿ALTER TABLE `courseunit` CHANGE COLUMN `ModuleId` `UnitId` int(11) NOT NULL;

ALTER TABLE `unitfieldplacementactivity` CHANGE COLUMN `ModuleId` `UnitId` int(11) NOT NULL;

ALTER TABLE `residentunit` CHANGE COLUMN `ModuleId` `UnitId` int(11) NOT NULL;

ALTER TABLE `residentunitattendance` CHANGE COLUMN `ResidentModuleId` `ResidentUnitId` bigint(20) NOT NULL;