﻿ALTER TABLE module RENAME unit;

ALTER TABLE coursemodule RENAME courseunit;

ALTER TABLE modulefieldplacementactivity RENAME unitfieldplacementactivity;

ALTER TABLE residentmodule RENAME residentunit;

ALTER TABLE residentmoduleattendance RENAME residentunitattendance

