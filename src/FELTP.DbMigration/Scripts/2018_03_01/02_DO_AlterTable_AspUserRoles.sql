﻿ALTER TABLE feltpusermanagement.aspnetuserroles ADD `DateAddedToRole` DATETIME NOT NULL;
ALTER TABLE feltpusermanagement.aspnetuserroles  ADD `UserAddedToRoleById` INT(11) DEFAULT  NULL;
ALTER TABLE feltpusermanagement.aspnetuserroles  ADD `DateRemovedFromRole` DATETIME DEFAULT NULL;
ALTER TABLE feltpusermanagement.aspnetuserroles  ADD `UserRemovedFromRoleById` INT(11) DEFAULT NULL;


ALTER TABLE feltpusermanagement.aspnetuserroles  ADD CONSTRAINT `FK_AspnetUserRoles_AspNetUsers` 
FOREIGN KEY (`UserAddedToRoleById`) REFERENCES `aspnetusers` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE feltpusermanagement.aspnetuserroles  ADD CONSTRAINT `FK_AspnetUserRoles_AspNetUsers_RM` 
FOREIGN KEY (`UserRemovedFromRoleById`) REFERENCES `aspnetusers` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION





