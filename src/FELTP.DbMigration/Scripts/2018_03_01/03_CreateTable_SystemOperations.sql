﻿CREATE TABLE `operation` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(100) NOT NULL,
  `Comment` VARCHAR(200) NOT NULL,
  `CreatedById` INT(11) NOT NULL,
  `DateCreated` DATETIME NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Operation_AspNetUsers_idx` (`CreatedById`),
  CONSTRAINT `FK_Operation_AspNetUsers_CreatedById` FOREIGN KEY (`CreatedById`) REFERENCES `aspnetusers` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

