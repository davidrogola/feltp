﻿SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
DELIMITER //
DROP PROCEDURE IF EXISTS `update_hilo_highvalue`;
CREATE PROCEDURE `update_hilo_highvalue` (IN sequencename VARCHAR(20))
BEGIN
	UPDATE hiloconfiguration SET  Hi = Hi + 1
	WHERE Name = sequencename; 
	SELECT Hi from hiloconfiguration where Name=sequencename;
END //


-- DELIMITER ;;
-- CREATE DEFINER=`root`@`localhost` PROCEDURE `update_hilo_highvalue` (IN sequencename VARCHAR(20))
-- BEGIN
-- START TRANSACTION; 
--    BEGIN
-- 	UPDATE hiloconfiguration SET  Hi = Hi + 1
-- 	WHERE Name = sequencename; 
-- 	SELECT Hi from hiloconfiguration where Name=sequencename;
--    END;
-- COMMIT ;
-- END;;
-- DELIMITER ;

