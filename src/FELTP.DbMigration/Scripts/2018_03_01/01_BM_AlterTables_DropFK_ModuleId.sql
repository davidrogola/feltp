﻿ALTER TABLE coursemodule DROP FOREIGN KEY FK_CourseModule_ModuleId;

ALTER TABLE coursemodule DROP FOREIGN KEY FK_CourseModule_CourseId;

ALTER TABLE modulefieldplacementactivity DROP FOREIGN KEY FK_ModuleFieldPlacementActivity_ModuleId;

ALTER TABLE modulefieldplacementactivity DROP FOREIGN KEY FK_ModuleFieldPlacementActivity_FieldPlacementActivityId;

ALTER TABLE residentmodule DROP FOREIGN KEY FK_ResidentModule_Module_Id;

ALTER TABLE residentmodule DROP FOREIGN KEY FK_ResidentModule_ResidentCourseWork_Id;

ALTER TABLE residentmoduleattendance DROP FOREIGN KEY FK_ResidentModuleAttendance_ResidentModuleId;

ALTER TABLE residentmoduleattendance DROP FOREIGN KEY FK_ResidentModuleAttendance_ResidentCourseAttendanceId;




