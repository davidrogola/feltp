﻿CREATE TABLE `resourcefile` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ResourceId` int(11) NOT NULL,
  `FileName` varchar(100) NOT NULL,
  `ContentType` varchar(100) NOT NULL,
  `FileContent` LONGBLOB NOT NULL,
  `DateCreated` datetime NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,

  PRIMARY KEY (`Id`),
  KEY `FK_TrainingResource_ResourceId_idx` (`ResourceId`),
  CONSTRAINT `FK_Training_ResourceId` FOREIGN KEY (`ResourceId`) REFERENCES `resource` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
