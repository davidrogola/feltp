﻿ALTER TABLE residentunitattendance
ADD AttendanceDate DATETIME NOT NULL DEFAULT NOW(),
ADD DateCreated DATETIME NOT NULL DEFAULT NOW(),
ADD AttendanceStatus INT(11) NOT NULL,
ADD TimetableId INT(11)  NOT NULL,
ADD FacultyId INT(11)  NOT NULL;
