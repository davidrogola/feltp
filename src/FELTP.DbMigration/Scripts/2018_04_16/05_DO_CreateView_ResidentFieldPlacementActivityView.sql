﻿CREATE VIEW ResidentFieldPlacementActivityView AS
SELECT rfp.Id, fa.Name AS ActivityName, sm.Id AS SemesterId,sm.Name AS Semester, 
rfp.FieldPlacementActivityId,rfp.ResidentUnitId,rfp.DateCreated,rfp.CreatedBy,rfp.ProgressStatus,rsc.ResidentId,rsc.ProgramEnrollmentId,fa.ActivityType
FROM residentfieldplacementactivity rfp
INNER JOIN fieldplacementactivity fa ON fa.Id = rfp.FieldPlacementActivityId
INNER JOIN residentunit ru ON ru.Id = rfp.ResidentUnitId
INNER JOIN residentcoursework rsc ON rsc.Id = ru.ResidentCourseWorkId
INNER JOIN programcourse pc ON pc.Id = rsc.ProgramCourseId
INNER JOIN semester sm ON sm.Id = pc.SemesterId;
