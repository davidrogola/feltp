﻿ALTER TABLE programoffer ADD ApplicationStartDate DATETIME NOT NULL DEFAULT NOW();

ALTER TABLE programoffer ADD ApplicationEndDate DATETIME NOT NULL DEFAULT NOW();