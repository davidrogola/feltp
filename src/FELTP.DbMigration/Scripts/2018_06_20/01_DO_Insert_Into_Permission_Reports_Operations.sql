﻿INSERT INTO feltpusermanagement.permission(Id,Name,Comment,DateCreated,CreatedById)
VALUES 
(0,'View Residents Report','Permission to view residents list report per cohort',NOW(),NULL),
(0,'View Residents Perfomance Report','Permission to view perfomance report',NOW(),NULL),
(0,'View Graduation Status Report','Permission to view a residents graduation status',NOW(),NULL),
(0,'View Graduation Details Report','Permission to view resident graduation details report',NOW(),NULL),
(0,'View Resident Deliverable Submission Report','Permission to view resident deliverable submissions',NOW(),NULL),
(0,'View Resident Thesis Defence Report','Permission to view resident thesis defence report',NOW(),NULL),
(0,'View Programs Report','Permission to deactivate a faculty',NOW(),NULL),
(0,'View Program Courses Report','Permission to view program courses',NOW(),NULL),
(0,'View Trainers Schedule Report','Permission to view trainers schedule',NOW(),NULL);
