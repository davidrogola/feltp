﻿CREATE VIEW feltpusermanagement.RolePermissionsView AS
SELECT rp.RoleId,ar.Name AS RoleName,p.Comment AS Description,p.Name AS Permission
FROM feltpusermanagement.rolepermissionmapping rp 
INNER JOIN feltpusermanagement.permission p ON p.Id = rp.PermissionId
INNER JOIN feltpusermanagement.aspnetroles ar ON ar.Id = rp.RoleId