﻿CREATE TABLE feltpusermanagement.`rolepermissionmapping` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `RoleId` INT(11) NOT NULL,
  `PermissionId` INT(11) NOT NULL,
  `DateCreated` DATETIME NOT NULL,
  `CreatedBy` VARCHAR(100) NOT NULL,
   PRIMARY KEY (`Id`),
  KEY FK_RolePermissionMapping_RoleId_idx (RoleId),
  KEY FK_RolePermissionMapping_PermissionId_idx (PermissionId),
  CONSTRAINT `FK_RolePermissionMapping_RoleId_idx` FOREIGN KEY (`RoleId`) REFERENCES `aspnetroles` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_RolePermissionMapping_PermissionId_idx` FOREIGN KEY (`PermissionId`) REFERENCES `permission` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;


