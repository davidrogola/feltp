﻿CREATE VIEW feltpusermanagement.RolePermissionsView AS
SELECT rc.ClaimValue as Permission, o.Comment as Description, rc.RoleId, rs.Name as RoleName
FROM feltpusermanagement.aspnetroleclaims rc 
INNER JOIN feltpusermanagement.aspnetroles rs on rs.Id = rc.RoleId
LEFT JOIN feltpusermanagement.permission o ON rc.ClaimValue = o.Name
