﻿CREATE TABLE `programlocation` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CountryId` int(11) NOT NULL,
  `CountyId` int(11) DEFAULT NULL,
  `SubCountyId` int(11) DEFAULT NULL,
  `PostalAddress` VARCHAR(100) NOT NULL,
  `Location` VARCHAR(100) NOT NULL,
  `Building` VARCHAR(100)  NULL,
  `Road` VARCHAR(100)  NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ProgramLocation_Country_idx` (`CountryId`),
  KEY `FK_ProgramLocation_SubCounty_idx` (`SubCountyId`),
  KEY `FK_ProgramLocation_County_idx` (`CountyId`),
  CONSTRAINT `FK_ProgramLocation_CountryId` FOREIGN KEY (`CountryId`) REFERENCES `country` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
   CONSTRAINT `FK_ProgramLocation_SubCountyId` FOREIGN KEY (`SubCountyId`) REFERENCES `subcounty` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ProgramLocation_CountyId` FOREIGN KEY (`CountyId`) REFERENCES `county` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
