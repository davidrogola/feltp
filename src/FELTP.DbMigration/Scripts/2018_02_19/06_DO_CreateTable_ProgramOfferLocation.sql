﻿CREATE TABLE `programofferlocation` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProgramOfferId` INT NOT NULL,
  `ProgramLocationId` INT NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ProgramOfferLocation_ProgramLocation_idx` (`ProgramLocationId`),
  KEY `FK_ProgramOfferLocation_ProgramOffer_idx` (`ProgramOfferId`),
  CONSTRAINT `FK_ProgramOfferLocation_ProgramLocationId` FOREIGN KEY (`ProgramLocationId`) REFERENCES `programlocation` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ProgramOfferLocation_ProgramOfferId` FOREIGN KEY (`ProgramOfferId`) REFERENCES `programoffer` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
