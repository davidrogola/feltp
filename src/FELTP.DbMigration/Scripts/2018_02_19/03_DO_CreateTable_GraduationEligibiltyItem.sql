﻿CREATE TABLE `graduateeligibilityitem` (
  `Id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `GraduateId` BIGINT(20) NOT NULL,
  `ExpectedValue` INT(11) NOT NULL,
  `ActualValue` INT(11) NOT NULL,
  `OutcomeStatus` INT(11) NOT NULL,
  `EligibiltyType` INT(11) NOT NULL,
  `FailureReason` INT(11) NOT NULL,
  `DateCreated` DATETIME NOT NULL,
  `CreatedBy` VARCHAR(100) NOT NULL,
  `DateDeactivated` DATETIME DEFAULT NULL,
  `DeactivatedBy` VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_GraduateEligiblityItem_Graduate_idx` (`GraduateId`),
  CONSTRAINT `FK_GraduateEligiblityItem_GraduateId` FOREIGN KEY (`GraduateId`) REFERENCES `graduate` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
