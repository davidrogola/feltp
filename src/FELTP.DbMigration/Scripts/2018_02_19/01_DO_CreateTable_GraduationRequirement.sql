﻿CREATE TABLE `graduationrequirement` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `ProgramId` INT(11) NOT NULL,
  `NumberOfDidacticModulesRequired` INT(11) NOT NULL,
  `NumberOfActivitiesRequired` INT(11) NOT NULL,
  `PassingGrade` INT(11) DEFAULT NULL,
  `DateCreated` DATETIME NOT NULL,
  `CreatedBy` VARCHAR(100) NOT NULL,
  `DateCompleted` DATETIME DEFAULT NULL,
  `DateDeactivated` DATETIME DEFAULT NULL,
  `DeactivatedBy` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_GraduationRequirement_ProgramId_idx` (`ProgramId`),
  CONSTRAINT `FK_GraduationRequirement_ProgramId` FOREIGN KEY (`ProgramId`) REFERENCES `program` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

