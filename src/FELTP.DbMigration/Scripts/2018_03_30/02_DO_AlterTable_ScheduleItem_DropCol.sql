﻿ALTER TABLE semesterscheduleitem DROP FOREIGN KEY FK_SemesterScheduleItem_FacultyId;

ALTER TABLE semesterscheduleitem DROP COLUMN FacultyId;