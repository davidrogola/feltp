﻿ALTER TABLE graduateeligibilityitem ADD COLUMN ResidentUnitId bigint(20) NULL;
ALTER TABLE graduateeligibilityitem ADD CONSTRAINT `FK_GraduateEligiblityItem_ResidentUinitId` FOREIGN KEY (`ResidentUnitId`) REFERENCES `residentunit` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
 