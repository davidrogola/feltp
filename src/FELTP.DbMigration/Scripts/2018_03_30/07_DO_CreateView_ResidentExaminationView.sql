﻿CREATE VIEW ResidentExaminationView 
AS
 SELECT s.Id AS SemesterId,s.Name as Semester,  e.Id AS ExaminationId,
 ru.UnitId,ru.Code,ru.Name,ru.ResidentId,es.Score,ru.ProgramEnrollmentId,ru.ResidentUnitId,ru.DateCreated
 ,ru.CreatedBy,ru.ResidentCourseWorkId,ru.HasFieldPlacementActivity
 FROM programoffer po
 INNER JOIN programenrollment pe ON pe.ProgramOfferId = po.Id
 INNER JOIN residentunitview ru ON ru.ProgramEnrollmentId = pe.Id 
 INNER JOIN examination e ON e.UnitId = ru.UnitId
 LEFT JOIN examinationscore es ON es.ResidentUnitId = ru.ResidentUnitId
 INNER JOIN residentcoursework rc on rc.Id = ru.ResidentCourseWorkId
 INNER JOIN programcourse pc on pc.Id = rc.ProgramCourseId
 INNER JOIN semester s on s.Id = pc.SemesterId

