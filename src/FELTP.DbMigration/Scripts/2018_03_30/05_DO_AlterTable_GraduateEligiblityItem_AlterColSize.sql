﻿ALTER TABLE graduateeligibilityitem CHANGE  `ActualValue` `ActualValue` DECIMAL(13,4) NOT NULL;
ALTER TABLE graduateeligibilityitem CHANGE  `ExpectedValue` `ExpectedValue` DECIMAL(13,4) NOT NULL;