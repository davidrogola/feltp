﻿UPDATE feltpusermanagement.emailtemplate 
SET Body = 
'Dear @Model.Name, <br />
<p></p>
Your password has been reset successfully.<br />
Please click on the link below to Login to <b>KFELTP</b>.<br />
<p></p>
Link : @Model.Link	
<p><p/>
Your user name to login to the system is : <br />
UserName : <b>@Model.Name </b><br/>
<p></p>
Best Regards,
<p></p>
<b>KFELTP</b> Team.
'
WHERE Id=1;