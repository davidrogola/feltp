﻿CREATE TABLE `addressinformation` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CountryId` int(11) NOT NULL,
  `CountyId` int(11) DEFAULT NULL,
  `SubCountyId` int(11) DEFAULT NULL,
  `PostalAddress` varchar(256) NOT NULL,
  `PersonId` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_AddressInformation_CountryId_idx` (`CountryId`),
  KEY `FK_AddressInformation_CountyId_idx` (`CountyId`),
  KEY `FK_AddressInformation_SubCountyId_idx` (`SubCountyId`),
  CONSTRAINT `FK_AddressInformation_PersonId` FOREIGN KEY (`PersonId`) REFERENCES `person` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_AddressInformation_CountryId` FOREIGN KEY (`CountryId`) REFERENCES `country` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_AddressInformation_CountyId` FOREIGN KEY (`CountyId`) REFERENCES `county` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_AddressInformation_SubCountyId` FOREIGN KEY (`SubCountyId`) REFERENCES `subcounty` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
