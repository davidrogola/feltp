﻿CREATE TABLE `person` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(100) NOT NULL,
  `LastName` varchar(100) NOT NULL,
  `OtherName` varchar(100) NOT NULL,
  `Gender` int (11) NOT NULL,
  `DateOfBirth` datetime NOT NULL,
  `CadreId` int(11) NULL,
  `HighestEducationLevel` varchar (100) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Person_CadreId_idx` (`CadreId`),
  CONSTRAINT `FK_Person_CadreId` FOREIGN KEY (`CadreId`) REFERENCES `cadre` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
