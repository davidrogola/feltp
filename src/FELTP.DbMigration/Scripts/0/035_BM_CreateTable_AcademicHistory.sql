﻿CREATE TABLE `academichistory` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PersonId` bigint(20) NOT NULL,
  `CourseName` varchar(256) NOT NULL,
  `UniversityName` varchar(256) NOT NULL,
  `DateGraduated` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_AcademicHistory_PersonId_idx` (`PersonId`),
  CONSTRAINT `FK_AcademicHistory_PersonId` FOREIGN KEY (`PersonId`) REFERENCES `person` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
