﻿CREATE TABLE `faculty` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PersonId` bigint(20) NOT NULL,
  `StaffNumber` varchar(100) NOT NULL,
  `DateCreated` datetime NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `DateDeactivated` datetime DEFAULT NULL,
  `DeactivatedBy` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Faculty_PersonId_idx` (`PersonId`),
  CONSTRAINT `FK_Faculty_PersonId` FOREIGN KEY (`PersonId`) REFERENCES `person` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
