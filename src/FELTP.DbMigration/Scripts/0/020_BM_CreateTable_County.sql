﻿CREATE TABLE `county` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `CountryId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_County_CountryId_idx` (`CountryId`),
  CONSTRAINT `FK_County_CountryId` FOREIGN KEY (`CountryId`) REFERENCES `country` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
