﻿CREATE TABLE `coursemodule` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CourseId` int(11) NOT NULL,
  `ModuleId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_CourseModule_CourseId_idx` (`CourseId`),
  KEY `FK_CourseModule_ModuleId_idx` (`ModuleId`),
  CONSTRAINT `FK_CourseModule_CourseId` FOREIGN KEY (`CourseId`) REFERENCES `course` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_CourseModule_ModuleId` FOREIGN KEY (`ModuleId`) REFERENCES `module` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
