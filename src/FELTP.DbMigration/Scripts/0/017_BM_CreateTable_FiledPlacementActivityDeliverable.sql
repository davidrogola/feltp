﻿CREATE TABLE `fieldplacementactivitydeliverable` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `DeliverableId` int(11) NOT NULL,
  `FieldPlacementActivityId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_FieldPlacementActivityDeliverable_DeliverableId_idx` (`DeliverableId`),
  KEY `FK_FieldPlacementActivityDeliverable_FieldPlacementActivity_idx` (`FieldPlacementActivityId`),
  CONSTRAINT `FK_FieldplacementActivityDeliverable_DeliverableId` FOREIGN KEY (`DeliverableId`) REFERENCES `deliverable` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fieldplacementactivitydeliverable_FiledPlacememntActivityId` FOREIGN KEY (`FieldPlacementActivityId`) REFERENCES `fieldplacementactivity` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
