﻿ALTER TABLE modulefieldplacementactivity
add `Description` varchar(250) NOT NULL,
add `DateCreated` datetime NOT NULL,
add `CreatedBy` varchar(100) NOT NULL,
add `DateDeactivated` datetime DEFAULT NULL,
add `DeactivatedBy` varchar(100) DEFAULT NULL,
add `CourseModuleId` int(11) NOT NULL,
add KEY `FK_ModuleFieldActivity_CourseModuleId_idx` (`CourseModuleId`),
add CONSTRAINT `FK_ModuleFieldPlacementActivity_CourseModuleId` FOREIGN KEY (`CourseModuleId`) REFERENCES `coursemodule` (`Id`) 
ON DELETE NO ACTION ON UPDATE NO ACTION