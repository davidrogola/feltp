﻿CREATE TABLE `modulefieldplacementactivity` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ModuleId` int(11) NOT NULL,
  `FieldPlacementActivityId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ModuleFieldplacementActivity_ModuleId_idx` (`ModuleId`),
  KEY `FK_ModuleFieldPlacementActivity_idx` (`FieldPlacementActivityId`),
  CONSTRAINT `FK_ModuleFieldPlacementActivity_FieldPlacementActivityId` FOREIGN KEY (`FieldPlacementActivityId`) REFERENCES `fieldplacementactivity` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ModuleFieldPlacementActivity_ModuleId` FOREIGN KEY (`ModuleId`) REFERENCES `module` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
