﻿CREATE TABLE `facultyrolemap` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `FacultyId` int(11) NOT NULL,
  `FacultyRoleId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_FacultyRoleMAp_FacultyId_idx` (`FacultyId`),
  KEY `FK_FacultyRoleMap_FacultyRoleId_idx` (`FacultyRoleId`),
  CONSTRAINT `FK_FacultyRoleMap_FacultyId` FOREIGN KEY (`FacultyId`) REFERENCES `faculty` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FacultyRoleMap_FacultyRoleId` FOREIGN KEY (`FacultyRoleId`) REFERENCES `facultyrole` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
