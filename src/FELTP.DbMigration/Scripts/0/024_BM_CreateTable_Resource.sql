﻿CREATE TABLE `resource` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ResourceTypeId` int(11) NOT NULL,
  `Name` varchar(250) NOT NULL,
  `Title` varchar(250) NOT NULL,
  `Source` varchar(100) NOT NULL,
  `SourceContactDetails` varchar(100) NOT NULL,
  `DateCreated` datetime NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `DateDeactivated` datetime DEFAULT NULL,
  `DeactivatedBy` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Resource_ResourcetypeId_idx` (`ResourceTypeId`),
  CONSTRAINT `FK_Resource_ResourcetypeId` FOREIGN KEY (`ResourceTypeId`) REFERENCES `resourcetype` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
