﻿CREATE TABLE `program` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProgramTierId` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Code` varchar(45) NOT NULL,
  `Description` varchar(250) NOT NULL,
  `DurationInMonths` int(11) NOT NULL,
  `HasMonthlySeminors` tinyint(4) NOT NULL,
  `DateCreated` datetime NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `DateDeactivated` datetime DEFAULT NULL,
  `DeactivatedBy` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Program_ProgramTierId_idx` (`ProgramTierId`),
  CONSTRAINT `FK_Program_ProgramTierId` FOREIGN KEY (`ProgramTierId`) REFERENCES `programtier` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
