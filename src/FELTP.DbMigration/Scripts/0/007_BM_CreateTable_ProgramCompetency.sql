﻿CREATE TABLE `programcompetency` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CompetencyId` int(11) NOT NULL,
  `ProgramId` int(11) NOT NULL,
  `DateCreated` datetime NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `DateDeactivated` datetime DEFAULT NULL,
  `DeactivatedBy` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ProgramCompetency_CompetencyId_idx` (`CompetencyId`),
  KEY `FK_ProgramCompetency_ProgramId_idx` (`ProgramId`),
  CONSTRAINT `FK_ProgramCompetency_CompetencyId` FOREIGN KEY (`CompetencyId`) REFERENCES `competency` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ProgramCompetency_ProgramId` FOREIGN KEY (`ProgramId`) REFERENCES `program` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
