﻿CREATE TABLE `contactinformation` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PersonId` bigint(20) NOT NULL,
  `EmailAddress` varchar(256) NOT NULL,
  `PrimaryMobileNumber` varchar(100) NOT NULL,
  `AlternateMobileNumber` varchar(100) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ContactInformation_PersonId_idx` (`PersonId`),
  CONSTRAINT `FK_ContactInformation_PersonId` FOREIGN KEY (`PersonId`) REFERENCES `person` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
