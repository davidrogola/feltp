﻿CREATE TABLE `subcounty` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `CountyId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Subcounty_CountyId_idx` (`CountyId`),
  CONSTRAINT `FK_Subcounty_CountyId` FOREIGN KEY (`CountyId`) REFERENCES `county` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
