﻿CREATE TABLE `programcourse` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProgramId` int(11) NOT NULL,
  `CourseId` int(11) NOT NULL,
  `SemesterId` int(11) NOT NULL,
  `DurationInWeeks` int(11) NOT NULL,
  `DateCreated` datetime NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `DateDeactivated` datetime DEFAULT NULL,
  `DeactivatedBy` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ProgramCourse_ProgramId_idx` (`ProgramId`),
  KEY `FK_ProgramCompetency_CourseId_idx` (`CourseId`),
  KEY `FK_ProgramCompetency_SemesterId_idx` (`SemesterId`),
  CONSTRAINT `FK_ProgramCourse_Course` FOREIGN KEY (`CourseId`) REFERENCES `course` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ProgramCourse_ProgramId` FOREIGN KEY (`ProgramId`) REFERENCES `program` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ProgramCourse_SemesterId` FOREIGN KEY (`SemesterId`) REFERENCES `semester` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
