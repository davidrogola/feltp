﻿CREATE TABLE `competencydeliverable` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CompetencyId` int(11) NOT NULL,
  `DeliverableId` int(11) NOT NULL,
  `DateCreated` datetime NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `DateDeactivated` datetime DEFAULT NULL,
  `DeactivatedBy` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_CompetencyDeliverable_CompetencyId_idx` (`CompetencyId`),
  KEY `FK_CompetencyDeliverable_DeliverableId_idx` (`DeliverableId`),
  CONSTRAINT `FK_CompetencyDeliverable_CompetencyId` FOREIGN KEY (`CompetencyId`) REFERENCES `competency` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_CompetencyDeliverable_DeliverableId` FOREIGN KEY (`DeliverableId`) REFERENCES `deliverable` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
