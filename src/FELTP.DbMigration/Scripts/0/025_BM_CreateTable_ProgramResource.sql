﻿CREATE TABLE `programresource` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProgramId` int(11) NOT NULL,
  `ResourceId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ProgramResource_programId_idx` (`ProgramId`),
  KEY `FK_ProgramResource_ResourceId_idx` (`ResourceId`),
  CONSTRAINT `FK_ProgramResource_ResourceId` FOREIGN KEY (`ResourceId`) REFERENCES `resource` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ProgramResource_programId` FOREIGN KEY (`ProgramId`) REFERENCES `program` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;