﻿ALTER TABLE programresource
add `DateCreated` datetime NOT NULL,
  add`CreatedBy` varchar(100) NOT NULL,
 add `DateDeactivated` datetime DEFAULT NULL,
 add `DeactivatedBy` varchar(100) DEFAULT NULL;