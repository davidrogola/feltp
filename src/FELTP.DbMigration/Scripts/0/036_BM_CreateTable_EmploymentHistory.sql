﻿CREATE TABLE `employmenthistory` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PersonId` bigint(20) NOT NULL,
  `PersonnelNumber` varchar(45) NOT NULL,
  `Designation` varchar(100) NOT NULL,
  `EmployerName` varchar(256) NOT NULL,
  `DurationInPosition` int(11) NOT NULL,
  `YearOfEmployment` datetime NOT NULL,
  `WorkStation` varchar(100) NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `FK_EmploymentHistory_PersonId` FOREIGN KEY (`PersonId`) REFERENCES `person` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
