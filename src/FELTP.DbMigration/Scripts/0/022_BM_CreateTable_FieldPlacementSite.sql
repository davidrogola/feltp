﻿CREATE TABLE `fieldplacementsite` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `CountryId` int(11) NOT NULL,
  `CountyId` int(11) DEFAULT NULL,
  `SubcountyId` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `DateDeactivated` datetime DEFAULT NULL,
  `DeactivatedBy` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_FieldPlacementSite_CountryId_idx` (`CountryId`),
  KEY `FK_FieldPlacementSite_CountyId_idx` (`CountyId`),
  KEY `FK_FieldPlacementSite_SubcountyId_idx` (`SubcountyId`),
  CONSTRAINT `FK_FieldPlacementSite_CountryId` FOREIGN KEY (`CountryId`) REFERENCES `country` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FieldPlacementSite_CountyId` FOREIGN KEY (`CountyId`) REFERENCES `county` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FieldPlacementSite_SubcountyId` FOREIGN KEY (`SubcountyId`) REFERENCES `subcounty` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
