﻿ALTER TABLE coursemodule
add `Description` varchar(250) NOT NULL,
add `DateCreated` datetime NOT NULL,
add `CreatedBy` varchar(100) NOT NULL,
add `DateDeactivated` datetime DEFAULT NULL,
add `DeactivatedBy` varchar(100) DEFAULT NULL