CREATE TABLE `fieldplacementsiteevaluation` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ResidentId` BIGINT NOT NULL,
  `FieldPlacementSiteId` INT NOT NULL,
  `Feedback` JSON NOT NULL,
  `DateEvaluated` datetime NOT NULL,
  `Rating` INT NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `FK_site_evaluation_fieldplacementid` FOREIGN KEY (`FieldPlacementSiteId`) REFERENCES `fieldplacementsite` (`Id`),
  CONSTRAINT `FK_site_evaluation_residentid` FOREIGN KEY (`ResidentId`) REFERENCES `resident` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
