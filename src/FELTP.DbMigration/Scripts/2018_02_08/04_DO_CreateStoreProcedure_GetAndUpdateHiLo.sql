﻿SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
DELIMITER //
CREATE PROCEDURE `update_hilo_highvalue` (IN sequencename VARCHAR(20))
BEGIN
	DECLARE hivalue INT DEFAULT 0;
	SET hivalue = 0;
	UPDATE hiloconfiguration SET  Hi = (SELECT hivalue = Hi + 1)
	WHERE Name = sequencename LIMIT 1; 
	SELECT hivalue;
END //
