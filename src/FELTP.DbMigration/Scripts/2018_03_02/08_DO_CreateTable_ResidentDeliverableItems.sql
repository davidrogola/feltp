﻿CREATE TABLE `residentdeliverableitem` (
  `Id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `ResidentDeliverableId` BIGINT(20) NOT NULL,
  `ResidentId` BIGINT(20) NOT NULL,
  `Title` MEDIUMTEXT NOT NULL,
  `Description` MEDIUMTEXT NOT NULL,
  `FileName` VARCHAR(200) NOT NULL,
  `ContentType` VARCHAR(100) NOT NULL,
  `File` LONGBLOB NOT NULL,
  `DateSubmitted` DATETIME NOT NULL,
  `SubmissionType` INT(11) NOT NULL,
  `DateCreated` DATETIME NOT NULL,
  `CreatedBy` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ResidentDeliverableItem_ResidentDeliverable_idx` (`ResidentDeliverableId`),
  KEY `FK_ResidentDeliverableItem_Resident_idx` (`ResidentId`),
  CONSTRAINT `FK_ResidentDeliverableItem_ResidentDeliverable_idx` FOREIGN KEY (`ResidentDeliverableId`) REFERENCES `residentdeliverable` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ResidentDeliverableItem_Resident_idx` FOREIGN KEY (`ResidentId`) REFERENCES `resident` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
