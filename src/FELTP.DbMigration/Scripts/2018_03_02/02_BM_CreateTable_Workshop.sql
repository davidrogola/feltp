﻿CREATE TABLE `workshop` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `Duration` int(11) NOT NULL,
  `Description` varchar(256) NOT NULL,
  `DateCreated` datetime NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `DateDeactivated` datetime DEFAULT NULL,
  `DeactivatedBy` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;