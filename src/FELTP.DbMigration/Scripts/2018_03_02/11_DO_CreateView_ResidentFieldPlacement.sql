﻿CREATE VIEW residentfieldplacement AS
SELECT fp.Id,  CONCAT(p.FirstName ,' ',p.LastName) as ResidentName, re.Id as ResidentId, fps.Name as SiteName,
CONCAT(fap.FirstName ,' ', fap.LastName) as Supervisor, fp.Description,fp.StartDate, fp.EndDate,fp.ProgressStatus,
fp.CreatedBy as PlacedBy,fp.ResidentFieldPlacementActivityId,fpa.Name as ActivityName
FROM fieldplacement fp 
INNER JOIN resident re ON fp.ResidentId = re.Id
INNER JOIN applicant ap ON ap.Id = re.ApplicantId
INNER JOIN person p ON p.Id= ap.PersonId
INNER JOIN fieldplacementsite fps ON fps.Id = fp.FieldPlacementSiteId
INNER JOIN fieldplacementsitesupervisor spf ON spf.Id = fp.SupervisorId
INNER JOIN faculty fa ON fa.Id = spf.FacultyId
INNER JOIN person fap ON fap.Id = fa.PersonId
INNER JOIN residentfieldplacementactivity rfp on rfp.Id = fp.ResidentFieldPlacementActivityId
INNER JOIN fieldplacementactivity fpa on fpa.Id = rfp.FieldPlacementActivityId

