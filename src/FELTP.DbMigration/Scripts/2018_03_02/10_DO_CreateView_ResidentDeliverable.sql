﻿ CREATE VIEW residentdeliverableview as
 SELECT d.Name AS DeliverableName,rcw.ProgramEnrollmentId,rcw.ResidentId,rd.Id as ResidentDeliverableId,
 rd.ResidentFieldPlacementActivityId, d.Id as DeliverableId,rd.DateCreated,rd.ProgressStatus,rd.CreatedBy
 FROM residentdeliverable rd
 INNER join residentfieldplacementactivity rf ON rd.ResidentFieldPlacementActivityId = rf.id
 INNER JOIN residentunit ru ON ru.Id = rf.ResidentUnitId
 INNER JOIN  deliverable d ON d.Id = rd.DeliverableId
 INNER JOIN residentcoursework rcw ON ru.ResidentCourseWorkId = rcw.id