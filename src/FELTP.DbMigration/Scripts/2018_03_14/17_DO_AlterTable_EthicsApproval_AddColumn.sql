﻿ALTER TABLE ethicsapproval ADD  ProgramEnrollmentId INT(11) NOT NULL;

ALTER TABLE ethicsapproval ADD CONSTRAINT `FK_EthicsApproval_ProgramEnrollmentId` 
FOREIGN KEY (`ProgramEnrollmentId`) REFERENCES `programenrollment` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;





