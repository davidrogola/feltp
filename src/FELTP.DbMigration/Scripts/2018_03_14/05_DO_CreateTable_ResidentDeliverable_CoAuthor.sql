﻿	CREATE TABLE `residentdeliverablecoauthor` (
	  `Id` INT(11) NOT NULL AUTO_INCREMENT,
	  `AuthorName` VARCHAR(200) NOT NULL,
	  `ResidentDeliverableId` BIGINT(20) NOT NULL,
	  `EthicsApprovalCommitteeId` INT(11) NOT NULL,
	  `CreatedBy` VARCHAR(100) NOT NULL,
	  `DateCreated` DATETIME NOT NULL,
	  `IsActive` TINYINT(4) NOT NULL,
	  `DeactivatedBy` VARCHAR(100) DEFAULT NULL,
	  `DateDeactivated` DATETIME DEFAULT NULL,
	  PRIMARY KEY (`Id`),
	   KEY FK_ResidentDeliverableCoauthor_ResidentDeliverable_idx (ResidentDeliverableId),
	   KEY FK_ResidentDeliverableCoauthor_EthicsApprovalCommittee_idx (EthicsApprovalCommitteeId),
	   CONSTRAINT `FK_ResidentDeliverableCoauthor_ResidentDeliverableId` FOREIGN KEY (`ResidentDeliverableId`) REFERENCES `residentdeliverable` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
	   CONSTRAINT `FK_ResidentDeliverableCoauthor_EthicsApprovalCommitteeId` FOREIGN KEY (`EthicsApprovalCommitteeId`) REFERENCES `ethicsapprovalcommittee` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
