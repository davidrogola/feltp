﻿ALTER TABLE ethicsapprovalitem ADD  EthicsApprovalDocumentId BIGINT(20) NOT NULL;

ALTER TABLE ethicsapprovalitem ADD CONSTRAINT `FK_EthicsApprovalItem_EthicsApprovalDocumentId` 
FOREIGN KEY (`EthicsApprovalDocumentId`) REFERENCES `ethicsapprovaldocument` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;





