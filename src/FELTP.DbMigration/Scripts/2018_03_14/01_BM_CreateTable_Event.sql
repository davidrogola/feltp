﻿CREATE TABLE `event` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(256) NOT NULL,
  `Theme` varchar(256) NOT NULL,
  `EventTypeId` int(11) NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `DateCreated` datetime NOT NULL,
  `DeactivatedBy` varchar(100) DEFAULT NULL,
  `DateDeactivated` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
