﻿CREATE TABLE `eventschedule` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProgramEventId` int(11) NOT NULL,
  `EventId` int(11) NOT NULL,
  `PresentationType` int(11) NOT NULL,
  `Location` varchar(100) NOT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime NOT NULL,
  `IsElapsed` TINYINT NOT NULL,
  `DateCreated` datetime NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `DateDeactivated` datetime DEFAULT NULL,
  `DeactivatedBy` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_EventSchedule_ProgramEventId_idx` (`ProgramEventId`),
  KEY `FK_EventSchedule_EventId_idx` (`EventId`),
  CONSTRAINT `FK_EventSchedule_EventId` FOREIGN KEY (`EventId`) REFERENCES `event` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_EventSchedule_ProgramEventId` FOREIGN KEY (`ProgramEventId`) REFERENCES `programevent` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
