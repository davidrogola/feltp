﻿	CREATE TABLE `ethicsapprovalitem` (
	  `Id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	  `EthicsApprovalId` BIGINT(20) NOT NULL,
	  `EthicsApprovalCommitteeId` INT(11) NOT NULL,
	  `ApprovalStatus` INT(11) NOT NULL,
	  `SubmissionDate` DATETIME NOT NULL,
	  `SubmittedBy` VARCHAR(100) NOT NULL,
	  `DateCreated` DATETIME NOT NULL,
	  `DateEvaluated` DATETIME DEFAULT NULL,
	  `EvaluatedBy` VARCHAR(100) DEFAULT  NULL,
	  `Comment` VARCHAR(100) DEFAULT  NULL,
	  `UpdatedBy` VARCHAR(100) DEFAULT  NULL,
	  PRIMARY KEY (`Id`),
	  KEY FK_EthicsApprovalItem_EthicsApproval_idx (EthicsApprovalId),
	  KEY FK_EthicsApprovalItem_EthicsApprovalCommittee_idx (EthicsApprovalCommitteeId),
	  CONSTRAINT `FK_EthicsApprovalItem_EthicsApprovalId` FOREIGN KEY (`EthicsApprovalId`) REFERENCES `ethicsapproval` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
	  CONSTRAINT `FK_EthicsApprovalItem_EthicsApprovalCommitteeId` FOREIGN KEY (`EthicsApprovalCommitteeId`) REFERENCES `ethicsapprovalcommittee` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
