﻿	CREATE TABLE `ethicsapprovalcommittee` (
	  `Id` INT(11) NOT NULL AUTO_INCREMENT,
	  `Name` VARCHAR(200) NOT NULL,
	  `CountryId` INT(11) NOT NULL,
	  `CreatedBy` varchar(100) NOT NULL,
	  `DateCreated` DATETIME NOT NULL,
	  PRIMARY KEY (`Id`),
	  KEY FK_EthicsApprovalCommittee_Country_idx (CountryId),
	  CONSTRAINT `FK_EthicsApprovalCommittee_CountryId` FOREIGN KEY (`CountryId`) REFERENCES `country` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;