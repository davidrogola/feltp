﻿	CREATE TABLE `ethicsapprovaldocument` (
	  `Id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	  `EthicsApprovalId` BIGINT(20) NOT NULL,
	  `Document` LONGBLOB NOT NULL,
	  `FileName` VARCHAR(200) NOT NULL,
	  `ContentType` VARCHAR(100) NOT NULL,
	  `SubmissionDate` DATETIME NOT NULL,
	  `CreatedBy` VARCHAR(100) NOT NULL,
	  `DateCreated` DATETIME NOT NULL,
	  `DocumentVersion` INT(11) NOT NULL,
	  PRIMARY KEY (`Id`),
	  KEY FK_EthicsApprovalDocument_EthicsApproval_idx (EthicsApprovalId),
	  CONSTRAINT `FK_EthicsApprovalDocument_EthicsApprovalId` FOREIGN KEY (`EthicsApprovalId`) REFERENCES `ethicsapproval` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
