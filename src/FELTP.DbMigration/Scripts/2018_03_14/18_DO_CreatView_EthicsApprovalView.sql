﻿CREATE VIEW EthicsApprovalView AS
SELECT ea.Id,rd.ResidentId,rd.ResidentDeliverableId,concat(p.FirstName,' ' ,p.LastName) as ResidentName,
ea.ApprovalStatus,ea.PaperCategory,ea.DateCreated,ea.SubmissionDueDate,ea.EnableEmailNotification,
ea.EnableSmsNotification,e.Title,e.EventTypeId,rd.DeliverableName,ea.ProgramEnrollmentId,ea.ProgramEventId,ea.CreatedBy
FROM ethicsapproval ea 
INNER JOIN residentdeliverableview rd ON rd.ResidentDeliverableId = ea.ResidentDeliverableId
INNER JOIN resident re ON re.Id = ea.ResidentId
INNER JOIN applicant ap ON ap.Id = re.ApplicantId
INNER JOIN person p ON p.Id = ap.PersonId
INNER JOIN programevent pe ON pe.Id = ea.ProgramEventId
INNER JOIN event e ON e.Id = pe.EventId


