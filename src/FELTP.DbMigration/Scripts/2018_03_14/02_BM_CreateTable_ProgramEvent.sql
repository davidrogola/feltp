﻿CREATE TABLE `programevent` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProgramId` int(11) NOT NULL,
  `EventId` int(11) NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `DateCreated` datetime NOT NULL,
  `DeactivatedBy` varchar(100) DEFAULT NULL,
  `DateDeactivated` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ProgramEvent_EventId_idx` (`EventId`),
  KEY `FK_ProgramEvent_ProgramId_idx` (`ProgramId`),
  CONSTRAINT `FK_ProgramEvent_EventId` FOREIGN KEY (`EventId`) REFERENCES `event` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ProgramEvent_ProgramId` FOREIGN KEY (`ProgramId`) REFERENCES `program` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
