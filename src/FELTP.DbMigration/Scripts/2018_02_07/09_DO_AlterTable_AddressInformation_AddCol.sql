﻿ALTER TABLE addressinformation
ADD `PersonId` BIGINT(20) NOT NULL,
ADD KEY `FK_AddressInformation_PersonId_idx` (`PersonId`),
ADD CONSTRAINT `FK_AddressInformation_PersonId` FOREIGN KEY (`PersonId`) REFERENCES `person` (`Id`) 
ON DELETE NO ACTION ON UPDATE NO ACTION