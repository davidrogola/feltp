﻿ALTER TABLE modulefieldplacementactivity
ADD `ModuleId` int(11) NOT NULL,
ADD KEY `FK_ModuleFieldActivity_Module_idx`(`ModuleId`),
add CONSTRAINT `FK_ModuleFieldPlacementActivity_ModuleId` FOREIGN KEY (`ModuleId`) REFERENCES `module` (`Id`) 
ON DELETE NO ACTION ON UPDATE NO ACTION
