﻿ALTER VIEW academichistoryview
AS
SELECT ac.*,un.Name AS University,unc.CourseName AS UniversityCourse FROM 
academichistory ac 
LEFT JOIN university un ON un.Id = ac.UniversityId
LEFT JOIN academiccourse unc ON unc.Id = ac.UniversityCourseId