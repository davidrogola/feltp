﻿UPDATE employmenthistory SET YearOfEmployment  = YEAR(YearOfEmployment) WHERE LENGTH(YearOfEmployment) > 4;

UPDATE employmenthistory SET DurationInPosition = (YEAR(NOW()) - YearOfEmployment) WHERE Id NOT IN (0);

UPDATE employmenthistory SET DurationInPosition = 1 WHERE DurationInPosition = 0;