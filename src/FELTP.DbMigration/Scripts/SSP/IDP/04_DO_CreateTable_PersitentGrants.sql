﻿CREATE TABLE feltpusermanagement.`persistedgrants` (
   `Key` VARCHAR(256) NOT NULL,
   `Type` VARCHAR(256) NOT NULL,
   `SubjectId` VARCHAR(256) NOT NULL,
   `ClientId` VARCHAR(256) NOT NULL,
   `CreationTime` DATETIME NOT NULL,
   `Expiration` DATETIME NULL,
   `Data` VARCHAR(2000) NOT NULL,
   PRIMARY KEY (`Key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


