﻿CREATE TABLE `programcompletionrequirement` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `ProgramOfferId` INT(11) NOT NULL,
  `CompletedProgramId` INT(11) NULL,
  `DateCreated` DATETIME NOT NULL,
  `CreatedBy` VARCHAR(100) NOT NULL,
   PRIMARY KEY (`Id`),
   KEY FK_ProgramCompletionRequirement_ProgramOffer_idx (ProgramOfferId),
   KEY FK_ProgramCompletionRequirement_CompletedProgram_idx (CompletedProgramId),
   CONSTRAINT `FK_ProgramCompletionRequirement_ProgramOffer` FOREIGN KEY (`ProgramOfferId`) REFERENCES `programoffer` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
   CONSTRAINT `FK_ProgramCompletionRequirement_CompletedProgram` FOREIGN KEY (`CompletedProgramId`) REFERENCES `program` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
