﻿CREATE TABLE `programofferacademicrequirement` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `AcademicCourseId` INT(11) NOT NULL,
  `ProgramOfferId` INT(11) NOT NULL,
  `MinimumCourseGradingId` INT(11) NULL,
  `DateCreated` DATETIME NOT NULL,
  `CreatedBy` VARCHAR(100) NOT NULL,
   PRIMARY KEY (`Id`),
   KEY FK_OfferAcademicRequirement_ProgramOfferId_idx (ProgramOfferId),
   KEY FK_OfferAcademicRequirement_AcademicCourse_idx (AcademicCourseId),
   KEY FK_OfferAcademicRequirement_AcademicCourseGrading_idx (MinimumCourseGradingId),
   CONSTRAINT `FK_OfferAcademicRequirement_ProgramOfferId` FOREIGN KEY (`ProgramOfferId`) REFERENCES `programoffer` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
   CONSTRAINT `FK_OfferAcademicRequirement_AcademicCourseGradingId` FOREIGN KEY (`MinimumCourseGradingId`) REFERENCES `academiccoursegrading` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
   CONSTRAINT `FK_OfferAcademicRequirement_AcademicCourseId` FOREIGN KEY (`AcademicCourseId`) REFERENCES `academiccourse` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
