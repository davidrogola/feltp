﻿INSERT INTO academiccourse(Id,EducationLevel,CourseName,DateCreated,CreatedBy)
VALUES(0,0,'PH.D. MEDICINE',NOW(),'Administrator'),
      (0,1,'MASTER OF SCIENCE IN CLINICAL PSYCHOLOGY',NOW(),'Administrator'),
      (0,1,'MASTER OF SCIENCE IN MEDICAL PHYSIOLOGY',NOW(),'Administrator'),
      (0,1,'MASTER OF SCIENCE IN HUMAN ANATOMY',NOW(),'Administrator'),
      (0,1,'MASTER OF SCIENCE (BIOCHEMISTRY)',NOW(),'Administrator'),
      (0,2,'BACHELOR OF MEDICINE AND BACHELOR OF SURGERY',NOW(),'Administrator'),
      (0,2,'BACHELOR OF SCIENCE IN MEDICAL LABORATORY SCIENCE AND TECHNOLOGY',NOW(),'Administrator'),
      (0,2,'BSC (BIOCHEMISTRY)',NOW(),'Administrator'),
      (0,2,'BACHELOR OF SCIENCE IN MEDICAL PHYSIOLOGY',NOW(),'Administrator'),
      (0,3,'POSTGRADUATE DIPLOMA IN CLINICAL AUDIOLOGY & PUBLIC HEALTH OTOLOGY',NOW(),'Administrator'),
      (0,3,'POSTGRADUATE DIPLOMA IN PSYCHOTRAUMA MANAGEMENT',NOW(),'Administrator'),
      (0,3,'POSTGRADUATE DIPLOMA IN PSYCHIATRIC SOCIAL WORK',NOW(),'Administrator'),
      (0,4,'DIPLOMA IN AUDIOLOGY AND PUBLIC HEALTH OTOLOGY',NOW(),'Administrator'),
	  (0,5,'MASTER OF SCIENCE IN CLINICAL PSYCHOLOGY',NOW(),'Administrator'),
	  (0,5,'Certificate in Morgue Training',NOW(),'Administrator');
      