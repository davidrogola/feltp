﻿CREATE TABLE `academiccoursegrading` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `EducationLevel` INT(11) NOT NULL,
  `GradeName` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
