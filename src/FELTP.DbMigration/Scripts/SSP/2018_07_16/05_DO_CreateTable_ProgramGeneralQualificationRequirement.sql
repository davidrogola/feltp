﻿CREATE TABLE `programoffergeneralrequirement` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `ProgramOfferId` INT(11) NOT NULL,
  `MinimumWorkExperienceDuration` INT(11) NOT NULL,
  `BasicComputerKnowledge` TINYINT(4) NOT NULL ,
  `InternalTravelRequired` TINYINT(4) NOT NULL,
  `ExternalTravelRequired` TINYINT(4) NOT NULL,
  `WillingnessToRelocate` TINYINT(4) NOT NULL,
  `DateCreated` DATETIME NOT NULL,
  `CreatedBy` VARCHAR(100) NOT NULL,
   PRIMARY KEY (`Id`),
   KEY FK_OfferGeneralRequirement_ProgramOffer_idx (ProgramOfferId),
   CONSTRAINT `FK_OfferGeneralRequirement_ProgramOffer` FOREIGN KEY (`ProgramOfferId`) REFERENCES `programoffer` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
