﻿SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `mark_mail_as_processed`(IN emailId BIGINT(20), IN operationFailed TINYINT(4))
BEGIN
START TRANSACTION; 
   BEGIN 
   DECLARE processingStatus INT(11) DEFAULT 1;
   DECLARE processed TINYINT(4) DEFAULT 1 ;
   IF operationFailed = FALSE THEN SET processingStatus = 1 ,processed = 1 ;
   ELSE SET processingStatus = 2, processed = 0;
   END IF;
   UPDATE mailqueue SET Processed = processed, Status = processingStatus  WHERE OutgoingMailMessageId = emailId;
   END ;
COMMIT ;
END //