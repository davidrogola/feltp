﻿INSERT INTO messagetype(`Id`,`Name`,`Description`)
VALUES
(5,'Applicant Short Listed','Applicant Short Listed message type'),
(6,'Applicant Enrolled','Applicant enrolled message type  '),
(7,'Applicant Successfully Evaluated','Applicant successfully evaluated message type'),
(8,'Applicant Failed Evaluation','Applicant failed evaluation message type'),
(9,'Applicant waiting list','Applicant waiting list message type');
