﻿INSERT INTO messagetemplate(Id,MessageTypeId,Subject,Body)
VALUES(5, 5,
'FELTP Program Offer Application Shortlisting', 
'Dear @Model.Name, <br />
 <p></p>
 <p></p>
 You have been shortlisted for the program offer @Model.ProgramOffer under the program @Model.Program <br />
 on date @Model.DateShortListed<br />
 <p></p>
 <p></p>
 Best Regards,
 <p></p>
 <b>KFELTP Team </b>');