﻿ALTER TABLE residentdeliverableitem CHANGE  `FileName` `FileName` VARCHAR(200) NULL;
ALTER TABLE residentdeliverableitem CHANGE  `ContentType` `ContentType` VARCHAR(100) NULL;
ALTER TABLE residentdeliverableitem CHANGE  `File` `File` LONGBLOB NULL;