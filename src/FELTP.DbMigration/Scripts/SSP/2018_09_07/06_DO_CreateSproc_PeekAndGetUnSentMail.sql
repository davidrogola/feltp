﻿SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `peek_unsent_email`(IN peekedBy VARCHAR(256), IN peekTimeInSeconds INT(11))
BEGIN
START TRANSACTION;
BEGIN
	DECLARE utcNow  DATETIME DEFAULT NOW();
	DECLARE endPeekAt DATETIME DEFAULT DATE_ADD(utcNow,INTERVAL peekTimeInSeconds SECOND); 
    DROP TABLE IF EXISTS EmailMessageIds;
    CREATE TEMPORARY TABLE IF NOT EXISTS EmailMessageIds(Id BIGINT(20) NOT NULL);
	INSERT INTO EmailMessageIds 
    SELECT mq.OutgoingMailMessageId from mailqueue mq 
	WHERE mq.Processed = 0 
	AND (mq.PeekedToDate IS NULL OR mq.PeekedDate <= utcNow)
    AND (mq.ResevervedToDate IS NULL or mq.ResevervedToDate <= utcNow) ;
   
   UPDATE mailqueue SET PeekedBy = peekedBy,PeekedDate = utcNow,PeekedToDate = endPeekAt 
   WHERE OutgoingMailMessageId IN (SELECT * FROM EmailMessageIds);
   
    SELECT * FROM EmailMessageIds ;
    END;
COMMIT ;
END //