﻿INSERT INTO messagetemplate(Id,MessageTypeId,Subject,Body)
VALUES(6, 6,
'Application Status – Successfully Enrolled', 
'Dear @Model.Name, <br />
<p></p>
<p></p>
Congratulations on your successful enrolment into the <b>@Model.ProgramOffer</b> program offer. <br/>
The Program Support Unit shall contact you with further details regarding the program training commencement. <br/>
Welcome to the Kenya Field Epidemiology & Laboratory Training Program fraternity. <br/>
<p></p>
<p></p>
<b>
Best Regards,<br/> 
Program Support Unit, <br/>
Field Epidemiology and Laboratory Training Program Office <br/>
P.O. Box 22313 – 00100, Nairobi, Kenya <br/>
Tel: +254-700 752550 / +254-700 752542 <br/>
Kenyatta National Hospital Grounds <br/>
Nairobi, Kenya. 
</b>'),
(7, 7,
'Application Status - Qualified ', 
'
Dear @Model.Name, <br />
<p></p>
<p></p>
The FELTP team would wish to thank you for showing interest in <b>@Model.ProgramOffer</b> program offer. <br/><br/>
This is to inform you that have you have qualified for enrolment to the program. <br/>
The Program support unit shall contact you with further details regarding the program enrollment offer.  

<p></p>
<p></p>
<b>
Best Regards,<br/> 
Program Support Unit, <br/>
Field Epidemiology and Laboratory Training Program Office <br/>
P.O. Box 22313 – 00100, Nairobi, Kenya <br/>
Tel: +254-700 752550 / +254-700 752542 <br/>
Kenyatta National Hospital Grounds <br/>
Nairobi, Kenya. 
</b>'),
(8, 8,
'Application Status – Waiting List ', 
'Dear @Model.Name, <br />
<p></p>
<p></p>

The FELTP team would wish to thank you for showing interest in <b>@Model.ProgramOffer</b> program offer.<br/><br/>
However, we regret to inform you that you were not successful in the evaluation process. <br/>

While you were not selected for the <b>@Model.ProgramOffer</b>, the evaluating team recognized the high quality of your skillset and capability. <br/>
For this reason, you have been put on a waiting list for a possible opportunity in this program offer.  <br/><br/>

We are glad, that you`ve shown interest in <b>@Model.ProgramOffer</b>.  
<p></p>
<p></p>
<b>
Best Regards,<br/> 
Program Support Unit, <br/>
Field Epidemiology and Laboratory Training Program Office <br/>
P.O. Box 22313 – 00100, Nairobi, Kenya <br/>
Tel: +254-700 752550 / +254-700 752542 <br/>
Kenyatta National Hospital Grounds <br/>
Nairobi, Kenya. 
</b>')
