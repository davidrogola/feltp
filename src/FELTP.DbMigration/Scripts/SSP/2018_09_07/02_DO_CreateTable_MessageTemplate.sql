﻿CREATE TABLE  `messagetemplate` (
  `Id` INT(11) NOT NULL,
  `MessageTypeId` INT(11) NOT NULL,
  `Subject` VARCHAR(256)  NULL,
  `Body` VARCHAR(2000) NOT NULL,
   PRIMARY KEY (`Id`),
  KEY FK_MessageTemplate_MessageTypeId_Idx (MessageTypeId),
  CONSTRAINT `FK_MessageTemplate_MessageTypeId` FOREIGN KEY (`MessageTypeId`) REFERENCES `messagetype` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




