﻿UPDATE messagetemplate SET Body ='Dear @Model.Name, <br />
<p></p>
<p></p>
The FELTP team would wish to thank you for showing interest in <b>@Model.ProgramOffer</b> program offer. <br/><br/>
This is to inform you that you were successfully shortlisted for further evaluation on <b>@Model.DateShortListed.</b><br/>
The Program support unit shall contact you with further details regarding the interview process.<br/>
We wish all the best in the interview process. 
<p></p>
<p></p>
<b>
Best Regards,<br/> 
Program Support Unit, <br/>
Field Epidemiology and Laboratory Training Program Office <br/>
P.O. Box 22313 – 00100, Nairobi, Kenya <br/>
Tel: +254-700 752550 / +254-700 752542 <br/>
Kenyatta National Hospital Grounds <br/>
Nairobi, Kenya. 
</b>',Subject='Application Status - Shortlisted' WHERE Id = 5;