﻿CREATE TABLE  `mailqueue` (
  `OutgoingMailMessageId` BIGINT(20) NOT NULL,
  `Recipient` VARCHAR(256) NOT NULL,
  `Subject` VARCHAR(256) NOT NULL,
  `Body` VARCHAR(2000) NOT NULL,
  `Processed` TINYINT(4) NOT NULL,
  `Status` INT(11) NOT NULL,
  `DateCreated` DATETIME NOT NULL,
  `PeekedBy` VARCHAR(256)  NULL,
  `PeekedDate` DATETIME  NULL,
  `PeekedToDate` DATETIME  NULL,
  `ReservedDate` DATETIME  NULL,
  `ResevervedToDate` DATETIME  NULL,
   PRIMARY KEY (`OutgoingMailMessageId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;