﻿CREATE TABLE  `outgoingmailmessage` (
  `Id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `Recipient` VARCHAR(256) NOT NULL,
  `Subject` VARCHAR(256) NOT NULL,
  `Body` VARCHAR(2000) NOT NULL,
  `DeliveryAttempts` INT(11) NOT NULL,
  `Status` INT(11) NOT NULL,
  `Createdy` VARCHAR(256) NOT NULL,
  `DateCreated` DATETIME NOT NULL,
  `CorrelationId` VARCHAR(256) NULL,
   PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



