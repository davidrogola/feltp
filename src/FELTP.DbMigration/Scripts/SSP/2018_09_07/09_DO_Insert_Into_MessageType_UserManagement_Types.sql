﻿INSERT INTO messagetype(`Id`,`Name`,`Description`)
VALUES
(1,'Password Reset','Password reset message type'),
(2,'Account Activation','Message type to activate account'),
(3,'Forgot Password','New User Password Request'),
(4,'Email Change','User Email Change Request');
