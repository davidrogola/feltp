﻿SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `try_reserve_and_get_unsent_mail` (IN emailId BIGINT(20), IN shouldBePeekedBy VARCHAR(256), IN secondsToHold INT(11))
BEGIN
START TRANSACTION;
  BEGIN
	DECLARE utcNow  DATETIME DEFAULT NOW();
	DECLARE reservedToDate DATETIME DEFAULT DATE_ADD(utcNow,INTERVAL secondsToHold SECOND);
    
  DROP TABLE IF EXISTS OutGoingMailMessageIds;
  CREATE TEMPORARY TABLE IF NOT EXISTS OutGoingMailMessageIds(Id BIGINT(20) NOT NULL);
  
 INSERT INTO OutGoingMailMessageIds
   SELECT mq.OutgoingMailMessageId from mailqueue mq 
   WHERE OutgoingMailMessageId = emailId 
   AND mq.Processed = 0 
   AND mq.PeekedBy = shouldBePeekedBy
   AND (mq.ResevervedToDate IS NULL OR mq.ResevervedToDate <= utcNow) ;
   
   UPDATE mailqueue SET ReservedDate = utcNow,ResevervedToDate = reservedToDate
   WHERE OutgoingMailMessageId IN (SELECT * FROM OutGoingMailMessageIds);
   
    SELECT OutGoingMailMessageId,Body,Subject,Recipient,ReservedDate,ResevervedToDate FROM mailqueue 
	WHERE OutGoingMailMessageId IN (SELECT * FROM OutGoingMailMessageIds) ;
END ;
   COMMIT ;
END //
