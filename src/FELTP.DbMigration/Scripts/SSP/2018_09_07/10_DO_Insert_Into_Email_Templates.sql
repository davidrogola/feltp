﻿INSERT INTO messagetemplate(Id,MessageTypeId,Subject,Body)
VALUES(1, 1,
'FELTP User Password Reset', 
'Dear @Model.Name, <br />
<p></p>
Your password has been reset successfully.<br />
Please click on the link below to Login to <b>KFELTP</b>.<br />
<p></p>
Link : @Model.Link	
<p><p/>
Your user name to login to the system is : <br />
UserName : <b>@Model.Name </b><br/>
<p></p>
Best Regards,
<p></p>
<b>KFELTP</b> Team.'),
(2, 2,
'FELTP User Account Activation', 
'Dear @Model.Name, <br />
<p></p>
You have successfully registered with <b>KFELTP</b>.<br />
Please click on the link below to activate your account.<br />
<p></p>
Link : @Model.Link	
<p><p/>
Your user name to login to the system is <br />
<p></p>
UserName : <b>@Model.Name </b><br/>
<p></p>
Best Regards,
<p></p>
<b>KFELTP</b> Team.'),
(3, 3,
'FELTP User New Password Request', 
'Dear @Model.Name, <br />
<p></p>
You have requested a password change for your <b>KFELTP</b> account. <br />
Click on the link below to reset your password.<br />
<p></p>
Link : @Model.Link	
<p><p/>
If you did not request this change, please contact Admin at it@feltp.or.ke <br />
immediately to prevent any fradulent activity on your account.
<p></p>
Best Regards,
<p></p>
<b>KFELTP</b> Team.'),
(4, 4,
'FELTP User Email Change Request', 
'Dear @Model.Name, <br /> <p></p> Your email address has been changed successfully to <b>@Model.Email</b>.<br /> Please click on the link below to confirm your new email address.<br />  <p></p> Link : @Model.Link	 <p><p/>  Your New user name to login to the system is : <br /> UserName : <b>@Model.Name </b><br/> <p></p> Best Regards, <p></p> <b>KFELTP</b> Team. ');