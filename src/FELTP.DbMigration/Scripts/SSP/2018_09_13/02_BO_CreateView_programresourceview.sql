﻿ CREATE VIEW programresourceview AS
 SELECT r.Id, p.id as ProgramId, rt.Name as ResourcesType, r.ResourceTitle, r.ResourceDescription, r.DateCreated, r.CreatedBy
 
 FROM resource r 
 INNER JOIN resourcetype rt on r.ResourceTypeId = rt.id
 INNER JOIN programresource pr ON r.id = pr.ResourceId
 INNER JOIN  program p ON pr.ProgramId = p.id



