﻿INSERT INTO `feltpusermanagement`.`aspnetusers`
(`Id`,`AccessFailedCount`,`ConcurrencyStamp`,`CreatedById`,`DateCreated`,`Email`,`EmailConfirmed`,`LastPasswordChangeDate`,`LockoutEnabled`,`LockoutEnd`,
`NormalizedEmail`,`NormalizedUserName`,`PasswordHash`,`PhoneNumber`,`PhoneNumberConfirmed`,`SecurityStamp`,`TwoFactorEnabled`,`UserName`,`UserStatus`,
`TermsAccepted`,`DateTermsAccepted`,`IdentificationNumber`,`ClientName`,`CorrelationId`,`IdentificationType`)
SELECT 0, 0, UUID(),NULL,NOW(),co.EmailAddress,0,NOW(),1,NULL,UCASE(co.EmailAddress),UCASE(SUBSTRING_INDEX(co.EmailAddress,'@',1)),NULL,co.PrimaryMobileNumber,1,UUID(),
0,SUBSTRING_INDEX(co.EmailAddress,'@',1),1,0,NULL,pe.IdentificationNumber,'FELTP SSP', ap.Id,pe.IdentificationType
FROM applicant ap 
INNER JOIN person pe ON pe.Id = ap.PersonId
INNER JOIN contactinformation co ON co.PersonId = pe.Id
WHERE SUBSTRING_INDEX(co.EmailAddress,'@',1) NOT IN (SELECT asp.UserName FROM feltpusermanagement.aspnetusers asp);