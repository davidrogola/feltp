﻿CREATE VIEW residentunitattendanceview
AS
SELECT rua.*,t.SemesterScheduleItemId,ru.ResidentCourseWorkId,t.StartDate AS StartTime, t.EndDate AS EndTime,
 CONCAT(fpa.FirstName,' ', fpa.LastName) AS Trainer, CONCAT(app.FirstName,' ', app.LastName) AS ResidentName,
 r.RegistrationNumber,ru.Name AS Unit, ru.Code,r.Id AS ResidentId,ru.UnitId
 FROM residentunitattendance rua
INNER JOIN timetable t ON rua.TimetableId = t.Id
INNER JOIN residentunitview ru ON rua.ResidentUnitId = ru.ResidentUnitId
INNER JOIN resident r ON r.Id = ru.ResidentId
INNER JOIN applicant ap ON ap.Id = r.ApplicantId
INNER JOIN person app ON app.Id = ap.PersonId
INNER JOIN faculty fa ON fa.Id = rua.FacultyId
INNER JOIN person fpa ON fpa.Id = fa.Id
GROUP BY rua.Id;