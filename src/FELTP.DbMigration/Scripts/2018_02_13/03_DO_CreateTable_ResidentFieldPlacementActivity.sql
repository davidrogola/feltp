﻿CREATE TABLE `residentfieldplacementactivity` (
  `Id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `FieldPlacementActivityId` INT(11) NOT NULL,
  `ResidentModuleId` BIGINT(20) NOT NULL,
  `DateCreated` DATETIME NOT NULL,
  `CreatedBy` VARCHAR(100) NOT NULL,
  `ProgressStatus` INT(11) NOT NULL,
  `EvalutedBy` VARCHAR(100) DEFAULT NULL,
  `DateEvaluated` DATETIME DEFAULT NULL,
  `DateCompleted` DATETIME DEFAULT NULL,
  `DateDeactivated` DATETIME DEFAULT NULL,
  `DeactivatedBy` VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ResidentFieldPlacementActivity_FieldPlacementActivity_idx` (`FieldPlacementActivityId`),
  KEY `FK_ResidentFieldPlacementActivity_ResidentModuleId_idx` (`ResidentModuleId`),
  CONSTRAINT `FK_ResidentFieldPlacementActivity_FieldPlacementActivity_Id` FOREIGN KEY (`FieldPlacementActivityId`) REFERENCES `fieldplacementactivity` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ResidentFieldPlacementActivity_ResidentModuleId_idx` FOREIGN KEY (`ResidentModuleId`) REFERENCES `residentmodule` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
