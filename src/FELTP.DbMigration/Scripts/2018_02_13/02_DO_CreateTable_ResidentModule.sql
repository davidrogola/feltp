﻿CREATE TABLE `residentmodule` (
                                `Id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                                `ResidentCourseWorkId` BIGINT(20) NOT NULL,
                                `ModuleId` INT(11) NOT NULL,
                                `DateCreated` DATETIME NOT NULL,
                                `CreatedBy` VARCHAR(100) NOT NULL,
                                PRIMARY KEY (`Id`),
                                KEY `FK_ResidentModule_ResidentCourseWork_idx` (`ResidentCourseWorkId`),
                                KEY `FK_ResidentModule_Module_idx` (`ModuleId`),
                                CONSTRAINT `FK_ResidentModule_ResidentCourseWork_Id` FOREIGN KEY (`ResidentCourseWorkId`) REFERENCES `residentcoursework` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                                CONSTRAINT `FK_ResidentModule_Module_Id` FOREIGN KEY (`ModuleId`) REFERENCES `module` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
