﻿CREATE TABLE `residentdeliverable` (
  `Id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `ResidentFieldPlacementActivityId` BIGINT(20) NOT NULL,
  `DeliverableId` INT(11) NOT NULL,
  `ProgressStatus` INT(11) NOT NULL,
  `DateCreated` DATETIME NOT NULL,
  `CreatedBy` VARCHAR(100) NOT NULL,
  `EvaluatedBy` VARCHAR(100) DEFAULT NULL,
  `DateEvaluated` DATETIME DEFAULT NULL,
  `DateCompleted` DATETIME DEFAULT NULL,
  `DateDeactivated` DATETIME DEFAULT NULL,
  `DeactivatedBy` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ResidentDeliverable_DeliverableId_idx` (`DeliverableId`),
  KEY `FK_ResidentDeliverable_ResidentFieldPlacementActivityId_idx` (`ResidentFieldPlacementActivityId`),
  CONSTRAINT `FK_ResidentDeliverable_DeliverableId` FOREIGN KEY (`DeliverableId`) REFERENCES `deliverable` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ResidentDeliverable_ResidentFieldPlacementActivityId` FOREIGN KEY (`ResidentFieldPlacementActivityId`) REFERENCES `residentfieldplacementactivity` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
