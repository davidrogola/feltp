﻿CREATE TABLE `residentcoursework` (
  `Id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `ProgramEnrollmentId` INT(11) NOT NULL,
  `ProgramCourseId` INT(11) NOT NULL,
  `ProgressStatus` INT(11) NOT NULL,
  `TotalScore` INT(11) DEFAULT NULL,
  `DateCreated` DATETIME NOT NULL,
  `CreatedBy` VARCHAR(100) NOT NULL,
  `EvaluatedBy` VARCHAR(100) DEFAULT NULL,
  `DateEvaluated` DATETIME DEFAULT NULL,
  `DateCompleted` DATETIME DEFAULT NULL,
  `DateDeactivated` DATETIME DEFAULT NULL,
  `DeactivatedBy` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ResidentCourseWork_ProgramEnrollmentId_idx` (`ProgramEnrollmentId`),
  KEY `FK_ResidentCourseWork_ProgramCourseId_idx` (`ProgramCourseId`),
  CONSTRAINT `FK_ResidentCourseWork_ProgramEnrollmentId` FOREIGN KEY (`ProgramEnrollmentId`) REFERENCES `programenrollment` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ResidentCourseWork_ProgramCourseId` FOREIGN KEY (`ProgramCourseId`) REFERENCES `programcourse` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
