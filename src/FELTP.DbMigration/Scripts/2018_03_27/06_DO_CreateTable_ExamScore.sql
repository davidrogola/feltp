﻿CREATE TABLE `examinationscore` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `ExaminationId` INT(11) NOT NULL,
  `ExaminationParticipationId` INT(11) NOT NULL,
  `Score` DECIMAL(13,4) NOT NULL,
  `ResidentUnitId` BIGINT(20) NOT NULL,
  `DateCreated` DATETIME NOT NULL,
  `CreatedBy` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY FK_ExaminationScore_Examination_idx (ExaminationId),
  KEY FK_ExaminationScore_ExaminationParticipation_idx (ExaminationParticipationId),
  KEY FK_ExaminationScore_ResidentUnit_idx (ResidentUnitId),
  CONSTRAINT `FK_ExaminationScore_ExaminationId` FOREIGN KEY (`ExaminationId`) REFERENCES `examination` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ExaminationScore_ExaminationParticipationId` FOREIGN KEY (`ExaminationParticipationId`) REFERENCES `examinationparticipation` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ExaminationScore_ResidentUnitId` FOREIGN KEY (`ResidentUnitId`) REFERENCES `residentunit` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

