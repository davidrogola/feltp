﻿CREATE TABLE `timetable` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `SemesterScheduleItemId` INT(11) NOT NULL,
  `StartDate` DATETIME NOT NULL,
  `EndDate` DATETIME NOT NULL,
  `TimeTableType` INT(11) NOT NULL,
  `UnitId` INT(11) NULL,
  `ExaminationId` INT(11) NULL,
  `FacultyId` INT(11) NOT NULL,
  `DateCreated` DATETIME NOT NULL,
  `CreatedBy` VARCHAR(100) NOT NULL,
   PRIMARY KEY (`Id`),
  KEY FK_Timetable_Unit_idx (UnitId),
  KEY FK_Timetable_SemesterScheduleItem_idx (SemesterScheduleItemId),
  KEY FK_Timetable_Examination_idx (ExaminationId),
  KEY FK_Timetable_Faculty_idx (FacultyId),
  CONSTRAINT `FK_Timetable_UnitId` FOREIGN KEY (`UnitId`) REFERENCES `unit` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Timetable_SemesterScheduleItemId` FOREIGN KEY (`SemesterScheduleItemId`) REFERENCES `semesterscheduleitem` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Timetable_ExaminationId` FOREIGN KEY (`ExaminationId`) REFERENCES `examination` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Timetable_FacultyId` FOREIGN KEY (`FacultyId`) REFERENCES `faculty` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;


