﻿CREATE TABLE `examinationparticipation` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `TimetableId` INT(11) NOT NULL,
  `ProgramEnrollmentId` INT(11) NOT NULL,
  `DateCreated` DATETIME NOT NULL,
  `CreatedBy` VARCHAR(100) NOT NULL,
   PRIMARY KEY (`Id`),
  KEY FK_ExaminationParticipation_Timetable_idx (TimetableId),
  KEY FK_ExaminationParticipation_ProgramEnrollment_idx (ProgramEnrollmentId),
  CONSTRAINT `FK_ExaminationParticipation_TimetableId` FOREIGN KEY (`TimetableId`) REFERENCES `timetable` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ExaminationParticipation_ProgramEnrollmentId` FOREIGN KEY (`ProgramEnrollmentId`) REFERENCES `programenrollment` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
