﻿CREATE TABLE `examination` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(256) NOT NULL,
  `UnitId` INT(11) NOT NULL,
  `DateCreated` DATETIME NOT NULL,
  `CreatedBy` VARCHAR(100) NOT NULL,
   PRIMARY KEY (`Id`),
  KEY FK_Examination_Unit_idx (UnitId),
  CONSTRAINT `FK_Examination_UnitId` FOREIGN KEY (`UnitId`) REFERENCES `unit` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

