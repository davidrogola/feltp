﻿SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
DELIMITER //
DROP PROCEDURE IF EXISTS `update_semester_and_residentcoursework_info`;
CREATE PROCEDURE `update_semester_and_residentcoursework_info` (IN semesterScheduleId INT(11),IN programOfferId INT(11), IN semesterId INT(11),IN newStatus INT(11))
BEGIN
START TRANSACTION;
DROP TABLE IF EXISTS ResidentCourseWorkIds;
DROP TABLE IF EXISTS SemesterScheduleIds;

CREATE TEMPORARY TABLE IF NOT EXISTS ResidentCourseWorkIds AS (SELECT rsc.Id FROM (SELECT * FROM residentcoursework) as rsc 
INNER JOIN programenrollment pe ON pe.Id = rsc.ProgramEnrollmentId
INNER JOIN programoffer po ON po.Id = pe.ProgramOfferId
INNER JOIN programcourse pc ON pc.id = rsc.ProgramCourseId
INNER JOIN semester s ON s.Id = pc.SemesterId
where po.Id = programOfferId AND s.Id = semesterId);

UPDATE residentcoursework SET ProgressStatus = newStatus WHERE Id IN (SELECT * FROM ResidentCourseWorkIds);

CREATE TEMPORARY TABLE IF NOT EXISTS SemesterScheduleIds AS (SELECT sms.Id FROM semesterschedule ss 
INNER JOIN (SELECT * FROM semesterscheduleitem) as sms ON sms.SemesterScheduleId = ss.Id
WHERE ss.Id = semesterScheduleId);
 
IF newStatus = 1 THEN /* In Progress */
UPDATE semesterscheduleitem  smsi SET smsi.Status = newStatus WHERE smsi.Id IN (SELECT * FROM SemesterScheduleIds)
AND  DATE(NOW()) >=  DATE(smsi.StartDate);
ELSE
UPDATE semesterscheduleitem  SET Status = newStatus WHERE Id IN (SELECT * FROM SemesterScheduleIds);
END IF;
COMMIT;
END //
