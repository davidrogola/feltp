﻿SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET SQL_SAFE_UPDATES = 0;

UPDATE residentcoursework res SET ResidentId = (SELECT ResidentId FROM programenrollment pe WHERE res.ProgramEnrollmentId = pe.Id );

SET SQL_SAFE_UPDATES = 1;