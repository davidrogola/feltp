﻿INSERT INTO ministry VALUES(1,'Ministry of Public Service, Youth, and Gender');
INSERT INTO ministry VALUES(2,'Ministry of Petroleum and Mining');
INSERT INTO ministry VALUES(3,'Ministry of Devolution and ASAL');
INSERT INTO ministry VALUES(4,'Ministry of Defence');
INSERT INTO ministry VALUES(5,'Ministry of Foreign Affairs and International Trade');
INSERT INTO ministry VALUES(6,'Ministry of Health');
INSERT INTO ministry VALUES(7,'Ministry of Sports and Heritage');
INSERT INTO ministry VALUES(8,'Ministry of Tourism');
INSERT INTO ministry VALUES(9,'Ministry of Lands');
INSERT INTO ministry VALUES(10,'Ministry of Labour and Social Protection');
INSERT INTO ministry VALUES(11,'Ministry of EAC and North Corridor Development');
INSERT INTO ministry VALUES(12,'Ministry of Education');
INSERT INTO ministry VALUES(13,'Ministry of ICT');
INSERT INTO ministry VALUES(14,'Ministry of Industrialization and Enterprise Development');
INSERT INTO ministry VALUES(15,'Ministry of Environment and Forestry');
INSERT INTO ministry VALUES(16,'Ministry of Interior');
INSERT INTO ministry VALUES(17,'Ministry of Water and Sanitation');
INSERT INTO ministry VALUES(18,'Ministry of The National Treasury and Planning');
INSERT INTO ministry VALUES(19,'Ministry of Agriculture');
INSERT INTO ministry VALUES(20,'Ministry of Transport and Infrastructure Development');
INSERT INTO ministry VALUES(21,'Ministry of Energy');
