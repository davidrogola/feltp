﻿ ALTER VIEW residentdeliverableview AS
 SELECT d.Name AS DeliverableName,rcw.ProgramEnrollmentId,rcw.ResidentId,rd.Id AS ResidentDeliverableId,
 rd.ResidentFieldPlacementActivityId, d.Id as DeliverableId,rd.DateCreated,rd.ProgressStatus,rd.CreatedBy, d.Code
 FROM residentdeliverable rd
 INNER join residentfieldplacementactivity rf ON rd.ResidentFieldPlacementActivityId = rf.id
 INNER JOIN residentunit ru ON ru.Id = rf.ResidentUnitId
 INNER JOIN  deliverable d ON d.Id = rd.DeliverableId
 INNER JOIN residentcoursework rcw ON ru.ResidentCourseWorkId = rcw.id