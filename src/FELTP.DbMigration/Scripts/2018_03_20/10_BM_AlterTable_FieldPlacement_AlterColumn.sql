﻿ALTER TABLE fieldplacement MODIFY COLUMN FieldPlacementSiteId INT(11) NULL;

ALTER TABLE fieldplacement MODIFY COLUMN SupervisorId INT(11) NULL;