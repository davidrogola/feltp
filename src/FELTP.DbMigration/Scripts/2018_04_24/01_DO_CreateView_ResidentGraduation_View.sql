﻿CREATE VIEW ResidentGraduationInfoView AS
SELECT g.Id,g.ResidentId,g.ProgramEnrollmentId,g.QualificationStatus,g.ExpectedProgramEndDate,
g.DateCreated,g.CompletionStatus,g.CreatedBy,pr.Name AS ProgramName,po.Name AS ProgramOffer,ap.Id AS ApplicantId,
CONCAT(p.FirstName ,' ',p.LastName) AS ResidentName,po.Id AS ProgramOfferId, pr.Id AS ProgramId,re.RegistrationNumber,g.GraduationRequirementId
FROM graduate g 
INNER JOIN  graduationrequirement gr ON gr.Id = g.GraduationRequirementId
INNER JOIN  programenrollment pe ON pe.Id = g.ProgramEnrollmentId
INNER JOIN  resident re ON re.Id = g.ResidentId
INNER JOIN  applicant ap ON ap.Id = re.ApplicantId
INNER JOIN  person p ON p.Id = ap.PersonId
INNER JOIN  programoffer po ON po.Id = pe.ProgramOfferId
INNER JOIN  program pr ON pr.Id = po.ProgramId