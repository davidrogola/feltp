﻿CREATE VIEW GraduationEligibityItemsView
AS
SELECT gi.*,u.Name AS UnitName,u.Code,s.Id AS SemesterId, s.Name AS Semester FROM graduateeligibilityitem gi 
LEFT JOIN residentunit ru ON ru.Id = gi.ResidentUnitId
LEFT JOIN unit u ON u.Id = ru.UnitId
LEFT JOIN residentcoursework rsc ON rsc.Id = ru.ResidentCourseWorkId
LEFT JOIN programcourse pc ON pc.Id  = rsc.ProgramCourseId
LEFT JOIN semester s ON s.Id = pc.SemesterId;


