﻿CREATE TABLE `programenrollment` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProgramOfferId` int(11) NOT NULL,
  `ResidentId` bigint(20) NOT NULL,
  `DateCreated` datetime NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `DateDeactivated` datetime DEFAULT NULL,
  `DeactivatedBy` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ProgramEnrollment_ResidentId_idx` (`ResidentId`),
  KEY `FK_ProgramEnrollemnt_ProgramOfferId_idx` (`ProgramOfferId`),
  CONSTRAINT `FK_ProgramEnrollment_ResidentId` FOREIGN KEY (`ResidentId`) REFERENCES `resident` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ProgramEnrollment_ProgramOfferId` FOREIGN KEY (`ProgramOfferId`) REFERENCES `programoffer` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
