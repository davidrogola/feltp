﻿CREATE TABLE `programoffer` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `ProgramId` int(11) NOT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime NOT NULL,
  `DateCreated` datetime NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `DateDeactivated` datetime DEFAULT NULL,
  `DeactivatedBy` varchar(100) DEFAULT NULL,
  `FacultyId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ProgramOffer_ProgramId_idx` (`ProgramId`),
  KEY `FK_ProgramOffer_Faculty_idx` (`FacultyId`),
  CONSTRAINT `FK_ProgramOffer_ProgramId` FOREIGN KEY (`ProgramId`) REFERENCES `program` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ProgramOffer_FacultyId` FOREIGN KEY (`FacultyId`) REFERENCES `faculty` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
