﻿CREATE TABLE `programofferlocation` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CountryId` int(11) NOT NULL,
  `CountyId` int(11) DEFAULT NULL,
  `SubCountyId` int(11) DEFAULT NULL,
  `PostalAddress` varchar(100) NOT NULL,
  `Location` varchar(100) NOT NULL,
  `Building` varchar(100) NOT NULL,
  `ProgramOfferId` INT NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ProgramOfferLocation_ProgramOffer_idx` (`ProgramOfferId`),
  KEY `FK_ProgramOffer_Country_idx` (`CountryId`),
  KEY `FK_ProgramOffer_SubCounty_idx` (`SubCountyId`),
  KEY `FK_ProgramOffer_County_idx` (`CountyId`),
  CONSTRAINT `FK_ProgramOfferLocation_ProgramOfferId` FOREIGN KEY (`ProgramOfferId`) REFERENCES `programoffer` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ProgramOfferLocation_CountryId` FOREIGN KEY (`CountryId`) REFERENCES `country` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
   CONSTRAINT `FK_ProgramOfferLocation_SubCountyId` FOREIGN KEY (`SubCountyId`) REFERENCES `subcounty` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ProgramOfferLocation_CountyId` FOREIGN KEY (`CountyId`) REFERENCES `county` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
