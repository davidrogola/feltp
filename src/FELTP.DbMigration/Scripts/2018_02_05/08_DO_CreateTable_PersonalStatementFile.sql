﻿CREATE TABLE `personalstatementfile` (
  `Id` int(20) NOT NULL AUTO_INCREMENT,
  `ApplicantId` bigint(20) NOT NULL, 
  `File` blob NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_PersonalStatementFile_Applicant_idx` (`ApplicantId`),
  CONSTRAINT `FK_PersonalStatementFile_Applicant_Id` FOREIGN KEY (`ApplicantId`) REFERENCES `applicant` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
