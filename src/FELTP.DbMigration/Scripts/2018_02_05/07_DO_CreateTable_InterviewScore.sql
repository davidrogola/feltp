﻿CREATE TABLE `interviewscore` (
  `Id` int(20) NOT NULL AUTO_INCREMENT,
  `ApplicantId` bigint(20) NOT NULL, 
  `OralInterview` int(20) NOT NULL,
  `WitternInterview` int(20) NOT NULL,
  `Total` int(20) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_InterviewScore_Applicant_idx` (`ApplicantId`),
  CONSTRAINT `FK_InterviewScore_ApplicantId` FOREIGN KEY (`ApplicantId`) REFERENCES `applicant` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
