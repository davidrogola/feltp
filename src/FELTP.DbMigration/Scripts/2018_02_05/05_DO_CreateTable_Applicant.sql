﻿CREATE TABLE `applicant` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `PersonId` bigint(20) NOT NULL,
  `DateCreated` datetime NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `PersonalStatementSubmitted` tinyint(4) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Applicant_Person_idx` (`PersonId`),
  CONSTRAINT `FK_Applicant_PersonId` FOREIGN KEY (`PersonId`) REFERENCES `person` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
