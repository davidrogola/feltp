﻿CREATE TABLE `resident` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ApplicantId` bigint(20) NOT NULL, 
  `RegistrationNumber` varchar(100) NOT NULL,
  `DateCreated` datetime NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `DateDeactivated` datetime DEFAULT NULL,
  `DeactivatedBy` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Resident_Applicant_idx` (`ApplicantId`),
  CONSTRAINT `FK_Resident_ApplicantId` FOREIGN KEY (`ApplicantId`) REFERENCES `applicant` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
