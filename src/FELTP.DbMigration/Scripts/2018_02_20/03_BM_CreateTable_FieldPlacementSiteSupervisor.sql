﻿CREATE TABLE `fieldplacementsitesupervisor` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `FacultyId` int(11) NOT NULL,
  `FieldPlacementSiteId` int(11) NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `DateCreated` datetime NOT NULL,
  `DateDeactivated` datetime DEFAULT NULL,
  `DeactivatedBy` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_FieldPlacementSiteSupervisor_FacultyId_idx` (`FacultyId`),
  KEY `FK_FieldPlacementSiteSupervisor_FieldPlacementSiteId_idx` (`FieldPlacementSiteId`),
  CONSTRAINT `FK_FieldPlacementSiteSupervisor_FacultyId` FOREIGN KEY (`FacultyId`) REFERENCES `faculty` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_FieldPlacementSiteSupervisor_FieldPlacementSiteId` FOREIGN KEY (`FieldPlacementSiteId`) REFERENCES `fieldplacementsite` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
