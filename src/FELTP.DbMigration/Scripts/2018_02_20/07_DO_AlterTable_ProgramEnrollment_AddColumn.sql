﻿AlTER TABLE programenrollment ADD ProgramOfferApplicationId BIGINT(20) NOT NULL;
 
AlTER TABLE programenrollment ADD CONSTRAINT `FK_ProgramEnrollment_ProgramOfferApplicationId` FOREIGN KEY (`ProgramOfferApplicationId`) REFERENCES `programofferapplication` (`Id`) 
ON DELETE NO ACTION ON UPDATE NO ACTION;