﻿START TRANSACTION;

SET SQL_SAFE_UPDATES = 0;

DELETE FROM residentdeliverable;

DELETE FROM residentfieldplacementactivity;

DELETE FROM residentmodule;

DELETE FROM residentcoursework;

DELETE FROM graduateeligibilityitem;

DELETE FROM graduate;

DELETE FROM programenrollment;

SET SQL_SAFE_UPDATES = 1;
COMMIT;