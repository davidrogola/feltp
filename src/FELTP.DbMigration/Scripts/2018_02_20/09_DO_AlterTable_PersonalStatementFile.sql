﻿AlTER TABLE personalstatementfile ADD ProgramOfferApplicationId BIGINT(20) NOT NULL;
 
AlTER TABLE personalstatementfile ADD CONSTRAINT `FK_PersonalStatementFile_ProgramOfferApplicationId` FOREIGN KEY (`ProgramOfferApplicationId`) REFERENCES `programofferapplication` (`Id`) 
ON DELETE NO ACTION ON UPDATE NO ACTION;