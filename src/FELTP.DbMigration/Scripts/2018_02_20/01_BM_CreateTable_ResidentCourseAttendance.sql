﻿CREATE TABLE `residentcourseattendance` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `CourseScheduleId` int(11) NOT NULL,
  `ResidentCourseWorkId` bigint(20) NOT NULL,
  `FacultyId` int(11) NOT NULL,
  `AttendanceDate` datetime NOT NULL,
  `DateCreated` datetime NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `DateDeactivated` datetime DEFAULT NULL,
  `DeactivatedBy` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ResidentCourseAttendance_FacultyId_idx` (`FacultyId`),
  KEY `FK_ResidentCourseAttendance_CourseScheduleId_idx` (`CourseScheduleId`),
  KEY `FK_ResidentCourseAttendance_ResidentCourseWorkId_idx` (`ResidentCourseWorkId`),
  CONSTRAINT `FK_ResidentCourseAttendance_CourseScheduleId` FOREIGN KEY (`CourseScheduleId`) REFERENCES `courseschedule` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ResidentCourseAttendance_FacultyId` FOREIGN KEY (`FacultyId`) REFERENCES `faculty` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

