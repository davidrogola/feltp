﻿CREATE TABLE `residentmoduleattendance` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ResidentModuleId` bigint(20) NOT NULL,
  `ResidentCourseAttendanceId` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ResidentModuleAttendance_ResidentModuleId_idx` (`ResidentModuleId`),
  KEY `FK_ResidentModuleAttendance_ResidentCourseAttendanceId_idx` (`ResidentCourseAttendanceId`),
  CONSTRAINT `FK_ResidentModuleAttendance_ResidentModuleId` FOREIGN KEY (`ResidentModuleId`) REFERENCES `resident` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ResidentModuleAttendance_ResidentCourseAttendanceId` FOREIGN KEY (`ResidentCourseAttendanceId`) REFERENCES `residentcourseattendance` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET=utf8;