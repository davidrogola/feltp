﻿AlTER TABLE interviewscore ADD ProgramOfferApplicationId BIGINT(20) NOT NULL;
 
AlTER TABLE interviewscore ADD CONSTRAINT `FK_InterviewScore_ProgramOfferApplicationId` FOREIGN KEY (`ProgramOfferApplicationId`) REFERENCES `programofferapplication` (`Id`) 
ON DELETE NO ACTION ON UPDATE NO ACTION;