﻿CREATE TABLE `programofferapplication` (
  `Id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `ProgramOfferId` INT(11) NOT NULL,
  `ApplicantId` BIGINT(20) NOT NULL,
  `Status` INT(11) NOT NULL,
  `DateApplied` DATETIME NOT NULL,
  `DateCreated` DATETIME NOT NULL,
  `CreatedBy` VARCHAR(100) NOT NULL,
  `DateEvaluated` DATETIME DEFAULT NULL,
  `EvaluatedBy` VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ProgramOfferApplication_ProgramOffer_idx` (`ProgramOfferId`),
  KEY `FK_ProgramOfferApplication_Applicant_idx` (`ApplicantId`),
  CONSTRAINT `FK_ProgramOfferApplication_ProgramOfferId` FOREIGN KEY (`ProgramOfferId`) REFERENCES `programoffer` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ProgramOfferApplication_ApplicantId` FOREIGN KEY (`ApplicantId`) REFERENCES `applicant` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
