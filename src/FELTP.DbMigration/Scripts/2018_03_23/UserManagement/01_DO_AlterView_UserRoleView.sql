﻿CREATE VIEW feltpusermanagement.UserRoleView AS
SELECT  ar.UserId,ar.RoleId,r.Name AS RoleName,ar.DateAddedToRole,asp.UserName as AddedToRoleBy,ar.DateRemovedFromRole,
 ar.UserRemovedFromRoleById
 FROM feltpusermanagement.aspnetuserroles ar
 INNER JOIN feltpusermanagement.aspnetusers asp ON ar.UserAddedToRoleById = asp.Id
 INNER JOIN feltpusermanagement.aspnetroles r ON ar.RoleId = r.Id
