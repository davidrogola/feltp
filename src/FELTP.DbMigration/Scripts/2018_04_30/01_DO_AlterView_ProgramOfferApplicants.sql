﻿ALTER VIEW programofferapplicants AS
SELECT po.Id,po.ApplicantId,po.ProgramOfferId,po.DateApplied,po.DateCreated, 
p.FirstName ,p.LastName,p.IdentificationNumber,c.Name as Cadre,pof.Name as ProgramOfferName, pog.Name as Program
,po.ApprovedByCounty,po.PersonalStatementSubmitted,po.EnrollmentStatus,r.RegistrationNumber,po.Comment,
po.EvaluatedBy,po.DateEvaluated,po.QualificationStatus,po.ApplicationStatus,po.DateShortListed,po.ShortListedBy
,r.Id as ResidentId,pe.Id as EnrollmentId,pog.Id as ProgramId,p.Id as PersonId
FROM programofferapplication po 
INNER JOIN applicant ap on ap.Id= po.ApplicantId
INNER JOIN person p on ap.PersonId = p.Id
LEFT JOIN  cadre c on c.Id = p.CadreId
LEFT JOIN resident r ON ap.Id = r.ApplicantId
LEFT JOIN programenrollment pe ON pe.ProgramOfferApplicationId = po.Id
LEFT JOIN programoffer pof ON pof.Id = po.ProgramOfferId
LEFT JOIN program pog ON pog.Id = pof.ProgramId
GROUP BY Id