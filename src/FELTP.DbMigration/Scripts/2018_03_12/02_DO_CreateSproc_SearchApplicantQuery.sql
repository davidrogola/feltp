﻿SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
DELIMITER //
DROP PROCEDURE IF EXISTS `search_applicant_query`;
CREATE PROCEDURE `search_applicant_query` (IN identificationNumber VARCHAR(100))
BEGIN
SELECT p.FirstName, p.LastName, p.IdentificationNumber, ap.CreatedBy,c.Name as Cadre,ap.Id as ApplicantId FROM person p
	INNER  JOIN applicant ap ON p.Id = ap.PersonId
    INNER JOIN Cadre c ON c.Id = p.CadreId
    WHERE MATCH (p.IdentificationNumber)
    AGAINST (identificationNumber IN BOOLEAN MODE) LIMIT 50;
END //
