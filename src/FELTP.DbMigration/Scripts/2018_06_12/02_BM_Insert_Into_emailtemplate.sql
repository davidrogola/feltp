﻿INSERT INTO feltpusermanagement.emailtemplate(Id,EmailType,Subject,Body)
VALUES(4, 4,
'FELTP User Email Change Request', 
'Dear @Model.Name, <br />
<p></p>
Your email address has been changed successfully to <b>@Model.Email</b>.<br />
Please click on the link below to confirm your new email address.<br />

<p></p>
Link : @Model.Link	
<p><p/>

Your New user name to login to the system is : <br />
UserName : <b>@Model.Name </b><br/>
<p></p>
Best Regards,
<p></p>
<b>KFELTP</b> Team.
')