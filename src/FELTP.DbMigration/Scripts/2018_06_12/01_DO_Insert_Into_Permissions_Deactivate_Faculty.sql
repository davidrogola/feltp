﻿INSERT INTO feltpusermanagement.permission(Id,Name,Comment,DateCreated,CreatedById)
VALUES (0,'Activate Faculty','Permission to activate an inactive faculty',NOW(),NULL),
(0,'Deactivate Faculty','Permission to deactivate a faculty',NOW(),NULL);
