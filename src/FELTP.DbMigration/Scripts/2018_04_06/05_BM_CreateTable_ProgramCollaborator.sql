﻿CREATE TABLE `programcollaborator` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProgramId` int(11) NOT NULL,
  `CollaboratorId` INT NOT NULL,
  `DateCreated` datetime NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `DateDeactivated` datetime DEFAULT NULL,
  `DeactivatedBy` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_ProgramCollaborator_ProgramId_idx` (`ProgramId`),
  KEY `FK_ProgramCollaborator_CollaboratorId_idx` (`CollaboratorId`),
  CONSTRAINT `FK_ProgramCollaborator_CollaboratorId` FOREIGN KEY (`CollaboratorId`) REFERENCES `collaborator` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ProgramCollaborator_ProgramId` FOREIGN KEY (`ProgramId`) REFERENCES `program` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;