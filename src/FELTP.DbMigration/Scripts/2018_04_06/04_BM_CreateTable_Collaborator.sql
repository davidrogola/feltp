﻿CREATE TABLE `collaborator` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(100) NOT NULL,
  `CollaboratorTypeId` INT NOT NULL,
  `DateCreated` DATETIME NOT NULL,
  `CreatedBy` VARCHAR(100) NOT NULL,
  `DateDeactivated` DATETIME NULL,
  `DeactivatedBy` VARCHAR(100) NULL,
  PRIMARY KEY (`Id`));
