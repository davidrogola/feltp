﻿SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
DELIMITER //
DROP PROCEDURE IF EXISTS `update_semester_items_oncompletion`;
CREATE PROCEDURE `update_semester_items_oncompletion` (IN semesterScheduleId INT(11),IN programOfferId INT(11), IN semesterId INT(11))
BEGIN
START TRANSACTION;
DROP TABLE IF EXISTS SemesterScheduleIds;
DROP TABLE IF EXISTS ResidentCourseWorkIds;
CREATE TEMPORARY TABLE IF NOT EXISTS SemesterScheduleIds AS (SELECT sms.Id FROM semesterschedule ss 
INNER JOIN (SELECT * FROM semesterscheduleitem) as sms ON sms.SemesterScheduleId = ss.Id
WHERE ss.Id = semesterScheduleId);

CREATE TEMPORARY TABLE IF NOT EXISTS ResidentCourseWorkIds AS (SELECT rsc.Id FROM (SELECT * FROM residentcoursework) as rsc 
INNER JOIN programenrollment pe ON pe.Id = rsc.ProgramEnrollmentId
INNER JOIN programoffer po ON po.Id = pe.ProgramOfferId
INNER JOIN programcourse pc ON pc.id = rsc.ProgramCourseId
INNER JOIN semester s ON s.Id = pc.SemesterId
where po.Id = programOfferId AND s.Id = semesterId);

UPDATE semesterscheduleitem  SET Status = 2 WHERE Id IN (SELECT * FROM SemesterScheduleIds);
  
UPDATE residentcoursework SET ProgressStatus = 2 WHERE Id IN (SELECT * FROM ResidentCourseWorkIds);


COMMIT ;
END //
