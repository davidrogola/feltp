﻿CREATE VIEW ResidentUnitActivityDeliverableView AS
SELECT ru.ResidentId,ra.ResidentFieldPlacementActivityId,ru.ProgramEnrollmentId,ru.Name as UnitName,
ra.ActivityName,rd.Id as ResidentDeliverableId, d.Name as Deliverable,d.Code,ru.ResidentUnitId,rd.Score,
s.Id as SemesterId,s.Name as Semester,rd.ProgressStatus FROM residentunitview ru
INNER JOIN residentcoursework rc on rc.Id = ru.ResidentCourseWorkId
INNER JOIN programcourse pc on pc.Id = rc.ProgramCourseId
INNER JOIN semester s on s.Id = pc.SemesterId
INNER JOIN residentactivityview ra ON ra.ResidentUnitId = ru.ResidentUnitId
INNER JOIN residentdeliverable rd ON ra.ResidentFieldPlacementActivityId = rd.ResidentFieldPlacementActivityId
INNER JOIN deliverable d ON d.Id = rd.DeliverableId;