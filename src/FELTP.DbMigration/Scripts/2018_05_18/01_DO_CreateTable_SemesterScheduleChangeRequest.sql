﻿CREATE TABLE `semesterschedulechangerequest` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `SemesterScheduleId` int(11) NOT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime NOT NULL,
  `Comment`varchar(256) NOT NULL,
  `RequestedBy`  varchar(100) NOT NULL,
  `ApprovalStatus` int(11) NOT NULL,
  `DateApproved` datetime DEFAULT NULL,
  `ApprovedBy` varchar(100) DEFAULT NULL,
  `ApprovalReason` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_SemesterScheduleChangeRequest_SemesterScheduleId_idx` (`SemesterScheduleId`),
  CONSTRAINT `FK_SemesterScheduleChangeRequest_SemesterScheduleId` FOREIGN KEY (`SemesterScheduleId`) REFERENCES `semesterschedule` (`Id`) 
  ON DELETE NO ACTION ON UPDATE NO ACTION 
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

