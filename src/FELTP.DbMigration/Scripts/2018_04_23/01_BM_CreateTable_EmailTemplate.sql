﻿CREATE TABLE feltpusermanagement.`emailtemplate` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `EmailType` INT(11) NOT NULL,
  `Subject` VARCHAR(256) NOT NULL,
  `Body` VARCHAR(1200) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
