﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Domain.Person
{
    public class Ministry
    {
        public Ministry()
        {

        }
        public Ministry(string name)
        {
            Name = name;
        }

        public int Id { get; set; }
        public string Name { get; set; }
    }
}
