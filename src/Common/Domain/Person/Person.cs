﻿using Common.Domain.Address;
using Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Common.Domain.Person
{
    public enum Gender
    {
        Male = 1,
        Female
    }

    public enum IdentificationType
    {
        Passport,
        [Display(Name = "National ID")]
        NationalID
    }

    public class Person
    {
        public Person()
        {

        }

        public Person(string firstName, string lastName, string otherName, Gender gender, int? cadre,
            DateTime dateOfBirth, string identificationNo, IdentificationType identificationType)
        {
            FirstName = firstName;
            LastName = lastName;
            OtherName = otherName;
            Gender = gender;
            CadreId = cadre;
            DateOfBirth = dateOfBirth;
            IdentificationNumber = identificationNo;
            IdentificationType = identificationType;
        }

        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OtherName { get; set; }
        public Gender Gender { get; set; }
        public int? CadreId { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string IdentificationNumber { get; set; }
        public IdentificationType IdentificationType { get; set; }
        public virtual Cadre Cadre { get; set; }
        public virtual ICollection<AcademicHistory> AcademicHistory { get; set; }
        public virtual ICollection<EmploymentHistory> EmploymentHistory { get; set; }
        public virtual ContactInformation ContactInformation { get; set; }
        public virtual AddressInformation AddressInformation { get; set; }

        public void SetCadre(int? cadreId)
        {
            CadreId = cadreId;
        }
    }
}
