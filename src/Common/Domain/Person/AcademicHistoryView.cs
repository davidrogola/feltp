﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Domain.Person
{
    public class AcademicHistoryView
    {
        public AcademicHistoryView()
        {

        }
        public int Id { get; set; }
        public long PersonId { get; set; }
        public string CourseName { get; set; }
        public string Other { get; set; }
        public bool GraduateofFELTP { get; set; }
        public bool GraduateofEIS { get; set; }
        public string OtherQualifications { get; set; }
        public string YearOfGraduation { get; set; }
        public string University { get; set; }
        public EducationLevel HighestEducationLevel { get; set; }
        public int? UniversityId { get; set; }
        public int ? UniversityCourseId { get; set; }
        public string UniversityCourse { get; set; }
    }
}
