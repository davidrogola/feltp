﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Domain.Person
{
    public class EmploymentHistory
    {
        public EmploymentHistory()
        {

        }
        public EmploymentHistory(long personId, string personnelNo, string designation,
            int durationInPosition, string employerName, string yearOfEmployment, string workstation, string currentposition
            , int? ministryId, bool isCurrentEmployer)
        {
            PersonId = personId;
            PersonnelNumber = personnelNo;
            Designation = designation;
            DurationInPosition = durationInPosition;
            EmployerName = employerName;
            YearOfEmployment = yearOfEmployment;
            WorkStation = workstation;
            CurrentPosition = currentposition;
            MinistryId = ministryId;
            IsCurrentEmployer = isCurrentEmployer;
        }

        public int Id { get; set; }
        public long PersonId { get; set; }
        public string PersonnelNumber { get; set; }
        public string Designation { get; set; }
        public int DurationInPosition { get; set; }
        public string EmployerName { get; set; }
        public string YearOfEmployment { get; set; }
        public string WorkStation { get; set; }
        public string CurrentPosition { get; set; }
        public int? MinistryId { get; set; }
        public bool IsCurrentEmployer { get; set; }
        public virtual Ministry Ministry { get; set; }

        public void SetPerson(long personId)
        {
            PersonId = personId;
        }

        public void SetEmploymentInformation(string startYear, string endYear, bool isCurrentEmployer)
        {
            YearOfEmployment = startYear;

            var durationInPostion = isCurrentEmployer ? DateTime.Today.Year - Convert.ToInt16(startYear)
                : Convert.ToInt16(endYear) - Convert.ToInt16(startYear);
            DurationInPosition = durationInPostion < 1 ? 1 : durationInPostion;

        }

    }
}
