﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Domain.Person
{
    public class Cadre
    {
        public Cadre()
        {

        }
        public Cadre(string name, string createdBy)
        {
            Name = name;
            DateCreated = DateTime.Now;
            CreatedBy = createdBy;
        }
       
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DateCreated { get; private set; }
        public string CreatedBy { get; private set; }
        public DateTime? DateDeactivated { get; private set; }
        public string DeactivatedBy { get; private set; }
        public void Deactivate(string deactivatedBy)
        {
            DeactivatedBy = deactivatedBy;
            DateDeactivated = DateTime.Now;
        }
        public void SetAuditInformation(string createdBy)
        {
            throw new NotImplementedException();
        }


    }
}
