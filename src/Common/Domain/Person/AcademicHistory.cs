﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Common.Domain.Person
{
    public enum EducationLevel
    {
        PhD,
        [Display(Name="Masters Degree")]
        MastersDegree,
        [Display(Name = "Basic or undergraduate Degree")]
        UnderGraduate,
        [Display(Name = "Higher Diploma")]
        Higherdiploma,
        Diploma,
        Certificate,
    }

    public class AcademicHistory
    {
        public AcademicHistory()
        {

        }

        public AcademicHistory(long personId, string courseName, string other,
            string graduationYear, EducationLevel educationLevel, int ? universityId,
            bool graduateOfFELTP, bool graduateOfEIS, string otherQualifications)
        {
            PersonId = personId;
            CourseName = courseName;
            Other = other;
            YearOfGraduation = graduationYear;
            HighestEducationLevel = educationLevel;
            UniversityId = universityId;
            GraduateofFELTP = graduateOfFELTP;
            GraduateofEIS = graduateOfEIS;
            OtherQualifications = otherQualifications;

        }
        public int Id { get; set; }
        public long PersonId { get; set; }
        public string CourseName { get; set; }
        public string Other { get; set; }
        public bool GraduateofFELTP { get; set; }
        public bool GraduateofEIS { get; set; }
        public string OtherQualifications { get; set; }
        public string YearOfGraduation { get; set; }
        public EducationLevel HighestEducationLevel { get; set; }
        public int ? UniversityId { get; set; }
        public int ? UniversityCourseId { get; set; }
        public virtual  University University { get; set; }
        public virtual AcademicCourse UniversityCourse { get; set; }
        public void SetPerson(long personId)
        {
            PersonId = personId;
        }
       
        public void SetUniversityInfo(int ? universityId, string universityName = null)
        {
            if (universityId.HasValue)
                UniversityId = universityId;
            else
                Other = universityName;
        }

        public void SetCourseName(int ? courseId, string courseName, EducationLevel educationLevel, string yearOfGraduation)
        {
            if (courseId.HasValue)
            {
                UniversityCourseId = courseId;
            }
            else
            {
                CourseName = courseName;
            }
            HighestEducationLevel = educationLevel;
            YearOfGraduation = yearOfGraduation;
        }
    }
}
