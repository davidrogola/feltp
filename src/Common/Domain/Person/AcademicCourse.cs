﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Domain.Person
{
    public class AcademicCourse
    {
        public AcademicCourse()
        {

        }
        public AcademicCourse(EducationLevel educationLevel, string courseName, string createdBy)
        {
            EducationLevel = educationLevel;
            CourseName = courseName;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
        }
        public int Id { get; set; }
        public EducationLevel EducationLevel { get; set; }
        public string CourseName { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
    }
}
