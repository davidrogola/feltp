﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Domain.Person
{
    public class AcademicCourseGrading
    {
        public int Id { get; set; }
        public EducationLevel EducationLevel { get; set; }
        public string GradeName { get; set; }
    }
}
