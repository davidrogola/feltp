﻿

namespace Common.Domain.Person
{
    public class ContactInformation
    {
        public ContactInformation()
        {

        }

        public ContactInformation(string emailAddress, string primaryMobileNumber, string alternateNumber, long personId)
        {
            EmailAddress = emailAddress;
            PrimaryMobileNumber = primaryMobileNumber;
            AlternateMobileNumber = alternateNumber;
            PersonId = personId;

        }

        public int Id { get; private set; }
        public string EmailAddress { get; private set; }
        public string PrimaryMobileNumber { get; private set; }
        public string AlternateMobileNumber { get; private set; }
        public long PersonId { get; private set; }

        public void SetPerson(long personId)
        {
            PersonId = personId;
        }
        public void UpdateContactInfo (string emailAddress, string primaryMobileNumber, string alternateNumber)
        {
            EmailAddress = emailAddress;
            PrimaryMobileNumber = primaryMobileNumber;
            AlternateMobileNumber = alternateNumber;

        }
    }
}
