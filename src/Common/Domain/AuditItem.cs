﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Models
{
   
    public interface IAuditItem
    {
         DateTime DateCreated { get;  }
         string CreatedBy { get;  }
         DateTime? DateDeactivated { get;  }
         string DeactivatedBy { get;  }
         void Deactivate(string deactivatedBy);
        
    }

}
