﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Models
{
    public class County
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }
        public virtual Country Country { get; set; }
    }
}
