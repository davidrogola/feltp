﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Domain.Address
{
    public class AddressInformation
    {
        public AddressInformation()
        {

        }
        public AddressInformation(int countryId, int ? countyId, int ? subcountyId,
            string postalAddress, long personId)
        {
            CountryId = countryId;
            CountyId = countyId;
            SubCountyId = subcountyId;
            PostalAddress = postalAddress;
            PersonId = personId;

        }

        public int Id { get; set; }
        public int CountryId { get; set; }
        public int ? CountyId { get; set; }
        public int ? SubCountyId { get; set; }
        public string PostalAddress { get; set; }
        public long PersonId { get; set; }
        public virtual Country Country { get; set; }
        public virtual County County { get; set; }
        public virtual SubCounty SubCounty { get; set; }

        
        public void SetPerson(long personId)
        {
            PersonId = personId;
        }

        public void UpdateAddressInfo(int countryId, int? countyId, int? subcountyId,
            string postalAddress)
        {
            CountryId = countryId;
            CountyId = countyId;
            SubCountyId = subcountyId;
            PostalAddress = postalAddress;
        }

    }
}
