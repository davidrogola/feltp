﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Domain.Messaging
{

    public enum MessageStatus
    {
        Queued,
        Sent,
        Failed
    }
    public class OutgoingMailMessage
    {
        public OutgoingMailMessage()
        {

        }

        public OutgoingMailMessage(string recipient, string subject, string body, string createdBy, string correlation)
        {
            Recipient = recipient;
            Subject = subject;
            Body = body;
            Createdy = createdBy;
            Status = MessageStatus.Queued;
            DateCreated = DateTime.Now;
            CorrelationId = correlation;
        }

        public long Id { get; set; }
        public string Recipient { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public int DeliveryAttempts { get; set; }
        public MessageStatus Status { get; set; }
        public string Createdy { get; set; }
        public DateTime DateCreated { get; set; }
        public string CorrelationId { get; set; }
        

        public void MarkAsCompleted(MessageStatus status)
        {
            Status = status;
        }

        public void AddDeliveryAttempt() => DeliveryAttempts = DeliveryAttempts + 1;

    }
}
