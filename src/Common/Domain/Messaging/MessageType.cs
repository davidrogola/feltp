﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Common.Domain.Messaging
{

    public enum MessageTypeEnum : int
    {
        PasswordReset = 1,
        AccountActivation = 2,
        ForgotPassword = 3,
        EmailChange = 4,
        ApplicantShortListed = 5,
        ApplicantEnrolled = 6,
        ApplicantSuccessfullyEvaluated = 7,
        ApplicantFailedEvaluation = 8,
        ApplicantWaitingList = 9 ,
        SubmittedDeliverable = 10,
        DeliverableEvaluated = 11
    }

    public class MessageType
    {
        public MessageType()
        {

        }

        [Key]
        public MessageTypeEnum Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
