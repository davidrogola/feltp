﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Domain.Messaging
{
    public class EmailQueue
    {
        public EmailQueue()
        {

        }
        public long OutgoingMailMessageId { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool Processed { get; set; }
        public DateTime DateCreated { get; set; }
        public string Recipient { get; set; }
        public string PeekedBy { get; set; }
        public DateTime? PeekedDate { get; set; }
        public DateTime? PeekedToDate { get; set; }
        public DateTime? ReservedDate { get; set; }
        public DateTime? ResevervedToDate { get; set; }
    }
}
