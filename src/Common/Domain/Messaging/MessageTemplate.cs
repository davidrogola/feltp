﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Domain.Messaging
{
    public class MessageTemplate
    {
        public MessageTemplate()
        {

        }

        public int Id { get; set; }
        public MessageTypeEnum MessageTypeId { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public virtual MessageType MessageType { get; set; }
    }
}
