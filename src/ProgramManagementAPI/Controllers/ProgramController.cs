﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Common.Models;
using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProgramManagement.Domain.Program;

namespace ProgramManagementAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [Authorize]
    public class ProgramController : Controller
    {
        IMediator mediator;
        public ProgramController(IMediator _mediator)
        {
            mediator = _mediator;
        }

        // GET api/Program/GetPrograms
        [HttpGet]
        public async Task<object> GetPrograms()
        {
            var programs = await mediator.Send(new GetProgramList() { });

            if (!programs.Any())
                return NotFound(new ApiResponse("Programs not found", null, false));

            return Ok(new ApiResponse("Get program request succesfully", JsonConvert.SerializeObject(programs), true));
        }

        // GET api/Program/GetProgramById/1
        [HttpGet("{id}")]
        public async Task<object> GetProgramById(int id)
        {
            var program = await mediator.Send(new GetProgramList {  Id = id });
            if (!program.Any())
                return NotFound(new ApiResponse("Program details not found", null, false));
            
            return new ApiResponse("Program details retrieved successfully", JsonConvert.SerializeObject(program.FirstOrDefault()), true);
        }

        // GET api/Program/Getallprograms
        [HttpGet]
        public async Task<object> Getallprograms()
        {
            var program = await mediator.Send(new GetProgramList());
            if (!program.Any())
                return NotFound(new ApiResponse("Program details not found", null, false));

            return new ApiResponse("Program details retrieved successfully", JsonConvert.SerializeObject(program.FirstOrDefault()), true);
        }
        [HttpGet]
        public async Task<object> GetallCourses()
        {
            var program = await mediator.Send(new GetProgramList());
            if (!program.Any())
                return NotFound(new ApiResponse("Program details not found", null, false));

            return new ApiResponse("Program details retrieved successfully", JsonConvert.SerializeObject(program.FirstOrDefault()), true);
        }

        // GET api/Program/GetProgramCourses/1
        [HttpGet("{Id}")]
        public async Task<object> GetProgramCourses(int Id)
        {
            
            var programs = await mediator.Send(new GetProgramCoursesList() { ProgramId = Id });
            if (!programs.Any())
                return NotFound(new ApiResponse("Course details not found", null, false));

            return new ApiResponse("Course details retrieved successfully", JsonConvert.SerializeObject(programs), true);
        }

        // GET api/Program/GetProgramResources/1
        [HttpGet("{Id}")]
        public async Task<object> GetProgramResources(int Id)
        {
            var programresourcefiles = await mediator.Send(new GetProgramResourcesByProgramId() { programId = Id });
            if (!programresourcefiles.Any())
                return NotFound(new ApiResponse("Resouce details not found", null, false));

            return new ApiResponse("resouce details retrieved successfully", JsonConvert.SerializeObject(programresourcefiles), true);
        }
       
        // GET api/Program/GetProgramResourcesFiles/1
        [HttpGet("{Id}")]
        public async Task<object> GetProgramResourcesFilesByResourceId(int Id)
        {
            var programresourcefiles = await mediator.Send(new GetProgramResourceFile() { resourceId = Id });
            if (!programresourcefiles.Any())
                return NotFound(new ApiResponse("Resouce Files details not found", null, false));

            return new ApiResponse("resouce files details retrieved successfully", JsonConvert.SerializeObject(programresourcefiles), true);
        }

        // GET api/Program/GetProgramResourcesFiles/1
        [HttpGet("{Id}")]
        public async Task<object> GetProgramResourcesFilesById(int Id)
        {
            var programresourcefiles = await mediator.Send(new GetProgramResourceFileById() { Id = Id });
            if (programresourcefiles== null)
                return NotFound(new ApiResponse("Resouce Files details not found", null, false));
            return Ok(new ApiResponse("source file details successfully retrieved", JsonConvert.SerializeObject(programresourcefiles), true));
        }
    }
}