﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

using Application.Common.Models;
using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using Application.ProgramManagement.QueryHandlers;
using static Application.Common.Services.ApiErrorModelBuilder;
using Microsoft.Extensions.Primitives;
using ProgramManagement.Domain.Program;

namespace ProgramManagementAPI.Controllers
{

    [Route("api/[controller]/[action]")]
    [Authorize]
    public class ProgramOfferController : Controller
    {
        IMediator mediator;
        public ProgramOfferController(IMediator _mediator)
        {
            mediator = _mediator;
        }

        // GET api/ProgramOffer/GetProgramOffers
        [HttpGet]
        public async Task<object> GetProgramOffers()
        {
            var programOffers = await mediator.Send(new GetProgramOfferList() { });

            if (!programOffers.Any())
                return NotFound(new ApiResponse("Program offers not found", null, false));

            return Ok(new ApiResponse("Program offer request processed succesfully", JsonConvert.SerializeObject(programOffers), true));
        }

        // GET api/ProgramOffer/GetProgramOfferById/1
        [HttpGet("{Id}")]
        public async Task<object> GetProgramOfferById(int Id)
        {
            var programOffer = await mediator.Send(new GetProgramOfferById { OfferId = Id });

            if (programOffer == null)
                return NotFound(new ApiResponse("Program offer details not found", null, false));

            return Ok(new ApiResponse("Program offer details successfully retrieved", JsonConvert.SerializeObject(programOffer), true));
        }
        [HttpGet("{Id}")]
        public async Task<object> GetProgramOfferEligiblityRequirements(int Id)
        {
            var eligilityRequirements = await mediator.Send(new GetProgramOfferRequirementsQuery { ProgramOfferId = Id });

            if (eligilityRequirements.GeneralRequirementsViewModel == null)
                return NotFound(new ApiResponse($"Eligiblity requirements not found for offer Id {Id}", null, false));

            return Ok(new ApiResponse("Program eligiblity details fetched successfully", JsonConvert.SerializeObject(eligilityRequirements), true));
        }
        [HttpGet("{Id}")]
        public async Task<object> GetProgramOfferLocations(int Id)
        {
            var programLocations = await mediator.Send(new GetProgramOfferLocationQuery { ProgramOfferId = Id });

            return Ok(new ApiResponse("Program offer locations fetched successfully", JsonConvert.SerializeObject(programLocations), true));
        }

        [HttpPost]
        public async Task<object> AddProgramOfferApplication([FromBody] AddNewProgramOfferApplicationCommand model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Model state validation failed", null, false, BuildModelFromModelStateErrors(ModelState.Values.SelectMany(x => x.Errors)));

            var response = await mediator.Send(model);

            return Ok(new ApiResponse(response.Message, JsonConvert.SerializeObject(response), response.Success));

        }

        [HttpPost]
        public async Task<object> AddApplicantPersonalStatementFile()
        {
            var form = HttpContext.Request.Form;
            var applicationIdExists = form.TryGetValue("ProgramOfferApplicationId", out StringValues formValues);

            if (form.Files.Count == 0 && !applicationIdExists)
                return Ok(new ApiResponse("An error occured while processing statement file upload", null, false));

            await mediator.Send(new AddPersonalStatementFileCommand
            {
                ProgramOfferApplicationId = Convert.ToInt64(formValues[0]),
                StatementFile = form.Files[0]
            });

            return Ok(new ApiResponse("Statement file processed succesfully", JsonConvert.SerializeObject(new { Completed = true, ProgramOfferApplicationId = formValues[0] }), true));
        }

        [HttpGet("{Id}")]
        public async Task<object> GetProgramOfferApplicationsByApplicantId(long Id)
        {
            var offerApplications = await mediator.Send(new GetProgramOfferApplicationByApplicantId { ApplicantId = Id });

            return Ok(offerApplications);
        }


        [HttpGet("{Id}")]
        public async Task<object> GetProgramOfferApplicationById(long Id)
        {
            var offerApplications = await mediator.Send(new GetProgramOfferApplicationbyId {  Id = Id });

            return Ok(offerApplications);
        }

        [HttpGet]
        public async Task<object> GetTimetableListByOfferId(int Id, int type, int ? semesterId)
        {
            var timetableList = await mediator.Send(new GetTimetableByProgramOfferId
            {
                Id = Id,
                SemesterId = semesterId,
                Type =(TimetableType) type
            });

            return Ok(timetableList);
        }

    }
}