﻿using Application.Common;
using Application.Common.Services;
using Application.MapperProfiles.ProgramManagement;
using Application.ProgramManagement.Queries;
using Application.ResidentManagement.Validators;
using AutoMapper;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using static Application.Common.Services.IdentityConfig;

namespace ProgramManagementAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(Configuration);

            services.AddFeltpPersistanceStores(Configuration);
            services.AddUnitOfWorkImplementations();

            services.AddMvcCore(options => {

                options.Filters.Add(typeof(WebApiCustomExceptionFilter));
            })
            .AddFluentValidation(fv =>
            {
                fv.RegisterValidatorsFromAssemblyContaining<AddNewProgramOfferApplicationCommandValidator>();
            })
            .AddAuthorization()
            .AddJsonFormatters();

            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = GetIdentityServerEndPoint(Configuration);
                    options.RequireHttpsMetadata = false;
                    options.ApiName = "ProgramManagementAPI";
                });
            services.AddAutoMapper(typeof(ProgramProfile).Assembly);
            services.AddMediatR(typeof(GetProgramList).Assembly);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
