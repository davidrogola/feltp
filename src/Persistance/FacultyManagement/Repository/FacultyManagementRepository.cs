﻿using Persistance.FacultyManagement.Database;
using Persistance.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistance.FacultyManagement.Repository
{
    public class FacultyManagementRepository : GenericRepository, IFacultyManagementRepository
    {

        public FacultyManagementRepository(FacultyManagementDbContext _facultyManagementDbContext) :
            base(_facultyManagementDbContext)
        {

        }


        public FacultyManagementDbContext FacultyManagementDbContext
        {
            get
            {
                return dbContext as FacultyManagementDbContext;
            }
        }
    }
}
