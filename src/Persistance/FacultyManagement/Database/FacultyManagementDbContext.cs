﻿using Common.Domain;
using Common.Domain.Address;
using Common.Domain.Person;
using FacultyManagement.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistance.FacultyManagement.Database
{
    public class FacultyManagementDbContext : DbContext
    {
        public FacultyManagementDbContext(DbContextOptions<FacultyManagementDbContext> dbContextOptions) :
            base(dbContextOptions)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FacultyRole>().ToTable(nameof(FacultyRole).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<Faculty>().ToTable(nameof(Faculty).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<FacultyRoleMap>().ToTable(nameof(FacultyRoleMap).ToLower()).HasKey(x => x.Id);

            modelBuilder.Entity<Person>().ToTable(nameof(Person).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<Cadre>().ToTable(nameof(Cadre).ToLower()).HasKey(x => x.Id);
        }
    }
}
