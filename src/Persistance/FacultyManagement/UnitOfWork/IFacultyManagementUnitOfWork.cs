﻿using Persistance.Generic;
using Persistance.FacultyManagement.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistance.FacultyManagement.UnitOfWork
{
    public interface IFacultyManagementUnitOfWork : IUnitOfWork
    {
        IFacultyManagementRepository FacultyManagementRepository { get; }

    }
}
