﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Persistance.FacultyManagement.Database;
using Persistance.FacultyManagement.Repository;
using Persistance.FacultyManagement.UnitOfWork;

namespace Persistance.FacultyManagement.UnitOfWork
{
    public class FacultyManagementUnitOfWork : IFacultyManagementUnitOfWork
    {
        FacultyManagementDbContext FacultyManagementDbContext;

        public FacultyManagementUnitOfWork(FacultyManagementDbContext _facultyManagementDbContext)
        {
            FacultyManagementDbContext = _facultyManagementDbContext;

        }
        public IFacultyManagementRepository FacultyManagementRepository
        {
            get
            {
                return new FacultyManagementRepository(FacultyManagementDbContext);
            }
        }

        public IDbContextTransaction BeginTransaction(IsolationLevel isolationLevel)
        {
            return FacultyManagementDbContext.Database.BeginTransaction(isolationLevel);
        }

        public void Dispose()
        {
            FacultyManagementDbContext.Dispose();
        }


        public void SaveChanges()
        {
            FacultyManagementDbContext.SaveChanges();
        }
    }
}
