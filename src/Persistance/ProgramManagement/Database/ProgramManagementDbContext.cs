﻿using Common.Models;
using Microsoft.EntityFrameworkCore;
using ProgramManagement.Domain.Course;
using ProgramManagement.Domain.FieldPlacement;
using ProgramManagement.Domain.Program;
using ProgramManagement.Domain.Event;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using ProgramManagement.Domain.Examination;
using Common.Domain.Person;

namespace Persistance.ProgramManagement.Database
{
    public class ProgramManagementDbContext : DbContext 
    {   
        public ProgramManagementDbContext(DbContextOptions<ProgramManagementDbContext> dbContextOptions) :
            base(dbContextOptions)
        {
           
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProgramTier>().ToTable(nameof(ProgramTier).ToLower())
                .HasKey(x => x.Id);
            modelBuilder.Entity<Program>().Property(x => x.ProgramTierId).IsRequired();
            modelBuilder.Entity<Program>().HasOne(d => d.ProgramTier).WithMany()
                   .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Program>().ToTable(nameof(Program).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ProgramCompetency>().Property(x => x.CompetencyId).IsRequired();
            modelBuilder.Entity<ProgramCompetency>().ToTable(nameof(ProgramCompetency).ToLower())
                .HasKey(x => x.Id);
            modelBuilder.Entity<ProgramCourse>().ToTable(nameof(ProgramCourse).ToLower()).HasKey(x => x.Id);

            modelBuilder.Entity<Competency>().ToTable(nameof(Competency).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<Deliverable>().ToTable(nameof(Deliverable).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<CompetencyDeliverable>().ToTable(nameof(CompetencyDeliverable).ToLower()).HasKey(x => x.Id);
        
            modelBuilder.Entity<Course>().ToTable(nameof(Course).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<Unit>().ToTable(nameof(Unit).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<CourseUnit>().Property(x => x.CourseId).IsRequired();
            modelBuilder.Entity<CourseUnit>().Property(x => x.UnitId).IsRequired();
            modelBuilder.Entity<CourseUnit>().ToTable(nameof(CourseUnit).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<CourseType>().ToTable(nameof(CourseType).ToLower()).HasKey(x => x.Id);

            modelBuilder.Entity<FieldPlacementSite>().ToTable(nameof(FieldPlacementSite).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<FieldPlacementSiteSupervisor>().ToTable(nameof(FieldPlacementSiteSupervisor).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<FieldPlacementSiteSupervisor>().Property(x => x.FacultyId).IsRequired();
            modelBuilder.Entity<FieldPlacementSiteSupervisor>().Property(x => x.FieldPlacementSiteId).IsRequired();

            modelBuilder.Entity<FieldPlacementActivity>().ToTable(nameof(FieldPlacementActivity).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<UnitFieldPlacementActivity>().ToTable(nameof(UnitFieldPlacementActivity).ToLower())
                .HasKey(x => x.Id);
            modelBuilder.Entity<FieldPlacementActivityDeliverable>().ToTable(nameof(FieldPlacementActivityDeliverable)
                .ToLower()).HasKey(x => x.Id);
            
            modelBuilder.Entity<Semester>().ToTable(nameof(Semester).ToLower()).HasKey(x => x.Id);

            modelBuilder.Entity<ResourceType>().ToTable(nameof(ResourceType).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<Resource>().ToTable(nameof(Resource).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ProgramResource>().ToTable(nameof(ProgramResource).ToLower()).HasKey(x => x.Id);           

            modelBuilder.Entity<ProgramOffer>().ToTable(nameof(ProgramOffer).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ProgramOfferApplication>().ToTable(nameof(ProgramOfferApplication)
                .ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ProgramOfferApplicants>().ToTable(nameof(ProgramOfferApplicants));
            modelBuilder.Entity<ProgramOfferLocation>().ToTable(nameof(ProgramOfferLocation).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ProgramLocation>().ToTable(nameof(ProgramLocation).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<Country>().ToTable(nameof(Country).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<County>().ToTable(nameof(County).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<SubCounty>().ToTable(nameof(SubCounty).ToLower()).HasKey(x => x.Id);

            modelBuilder.Entity<ProgramEnrollment>().ToTable(nameof(ProgramEnrollment).ToLower()).HasKey(x => x.Id);

           modelBuilder.Entity<GraduationRequirement>().ToTable(nameof(GraduationRequirement)).HasKey(x => x.Id);

            modelBuilder.Entity<CourseSchedule>().ToTable(nameof(CourseSchedule).ToLower())
               .HasKey(x => x.Id);

            modelBuilder.Entity<Category>().ToTable(nameof(Category).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<Event>().ToTable(nameof(Event).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<EventSchedule>().ToTable(nameof(EventSchedule).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ProgramEvent>().ToTable(nameof(ProgramEvent).ToLower()).HasKey(x => x.Id);

            modelBuilder.Entity<SemesterSchedule>().ToTable(nameof(SemesterSchedule).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<SemesterScheduleItem>().ToTable(nameof(SemesterScheduleItem).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<Examination>().ToTable(nameof(Examination).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<Timetable>().ToTable(nameof(Timetable).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ExaminationParticipation>().ToTable(nameof(ExaminationParticipation).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ExaminationScore>().ToTable(nameof(ExaminationScore).ToLower()).HasKey(x => x.Id);
   
            modelBuilder.Entity<Collaborator>().ToTable(nameof(Collaborator).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ProgramCollaborator>().ToTable(nameof(ProgramCollaborator).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<SemesterScheduleChangeRequest>().ToTable(nameof(SemesterScheduleChangeRequest).ToLower()).HasKey(x => x.Id);

            modelBuilder.Entity<ProgramOfferAcademicRequirement>().ToTable(nameof(ProgramOfferAcademicRequirement).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ProgramOfferGeneralRequirement>().ToTable(nameof(ProgramOfferGeneralRequirement).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<AcademicCourse>().ToTable(nameof(AcademicCourse).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<AcademicCourseGrading>().ToTable(nameof(AcademicCourseGrading).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ProgramCompletionRequirement>().ToTable(nameof(ProgramCompletionRequirement).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ResourceFile>().ToTable(nameof(ResourceFile).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ProgramResourceFilesView>().ToTable(nameof(ProgramResourceFilesView).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ProgramResourceView>().ToTable(nameof(ProgramResourceView).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<FieldPlacementSiteEvaluation>().ToTable(nameof(FieldPlacementSiteEvaluation).ToLower()).HasKey(x => x.Id);

        }

    }
}
