﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Persistance.ProgramManagement.Database;
using Persistance.ProgramManagement.Repository;

namespace Persistance.ProgramManagement.UnitOfWork
{
    public class ProgramManagementUnitOfWork : IProgramManagementUnitOfWork
    {
        ProgramManagementDbContext programManagementDbContext;

        public ProgramManagementUnitOfWork(ProgramManagementDbContext _programManagementDbContext)
        {
            programManagementDbContext = _programManagementDbContext;

        }
        public IProgramManagementRepository ProgramManagementRepository
        {
            get
            {
                return new ProgramManagementRepository(programManagementDbContext);
            }
        }

        public IDbContextTransaction BeginTransaction(IsolationLevel isolationLevel)
        {
            return programManagementDbContext.Database.BeginTransaction(isolationLevel);
        }

        public void Dispose()
        {
            programManagementDbContext.Dispose();
        }


        public void SaveChanges()
        {
            programManagementDbContext.SaveChanges();
        }
    }
}
