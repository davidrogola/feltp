﻿using Persistance.Generic;
using Persistance.FacultyManagement.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using Persistance.ProgramManagement.Repository;

namespace Persistance.ProgramManagement.UnitOfWork
{
    public interface IProgramManagementUnitOfWork : IUnitOfWork
    {
        IProgramManagementRepository ProgramManagementRepository { get; }

    }
}
