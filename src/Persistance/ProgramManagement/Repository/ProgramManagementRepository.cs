﻿using Microsoft.EntityFrameworkCore;
using Persistance.Generic;
using Persistance.ProgramManagement.Database;
using ProgramManagement.Domain.Course;
using ProgramManagement.Domain.FieldPlacement;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Persistance.ProgramManagement.Repository
{
    public class ProgramManagementRepository : GenericRepository, IProgramManagementRepository
    {

        public ProgramManagementRepository(ProgramManagementDbContext _programManagementDbContext) :
            base(_programManagementDbContext)
        {

        }

    }
}
