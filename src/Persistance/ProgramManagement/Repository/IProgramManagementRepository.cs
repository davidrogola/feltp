﻿using Persistance.Generic;
using ProgramManagement.Domain.Course;
using ProgramManagement.Domain.FieldPlacement;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Persistance.ProgramManagement.Repository
{
    public interface IProgramManagementRepository : IRepository
    {
    }
}
