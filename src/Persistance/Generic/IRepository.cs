﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Persistance.Generic
{
    public interface IRepository
    {
        void Add<TEntity>(TEntity entity) where TEntity : class;

        void AddRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class;
        void Update<TEntity>(TEntity entity) where TEntity : class;

        TEntity Get<TEntity>(object Id) where TEntity : class;

        IEnumerable<TEntity> GetAll<TEntity>() where TEntity : class;

        void Remove<TEntity>(TEntity entity) where TEntity : class;

        void RemoveRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class;

        IEnumerable<TEntity> FindBy<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class;

        void FromSql<TEntity>(RawSqlString sqlString, params object [] parameters) where TEntity : class;
        int ExecuteSql<TEntity>(string sql, params object[] parameters) where TEntity : class;

        IEnumerable<TEntity> FindByWithInclude<TEntity>(Expression<Func<TEntity, bool>> predicate = null,
           params Expression<Func<TEntity, object>>[] includes) where TEntity : class;

    }
}
