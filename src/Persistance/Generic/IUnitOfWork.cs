﻿using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace Persistance.Generic
{
    public interface IUnitOfWork : IDisposable
    {
        void SaveChanges();

        IDbContextTransaction BeginTransaction(IsolationLevel  isolationLevel);
        
    }
}
