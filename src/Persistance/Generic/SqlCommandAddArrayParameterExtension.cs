﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;

namespace Persistance.Generic
{
    public static class SqlCommandAddArrayParameterExtension
    {
        public static MySqlParameter[] AddArrayParameters<T> (this MySqlCommand command,string paramNameRoot,IEnumerable<T> values)
        {
            var parameterNames = new List<string>();
            var parameters = new List<MySqlParameter>();
            var paramNo = 1;

            foreach (var value in values)
            {
                var paramName = string.Format("@{0}{1}",paramNameRoot,paramNo++);
                parameterNames.Add(paramName);
                MySqlParameter parameter = new MySqlParameter(paramName, value);
                command.Parameters.Add(parameter);
                parameters.Add(parameter);
            }
            command.CommandText = command.CommandText.Replace("{" + paramNameRoot + "}", string.Join(",", parameterNames));
            return parameters.ToArray();
        }
    }
}
