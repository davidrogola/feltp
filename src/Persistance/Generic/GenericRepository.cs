﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Persistance.Generic
{
    public abstract class GenericRepository : IRepository
    {
        protected readonly DbContext dbContext;
        public GenericRepository(DbContext _dbContext)
        {
            dbContext = _dbContext;
        }
        public void Add<TEntity>(TEntity entity) where TEntity : class
        {
            dbContext.Set<TEntity>().Add(entity);
        }

        public void AddRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            dbContext.Set<TEntity>().AddRange(entities);
        }

        public int ExecuteSql<TEntity>(string sql, params object[] parameters) where TEntity : class
        {
            return dbContext.Database.ExecuteSqlCommand(sql, parameters);
        }

        public IEnumerable<TEntity> FindBy<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            return dbContext.Set<TEntity>().AsNoTracking().Where(predicate).ToList();
        }

        public TEntity Get<TEntity>(object Id) where TEntity : class
        {
            return dbContext.Set<TEntity>().Find(Id);
        }

        public IEnumerable<TEntity> GetAll<TEntity>() where TEntity : class
        {
            return dbContext.Set<TEntity>().AsNoTracking().ToList();
        }

        public void Remove<TEntity>(TEntity entity) where TEntity : class
        {
            dbContext.Set<TEntity>().Remove(entity);
        }

        public void RemoveRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            dbContext.Set<TEntity>().RemoveRange(entities);
        }

        public  IEnumerable<TEntity> FindByWithInclude<TEntity>(Expression<Func<TEntity, bool>> predicate=null,
            params Expression<Func<TEntity, object>>[] includes) where TEntity : class
        {
            var query = dbContext.Set<TEntity>().AsNoTracking().AsQueryable();

            if (predicate != null)
                query = query.Where(predicate);

            if (includes != null)
                query = includes.Aggregate(query,
                          (current, include) => current.Include(include));

            return query;
        }

        public void Update<TEntity>(TEntity entity) where TEntity : class
        {
            dbContext.Attach(entity);
            dbContext.Entry(entity).State = EntityState.Modified;            
        }

        public void FromSql<TEntity>(RawSqlString sqlString, params object[] parameters) where TEntity : class
        {
            dbContext.Set<TEntity>().FromSql(sqlString, parameters);
        }
    }
}
