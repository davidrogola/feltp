﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Persistance.ResidentManagement.Database;
using Persistance.ResidentManagement.Repository;

namespace Persistance.ResidentManagement.UnitOfWork
{
    public class ResidentManagementUnitOfWork : IResidentManagementUnitOfWork
    {
        ResidentManagementDbContext residentManagementDbContext;
        public ResidentManagementUnitOfWork(ResidentManagementDbContext dbContext)
        {
            residentManagementDbContext = dbContext;
        }
        public IResidentManagementRepository ResidentManagementRepository
        {
            get
            {
                return new ResidentManagementRepository(residentManagementDbContext);
            }
        }

        public void SaveChanges()
        {
            residentManagementDbContext.SaveChanges();
        }

        public void Dispose()
        {
            residentManagementDbContext.Dispose();
        }

        public IDbContextTransaction BeginTransaction(IsolationLevel isolationLevel)
        {
            return residentManagementDbContext.Database.BeginTransaction(isolationLevel);
        }
    }
}
