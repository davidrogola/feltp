﻿using Persistance.Generic;
using Persistance.ResidentManagement.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistance.ResidentManagement.UnitOfWork
{
    public interface IResidentManagementUnitOfWork : IUnitOfWork
    {
        IResidentManagementRepository ResidentManagementRepository { get; }
    }
}
