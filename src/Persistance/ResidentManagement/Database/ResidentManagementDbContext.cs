﻿using Common.Domain;
using Common.Domain.Address;
using Common.Domain.Person;
using Microsoft.EntityFrameworkCore;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;
using ResidentManagement.Domain.Ethics;

namespace Persistance.ResidentManagement.Database
{
    public class ResidentManagementDbContext : DbContext
    {
        public ResidentManagementDbContext(DbContextOptions<ResidentManagementDbContext> options) :
            base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>().ToTable(nameof(Person).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<Cadre>().ToTable(nameof(Cadre).ToLower()).HasKey(x => x.Id);
           
            modelBuilder.Entity<Applicant>().ToTable(nameof(Applicant).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ProgramEnrollment>().ToTable(nameof(ProgramEnrollment).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<Resident>().ToTable(nameof(Resident).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<InterviewScore>().ToTable(nameof(InterviewScore).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<PersonalStatementFile>().ToTable(nameof(PersonalStatementFile).ToLower()).HasKey(x => x.Id);

            modelBuilder.Entity<ResidentCourseWork>().ToTable(nameof(ResidentCourseWork).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ResidentDeliverable>().ToTable(nameof(ResidentDeliverable).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ResidentFieldPlacementActivity>().ToTable(nameof(ResidentFieldPlacementActivity)
                .ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ResidentUnit>().ToTable(nameof(ResidentUnit).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<Graduate>().ToTable(nameof(Graduate).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<GraduateEligibilityItem>().ToTable(nameof(GraduateEligibilityItem).ToLower())
                .HasKey(x => x.Id);

            modelBuilder.Entity<ResidentUnitAttendance>().ToTable(nameof(ResidentUnitAttendance).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ResidentUnitAttendanceView>().ToTable(nameof(ResidentUnitAttendanceView).ToLower()).HasKey(x => x.Id);

            modelBuilder.Entity<FieldPlacement>().ToTable(nameof(FieldPlacement).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ResidentDeliverableItem>().ToTable(nameof(ResidentDeliverableItem).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ResidentFieldPlacement>().ToTable(nameof(ResidentFieldPlacement).ToLower()).HasKey(x => x.Id);

            modelBuilder.Entity<ResidentDeliverableView>().ToTable(nameof(ResidentDeliverableView).ToLower()).HasKey(x => x.ResidentId);

            modelBuilder.Entity<EthicsApproval>().ToTable(nameof(EthicsApproval).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<EthicsApprovalCommittee>().ToTable(nameof(EthicsApprovalCommittee).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<EthicsApprovalDocument>().ToTable(nameof(EthicsApprovalDocument).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<EthicsApprovalItem>().ToTable(nameof(EthicsApprovalItem).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ResidentDeliverableCoauthor>().ToTable(nameof(ResidentDeliverableCoauthor).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<EthicsApprovalView>().ToTable(nameof(EthicsApprovalView).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ResidentUnitView>().ToTable(nameof(ResidentUnitView).ToLower()).HasKey(x => x.ResidentUnitId);
            modelBuilder.Entity<ResidentCatScoreView>().ToTable(nameof(ResidentCatScoreView).ToLower()).HasKey(x => x.ResidentUnitId);
            modelBuilder.Entity<ResidentExaminationView>().ToTable(nameof(ResidentExaminationView).ToLower())
                .HasKey(x => x.ResidentCourseWorkId);
            modelBuilder.Entity<ResidentUnitActivityDeliverableView>().ToTable(nameof(ResidentUnitActivityDeliverableView).ToLower())
                .HasKey(x => x.ResidentUnitId);
            modelBuilder.Entity<ResidentFieldPlacementActivityView>().ToTable(nameof(ResidentFieldPlacementActivityView).ToLower())
              .HasKey(x => x.Id);

            modelBuilder.Entity<ResidentGraduationInfoView>().ToTable(nameof(ResidentGraduationInfoView).ToLower())
            .HasKey(x => x.Id);
            modelBuilder.Entity<GraduationEligibityItemsView>().ToTable(nameof(GraduationEligibityItemsView).ToLower())
           .HasKey(x => x.Id);

        }
    }
}
