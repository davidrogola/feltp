﻿using System;
using System.Collections.Generic;
using System.Text;
using Persistance.Generic;
using Persistance.ResidentManagement.Database;


namespace Persistance.ResidentManagement.Repository
{
    public class ResidentManagementRepository : GenericRepository, IResidentManagementRepository
    {
        public ResidentManagementRepository(ResidentManagementDbContext dbContext) :
            base(dbContext)
        {

        }

        public ResidentManagementDbContext ResidentManagementDbContext
        {
            get { return dbContext as ResidentManagementDbContext; }
        }

    }
}
