﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Persistance.Generic;
using Persistance.LookUpRepo.Database;
using Persistance.LookUpRepo.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Persistance.LookUpRepo.UnitOfWork
{
    public class LookUpUnitOfWork : ILookUpUnitOfWork
    {
         LookUpDbContext dbContext;

        public LookUpUnitOfWork(LookUpDbContext _dbContext)
        {
            dbContext = _dbContext;
        }
        public ILookUpRepository LookUpRepository
        {
            get
            {
                return new LookUpRepository(dbContext);
            }
        }

        public IDbContextTransaction BeginTransaction(IsolationLevel isolationLevel)
        {
            return dbContext.Database.BeginTransaction(isolationLevel);
        }

        public void Dispose()
        {
            dbContext.Dispose();
        }

        public void SaveChanges()
        {
            dbContext.SaveChanges();
        }
    }
}
