﻿using Persistance.Generic;
using Persistance.LookUpRepo.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistance.LookUpRepo.UnitOfWork
{
    public interface ILookUpUnitOfWork : IUnitOfWork 
    {
        ILookUpRepository LookUpRepository { get; }
    }
}
