﻿using Common.Domain.Address;
using Common.Domain.Messaging;
using Common.Domain.Person;
using Common.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistance.LookUpRepo.Database
{
    public class LookUpDbContext : DbContext
    {
        public LookUpDbContext(DbContextOptions<LookUpDbContext> dbContextOptions) :
            base(dbContextOptions)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>().ToTable(nameof(Person).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<AddressInformation>().ToTable(nameof(AddressInformation).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<AcademicHistory>().ToTable(nameof(AcademicHistory).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<AcademicHistoryView>().ToTable(nameof(AcademicHistoryView).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<ContactInformation>().ToTable(nameof(ContactInformation).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<EmploymentHistory>().ToTable(nameof(EmploymentHistory).ToLower()).HasKey(x => x.Id);

            modelBuilder.Entity<Country>().ToTable(nameof(Country).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<County>().ToTable(nameof(County).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<SubCounty>().ToTable(nameof(SubCounty).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<University>().ToTable(nameof(University).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<Ministry>().ToTable(nameof(Ministry).ToLower()).HasKey(x => x.Id);

            modelBuilder.Entity<MessageTemplate>().ToTable(nameof(MessageTemplate).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<OutgoingMailMessage>().ToTable(nameof(OutgoingMailMessage).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<MessageType>().ToTable(nameof(MessageType).ToLower()).HasKey(x => x.Id);
            modelBuilder.Entity<EmailQueue>().ToTable(nameof(EmailQueue).ToLower()).HasKey(x => x.OutgoingMailMessageId);




        }
    }
}