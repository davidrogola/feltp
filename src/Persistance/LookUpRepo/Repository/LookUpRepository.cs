﻿using Persistance.Generic;
using Persistance.LookUpRepo.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistance.LookUpRepo.Repository
{
   public  class LookUpRepository : GenericRepository, ILookUpRepository
    {
        public LookUpRepository(LookUpDbContext lookUpDbContext): base(lookUpDbContext)
        {

        }

        public LookUpDbContext LookUpDbContext
        {
            get
            {
                return dbContext as LookUpDbContext;
            }
        }


    }
}
