﻿using Application.ProgramManagement.CommandHandlers;
using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Validators;
using AutoMapper;
using FELTP.UnitTests.TestHelpers;
using FluentValidation;
using MediatR;
using Moq;
using NUnit.Framework;
using Persistance.ProgramManagement.Database;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FELTP.UnitTests.Features.ProgramManagement
{
    [TestFixture]
    public class AddProgramCourseCommandHandlerTests
    {
        private IProgramManagementUnitOfWork unitOfWork;
        private IMapper mapper;
        private IValidator<AddProgramCourseCommand> addProgramCourseCommandValidator;
        private IMediator mediator;

        [SetUp]
        public void SetUp()
        {
            var inMemoryDbConfiguration = new ProgramManagementDbContextSetUp();
            var progManagemntDb = new ProgramManagementDbContext(inMemoryDbConfiguration.InitializeOptions("ProgramManagemntDb"));

            unitOfWork = new Mock<ProgramManagementUnitOfWork>(progManagemntDb).Object;

            SetUpProgramData setUpProgram = new SetUpProgramData(unitOfWork);
            setUpProgram.AddProgramTier();
            setUpProgram.AddPrograms();

            setUpProgram.AddProgramCourse();
            var mediatorMock = new Mock<IMediator>();
            mediator = mediatorMock.Object;
            var mapperMock = new Mock<IMapper>();

            var programCourse = new ProgramCourse(2, 2, 1, 5, "David");

            mapperMock.Setup(mapper => mapper.Map<ProgramCourse>(It.IsAny<AddProgramCourseCommand>()))
                .Returns(programCourse);

            mapper = mapperMock.Object;
            addProgramCourseCommandValidator = new AddProgramCourseCommandValidator(unitOfWork);
        }

        [Test]
        public async Task Add_Program_Course_Returns_Valid_Program_CourseId()
        {
            var handler = new AddProgramCourseCommandHandler(unitOfWork, mapper, mediator);
            AddProgramCourseCommand command = new AddProgramCourseCommand()
            {
                CourseId = 1,
                CreatedBy = "David",
                DateCreated = DateTime.Now,
                DurationInWeeks = 10,
                ProgramId = 1,
                SemesterId = 1
            };

            int programCourseId = await handler.Handle(command, CancellationToken.None);
            Assert.NotZero(programCourseId);
        }

        [Test]
        public void Adding_Program_Course_For_Semester_Fails_When_A_Mapping_Already_Exists()
        {
            //duplicate program to course mapping for the same semester

            AddProgramCourseCommand command = new AddProgramCourseCommand()
            {
                CourseId = 1,
                CreatedBy = "David",
                DateCreated = DateTime.Now,
                DurationInWeeks = 10,
                ProgramId = 1,
                SemesterId = 1
            };
            Assert.False(addProgramCourseCommandValidator.Validate(command).IsValid);

        }

        [Test]
        public void Adding_Program_Course_Fails_When_Program_Not_Specified()
        {
            AddProgramCourseCommand command = new AddProgramCourseCommand()
            {
                CourseId = 1,
                CreatedBy = "David",
                DateCreated = DateTime.Now,
                DurationInWeeks = 10,
                ProgramId = 0, //default value for the dropdown
                SemesterId = 1
            };
            Assert.False(addProgramCourseCommandValidator.Validate(command).IsValid);

        }
        [Test]
        public void Adding_Program_Course_Fails_When_Course_Not_Specified()
        {
            AddProgramCourseCommand command = new AddProgramCourseCommand()
            {
                CourseId = 0,
                CreatedBy = "David",
                DateCreated = DateTime.Now,
                DurationInWeeks = 10,
                ProgramId = 1, //default value for the dropdown
                SemesterId = 1
            };
            Assert.False(addProgramCourseCommandValidator.Validate(command).IsValid);

        }
        [Test]
        public void Adding_Program_Course_Fails_When_Semester_Not_Specified()
        {
            AddProgramCourseCommand command = new AddProgramCourseCommand()
            {
                CourseId = 1,
                CreatedBy = "David",
                DateCreated = DateTime.Now,
                DurationInWeeks = 10,
                ProgramId = 1,
                SemesterId = 0
            };
            Assert.False(addProgramCourseCommandValidator.Validate(command).IsValid);

        }





    }
}
