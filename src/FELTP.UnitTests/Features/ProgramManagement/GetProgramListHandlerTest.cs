﻿using Application.ProgramManagement.QueryHandlers;
using MediatR;
using Moq;
using NUnit.Framework;
using Persistance.ProgramManagement.UnitOfWork;
using Application.ProgramManagement.Queries;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Persistance.ProgramManagement.Repository;
using Microsoft.EntityFrameworkCore;
using Persistance.ProgramManagement.Database;
using ProgramManagement.Domain.Program;
using AutoMapper;
using System.Linq;
using FELTP.UnitTests.TestHelpers;

namespace FELTP.UnitTests.Features.ProgramManagement
{
    [TestFixture]
    public class GetProgramListHandlerTest
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        List<ProgramViewModel> programViewModel;

        [SetUp]
        protected void SetUp()
        {

            var inMemoryDbConfiguration = new ProgramManagementDbContextSetUp();
            var progManagemntDb = new ProgramManagementDbContext(inMemoryDbConfiguration.InitializeOptions("ProgramManagemntDb"));

            unitOfWork = new Mock<ProgramManagementUnitOfWork>(progManagemntDb).Object;

            SetUpProgramData setUpProgram = new SetUpProgramData(unitOfWork);

            setUpProgram.AddProgramTier();
            setUpProgram.AddPrograms();
            
            programViewModel = CreateMap(unitOfWork);

            var mapperMock = new Mock<IMapper>();

            mapperMock.Setup(x => x.Map<List<ProgramViewModel>>(It.IsAny<List<Program>>()))
                  .Returns(programViewModel);

            mapper = mapperMock.Object;

        }

        [Test]
        public async Task GetProgramList_Handler_Returns_Correct_ProgramViewModel()
        {
            var handler = new GetProgramListQueryHandler(unitOfWork, mapper);

            var result = await handler.Handle(new GetProgramList { }, CancellationToken.None);

            Assert.IsInstanceOf<List<ProgramViewModel>>(result);
        }

        [Test]
        public async Task GetProgramList_Query_Returns_ProgramListCollection()
        {
            var handler = new GetProgramListQueryHandler(unitOfWork, mapper);

            var result = await handler.Handle(new GetProgramList { }, CancellationToken.None);
            Assert.IsTrue(result.Any());
        }


        private List<ProgramViewModel> CreateMap(IProgramManagementUnitOfWork unitOfWork)
        {
            var programList = unitOfWork.ProgramManagementRepository.GetAll<Program>().Select(
                 x => 
                 new ProgramViewModel()
                 {
                     Id = x.Id,
                     Name = x.Name,
                     CreatedBy = x.CreatedBy,
                     Code = x.Code,
                     DateCreated =
                     x.DateCreated.ToString(),
                     Description = x.Description,
                     Duration = x.Duration.ToString(),
                     HasMonthlySeminors = x.HasMonthlySeminars,
                     ProgramTier = x.ProgramTier == null ? String.Empty : x.ProgramTier.Name,
                     ProgramTierId = x.ProgramTierId
                 }).ToList();

            return programList;
        }
    }
}
