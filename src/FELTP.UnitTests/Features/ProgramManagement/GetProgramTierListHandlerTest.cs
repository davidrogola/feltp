﻿using Application.ProgramManagement.Queries;
using Application.ProgramManagement.QueryHandlers;
using AutoMapper;
using FELTP.UnitTests.TestHelpers;
using Moq;
using NUnit.Framework;
using Persistance.ProgramManagement.Database;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FELTP.UnitTests.Features.ProgramManagement
{
    [TestFixture]
    public class GetProgramTierListHandlerTest
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        List<ProgramTierViewModel> programTierViewModel;
            
        [SetUp]
        public void SetUp()
        {
            var inMemoryDbConfiguration = new ProgramManagementDbContextSetUp();
            var progManagemntDb = new ProgramManagementDbContext(inMemoryDbConfiguration.InitializeOptions("ProgramManagemntDb"));

            unitOfWork = new Mock<ProgramManagementUnitOfWork>(progManagemntDb).Object;

            SetUpProgramData setUpProgram = new SetUpProgramData(unitOfWork);
            setUpProgram.AddProgramTier();

            var mapperMock = new Mock<IMapper>();

            programTierViewModel = CreateMap(unitOfWork);

            mapperMock.Setup(x => x.Map<List<ProgramTierViewModel>>(It.IsAny<List<ProgramTier>>()))
                  .Returns(programTierViewModel);

            mapper = mapperMock.Object;

        }

        
        [Test]
        public async Task GetProgramTierList_Handler_Returns_Correct_ProgramTierViewModel()
        {
            var handler = new GetProgramTierListQueryHandler(unitOfWork, mapper);

            var result = await handler.Handle(new GetProgramTierList { }, CancellationToken.None);

            Assert.NotNull(result);
        }

        [Test]
        public async Task GetProgramListTier_Query_Returns_ProgramListCollection()
        {
            var handler = new GetProgramTierListQueryHandler(unitOfWork, mapper);

            var result = await handler.Handle(new GetProgramTierList { }, CancellationToken.None);
            Assert.AreEqual(result, programTierViewModel);
        }


        private List<ProgramTierViewModel> CreateMap(IProgramManagementUnitOfWork unitOfWork)
        {
            var programTiers = unitOfWork.ProgramManagementRepository.GetAll<ProgramTier>()
                   .Select(x => new ProgramTierViewModel
                   {
                       Id = x.Id,
                       Name = x.Name
                   }).ToList();

            return programTiers;
        }

    }
}
