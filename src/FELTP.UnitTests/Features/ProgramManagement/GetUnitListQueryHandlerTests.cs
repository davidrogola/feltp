﻿using Application.ProgramManagement.Queries;
using Application.ProgramManagement.QueryHandlers;
using AutoMapper;
using FELTP.UnitTests.TestHelpers;
using Moq;
using NUnit.Framework;
using Persistance.ProgramManagement.Database;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FELTP.UnitTests.Features.ProgramManagement
{
    [TestFixture]
    public class GetUnitListQueryHandlerTests
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        List<UnitViewModel> viewModel;
        [SetUp]
        public void SetUp()
        {
            var inMemoryDbConfiguration = new ProgramManagementDbContextSetUp();
            var progManagemntDb = new ProgramManagementDbContext(inMemoryDbConfiguration.InitializeOptions("ProgramManagemntDb"));

            unitOfWork = new Mock<ProgramManagementUnitOfWork>(progManagemntDb).Object;

            SetUpProgramData setUpProgram = new SetUpProgramData(unitOfWork);
            setUpProgram.AddUnits();

            viewModel = CreateMap(unitOfWork);

            var mapperMock = new Mock<IMapper>();
            mapperMock.Setup(x => x.Map<List<UnitViewModel>>(It.IsAny<List<Unit>>())).Returns(viewModel);

            mapper = mapperMock.Object;

        }

        [Test]
        public async Task Get_Unit_List_Handler_Returns_Correct_ViewModel()
        {
            var handler = new GetUnitListQueryHandler(unitOfWork, mapper);
            var result = await handler.Handle(new GetUnitList { }, CancellationToken.None);

            Assert.IsInstanceOf<List<UnitViewModel>>(result);

        }
        [Test]
        public async Task Get_Unit_Query_Returns_Valid_Unit_ListCollection()
        {
            var handler = new GetUnitListQueryHandler(unitOfWork, mapper);
            var result = await handler.Handle(new GetUnitList { }, CancellationToken.None);

            Assert.IsTrue(result.Any());
        }
        

            List<UnitViewModel> CreateMap(IProgramManagementUnitOfWork unitOfWork)
        {
            var units = unitOfWork.ProgramManagementRepository.GetAll<Unit>()
                .Select(x => new UnitViewModel
                {
                    Id = x.Id,
                    Code = x.Code,
                    CreatedBy = x.CreatedBy,
                    DateCreated = x.DateCreated.ToString(),
                    Name = x.Name,
                    DateDeactivated = x.DateDeactivated,
                    DeactivatedBy = x.DeactivatedBy
                }).ToList();
            return units;
        }
    }
}
