﻿using Application.Common;
using Application.ProgramManagement.CommandHandlers;
using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Validators;
using AutoMapper;
using FELTP.UnitTests.TestHelpers;
using FluentValidation;
using Moq;
using NUnit.Framework;
using Persistance.ProgramManagement.Database;
using Persistance.ProgramManagement.Repository;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FELTP.UnitTests.Features.ProgramManagement
{
    [TestFixture]
    public class AddProgramCommandHandlerTests
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        IValidator<AddProgramCommand> addProgramCommandValidator;
        ICodeGenerator codeGenerator;

        [SetUp]
        public void SetUp()
        {
            var inMemoryDbConfiguration = new ProgramManagementDbContextSetUp();
            var progManagemntDb = new ProgramManagementDbContext(inMemoryDbConfiguration.InitializeOptions("ProgramManagemntDb"));

            unitOfWork = new Mock<ProgramManagementUnitOfWork>(progManagemntDb).Object;

            SetUpProgramData setUpProgram = new SetUpProgramData(unitOfWork);
            setUpProgram.AddProgramTier();
            setUpProgram.AddPrograms();

            var mapperMock = new Mock<IMapper>();

            var program = new Program("Advanced FELTP", 3, "C002", "Test Program", 12, true, "System");

            mapperMock.Setup(x => x.Map<Program>(It.IsAny<AddProgramCommand>()))
                  .Returns(program);

            mapper = mapperMock.Object;

            var mockGenerator = new Mock<ICodeGenerator>();
            mockGenerator.Setup(x => x.Generate(It.IsAny<string>())).Returns("PRGM-001");
            codeGenerator = mockGenerator.Object;

            addProgramCommandValidator = new AddProgramCommandValidator(unitOfWork);

        }

        [Test]
        public async Task AddProgram_Returns_Valid_ProgramId()
        {
            var addProgramCommand = new AddProgramCommand()
            {
                Code = "C002",
                CreatedBy = "System",
                DateCreated = DateTime.Now,
                Description = "Test Program",
                DurationInMonths = 12,
                ProgramTierId = 3,
                HasMonthlySeminors = true,
                Name = "Advanced FELTP"
            };

            var handler = new AddProgramCommandHandler(unitOfWork, mapper,codeGenerator);

            int programId = await handler.Handle(addProgramCommand, CancellationToken.None);

            Assert.NotZero(programId);

        }
        [Test]
        public void Add_Program_With_Duplicate_Program_Name_Fails()
        {
            var addProgramCommand = new AddProgramCommand()
            {
                Code = "C002",
                CreatedBy = "System",
                DateCreated = DateTime.Now,
                Description = "Test Program",
                DurationInMonths = 12,
                ProgramTierId = 3,
                HasMonthlySeminors = true,
                Name = "Advanced FELTP"
            };

            Assert.False(addProgramCommandValidator.Validate(addProgramCommand).IsValid);

        }

        [Test]
        public void Add_Program_With_NullOrEmpty_Program_Tier_Fails()
        {
            var addProgramCommand = new AddProgramCommand()
            {
                Code = "C002",
                CreatedBy = "System",
                DateCreated = DateTime.Now,
                Description = "Test Program",
                DurationInMonths = 12,
                HasMonthlySeminors = true,
                Name = "New Program"
            };

            Assert.False(addProgramCommandValidator.Validate(addProgramCommand).IsValid);
        }

    }
}
