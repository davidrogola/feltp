﻿using Application.ProgramManagement.CommandHandlers;
using Application.ProgramManagement.Commands;
using AutoMapper;
using FELTP.UnitTests.TestHelpers;
using Moq;
using NUnit.Framework;
using Persistance.ProgramManagement.Database;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FELTP.UnitTests.Features.ProgramManagement
{
    [TestFixture]
    public class AddUnitCommandHandlerTests
    {

        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapperMock;

        [SetUp]
        public void SetUp()
        {
            var inMemoryDbConfiguration = new ProgramManagementDbContextSetUp();
            var progManagemntDb = new ProgramManagementDbContext(inMemoryDbConfiguration.InitializeOptions("ProgramManagemntDb"));

            unitOfWork = new Mock<ProgramManagementUnitOfWork>(progManagemntDb).Object;
            mapperMock = new Mock<IMapper>().Object;
        }

        [Test]
        public async Task Add_Unit_Returns_A_Valid_UnitId()
        {
            var addUnit = new AddUnitCommand()
            {
                Code = "M001",
                Name = "Test Unit",
                Description = "Test Unit description"
            };
            var handler = new AddUnitCommandHandler(unitOfWork,mapperMock);
            var unitId = await handler.Handle(addUnit, CancellationToken.None);
            Assert.NotZero(unitId);
        }

    }
}

