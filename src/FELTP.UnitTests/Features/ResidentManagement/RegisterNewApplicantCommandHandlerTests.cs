﻿using Application.Common.Models;
using Application.ResidentManagement.CommandHandlers;
using Application.ResidentManagement.Commands;
using AutoMapper;
using Common.Domain.Address;
using Common.Domain.Person;
using FELTP.UnitTests.TestHelpers;
using MediatR;
using Moq;
using NUnit.Framework;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.Database;
using Persistance.ResidentManagement.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FELTP.UnitTests.Features.ResidentManagement
{

    [TestFixture]
    public class RegisterNewApplicantCommandHandlerTests
    {
        IResidentManagementUnitOfWork unitOfWork;
        IProgramManagementUnitOfWork programUnitOfWork;
        IMapper mapper;
        IMediator mediator;

        [SetUp]
        public void SetUp()
        {
            var inMemoryDbConfiguration = new ResidentManagementDbContextSetUp();
            var residentManagementDb = new ResidentManagementDbContext(inMemoryDbConfiguration.InitializeOptions("ResidentManagemntDb"));

            unitOfWork = new Mock<ResidentManagementUnitOfWork>(residentManagementDb).Object;

            var person = new Person("David", "Ogola", "Rakoro", Gender.Male, null, DateTime.Now.AddYears(-20), "28702206", IdentificationType.NationalID);
            var addressInfo = new AddressInformation(1, 2, 3, "186 Luanda", person.Id);
            var contactInfo = new ContactInformation("davidrogola@gmail.com", "0728777136", "073283232", person.Id);
           // var employmentInfo = new EmploymentHistory(person.Id, "P001", "Nurse", 10, "CDC","2015", "Nairobi","Director MOH");
            var academicDetail = new AcademicHistory(person.Id, "Bsc. IT", "JKUAT", DateTime.Now.AddYears(-4).Year.ToString(),EducationLevel.UnderGraduate,2,false,true,"Test");

            var mapperMock = new Mock<IMapper>();
            var mediatorMock = new Mock<IMediator>();
            var programUnitOfWorkMock = new Mock<IProgramManagementUnitOfWork>();
            mapperMock.Setup(x => x.Map<Person>(It.IsAny<BioDataInformation>()))
                 .Returns(person);

            mapperMock.Setup(x => x.Map<AddressInformation>(It.IsAny<AddressInfo>()))
               .Returns(addressInfo);

            mapperMock.Setup(x => x.Map<ContactInformation>(It.IsAny<ContactInfo>()))
               .Returns(contactInfo);

            //mapperMock.Setup(x => x.Map<EmploymentHistory>(It.IsAny<EmploymentDetail>()))
               //.Returns(employmentInfo);

            mapperMock.Setup(x => x.Map<AcademicHistory>(It.IsAny<AcademicDetail>()))
               .Returns(academicDetail);

            mapper = mapperMock.Object;
            mediator = mediatorMock.Object;
            programUnitOfWork = programUnitOfWorkMock.Object;
        }

        //[Test]
        //public async Task Register_New_Applicant_CommandHandler_Returns_Valid_Applicant_Id()
        //{
        //    var handler = new RegisterNewApplicantCommandHandler(unitOfWork,programUnitOfWork,mediator);
        //    var person = new AddPersonCommand();
        //    var addressInfo = new AddressInfo
        //    {
        //        CountryId = 1,
        //        CountyId = 1,
        //        SubCountyId = 3,
        //        PostalAddress = "186 Luanda"
        //    };
        //    person.AddressInfo = addressInfo;
        //    var applicantId = await handler.Handle(new RegisterNewApplicantCommand() {  Person = person }, CancellationToken.None);

        //    Assert.NotZero(applicantId);

        //}
    }
}
