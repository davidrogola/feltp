﻿using Application.Common;
using Application.ResidentManagement.CommandHandlers;
using Application.ResidentManagement.Commands;
using FELTP.UnitTests.TestHelpers;
using Moq;
using NUnit.Framework;
using Persistance.ResidentManagement.Database;
using Persistance.ResidentManagement.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FELTP.UnitTests.Features.ResidentManagement
{
    [TestFixture]
    public class AddResidentCommandHandlerTests 
    {
        ICodeGenerator codeGenerator;
        IResidentManagementUnitOfWork unitOfWork;
        [SetUp]
        public void SetUp()
        {
            var inMemoryDbConfiguration = new ResidentManagementDbContextSetUp();
            var residentManagementDb = new ResidentManagementDbContext(inMemoryDbConfiguration.InitializeOptions("ResidentManagemntDb"));
            var mockGenerator = new Mock<ICodeGenerator>();
            mockGenerator.Setup(x => x.Generate(It.IsAny<string>())).Returns("FELTP-001");
            codeGenerator = mockGenerator.Object;
            unitOfWork = new Mock<ResidentManagementUnitOfWork>(residentManagementDb).Object;
        }

        [Test]
        public async Task Add_Resident_Command_Returns_Valid_Resident_Id()
        {
            var handler = new AddResidentCommandHandler(unitOfWork,codeGenerator);
            var residentId = await handler.Handle(new AddResidentCommand()
            {
                ApplicantId = 1,
                CreatedBy = "David Ogola"
            }, CancellationToken.None);

            Assert.NotZero(residentId);

        }

    }
}
