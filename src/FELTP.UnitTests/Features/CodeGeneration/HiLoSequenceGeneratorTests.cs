﻿using Application.Common.SequenceGenerator;
using NUnit.Framework;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FELTP.UnitTests.Features.CodeGeneration
{
    [TestFixture]
    public class HiLoSequenceGeneratorTests
    {
        [Test]
        public void Generator_Returns_Values_In_Sequence()
        {
            var generator = new HiLoDistributedSequenceGenerator(new InMemorySequenceGenerator(), c => 10);
            var items = new List<long>(100);
            for (int i = 0; i < 100; i++)
                items.Add(generator.Next("test"));
            Assert.AreEqual(1, items[0]);
            for (int i = 1; i < 100; i++)
                Assert.AreEqual(items[i], items[i - 1] + 1);
            
        }

        [Test]
        public async Task Generator_Can_Handle_Multiple_Threads()
        {
            var generator = new HiLoDistributedSequenceGenerator(new InMemorySequenceGenerator(), c => 10);
            ConcurrentBag<long> bag = new ConcurrentBag<long>();
            var tasks = new Task[10];
            for (int i = 0; i < tasks.Length; i++)
                tasks[i] = Task.Run(() =>
                {
                    for (int j = 0; j < 1000; j++)
                        bag.Add(generator.Next("abc"));
                });
            await Task.WhenAll(tasks);
            var set = new HashSet<long>();
            foreach (var item in bag)
                Assert.True(set.Add(item));
        }
    }
}
