﻿using Application.Common.SequenceGenerator;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FELTP.UnitTests.Features.CodeGeneration
{
    public class InMemorySequenceGenerator : IDistributedSequenceGenerator
    {
        ConcurrentDictionary<string, Generator> generators = new ConcurrentDictionary<string, Generator>();

        public long Next(string sequenceName)
        {
            var gen = generators.GetOrAdd(sequenceName, key => new Generator());
            return gen.Next();
        }

        private class Generator
        {
            long value = -1;
            public long Next()
            {
                return Interlocked.Increment(ref value);
            }
        }
    }
}
