﻿using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace FELTP.UnitTests.TestHelpers
{
    public class SetUpProgramData
    {
        IProgramManagementUnitOfWork unitOfWork;
        public SetUpProgramData(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;

        }
        public void AddProgramTier()
        {
            List<ProgramTier> tiers = new List<ProgramTier>() {
                new ProgramTier("Tier 1: Basic","System"),
                new ProgramTier("Tier 2: Intermediate","System"),
                new ProgramTier("Tier 3: Advanced","System")
            };

            unitOfWork.ProgramManagementRepository.AddRange(tiers);
            unitOfWork.SaveChanges();
        }

        public void AddPrograms()
        {
            List<Program> programs = new List<Program>()
            {
                 new Program("Basic FELTP", 1, "C001", "Test program set up", 3, false, "David"),
                new Program("Intermediate FELTP", 2, "C002", "Test program set up", 12, true, "System"),
                new Program("Advanced FELTP", 3, "C003", "Test program set up", 12, true, "Bree")

            };

            unitOfWork.ProgramManagementRepository.AddRange(programs);
            unitOfWork.SaveChanges();

        }

        public void AddCourseTypes()
        {
            List<CourseType> courseTypes = new List<CourseType>()
            {
                new CourseType("Didactic","System"),
                new CourseType("Fied Placement","System")
            };

            unitOfWork.ProgramManagementRepository.AddRange(courseTypes);
            unitOfWork.SaveChanges();
        }

        public void AddSemesters()
        {
            List<Semester> semesters = new List<Semester>()
            {
                new Semester("Semester 1","System" ),
                new Semester("Semester 2" ,"System"),
                new Semester( "Semester 3","System")
            };

            unitOfWork.ProgramManagementRepository.AddRange(semesters);
            unitOfWork.SaveChanges();
        }

        public void AddCourses()
        {
            List<Course> courses = new List<Course>()
            {
                new Course(1,"Scientific Communication","SC001","System"),
                 new Course(1,"Scientific Communication II","SC002","System"),
                 new Course(4,"Field Placement","F001","System")
            };

        }

        public void AddProgramCourse()
        {
            var programCourse = new ProgramCourse(1, 1, 1, 10, "David");
            unitOfWork.ProgramManagementRepository.Add(programCourse);
            unitOfWork.SaveChanges();
        }

        public void AddUnits()
        {
            List<Unit> unit = new List<Unit>()
            {
                new Unit("Basic biostatistics","BB2","David","Basic Bio",false),
                new Unit("Outbreak investigation","OOS","System","Investigation",true)
            };

            unitOfWork.ProgramManagementRepository.AddRange(unit);
            unitOfWork.SaveChanges();
        }
    }
}
