﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Persistance.ProgramManagement.Database;
using Persistance.ResidentManagement.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace FELTP.UnitTests.TestHelpers
{
    public class ProgramManagementDbContextSetUp
    {

        public  DbContextOptions<ProgramManagementDbContext> InitializeOptions(string dbName)
        {
            var options = new DbContextOptionsBuilder<ProgramManagementDbContext>()
              .UseInMemoryDatabase(databaseName: $"{dbName}InMemoryDatabase")
              .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
              .Options;
            return options;
        }

    }

    public class ResidentManagementDbContextSetUp
    {

        public DbContextOptions<ResidentManagementDbContext> InitializeOptions(string dbName)
        {
            var options = new DbContextOptionsBuilder<ResidentManagementDbContext>()
              .UseInMemoryDatabase(databaseName: $"{dbName}InMemoryDatabase")
              .ConfigureWarnings(x=>x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
              .Options;
            return options;
        }
    }
}
