﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UserManagement.Services.Authorization;

namespace FELTP.Reports.Controllers
{
    [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
    [Authorize]
    public class ResidentReportsController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [Operation("View Residents Report")]
        public IActionResult Residents()
        {
            return View();
        }

        [Operation("View Residents Perfomance Report")]
        public IActionResult ResidentPerformance()
        {
            return View();
        }

        [Operation("View Graduation Status Report")]
        public IActionResult GraduationStatusReport()
        {
            return View();
        }

        [Operation("View Graduation Details Report")]
        public IActionResult GraduationDetailsReport()
        {
            return View();
        }

        [Operation("View Resident Deliverable Submission Report")]
        public IActionResult ResidentDeliverableSubmissionReport()
        {
            return View();
        }

        [Operation("View Resident Thesis Defence Report")]
        public IActionResult ResidentThesisDefenceReport()
        {
            return View();
        }
    }
}