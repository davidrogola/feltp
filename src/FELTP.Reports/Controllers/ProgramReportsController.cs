﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UserManagement.Services.Authorization;

namespace FELTP.Reports.Controllers
{
    [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
    [Authorize]
    public class ProgramReportsController : Controller
    {
        [Operation("View Programs Report")]
        public IActionResult Programs()
        {
            return View();
        }

        [Operation("View Program Courses Report")]
        public IActionResult ProgramCourses()
        {
            return View();
        }
    }
}