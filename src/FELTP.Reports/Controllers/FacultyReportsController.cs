﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UserManagement.Services.Authorization;

namespace FELTP.Reports.Controllers
{
    public class FacultyReportsController : Controller
    {
        [Operation("View Trainers Schedule Report")]
        public IActionResult TrainerSchedule()
        {
            return View();
        }

    }
}