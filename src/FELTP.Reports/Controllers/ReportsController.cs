﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Telerik.Reporting.Cache.File;
using Telerik.Reporting.Services;
using Telerik.Reporting.Services.AspNetCore;

namespace FELTP.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/reports")]
    public class ReportsController : ReportsControllerBase
    {

        string reportsPath = string.Empty;

        public ReportsController(IHostingEnvironment environment)
        {
            this.reportsPath = Path.Combine(environment.WebRootPath, "Reports");

            this.ReportServiceConfiguration = new ReportServiceConfiguration
            {
                HostAppId = "FELTP-Reports",
                Storage = new FileStorage(),
                ReportResolver = new ReportTypeResolver()
                                    .AddFallbackResolver(new ReportFileResolver(this.reportsPath)),
            };
        }

        [HttpGet("reportlist")]
        public IEnumerable<string> GetReports()
        {
            return Directory
                .GetFiles(this.reportsPath)
                .Select(path =>
                    Path.GetFileName(path));
        }
    }
}