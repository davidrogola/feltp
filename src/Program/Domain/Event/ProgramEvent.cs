﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Event
{
    public class ProgramEvent
    {
        public ProgramEvent()
        {

        }
        public ProgramEvent(int eventId, int programId, string createdBy)
        {
            EventId = eventId;
            ProgramId = programId;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;          
        }

        public int Id { get; set; }
        public int EventId { get; set; }
        public int ProgramId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }  
        public virtual Program.Program Program { get; set; }
        public virtual  Event Event { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public void Deactivate(string deactivatedBy)
        {
            DeactivatedBy = deactivatedBy;
            DateDeactivated = DateTime.Now;
        }
    }
}
