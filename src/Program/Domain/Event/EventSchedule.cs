﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Event
{
    public enum PresentationType
        {
            Oral =1,
            Poster
        }
    public class EventSchedule
    {
        public EventSchedule()
        {

        }
        public EventSchedule(int programEventId, PresentationType presentationType, string location,
            DateTime startDate,  DateTime endDate,bool isElapsed, string createdBy)
        {
            ProgramEventId = programEventId;
            PresentationType = presentationType;
            Location = location;
            StartDate = startDate;
            EndDate = endDate;
            IsElapsed = isElapsed;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
            
        }
        public int Id { get; set; }
        public int ProgramEventId { get; set; }
        public PresentationType PresentationType { get; set; }
        public string Location { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsElapsed { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public virtual ProgramEvent ProgramEvent { get; set; }
    }
}
