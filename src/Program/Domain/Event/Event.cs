﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Event
{
    public enum EventType
    {
        Seminar = 1,
        Workshop,
        Conference
    }
    public class Event 
    {
        public Event()
        {

        }
        public Event(string title, string theme, EventType eventType, string createdBy)
        {
            Title = title;
            Theme = theme;
            EventTypeId = eventType;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Theme { get; set; }
        public EventType EventTypeId { get; set; }
        public DateTime DateCreated { get; private set; }
        public string CreatedBy { get; private set; }
        public DateTime? DateDeactivated { get; private set; }
        public string DeactivatedBy { get; private set; }

        public void UpdateEvent(string title, string theme, EventType type)
        {
            Title = title;
            Theme = theme;
            EventTypeId = type;
        }

        public List<ProgramEvent> SelectPrograms(int[] programs, string createdBy)
        {
            var programEvents = new List<ProgramEvent>();
            foreach (var program in programs)
            {
                var programEvent = new ProgramEvent(Id, program, createdBy);
                programEvents.Add(programEvent);
            }
            return programEvents;
        }


    }
}
