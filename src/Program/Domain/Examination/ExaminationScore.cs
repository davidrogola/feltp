﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProgramManagement.Domain.Examination
{
    public enum ExamType
    {
        [Display(Name = "CAT I")]
        CatOne = 1,
        [Display(Name="CAT II")]
        CatTwo,
        [Display(Name = "CAT III")]
        CatThree,
        [Display(Name = "Final Exam")]
        Final
    }
    public class ExaminationScore
    {
       
        public ExaminationScore()
        {

        }
        public ExaminationScore(int examId, int ? examParticipationId, string createdBy, Decimal score,
            long ? residentUnitId, long ? residentCourseWorkId, ExamType examType)
        {
            ExaminationId = examId;
            ExaminationParticipationId = examParticipationId;
            CreatedBy = createdBy;
            Score = score;
            ResidentUnitId = residentUnitId;
            DateCreated = DateTime.Now;
            ExamType = examType;
            ResidentCourseWorkId = residentCourseWorkId;
        }
        public int Id { get; set; }
        public ExamType ExamType { get; set; }
        public int ExaminationId { get; set; }
        public int ? ExaminationParticipationId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public Decimal Score { get; set; }
        public long ? ResidentUnitId { get; set; }
        public long ? ResidentCourseWorkId { get; set; }
        public virtual Examination Examination { get; set; }
        public virtual ExaminationParticipation ExaminationPaticipation { get; set; }

    }
}
