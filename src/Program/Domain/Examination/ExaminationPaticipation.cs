﻿using System;
using ProgramManagement.Domain.Program;


namespace ProgramManagement.Domain.Examination
{
    public class ExaminationParticipation 
    {
        public ExaminationParticipation()
        {

        }
        public ExaminationParticipation(int timetableId, int enrollmentId,string createdBy)
        {
            TimetableId = timetableId;
            ProgramEnrollmentId = enrollmentId;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
        }
        
        public int Id { get; set; }
        public int TimetableId { get; set; }
        public int ProgramEnrollmentId { get; set; }     
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public virtual Timetable Timetable { get; set; }
        public virtual ProgramEnrollment ProgramEnrollment { get; set; }
        public virtual ExaminationScore ExaminationScore { get; set; }
    }
}
