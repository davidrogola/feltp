﻿using System;
using ProgramManagement.Domain.Course;

namespace ProgramManagement.Domain.Examination
{
    public class Examination
    {
        public Examination()
        {

        }
        public Examination(string name, int ? unitId,int ? courseId, string createdBy)
        {
            Name = name;
            UnitId = unitId;
            CourseId = courseId;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int ? UnitId { get; set; }
        
        public int ? CourseId { get; set; }
        public DateTime DateCreated{ get; set; }
        public string CreatedBy { get; set; }
        public virtual Unit Unit { get; set; }
        
        public virtual Course.Course Course { get; set; }

    }
}
