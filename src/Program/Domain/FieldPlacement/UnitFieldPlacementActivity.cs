﻿using Common.Models;
using ProgramManagement.Domain.Course;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.FieldPlacement
{
    public class UnitFieldPlacementActivity 
    {

        public UnitFieldPlacementActivity()
        {

        }
        public UnitFieldPlacementActivity(int unitId, int fieldPlacementActivityId, 
            string createdBy)
        {
            UnitId = unitId;
            FieldPlacementActivityId = fieldPlacementActivityId;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
        }

        public int Id { get; set; }
        public int UnitId { get; set; }
        public int FieldPlacementActivityId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public virtual  Unit Unit { get; set; }
        public virtual  FieldPlacementActivity FieldPlacementActivity { get; set; }

        

        public void Deactivate(string deactivatedBy)
        {
            DateDeactivated = DateTime.Now;
            DeactivatedBy = deactivatedBy;
        }
    }
}