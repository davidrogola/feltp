﻿using Common.Models;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.FieldPlacement
{
    public class FieldPlacementActivityDeliverable : IAuditItem
    {
        public FieldPlacementActivityDeliverable()
        {

        }

        public FieldPlacementActivityDeliverable(int fieldPlacementActivityId, int delivarableId, string createdBy)
         
        {
            DeliverableId = delivarableId;
            FieldPlacementActivityId = fieldPlacementActivityId;
            DateCreated = DateTime.Now;
            CreatedBy = createdBy;
        }

        public int Id { get; set; }
        public int DeliverableId { get; set; }
        public int FieldPlacementActivityId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }

        public virtual FieldPlacementActivity FieldPlacementActivity { get; set; }
        public virtual Deliverable Deliverable { get; set; }


        public void Deactivate(string deactivatedBy)
        {
            DeactivatedBy = deactivatedBy;
            DateDeactivated = DateTime.Now;
        }
    }
}
