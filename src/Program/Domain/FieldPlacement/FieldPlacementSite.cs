﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProgramManagement.Domain.FieldPlacement
{
    public enum Level
    {
        National = 1,
        County
    }

    public class FieldPlacementSite : IAuditItem 
    {
        public FieldPlacementSite()
        {

        }

        public FieldPlacementSite(string name, int countryId, int ? countyId, int ? subCountyId, 
            string postalAddress, string building, string location, Level level, string createdBy) 
        {
            Name = name;
            CountryId = countryId;
            CountyId = countyId;
            SubCountyId = subCountyId;
            PostalAddress = postalAddress;
            Building = building;
            Location = location;
            Level = level;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
           

        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }
        public int? CountyId { get; set; }
        public int? SubCountyId { get; set; }
        public string PostalAddress { get; set; }
        public string Building { get; set; }
        public string Location { get; set; }
        public Level Level { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public virtual Country Country { get; set; }
        public virtual County County { get; set; }
        public virtual SubCounty SubCounty { get; set; }
        public virtual ICollection<FieldPlacementSiteSupervisor> SiteSupervisors { get; set; }

        public void Deactivate(string deactivatedBy)
        {
            throw new NotImplementedException();
        }
        public List<FieldPlacementSiteSupervisor> AddSupervisors(int[] supervisors, string createdBy)
        {
            var fieldPlacementSiteSupevisors = new List<FieldPlacementSiteSupervisor>();
            foreach(var supervisor in supervisors)
            {
                var fieldPlacementSiteSupevisor = new FieldPlacementSiteSupervisor(supervisor,Id, createdBy);
                fieldPlacementSiteSupevisors.Add(fieldPlacementSiteSupevisor);
            }
            return fieldPlacementSiteSupevisors;
        }
        public void UpdateFieldPlacementSite(string name, int countryId, int? countyId, int? subCountyId,
            string postalAddress, string building, string location, Level level)
        {
            Name = name;
            CountryId = countryId;
            CountyId = countyId;
            SubCountyId = subCountyId;
            PostalAddress = postalAddress;
            Building = building;
            Location = location;
            Level = level;
        }
   
    }
}
