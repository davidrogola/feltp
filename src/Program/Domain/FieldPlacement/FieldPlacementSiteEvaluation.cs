using System;

namespace ProgramManagement.Domain.FieldPlacement
{
    public class FieldPlacementSiteEvaluation
    {
        public FieldPlacementSiteEvaluation()
        {
            
        }

        public FieldPlacementSiteEvaluation(long residentId, long fieldPlacementSiteId, 
            string feedback, int rating)
        {
            ResidentId = residentId;
            FieldPlacementSiteId = fieldPlacementSiteId;
            DateEvaluated = DateTime.Now;
            Feedback = feedback;
            Rating = rating;
        }
        
        public int Id { get; set; }
        public long ResidentId { get; set; }
        public long FieldPlacementSiteId { get; set; }
        public DateTime DateEvaluated { get; set; }
        public string Feedback { get; set; }
        public int Rating { get; set; }
    }
}