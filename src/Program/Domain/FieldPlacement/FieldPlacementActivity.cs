﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProgramManagement.Domain.FieldPlacement
{
    public enum ActivityType
    {
        Field = 1,
        [Display(Name = "Non-Field")]
        NonField
    }
    public class FieldPlacementActivity : IAuditItem
    {
        public FieldPlacementActivity()
        {

        }
        public FieldPlacementActivity(string name, string createdBy, string description, ActivityType activityType)
        {
            Name = name;
            CreatedBy = createdBy;
            Description = description;
            DateCreated = DateTime.Now;
            ActivityType = activityType;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Description { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public ActivityType ActivityType { get; set; }
        public virtual ICollection<FieldPlacementActivityDeliverable> ActivityDeliverables { get; set; }

        public void Deactivate(string deactivatedBy)
        {
            DateDeactivated = DateTime.Now;
            DeactivatedBy = deactivatedBy;
        }

        public List<FieldPlacementActivityDeliverable> AddDeliverables(int[] deliverables)
        {
            var fieldPlacementActivityDeliverables = new List<FieldPlacementActivityDeliverable>();
            foreach (var deliverable in deliverables)
            {
                var fieldPlacementActivityDeliverable = new FieldPlacementActivityDeliverable(Id, deliverable, "System");
                fieldPlacementActivityDeliverables.Add(fieldPlacementActivityDeliverable);
            }
            return fieldPlacementActivityDeliverables;
        }

        public void UpdateFieldPlacementActivity(string name, string description, ActivityType activityType)
        {
            Name = name;
            Description = description;
            ActivityType = activityType;
        }

    }
}
