﻿using Common.Models;
using FacultyManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.FieldPlacement
{
    public class FieldPlacementSiteSupervisor  : IAuditItem
    {
        public FieldPlacementSiteSupervisor() 
        {

        }

        public FieldPlacementSiteSupervisor(int facultyId, int fieldPlacementSiteId, string createdBy)          
        {
            FacultyId = facultyId;
            FieldPlacementSiteId = fieldPlacementSiteId;
            DateCreated = DateTime.Now;
            CreatedBy = createdBy;
        }

        public int Id { get; set; }
        public int FacultyId { get; set; }
        public int FieldPlacementSiteId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public virtual FieldPlacementSite FieldPlacementSite { get; set; }
        public virtual Faculty  Faculty { get; set; }
        public void Deactivate(string deactivatedBy)
        {
            DateDeactivated = DateTime.Now;
            DeactivatedBy = deactivatedBy;
        }

    }
}
