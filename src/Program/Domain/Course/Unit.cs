﻿using Common.Models;
using ProgramManagement.Domain.FieldPlacement;
using System;
using System.Collections.Generic;
using System.Text;

 namespace ProgramManagement.Domain.Course

{
    public class Unit 
    {

        public Unit()
        {

        }

        public Unit(string name, string code , string createdBy, string description,bool hasActivity)
        {
            Name = name;
            Code = code;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
            Description = description;
            HasFieldPlacementActivity = hasActivity;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool HasFieldPlacementActivity { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public ICollection<UnitFieldPlacementActivity> FieldPlacementActivities { get; set; }

        public void Update(string name, string code, string description,string updatedBy,bool hasActivity)
        {
            Name = name;
            Code = code;
            Description = description;
            CreatedBy = updatedBy;
            HasFieldPlacementActivity = hasActivity;
        }

        public List<UnitFieldPlacementActivity> AddActivities(int[] activities)
        {
            var unitActivities = new List<UnitFieldPlacementActivity>();
            foreach (var activity in activities)
            {
                var unitActivity = new UnitFieldPlacementActivity(Id, activity, CreatedBy);
                unitActivities.Add(unitActivity);
            }
            return unitActivities;
        }
    }
}
