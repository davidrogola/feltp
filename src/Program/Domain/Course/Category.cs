﻿using Common.Models;
using ProgramManagement.Domain.Course;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Course
{
      public class Category 
    {
        public Category()
        {

        }
        public Category(string name, int courseTypeId, string createdBy)
        {
            Name = name;
            CourseTypeId = courseTypeId;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int CourseTypeId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public virtual CourseType CourseType { get; set; }

        public void Update(string name, int courseTypeId)
        {
            CourseTypeId = courseTypeId;
            Name = name;
        }
       
    }
}
