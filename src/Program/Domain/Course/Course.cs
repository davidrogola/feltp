﻿using Common.Models;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Course
{
    public class Course  
    {
        public Course()
        {

        }

        public Course(int categoryId, string name, string code, string createdBy) 
        {
            CategoryId = categoryId;
            Name = name; 
            CreatedBy = createdBy;
            Code = code;
            DateCreated = DateTime.Now;
        }

        public int Id { get; private set; }
        public int CategoryId { get; set; }
        public string Name { get; private set; }
        public string Code { get; private set; }
        public virtual Category Category { get; private set; }
        public DateTime DateCreated { get; private set; }
        public string CreatedBy { get; private set; }
        public DateTime? DateDeactivated { get; private set; }
        public string DeactivatedBy { get; private set; }
       
        public void SetCode(string code)
        {
            Code = code;
        }

        public void Update(string name,int categoryId)
        {
            Name = name;
            CategoryId = categoryId;
        }
        public void SetAuditInfomation(string createdBy)
        {
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
        }
        public List<CourseUnit> AddUnits(int[] units,string createdBy)
        {
            var courseUnits = new List<CourseUnit>();
            foreach (var unit in units)
            {
                var courseUnit = new CourseUnit(Id, unit, createdBy);
                courseUnits.Add(courseUnit);
            }
            return courseUnits;
        }
    }
}
