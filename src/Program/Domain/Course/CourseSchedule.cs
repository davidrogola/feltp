﻿using Common.Models;
using FacultyManagement.Domain;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Course

{
    public class CourseSchedule : IAuditItem
    {
        public CourseSchedule()
        {

        }
        public CourseSchedule(string name,int programOfferId, int programCourseId, DateTime startDate, DateTime endDate,
            int facultyId, string location, string createdBy)
        {
            Name = name;
            ProgramOfferId = programOfferId;
            ProgramCourseId = programCourseId;
            StartDate = startDate;
            EndDate = endDate;
            FacultyId = facultyId;
            Location = location;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int ProgramOfferId { get; set; }
        public int ProgramCourseId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int FacultyId { get; set; }
        public string Location { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public virtual ProgramCourse ProgramCourse { get; set; }
        public virtual Faculty Faculty { get; set; }
        public virtual ProgramOffer ProgramOffer {get; set;}

        public void Deactivate(string deactivatedBy)
        {
            throw new NotImplementedException();
        }
    }
}
