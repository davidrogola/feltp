﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Course

{
    public class CourseType
    {
        public CourseType()
        {

        }
        public CourseType(string name, string createdBy)
        {
            Name = name;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
        }

        public int Id { get; private set; }
        public string Name { get; private set; }
        public DateTime DateCreated { get; private set; }
        public string CreatedBy { get; private set; }
        public DateTime? DateDeactivated { get; private set; }
        public string DeactivatedBy { get; private set; }
       
    }
}
