﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Course

{
    public class CourseUnit : IAuditItem
    {
        public CourseUnit()
        {

        }

        public CourseUnit(int courseId, int unitId, string createdBy) 
        {
            CourseId = courseId;
            UnitId = unitId;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;

        }

        public int Id { get; set; }
        public int CourseId { get; set; }
        public int UnitId { get; set; }
        public DateTime DateCreated { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? DateDeactivated { get; set; }

        public string DeactivatedBy { get; set; }

        public virtual Unit Unit { get; set; }
        public virtual Course Course { get; set; }

        public void Deactivate(string deactivatedBy)
        {
            DateDeactivated = DateTime.Now;
            DeactivatedBy = deactivatedBy;
        }
    }
}
