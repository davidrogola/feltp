﻿using System;

namespace ProgramManagement.Domain.Program
{
    public class ResourceFile
    {
        public ResourceFile()
        {

        }
        public ResourceFile(int resourceId)
        {
            ResourceId = resourceId;
        }
        public int Id { get; set; }
        public int ResourceId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte [] FileContent { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }

    }
}
