﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Program
{
    public enum CollaboratorType
    {
        Collaborator = 1,
        Partner
    }
    public class Collaborator
    {
        public Collaborator()
        {

        }
        public Collaborator(string name, CollaboratorType CollaboratorType, string createdBy)
        {
            Name = name;
            CollaboratorTypeId = CollaboratorType;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public CollaboratorType CollaboratorTypeId { get; set; }
        public DateTime DateCreated { get; private set; }
        public string CreatedBy { get; private set; }
        public DateTime? DateDeactivated { get; private set; }
        public string DeactivatedBy { get; private set; }
        public virtual ICollection<ProgramCollaborator> ProgramCollaborators { get; set; }

        public void Deactivate(string deactivatedBy)
        {
            DateDeactivated = DateTime.Now;
            DeactivatedBy = deactivatedBy;
        }
        public void SetAuditInformation(string createdBy)
        {
            throw new NotImplementedException();
        }

        public List<ProgramCollaborator> SelectPrograms(int[] programs)
        {
            var programCollaborators = new List<ProgramCollaborator>();
            foreach (var program in programs)
            {
                var programCollaborator = new ProgramCollaborator(Id, program, CreatedBy);
                programCollaborators.Add(programCollaborator);
            }
            return programCollaborators;
        }

        public void UpdateCollaborator(string name, CollaboratorType CollaboratorType)
        {
            Name = name;
            CollaboratorTypeId = CollaboratorType;
        }

    }

}

