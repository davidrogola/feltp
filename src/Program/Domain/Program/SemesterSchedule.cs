﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Program
{
    public enum ProgressStatus
    {
        NotStarted = 0,
        InProgress,
        Completed
    }
    public enum ApprovalStatus : int
    {
        Pending = 1,
        Approved,
        Declined
    }

    public class SemesterSchedule
    {
        public SemesterSchedule()
        {

        }
        public SemesterSchedule(int programOfferId, string description, int semesterId, DateTime estimatedStartDate,
            DateTime estimatedEndDate, string createdBy, ProgressStatus status)
        {
            ProgramOfferId = programOfferId;
            Description = description;
            SemesterId = semesterId;
            EstimatedEndDate = estimatedEndDate;
            EstimatedStartDate = estimatedStartDate;
            DateCreated = DateTime.Now;
            CreatedBy = createdBy;
            Status = status;
            ActualStartDate = estimatedStartDate;
            ActualEndDate = estimatedEndDate;

        }
        public int Id { get; set; }
        public int ProgramOfferId { get; set; }
        public string Description { get; set; }
        public int SemesterId { get; set; }
        public DateTime EstimatedStartDate { get; set; }
        public DateTime EstimatedEndDate { get; set; }
        public DateTime? ActualStartDate { get; set; }
        public DateTime? ActualEndDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public ProgressStatus Status { get; set; }
        public string CompletionRequestedBy { get; set; }
        public DateTime? CompletionRequestDate { get; set; }
        public string CompletionRequestReason{ get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public ApprovalStatus? ApprovalStatus { get; set; }
        public string ApprovalReason { get; set; }
        public virtual Semester Semester { get; set; }
        public virtual ProgramOffer ProgramOffer { get; set; }
        public List<SemesterScheduleItem> SemesterScheduleItems { get; set; }

        public void InitiateSemesterCompletionRequest(string requestedBy, string comment)
        {
            CompletionRequestedBy = requestedBy;
            CompletionRequestReason = comment;
            CompletionRequestDate = DateTime.Now;
            ApprovalStatus = Domain.Program.ApprovalStatus.Pending;
        }

        public void ApproveSemesterCompletionRequest(ApprovalStatus approvalStatus, string comment, string approvedBy)
        {
            ApprovalStatus = approvalStatus;
            ApprovalReason = comment;
            ApprovedBy = approvedBy;
            ApprovalDate = DateTime.Now;
            if (approvalStatus == Domain.Program.ApprovalStatus.Approved)
                Status = ProgressStatus.Completed;
        }

        public void StartSemester()
        {
            Status = ProgressStatus.InProgress;
        }

        public void AdjustSemesterDate(DateTime startDate, DateTime endDate)
        {
            EstimatedStartDate = startDate;
            EstimatedEndDate = endDate;
            ActualStartDate = startDate;
            ActualEndDate = endDate;
        }

    }
}
