﻿using Common.Domain.Person;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Program
{
    public class ProgramOfferAcademicRequirement
    {
        public ProgramOfferAcademicRequirement()
        {

        }
        public ProgramOfferAcademicRequirement(int academicCourseId, int programOfferId, int ? minimumCourseGradingId, string createdBy)
        {
            AcademicCourseId = academicCourseId;
            ProgramOfferId = programOfferId;
            MinimumCourseGradingId = minimumCourseGradingId;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
        }
        public int Id { get; set; }
        public int AcademicCourseId { get; set; }
        public int ProgramOfferId { get; set; }
        public int ? MinimumCourseGradingId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public virtual ProgramOffer ProgramOffer { get; set; }
        public virtual AcademicCourse AcademicCourse { get; set; }
        public virtual AcademicCourseGrading MinimumCourseGrading { get; set; }

        public void SetProgramOffer(int programOfferId)
        {
            ProgramOfferId = programOfferId;
        }

    }
}