﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Program
{
    public class ProgramLocation
    {
        public ProgramLocation()
        {

        }
        public ProgramLocation(int countryId, int? countyId, int? subCountyId,
            string postalAddress, string building, string location, string road, string createdBy)
        {
            CountryId = countryId;
            CountyId = countyId;
            SubCountyId = subCountyId;
            PostalAddress = postalAddress;
            Building = building;
            Location = location;
            Road = road;
            DateCreated = DateTime.Now;
            CreatedBy = createdBy;
        }

        public int Id { get; set; }
        public int CountryId { get; set; }
        public int? CountyId { get; set; }
        public int? SubCountyId { get; set; }
        public string PostalAddress { get; set; }
        public string Building { get; set; }
        public string Location { get; set; }
        public string Road { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        
        public string DeactivatedBy { get; set; }
        
        public DateTime ? DateDeactivated { get; set; }
        public virtual Country Country { get; set; }
        public virtual SubCounty SubCounty { get; set; }
        public virtual County County { get; set; }

        public void UpdateLocation(int countryId, int? countyId, int? subCountyId,
            string postalAddress, string building, string location, string road)
        {
            CountryId = countryId;
            CountyId = countyId;
            SubCountyId = subCountyId;
            PostalAddress = postalAddress;
            Building = building;
            Location = location;
            Road = road;
        }

        public void Deactivate(string deactivatedBy)
        {
            DeactivatedBy = deactivatedBy;
            DateDeactivated = DateTime.Now;
        }


    }
}
