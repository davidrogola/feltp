﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using FacultyManagement.Domain;
using ProgramManagement.Domain.Course;

namespace ProgramManagement.Domain.Program
{
    public enum TimetableType
    {
        Unit = 1,
        Examination,
        [Display(Name= "Cat One")]
        CatOne,
        [Display(Name= "Cat Two")]
        CatTwo,
        [Display(Name = "Cat Three")]
        CatThree
    }
    public class Timetable
    {
        public Timetable()
        {

        }
        public Timetable(int scheduleComponentId, DateTime startDate, DateTime endDate, TimetableType type,
            int facultyId, int? examId, int? unitId, string createdBy)
        {
            SemesterScheduleItemId = scheduleComponentId;
            StartDate = startDate;
            EndDate = endDate;
            TimeTableType = type;
            FacultyId = facultyId;
            ExaminationId = examId;
            UnitId = unitId;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
        }
        public int Id { get; set; }
        public int SemesterScheduleItemId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public TimetableType TimeTableType { get; set; }
        public int FacultyId { get; set; }
        public int? ExaminationId { get; set; }
        public int? UnitId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public virtual SemesterScheduleItem SemesterScheduleItem { get; set; }
        public virtual Examination.Examination Examination { get; set; }
        public virtual Faculty Faculty { get; set; }
        public virtual Unit Unit { get; set; }
        
        public string CalculateDuration()
        {
            var timeSpan = EndDate.Subtract(StartDate);
            
            var duration = $"{timeSpan.Hours} Hour(s) {timeSpan.Minutes} Minutes";
            if(timeSpan.Minutes <= 0)
                duration = $"{timeSpan.Hours} Hour(s)";

            return duration;
        }

        public void Update(DateTime startDate,DateTime endDate,int facultyId,int ? examinationId,
            int ? unitId)
        {
            StartDate = startDate;
            EndDate = endDate;
            FacultyId = facultyId;

            if (TimeTableType == TimetableType.Examination)
                ExaminationId = examinationId;
            else
                UnitId = unitId;
        }

    }
}
