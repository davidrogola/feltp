﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Program
{
   public class ProgramCollaborator : IAuditItem
    {
        public ProgramCollaborator()
        {

        }
        public ProgramCollaborator(int collaboratorId, int programId, string createdBy)
        {        
            CollaboratorId = collaboratorId;
            ProgramId = programId;
            DateCreated = DateTime.Now;
            CreatedBy = createdBy;
        }

        public int Id { get; set; }
        public int ProgramId { get; set; }
        public int CollaboratorId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public virtual Program Program { get; private set; }
        public virtual Collaborator Collaborator { get; private set; }
        public void Deactivate(string deactivatedBy)
        {
            DateDeactivated = DateTime.Now;
            DeactivatedBy = deactivatedBy;
        }

    }
}
