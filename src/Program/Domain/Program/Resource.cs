﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProgramManagement.Domain.Program
{
    public class Resource 
    {
        public Resource()
        {

        }

        public Resource(string resourceTitle, string resourceDescription, string source, string sourcecontactdetails,
            int resourcetypeid,
            string createdBy)
        {
            ResourceTitle = resourceTitle;
            ResourceDescription = resourceDescription;
            Source = source;
            SourceContactDetails = sourcecontactdetails;
            ResourceTypeId = resourcetypeid;
            DateCreated = DateTime.Now;
            CreatedBy = createdBy;
        }

        public int Id { get; set; }
        public string ResourceTitle { get; set; }
        public string ResourceDescription { get; set; }
        public string Source { get; set; }
        public string SourceContactDetails { get; set; }
        public int ResourceTypeId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public virtual ResourceType ResourceType { get; set; }
        public virtual ICollection<ProgramResource> LinkedPrograms { get; set; }

        public List<ProgramResource> AddPrograms(int[] programs)
        {
            var programResources = new List<ProgramResource>();
            foreach (var program in programs)
            {
                var programResource = new ProgramResource(Id, program, CreatedBy);
                programResources.Add(programResource);
            }
            return programResources;
        }

        public void UpdateResourceDetails(int resourceTypeId,string resourceTitle,string source, string sourceContactDetails, string description)
        {
            ResourceTypeId = resourceTypeId;
            ResourceDescription = description;
            SourceContactDetails = sourceContactDetails;
            ResourceTitle = resourceTitle;
            Source = source;
        }

       
    }
}
