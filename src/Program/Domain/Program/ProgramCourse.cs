﻿using System;
using Common.Models;
using Course = ProgramManagement.Domain.Course;

namespace ProgramManagement.Domain.Program
{
    public class ProgramCourse 
    {
        public ProgramCourse()
        {

        }
        public ProgramCourse(int programId, int courseId, int semesterId, 
            int durationInWeeks, string createdBy)
        {
            ProgramId = programId;
            CourseId = courseId;
            SemesterId = semesterId;
            DurationInWeeks = durationInWeeks;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
        }
        public int Id { get; set; }
        public int ProgramId { get; set; }
        public int CourseId { get; set; }
        public int SemesterId { get; set; }
        public int DurationInWeeks { get; set; }
        public virtual Program Program { get; set; }
        public virtual  Course.Course Course { get; set; }
        public Semester Semester { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public void Deactivate(string deactivatedBy)
        {
            DeactivatedBy = deactivatedBy;
            DateDeactivated = DateTime.Now;
        }
    }
}
