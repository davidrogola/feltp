﻿using System;
using System.Collections.Generic;
using FacultyManagement.Domain;



namespace ProgramManagement.Domain.Program
{
    public enum ScheduleItemType
    {
        Course,
        Examination,
        Revision,
        Cat
    }
    public class SemesterScheduleItem
    {
        public SemesterScheduleItem()
        {

        }
        public SemesterScheduleItem(ScheduleItemType componentType, DateTime startDate, DateTime endDate, string venue,
            string name, int? programCourseId, int? revisionId, ProgressStatus status, string createdBy,
            int semesterScheduleId)
        {
            ScheduleItemType = componentType;
            StartDate = startDate;
            EndDate = endDate;
            DateCreated = DateTime.Now;
            Venue = venue;
            Name = name;
            ProgramCourseId = programCourseId;
            RevisionId = revisionId;
            Status = status;
            CreatedBy = createdBy;
            SemesterScheduleId = semesterScheduleId;
        }
        public int Id { get; set; }
        public ScheduleItemType ScheduleItemType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Venue { get; set; }
        public string Name { get; set; }
        public int? ProgramCourseId { get; set; }
        public int? RevisionId { get; set; }
        public ProgressStatus Status { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public int SemesterScheduleId { get; set; }
        public List<Timetable> TimetableCollection { get; set; }
        public virtual SemesterSchedule SemesterSchedule { get; set; }
        public virtual ProgramCourse ProgramCourse { get; set; }

        public void Start()
        {
            Status = ProgressStatus.InProgress;
        }

        public void Stop()
        {
            Status = ProgressStatus.Completed;
        }

    }
}
