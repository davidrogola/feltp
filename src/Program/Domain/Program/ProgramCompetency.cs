﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Program
{
    public class ProgramCompetency : IAuditItem
    {
        public ProgramCompetency()
        {

        }

        public ProgramCompetency(int programId, int competencyId, string createdBy) 
        {
            CompetencyId = competencyId;
            ProgramId = programId;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
        }

        public int Id { get; set; }
        public int CompetencyId { get; set; }
        public int ProgramId { get; set; }
        public virtual Program Program { get; set; }
        public virtual Competency Competency { get; set; }

        public DateTime DateCreated { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? DateDeactivated { get; set; }

        public string DeactivatedBy { get; set; }

        public void Deactivate(string deactivatedBy)
        {
            throw new NotImplementedException();
        }
    }
}
