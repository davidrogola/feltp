﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Program
{
    public class ProgramOfferLocation
    {
        public ProgramOfferLocation()
        {

        }
        public ProgramOfferLocation(int programOfferId, int programLocationId, string createdBy)
        {

            ProgramLocationId = programLocationId;
            ProgramOfferId = programOfferId;
            CreatedBy = createdBy;

        }
        public int Id { get; set; }
        public int ProgramLocationId { get; set; }
        public int ProgramOfferId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public virtual ProgramLocation ProgramLocation { get; set; }
        public virtual ProgramOffer ProgramOffer { get; set; }

        public void Deactivate(string deactivatedBy)
        {
            DateDeactivated = DateTime.Now;
            DeactivatedBy = deactivatedBy;
        }
        public void UpdateProgramOfferLocation (int programOfferId, int programLocationId)
        {
            ProgramLocationId = programLocationId;
            ProgramOfferId = programOfferId;
        }
    }
}
