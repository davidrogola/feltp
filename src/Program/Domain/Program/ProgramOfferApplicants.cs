﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Program
{
    public class ProgramOfferApplicants
    {
        public long Id { get; set; }
        public int ProgramOfferId { get; set; }
        public int ProgramId { get; set; }
        public string ProgramOfferName { get; set; }
        public string Program { get; set; }
        public long ApplicantId { get; set; }
        public ProgramQualificationStatus QualificationStatus { get; set; }
        public ApplicationStatus ApplicationStatus { get; set; }
        public DateTime DateApplied { get; set; }
        public DateTime DateCreated { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IdentificationNumber { get; set; }
        public string RegistrationNumber { get; set; }
        public string Cadre { get; set; }
        public bool ApprovedByCounty { get; set; }
        public bool PersonalStatementSubmitted { get; set; }
        public EnrollmentStatus EnrollmentStatus { get; set; }
        public long ? ResidentId { get; set; }
        public string EvaluatedBy { get; set; }
        public DateTime ? DateEvaluated { get; set; }
        public string Comment { get; set; }
        public string ShortListedBy { get; set; }
        public DateTime? DateShortListed { get; set; }
        public int ? EnrollmentId { get; set; }
        public long PersonId { get; set; }
        public string EmailAddress { get; set; }
        public DateTime ? DateEnrolled { get; set; }
        public string EnrolledBy { get; set; }
        public long ? ResidentCourseWorkId { get; set; }

        public ProgramApplicationProgressIndicator GenerateProgressIndicator()
        {
            var progressIndicator = new ProgramApplicationProgressIndicator();
            var invalidStages = new ProgressStage[] { ProgressStage.FailedEvaluation, ProgressStage.WaitingList };
            if (EnrollmentStatus == EnrollmentStatus.Enrolled)    // we have completed all the stages of application to a program
            {
                progressIndicator.InvalidStages = invalidStages;
                progressIndicator.CurrentStatus =  ProgressStage.Enrolled;
                progressIndicator.IsFinal = true;
                return progressIndicator;

            }

             if (ApplicationStatus == ApplicationStatus.Pending)
            {
                progressIndicator.CurrentStatus = ProgressStage.ApplicationSubmitted;
                progressIndicator.InvalidStages = invalidStages;

                return progressIndicator;
            }

             if(ApplicationStatus == ApplicationStatus.Shortlisted)
            {
                if(QualificationStatus == ProgramQualificationStatus.NotQualified)
                {
                    progressIndicator.CurrentStatus = ProgressStage.FailedEvaluation;
                    progressIndicator.IsFinal = true;
                }
                else if(QualificationStatus == ProgramQualificationStatus.Qualified)
                {
                    progressIndicator.CurrentStatus = ProgressStage.TrainingOffered;
                    progressIndicator.InvalidStages = invalidStages;
                }
                else if(QualificationStatus == ProgramQualificationStatus.Pending_Evaluation)
                {
                    progressIndicator.CurrentStatus = ProgressStage.Shortlisted;
                }

            }

            return progressIndicator;

        }

    }

    public class ProgramApplicationProgressIndicator
    {
        public IDictionary<ProgressStage, string> ProgressStatusDict
        {
            get { return ProgressDictionaryBuilder.BuildProgressDict(); }
            set
            {
            }
        }
        public ProgressStage [] InvalidStages { get; set; }
        public ProgressStage CurrentStatus { get; set; }
        public bool IsFinal { get; set; }

    }

    public enum ProgressStage
    {
        ApplicationSubmitted = 1,
        Shortlisted = 2,
        FailedEvaluation = 3,
        WaitingList = 4,
        TrainingOffered = 5,
        Enrolled = 6,
        ApplicationClosed = 7
    }

    public static class ProgressDictionaryBuilder
    {
        public static Dictionary<ProgressStage, string> BuildProgressDict()
        {
            var dict = new Dictionary<ProgressStage, string>
            {
                { ProgressStage.ApplicationSubmitted, "Application Submitted" },
                { ProgressStage.Shortlisted, "Shortlisted"},
                { ProgressStage.FailedEvaluation, "Failed Evaluation"},
                { ProgressStage.WaitingList, "Added to waiting list"},
                { ProgressStage.TrainingOffered, "Training program opportunity offered" },
                { ProgressStage.Enrolled, "Enrolled to program"},
                { ProgressStage.ApplicationClosed, "Application closed" }
            };

            return dict;
        }
    }
}
