﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Program
{
    public class ProgramOfferGeneralRequirement
    {
        public ProgramOfferGeneralRequirement()
        {

        }

        public ProgramOfferGeneralRequirement(int programOfferId, int workExperienceDuration,
            bool computerKnowledge,bool internalTravel,bool externalTravel, bool relocation,string createdBy)
        {
            ProgramOfferId = programOfferId;
            MinimumWorkExperienceDuration = workExperienceDuration;
            BasicComputerKnowledge = computerKnowledge;
            InternalTravelRequired = internalTravel;
            ExternalTravelRequired = externalTravel;
            WillingnessToRelocate = relocation;
            DateCreated = DateTime.Now;
            CreatedBy = createdBy;
        }
        public int Id { get; set; }
        public int ProgramOfferId { get; set; }
        public int MinimumWorkExperienceDuration { get; set; }
        public bool BasicComputerKnowledge { get; set; }
        public bool InternalTravelRequired { get; set; }
        public bool ExternalTravelRequired { get; set; }
        public bool WillingnessToRelocate { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public virtual ProgramOffer ProgramOffer { get; set; }

        public void SetProgramOffer(int programOfferId)
        {
            ProgramOfferId = programOfferId;
        }

        public void SetAuditInfo(string createdBy)
        {
            CreatedBy = createdBy;
        }
    }
}
