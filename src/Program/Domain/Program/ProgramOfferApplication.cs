﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProgramManagement.Domain.Program
{
    public enum ApplicationStatus
    {
        Pending,
        Shortlisted,
        All,
        NotShortlisted
    }

    public enum ProgramQualificationStatus
    {
        [Display(Name = "Pending Evaluation")]
        Pending_Evaluation = 0,
        Qualified = 1,
        [Display(Name = "Not Qualified")]
        NotQualified,
        [Display(Name = "Wating List")]
        Waiting_List
    }

    public enum EnrollmentStatus
    {
        Pending,
        Enrolled
    }

    public class ProgramOfferApplication
    {
        public ProgramOfferApplication()
        {

        }
        public ProgramOfferApplication(int offerId, long applicantId, ProgramQualificationStatus status,
            DateTime dateApplied, string createdBy, bool approved, bool statementSubmitted
            , ApplicationStatus applicationStatus)
        {
            ProgramOfferId = offerId;
            ApplicantId = applicantId;
            QualificationStatus = status;
            DateApplied = dateApplied;
            DateCreated = DateTime.Now;
            CreatedBy = createdBy;
            ApprovedByCounty = approved;
            PersonalStatementSubmitted = statementSubmitted;
            ApplicationStatus = applicationStatus;
        }

        public long Id { get; set; }
        public int ProgramOfferId { get; set; }
        public long ApplicantId { get; set; }
        public ProgramQualificationStatus QualificationStatus { get; set; }
        public ApplicationStatus ApplicationStatus { get; set; }
        public EnrollmentStatus EnrollmentStatus { get; set; }
        public DateTime DateApplied { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string ShortListedBy { get; set; }
        public DateTime? DateShortListed { get; set; }
        public DateTime? DateEvaluated { get; set; }
        public string EvaluatedBy { get; set; }
        public string Comment { get; set; }
        public bool ApprovedByCounty { get; set; }
        public bool PersonalStatementSubmitted { get; set; }
        public ProgramOffer ProgramOffer { get; set; }

        public void Evaluate(ProgramQualificationStatus status, string evaluatedBy, string comment, bool approvedByCounty)
        {
            DateEvaluated = DateTime.Now;
            EvaluatedBy = evaluatedBy;
            QualificationStatus = status;
            Comment = comment;
            ApprovedByCounty = approvedByCounty;
        }

        public void UpdateEnrollment()
        {
            EnrollmentStatus = EnrollmentStatus.Enrolled;
        }
    }
}
