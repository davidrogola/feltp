﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Program

{
    public class ProgramResource : IAuditItem
    {
        public ProgramResource()
        {

        }

        public ProgramResource(int resourceId, int programId, string createdBy)            
        {
            ResourceId = resourceId;
            ProgramId = programId;         
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;

        }

        public int Id { get; set; }
        public int ProgramId { get; set; }
        public int ResourceId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public virtual Program Program { get; set; }
        public virtual Resource Resource { get; set; }
        public void Deactivate(string deactivatedBy)
        {
            DeactivatedBy = deactivatedBy;
            DateDeactivated = DateTime.Now;
        }
    }
}
