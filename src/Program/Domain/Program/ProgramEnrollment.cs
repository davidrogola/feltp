﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Program
{
    public class ProgramEnrollment : IAuditItem 
    {
        public ProgramEnrollment()
        {

        }

        public ProgramEnrollment(int programOfferId, long residentId, string createdBy,long offerApplicationId) 
        {
            ProgramOfferId = programOfferId;
            ResidentId = residentId;
            DateCreated = DateTime.Now;
            CreatedBy = createdBy;
            ProgramOfferApplicationId = offerApplicationId;
        }

        public int Id { get; set; }
        public int ProgramOfferId { get; set; }
        public long ProgramOfferApplicationId { get; set; }
        public long ResidentId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public virtual ProgramOffer ProgramOffer { get; set; }
        public virtual ProgramOfferApplication ProgramOfferApplication { get; set; }
        public void Deactivate(string deactivatedBy)
        {
            throw new NotImplementedException();
        }
    }
}
