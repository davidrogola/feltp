﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Program
{
    public class Program  
    {
        public Program()
        {

        }

        public Program(string name,int programTierId, string code, string description, 
            int duration, bool hasmMonthlySeminars, string createdBy)
        {
            Name = name;
            ProgramTierId = programTierId;
            Code = code;
            Description = description;
            Duration = duration;
            HasMonthlySeminars = hasmMonthlySeminars;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
        }

        public int Id { get; private set; }
        public string Name { get; private set; }
        public int ProgramTierId { get; private set; }
        public string Code { get; private  set; }
        public string Description { get; private set; }
        public int Duration { get; private set; }
        public bool HasMonthlySeminars { get; private set; }
        public DateTime DateCreated { get; private set; }
        public string CreatedBy { get; private set; }
        public DateTime ? DateDeactivated { get; private set; }
        public string DeactivatedBy { get; private set; }
        public virtual  ProgramTier ProgramTier { get; private set; }
        public virtual ICollection<ProgramCourse> Courses { get; set; }


        public void SetCode(string code)
        {
            Code = code;
        }

        public void SetAuditInfomation(string createdBy)
        {
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
        }

        public void AddDuration(int durationInWeeks)
        {
            Duration = Duration + durationInWeeks;
        }

        public string ConvertDurationToMonths()
        {
            int totalWeeksInMonth = 4;
            decimal numberOfMonths = 0;
            int numberOfWeeks = 0;

            if (Duration == 0)
                return "Not Set";

            if (Duration < totalWeeksInMonth)
                return $"{Duration} Week(s)";

            numberOfMonths = Math.Floor(Convert.ToDecimal(Duration / totalWeeksInMonth));
            numberOfWeeks = Duration % totalWeeksInMonth;

            if(numberOfWeeks <= 0)
                  return $"{numberOfMonths} Month(s)";

             return $"{numberOfMonths} Month(s) {numberOfWeeks} Week(s)";

        }

        public void AdjustProgramDuration(int courseDuration)
        {
            Duration = Duration - courseDuration;
        }

        public void Update(string name, string code, string description, int programTierId, bool hasMonthlySeminars)
        {
            Name = name;
            Code = code;
            Description = description;
            ProgramTierId = programTierId;
            HasMonthlySeminars = hasMonthlySeminars;
        }
    }
}
