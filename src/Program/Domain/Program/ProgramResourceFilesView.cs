﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Program
{
    public class ProgramResourceFilesView 
    {
        public ProgramResourceFilesView()
        {

        }
        public ProgramResourceFilesView(int resourceId)
        {
            ResourceId = resourceId;
        }
      
        public int Id { get; set; }
        public string FileName { get; set; }
        public byte[] FileContent { get; set; }
        public string ContentType { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public int ResourceId { get; set; }

    }
}

 