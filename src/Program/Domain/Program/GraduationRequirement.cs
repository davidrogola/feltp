﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Program
{
    public class GraduationRequirement 
    {
        public GraduationRequirement()
        {

        }
        public GraduationRequirement(int programId, int noOfDidacticUnits, int noOfActivities, int passingGrade,
            int thesisDefenceDeliverables, string createdBy)
        {
            ProgramId = programId;
            NumberOfDidacticModulesRequired = noOfDidacticUnits;
            NumberOfActivitiesRequired = noOfActivities;
            PassingGrade = passingGrade;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
            NumberOfThesisDefenceDeliverables = thesisDefenceDeliverables;
        }
        public int Id { get; set; }
        public int ProgramId { get; set; }
        public int NumberOfDidacticModulesRequired { get; set; }
        public int NumberOfActivitiesRequired { get; set; }
        public int NumberOfThesisDefenceDeliverables { get; set; }
        public int PassingGrade { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public virtual Program Program { get; set; }
       
        public bool UpdateRequirementDetails(int noOfDidacticUnits, int noOfActivitiesRequired, int passingGrade, int noOfThesisDefenceDeliverables)
        {
            var stateModified = EntityStateModified(noOfDidacticUnits, noOfActivitiesRequired, passingGrade,noOfThesisDefenceDeliverables);

            NumberOfActivitiesRequired = noOfActivitiesRequired;
            NumberOfDidacticModulesRequired = noOfDidacticUnits;
            PassingGrade = passingGrade;
            NumberOfThesisDefenceDeliverables = noOfThesisDefenceDeliverables;

            return stateModified;
        }

        private bool EntityStateModified(int noOfDidacticUnits, int noOfActivitiesRequired, int passingGrade, int thesisDeliverables )
        {
            if (NumberOfActivitiesRequired != noOfDidacticUnits || PassingGrade != passingGrade
                || NumberOfActivitiesRequired != noOfActivitiesRequired || NumberOfThesisDefenceDeliverables != thesisDeliverables)
                return true;

            return false;
        }
    }
}
