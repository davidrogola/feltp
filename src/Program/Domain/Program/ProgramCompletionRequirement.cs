﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Program
{
    public class ProgramCompletionRequirement
    {
        public ProgramCompletionRequirement()
        {

        }
        public ProgramCompletionRequirement(int programOfferId, int completedProgramId,string createdBy)
        {
            ProgramOfferId = programOfferId;
            CompletedProgramId = completedProgramId;
            DateCreated = DateTime.Now;
            CreatedBy = createdBy;
        }
        public int Id { get; set; }
        public int ProgramOfferId { get; set; }
        public int CompletedProgramId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public virtual ProgramOffer ProgramOffer { get; set; }
        public virtual Program CompletedProgram { get; set; }
    }
}
