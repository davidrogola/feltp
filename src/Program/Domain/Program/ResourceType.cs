﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Program
{
    public class ResourceType
    {
        public ResourceType()
        {

        }
        public ResourceType(string name, string createdBy)
        {
            Name = name;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
        }
        public int Id { get; private set; }
        public string Name { get; private set; }
        public DateTime DateCreated { get; private set; }

        public string CreatedBy { get; private set; }

        public DateTime? DateDeactivated { get; private set; }

        public string DeactivatedBy { get; private set; }

        public void Deactivate(string deactivatedBy)
        {
            DeactivatedBy = deactivatedBy;
            DateDeactivated = DateTime.Now;
        }

        public void SetAuditInformation(string createdBy)
        {
            throw new NotImplementedException();
        }
    }
}
