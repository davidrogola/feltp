﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Program
{
    public class CompetencyDeliverable : IAuditItem 
    {
        public CompetencyDeliverable()
        {

        }
        public CompetencyDeliverable(int competencyId, int deliverableId, string createdBy)
        {
            CompetencyId = competencyId;
            DeliverableId = deliverableId;
            DateCreated = DateTime.Now;
            CreatedBy = createdBy;
        }

        public int Id { get; set; }
        public int CompetencyId { get; set; }
        public int DeliverableId { get; set; }

        public DateTime DateCreated { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? DateDeactivated { get; set; }

        public string DeactivatedBy { get; set; }
        public virtual Competency Competency { get; private set; }
        public virtual Deliverable Deliverable { get; private set; }

        public void Deactivate(string deactivatedBy)
        {
            DateDeactivated = DateTime.Now;
            DeactivatedBy = deactivatedBy;
        }

        public void SetAuditInformation(string createdBy)
        {
            throw new NotImplementedException();
        }
    }
}
