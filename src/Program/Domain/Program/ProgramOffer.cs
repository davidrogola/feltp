﻿using Common.Models;
using FacultyManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Program
{
    public class ProgramOffer  
    {
        public ProgramOffer()
        {

        }
        public ProgramOffer(int programId, DateTime startDate, int facultyId,string name,
            string createdBy, DateTime applicationStartDate, DateTime applicationEndDate)
        {
            ProgramId = programId;
            StartDate = startDate;
            FacultyId = facultyId;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
            Name = name;
            ApplicationStartDate = applicationStartDate;
            ApplicationEndDate = applicationEndDate;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int ProgramId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime ApplicationStartDate { get; set; }
        public DateTime ApplicationEndDate { get; set; }
        public int FacultyId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public virtual Program Program { get; set; }
        public virtual Faculty Faculty { get; set; }
        public virtual ICollection<ProgramOfferLocation> ProgramOfferLocations { get; set; }
        public virtual ICollection<ProgramOfferApplication> ProgramOfferApplications { get; set; }

        public void SetEndDate(int programDurationInWeeks)
        {
           const int daysInWeek = 7;

            var durationInDays = daysInWeek * programDurationInWeeks;

            EndDate =  StartDate.AddDays(durationInDays);
        }

        public void UpdateProgramOffer(int programId, DateTime startDate, int facultyId, string name,
             DateTime applicationStartDate, DateTime applicationEndDate)
        {
            ProgramId = programId;
            StartDate = startDate;
            FacultyId = facultyId;
            Name = name;
            ApplicationStartDate = applicationStartDate;
            ApplicationEndDate = applicationEndDate;
        }
    }
}
