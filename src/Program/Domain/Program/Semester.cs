﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Program
{
    public class Semester
    {
        public Semester()
        {

        }
        public Semester(string name, string createdBy)
        {
            Name = name;
            DateCreated = DateTime.Now;
            CreatedBy = createdBy;
        }

        public int Id { get; private set; }
        public string Name { get; private set; }
        public DateTime DateCreated { get; private set; }
        public string CreatedBy { get; private set; }
        public DateTime? DateDeactivated { get; private set; }
        public string DeactivatedBy { get; private set; }
        
        public void Update(string name)
        {
            Name = name;
        }

        public void Deactivate(string deactivatedBy)
        {
            DeactivatedBy = deactivatedBy;
            DateDeactivated = DateTime.Now;
        }
    }
}
