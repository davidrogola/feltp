﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Program
{
    public class Competency
    {
        public Competency()
        {

        }
        public Competency(string code, string name, string description, int durationInWeeks, string createdBy) 
        {
            Code = code;
            Name = name; 
            Description = description;
            DurationInWeeks = durationInWeeks;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int DurationInWeeks { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public virtual ICollection<CompetencyDeliverable> CompetencyDeliverables { get; set; }

        public void Deactivate(string deactivatedBy)
        {
            DateDeactivated = DateTime.Now;
            DeactivatedBy = deactivatedBy;
        }

        public void SetCode(string code)
        {
            Code = code;
        }

        public void SetAuditInfomation(string createdBy)
        {
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
        }

        public void SetDuration(int durationInWeeks)
        {
            DurationInWeeks = DurationInWeeks + durationInWeeks;
        }

        public void SubtractDuration(int durationInWeeks)
        {
            DurationInWeeks = DurationInWeeks - durationInWeeks;
            if (DurationInWeeks < 0)
                DurationInWeeks = 0;
        }
        public List<CompetencyDeliverable> AddDeliverables(int[] deliverables, string createdBy)
        {
            var competencyDeliverables = new List<CompetencyDeliverable>();
            foreach (var deliverable in deliverables)
            {
                var competencyDeliverable = new CompetencyDeliverable(Id, deliverable, createdBy);
                competencyDeliverables.Add(competencyDeliverable);
            }
            return competencyDeliverables;
        }

        public void UpdateCompetency (string name, string description)
        {
            Name = name;
            Description = description;
        }

    }
}
