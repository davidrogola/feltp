﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Program
{
    public class Deliverable 
    {
        public Deliverable()
        {

        }
        public Deliverable(string code, string name, string description, int durationInWeeks, string createdBy,bool hasThesisDefence)
        {
            Code = code;
            Name = name;
            Description = description;
            DurationInWeeks = durationInWeeks;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
            HasThesisDefence = hasThesisDefence;
 
        }
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int DurationInWeeks { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public bool HasThesisDefence { get; set; }
        public void SetCode(string code)
        {
            Code = code;
        }

        public void SetAuditInfomation(string createdBy)
        {
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
        }

        public void Update(string code,string name,string description,int duration,bool hasThesisDefence)
        {
            Code = code;
            Name = name;
            Description = description;
            DurationInWeeks = duration;
            HasThesisDefence = hasThesisDefence;
        }

        public void Deactivate(string deactivatedBy)
        {
            DeactivatedBy = deactivatedBy;
            DateDeactivated = DateTime.Now;
        }
    }
}
