﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Program
{
    public class SemesterScheduleChangeRequest
    {
        public SemesterScheduleChangeRequest()
        {

        }
        public SemesterScheduleChangeRequest(int semesterScheduleId, string requestedBy, DateTime startDate,
            DateTime endDate, string comment,string originalValues)
        {
            SemesterScheduleId = semesterScheduleId;
            RequestedBy = requestedBy;
            StartDate = startDate;
            EndDate = endDate;
            Comment = comment;
            ApprovalStatus = ApprovalStatus.Pending;
            DateRequested = DateTime.Now;
            OriginalValues = originalValues;
        }

        public int Id { get; set; }
        public int SemesterScheduleId { get; set; }
        public string RequestedBy { get; set; }
        public DateTime DateRequested { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string OriginalValues { get; set; }
        public string Comment { get; set; }
        public ApprovalStatus ApprovalStatus { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovalReason { get; set; }
        public DateTime ? DateApproved { get; set; }
        public virtual SemesterSchedule SemesterSchedule  { get; set; }
        
        public void ActionRequest(ApprovalStatus approvalStatus, string actionedBy, string approvalReason)
        {
            ApprovalStatus = approvalStatus;
            ApprovedBy = actionedBy;
            DateApproved = DateTime.Now;
            ApprovalReason = approvalReason;
        }
    }
}
