﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProgramManagement.Domain.Program
{
    public class Workshop : IAuditItem
    {
        public Workshop()
        {

        }
        public Workshop(string name, string description, int duration, string createdBy)
        {
            Name = name;
            Description = description;
            Duration = duration;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Duration { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public void Deactivate(string deactivatedBy)
        {
            throw new NotImplementedException();
        }
    }
}
