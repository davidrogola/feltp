﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Azure.KeyVault;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using UserManagement.ConfigurationStrore.Entities;
using UserManagement.ConfigurationStrore.StoreImp;
using UserManagement.Domain;
using UserManagement.Services;

namespace FELTP.IdentityServer
{
    public class Startup
    {

        public Startup(IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            Configuration = configuration;
            Environment = hostingEnvironment;
        }

        public IConfiguration Configuration { get; }

        public IHostingEnvironment Environment { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(Configuration);

            services.AddDbContext<IdentityServerDbContext>
              (options => options.UseMySql(Configuration.GetConnectionString("user-management")));

            services.AddIdentity<ApplicationUser, ApplicationRole>()
                 .AddEntityFrameworkStores<IdentityServerDbContext>()
                 .AddDefaultTokenProviders();
            services.AddMvc();

            services.AddIdentityConfigurationStore(Configuration);
            var identityServiceBuilder = services.AddIdentityServer();
            //if (Environment.IsDevelopment())
            //{
                identityServiceBuilder.AddDeveloperSigningCredential();
            //}
            /*else
            {
                var key = Configuration["feltp-cert"];
                var pfxBytes = Convert.FromBase64String(key);
                var cert = new X509Certificate2(pfxBytes, (string)null, X509KeyStorageFlags.MachineKeySet);
                identityServiceBuilder.AddSigningCredential(cert);
            }*/

            identityServiceBuilder
               .AddClientStore<ClientStore>()
               .AddResourceStore<ResourceStore>()
               .AddAspNetIdentity<ApplicationUser>()
               .AddProfileService<CustomClaimsProfileService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,IdentityServerDbContext identityServerDbContext)
        {
           
           // identityServerDbContext.Database.EnsureCreated();
            app.SeedIdentityConfigurationStore(Configuration, identityServerDbContext);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseIdentityServer();
            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
        }
    }
}
