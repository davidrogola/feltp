﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using MediatR;
using Newtonsoft.Json;

using Application.Common.Models;
using UserManagement.Domain;
using UserManagement.ViewModels;
using static Application.Common.Services.ApiErrorModelBuilder;
using Microsoft.AspNetCore.Authorization;
using Application.Common.Services;
using Common.Domain.Person;
using System.Collections.Generic;
using UserManagement.ConfigurationStrore.Entities;

namespace UserManagementAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    public class UserManagementController : Controller
    {
        UserManager<ApplicationUser> userManager;
        IdentityServerDbContext identityDbContext;
        IMediator mediator;
        public UserManagementController(UserManager<ApplicationUser> _userManager,
            IdentityServerDbContext _identityDbContext,IMediator _mediator)
        {
            userManager = _userManager;
            identityDbContext = _identityDbContext;
            mediator = _mediator;
        }

        [HttpPost]
        public async Task<ApiResponse> CreateNewUser([FromBody]RegisterSspUserModel userModel)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Valdation errors occured on creating the user", null, false, BuildModelFromModelStateErrors(ModelState.Values.SelectMany(x => x.Errors)));

            var emailSplit = userModel.Email.Split("@");

            var user = new ApplicationUser()
            {
                UserName = userModel.UserName,
                Email = userModel.Email,
                PhoneNumber = userModel.PhoneNumber,
                CreatedById = userModel.CreatedByUserId,
                DateCreated = DateTime.Now,
                LastPasswordChangeDate = DateTime.Now,
                LockoutEnabled = false,
                UserStatus = ApplicationUserStatus.UnConfirmed,
                IdentificationNumber = userModel.IdentificationNumber,
                CorrelationId = userModel.CorrelationId,
                IdentificationType = userModel.IdentificationType
            };

            var createUserResult = await userManager.CreateAsync(user);

            if (createUserResult.Succeeded)
            {

                var code = await userManager.GenerateEmailConfirmationTokenAsync(user);

                return new ApiResponse("User created successfully", JsonConvert.SerializeObject(new
                {
                    UserId = user.Id,
                    user.UserName,
                    EmailAddress = user.Email,
                    EmailConfirmationToken = code,
                }), true);

            }

            return new ApiResponse($"An error occured while creating user account with email address {userModel.Email}",
                null, false, BuildErrorModelFromIdentityErrors(createUserResult.Errors));
        }



        [HttpGet("{userId}")]
        public object GetUserDetails(int userId)
        {
            var user = identityDbContext.Set<ApplicationUser>().AsNoTracking()
                .Where(x => x.Id == userId).Select(x => new UserDetailsModel
                {
                    Id = x.Id,
                    CreatedBy = x.CreatedBy.UserName,
                    CreatedDate = x.DateCreated.ToShortDateString(),
                    EmailAddress = x.Email,
                    UserName = x.UserName,
                    PhoneNumber = x.PhoneNumber,
                    Status = x.UserStatus,
                    LockoutEnabled = x.LockoutEnabled,
                    LockoutEndDate = x.LockoutEnd,
                    IsActive = true,
                    IdentificationNumber = x.IdentificationNumber,
                    IdentificationType = x.IdentificationType.HasValue ? x.IdentificationType.Value : 0
                }).SingleOrDefault();

            if (user == null)
                return new ApiResponse($"User details with id {userId} not found", null, false);

            if (user.LockoutEndDate.HasValue)
            {
                var maxDate = DateTime.MaxValue.Date;
                var lockoutDate = user.LockoutEndDate.Value.Date;
                if (user.LockoutEnabled == true && lockoutDate == maxDate)
                    user.IsActive = false;
            }

            return new ApiResponse("User details succesfully retrieved", JsonConvert.SerializeObject(user), true);
        }

        [HttpGet]
        public object GetAllUsers()
        {
            var users = identityDbContext.Set<ApplicationUser>().AsNoTracking()
                .Select(x => new UserDetailsModel
                {
                    Id = x.Id,
                    CreatedBy = x.CreatedBy.UserName,
                    CreatedDate = x.DateCreated.ToShortDateString(),
                    EmailAddress = x.Email,
                    UserName = x.UserName,
                    PhoneNumber = x.PhoneNumber,
                    Status = x.UserStatus,
                    LockoutEnabled = x.LockoutEnabled,
                    LockoutEndDate = x.LockoutEnd,
                    IsActive = true
                }).ToList();

            if (!users.Any())
                return new ApiResponse("Users not found", null, false);

            return new ApiResponse("Users fetched succesfully", JsonConvert.SerializeObject(users), true);

        }

        [HttpPost]
        public async Task<object> ActivateEmail([FromBody] SetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Model state validation failed on email activation request", null, false, BuildModelFromModelStateErrors(ModelState.Values.SelectMany(x => x.Errors)));

            var user = await userManager.FindByNameAsync(model.UserName);
            if (user == null)
                return new ApiResponse($"User with username :{model.UserName} not found", null, false);

            var result = await userManager.ConfirmEmailAsync(user, model.Code);
            if (!result.Succeeded)
                return new ApiResponse($"Invalid user token code", null, false, BuildErrorModelFromIdentityErrors(result.Errors));

            user.PasswordHash = userManager.PasswordHasher.HashPassword(user, model.Password);
            user.UserStatus = ApplicationUserStatus.Confirmed;
            await userManager.UpdateAsync(user);

            return new ApiResponse("User email address activated successfully",
                JsonConvert.SerializeObject(new { UserId = user.Id, user.UserName }), true);
        }

        [HttpPost]
        public async Task<object> EditUser([FromBody] EditUserModel editUserModel)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Model state validation failed on edit user request", null, false, BuildModelFromModelStateErrors(ModelState.Values.SelectMany(x => x.Errors)));

            var user = await userManager.FindByNameAsync(editUserModel.UserName);

            var code = await userManager.GenerateEmailConfirmationTokenAsync(user);
            var emailSplit = editUserModel.Email.Split("@");

            bool emailChanged = user.Email != editUserModel.Email;
            bool emailConfirmed = user.EmailConfirmed;

            user.UpdateUserDetails(emailSplit[0], editUserModel.Email, editUserModel.PhoneNumber);

            var result = await userManager.UpdateAsync(user);
            return new ApiResponse(result.Succeeded ? "Users details edited successfully" : "An error occured while editing user details",
                JsonConvert.SerializeObject(new
                {
                    EmailChanged = emailChanged,
                    EmailConfirmed = emailConfirmed,
                    user.UserName,
                    IdentityResult = result,
                    EmailConfirmationToken = code,
                    EmailAddress = user.Email
                }), result.Succeeded, BuildErrorModelFromIdentityErrors(result.Errors));
        }

        [HttpPost]
        public async Task<object> GenerateResetPasswordToken([FromBody]ForgotPasswordViewModel forgotPasswordModel)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Model state validation failed for change password request", null, false,
                    BuildModelFromModelStateErrors(ModelState.Values.SelectMany(x => x.Errors)));

            var user = await userManager.FindByEmailAsync(forgotPasswordModel.Email);
            if (user == null)
            {
                return new ApiResponse($"User details with email address { forgotPasswordModel.Email } not found", null, false);
            }

            if (!user.EmailConfirmed)
            {
                return new ApiResponse("User email address is not confirmed", null, false);
            }

            var token = await userManager.GeneratePasswordResetTokenAsync(user);

            return new ApiResponse("Reset password token generated succesfully",
                JsonConvert.SerializeObject(new
                {
                    EmailAddress = user.Email,
                    user.UserName,
                    PasswordResetToken = token
                }), true);
        }

        [HttpPost]
        public async Task<object> ResetUserPassword([FromBody] ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Model state validation failed for reset user password request", null, false,
                    BuildModelFromModelStateErrors(ModelState.Values.SelectMany(x => x.Errors)));

            var user = await userManager.FindByNameAsync(model.UserName);
            if (user == null)
                return new ApiResponse($"User with email address {user.Email} not found", null, false);

            var result = await userManager.ResetPasswordAsync(user, model.Code, model.Password);

            if (!result.Succeeded)
                return new ApiResponse("An error occured while reseting user password", null, false,
                    BuildErrorModelFromIdentityErrors(result.Errors));


            return new ApiResponse("User password reset succesfully",
                JsonConvert.SerializeObject(new { user.UserName, EmailAddress = user.Email }), true);
        }

        [HttpPost]
        public async Task<object> SetUserCorrelationValue([FromBody]UpdateUserCorrelationModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Validation failed while updating user correlation details", null, false,
                   BuildModelFromModelStateErrors(ModelState.Values.SelectMany(x => x.Errors)));

            var user = await userManager.FindByIdAsync(model.UserId).ConfigureAwait(false);

            if (user == null)
                return new ApiResponse("User details not found", null, false,
                  new List<Error>() { new Error { ErrorCode = "err", ErrorMessage = "User details not found" } });

            user.SetCorrelationId(model.CorrelationId);
            var result = await userManager.UpdateAsync(user);

            await userManager.AddClaimAsync(user, new Claim("ApplicantId", model.CorrelationId));

            var successful = !result.Errors.Any();

            return Ok(new ApiResponse(successful ? "User correlationId updated succesfully" : "User correlationId update failed", JsonConvert.SerializeObject(new { }), successful, successful ? null : BuildErrorModelFromIdentityErrors(result.Errors)));

        }

        [HttpPost]
        public async Task<object> GenerateExistingUserAccountActivationToken([FromBody]ExistingApplicantAccountActivationModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Validation failed existing user account validation", null, false, BuildModelFromModelStateErrors(ModelState.Values.SelectMany(x => x.Errors)));

            var user = await userManager.FindByEmailAsync(model.EmailAddress);
            if (user == null)
                return new ApiResponse($"User with email address {model.EmailAddress} not found", null, false, new List<Error>() { new Error { ErrorCode = "User", ErrorMessage = $"User with email address {model.EmailAddress } not found" } });

            var activationToken = await userManager.GenerateEmailConfirmationTokenAsync(user);

            return Ok(new ApiResponse("User account activation token generated successfully", 
                JsonConvert.SerializeObject(new { user.UserName, model.EmailAddress, UserId = user.Id, ActivationToken = activationToken }), true));
        }
    }
}