﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Application.Common.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using UserManagement.ConfigurationStrore.Entities;
using UserManagement.Domain;
using UserManagement.ViewModels;
using static Application.Common.Services.ApiErrorModelBuilder;

namespace UserManagementAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    public class RoleManagementController : Controller
    {
        private readonly RoleManager<ApplicationRole> roleManager;
        private readonly UserManager<ApplicationUser> userManager;
        IdentityServerDbContext identityDbContext;

        public RoleManagementController(RoleManager<ApplicationRole> _roleManager,
            UserManager<ApplicationUser> _userManager, IdentityServerDbContext _identityDbContext)
        {
            roleManager = _roleManager;
            userManager = _userManager;
            identityDbContext = _identityDbContext;
        }



        [HttpPost]
        public object AddRole([FromBody]RoleViewModel model)
        {
           using(var transaction = identityDbContext.Database.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                if (!ModelState.IsValid)
                    return new ApiResponse("Model state validation failed on add role request", null, false,
                        BuildModelFromModelStateErrors(ModelState.Values.SelectMany(x => x.Errors)));

                var applicationRole = new ApplicationRole(model.RoleName, model.CreatedById);

                identityDbContext.Set<ApplicationRole>().Add(applicationRole);
                identityDbContext.SaveChanges();

                var rolePermissionMappings = model.Permissions
                     .Select(x => new RolePermissionMapping
                     {
                         RoleId = applicationRole.Id,
                         PermissionId = x,
                         CreatedBy = model.CreatedBy,
                         DateCreated = DateTime.Now,
                     }).ToList();

                identityDbContext.Set<RolePermissionMapping>().AddRange(rolePermissionMappings);
                identityDbContext.SaveChanges();

                var permissionNames = identityDbContext.Set<Permission>().AsNoTracking()
                    .Where(x => model.Permissions.Contains(x.Id)).Select(x => x.Name).ToList();

                transaction.Commit();
                return new ApiResponse("Role added successfully", JsonConvert.SerializeObject(new { model.RoleName, Permissions = permissionNames, RoleId = applicationRole.Id }), true);
            }           
        }

        [HttpGet]
        public object GetPermissionsList()
        {
            var permissions = identityDbContext.Set<Permission>().AsNoTracking()
               .AsQueryable().Select(x => new OperationViewModel
               {
                   CreatedBy = x.CreatedBy.UserName ?? "System",
                   DateCreated = x.DateCreated.ToShortDateString(),
                   Id = x.Id,
                   Name = x.Name
               }).ToList();

            return Ok(permissions);
        }

        [HttpGet]
        public object GetRoles()
        {
            var roles = identityDbContext.Set<ApplicationRole>().AsNoTracking()
                .AsQueryable().Select(x => new RolesList
                {
                    CreatedBy = x.CreatedBy.UserName,
                    DateCreated = x.DateCreated.ToShortDateString(),
                    Id = x.Id,
                    RoleName = x.Name
                }).ToList();

            return new ApiResponse(roles.Any() ? "Roles fetched succesfully" : "Roles not found", JsonConvert.SerializeObject(roles), true);
        }

        [HttpGet("{UserId}")]
        public object GetUserRoles(int userId)
        {
            var userRoles = identityDbContext.Set<UserRoleView>().AsNoTracking()
             .Where(x => x.UserId == userId)
             .Select(x => new UserRoleViewModel
             {
                 AddedToRoleBy = x.AddedToRoleBy,
                 DateAddedToRole = x.DateAddedToRole.ToShortDateString(),
                 UserId = x.UserId,
                 RoleId = x.RoleId,
                 RoleName = x.RoleName,
                 Status = x.DateRemovedFromRole == null ? UserRoleStatus.Active.ToString()
                 : UserRoleStatus.InActive.ToString()
             }).ToList();

            return new ApiResponse(userRoles.Any() ? "User roles fetched successfully" : "User roles not found",
             JsonConvert.SerializeObject(userRoles), userRoles.Any());
    }

        [HttpPost]
        public async Task<object> AddUserToRole([FromBody] AddUserToRoleViewModel model)
        {
            if (!ModelState.IsValid)
                return new ApiResponse("Model state validation failed on add user to role request", null, false, 
                    BuildModelFromModelStateErrors(ModelState.Values.SelectMany(x => x.Errors)));

            var createdBy = userManager.GetUserId(User);

            var user = await userManager.FindByIdAsync(model.UserId.ToString());
            if (user == null)
                return new ApiResponse("User details not found", null, false);

            var userRoles = model.RoleIds.Select(x => new ApplicationUserRole
            {
                DateAddedToRole = DateTime.Now,
                RoleId = x,
                UserAddedToRoleById = Convert.ToInt16(createdBy),
                UserId = model.UserId,
            }).ToList();

            identityDbContext.Set<ApplicationUserRole>().AddRange(userRoles);
            identityDbContext.SaveChanges();

            var claims = identityDbContext.Set<ApplicationRole>().AsNoTracking()
                .Where(x => model.RoleIds.Contains(x.Id))
                .Select(x => new Claim(CustomClaimType.Role.ToString(), x.Name)).ToList();

            await userManager.AddClaimsAsync(user, claims);

            return new ApiResponse($"Roles successfully assigned to user {user.UserName}",
                JsonConvert.SerializeObject(new { UserId = user.Id, EmailAddress = user.Email, user.UserName }), true);
        }

        [HttpPost]
        public async Task<object> ActivateUserRole([FromBody]UserRoleView model)
        {
            var userRole = identityDbContext.UserRoles.Where(x => x.UserId == model.UserId && x.RoleId == model.RoleId).SingleOrDefault();

            if (userRole == null)
                return new ApiResponse($"RoleId {model.RoleId} for user with Id {model.UserId} not found",null,false);

            userRole.ActivateRole();
            identityDbContext.Set<ApplicationUserRole>().Attach(userRole);
            identityDbContext.Entry(userRole).State = EntityState.Modified;
            identityDbContext.SaveChanges();

            var user = await userManager.FindByIdAsync(model.UserId.ToString());

            var claims = identityDbContext.Set<ApplicationRoleClaim>().AsNoTracking()
                .Where(x => x.RoleId == model.RoleId)
                .Select(x => new Claim(x.ClaimType, x.ClaimValue)).ToList();

            await userManager.AddClaimsAsync(user, claims);

            return new ApiResponse("User role successfully activated", JsonConvert.SerializeObject(new { UserId = user.Id, user.UserName }), true);
        }

        [HttpPost]
        public async Task<object> EditRole([FromBody]RoleViewModel model)
        {
            var rolePermissionMappings = identityDbContext.Set<RolePermissionMapping>()
                .Where(x => x.RoleId == model.RoleId).ToList();

            List<string> removedPemissionNames = null;
            List<string> newPermissionNames = null;

            var role = await roleManager.FindByIdAsync(model.RoleId.ToString());
            if (role == null)
                return new ApiResponse($"Role with Id {model.RoleId} not found",null,false);

            role.Update(model.RoleName);
            await roleManager.UpdateAsync(role);

            var existingPermissions = rolePermissionMappings.Select(x => x.PermissionId).ToList();

            var newPermissions = model.Permissions?.Except(existingPermissions);

            if (newPermissions.Any())
            {

                var roleMappings = newPermissions.Select(x => new RolePermissionMapping
                {
                    RoleId = role.Id,
                    CreatedBy = User.Identity.Name,
                    PermissionId = x,
                    DateCreated = DateTime.Now
                }).ToList();

                identityDbContext.Set<RolePermissionMapping>().AddRange(roleMappings);
                identityDbContext.SaveChanges();

                newPermissionNames = identityDbContext.Set<Permission>()
                   .Where(x => newPermissions.Contains(x.Id)).Select(x => x.Name).ToList();
            }

            var removedPermissions = model.Permissions == null ? existingPermissions : existingPermissions.Except(model.Permissions);

            if (removedPermissions.Any())
            {
                var removedRoleMappings = identityDbContext.Set<RolePermissionMapping>()
                    .Where(x => removedPermissions.ToList().Contains(x.PermissionId))
                    .Include(x => x.Permission)
                    .ToList();

                 removedPemissionNames = removedRoleMappings.Select(x => x.Permission.Name).ToList();

                identityDbContext.Set<RolePermissionMapping>().RemoveRange(removedRoleMappings);
                identityDbContext.SaveChanges();             
            }

            return new ApiResponse("Role details successfully edited",
                JsonConvert.SerializeObject(new
                {
                    RoleName = role.Name,
                    RoleId = role.Id,
                    RemovedPermissions = removedPemissionNames,
                    NewPermissions = newPermissionNames
                }),true);
        }

        [HttpGet("{roleId}")]
        public object GetRolePermissions(int roleId)
        {
            var rolePermissions = identityDbContext.Set<RolePermissionsView>().AsNoTracking()
              .Where(x => x.RoleId == roleId)
              .Select(x => new RoleOperationsViewModel
              {
                  Description = x.Description,
                  Permission = x.Permission,
                  RoleId = x.RoleId,
                  RoleName = x.RoleName
              }).ToList();

            return new ApiResponse("Role permissions fetched successfully", JsonConvert.SerializeObject(rolePermissions), true);
        }

        [HttpGet]
        public object GetAllRolePermissions()
        {
            var rolePermissions = identityDbContext.Set<RolePermissionsView>().AsNoTracking()
              .Select(x => new RoleOperationsViewModel
              {
                  Description = x.Description,
                  Permission = x.Permission,
                  RoleId = x.RoleId,
                  RoleName = x.RoleName
              }).ToList();

            return new ApiResponse("Role permissions fetched successfully", JsonConvert.SerializeObject(rolePermissions), true);
        }
    }
}