﻿using MediatR;
using FluentValidation.AspNetCore;


using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Application.Common.Notifications;
using Application.Common.Services;
using UserManagement.Domain;
using UserManagement.Validators;
using static Application.Common.Services.IdentityConfig;
using UserManagement.Services;
using System;
using UserManagement.ConfigurationStrore.Entities;

namespace UserManagementAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(Configuration);
            services.AddDbContext<IdentityServerDbContext>
             (options => options.UseMySql(Configuration.GetConnectionString("user-management")));

            services.AddIdentity<ApplicationUser, ApplicationRole>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 6;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = false;
                options.Password.RequiredUniqueChars = 3;

                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                // User settings
                options.User.RequireUniqueEmail = true;

            }).AddEntityFrameworkStores<IdentityServerDbContext>()
              .AddDefaultTokenProviders();

            services.AddMvcCore(options =>
            {

                options.Filters.Add(typeof(WebApiCustomExceptionFilter));
            })
            .AddFluentValidation(fv =>
            {
                fv.RegisterValidatorsFromAssemblyContaining<ApplicationRoleValidator>();
            })
            .AddAuthorization()
            .AddJsonFormatters();

            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication(options =>
                {
                    options.ApiName = "UserManagementAPI";
                    options.Authority = GetIdentityServerEndPoint(Configuration);
                    options.RequireHttpsMetadata = false;
                });
            services.AddMediatR(typeof(UserCreatedNotification).Assembly);
            services.AddSingleton(typeof(EmailTemplateFinder));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
