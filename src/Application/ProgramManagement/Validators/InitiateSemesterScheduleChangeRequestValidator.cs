﻿using Application.Common.Services;
using Application.ProgramManagement.Commands;
using FluentValidation;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using static Application.Common.Services.DateTimeParser;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ProgramManagement.Validators
{
    public class InitiateSemesterScheduleChangeRequestValidator : AbstractValidator<InitiateSemesterScheduleChangeRequest>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public InitiateSemesterScheduleChangeRequestValidator(IProgramManagementUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;

            RuleFor(x => x.SemesterScheduleId).Must((int scheduleId) =>
              {
                  if (scheduleId == 0)
                      return true;
                  var duplicateRequestExists = unitOfWork.ProgramManagementRepository.FindBy<SemesterScheduleChangeRequest>
                  (x => x.SemesterScheduleId == scheduleId && x.ApprovalStatus == ApprovalStatus.Pending).Any();
                  return !duplicateRequestExists;
              }).WithMessage("A semester schedule date adjustment with pending approval already exists");

            RuleFor(x => x).Custom((model, context) =>
            {
                var semesterSchedule = unitOfWork.ProgramManagementRepository
                .FindBy<SemesterSchedule>(x => x.Id == model.SemesterScheduleId).SingleOrDefault();
                if (semesterSchedule == null)
                    return;
                if(model.Status == ProgressStatus.Completed)
                    context.AddFailure("You can not change the schedule of a completed semester");                 

                if (model.Status == ProgressStatus.InProgress && !String.Equals(model.StartDate,semesterSchedule.EstimatedStartDate.ToShortDateString()))
                    context.AddFailure("You can not change start date while the semester is in progress");

                if(model.StartDate.ToLocalDate() < model.OfferStartDate.ToLocalDate())
                    context.AddFailure($"Semester schedule start date cannot be less than the program start date of {model.OfferStartDate}");

                var newDurationInDays = model.EndDate.ToLocalDate().Subtract(model.StartDate.ToLocalDate()).Days;
                var daysInSemester = GetDaysInWeekDuration(model.Duration);
                if (newDurationInDays < daysInSemester)
                    context.AddFailure($"The slected duration can not be less than entire course duration{model.Duration}");
            });
        }
    }
}
