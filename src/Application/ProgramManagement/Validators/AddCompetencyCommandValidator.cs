﻿using Application.ProgramManagement.Commands;
using FluentValidation;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ProgramManagement.Validators
{
    public class AddCompetencyCommandValidator : AbstractValidator<AddCompetencyCommand>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public AddCompetencyCommandValidator(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
            RuleFor(x => x.Name).NotNull().WithMessage(String.Format(ProgramManagementResource.NameIsRequired, "Competency"));
            RuleFor(x => x.Description).NotNull().WithMessage(ProgramManagementResource.DescriptionRequired);
            RuleFor(x => x.Name).Must((string name) =>
            {
                if (string.IsNullOrEmpty(name))
                    return true;
                var duplicateExists = unitOfWork.ProgramManagementRepository.FindBy<Competency>(x => x.Name == name)
                .Any();
                return !duplicateExists;
            }).WithMessage(String.Format(ProgramManagementResource.DuplicateExists, "Competency"));

        }
    }
}
