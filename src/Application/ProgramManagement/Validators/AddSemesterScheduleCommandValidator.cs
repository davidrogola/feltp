﻿using Application.Common.Services;
using Application.ProgramManagement.Commands;
using FluentValidation;
using Microsoft.Extensions.Configuration;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Application.ProgramManagement.Validators
{
    public class AddSemesterScheduleCommandValidator : AbstractValidator<AddSemesterScheduleCommand>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public AddSemesterScheduleCommandValidator(IProgramManagementUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;

            RuleFor(x => x.ProgramOfferId).NotEqual(0).WithMessage(ProgramManagementResource.ProgramOfferRequired);
            RuleFor(x => x.SemesterId).NotEqual(0).WithMessage(ProgramManagementResource.SemesterRequired);
            RuleFor(x => x.StrEstimatedStartDate).NotNull().WithMessage(ProgramManagementResource.StartDateRequired);
            RuleFor(x => x.StrEstimatedEndDate).NotNull().WithMessage(ProgramManagementResource.EndDateRequired);

            RuleFor(x => x).Custom((command, context) =>
            {
                if (command == null)
                {
                    context.AddFailure(ProgramManagementResource.FillRequiredFields);
                    return;
                }

                var startDate = command.StrEstimatedStartDate.ToLocalDate().Date;
                var endDate = command.StrEstimatedEndDate.ToLocalDate().Date;

                if (endDate < startDate)
                    context.AddFailure(ProgramManagementResource.DateValidator);

                var offer = unitOfWork.ProgramManagementRepository.Get<ProgramOffer>(command.ProgramOfferId);
                if (offer != null)
                    if (startDate < offer.StartDate.Date)
                        context.AddFailure(string.Format(ProgramManagementResource.SemesterStartDateValidator, offer.StartDate.ToShortDateString()));
            });


            RuleFor(x => x).Custom((model, context) =>
            {

                if (model.SemesterId == 0 || model.ProgramOfferId == 0)
                {
                    context.AddFailure(ProgramManagementResource.FillRequiredFields);
                    return;
                }
                var semester = unitOfWork.ProgramManagementRepository.Get<Semester>(model.SemesterId);
                var offer = unitOfWork.ProgramManagementRepository.Get<ProgramOffer>(model.ProgramOfferId);

                var hasExisting = unitOfWork.ProgramManagementRepository.FindBy<SemesterSchedule>(x => x.ProgramOfferId == model.ProgramOfferId && x.SemesterId == model.SemesterId).Any();
                if (hasExisting)
                    context.AddFailure($"{semester.Name} schedule for {offer.Name} already exists");

                var activeSemesterSchedule = unitOfWork.ProgramManagementRepository
                .FindByWithInclude<SemesterSchedule>(x => x.ProgramOfferId == model.ProgramOfferId && x.Status != ProgressStatus.Completed,x=>x.Semester).FirstOrDefault();

                if(activeSemesterSchedule != null)
                    context.AddFailure($"{activeSemesterSchedule.Semester.Name} schedule for {offer.Name} exists with a status of    {activeSemesterSchedule.Status.ToString()}" + $". Please complete the semester schedule before creating another");
            });

        }
    }
}
