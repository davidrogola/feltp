﻿using Application.Common.Services;
using Application.FacultyManagement;
using Application.ProgramManagement.Commands;
using FluentValidation;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ProgramManagement.Validators
{
       public class AddScheduleItemTimetableCommandValidator : AbstractValidator<AddScheduleItemTimetableCommand>
        {
            IProgramManagementUnitOfWork unitOfWork;
            public AddScheduleItemTimetableCommandValidator(IProgramManagementUnitOfWork _unitOfWork)
            {
                unitOfWork = _unitOfWork;
                RuleFor(x => x.StartDate).NotNull().WithMessage(ProgramManagementResource.CourseTimetableStartDateRequired);
                RuleFor(x => x.EndDate).NotNull().WithMessage(ProgramManagementResource.CourseTimetableEndDateRequired);
                RuleFor(x => x.TimetableItem.FacultyId).NotEqual(0).WithMessage(FacultyManagementResource.SelectFaculty);
                RuleFor(x => x.TimetableItem.StartTime).NotNull().WithMessage(ProgramManagementResource.StartTimeIsRequired);
                RuleFor(x => x.TimetableItem.EndTime).NotNull().WithMessage(ProgramManagementResource.EndTimeIsRequired);

                RuleFor(x => x).Custom((command, context) =>
                {
                    if (command == null)
                    {
                        context.AddFailure(ProgramManagementResource.FillRequiredFields);
                        return;
                    }

                    if(command.TimetableType == TimetableType.Unit && command.TimetableItem.UnitId == 0 )
                        context.AddFailure(ProgramManagementResource.SelectUnit);


                    if (command.TimetableType == TimetableType.Examination && command.TimetableItem.ExaminationId == 0)
                        context.AddFailure(ProgramManagementResource.SelectExam);
                   
                    if(command.TimetableItem.StartTime == null)
                    {
                        context.AddFailure(ProgramManagementResource.CourseTimetableStartDateRequired);
                        return;
                    }
                    if (command.TimetableItem.EndTime == null)
                    {
                        context.AddFailure(ProgramManagementResource.CourseTimetableEndDateRequired);
                        return;
                    }

                    var startDate = $"{ command.TimetableItem.Date } {command.TimetableItem.StartTime.ToString()}".ToLocalDate();
                    var endDate = $"{ command.TimetableItem.Date } {command.TimetableItem.EndTime.ToString()}".ToLocalDate();

                    if (endDate < startDate)
                        context.AddFailure("End time cannot be less than the start time");

                    var selectedDaysSchedules = unitOfWork.ProgramManagementRepository
                    .FindBy<Timetable>(x => x.UnitId == command.TimetableItem.UnitId && x.SemesterScheduleItemId == command.ScheduleItemId && x.StartDate.Date == startDate.Date).ToList();

                    var unitScheduleExists = selectedDaysSchedules.Any(x => x.StartDate == startDate && x.EndDate == endDate);
                    if (unitScheduleExists)
                        context.AddFailure($" The time slot {command.TimetableItem.StartTime} to {command.TimetableItem.EndTime} for the unit {command.TimetableItem.UnitName} already exists");


                    var maxEndDate = selectedDaysSchedules.Any() ? selectedDaysSchedules.Max(x => x.EndDate):default(DateTime);

                    if(startDate < maxEndDate)
                       context.AddFailure($" The time slot {command.TimetableItem.StartTime} to {command.TimetableItem.EndTime} for unit {command.TimetableItem.UnitName} conflicts with another time slot");

                    var selectedFacultySchedules = unitOfWork.ProgramManagementRepository
                    .FindBy<Timetable>(x => x.FacultyId == command.TimetableItem.FacultyId && x.StartDate.Date == startDate.Date).ToList();


                    var maxFacultyEndDate = selectedFacultySchedules.Any() ? selectedFacultySchedules.Max(x => x.EndDate) : default(DateTime);
                    if (startDate < maxFacultyEndDate)
                        context.AddFailure($" The time slot {command.TimetableItem.StartTime} to {command.TimetableItem.EndTime} for faculty {command.TimetableItem.FacultyName} conflicts with another time slot");


                });

            }
        }

    }


