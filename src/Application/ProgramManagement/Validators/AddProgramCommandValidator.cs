﻿using Application.ProgramManagement.Commands;
using FluentValidation;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ProgramManagement.Validators
{
    public class AddProgramCommandValidator : AbstractValidator<AddProgramCommand>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public AddProgramCommandValidator(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;

            RuleFor(x => x.Name).NotEmpty().WithMessage(String.Format(ProgramManagementResource.NameIsRequired,"program"));
            RuleFor(x => x.Name).Must((string p) =>
            {
                if (String.IsNullOrEmpty(p))
                    return true;
                return !unitOfWork.ProgramManagementRepository.FindBy<Program>(x => x.Name.Equals(p, StringComparison.OrdinalIgnoreCase)).Any();

            }).WithMessage(ProgramManagementResource.NameExists);

            RuleFor(x => x.ProgramTierId).NotEqual(0).WithMessage(ProgramManagementResource.ProgramTierRequired);
            RuleFor(x => x.Description).NotEmpty().WithMessage(ProgramManagementResource.DescriptionRequired);

        }
    }
}
