﻿using Application.ProgramManagement.Commands;
using FluentValidation;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ProgramManagement.Validators
{
    public class ApproveDateResheduleValidator : AbstractValidator<ApproveSemesterChangeRequest>
    {
        public ApproveDateResheduleValidator()
        {
            RuleFor(x => x.ApprovalStatus).Custom((status, context) =>
            {
                if (status == 0)
                    context.AddFailure("The slected status is invalid. Kindly make a valid selection");
            });
            RuleFor(x => x.ApprovalReason).NotEmpty().WithMessage("Approval reason can not be null");
        }
    }
}
