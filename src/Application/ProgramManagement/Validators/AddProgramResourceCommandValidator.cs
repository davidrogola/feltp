﻿using Application.ProgramManagement.Commands;
using FluentValidation;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ProgramManagement.Validators
{
    public class AddProgramResourceCommandValidator : AbstractValidator<AddProgramResourceCommand>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public AddProgramResourceCommandValidator(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
            RuleFor(x => x.ResourceTypeId).NotEqual(0).WithMessage(ProgramManagementResource.ResourceTypeRequired);
            RuleFor(x => x.ProgramId).NotEqual(0).WithMessage(ProgramManagementResource.ProgramRequired);
            RuleFor(x => x.ResourceId).NotEqual(0).WithMessage(ProgramManagementResource.ResourceRequired);

            RuleFor(x => x).Must((AddProgramResourceCommand command) =>
            {
                if (HasInvalidCommandValues(command))
                    return true;

                bool duplicateExists = unitOfWork.ProgramManagementRepository.FindBy<ProgramResource>(x => x.ProgramId == Convert.ToInt16(command.ProgramId) && x.ResourceId == Convert.ToInt16(command.ResourceId)).Any();

                return !duplicateExists;
            }).WithMessage(ProgramManagementResource.DuplicateProgramResource);
        }

        private bool HasInvalidCommandValues(AddProgramResourceCommand command)
        {
            if (command == null)
                return true;

            if (command.ProgramId == 0 || command.ResourceId == 0)
                return true;

            return false;
        }
    }
}
