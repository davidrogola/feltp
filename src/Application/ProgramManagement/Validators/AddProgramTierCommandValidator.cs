﻿using Application.ProgramManagement.Commands;
using FluentValidation;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ProgramManagement.Validators
{
    public class AddProgramTierCommandValidator : AbstractValidator<AddProgramTierCommand>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public AddProgramTierCommandValidator(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
            RuleFor(x => x.Name).NotNull().WithMessage(String.Format(ProgramManagementResource.NameIsRequired,"Program tier"));
            RuleFor(x => x.Name).Must((string p) =>
            {
                if (String.IsNullOrEmpty(p))
                    return true;
                return !unitOfWork.ProgramManagementRepository.FindBy<ProgramTier>(x => x.Name.Equals(p, StringComparison.OrdinalIgnoreCase)).Any();
            }).WithMessage(ProgramManagementResource.NameExists);
        }
    }
}
