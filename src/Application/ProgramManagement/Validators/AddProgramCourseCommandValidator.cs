﻿using Application.ProgramManagement.Commands;
using FluentValidation;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ProgramManagement.Validators
{
    public class AddProgramCourseCommandValidator : AbstractValidator<AddProgramCourseCommand>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public AddProgramCourseCommandValidator(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;

            RuleFor(x => x.ProgramId).NotEqual(0).WithMessage(ProgramManagementResource.ProgramRequired);
            RuleFor(x => x.CourseId).NotEqual(0).WithMessage(ProgramManagementResource.CourseRequired);
            RuleFor(x => x.SemesterId).NotEqual(0).WithMessage(ProgramManagementResource.SemesterRequired);
            RuleFor(x => x.DurationInWeeks).GreaterThan(0).WithMessage(ProgramManagementResource.DurationRequired);

            RuleFor(x => x).Must((AddProgramCourseCommand command) =>
             {
                 if (HasInvalidCommandValues(command))
                     return true;
                   
                 bool duplicateExists = unitOfWork.ProgramManagementRepository.FindBy<ProgramCourse>(x => x.ProgramId == Convert.ToInt16(command.ProgramId) && x.CourseId == Convert.ToInt16(command.CourseId)).Any();

                 return !duplicateExists;
             }).WithMessage(ProgramManagementResource.DuplicateProgramCourse);

            RuleFor(x => x.CourseId).Must((int courseId) =>
              {
                  if (courseId.Equals(0))
                      return true;
                  bool unitsConfigured = unitOfWork.ProgramManagementRepository.FindBy<CourseUnit>
                  (x => x.CourseId == courseId).Any();
                  bool configured = unitsConfigured == true ? true: false;
                  return configured;
              }).WithMessage(ProgramManagementResource.UnitsRequired);


        }

        private bool HasInvalidCommandValues(AddProgramCourseCommand command)
        {
            if (command == null)
                return true;

            if (command.ProgramId==0 || command.CourseId==0
                || command.SemesterId==0)
                return true;

            return false;
        }
    }
}
