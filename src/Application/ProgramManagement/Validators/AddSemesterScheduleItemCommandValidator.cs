﻿using Application.Common.Services;
using Application.ProgramManagement.Commands;
using FluentValidation;
using Microsoft.Extensions.Configuration;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Application.ProgramManagement.Validators
{
    public class AddSemesterScheduleItemCommandValidator : AbstractValidator<AddSemesterScheduleItemCommand>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IConfiguration config;
        public AddSemesterScheduleItemCommandValidator(IProgramManagementUnitOfWork unitOfWork,
        IConfiguration config)
        {
            this.unitOfWork = unitOfWork;
            this.config = config;

            RuleFor(x => x.SemesterScheduleId).NotEqual(0).WithMessage(ProgramManagementResource.SemesterScheduleRequired);
            RuleFor(x => x.StrStartDate).NotNull().WithMessage(ProgramManagementResource.StartDateRequired);
            RuleFor(x => x.StrEndDate).NotNull().WithMessage(ProgramManagementResource.EndDateRequired);
            RuleFor(x => x.ProgramCourseId).NotEqual(0).WithMessage(ProgramManagementResource.CourseRequired);
           
            RuleFor(x => x).Custom((command, context) =>
            {
                if (command == null)
                {
                    context.AddFailure(ProgramManagementResource.FillRequiredFields);
                    return;
                }

                if (command.ScheduleItemType == ScheduleItemType.Cat)
                {
                    return;
                }
                
                var startDate = command.StrStartDate.ToLocalDate();

                var endDate = command.StrEndDate.ToLocalDate();
                if (command.Duration != 0)
                  endDate = startDate.CalculateEndDate(command.Duration);

                var semester = unitOfWork.ProgramManagementRepository.FindByWithInclude<SemesterSchedule>(x => x.Id == command.SemesterScheduleId, x => x.Semester).SingleOrDefault();
                if (startDate < semester.EstimatedStartDate.Date)
                    context.AddFailure($"{command.ScheduleItemType.ToString()} schedule start date cannot be less than the semester start date");
                if (endDate > semester.EstimatedEndDate.Date)
                    context.AddFailure($"{command.ScheduleItemType.ToString()} schedule end date cannot be greater than the semester end date");

                var duplicateExists = unitOfWork.ProgramManagementRepository.FindBy<SemesterScheduleItem>
                (x => x.ProgramCourseId == command.ProgramCourseId && x.ScheduleItemType == command.ScheduleItemType && x.SemesterScheduleId == command.SemesterScheduleId).Any();

                if (duplicateExists)
                    context.AddFailure($"{command.ScheduleItemType.ToString() } schedule for {semester.Semester.Name} already exists");
            });

        }
    }
}
