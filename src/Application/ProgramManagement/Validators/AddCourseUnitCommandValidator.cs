﻿using Application.ProgramManagement.Commands;
using FluentValidation;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ProgramManagement.Validators
{
    public class AddCourseUnitCommandValidator : AbstractValidator<AddCourseUnitCommand>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public AddCourseUnitCommandValidator(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;

            RuleFor(x => x.CourseId).NotEqual(0).WithMessage(ProgramManagementResource.CourseRequired);
            RuleFor(x => x.UnitId).Must((List<int> modules) =>
            {
                if (modules == null)
                    return false;
                return modules.Any();

            }).WithMessage(ProgramManagementResource.SelectUnit);

            RuleFor(x => x).Must((AddCourseUnitCommand command) =>
              {
                  if (command.CourseId == 0 || command.UnitId == null)
                      return true;
                  var courseModuleExists = unitOfWork.ProgramManagementRepository.FindBy<CourseUnit>(x => x.CourseId == command.CourseId && command.UnitId.Contains(x.UnitId)).Any();                    
                  return !courseModuleExists;
              }).WithMessage(ProgramManagementResource.CourseUnitExists);
        }
    }
}
