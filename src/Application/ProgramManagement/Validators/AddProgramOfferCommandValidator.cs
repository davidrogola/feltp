﻿using Application.ProgramManagement.Commands;
using FluentValidation;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ProgramManagement.Validators
{
    public class AddProgramOfferCommandValidator : AbstractValidator<AddProgramOfferCommand>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public AddProgramOfferCommandValidator(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;

            RuleFor(x => x.Name).NotNull().WithMessage(string.Format(ProgramManagementResource.NameIsRequired, "Offer name"));
            RuleFor(x => x.FacultyId).NotEqual(0).WithMessage(ProgramManagementResource.ContactPersonRequired);
            RuleFor(x => x.ProgramId).NotEqual(0).WithMessage(ProgramManagementResource.ProgramRequired);
            RuleFor(x => x.StartDate).NotNull().WithMessage(ProgramManagementResource.OfferStartDateRequired);

            RuleFor(x => x.ProgramLocationId).Must((int[] location) =>
            {
                if (location != null)
                    return true;
                else return false;
            }).WithMessage(ProgramManagementResource.OfferLocationRequired);

            RuleFor(x => x.ProgramId).Must((int programId) =>
              {
                  if (programId == 0)
                  {
                      return true;
                  }

                  var graduationRequirementExists = unitOfWork.ProgramManagementRepository
                  .FindBy<GraduationRequirement>(x => x.ProgramId == programId).Any();
                  return graduationRequirementExists;
              }).WithMessage(ProgramManagementResource.GraudationRequirementRequired);

            RuleFor(x => x.ProgramId).Must((int programId) =>
            {
                if (programId == 0)
                {
                    return true;
                }

                var coursesExist = unitOfWork.ProgramManagementRepository
                .FindBy<ProgramCourse>(x => x.ProgramId == programId).Any();
                return coursesExist;
            }).WithMessage(ProgramManagementResource.ProgramCourseRequired);


            RuleFor(x => x).Custom((model, context) =>
            {
                if (model.AcademicRequirementDetails == null)
                {
                    context.AddFailure("Academic requirements section is required");
                    return;
                }
                if (model.AcademicRequirementDetails.AcademicCourseToEducationLevelMapping == null)
                    context.AddFailure("Please select atleast one academic course");


                if (model.GeneralProgramQualificationRequirements == null)
                {
                    context.AddFailure("General program offer requirements section is required");
                    return;
                }

                if (model.GeneralProgramQualificationRequirements.MinimumWorkExperienceDuration == 0)
                    context.AddFailure("Minimum work experience duration is required");

            });
    }


    }
    public class AddProgramLocationCommandValidator : AbstractValidator<AddProgramLocationCommand>
    {
        public AddProgramLocationCommandValidator()
        {
            RuleFor(x => x.CountryId).NotEqual(0).WithMessage(ProgramManagementResource.BuildingRequired);
            RuleFor(x => x.Location).NotNull().WithMessage(ProgramManagementResource.LocationRequired);
        }
    }
}

