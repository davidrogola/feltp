﻿using Application.ResidentManagement.Commands;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProgramManagement.Domain.Examination;

namespace Application.ProgramManagement.Validators
{
    public class InputResidentExaminationScoreCommandValidator : AbstractValidator<InputResidentExaminationScoreCommand>
    {
        public InputResidentExaminationScoreCommandValidator()
        {
            RuleFor(x => x).Custom((command, context) =>
            {
                if (command == null)
                {
                    context.AddFailure("Please select at least one examination before submitting scores");
                    return;
                }

                if (command.ExamDetailResponse.ResidentExams.All(x => !x.Score.HasValue))
                {
                    context.AddFailure("Please input scores for at least one examination before submitting");
                    return;
                }

                var examTypes = new[] {ExamType.CatOne, ExamType.CatTwo, ExamType.CatThree};

                foreach (var exam in command.ExamDetailResponse.ResidentExams)
                {
                    if (exam.Score.HasValue)
                    {                        
                        if (examTypes.Contains(exam.ExamType) && (exam.Score.Value < 0 || exam.Score.Value > 30))
                            context.AddFailure($"Scores for cat {exam.Name} should be between 0-30");
                        else if (exam.Score.Value < 0 || exam.Score.Value > 100)
                            context.AddFailure($"Scores for exam {exam.Name} should be between 0-100");
                        
                    }                 
                }
            });
        }
    }
}
