﻿using Application.ProgramManagement.Commands;
using FluentValidation;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.FieldPlacement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ProgramManagement.Validators
{
    public class AddFieldPlacementActivityCommandValidator : AbstractValidator<AddFieldPlacementActivityCommand>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public AddFieldPlacementActivityCommandValidator(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
            RuleFor(x => x.Name).NotNull().WithMessage(String.Format(ProgramManagementResource.NameIsRequired, "field placement activity"));
            RuleFor(x => x.Description).NotNull().WithMessage(ProgramManagementResource.DescriptionRequired);

            RuleFor(x => x.Name).Must((string name) =>
            {
                if (String.IsNullOrEmpty(name))
                    return true;
                return !unitOfWork.ProgramManagementRepository.FindBy<FieldPlacementActivity>(x => x.Name.Contains(name)).Any();
            }).WithMessage(String.Format(ProgramManagementResource.DuplicateValue,"field placement activity"));

        }
    }

    public class AddFielPlacementActivityDeliverableCommandValidator : AbstractValidator<AddFieldPlacementActivityDeliverableCommand>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public AddFielPlacementActivityDeliverableCommandValidator(IProgramManagementUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;

            RuleFor(x => x.FieldPlacementActivityId).NotEqual(0).WithMessage(ProgramManagementResource.ActivityRequired);
            RuleFor(x => x.DeliverableId).Must((List<int> deliverables) =>
              {
                  if (deliverables.Any())
                      return true;
                  return false;

              }).WithMessage(ProgramManagementResource.DeliverableRequired);

            RuleFor(x => x).Must((AddFieldPlacementActivityDeliverableCommand command) =>
            {
                if (command == null)
                    return true;

                var duplicateExists = unitOfWork.ProgramManagementRepository.FindBy<FieldPlacementActivityDeliverable>(x => x.FieldPlacementActivityId == command.FieldPlacementActivityId && command.DeliverableId.Contains(x.DeliverableId))
                   .Any();

                return !duplicateExists;

            }).WithMessage("Duplicate Field activity to deliverable mapping exists");
        }

    }

}
