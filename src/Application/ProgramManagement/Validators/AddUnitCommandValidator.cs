﻿using Application.ProgramManagement.Commands;
using FluentValidation;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ProgramManagement.Validators
{
    public class AddUnitCommandValidator : AbstractValidator<AddUnitCommand>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public AddUnitCommandValidator(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;

            RuleFor(x => x.Name).NotNull().WithMessage(String.Format(ProgramManagementResource.NameIsRequired,"module"));
            RuleFor(x => x.Description).NotNull().WithMessage(ProgramManagementResource.DescriptionRequired);

            RuleFor(x => x.Name).Must((string name) => 
            {
                if (String.IsNullOrEmpty(name))
                    return true;

                return !unitOfWork.ProgramManagementRepository.FindBy<Unit>(x => x.Name.Contains(name)).Any();
            }).WithMessage(String.Format(ProgramManagementResource.DuplicateValue,"unit"));

            RuleFor(x => x).Must((AddUnitCommand command) =>
            {
                if (command.HasFieldPlacementActivity && !command.FieldPlacementActivities.Any())
                    return false;

                return true;
            }).WithMessage(ProgramManagementResource.ActivityRequired);


        }
    }
}
