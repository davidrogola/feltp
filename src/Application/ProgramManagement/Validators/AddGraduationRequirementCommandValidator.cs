﻿using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using FluentValidation;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ProgramManagement.Validators
{
    public class AddGraduationRequirementCommandValidator : AbstractValidator<AddGraduationRequirementCommand>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMediator mediator;
        public AddGraduationRequirementCommandValidator(IProgramManagementUnitOfWork _unitOfWork, IMediator _mediator)
        {
            unitOfWork = _unitOfWork;
            mediator = _mediator;

            RuleFor(x => x.ProgramId).NotEqual(0).WithMessage(ProgramManagementResource.ProgramRequired);

            RuleFor(x => x.ProgramId).Must((int programId) =>
              {
                  if (programId == 0)
                      return true;
                  var exists = unitOfWork.ProgramManagementRepository.FindBy<GraduationRequirement>(x => x.ProgramId == programId).Any();
                  return !exists;
              }).WithMessage(ProgramManagementResource.GraduationReqDuplicate);

            RuleFor(x => x.NumberOfActivitiesRequired).NotEqual(0).WithMessage(ProgramManagementResource.NumberOfActivitiesRequired);

            RuleFor(x => x.NumberOfDidacticUnitsRequired).NotEqual(0).WithMessage(ProgramManagementResource.DidacticUnitsRequired);

            RuleFor(x => x.PassingGrade).NotEqual(0).WithMessage(ProgramManagementResource.PassingGradeRequired);

            RuleFor(x => x).Must((AddGraduationRequirementCommand model) =>
              {
                  if (model == null)
                      return true;
                  var modulesCount = GetUnitsCount(model.ProgramId);
                  if (modulesCount == null)
                      return true;
                  if (model.NumberOfDidacticUnitsRequired <= modulesCount.NumberOfDidacticUnits || model.NumberOfActivitiesRequired <= modulesCount.NumberOfFieldActivities)
                      return true;
                  else
                      return false;
            }).WithMessage(ProgramManagementResource.TotalUnitsNoMistmatch);
        }

       

        public class UpdateGraduationRequirementCommandValidator : AbstractValidator<UpdateGraduationRequirementCommand>
        {
            public UpdateGraduationRequirementCommandValidator()
            {
                RuleFor(x => x.NumberOfActivitiesRequired).NotEqual(0).WithMessage(ProgramManagementResource.NumberOfActivitiesRequired);

                RuleFor(x => x.NumberOfDidacticUnitsRequired).NotEqual(0).WithMessage(ProgramManagementResource.DidacticUnitsRequired);

                RuleFor(x => x.PassingGrade).NotEqual(0).WithMessage(ProgramManagementResource.PassingGradeRequired);

                // RuleFor(x => x.NumberOfThesisDefenceDeliverables).NotEqual(0).WithMessage("Thesis Defence deliverables required");


            }

        }

        private UnitsCount GetUnitsCount(int programId)
        {
            var courseIds = GetProgramCourseIds(programId);

            var modulesCount = mediator.Send(new GetCourseUnitsCount { CourseIds = courseIds }).Result;

            return modulesCount;
        }

        private int[] GetProgramCourseIds(int programId)
        {
            var programCourses = mediator.Send(new GetProgramCoursesList { ProgramId = programId }).Result;
            var courseIds = programCourses.Select(x => x.CourseId).Distinct().ToArray();

            return courseIds;
        }
    }
}
