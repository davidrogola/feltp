﻿using Application.ProgramManagement.Commands;
using FluentValidation;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ProgramManagement.Validators
{
    public class AddCourseCommandValidator : AbstractValidator<AddCourseCommand>
    {
         IProgramManagementUnitOfWork unitOfWork;
        public AddCourseCommandValidator(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;

            RuleFor(x => x.Name).NotEmpty().WithMessage(String.Format(ProgramManagementResource.NameIsRequired,"course"));
            RuleFor(x => x.CategoryId).NotEqual(0).WithMessage(ProgramManagementResource.CategoryRequired);

            RuleFor(x => x).Must((AddCourseCommand model) =>
            {
            if (model == null)
                    return true;
                var category = unitOfWork.ProgramManagementRepository.Get<Category>(model.CategoryId);
                if (category == null)
                    return true;
                var course = unitOfWork.ProgramManagementRepository.FindByWithInclude<Course>(x => x.Name.Equals(model.Name, StringComparison.OrdinalIgnoreCase) && x.Category.CourseType.Id == category.CourseTypeId,x=>x.Category)
                .SingleOrDefault();
                if (course == null)
                    return true;

                bool exists = course.Category.CourseTypeId == category.CourseTypeId ? false : true;

                return exists;

            }).WithMessage("Semester title entered already exists");
       
        }
    }
}
