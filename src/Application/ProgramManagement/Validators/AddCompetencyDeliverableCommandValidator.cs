﻿using Application.ProgramManagement.Commands;
using FluentValidation;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ProgramManagement.Validators
{
    public class AddCompetencyDeliverableCommandValidator : AbstractValidator<AddCompetencyDeliverableCommand>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public AddCompetencyDeliverableCommandValidator(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;

            RuleFor(x => x.DeliverableId).Must((List<int> deliverables)=> 
            {
                if (deliverables.Any())
                    return true;
                return false;
            }).WithMessage(ProgramManagementResource.DeliverableRequired);
            RuleFor(x => x.CompetencyId).NotEqual(0).WithMessage(ProgramManagementResource.CompetencyRequired);

            RuleFor(x => x).Must((AddCompetencyDeliverableCommand command) =>
              {
                  if (command.CompetencyId==0 || command.DeliverableId==null)
                      return true;
                  bool exists = unitOfWork.ProgramManagementRepository.FindBy<CompetencyDeliverable>(x => x.CompetencyId == command.CompetencyId && command.DeliverableId.Contains(x.DeliverableId)).Any();
                  return !exists;
              }).WithMessage(ProgramManagementResource.CompetencyDeliverableExists);
        }
    }
}
