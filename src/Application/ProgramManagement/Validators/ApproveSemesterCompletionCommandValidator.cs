﻿using Application.ProgramManagement.Commands;
using FluentValidation;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Validators
{
    public class ApproveSemesterCompletionCommandValidator : AbstractValidator<ApproveSemesterCompletionCommand>
    {
        public ApproveSemesterCompletionCommandValidator()
        {
            RuleFor(x => x.Comment).NotEmpty().WithMessage("Please enter comment");
            RuleFor(x => x.Status).NotEqual(ApprovalStatus.Pending).WithMessage("Please select status");

        }
    }
}
