﻿using Application.ProgramManagement.Commands;
using FluentValidation;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ProgramManagement.Validators
{
 public   class AddResourceCommandValidator : AbstractValidator<AddResourceCommand>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public AddResourceCommandValidator(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;

            RuleFor(x => x.ResourceTitle).NotEmpty().WithMessage(String.Format(ProgramManagementResource.NameIsRequired, "resource"));
            RuleFor(x => x.ResourceTitle).Must((string p) =>
            {
                if (String.IsNullOrEmpty(p))
                    return true;
                return !unitOfWork.ProgramManagementRepository.FindBy<Resource>(x => x.ResourceTitle.Equals(p, StringComparison.OrdinalIgnoreCase)).Any();

            }).WithMessage(ProgramManagementResource.NameExists);

            RuleFor(x => x.ResourceDescription).NotEmpty().WithMessage(ProgramManagementResource.CodeIsRequired);
            RuleFor(x => x.ResourceDescription).Must((string title) =>
            {
                if (String.IsNullOrEmpty(title))
                    return true;
                return !unitOfWork.ProgramManagementRepository.FindBy<Resource>(x => x.ResourceDescription.Equals(title, StringComparison.OrdinalIgnoreCase)).Any();
            }).WithMessage(ProgramManagementResource.CodeExists);

            RuleFor(x => x.ResourceTypeId).NotEqual(0).WithMessage(ProgramManagementResource.ResourceTypeRequired);
            RuleFor(x => x.Source).NotEmpty().WithMessage(ProgramManagementResource.SourceRequired);
            RuleFor(x => x.SourceContactDetails).NotEmpty().WithMessage(ProgramManagementResource.SourceContactDetailsRequired);

        }
    }
}