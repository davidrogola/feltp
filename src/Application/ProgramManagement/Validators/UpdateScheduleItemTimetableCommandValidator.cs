﻿using Application.Common.Services;
using Application.FacultyManagement;
using Application.ProgramManagement.Commands;
using FluentValidation;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ProgramManagement.Validators
{
    public class UpdateScheduleItemTimetableCommandValidator : AbstractValidator<UpdateScheduleItemTimetableCommand>
    {
        IProgramManagementUnitOfWork unitOfWork;

        public UpdateScheduleItemTimetableCommandValidator(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;

            RuleFor(x => x.StartDate).NotNull().WithMessage(ProgramManagementResource.CourseTimetableStartDateRequired);
            RuleFor(x => x.EndDate).NotNull().WithMessage(ProgramManagementResource.CourseTimetableEndDateRequired);
            RuleFor(x => x.FacultyId).NotEqual(0).WithMessage(FacultyManagementResource.SelectFaculty);
            RuleFor(x => x.StartTime).NotNull().WithMessage(ProgramManagementResource.StartTimeIsRequired);
            RuleFor(x => x.EndTime).NotNull().WithMessage(ProgramManagementResource.EndTimeIsRequired);

            RuleFor(x => x).Custom((command, context) =>
            {
                if (command == null)
                {
                    context.AddFailure(ProgramManagementResource.FillRequiredFields);
                    return;
                }

                if (command.TimetableType == TimetableType.Unit.ToString() && !command.UnitId.HasValue)
                    context.AddFailure(ProgramManagementResource.SelectUnit);


                if (command.TimetableType == TimetableType.Examination.ToString() && !command.ExaminationId.HasValue)
                    context.AddFailure(ProgramManagementResource.SelectUnit);

                if (command.StartTime == null)
                {
                    context.AddFailure(ProgramManagementResource.CourseTimetableStartDateRequired);
                    return;
                }
                if (command.EndTime == null)
                {
                    context.AddFailure(ProgramManagementResource.CourseTimetableEndDateRequired);
                    return;
                }

                var startDate = $"{ command.Date } {command.StartTime.ToString()}".ToLocalDate();
                var endDate = $"{ command.Date } {command.EndTime.ToString()}".ToLocalDate();

                if (endDate < startDate)
                    context.AddFailure("End time cannot be less than the start time");

                var selectedDaysSchedules = unitOfWork.ProgramManagementRepository
                .FindBy<Timetable>(x => x.UnitId == command.UnitId && x.SemesterScheduleItemId == command.ScheduleItemId && x.StartDate.Date == startDate.Date).ToList();

                var unitScheduleExists = selectedDaysSchedules.Where(x => x.StartDate == startDate && x.EndDate == endDate).Any();
                if (unitScheduleExists)
                    context.AddFailure($" The time slot {command.StartTime} to {command.EndTime} for the unit already exists");


                var maxEndDate = selectedDaysSchedules.Any() ? selectedDaysSchedules.Max(x => x.EndDate) : default(DateTime);

                if (startDate < maxEndDate)
                    context.AddFailure($" The time slot {command.StartTime} to {command.EndTime} for unit  conflicts with another time slot");

                var selectedFacultySchedules = unitOfWork.ProgramManagementRepository
                .FindBy<Timetable>(x => x.FacultyId == command.FacultyId && x.StartDate.Date == startDate.Date).ToList();


                var maxFacultyEndDate = selectedFacultySchedules.Any() ? selectedFacultySchedules.Max(x => x.EndDate) : default(DateTime);
                if (startDate < maxFacultyEndDate)
                    context.AddFailure($" The time slot {command.StartTime} to {command.EndTime} for faculty {command.FacultyName} conflicts with another time slot");


            });
        }
    }
}
