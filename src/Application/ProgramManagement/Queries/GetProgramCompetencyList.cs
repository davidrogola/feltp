﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetProgramCompetencyList : IRequest<List<ProgramCompetencyViewModel>>
    {
        public int Id { get; set; }
    }

    public class ProgramCompetencyViewModel
    {
        public int Id { get; set; }
        public int ProgramId { get; set; }
        public int CompetencyId { get; set; }
        public string Program { get; set; }
        public string Competency { get; set; }
        public List<string> Deliverables { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Status { get; set; }
    }
}
