﻿using MediatR;
using ProgramManagement.Domain.Event;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetEventScheduleList : IRequest<List<EventScheduleViewModel>>
    {
        public int? ProgramEventId { get; set; }
    }

    public class EventScheduleViewModel
    {
        public int Id { get; set; }
        public int ProgramId { get; set; }
        public string ProgramName { get; set; }
        public int ProgramEventId { get; set; }
        public string ProgramEvent { get; set; }
        public string Program { get; set; }
        public PresentationType PresentationType { get; set; }
        public string Location { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public bool IsElapsed { get; set; }
        public string Status { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
    }
}
