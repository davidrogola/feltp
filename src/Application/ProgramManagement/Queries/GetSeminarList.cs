﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetSeminarList : IRequest<List<SeminarViewModel>>
    {
        public int? Id { get; set; }
    }

    public class SeminarViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Duration { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Status { get; set; }
    }

}
