﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetProgramOfferList : IRequest<List<ProgramOfferViewModel>>
    {
        public int ? ProgramId { get; set; }
        public int Take { get; set; }
        public int Skip { get; set; }
    }

    public class GetProgramOfferById : IRequest<ProgramOfferViewModel>
    {
        public int? OfferId { get; set; }
    }

    public class ProgramOfferViewModel
    {
        public int Id { get; set; }
        public int ProgramId { get; set; }
        public string Name { get; set; }
        public string ProgramName { get; set; }
        public string ProgramDescription { get; set; }
        public string StrStartDate { get; set; }
        public string StrEndDate { get; set; }
        public string Faculty { get; set; }
        public string Duration { get; set; }
        public int ProgramOfferId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ApplicationStartDate { get; set; }
        public DateTime ApplicationEndDate { get; set; }
        public string StrApplicationStartDate { get; set; }
        public string StrApplicationEndDate { get; set; }
        public int FacultyId { get; set; }
        public int[] ProgramLocationId { get; set; }
        public DateTime DateCreated { get; set; }
        public bool HasEligiblityRequirements { get; set; }

        public OfferApplicationDurationValidResponse ValidateProgramOfferApplicationDuration()
        {
            if (ApplicationStartDate.Date > DateTime.Now.Date && DateTime.Now.Date < ApplicationEndDate.Date)
                return new OfferApplicationDurationValidResponse
                {
                    ApplicationsOpen = false,
                    Message = $"Applications for {Name } not opened. Applications start on {ApplicationStartDate.Date}",
                    ApplicationsClosed = false,
                    OfferId = Id,
                    Name = Name
                };

            if (ApplicationEndDate.Date < DateTime.Now.Date )
                return new OfferApplicationDurationValidResponse
                {
                    OfferId = Id,
                    Message = $"Applications for {Name } was closed on {ApplicationEndDate.ToShortDateString()}",
                    ApplicationsClosed = true,
                    Name = Name
                };

            return new OfferApplicationDurationValidResponse
            {
                OfferId = Id,
                ApplicationsClosed = false,
                ApplicationsOpen = true,
                Message = $"Applications for {Name} are ongoing",
                Name = Name

            };

            
        }

    }

    public class OfferApplicationDurationValidResponse
    {
        public int OfferId { get; set; }
        public bool ApplicationsOpen { get; set; }
        public bool ApplicationsClosed { get; set; }
        public string Message { get; set; }
        public string Name { get; set; }
    }
}
