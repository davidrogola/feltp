﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetGraduationRequirementList : IRequest<List<GraduationRequirementViewModel>>
    {
        public int ? ProgramId { get; set; }
        public int ? Id { get; set; }
    }

    public class GraduationRequirementViewModel
    {
        public int Id { get; set; }
        public int ProgramId { get; set; }
        public string Program { get; set; }
        public int NumberOfDidacticModulesRequired { get; set; }
        public int NumberOfActivitiesRequired { get; set; }
        public int NumberOfThesisDefenceDeliverables { get; set; }
        public int PassingGrade { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
    }
}
