﻿using Application.ProgramManagement.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetUnitFieldPlacementActivityWithDeliverables : IRequest<UnitPlacementActivityViewModel>
    {
        public int UnitId { get; set; }
    }

    public class UnitPlacementActivityViewModel
    {
        public int UnitId { get; set; }
        public string UnitName { get; set; }
        public List<FieldPlacementActivityViewModel> Activities { get; set; }
    }


    public class GetUnitFieldPlacementActivities : IRequest<List<FieldPlacementActivityViewModel>>
    {
        public int UnitId { get; set; }

    }


}
