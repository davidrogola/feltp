﻿using MediatR;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetTimetableListQuery : IRequest<List<TimetableViewModel>>
    {
        public int SemesterScheduleItemId { get; set; }
        public List<TimetableType> TimetableTypes { get; set; }

    }

    public class GetTimetableItemQuery : IRequest<TimetableViewModel>
    {
        public int Id { get; set; }
        
    }

    public class GetTimetableByProgramOfferId : IRequest<List<TimetableViewModel>>
    {
        public int Id { get; set; }
        public TimetableType Type { get; set; }
        public int ? SemesterId { get; set; }
    }


    public class TimetableViewModel
    {
        public int Id { get; set; }
        public int SemesterScheduleItemId { get; set; }
        public int SemesterScheduleId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string TimeTableType { get; set; }
        public string Faculty { get; set; }
        public string UnitName { get; set; }
        public string ExaminationName { get; set; }
        public int? ExaminationId { get; set; }
        public int? UnitId { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Duration { get; set; }
        public string ScheduleItemName { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Code { get; set; }
        public int FacultyId { get; set; }
        public string ScheduleStartDate { get; set; }
        public string ScheduleEndDate { get; set; }
        public string Semester { get; set; }
        public int SemesterId { get; set; }
        
    }
}
