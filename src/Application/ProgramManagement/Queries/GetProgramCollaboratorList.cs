﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public enum GetCollaboratorBy
    {
        Id,
        ProgramId,
        CollaboratorId
    }
    public class GetProgramCollaboratorList : IRequest<List<ProgramCollaboratorViewModel>>
    {
        public int? Id { get; set; }
        public GetCollaboratorBy GetBy { get; set; }
    }

    public class ProgramCollaboratorViewModel
    {
        public int Id { get; set; }
        public int CollaboratorId { get; set; }
        public int ProgramId { get; set; }
        public string ProgramName { get; set; }
        public string CollaboratorName { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Status { get; set; }
    }
}
