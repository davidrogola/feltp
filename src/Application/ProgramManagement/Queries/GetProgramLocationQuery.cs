﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetProgramOfferLocationQuery : IRequest<List<ProgramLocationViewModel>>
    {
        public int? Id { get; set; }
        public int? ProgramOfferId { get; set; }
    }

    public class GetProgramLocationQuery : IRequest<ProgramLocationViewModel>
    {
        public int Id { get; set; }
    }

    public class ProgramLocationViewModel
    {
        public int Id { get; set; }
        public string Country { get; set; }
        public int CountryId { get; set; }
        public string County { get; set; }
        public int? CountyId { get; set; }
        public string SubCounty { get; set; }
        public int? SubCountyId { get; set; }
        public string PostalAddress { get; set; }
        public string Building { get; set; }
        public string Location { get; set; }
        public string Road { get; set; }
    }
}
