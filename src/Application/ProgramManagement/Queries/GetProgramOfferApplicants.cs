﻿using MediatR;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetProgramOfferApplicationsQuery : IRequest<List<ProgramOfferApplicationViewModel>>
    {
        public int ProgramOfferId { get; set; }
        public ApplicationStatus ApplicationStatus { get; set; }

    }

    public class GetProgramOfferApplicationByApplicantId : IRequest<List<ProgramOfferApplicationViewModel>>
    {
        public long ApplicantId { get; set; }
    }

    public class GetProgramOfferApplicationbyId : IRequest<ProgramOfferApplicationViewModel>
    {
        public long Id { get; set; }
    }

    public class ProgramOfferApplicationViewModel
    {
        public long Id { get; set; }
        public int ProgramOfferId { get; set; }
        public long ApplicantId { get; set; }
        public string QualificationStatus { get; set; }
        public ApplicationStatus ApplicationStatus { get; set; }
        public string StrApplicationStatus { get; set; } 
        public string DateApplied { get; set; }
        public DateTime DateCreated { get; set; }
        public string Name { get { return FirstName + " " + LastName; } set { } }
        public string FirstName  { get; set; }
        public string LastName { get; set; }
        public string RegistrationNumber { get; set; }
        public string IdentificationNumber { get; set; }
        public string Cadre { get; set; }
        public string Program { get; set; }
        public int ProgramId { get; set; }
        public string ProgramOfferName { get; set; }
        public string ApprovedByCounty { get; set; }
        public bool PersonalStatementSubmitted { get; set; }
        public EnrollmentStatus EnrollmentStatus { get; set; }
        public string EvaluatedBy { get; set; }
        public string DateEvaluated { get; set; }
        public string Comment { get; set; }
        public string EmailAddress { get; set; }
        public ProgramApplicationProgressIndicator ProgressIndicator { get; set; }
        public ProgressBuilder  ProgressBuilder { get; set; }
    }

    public class ProgressBuilder
    {
        public List<KeyValuePair<ProgressStage,string>> ValidStatuses { get; set; }
        public int Width { get; set; }
        

    }
}
