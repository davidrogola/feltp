﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public enum GetEventBy
    {
        Id,
        ProgramId,
        EventId
    }
    public class GetProgramEventList : IRequest<List<ProgramEventViewModel>>
    {
        public int? Id { get; set; }
        public GetEventBy GetBy { get; set; }
    }

    public class ProgramEventViewModel
    {
        public int Id { get; set; }
        public int EventId { get; set; }
        public int ProgramId { get; set; }
        public string ProgramName { get; set; }
        public string EventTitle { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Status { get; set; }
    }
}
