﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetCourseScheduleList : IRequest<List<CourseScheduleViewModel>>
    {
        public int? ProgramCourseId { get; set; }
    }

    public class CourseScheduleViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ProgramOfferId { get; set; }
        public string ProgramOffer { get; set; }
        public string ProgramTier { get; set; }
        public int ProgramCourseId { get; set; }
        public string ProgramCourse { get; set; }
        public string Program { get; set; }
        public int FacultyId { get; set; }
        public string Faculty { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }    
        public string Location { get; set; }
        public string Status { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
    }
}
