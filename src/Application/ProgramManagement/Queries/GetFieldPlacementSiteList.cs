﻿using Application.Common.Models;
using Common.Models;
using MediatR;
using ProgramManagement.Domain.FieldPlacement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{

    public class GetFieldPlacementSiteList : IRequest<List<FieldPlacementSiteViewModel>>
    {
        public int? Id { get; set; }
    }

    public class FieldPlacementSiteViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Status { get; set; }
        public string Location { get; set; }
        public string Building { get; set; }
        public Level Level { get; set; }
        public string PostalAddress { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public string Country { get; set; }
        public int CountryId { get; set; }
        public string County { get; set; }
        public int? CountyId { get; set; }
        public string SubCounty { get; set; }
        public int? SubCountyId { get; set; }
        public string Faculty { get; set; }
        public int[] FacultyId { get; set; }


    }

    public class GetFieldSiteSupervisor : IRequest<List<FieldSiteSupervisorViewModel>>
    {
        public int ? SiteId { get; set; }
    }


    public class FieldSiteSupervisorViewModel
    {
        public string SupervisorName { get; set; }
        public int SupervisorId { get; set; }
        public int FieldSiteId { get; set; }
        public string SiteName { get; set; }
    }
}
