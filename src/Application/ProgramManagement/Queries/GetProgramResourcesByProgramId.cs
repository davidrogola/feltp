﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetProgramResourcesByProgramId : IRequest<List<ProgramResourceViewModel>>
    {
        public int? programId { get; set; }
    }
    public class ProgramResourceViewModel
    {
        public int Id { get; set; }
        public int ProgramId { get; set; }
        public string ResourceTitle { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public string ResourceDescription { get; set; }
        public string ResourcesType { get; set; }

    }
}
