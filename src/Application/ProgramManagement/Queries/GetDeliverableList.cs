﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetDeliverableList : IRequest<List<DeliverableViewModel>>
    {
        public int? Id { get; set; }
    }

    public class DeliverableViewModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int DurationInWeeks { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Status { get; set; }
        public bool HasThesisDefence { get; set; }
        public string StrHasThesisDefence { get; set; }

    }

}
