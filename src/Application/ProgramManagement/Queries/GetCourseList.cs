﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetCourseList : IRequest<List<CourseViewModel>>
    {
        public int ? Id { get; set; }
        public int ? CategoryId { get; set; }
    }

    public class CourseViewModel
    {
        public int Id { get;  set; }
        public int CategoryId { get; set; }
        public string Name { get;  set; }
        public string Code { get;  set; }
        public int CourseTypeId { get;  set; }
        public string CourseType  { get;  set; }
        public string DateCreated { get;  set; }
        public string CreatedBy { get; private set; }
        public string Status { get; set; }
        public string Category { get; set; }

    }
}
