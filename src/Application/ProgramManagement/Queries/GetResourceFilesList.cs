﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
   

    public class GetResourceFilesList : IRequest<List<ResourceFilesViewModel>>
    {
        public int? RecourceId { get; set; }
    }

    public class ResourceFilesViewModel
    {
        public int Id { get; set; }
        public int ResourceId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte[] FileContent { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }


    }
}
