﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetUnitList :IRequest<List<UnitViewModel>>
    {       
        public bool HasActivity { get; set; }
        public bool IsDropDownRequest { get; set; }

    }

    public class GetUnit : IRequest<UnitViewModel>
    {
        public int Id { get; set; }
    }


    public class UnitViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Description { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string Status { get; set; }
        public bool HasActivity { get; set; }
        public string DeactivatedBy { get; set; }
        public int [] FieldPlacementActivityIds { get; set; }
    }
}
