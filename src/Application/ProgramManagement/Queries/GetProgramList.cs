﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetProgramList : IRequest<List<ProgramViewModel>>
    {
        public int? Id { get; set; }
        public int ? ProgramTierId { get; set; }
    } 


    public class ProgramViewModel
    {
        public int Id { get;  set; }
        public string Name { get;  set; }
        public string Status { get; set; }
        public string ProgramTier { get;  set; }
        public int ProgramTierId { get; set; }
        public string Code { get;  set; }
        public string Description { get;  set; }
        public string Duration { get;  set; }
        public bool HasMonthlySeminors { get;  set; }
        public string DateCreated { get;  set; }
        public string CreatedBy { get;  set; }
    }

}
