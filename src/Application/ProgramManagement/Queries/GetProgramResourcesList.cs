﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetProgramResourcesList : IRequest<List<ProgramResourcesViewModel>>
    {
        public int ? ResourceId { get; set; }
    }

    public class GetProgramResource : IRequest<ProgramResourcesViewModel>
    {
        public int Id { get; set; }
    }

    public class ProgramResourcesViewModel
    {
        public int Id { get; set; }
        public string ProgramId { get; set; }
        public string ProgramName { get; set; }
        public string ProgramTier { get; set; }
        public int ProgramTierId { get; set; }
        public string ResourceName { get; set; }
        public string ResourceType { get; set; }
        public int ResourceTypeId { get; set; }
        public int ResourceId { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Status { get; set; }
        public string ResourceTitle { get; set; }
        public string ResourceDescription { get; set; }
        public string Source { get; set; }
        public string SourceContactDetails { get; set; }
        public List<int> ProgramIds { get; set; }

    }

}
