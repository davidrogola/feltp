﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public enum CourseTypeEnum
    {
        Didactic,
        
        FieldPlacement
    }

    public class GetProgramCoursesList : IRequest<List<ProgramCoursesViewModel>>
    {
        public int ? ProgramId { get; set; }
        public int ? Id { get; set; }
        
        public int ? SemesterId { get; set; }

    }

    public class ProgramCoursesViewModel
    {
        public int Id { get; set; }
        public int ProgramId { get; set; }
        public string ProgramName { get; set; }
        public string CourseName { get; set; }
        public string CourseType { get; set; }
        public int CourseId { get; set; }
        public int SemesterId { get; set; }
        public int DurationInWeeks { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Status { get; set; }
        public string Semester { get; set; }
        public string ProgramTier { get; set; }
        public string Category { get; set; }

    }

    public class SemesterCourseInfomation
    {
        public string Semester { get; set; }
        public string CourseTitle { get; set; }
        public int DurationInWeeks { get; set; }
        public int TotalDuration { get; set; }
    }

}
