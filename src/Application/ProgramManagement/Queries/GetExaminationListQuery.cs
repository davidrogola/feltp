﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetExaminationListQuery : IRequest<List<ExaminationViewModel>>
    {
        public int Id { get; set; }
    }

    public class ExaminationViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int UnitId { get; set; }
        public int ? CourseId { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }

    }

}
