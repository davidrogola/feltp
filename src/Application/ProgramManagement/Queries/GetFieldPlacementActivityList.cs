﻿using MediatR;
using ProgramManagement.Domain.FieldPlacement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetFieldPlacementActivityList : IRequest<List<FieldPlacementActivityViewModel>>
    {
        public int? Id { get; set; }
    }

    public class FieldPlacementActivityViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Description { get; set; }
        public ActivityType ActivityType { get; set; }
        public string StrActivityType { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string Status { get; set; }
        public string DeactivatedBy { get; set; }
        public int[] DeliverableId { get; set; }
        public List<DeliverableViewModel> Deliverables { get; set; }
    }
}
