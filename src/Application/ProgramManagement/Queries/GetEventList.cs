﻿using MediatR;
using ProgramManagement.Domain.Event;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetEventList : IRequest<List<EventViewModel>>
    {
        public int? Id { get; set; }
    }

    public class EventViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Theme { get; set; }
        public EventType EventTypeId { get; set; }
        public string EventType { get; set; }
        public string Program { get; set; }
        public List<int> Programs { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; private set; }
        public string Status { get; set; }

    }
}
