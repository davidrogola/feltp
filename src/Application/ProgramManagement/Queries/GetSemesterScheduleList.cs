﻿using MediatR;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetSemesterScheduleList : IRequest<List<SemesterScheduleViewModel>>
    {
        public int ? Id { get; set; }
    }

    public class SemesterScheduleViewModel
    {
        public int Id { get; set; }
        public int ProgramOfferId { get; set; }
        public int ProgramId { get; set; }
        public string Description { get; set; }
        public int SemesterId { get; set; }
        public string EstimatedStartDate { get; set; }
        public string EstimatedEndDate { get; set; }
        public string ActualStartDate { get; set; }
        public string ActualEndDate { get; set; }
        public string CreatedBy { get; set; }
        public string DateCreated { get; set; }
        public string Status { get; set; }
        public string Program { get; set; }
        public string ProgramOffer { get; set; }
        public string Semester { get; set; }
        public bool ExaminationScheduleCreated { get; set; }
        public bool CourseScheduleCreated { get; set; }
        public bool CatScheduleCreated { get; set; }
        public bool SemesterCompletionRequestPending { get; set; }
        public string CompletionRequestedBy { get; set; }
        public string CompletionRequestReason { get; set; }
        public string CompletionRequestDate { get; set; }
        public ApprovalStatus ApprovalStatus { get; set; }


    }

    public class GetProgramOfferSemesterSchedule : IRequest<List<SemesterScheduleViewModel>>
    {
        public int ProgramOfferId { get; set; }

    }

    public class GetSemesterScheduleItemList : IRequest<List<SemesterScheduleItemViewModel>>
    {
        public int ? SemesterScheduleId { get; set; }
        public int ? Id { get; set; }

    }

    public class SemesterScheduleItemViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ScheduleItemType { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Status { get; set; }
        public int ProgramCourseId { get; set; }
        public int SemesterScheduleId { get; set; }
        public bool TimetableCreated { get; set; }
        public string CourseType { get; set; }

        public int SemesterId { get; set; }

    }

}
