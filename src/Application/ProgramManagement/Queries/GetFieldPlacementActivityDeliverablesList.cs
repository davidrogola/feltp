﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetFieldPlacementActivityDeliverablesList : IRequest<List<FieldPlacementActivityDeliverablesViewModel>>
    {
        public int ? FieldPlacementActivityId { get; set; }

    }

    public class FieldPlacementActivityDeliverablesViewModel
    {
        public int Id { get; set; }
        public int DeliverableId { get; set; }
        public int FieldPlacementActivityId { get; set; }
        public string Deliverable { get; set; }
        public string FieldPlacementActivity { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Status { get; set; }


    }

}
