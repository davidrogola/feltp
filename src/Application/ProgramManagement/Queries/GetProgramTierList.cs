﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetProgramTierList : IRequest<List<ProgramTierViewModel>>
    {
        public int? Id { get; set; }

    }

    public class ProgramTierViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; private set; }
        public string Status { get; set; }

    }

}
