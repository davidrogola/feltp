﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetCompetencyDeliverableList : IRequest<List<CompetencyDeliverableViewModel>>
    {
        public int ? CompetencyId { get; set; }
    }

    public class CompetencyDeliverableViewModel
    {
        public int Id { get; set; }
        public string Competency { get; set; }    
        public int CompetencyId { get; set; }
        public string Deliverable { get; set; }
        public int DeliverableId { get; set; }
        public string Code { get; set; }
        public int Duration { get; set; }
        public string Description { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Status { get; set; }

    }

}
