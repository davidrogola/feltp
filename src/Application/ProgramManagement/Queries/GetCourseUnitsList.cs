﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetCourseUnitsList : IRequest<List<CourseUnitViewModel>>
    {
        public int? Id { get; set; }
    }

    public class CourseUnitViewModel
    {
        public int Id { get; set; }
        public int CourseId { get; set; }
        public string CourseName { get; set; }
        public int UnitId { get; set; }
        public string UnitName { get; set; }
        public string Course { get; set; }
        public string Unit { get; set; }
        public string Code { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Status { get; set; }
        public bool HasActivity { get; set; }
    }

    public class GetCourseUnitsCount : IRequest<UnitsCount>
    {
        public int[] CourseIds { get; set; }
    }

    public class UnitsCount
    {
        public int NumberOfDidacticUnits { get; set; }
        public int NumberOfFieldActivities { get; set; }

    }
}
