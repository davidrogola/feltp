﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetProgramResourceFile : IRequest<List<ProgramResourceFilesViewModel>>
    {
        public int? resourceId { get; set; }
    }
    
    public class GetProgramResourceFileById : IRequest<ProgramResourceFilesViewModel>
    {
        public int? Id { get; set; }
    }
    public class ProgramResourceFilesViewModel
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public byte[] FileContent { get; set; }
        public string ContentType { get; set; }
        public string CreatedBy { get; set; }
        public string DateCreated { get; set; }
        public string ResourceId { get; set; }

    }
    public class ResourceFileViewModel
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public byte FileContent { get; set; }
        public string ContentType { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public string ResourceId { get; set; }

    }
}
