﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetResourceList : IRequest<List<ResourceViewModel>>
    {
        public int? ResourceTypeId { get; set; }
    }

    public class GetResource : IRequest<ResourceViewModel>
    {
        public int ResourceId { get; set; }

    }
    public class ResourceViewModel
    {
        public int Id { get; set; }
        public string ResourceTitle { get; set; }
        public string ResourceDescription { get; set; }
        public string Source { get; set; }
        public string SourceContactDetails { get; set; }
        public int ResourceTypeId { get; set; }
        public string ResourceType { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Status { get; set; }
        public List<int> ProgramId { get; set; }
    }

}
