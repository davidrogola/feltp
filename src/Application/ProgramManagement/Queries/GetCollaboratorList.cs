﻿using MediatR;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Queries
{
    public class GetCollaboratorList : IRequest<List<CollaboratorViewModel>>
    {
        public int? Id { get; set; }
    }

    public class CollaboratorViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public CollaboratorType CollaboratorTypeId { get; set; }
        public string CollaboratorType { get; set; }
        public string Program { get; set; }
        public int [] ProgramId { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; private set; }
        public string Status { get; set; }

    }
}
