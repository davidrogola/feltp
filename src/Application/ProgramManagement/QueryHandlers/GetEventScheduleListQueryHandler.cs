﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetEventScheduleQueryHandler : IRequestHandler<GetEventScheduleList, List<EventScheduleViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetEventScheduleQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<EventScheduleViewModel>> Handle(GetEventScheduleList request, CancellationToken cancellationToken)
        {
            IEnumerable<EventSchedule> eventSchedule = null;

            if (request.ProgramEventId.HasValue)
            {
                eventSchedule = unitOfWork.ProgramManagementRepository
                    .FindByWithInclude<EventSchedule>(x => x.ProgramEventId == request.ProgramEventId,x=>x.ProgramEvent.Program).ToList();
            }
            else
            {
                eventSchedule = unitOfWork.ProgramManagementRepository.FindByWithInclude<EventSchedule>(null,
                x => x.ProgramEvent.Event).ToList();

            }

            var eventScheduleViewModel = mapper.Map<List<EventScheduleViewModel>>(eventSchedule);

            return Task.FromResult(eventScheduleViewModel);
        }
    }

}