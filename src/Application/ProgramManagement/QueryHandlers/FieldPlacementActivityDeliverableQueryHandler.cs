﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.FieldPlacement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class FieldPlacementActivityDeliverableQueryHandler : IRequestHandler<GetFieldPlacementActivityDeliverablesList, List<FieldPlacementActivityDeliverablesViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public FieldPlacementActivityDeliverableQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<FieldPlacementActivityDeliverablesViewModel>> Handle(GetFieldPlacementActivityDeliverablesList request, CancellationToken cancellationToken)
        {
            var programManagementRepository = unitOfWork.ProgramManagementRepository;

            var fieldPlacementActivityId = request.FieldPlacementActivityId;
            IEnumerable<FieldPlacementActivityDeliverable> fieldPlacementActivityDeliverables;

            if (fieldPlacementActivityId.HasValue)
            {
                 fieldPlacementActivityDeliverables = programManagementRepository.FindByWithInclude<FieldPlacementActivityDeliverable>(x => x.FieldPlacementActivityId == fieldPlacementActivityId.Value, x => x.Deliverable,
                     x => x.FieldPlacementActivity);
            }
            else
            {
                fieldPlacementActivityDeliverables = programManagementRepository.FindByWithInclude<FieldPlacementActivityDeliverable>(null, x => x.FieldPlacementActivity, x => x.Deliverable);
            }

            var activityViewModel = mapper.Map<List<FieldPlacementActivityDeliverablesViewModel>>(fieldPlacementActivityDeliverables.OrderByDescending(x=>x.DateCreated));

            return Task.FromResult(activityViewModel);
        }
    }
}
