﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetCollaboratorListQueryHandler : IRequestHandler<GetCollaboratorList, List<CollaboratorViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetCollaboratorListQueryHandler(IProgramManagementUnitOfWork _unitOfWork,
            IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }

        public Task<List<CollaboratorViewModel>> Handle(GetCollaboratorList request, CancellationToken cancellationToken)
        {
            var Collaborators = request.Id.HasValue ? unitOfWork.ProgramManagementRepository
               .FindByWithInclude<Collaborator>(x => x.Id == request.Id, x => x.ProgramCollaborators)
           : unitOfWork.ProgramManagementRepository.FindByWithInclude<Collaborator>(null,
            x => x.ProgramCollaborators).ToList();

            var CollaboratorViewModel = mapper.Map<List<CollaboratorViewModel>>(Collaborators);
            return Task.FromResult(CollaboratorViewModel);
        }
    }

}
