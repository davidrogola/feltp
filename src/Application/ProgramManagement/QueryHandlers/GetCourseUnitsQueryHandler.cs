﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetCourseUnitsQueryHandler : IRequestHandler<GetCourseUnitsList, List<CourseUnitViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetCourseUnitsQueryHandler(IProgramManagementUnitOfWork _unitOfWork,IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<CourseUnitViewModel>> Handle(GetCourseUnitsList request, CancellationToken cancellationToken)
        {
            var courseUnits = request.Id.HasValue ? unitOfWork.ProgramManagementRepository.FindByWithInclude<CourseUnit>
                (x => x.CourseId == request.Id, x => x.Course, x => x.Unit)
                : unitOfWork.ProgramManagementRepository.FindByWithInclude<CourseUnit>(null, x => x.Course, x => x.Unit);

            var courseUnitViewModel = mapper.Map<List<CourseUnitViewModel>>(courseUnits);

            return Task.FromResult(courseUnitViewModel);
        }
    }

    public class GetUnitsCountQueryHandler : IRequestHandler<GetCourseUnitsCount, UnitsCount>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public GetUnitsCountQueryHandler(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }

        public Task<UnitsCount> Handle(GetCourseUnitsCount request, CancellationToken cancellationToken)
        {
            var modules = unitOfWork.ProgramManagementRepository.FindByWithInclude<CourseUnit>(x => request.CourseIds.Contains(x.CourseId),x=>x.Unit).Distinct();

            var didacticUnitsCount = modules.Count(x => x.Unit.HasFieldPlacementActivity == false);
            var activityUnitsCount = modules.Count(x => x.Unit.HasFieldPlacementActivity);

            return Task.FromResult(new UnitsCount
            {
                NumberOfDidacticUnits = didacticUnitsCount,
                NumberOfFieldActivities = activityUnitsCount
            });

        }
    }
}
