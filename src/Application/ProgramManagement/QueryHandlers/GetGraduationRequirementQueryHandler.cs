﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetGraduationRequirementQueryHandler : IRequestHandler<GetGraduationRequirementList, List<GraduationRequirementViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;

        public GetGraduationRequirementQueryHandler(IProgramManagementUnitOfWork _unitOfWork,
            IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }

        public Task<List<GraduationRequirementViewModel>> Handle(GetGraduationRequirementList request,
            CancellationToken cancellationToken)
        {
            var graduationRequirements = request.Id.HasValue ?
                unitOfWork.ProgramManagementRepository.FindByWithInclude<GraduationRequirement>(x => x.Id == request.Id.Value, x => x.Program)
                : unitOfWork.ProgramManagementRepository.FindByWithInclude<GraduationRequirement>(null, x => x.Program);

            var requirementViewModel = mapper.Map<List<GraduationRequirementViewModel>>(graduationRequirements.ToList());

            return Task.FromResult(requirementViewModel);
        }
    }
}
