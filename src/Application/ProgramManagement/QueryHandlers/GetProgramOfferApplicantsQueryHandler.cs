﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetProgramOfferApplicantsQueryHandler : IRequestHandler<GetProgramOfferApplicationsQuery, List<ProgramOfferApplicationViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetProgramOfferApplicantsQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<ProgramOfferApplicationViewModel>> Handle(GetProgramOfferApplicationsQuery request,
            CancellationToken cancellationToken)
        {
            IEnumerable<ProgramOfferApplicants> applications = null;

            switch (request.ApplicationStatus)
            {
                case ApplicationStatus.Pending:
                    applications = unitOfWork.ProgramManagementRepository
                        .FindBy<ProgramOfferApplicants>(x => x.ProgramOfferId == request.ProgramOfferId 
                        && x.ApplicationStatus== ApplicationStatus.Pending).ToList();
                    break;
                case ApplicationStatus.Shortlisted:
                    applications = unitOfWork.ProgramManagementRepository
                       .FindBy<ProgramOfferApplicants>(x => x.ProgramOfferId == request.ProgramOfferId
                       && x.ApplicationStatus == ApplicationStatus.Shortlisted).ToList();
                    break;
                case ApplicationStatus.All:
                    applications = unitOfWork.ProgramManagementRepository
                       .FindBy<ProgramOfferApplicants>(x => x.ProgramOfferId == request.ProgramOfferId).ToList();
                    break;
               
            }
           

            var applicantsViewModel = mapper.Map<List<ProgramOfferApplicationViewModel>>(applications);

            return Task.FromResult(applicantsViewModel);

        }
    }

    public class GetProgramOfferApplicationByApplicantIdQueryHandler : IRequestHandler<GetProgramOfferApplicationByApplicantId, List<ProgramOfferApplicationViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetProgramOfferApplicationByApplicantIdQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }

        public Task<List<ProgramOfferApplicationViewModel>> Handle(GetProgramOfferApplicationByApplicantId request, CancellationToken cancellationToken)
        {
            var applications = unitOfWork.ProgramManagementRepository.FindBy<ProgramOfferApplicants>
                (x => x.ApplicantId == request.ApplicantId).ToList();

            var applicantsViewModel = mapper.Map<List<ProgramOfferApplicationViewModel>>(applications);

            return Task.FromResult(applicantsViewModel);
        }
    }

    public class GetOfferApplicationByIdQueryHandler : IRequestHandler<GetProgramOfferApplicationbyId, ProgramOfferApplicationViewModel>
    {
        IProgramManagementUnitOfWork programUnitOfWork;
        IMapper mapper;
        public GetOfferApplicationByIdQueryHandler(IProgramManagementUnitOfWork _programUnitOfWork,IMapper _mapper)
        {
            programUnitOfWork = _programUnitOfWork;
            mapper = _mapper;
        }
        public Task<ProgramOfferApplicationViewModel> Handle(GetProgramOfferApplicationbyId request, CancellationToken cancellationToken)
        {
            var offerApplication = programUnitOfWork.ProgramManagementRepository.Get<ProgramOfferApplicants>(request.Id);

            var model =  mapper.Map<ProgramOfferApplicants,ProgramOfferApplicationViewModel>(offerApplication);

            return Task.FromResult(model);
        }
    }
}
