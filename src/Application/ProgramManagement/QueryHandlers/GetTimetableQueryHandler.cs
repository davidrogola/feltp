﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetTimetableQueryHandler : IRequestHandler<GetTimetableListQuery, List<TimetableViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetTimetableQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<TimetableViewModel>> Handle(GetTimetableListQuery request, CancellationToken cancellationToken)
        {
            var timetableDetails = unitOfWork.ProgramManagementRepository.FindByWithInclude<Timetable>
                (x => x.SemesterScheduleItemId == request.SemesterScheduleItemId, x => x.SemesterScheduleItem, 
                x => x.Faculty.Person,x => x.Unit, x => x.Examination.Course);
            if (request.TimetableTypes != null)
                timetableDetails = timetableDetails.Where(x => request.TimetableTypes.Contains(x.TimeTableType));
            
            var model = mapper.Map<List<TimetableViewModel>>(timetableDetails);

            return Task.FromResult(model);
        }
    }

    public class GetTimetableItemQueryHandler : IRequestHandler<GetTimetableItemQuery, TimetableViewModel>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetTimetableItemQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<TimetableViewModel> Handle(GetTimetableItemQuery request, CancellationToken cancellationToken)
        {
            var timetableDetails = unitOfWork.ProgramManagementRepository.FindByWithInclude<Timetable>
                (x => x.Id == request.Id, x => x.SemesterScheduleItem, x => x.Faculty.Person, x => x.Unit, x => x.Examination.Unit)
                .SingleOrDefault();

            var model = mapper.Map<TimetableViewModel>(timetableDetails);

            return Task.FromResult(model);
        }
    }

    public class GetTimetableOfferIdQueryHandler : IRequestHandler<GetTimetableByProgramOfferId, List<TimetableViewModel>>
    {
        IProgramManagementUnitOfWork programUnitOfWork;
        IMapper mapper;
        public GetTimetableOfferIdQueryHandler(IProgramManagementUnitOfWork _programUnitOfWork, IMapper _mapper)
        {
            programUnitOfWork = _programUnitOfWork;
            mapper = _mapper;
        }
        public Task<List<TimetableViewModel>> Handle(GetTimetableByProgramOfferId request, CancellationToken cancellationToken)
        {
            var semesterScheduleItems = request.SemesterId.HasValue ? programUnitOfWork.ProgramManagementRepository
                .FindByWithInclude<SemesterSchedule>(x => x.ProgramOfferId == request.Id && x.SemesterId == request.SemesterId.Value,
                x => x.SemesterScheduleItems) :
                programUnitOfWork.ProgramManagementRepository.FindByWithInclude<SemesterSchedule>(x => x.ProgramOfferId == request.Id,x=>x.SemesterScheduleItems);

            if (!semesterScheduleItems.Any())
                return Task.FromResult(new List<TimetableViewModel>());

            var scheduleItemIds = semesterScheduleItems
                .SelectMany(x => x.SemesterScheduleItems)
                .Select(x=>x.Id).ToList();

            var timetableList = programUnitOfWork.ProgramManagementRepository
                .FindByWithInclude<Timetable>(x => scheduleItemIds.Contains(x.SemesterScheduleItemId) && x.TimeTableType == request.Type,
                x => x.SemesterScheduleItem.SemesterSchedule.Semester,
                x => x.Faculty.Person, 
                x => x.Unit, 
                x => x.Examination.Unit).ToList();

            var model = mapper.Map<List<TimetableViewModel>>(timetableList);

            return Task.FromResult(model);
        }
    }
}
