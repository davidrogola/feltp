﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetSemesterQueryHandler : IRequestHandler<GetSemesterList, List<SemesterViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public GetSemesterQueryHandler(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<List<SemesterViewModel>> Handle(GetSemesterList request, CancellationToken cancellationToken)
        {
            var semesters = request.Id.HasValue ? unitOfWork.ProgramManagementRepository.FindByWithInclude<Semester>(x => x.Id == request.Id)
            : unitOfWork.ProgramManagementRepository.GetAll<Semester>();
            var semesterViewModel = Mapper.Map<List<SemesterViewModel>>(semesters);

            return Task.FromResult(semesterViewModel);
        }
    }
}
