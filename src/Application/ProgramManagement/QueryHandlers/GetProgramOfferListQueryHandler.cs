﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetProgramOfferListQueryHandler : IRequestHandler<GetProgramOfferList, List<ProgramOfferViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetProgramOfferListQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<ProgramOfferViewModel>> Handle(GetProgramOfferList request, CancellationToken cancellationToken)
        {

            Expression<Func<ProgramOffer, bool>> predicate = null;

            var programOffer = unitOfWork.ProgramManagementRepository
                .FindByWithInclude(request.ProgramId.HasValue ? x => x.ProgramId == request.ProgramId.Value : predicate,
                x => x.Faculty.Person,
                x => x.Program).OrderByDescending(x => x.DateCreated);

            var programOfferViewModel = mapper.Map<List<ProgramOfferViewModel>>(programOffer);

            return Task.FromResult(programOfferViewModel);
        }
    }


    public class GetProgramOfferByOfferIdQueryHandler : IRequestHandler<GetProgramOfferById, ProgramOfferViewModel>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetProgramOfferByOfferIdQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<ProgramOfferViewModel> Handle(GetProgramOfferById request, CancellationToken cancellationToken)
        {
            var programOffer = unitOfWork.ProgramManagementRepository.FindByWithInclude<ProgramOffer>
                (x => x.Id == request.OfferId, 
                x => x.Faculty.Person, 
                x => x.Program, 
                x => x.ProgramOfferLocations).SingleOrDefault();

            var hasEligibiltyRequirements = unitOfWork.ProgramManagementRepository
                .FindBy<ProgramOfferGeneralRequirement>(x => x.ProgramOfferId == request.OfferId).Any();

            var programOfferViewModel = mapper.Map<ProgramOfferViewModel>(programOffer);

            if (programOfferViewModel != null)
                programOfferViewModel.HasEligiblityRequirements = hasEligibiltyRequirements;
            
            return Task.FromResult(programOfferViewModel);
        }
    }

}


