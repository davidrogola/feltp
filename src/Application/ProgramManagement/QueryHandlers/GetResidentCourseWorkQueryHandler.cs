﻿using Application.ResidentManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetResidentCourseWorkQueryHandler : IRequestHandler<GetResidentCourseWork, List<ResidentCourseWorkViewModel>>
    {
        IResidentManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetResidentCourseWorkQueryHandler(IResidentManagementUnitOfWork _unitOfWork,IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<ResidentCourseWorkViewModel>> Handle(GetResidentCourseWork request, CancellationToken cancellationToken)
        {
            var residentCourseWork = unitOfWork.ResidentManagementRepository.FindByWithInclude<ResidentCourseWork>(x => x.ProgramEnrollmentId == request.EnrollmentId, x => x.ProgramCourse.Course.Category.CourseType,x=>x.ProgramCourse.Semester);

            if (request.SemesterId.HasValue)
                residentCourseWork = residentCourseWork.Where(x => x.ProgramCourse.SemesterId == request.SemesterId.Value).ToList();

            var residentCourseWorkModel = mapper.Map<List<ResidentCourseWorkViewModel>>(residentCourseWork);

            return Task.FromResult(residentCourseWorkModel);

        }
    }
}
