﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.FieldPlacement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetFieldPlacementActivityListQueryHandler : IRequestHandler<GetFieldPlacementActivityList, List<FieldPlacementActivityViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;

        public GetFieldPlacementActivityListQueryHandler(IProgramManagementUnitOfWork _unitOfWork, 
            IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;

        }

        public Task<List<FieldPlacementActivityViewModel>> Handle(GetFieldPlacementActivityList request, CancellationToken cancellationToken)
        {
            var fieldPlacementActivities = request.Id.HasValue ? unitOfWork.ProgramManagementRepository
                .FindByWithInclude<FieldPlacementActivity>(x => x.Id == request.Id, x => x.ActivityDeliverables)
            : unitOfWork.ProgramManagementRepository.FindByWithInclude<FieldPlacementActivity>(null,
             x => x.ActivityDeliverables).ToList();

            var activityViewModel = mapper.Map<List<FieldPlacementActivityViewModel>>(fieldPlacementActivities);

            return Task.FromResult(activityViewModel);

        }
    }
}
