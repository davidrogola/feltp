﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using Common.Models;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.FieldPlacement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetFieldPlacementListQueryHandler : IRequestHandler<GetFieldPlacementSiteList, List<FieldPlacementSiteViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetFieldPlacementListQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<FieldPlacementSiteViewModel>> Handle(GetFieldPlacementSiteList request, CancellationToken cancellationToken)
        {
            var fieldPlacememntSites = request.Id.HasValue ? unitOfWork.ProgramManagementRepository
                .FindByWithInclude<FieldPlacementSite>(x => x.Id == request.Id, x => x.SiteSupervisors)
            : unitOfWork.ProgramManagementRepository.FindByWithInclude<FieldPlacementSite>(null,
                x => x.Country,
                x => x.County,
                x => x.SubCounty,
                x => x.SiteSupervisors).ToList();

            var fieldPlacementSiteViewModel = mapper.Map<List<FieldPlacementSiteViewModel>>(fieldPlacememntSites);

            return Task.FromResult(fieldPlacementSiteViewModel);
        }
    }

    public class GetFieldSiteSupervisorQueryHandler : IRequestHandler<GetFieldSiteSupervisor, List<FieldSiteSupervisorViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public GetFieldSiteSupervisorQueryHandler(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<List<FieldSiteSupervisorViewModel>> Handle(GetFieldSiteSupervisor request, CancellationToken cancellationToken)
        {
            var siteSupervisor = unitOfWork.ProgramManagementRepository.FindByWithInclude<FieldPlacementSiteSupervisor>(null,
                x => x.FieldPlacementSite, x => x.Faculty.Person);

            var siteSupervisorViewModel = siteSupervisor.Select(x => new FieldSiteSupervisorViewModel
            {
                FieldSiteId = x.FieldPlacementSiteId,
                SiteName = x.FieldPlacementSite.Name,
                SupervisorId = x.Id,
                SupervisorName = x.Faculty.Person.FirstName + " " + x.Faculty.Person.LastName
            }).ToList();

            return Task.FromResult(siteSupervisorViewModel);
        }
    }

}
