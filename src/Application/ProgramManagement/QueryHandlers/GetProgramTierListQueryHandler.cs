﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.Generic;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetProgramTierListQueryHandler : IRequestHandler<GetProgramTierList, List<ProgramTierViewModel>>
    {

        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;

        public GetProgramTierListQueryHandler(IProgramManagementUnitOfWork _unitOfWork,
            IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }

        public Task<List<ProgramTierViewModel>> Handle(GetProgramTierList request, CancellationToken cancellationToken)
        {
            var programTiers = request.Id.HasValue ? unitOfWork.ProgramManagementRepository.FindBy<ProgramTier>(x => x.Id == request.Id)
                : unitOfWork.ProgramManagementRepository.GetAll<ProgramTier>();
            var facultyRoleViewModel = mapper.Map<List<ProgramTierViewModel>>(programTiers);
            return Task.FromResult(facultyRoleViewModel);
        }
    }
}
