﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetResourceTypeListQueryHandler : IRequestHandler<GetResourceTypeList, List<ResourceTypeViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public GetResourceTypeListQueryHandler(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<List<ResourceTypeViewModel>> Handle(GetResourceTypeList request, CancellationToken cancellationToken)
        {
            var resourcetypes = request.Id.HasValue ? unitOfWork.ProgramManagementRepository.FindBy<ResourceType>(x => x.Id == request.Id)
                : unitOfWork.ProgramManagementRepository.GetAll<ResourceType>();
            var resourcetypeViewModel = Mapper.Map<List<ResourceTypeViewModel>>(resourcetypes);

            return Task.FromResult(resourcetypeViewModel);
        }
    }
}
