﻿using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
   public  class GetProgramOfferRequirementsQuery : IRequest<ProgramOfferRequirementsViewModel>
    {
        public int ProgramOfferId { get; set; }
    }

    public class GetProgramOfferRequirementsQueryHandler : IRequestHandler<GetProgramOfferRequirementsQuery, ProgramOfferRequirementsViewModel>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetProgramOfferRequirementsQueryHandler(IProgramManagementUnitOfWork _unitOfWork,IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }

        public Task<ProgramOfferRequirementsViewModel> Handle(GetProgramOfferRequirementsQuery request, CancellationToken cancellationToken)
        {
            var generalRequirements = unitOfWork.ProgramManagementRepository
               .FindByWithInclude<ProgramOfferGeneralRequirement>(x => x.ProgramOfferId == request.ProgramOfferId, x => x.ProgramOffer.Program)
               .SingleOrDefault();

            var academicRequirements = unitOfWork.ProgramManagementRepository
                .FindByWithInclude<ProgramOfferAcademicRequirement>(x => x.ProgramOfferId == request.ProgramOfferId, x => x.MinimumCourseGrading, x => x.AcademicCourse);

            var programCompletionRequirement = unitOfWork.ProgramManagementRepository
                .FindByWithInclude<ProgramCompletionRequirement>(x => x.ProgramOfferId == request.ProgramOfferId, x => x.CompletedProgram,x=>x.ProgramOffer.Program);

            var programOfferRequirementsModel = new ProgramOfferRequirementsViewModel
            {
                ProgramOfferId = request.ProgramOfferId,
                DateCreated = generalRequirements?.DateCreated.ToShortDateString(),
                CreatedBy = generalRequirements?.CreatedBy,
                ProgramOfferName = generalRequirements?.ProgramOffer.Name,
                ProgramName = generalRequirements?.ProgramOffer.Program.Name,
                GeneralRequirementsViewModel = mapper.Map<GeneralRequirementsViewModel>(generalRequirements),
                AcademicRequirementsViewModel = mapper.Map<List<AcademicRequirementsViewModel>>(academicRequirements),
                ProgramCompletionViewModel = mapper.Map<List<ProgramCompletionViewModel>>(programCompletionRequirement)
            };

            return Task.FromResult(programOfferRequirementsModel);
        }
    }

    public class ProgramOfferRequirementsViewModel
    {
        public GeneralRequirementsViewModel GeneralRequirementsViewModel { get; set; }
        public List<AcademicRequirementsViewModel> AcademicRequirementsViewModel { get; set; }
        public List<ProgramCompletionViewModel> ProgramCompletionViewModel { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public int ProgramOfferId { get; set; }
        public string ProgramOfferName { get; set; }
        public string ProgramName { get; set; }
        
    }

    public class GeneralRequirementsViewModel
    {
        public int MinimumWorkExperienceDuration { get; set; }
        public string BasicComputerKnowledge { get; set; }
        public string InternalTravelRequired { get; set; }
        public string ExternalTravelRequired { get; set; }
        public string WillingnessToRelocate { get; set; }
    }

    public class AcademicRequirementsViewModel
    {
        public int Id { get; set; }
        public string CourseName { get; set; }
        public string MinimunGrade { get; set; }
        public int AcademicCourseId { get; set; }
        public int? MinimumCourseGradingId { get; set; }
        public string EducationLevel { get; set; }
    }

    public class ProgramCompletionViewModel
    {
        public int Id { get; set; }
        public int CompletedProgramId { get; set; }
        public string CompletedProgramName { get; set; }
        public string ProgramName { get; set; }

    }
}
