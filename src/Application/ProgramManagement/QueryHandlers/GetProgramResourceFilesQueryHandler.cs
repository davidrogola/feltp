﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetProgramResourceFilesQueryHandler : IRequestHandler<GetProgramResourceFile, List<ProgramResourceFilesViewModel>>
    {
        public IProgramManagementUnitOfWork unitOfWork;
        public IMapper mapper;

        public GetProgramResourceFilesQueryHandler(IProgramManagementUnitOfWork _unitOfWork,
            IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }


        public Task<List<ProgramResourceFilesViewModel>> Handle(GetProgramResourceFile request, CancellationToken cancellationToken)
        {
            var programResourcesFiles = request.resourceId.HasValue ? unitOfWork.ProgramManagementRepository.FindByWithInclude<ProgramResourceFilesView>
                (x => x.ResourceId == request.resourceId.Value)
                : unitOfWork.ProgramManagementRepository.FindByWithInclude<ProgramResourceFilesView>(null);

            var programResourcesViewModel = mapper.Map<List<ProgramResourceFilesViewModel>>(programResourcesFiles);

            return Task.FromResult(programResourcesViewModel);
        }
    }

    public class GetProgramResourceFilesByIdQueryHandler : IRequestHandler<GetProgramResourceFileById, ProgramResourceFilesViewModel>
    {
        public IProgramManagementUnitOfWork unitOfWork;
        public IMapper mapper;

        public GetProgramResourceFilesByIdQueryHandler(IProgramManagementUnitOfWork _unitOfWork,
            IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }

        public Task<ProgramResourceFilesViewModel> Handle(GetProgramResourceFileById request, CancellationToken cancellationToken)
        {
            var programResourcesFiles = unitOfWork.ProgramManagementRepository.FindBy<ProgramResourceFilesView>
                (x => x.Id == request.Id.Value).SingleOrDefault();
            var programResourcesViewModel = mapper.Map<ProgramResourceFilesViewModel>(programResourcesFiles);
            return Task.FromResult(programResourcesViewModel);
        }
    }


}
