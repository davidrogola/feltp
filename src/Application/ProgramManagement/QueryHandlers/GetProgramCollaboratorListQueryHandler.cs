﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ProgramManagement.Domain.Program;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetProgramCollaboratorQueryHandler : IRequestHandler<GetProgramCollaboratorList, List<ProgramCollaboratorViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetProgramCollaboratorQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<ProgramCollaboratorViewModel>> Handle(GetProgramCollaboratorList request, CancellationToken cancellationToken)
        {
            IEnumerable<ProgramCollaborator> programCollaborator = null;

            switch (request.GetBy)
            {
                case GetCollaboratorBy.Id:
                    programCollaborator = unitOfWork.ProgramManagementRepository.FindByWithInclude<ProgramCollaborator>(x => x.Id == request.Id
                    , x => x.Program, x => x.Collaborator);
                    break;
                case GetCollaboratorBy.ProgramId:
                    programCollaborator = unitOfWork.ProgramManagementRepository
                          .FindByWithInclude<ProgramCollaborator>(x => x.ProgramId == request.Id.Value, x => x.Program, x => x.Collaborator);
                    break;
                case GetCollaboratorBy.CollaboratorId:
                    programCollaborator = unitOfWork.ProgramManagementRepository
                        .FindByWithInclude<ProgramCollaborator>(x => x.CollaboratorId == request.Id.Value, x => x.Program, x => x.Collaborator);
                    break;
                default:
                    programCollaborator = unitOfWork.ProgramManagementRepository.FindByWithInclude<ProgramCollaborator>(null, x => x.Program, x => x.Collaborator);
                    break;
            }

            var programCollaboratorViewModel = mapper.Map<List<ProgramCollaboratorViewModel>>(programCollaborator);

            return Task.FromResult(programCollaboratorViewModel);
        }
    }

}
