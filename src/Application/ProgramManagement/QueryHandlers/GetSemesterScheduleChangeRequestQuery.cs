﻿using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
   
    public class GetSemesterScheduleChangeRequestQueryHandler : IRequestHandler<GetSemesterScheduleChangeRequestQuery, 
        List<SemesterScheduleChangeRequestViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetSemesterScheduleChangeRequestQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<SemesterScheduleChangeRequestViewModel>> Handle(GetSemesterScheduleChangeRequestQuery request, CancellationToken cancellationToken)
        {
            var semesterChangeRequests = request.Id.HasValue ? unitOfWork.ProgramManagementRepository.FindByWithInclude<SemesterScheduleChangeRequest>
                (x=>x.Id == request.Id.Value,x=>x.SemesterSchedule):unitOfWork.ProgramManagementRepository.FindByWithInclude<SemesterScheduleChangeRequest>
                (null, x => x.SemesterSchedule.ProgramOffer.Program.Courses);

            var changeRequestModel = mapper.Map<List<SemesterScheduleChangeRequestViewModel>>(semesterChangeRequests);

            return Task.FromResult(changeRequestModel);
        }
    }

    public class GetSemesterScheduleChangeRequestQuery : IRequest<List<SemesterScheduleChangeRequestViewModel>>
    {
        public int? Id { get; set; }
    }

    public class SemesterScheduleChangeRequestViewModel
    {
        public int Id { get; set; }
        public string ProgramOffer { get; set; }
        public string Program { get; set; }
        public string Semester { get; set; }
        public string DateRequested { get; set; }
        public string Comment { get; set; }
        public string RequestedBy { get; set; }
        public int SemesterScheduleId { get; set; }
        public string ScheduleName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string OriginalStartDate { get; set; }
        public string OriginalEndDate { get; set; }
        public int Duration { get; set; }
        public string ApprovalStatus { get; set; }

    }
}
