using System.Collections.Generic;
using Application.Common.Services;
using ProgramManagement.Domain.Examination;
using ProgramManagement.Domain.Program;

namespace Application.ProgramManagement.QueryHandlers
{
    public static class ExamTypeHelper
    {
        private static readonly Dictionary<TimetableType, ExamType> TimeTableToExamMap =
            new Dictionary<TimetableType, ExamType>()
            {
                {TimetableType.Examination, ExamType.Final},
                {TimetableType.CatOne, ExamType.CatOne},
                {TimetableType.CatTwo, ExamType.CatTwo},
                {TimetableType.CatThree, ExamType.CatThree},
            };
        
        public static string GetExamTypeStr(TimetableType timetableType)
        {
            var examType = TimeTableToExamMap[timetableType];
            return DispalyAttributeFetcher.GetDisplayAttributeValue(typeof(ExamType), examType.ToString());
        }
        
        public static ExamType GetExamTypeEnum(TimetableType timetableType)
        {
            return TimeTableToExamMap[timetableType];
         
        }

    }
}