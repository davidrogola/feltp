﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetProgramResourcesListQueryHandler : IRequestHandler<GetProgramResourcesList, List<ProgramResourcesViewModel>>
    {
        public IProgramManagementUnitOfWork unitOfWork;
        public IMapper mapper;

        public GetProgramResourcesListQueryHandler(IProgramManagementUnitOfWork _unitOfWork,
            IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }


        public Task<List<ProgramResourcesViewModel>> Handle(GetProgramResourcesList request, CancellationToken cancellationToken)
        {
            var programResources = request.ResourceId.HasValue ? unitOfWork.ProgramManagementRepository.FindByWithInclude<ProgramResource>
                (x=>x.ResourceId == request.ResourceId.Value, x => x.Program.ProgramTier, x => x.Resource.ResourceType)
                : unitOfWork.ProgramManagementRepository.FindByWithInclude<ProgramResource>(null, x => x.Program.ProgramTier, 
                x => x.Resource.ResourceType);

            var programResourcesViewModel = mapper.Map<List<ProgramResourcesViewModel>>(programResources.ToList());

            return Task.FromResult(programResourcesViewModel);
        }
    }

    public class GetProgramResourceHandler : IRequestHandler<GetProgramResource, ProgramResourcesViewModel>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetProgramResourceHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<ProgramResourcesViewModel> Handle(GetProgramResource request, CancellationToken cancellationToken)
        {
            var programResource = unitOfWork.ProgramManagementRepository.FindByWithInclude<ProgramResource>(x => x.Id == request.Id, x => x.Resource.ResourceType, x => x.Program.ProgramTier).SingleOrDefault();

            var resourceViewModel = mapper.Map<ProgramResourcesViewModel>(programResource);

            return Task.FromResult(resourceViewModel);
        }
    }

}
