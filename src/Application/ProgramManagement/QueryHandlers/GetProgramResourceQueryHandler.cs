﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetProgramResourceQueryHandler : IRequestHandler<GetProgramResourcesByProgramId, List<ProgramResourceViewModel>>
    {
        public IProgramManagementUnitOfWork unitOfWork;
        public IMapper mapper;

        public GetProgramResourceQueryHandler(IProgramManagementUnitOfWork _unitOfWork,
            IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }

        public Task<List<ProgramResourceViewModel>> Handle(GetProgramResourcesByProgramId request, CancellationToken cancellationToken)
        {
            var programResources = request.programId.HasValue ? unitOfWork.ProgramManagementRepository.FindByWithInclude<ProgramResourceView>
                (x => x.ProgramId == request.programId.Value)
                : unitOfWork.ProgramManagementRepository.FindByWithInclude<ProgramResourceView>(null, x => x.ProgramId,
                x => x.ProgramId);

            var programResourcesViewModel = mapper.Map<List<ProgramResourceViewModel>>(programResources);

            return Task.FromResult(programResourcesViewModel);
        }
    }
}
