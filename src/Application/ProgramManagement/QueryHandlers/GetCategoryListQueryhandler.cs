﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetCategoryQueryHandler : IRequestHandler<GetCategoryList, List<CategoryViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetCategoryQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<CategoryViewModel>> Handle(GetCategoryList request, CancellationToken cancellationToken)
        {
            var categories = request.Id.HasValue ? unitOfWork.ProgramManagementRepository.FindByWithInclude<Category>(x => x.Id == request.Id,
                x => x.CourseType)
                : unitOfWork.ProgramManagementRepository.FindByWithInclude<Category>(null, x => x.CourseType);

            if (request.CourseTypeId.HasValue)
                categories = unitOfWork.ProgramManagementRepository.FindByWithInclude<Category>(x => x.CourseTypeId == request.CourseTypeId);

            var categoryViewModel = Mapper.Map<List<CategoryViewModel>>(categories);

            return Task.FromResult(categoryViewModel);
        }


    }
}
