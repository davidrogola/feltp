﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;
using MediatR;

using Persistance.ProgramManagement.UnitOfWork;
using Application.ProgramManagement.Queries;
using ProgramManagement.Domain.Program;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetProgramListQueryHandler : IRequestHandler<GetProgramList, List<ProgramViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetProgramListQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }

        public Task<List<ProgramViewModel>> Handle(GetProgramList request, CancellationToken cancellationToken)
        {
            var programs = request.Id.HasValue ? unitOfWork.ProgramManagementRepository.FindByWithInclude<Program>(x => x.Id == request.Id, x => x.ProgramTier)
                : unitOfWork.ProgramManagementRepository.FindByWithInclude<Program>(null, x => x.ProgramTier);

            var programViewModel = mapper.Map<List<ProgramViewModel>>(programs.OrderByDescending(x=>x.DateCreated).ToList());

            return Task.FromResult(programViewModel);
        }
    }
}
