﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Examination;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetExaminationListQueryHandler : IRequestHandler<GetExaminationListQuery, List<ExaminationViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetExaminationListQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<ExaminationViewModel>> Handle(GetExaminationListQuery request, CancellationToken cancellationToken)
        {
            var examinations = unitOfWork.ProgramManagementRepository
                .FindBy<Examination>(x => x.CourseId.HasValue)
                .ToList();

            var examsModel = mapper.Map<List<ExaminationViewModel>>(examinations);

            return Task.FromResult(examsModel);
        }
    }
}
