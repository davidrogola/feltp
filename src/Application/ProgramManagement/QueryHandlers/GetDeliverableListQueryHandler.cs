﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetDeliverableListQueryHandler : IRequestHandler<GetDeliverableList, List<DeliverableViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;

        public GetDeliverableListQueryHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork)
        {
            unitOfWork = _programManagementUnitOfWork;
        }

        public Task<List<DeliverableViewModel>> Handle(GetDeliverableList request, CancellationToken cancellationToken)
        {
            var deliverables = request.Id.HasValue ? unitOfWork.ProgramManagementRepository.FindByWithInclude<Deliverable>(x => x.Id == request.Id)
            : unitOfWork.ProgramManagementRepository.GetAll<Deliverable>();
            var deliverableViewModel = Mapper.Map<List<DeliverableViewModel>>(deliverables);

            return Task.FromResult(deliverableViewModel);
        }
    }
}
