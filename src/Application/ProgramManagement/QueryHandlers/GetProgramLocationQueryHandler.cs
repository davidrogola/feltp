﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using IdentityModel;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetProgramOfferLocationQueryHandler : IRequestHandler<GetProgramOfferLocationQuery, List<ProgramLocationViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetProgramOfferLocationQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<ProgramLocationViewModel>> Handle(GetProgramOfferLocationQuery request, CancellationToken cancellationToken)
        {


            List<ProgramLocation> programLocations = null;

            if (request.ProgramOfferId.HasValue)
            {
                programLocations = unitOfWork.ProgramManagementRepository.FindByWithInclude<ProgramOfferLocation>
                        (x => x.ProgramOfferId == request.ProgramOfferId, 
                            x => x.ProgramLocation.SubCounty.County.Country)
                    .Select(x => x.ProgramLocation).ToList();
            }
            else
            {
                programLocations = unitOfWork.ProgramManagementRepository.FindByWithInclude<ProgramLocation>
                    (x=>x.Id > 0,x => x.SubCounty.County.Country).ToList();
            }

            var locationViewModel = mapper.Map<List<ProgramLocationViewModel>>(programLocations);

            return Task.FromResult(locationViewModel);

        }
    }

    public class GetProgramLocationQueryHandler : IRequestHandler<GetProgramLocationQuery, ProgramLocationViewModel>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetProgramLocationQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }

        public Task<ProgramLocationViewModel> Handle(GetProgramLocationQuery request, CancellationToken cancellationToken)
        {
            var programLocation = unitOfWork.ProgramManagementRepository.Get<ProgramLocation>(request.Id);

            var programLocationViewModel = mapper.Map<ProgramLocationViewModel>(programLocation);

            return Task.FromResult(programLocationViewModel);
        }
    }

}
