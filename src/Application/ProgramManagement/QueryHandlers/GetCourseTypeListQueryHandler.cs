﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetCourseTypeListQueryHandler : IRequestHandler<GetCourseTypeList, List<CourseTypeViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetCourseTypeListQueryHandler(IProgramManagementUnitOfWork _unitOfWork,
            IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }

        public Task<List<CourseTypeViewModel>> Handle(GetCourseTypeList request, CancellationToken cancellationToken)
        {

            var courseTypes = request.Id.HasValue ? unitOfWork.ProgramManagementRepository.FindBy<CourseType>(x => x.Id == request.Id)
                : unitOfWork.ProgramManagementRepository.GetAll<CourseType>();
            var courseTypeViewModel = mapper.Map<List<CourseTypeViewModel>>(courseTypes);
            return Task.FromResult(courseTypeViewModel);
        }
    }
}
