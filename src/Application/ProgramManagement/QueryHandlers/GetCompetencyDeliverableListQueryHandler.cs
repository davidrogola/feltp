﻿using Application.MapperProfiles.ProgramManagement;
using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetCompetencyDeliverableListQueryHandler : IRequestHandler<GetCompetencyDeliverableList, List<CompetencyDeliverableViewModel>>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;

        public GetCompetencyDeliverableListQueryHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
        }

        public Task<List<CompetencyDeliverableViewModel>> Handle(GetCompetencyDeliverableList request, CancellationToken cancellationToken)
        {
            IEnumerable<CompetencyDeliverable> competencyDeliverable = null;
            if (request.CompetencyId.HasValue)
            {
                competencyDeliverable = programManagementUnitOfWork.ProgramManagementRepository
                    .FindByWithInclude<CompetencyDeliverable>(x => x.CompetencyId == request.CompetencyId.Value, x => x.Competency, x => x.Deliverable)
                    .OrderByDescending(x => x.DateCreated);
            }
            else
            {
                competencyDeliverable = programManagementUnitOfWork.ProgramManagementRepository
                    .FindByWithInclude<CompetencyDeliverable>(null,x => x.Competency, x => x.Deliverable)
                    .OrderByDescending(x => x.DateCreated);
            }
            var competencydeliverableViewModel = Mapper.Map<List<CompetencyDeliverableViewModel>>(competencyDeliverable);

            return Task.FromResult(competencydeliverableViewModel);
        }
    }
}

