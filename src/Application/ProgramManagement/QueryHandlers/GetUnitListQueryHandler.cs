﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetUnitListQueryHandler : MediatR.IRequestHandler<GetUnitList, List<UnitViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;

        public GetUnitListQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<UnitViewModel>> Handle(GetUnitList request, CancellationToken cancellationToken)
        {
            var units = request.IsDropDownRequest ? unitOfWork.ProgramManagementRepository.FindBy<Unit>(x => x.HasFieldPlacementActivity == request.HasActivity) :
                unitOfWork.ProgramManagementRepository.GetAll<Unit>();

            var unitViewModel =  mapper.Map<List<UnitViewModel>>(units).OrderByDescending(x=>x.DateCreated).ToList();

            return Task.FromResult(unitViewModel);
        }
    }

    public class GetUnitQueryHandler : MediatR.IRequestHandler<GetUnit, UnitViewModel>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetUnitQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }

        public Task<UnitViewModel> Handle(GetUnit request, CancellationToken cancellationToken)
        {
            var unit = unitOfWork.ProgramManagementRepository.FindByWithInclude<Unit>
                (x=>x.Id ==request.Id,x=>x.FieldPlacementActivities).SingleOrDefault();

            var unitViewModel = mapper.Map<UnitViewModel>(unit);

            return Task.FromResult(unitViewModel);
        }
    }
}
