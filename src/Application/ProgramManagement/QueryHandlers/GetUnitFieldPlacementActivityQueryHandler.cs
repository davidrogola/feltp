﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.FieldPlacement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetUnitFieldPlacementActivityQueryHandler : IRequestHandler<GetUnitFieldPlacementActivityWithDeliverables, UnitPlacementActivityViewModel>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetUnitFieldPlacementActivityQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<UnitPlacementActivityViewModel> Handle(GetUnitFieldPlacementActivityWithDeliverables request,
            CancellationToken cancellationToken)
        {
            var unitFieldPlacementActivities = unitOfWork.ProgramManagementRepository
                .FindByWithInclude<UnitFieldPlacementActivity>(x => x.UnitId == request.UnitId,
                x => x.FieldPlacementActivity,
                x => x.Unit).ToList();

            List<FieldPlacementActivityViewModel> activites = new List<FieldPlacementActivityViewModel>();

            var moduleFieldPlacementActivitiesViewModel = new UnitPlacementActivityViewModel();

            unitFieldPlacementActivities.ForEach(x =>
            {
                moduleFieldPlacementActivitiesViewModel.UnitName = x.Unit.Name;
                moduleFieldPlacementActivitiesViewModel.UnitId = x.UnitId;

                activites.Add(mapper.Map<FieldPlacementActivityViewModel>(x.FieldPlacementActivity));
            });

            moduleFieldPlacementActivitiesViewModel.Activities = activites;

            int[] fieldActivityIds = activites.Select(x => x.Id).ToArray();

            var fieldPlacementActivityDeliverables = unitOfWork.ProgramManagementRepository.FindByWithInclude<FieldPlacementActivityDeliverable>(
                x => fieldActivityIds.Contains(x.FieldPlacementActivityId), x => x.Deliverable).ToList();

            foreach (var fieldPlacementActivity in activites)
            {
                List<DeliverableViewModel> deliverablesViewModel = new List<DeliverableViewModel>();

                foreach (var activityDeliverable in fieldPlacementActivityDeliverables)
                {
                    if (fieldPlacementActivity.Id == activityDeliverable.FieldPlacementActivityId)
                        deliverablesViewModel.Add(mapper.Map<DeliverableViewModel>(activityDeliverable.Deliverable));
                }
                fieldPlacementActivity.Deliverables = deliverablesViewModel;
            }

            return Task.FromResult(moduleFieldPlacementActivitiesViewModel);
        }
    }

    public class GetUnitFieldPlacementActivitiesQueryHandler : IRequestHandler<GetUnitFieldPlacementActivities, List<FieldPlacementActivityViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetUnitFieldPlacementActivitiesQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<FieldPlacementActivityViewModel>> Handle(GetUnitFieldPlacementActivities request, CancellationToken cancellationToken)
        {
            var unitFieldPlacementActivities = unitOfWork.ProgramManagementRepository
                .FindByWithInclude<UnitFieldPlacementActivity>(x => x.UnitId == request.UnitId,
                x => x.FieldPlacementActivity).ToList();

            var fieldPlacementActivityViewModel = mapper.Map<List<FieldPlacementActivityViewModel>>(unitFieldPlacementActivities);

            return Task.FromResult(fieldPlacementActivityViewModel);
        }
    }
}
