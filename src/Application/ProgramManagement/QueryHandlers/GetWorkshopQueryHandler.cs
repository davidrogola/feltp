﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetWorkshopQueryHandler : IRequestHandler<GetWorkshopList, List<WorkshopViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public GetWorkshopQueryHandler(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<List<WorkshopViewModel>> Handle(GetWorkshopList request, CancellationToken cancellationToken)
        {
            var workshops = request.Id.HasValue ? unitOfWork.ProgramManagementRepository.FindBy<Workshop>(x => x.Id == request.Id)
        : unitOfWork.ProgramManagementRepository.GetAll<Workshop>();
            var workshopViewModel = Mapper.Map<List<WorkshopViewModel>>(workshops);

            return Task.FromResult(workshopViewModel);
        }
    }
}
