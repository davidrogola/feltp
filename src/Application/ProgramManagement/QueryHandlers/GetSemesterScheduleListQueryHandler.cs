﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetSemesterScheduleListQueryHandler : IRequestHandler<GetSemesterScheduleList, List<SemesterScheduleViewModel>>
    {
        IMapper mapper;
        IProgramManagementUnitOfWork unitOfWork;
        public GetSemesterScheduleListQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            mapper = _mapper;
            unitOfWork = _unitOfWork;
        }
        public Task<List<SemesterScheduleViewModel>> Handle(GetSemesterScheduleList request, CancellationToken cancellationToken)
        {
            var semesterScheduleList = request.Id.HasValue ? unitOfWork.ProgramManagementRepository
                .FindByWithInclude<SemesterSchedule>(x => x.Id == request.Id.Value, x => x.ProgramOffer.Program, x => x.Semester, x => x.SemesterScheduleItems)
                : unitOfWork.ProgramManagementRepository
                .FindByWithInclude<SemesterSchedule>(null, x => x.ProgramOffer.Program, x => x.Semester, x => x.SemesterScheduleItems);

            var scheduleViewModel = mapper.Map<List<SemesterScheduleViewModel>>(semesterScheduleList.OrderByDescending(x=>x.DateCreated));

            return Task.FromResult(scheduleViewModel);
        }
    }

    public class GetSemesterScheduleItemListQueryHandler : IRequestHandler<GetSemesterScheduleItemList, List<SemesterScheduleItemViewModel>>
    {
        IMapper mapper;
        IProgramManagementUnitOfWork unitOfWork;

        public GetSemesterScheduleItemListQueryHandler(IMapper _mapper, IProgramManagementUnitOfWork _unitOfWork)

        {
            mapper = _mapper;
            unitOfWork = _unitOfWork;
        }
        public Task<List<SemesterScheduleItemViewModel>> Handle(GetSemesterScheduleItemList request, CancellationToken cancellationToken)
        {
            IEnumerable<SemesterScheduleItem> semesterScheduleItems = null;

            semesterScheduleItems = request.SemesterScheduleId.HasValue ? unitOfWork.ProgramManagementRepository
               .FindByWithInclude<SemesterScheduleItem>(x => x.SemesterScheduleId == request.SemesterScheduleId, x=>x.SemesterSchedule, x => x.TimetableCollection,
               x => x.ProgramCourse.Course.Category.CourseType)
               : unitOfWork.ProgramManagementRepository.FindByWithInclude<SemesterScheduleItem>(x => x.Id == request.Id, x=>x.SemesterSchedule, x => x.TimetableCollection, 
               x => x.ProgramCourse.Course.Category.CourseType);

            var scheduleItemViewModel = mapper.Map<List<SemesterScheduleItemViewModel>>(semesterScheduleItems);

            return Task.FromResult(scheduleItemViewModel);
        }
    }

    public class GetProgramOfferSemesterScheduleQueryHandler : IRequestHandler<GetProgramOfferSemesterSchedule, List<SemesterScheduleViewModel>>
    {
        IMapper mapper;
        IProgramManagementUnitOfWork unitOfWork;

        public GetProgramOfferSemesterScheduleQueryHandler(IMapper _mapper, IProgramManagementUnitOfWork _unitOfWork)
        {
            mapper = _mapper;
            unitOfWork = _unitOfWork;
        }
        public Task<List<SemesterScheduleViewModel>> Handle(GetProgramOfferSemesterSchedule request, CancellationToken cancellationToken)
        {
            var semesterScheduleList = unitOfWork.ProgramManagementRepository
                .FindByWithInclude<SemesterSchedule>(x => x.ProgramOfferId == request.ProgramOfferId, x => x.ProgramOffer.Program, x => x.Semester);

            var scheduleViewModel = mapper.Map<List<SemesterScheduleViewModel>>(semesterScheduleList);

            return Task.FromResult(scheduleViewModel);
        }
    }
}
