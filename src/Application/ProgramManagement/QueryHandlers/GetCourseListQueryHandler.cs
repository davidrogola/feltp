﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;
using MediatR;

using Application.ProgramManagement.Queries;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetCourseListQueryHandler : IRequestHandler<GetCourseList, List<CourseViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetCourseListQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<CourseViewModel>> Handle(GetCourseList request, CancellationToken cancellationToken)
        {
            IEnumerable<Course> courses = null;

            if (request.Id.HasValue || request.CategoryId.HasValue)
            {
                courses = request.Id.HasValue ?
                unitOfWork.ProgramManagementRepository.FindByWithInclude<Course>(x => x.Id == request.Id.Value,x=>x.Category) :
                unitOfWork.ProgramManagementRepository.FindByWithInclude<Course>(x => x.CategoryId == request.CategoryId, x => x.Category);
            }
            else
            {
                courses = unitOfWork.ProgramManagementRepository.FindByWithInclude<Course>(null, x => x.Category);
            }

            var courseViewModel = mapper.Map<List<CourseViewModel>>(courses.OrderByDescending(x => x.DateCreated).ToList());

            return Task.FromResult(courseViewModel);

        }
    }
}
