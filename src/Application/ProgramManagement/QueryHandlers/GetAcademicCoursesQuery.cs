﻿using AutoMapper;
using Common.Domain.Person;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetAcademicCoursesQuery : IRequest<List<AcademicCourseViewModel>>
    {
        public int ? Id { get; set; }

    }

    public class GetAcademicCourseGradingQuery : IRequest<List<CourseGradingViewModel>>
    {
        public int ? Id { get; set; }

    }

    public class GetAcademicCoursesQueryHandler : IRequestHandler<GetAcademicCoursesQuery, List<AcademicCourseViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetAcademicCoursesQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<AcademicCourseViewModel>> Handle(GetAcademicCoursesQuery request, CancellationToken cancellationToken)
        {
            var academicCourses = unitOfWork.ProgramManagementRepository.GetAll<AcademicCourse>();

            var academicCourseViewModel = mapper.Map<List<AcademicCourseViewModel>>(academicCourses);

            return Task.FromResult(academicCourseViewModel);
        }
    }


    public class GetAcademicCourseGradingQueryHandler : IRequestHandler<GetAcademicCourseGradingQuery, List<CourseGradingViewModel>>
    {
        IProgramManagementUnitOfWork programUnitOfWork;
        IMapper mapper;
        public GetAcademicCourseGradingQueryHandler(IProgramManagementUnitOfWork _programUnitOfWork, IMapper _mapper)
        {
            programUnitOfWork = _programUnitOfWork;
            mapper = _mapper;
        }
        public Task<List<CourseGradingViewModel>> Handle(GetAcademicCourseGradingQuery request, CancellationToken cancellationToken)
        {
            var courseGrading = programUnitOfWork.ProgramManagementRepository.GetAll<AcademicCourseGrading>();

            var courseGradingViewModel = mapper.Map<List<CourseGradingViewModel>>(courseGrading);

            return Task.FromResult(courseGradingViewModel);
        }
    }


    public class CourseGradingViewModel
    {
        public int Id { get; set; }
        public string EducationLevel { get; set; }
        public string GradeName { get; set; }
    }

    public class AcademicCourseViewModel
    {
        public int Id { get; set; }
        public string EducationLevel { get; set; }
        public string CourseName { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
    }
}
