﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetSeminarQueryHandler : IRequestHandler<GetSeminarList, List<SeminarViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public GetSeminarQueryHandler(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<List<SeminarViewModel>> Handle(GetSeminarList request, CancellationToken cancellationToken)
        {
            var seminars = request.Id.HasValue ? unitOfWork.ProgramManagementRepository.FindBy<Seminar>(x => x.Id == request.Id)
        : unitOfWork.ProgramManagementRepository.GetAll<Seminar>();
            var seminarViewModel = Mapper.Map<List<SeminarViewModel>>(seminars);

            return Task.FromResult(seminarViewModel);
        }


    }
}
