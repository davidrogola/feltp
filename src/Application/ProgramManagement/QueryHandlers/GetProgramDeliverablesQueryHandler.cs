﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using ProgramManagement.Domain.FieldPlacement;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetProgramDeliverablesQueryHandler : IRequestHandler<GetProgramDeliverables, List<DeliverableViewModel>>
    {
        IProgramManagementUnitOfWork programUnitOfWork;
        IMapper mapper;
        public GetProgramDeliverablesQueryHandler(IProgramManagementUnitOfWork _programUnitOfWork, IMapper _mapper)
        {
            programUnitOfWork = _programUnitOfWork;
            mapper = _mapper;
        }
        public Task<List<DeliverableViewModel>> Handle(GetProgramDeliverables request, CancellationToken cancellationToken)
        {
            var courseIds = programUnitOfWork.ProgramManagementRepository.FindBy<ProgramCourse>
                               (x => x.ProgramId == request.ProgramId).Select(x => x.CourseId).ToList();

            var unitIds = programUnitOfWork.ProgramManagementRepository.FindBy<CourseUnit>(x => courseIds.Contains(x.CourseId) && x.Unit.HasFieldPlacementActivity == true).Select(x => x.UnitId).Distinct();

            var fieldPlacementActivityIds = programUnitOfWork.ProgramManagementRepository.FindBy<UnitFieldPlacementActivity>
                (x => unitIds.Contains(x.UnitId)).Select(x => x.FieldPlacementActivityId).Distinct();

            var deliverables = programUnitOfWork.ProgramManagementRepository.FindByWithInclude<FieldPlacementActivityDeliverable>(x => fieldPlacementActivityIds.Contains(x.FieldPlacementActivityId),x=>x.Deliverable).ToList();

            var deliverableViewModel = deliverables.Select(x => mapper.Map<DeliverableViewModel>(x.Deliverable)).ToList();

            return Task.FromResult(deliverableViewModel);
        }
    }

    public class GetProgramDeliverables: IRequest<List<DeliverableViewModel>>
    {
        public int ProgramId { get; set; }

    }
}
