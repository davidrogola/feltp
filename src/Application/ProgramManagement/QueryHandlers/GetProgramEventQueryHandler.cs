﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using ProgramManagement.Domain.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetProgramEventQueryHandler : IRequestHandler<GetProgramEventList, List<ProgramEventViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetProgramEventQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<ProgramEventViewModel>> Handle(GetProgramEventList request, CancellationToken cancellationToken)
        {
            IEnumerable<ProgramEvent> programEvent = null;

            switch (request.GetBy)
            {
                case GetEventBy.Id:
                    programEvent = unitOfWork.ProgramManagementRepository.FindByWithInclude<ProgramEvent>(x=>x.Id==request.Id
                    , x => x.Program, x => x.Event);
                    break;
                case GetEventBy.ProgramId:
                  programEvent =  unitOfWork.ProgramManagementRepository
                        .FindByWithInclude<ProgramEvent>(x => x.ProgramId == request.Id.Value, x => x.Program, x => x.Event);
                    break;
                case GetEventBy.EventId:
                    programEvent = unitOfWork.ProgramManagementRepository
                        .FindByWithInclude<ProgramEvent>(x => x.EventId == request.Id.Value, x => x.Program, x => x.Event);
                    break;
                default:
                    programEvent = unitOfWork.ProgramManagementRepository.FindByWithInclude<ProgramEvent>(null, x => x.Program, x => x.Event);
                    break;
            }

            var programEventViewModel = mapper.Map<List<ProgramEventViewModel>>(programEvent);

            return Task.FromResult(programEventViewModel);
        }
    }

}
