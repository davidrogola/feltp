﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetProgramCoursesListQueryHandler : IRequestHandler<GetProgramCoursesList, List<ProgramCoursesViewModel>>
    {
        public IProgramManagementUnitOfWork unitOfWork;
        public IMapper mapper;

        public GetProgramCoursesListQueryHandler(IProgramManagementUnitOfWork _unitOfWork,
            IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }


        public Task<List<ProgramCoursesViewModel>> Handle(GetProgramCoursesList request, CancellationToken cancellationToken)
        {
            IEnumerable<ProgramCourse> programCourses = null;

            if (request.ProgramId.HasValue)
            {
                programCourses = unitOfWork.ProgramManagementRepository
                    .FindByWithInclude<ProgramCourse>(x => x.ProgramId == request.ProgramId.Value, x => x.Program,
                    x => x.Course, x => x.Program.ProgramTier, x => x.Semester,
                    x => x.Course.Category.CourseType).ToList();
            }
            else if(request.Id.HasValue)
            {
                programCourses = unitOfWork.ProgramManagementRepository.FindByWithInclude<ProgramCourse>(x => x.Id == request.Id,
                x => x.Program, x => x.Course, x => x.Program.ProgramTier, x => x.Semester,
                x => x.Course.Category.CourseType).ToList();
            }
            else
            {
                programCourses = unitOfWork.ProgramManagementRepository.FindByWithInclude<ProgramCourse>(x => x.SemesterId == request.SemesterId,
                    x => x.Program, x => x.Course, x => x.Program.ProgramTier, x => x.Semester,
                    x => x.Course.Category.CourseType).ToList();
            }

            var programViewModel = mapper.Map<List<ProgramCoursesViewModel>>(programCourses);

            return Task.FromResult(programViewModel);
        }
    }
}
