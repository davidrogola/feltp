﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;
using MediatR;

using Persistance.ProgramManagement.UnitOfWork;
using Application.ProgramManagement.Queries;
using ProgramManagement.Domain.Program;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetResourceListQueryHandler : IRequestHandler<GetResourceList, List<ResourceViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetResourceListQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }

        public Task<List<ResourceViewModel>> Handle(GetResourceList request, CancellationToken cancellationToken)
        {
            IEnumerable<Resource> resources = null;

            resources = request.ResourceTypeId.HasValue ? unitOfWork.ProgramManagementRepository
                .FindByWithInclude<Resource>(x => x.ResourceTypeId == request.ResourceTypeId, x => x.ResourceType) : unitOfWork.ProgramManagementRepository
                .FindByWithInclude<Resource>(null, x => x.ResourceType);

            var resourceViewModel = mapper.Map<List<ResourceViewModel>>(resources.ToList());
            return Task.FromResult(resourceViewModel);
        }
    }

    public class GetResourceQueryHandler : IRequestHandler<GetResource, ResourceViewModel>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetResourceQueryHandler(IProgramManagementUnitOfWork _unitOfWork,
        IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<ResourceViewModel> Handle(GetResource request, CancellationToken cancellationToken)
        {
            var resource = unitOfWork.ProgramManagementRepository.FindByWithInclude<Resource>(x => x.Id == request.ResourceId, x => x.ResourceType, 
                x => x.LinkedPrograms).SingleOrDefault();

            var resourceViewModel = mapper.Map<ResourceViewModel>(resource);
            return Task.FromResult(resourceViewModel);
        }
    }
}
