﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetProgramCompetencyListQueryHandler : IRequestHandler<GetProgramCompetencyList, List<ProgramCompetencyViewModel>>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IMapper mapper;

        public GetProgramCompetencyListQueryHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork,IMapper _mapper)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            mapper = _mapper;
        }

        public Task<List<ProgramCompetencyViewModel>> Handle(GetProgramCompetencyList request, CancellationToken cancellationToken)
        {
            var programCompetency = programManagementUnitOfWork
             .ProgramManagementRepository.FindByWithInclude<ProgramCompetency>(null,
             x => x.Program, x => x.Competency, x => x.Competency).ToList();

            var competencyIds = programCompetency.Select(x => x.CompetencyId).ToArray();

            var competencyDeliverables = programManagementUnitOfWork.ProgramManagementRepository
                .FindByWithInclude<CompetencyDeliverable>(x => competencyIds.Contains(x.CompetencyId), x => x.Deliverable)
                .Select(x => new
                {
                    x.CompetencyId,
                    Delivarable = x.Deliverable.Name
                }).ToList();

            var competencyViewModel = Mapper.Map<List<ProgramCompetencyViewModel>>(programCompetency);

            competencyViewModel.ForEach(x =>
            {
                List<string> deliverables = new List<string>();

                competencyDeliverables.ForEach(comp =>
                {
                    if (x.CompetencyId == comp.CompetencyId)
                        deliverables.Add(comp.Delivarable);
                });

                x.Deliverables = deliverables;
            });

            return Task.FromResult(competencyViewModel);
        }
    }
}
