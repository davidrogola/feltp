﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetCourseScheduleQueryHandler : IRequestHandler<GetCourseScheduleList, List<CourseScheduleViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetCourseScheduleQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<CourseScheduleViewModel>> Handle(GetCourseScheduleList request, CancellationToken cancellationToken)
        {
            IEnumerable<CourseSchedule> courseSchedule = null;

            if (request.ProgramCourseId.HasValue)
            {
                courseSchedule = unitOfWork.ProgramManagementRepository.FindByWithInclude<CourseSchedule>(x=>x.ProgramCourseId==request.ProgramCourseId,   x => x.ProgramCourse.Course, 
                    x => x.ProgramOffer.Program.ProgramTier, x => x.Faculty.Person).ToList();
            }
            else
            {
                courseSchedule = unitOfWork.ProgramManagementRepository.FindByWithInclude<CourseSchedule>(null,
                x => x.ProgramCourse.Course,x => x.ProgramOffer.Program.ProgramTier,x => x.Faculty.Person).ToList();
            }

            var CourseScheduleViewModel = mapper.Map<List<CourseScheduleViewModel>>(courseSchedule);

            return Task.FromResult(CourseScheduleViewModel);
        }
    }

}