﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using ProgramManagement.Domain.Event;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetEventListQueryHandler : IRequestHandler<GetEventList, List<EventViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetEventListQueryHandler(IProgramManagementUnitOfWork _unitOfWork,
            IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }

        public Task<List<EventViewModel>> Handle(GetEventList request, CancellationToken cancellationToken)
        {

            var Events = request.Id.HasValue ? unitOfWork.ProgramManagementRepository.FindBy<Event>(x => x.Id == request.Id)
                : unitOfWork.ProgramManagementRepository.GetAll<Event>();
            var EventViewModel = mapper.Map<List<EventViewModel>>(Events);
            return Task.FromResult(EventViewModel);
        }
    }
}
