﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.QueryHandlers
{
    public class GetCompetencyListQueryHandler : IRequestHandler<GetCompetencyList, List<CompetencyViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;

        public GetCompetencyListQueryHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork)
        {
            unitOfWork = _programManagementUnitOfWork;
        }

        public Task<List<CompetencyViewModel>> Handle(GetCompetencyList request, CancellationToken cancellationToken)
        {
            var competencies = request.Id.HasValue ? unitOfWork.ProgramManagementRepository
                .FindByWithInclude<Competency>(x => x.Id == request.Id, x => x.CompetencyDeliverables)
            : unitOfWork.ProgramManagementRepository.FindByWithInclude<Competency>(null,
             x => x.CompetencyDeliverables).ToList();

            var competencyViewModel = Mapper.Map<List<CompetencyViewModel>>(competencies);

            return Task.FromResult(competencyViewModel);
        }
    }
}
