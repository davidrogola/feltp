﻿using MySql.Data.MySqlClient;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Application.ProgramManagement.Services
{
    public class ResidentCourseProgressUpdator
    {
        Func<MySqlConnection> mySqlConnection;
        public ResidentCourseProgressUpdator(Func<MySqlConnection> _mySqlConnection)
        {
            mySqlConnection = _mySqlConnection;
        }

        public void UpdateResidentCourseProgress(int semesterScheduleId, int semesterId, int offerId, ProgressStatus status)
        {
            const string query = @"update_semester_and_residentcoursework_info";
            using (var con = mySqlConnection())
            using (var command = new MySqlCommand(query, con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@semesterScheduleId", semesterScheduleId);
                command.Parameters.AddWithValue("@semesterId", semesterId);
                command.Parameters.AddWithValue("@programOfferId", offerId);
                command.Parameters.AddWithValue("@newStatus", status);


                command.Parameters["@semesterScheduleId"].Direction = ParameterDirection.Input;
                command.Parameters["@semesterId"].Direction = ParameterDirection.Input;
                command.Parameters["@programOfferId"].Direction = ParameterDirection.Input;
                command.Parameters["@newStatus"].Direction = ParameterDirection.Input;

                command.Connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        var result = reader.GetBoolean(0);
                    }

                }

            }
        }
    }
}
