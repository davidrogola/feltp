﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Services
{
    public static class GraduationServicesInstallers
    {
        public static void AddGraduandEligibiltyService(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IValidateGraduandEligibilty, GraduandEligibiltyValidator>();
        }

        public static void AddGraduandListGeneratorService(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IGenerateGraduandsList, GraduandsListGenerator>();
        }

    }

    public static class ResidentCourseProgressUpdatorInstaller
    {
        public static void AddResidentCourseProgressUpdator(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient(typeof(ResidentCourseProgressUpdator));
        }
    }
}
