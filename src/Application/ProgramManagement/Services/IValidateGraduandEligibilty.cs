﻿using Application.ProgramManagement.Commands;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.ProgramManagement.Services
{
    public interface IValidateGraduandEligibilty
    {
        void ValidateGraduandEligiblity(int programOfferId);
        void UpdateGraduandsEligiblityCriteria(UpdateGraduationRequirementCommand command);


    }


    public class GraduandEligibiltyValidator : IValidateGraduandEligibilty
    {
        IProgramManagementUnitOfWork unitOfWork;
        IResidentManagementUnitOfWork residentUnitOfWork;

        ILogger logger = Log.ForContext<GraduandsListGenerator>();
        public GraduandEligibiltyValidator(IProgramManagementUnitOfWork _unitOfWork, IResidentManagementUnitOfWork _residentUnitOfWork)
        {
            unitOfWork = _unitOfWork;
            residentUnitOfWork = _residentUnitOfWork;
        }

        public void UpdateGraduandsEligiblityCriteria(UpdateGraduationRequirementCommand command)
        {
            try
            {
                var graduationRequirement = unitOfWork.ProgramManagementRepository.Get<GraduationRequirement>(command.Id);
                if (graduationRequirement == null)
                {
                    logger.Information($"Graduation requirement details not found for Id {command.Id}");
                    return;
                }

                var orginalGraduationRequirement = graduationRequirement;

                var stateModified = graduationRequirement.UpdateRequirementDetails(command.NumberOfDidacticUnitsRequired, command.NumberOfActivitiesRequired, command.PassingGrade,command.NumberOfThesisDefenceDeliverables);

                if (!stateModified)
                    return;

                unitOfWork.ProgramManagementRepository.Update(graduationRequirement);
                unitOfWork.SaveChanges();

                var graduates = residentUnitOfWork.ResidentManagementRepository.FindBy<Graduate>(x => x.GraduationRequirementId == graduationRequirement.Id && x.CompletionStatus != CompletionStatus.Completed).ToList();

                var requiredElibiltyItems = new[]
                {
                EligibilityType.DidacticUnitsAttendance,
                EligibilityType.ThesisDefenceSuccesful,
                EligibilityType.FieldActivitiesParticipated
            };

                foreach (var graduate in graduates)
                {
                    var graduateEligbiltyItems = residentUnitOfWork.ResidentManagementRepository.FindBy<GraduateEligibilityItem>
                        (x => x.GraduateId == graduate.Id).ToList();

                    List<GraduateEligibilityItem> newEligiblityItems = new List<GraduateEligibilityItem>();
                    foreach (var requiredEligiblity in requiredElibiltyItems)
                    {
                        var eligiblityTypeExists = graduateEligbiltyItems.Any(x => x.EligibiltyType == requiredEligiblity);
                        if (!eligiblityTypeExists)
                        {
                            newEligiblityItems.Add(CreateNewEligiblityItem(requiredEligiblity, graduationRequirement, graduate.Id));

                        }
                    }

                    if (newEligiblityItems.Any())
                    {
                        residentUnitOfWork.ResidentManagementRepository.AddRange(newEligiblityItems);
                        residentUnitOfWork.SaveChanges();
                    }

                    var trackedEligiblityItemIds = new List<long>();

                    foreach (var eligbiltyItem in graduateEligbiltyItems)
                    {
                        switch (eligbiltyItem.EligibiltyType)
                        {
                            case EligibilityType.DidacticUnitsAttendance:

                                if (eligbiltyItem.ExpectedValue == orginalGraduationRequirement.NumberOfDidacticModulesRequired)
                                    continue;
                                eligbiltyItem.UpdateEligibilityExpectedValue(graduationRequirement.NumberOfDidacticModulesRequired);
                                eligbiltyItem.ReEvaluateNonPassmarkEligiblityItem();

                                break;

                            case EligibilityType.FieldActivitiesParticipated:
                                if (eligbiltyItem.ExpectedValue == orginalGraduationRequirement.NumberOfActivitiesRequired)
                                    continue;
                                eligbiltyItem.UpdateEligibilityExpectedValue(graduationRequirement.NumberOfActivitiesRequired);
                                eligbiltyItem.ReEvaluateNonPassmarkEligiblityItem();

                                break;
                            case EligibilityType.PassmarkAttained:
                                if (eligbiltyItem.ExpectedValue == orginalGraduationRequirement.PassingGrade)
                                    continue;
                                eligbiltyItem.UpdateEligibilityExpectedValue(graduationRequirement.PassingGrade);
                                eligbiltyItem.Evaluate(eligbiltyItem.ActualValue);

                                break;
                            case EligibilityType.ThesisDefenceSuccesful:
                                if (eligbiltyItem.ExpectedValue == orginalGraduationRequirement.NumberOfThesisDefenceDeliverables)
                                    continue;
                                eligbiltyItem.UpdateEligibilityExpectedValue(graduationRequirement.NumberOfThesisDefenceDeliverables);
                                eligbiltyItem.ReEvaluateNonPassmarkEligiblityItem();

                                break;
                            default:
                                break;
                        }
                        if (!trackedEligiblityItemIds.Contains(eligbiltyItem.Id))
                        residentUnitOfWork.ResidentManagementRepository.Update(eligbiltyItem);

                    }



                    residentUnitOfWork.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while updating graduation requirement details for id {command.Id}");
                throw;
            }

            
        }

        private GraduateEligibilityItem CreateNewEligiblityItem(EligibilityType eligibilityType, GraduationRequirement requirement,long graduateId)
        {
            
            GraduateEligibilityItem graduateEligibilityItem = null;

            switch (eligibilityType)
            {
                case EligibilityType.DidacticUnitsAttendance:
                    graduateEligibilityItem = new GraduateEligibilityItem(graduateId, requirement.NumberOfDidacticModulesRequired, 0, OutcomeStatus.PendingEvaluation, eligibilityType, FailureReason.None, requirement.CreatedBy, null, null);
                    break;
                case EligibilityType.FieldActivitiesParticipated:
                    graduateEligibilityItem = new GraduateEligibilityItem(graduateId, requirement.NumberOfActivitiesRequired, 0, OutcomeStatus.PendingEvaluation, eligibilityType, FailureReason.None, requirement.CreatedBy, null,null);
                    break;
                case EligibilityType.PassmarkAttained:
                    graduateEligibilityItem = new GraduateEligibilityItem(graduateId, requirement.PassingGrade, 0, OutcomeStatus.PendingEvaluation, eligibilityType, FailureReason.None, requirement.CreatedBy, null,null);
                    break;
                case EligibilityType.ThesisDefenceSuccesful:
                    graduateEligibilityItem = new GraduateEligibilityItem(graduateId, requirement.NumberOfThesisDefenceDeliverables, 0, OutcomeStatus.PendingEvaluation, eligibilityType, FailureReason.None, requirement.CreatedBy, null,null);
                    break;
                default:
                    break;
            }

            return graduateEligibilityItem;
        }

        public void ValidateGraduandEligiblity(int programOfferId)
        {
            try
            {
                var programOffer = unitOfWork.ProgramManagementRepository.Get<ProgramOffer>(programOfferId);
                if (programOffer == null)
                    return;

                var programSemesterIds = unitOfWork.ProgramManagementRepository.FindBy<ProgramCourse>(x => x.ProgramId == programOffer.ProgramId)
                    .Select(x => x.SemesterId).Distinct();

                var semesterSchedules = unitOfWork.ProgramManagementRepository.FindBy<SemesterSchedule>(x => x.ProgramOfferId == programOfferId && programSemesterIds.Contains(x.SemesterId)).ToList();

                if (semesterSchedules.Count != programSemesterIds.Count())
                {
                    logger.Information($"Semester schedules pending creation for programOfferId {programOfferId}");
                    return;
                }

                if (semesterSchedules.Any(x => x.Status != ProgressStatus.Completed))
                {
                    logger.Information($"Semester schedules pending completion for programOfferId {programOfferId}");
                    return;
                }

                var graduandIds = residentUnitOfWork.ResidentManagementRepository.FindBy<ResidentGraduationInfoView>
                    (x => x.ProgramOfferId == programOfferId && x.CompletionStatus != CompletionStatus.Completed)
                    .Select(x => x.Id)
                    .ToList();

                if (!graduandIds.Any())
                {
                    logger.Information($"Graduands not found for programofferId {programOfferId}");
                    return;
                }

                var graduandsInformation = residentUnitOfWork.ResidentManagementRepository
                    .FindByWithInclude<Graduate>(x => graduandIds.Contains(x.Id), x=> x.Resident).ToList();

                var trackedEligibiltyIds = new List<long>();

                foreach (var graduand in graduandsInformation)
                {
                    var graduandEligibityItemsPendingEvaluation = residentUnitOfWork.ResidentManagementRepository
                        .FindBy<GraduateEligibilityItem>(x => x.GraduateId == graduand.Id &&
                        x.OutcomeStatus == OutcomeStatus.PendingEvaluation && x.EligibiltyType != EligibilityType.DidacticUnitsAttendance);

                    foreach (var eligbiltyItem in graduandEligibityItemsPendingEvaluation)
                    {
                        eligbiltyItem.AppendFinalOutcome();

                        if (!trackedEligibiltyIds.Contains(eligbiltyItem.Id))
                        {
                            residentUnitOfWork.ResidentManagementRepository.Update(eligbiltyItem);
                            trackedEligibiltyIds.Add(eligbiltyItem.Id);
                        }
                    }

                }
                residentUnitOfWork.SaveChanges();

                var inEligibleOutcomeStatuses = new[] { OutcomeStatus.Failed, OutcomeStatus.PendingEvaluation };
                foreach (var graduandInfo in graduandsInformation)
                {
                    var anyFailedRequirement = residentUnitOfWork.ResidentManagementRepository.FindBy<GraduateEligibilityItem>(x => x.GraduateId == graduandInfo.Id && x.OutcomeStatus == OutcomeStatus.Failed).Any();

                    if (anyFailedRequirement)
                    {
                        graduandInfo.EvaluateGraduate(QualificationStatus.NotQualified, CompletionStatus.Pending, "System");
                        residentUnitOfWork.ResidentManagementRepository.Update(graduandInfo);
                        continue;
                    }
                    var graduationEligiblityItems = residentUnitOfWork.ResidentManagementRepository
                        .FindBy<GraduateEligibilityItem>(x => x.GraduateId == graduandInfo.Id &&
                        x.EligibiltyType != EligibilityType.DidacticUnitsAttendance).ToList();

                    var passedAllRequirements = graduationEligiblityItems.Where(x => !inEligibleOutcomeStatuses.Contains(x.OutcomeStatus))
                        .Any();

                    if (passedAllRequirements)
                    {
                        graduandInfo.EvaluateGraduate(QualificationStatus.Qualified, CompletionStatus.Completed, "System");
                        var resident = graduandInfo.Resident;
                        resident.SetAlumni();
                        residentUnitOfWork.ResidentManagementRepository.Update(resident);
                        residentUnitOfWork.ResidentManagementRepository.Update(graduandInfo);
                    }

                }

                unitOfWork.SaveChanges();
                residentUnitOfWork.SaveChanges();
                return;
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while generating graduation information for programofferId {programOfferId}");
                return;
            }
        }
    }
}
