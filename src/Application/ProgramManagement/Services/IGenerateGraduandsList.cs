﻿using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ProgramManagement.Services
{
    public interface IGenerateGraduandsList
    {
        void GenerateGraduands(int programOfferId);

        void GenerateGraduands();

    }

    public class GraduandsListGenerator : IGenerateGraduandsList
    {

        IResidentManagementUnitOfWork residentUnitOfWork;
        IValidateGraduandEligibilty graduandEligibiltyValidator;
        public GraduandsListGenerator(IResidentManagementUnitOfWork _residentUnitOfWork, IValidateGraduandEligibilty _graduandEligibiltyValidator)
        {
         
            residentUnitOfWork = _residentUnitOfWork;
            graduandEligibiltyValidator = _graduandEligibiltyValidator;
        }

        public void GenerateGraduands(int programOfferId)
        {
            graduandEligibiltyValidator.ValidateGraduandEligiblity(programOfferId);
        }


        public void GenerateGraduands()
        {
            var programOffersWithGraduandsPendingCompletion = residentUnitOfWork.ResidentManagementRepository
                .FindBy<ResidentGraduationInfoView>(x => x.CompletionStatus == CompletionStatus.Pending)
                .Select(x => x.ProgramOfferId).Distinct();

            foreach (var programOfferId in programOffersWithGraduandsPendingCompletion)
            {
                graduandEligibiltyValidator.ValidateGraduandEligiblity(programOfferId);
            }
        }
    }

   



}
