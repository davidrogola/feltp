﻿using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddCourseUnitCommand: IRequest<List<int>>
    {
        public int CourseId { get; set; }
        public List<int> UnitId { get; set; }
        public int CourseTypeId { get; set; }
        public DateTime DateCreated { get; set; }
        public int FieldPlacementActivityId { get; set; }
        public string CreatedBy { get; set; }

    }
}
