﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddProgramTierCommand : IRequest<int>
    {
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
    }

    public class UpdateProgramTierCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }

    }
    public class DeactivateProgramTierCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateDeactivatead { get; set; }
        public string DeactivatedBy { get; set; }
        public string UpdatedBy { get; set; }

    }
}
