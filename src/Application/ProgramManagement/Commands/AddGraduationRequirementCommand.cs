﻿using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddGraduationRequirementCommand : GraduationRequirementCommandDetails, IRequest<int>
    {
       
    }

    public class UpdateGraduationRequirementCommand : GraduationRequirementCommandDetails
    {

    }

    public class GraduationRequirementCommandDetails
    {
        public int Id { get; set; }
        public int ProgramId { get; set; }
        public int NumberOfDidacticUnitsRequired { get; set; }
        public int NumberOfActivitiesRequired { get; set; }
        public int NumberOfThesisDefenceDeliverables { get; set; }
        public int PassingGrade { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public SelectList ProgramSelectList { get; set; }
    }
}
