﻿using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddCourseCommand : IRequest<int>
    {
        public string Code { get; set; }
        public int CategoryId { get; set; }
        public string Name { get;  set; }
        public int CourseTypeId { get;  set; }
        public DateTime DateCreated { get;  set; }
        public string CreatedBy { get;  set; }
        public List<SelectListItem> CourseTypes { get; set; }
        public List<int> UnitId { get; set; }
        public SelectList CategorySelectList { get; set; }
    }

    public class UpdateCourseCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public int CourseTypeId { get; set; }
        public DateTime DateCreated { get; set; }
        public string UpdatedBy { get; set; }
        public List<SelectListItem> CourseTypes { get; set; }
        public List<int> UnitId { get; set; }
        public SelectList CategorySelectList { get; set; }
        public List<SelectListItem> UnitSelectList { get; set; }
    }
}
