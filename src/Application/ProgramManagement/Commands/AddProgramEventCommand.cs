﻿using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddProgramEventCommand : IRequest<int>
    {
        public int EventId { get; set; }
        public int ProgramId { get; set; }
        public DateTime  DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public SelectList ProgramSelectList { get; set; }
     }
}
