﻿using Application.ProgramManagement.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;
using ProgramManagement.Domain.Examination;

namespace Application.ProgramManagement.Commands
{
    public class AddScheduleItemTimetableCommand : IRequest<AddTimetableResponse>
    {
        public TimetableType TimetableType { get; set; }
        public int ScheduleItemId { get; set; }
        public int SemesterScheduleId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string EndDateLimit { get; set; }
        public string ScheduleItemName { get; set; }
        public string CreatedBy { get; set; }
        public List<SelectListItem> FacultySelectList { get; set; }
        public TimetableItemCommand TimetableItem { get; set; }
        public int ProgramCourseId { get; set; }
        public string FacultyName { get; set; }

    }

    public class UpdateScheduleItemTimetableCommand : IRequest<AddTimetableResponse>
    {
        public int Id { get; set; }
        public string TimetableType { get; set; }
        public string ExaminationName { get; set; }
        public string Code { get; set; }
        public int FacultyId { get; set; }
        public int? ExaminationId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Date { get; set; }
        public int? UnitId { get; set; }
        public string UnitName { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string FacultyName { get; set; }
        public int ScheduleItemId { get; set; }
        public int SemesterScheduleId { get; set; }
        public List<SelectListItem> FacultySelectList { get; set; }

    }

    public class TimetableItemCommand
    {
        public TimetableType TimetableType { get; set; }
        public string  ExaminationName { get; set; }
        public int FacultyId { get; set; }
        public int? ExaminationId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int? UnitId { get; set; }
        public string UnitName { get; set; }
        public string Date { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string FacultyName { get; set; }
        public List<SelectListItem> Units { get; set; }
        public List<SelectListItem> Exams { get; set; }
        public List<SelectListItem> Cats { get; set; }

    }

    public class AddTimetableResponse
    {
        public int ScheduleItemId { get; set; }
        public bool IsSuccessful { get; set; }
        public string Message { get; set; }
        public int SemesterScheduleId { get; set; }
    }

}
