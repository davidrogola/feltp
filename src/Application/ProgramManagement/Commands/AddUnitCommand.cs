﻿using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddUnitCommand : IRequest<int>
    {
       
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int ? FieldPlacementActivityId { get; set; }
        public List<int> FieldPlacementActivities { get; set; }
        public bool HasFieldPlacementActivity { get; set; }
        public string CreatedBy { get; set; }

    }

    public class UpdateUnitCommand : IRequest<UpdateUnitResponse>
    {
        public int Id { get; set; }
        [Required(AllowEmptyStrings =false)]
        public string Name { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Code { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Description { get; set; }
        public int [] FieldPlacementActivityId { get; set; }
        public bool HasFieldPlacementActivity { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public List<SelectListItem> FieldPlacementActivities { get; set; }
    }

    public class UpdateUnitResponse
    {
        public bool IsSuccessful { get; set; }
        public string Message { get; set; }


    }

}
