﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddCourseTypeCommand : IRequest<int>
    {
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }
       public string CreatedBy { get; set; }
    }

    public class UpdateCourseTypeCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
    }
}
