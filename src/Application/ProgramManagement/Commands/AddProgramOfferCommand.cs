﻿using Common.Domain.Person;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddProgramOfferCommand : IRequest<int>
    {
        public string Name { get; set; }
        public int ProgramId { get; set; }
        public string StrStartDate { get; set; }
        public string StrApplicationStartDate { get; set; }
        public string StrApplicationEndDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int FacultyId { get; set; }
        public int []  ProgramLocationId { get; set; }
        public SelectList ProgramSelectList { get; set; }
        public string CreatedBy { get; set; }
        public AddProgramAcademicRequirementCommand AcademicRequirementDetails { get; set; }
        public AddProgramGeneralQualificationRequirement GeneralProgramQualificationRequirements { get; set; }

    }

    public class AddProgramAcademicRequirementCommand
    {
        public EducationLevel EducationLevel { get; set; }
        public List<string> AcademicCourseToEducationLevelMapping { get; set; }
        public List<string> CourseGradingToEducationLevelMapping { get; set; }
        public int[] CompletedProgramIds { get; set; }
        public SelectList AcademicCourses { get; set; }
        public SelectList CourseGrades { get; set; }
        public SelectList PreviousProgramsCompleted { get; set; }
    }

    public class AddProgramGeneralQualificationRequirement
    {
        public int MinimumWorkExperienceDuration { get; set; }
        public bool BasicComputerKnowledge { get; set; }
        public bool InternalTravelRequired { get; set; }
        public bool ExternalTravelRequired { get; set; }
        public bool WillingnessToRelocate { get; set; }

    }

    public class UpdateProgramOfferCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ProgramId { get; set; }
        public string StrStartDate { get; set; }
        public string StrApplicationStartDate { get; set; }
        public string StrApplicationEndDate { get; set; }
        public int FacultyId { get; set; }
        public int[] ProgramLocationId { get; set; }
        public List<SelectListItem> Programs { get; set; }
        public List<SelectListItem> ProgramLocations { get; set; }
        public string CreatedBy { get; set; }
        public List<SelectListItem> Faculties { get; set; }


    }

    public class AddProgramLocationCommand : IRequest<int>
    {
        public int CountryId { get; set; }
        public int? CountyId { get; set; }
        public int? SubCountyId { get; set; }
        public string PostalAddress { get; set; }
        public string Building { get; set; }
        public string Location { get; set; }
        public string Road { get; set; }
        public string CreatedBy { get; set; }
        public List<SelectListItem> Countries { get; set; }
        public List<SelectListItem> Counties { get; set; }
        public List<SelectListItem> Subcounties { get; set; }
    }

    public class UpdateProgramLocationCommand : IRequest<int>
    {
        public int Id { get; set; }
        public int CountryId { get; set; }
        public int? CountyId { get; set; }
        public int? SubCountyId { get; set; }
        public string PostalAddress { get; set; }
        public string Building { get; set; }
        public string Location { get; set; }
        public string Road { get; set; }
        public List<SelectListItem> Countries { get; set; }
        public List<SelectListItem> Counties { get; set; }
        public List<SelectListItem> Subcounties { get; set; }
    }

    public class DeactivateProgramLocationCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string DeactivatedBy { get; set; }
    }
}
