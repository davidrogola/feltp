﻿using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class CompleteSemesterCommand : IRequest<ScheduleResponse>
    {
        public int SemesterScheduleId { get; set; }
        public string RequestedBy { get; set; }
        public DateTime DateRequested { get; set; }
        public string ScheduleName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        [Required(AllowEmptyStrings =false,ErrorMessage ="Please enter comment")]
        public string CompletionReason { get; set; }
    }
}
