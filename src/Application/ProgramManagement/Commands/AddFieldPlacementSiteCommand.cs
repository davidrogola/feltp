﻿using Application.Common.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProgramManagement.Domain.FieldPlacement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Commands
{
  public class AddFieldPlacementSiteCommand : IRequest<int>
    {
        public string Name { get; set; }
        public int CountryId { get; set; }
        public int? CountyId { get; set; }
        public int? SubCountyId { get; set; }
        public string PostalAddress { get; set; }
        public string Building { get; set; }
        public string Location { get; set; }
        public Level Level { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public List<int> Faculty { get; set; }
    }
    public class UpdateFieldPlacementSiteCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }
        public int? CountyId { get; set; }
        public int? SubCountyId { get; set; }
        public string PostalAddress { get; set; }
        public string Building { get; set; }
        public string Location { get; set; }
        public Level Level { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public int [] FacultyId { get; set; }
        public List<SelectListItem> Faculties { get; set; }
        public List<SelectListItem> Countries { get; set; }
        public List<SelectListItem> Counties { get; set; }
        public List<SelectListItem> Subcounties { get; set; }
    }
}
