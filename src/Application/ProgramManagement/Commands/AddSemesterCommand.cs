﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddSemesterCommand : IRequest<int>
    {
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
    }

    public class UpdateSemesterCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
    }

    public class DeactivateSemesterCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DateDeactivatead { get; set; }
        public string DeactivatedBy { get; set; }

    }
}

