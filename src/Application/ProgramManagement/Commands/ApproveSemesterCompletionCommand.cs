﻿using MediatR;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class ApproveSemesterCompletionCommand : IRequest<ScheduleResponse>
    {
        public int SemesterScheduleId { get; set; }
        public DateTime DateApproved { get; set; }
        public string ApprovedBy { get; set; }
        [Required(AllowEmptyStrings =false,ErrorMessage ="Please select status")]
        public ApprovalStatus Status { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter comment")]
        public string Comment { get; set; }
        public string RequestReason { get; set; }
        public string RequestedBy { get; set; }
        public string ScheduleName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}
