﻿using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddSemesterScheduleCommand : IRequest<ScheduleResponse>
    {
        public SelectList ProgramSelectList { get; set; }
        public int ProgramId { get; set; }
        public int ProgramOfferId { get; set; }
        public string Description { get; set; }
        public int SemesterId { get; set; }
        public string StrEstimatedStartDate { get; set; }
        public string StrEstimatedEndDate { get; set; }
        public DateTime EstimatedStartDate { get; set; }
        public DateTime EstimatedEndDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public ProgressStatus Status { get; set; }
    }

    public class ScheduleResponse
    {
        public string Message { get; set; }
        public bool CreatedSuccesfully { get; set; }
        public int ScheduleId { get; set; }
    }
  
}
