﻿using Application.ProgramManagement.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class EnrollResidentCommand : IRequest<ResidentEnrollmentResult>
    {
        
        public int ProgramOfferId { get; set; }
        public long ResidentId { get; set; }
        public string CreatedBy { get; set; }
        public int ProgramId { get; set; }
        public long ApplicantId { get; set; }
        public long ProgramOfferApplicationId { get; set; }

    }

    public class ResidentEnrollmentResult
    {
        public int ProgramOfferId { get; set; }
        public string Message { get; set; }
        public long ApplicantId { get; set; }
        public bool Successful { get; set; }

    }

    public class GenerateResidentCourseWorkCommand : IRequest<bool>
    {
        public int ProgramOfferId { get; set; }
        public long ResidentId { get; set; }
        public int ProgramEnrollmentId { get; set; }
    }
    public class EnrollResidentModel
    {
        public EnrollResidentCommand EnrollResidentCommand { get; set; }
        public string Name { get; set; }
        public string RegistrationNumber { get; set; }
        public ProgramOfferViewModel ProgramOfferDetail { get; set; }
    }
}
