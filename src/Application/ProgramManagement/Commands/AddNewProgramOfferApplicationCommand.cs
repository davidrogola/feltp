﻿using Application.ProgramManagement.Queries;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddNewProgramOfferApplicationCommand : IRequest<AddProgramOfferApplicationResponse>
    {
        public int ProgramId { get; set; }
        public long ResidentId { get; set; }
        public long PersonId { get; set; }
        public int ProgramOfferId { get; set; }
        public bool ApprovedByCounty { get; set; }
        public bool PersonalStatementSubmitted { get; set; }
        public long ApplicantId { get; set; }
        public string CreatedBy { get; set; }
        public SelectList ProgramSelectItem { get; set; }
        public string Message { get; set; }
        public bool IsValid { get; set; }
        public ProgramOfferViewModel ProgramOfferDetail { get; set; }

        public AddPersonalStatementFileCommand StatementFileCommand { get; set; }
    }

    public class AddPersonalStatementFileCommand : IRequest
    {
        public IFormFile StatementFile { get; set; }
        public long ProgramOfferApplicationId { get; set; }
    }

    public class AddProgramOfferApplicationResponse
    {
        public long ApplicantId { get; set; }
        public int ProgramOfferId { get; set; }
        public bool Success { get; set; }
        public long PersonId { get; set; }
        public string Message { get; set; }
        public long ProgramOfferApplicationId { get; set; }
        public long ResidentId { get; set; }

    }
}
