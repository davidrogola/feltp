﻿using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddSemesterScheduleItemCommand : IRequest<ScheduleResponse>
    {
        public ScheduleItemType ScheduleItemType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string StrStartDate { get; set; }
        public string StrEndDate { get; set; }
        public string Venue { get; set; }
        public string Name { get; set; }
        public int? ProgramCourseId { get; set; }
        public int? RevisionId { get; set; }
        public ProgressStatus Status { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public int FacultyId { get; set; }
        public int SemesterScheduleId { get; set; }
        public SelectList ProgramCourses { get; set; }
        public int Duration { get; set; }
        public int ProgramId { get; set; }
        public int SemesterId { get; set; }
        public SemesterScheduleDates SemesterSchedule { get; set; }
    }

    public class SemesterScheduleDates
    {

        public string Semester { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }

    }

}
