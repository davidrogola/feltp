﻿using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProgramManagement.Domain.FieldPlacement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddFieldPlacementActivityCommand : IRequest<int>
    {       
        public string Name { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public List<int> DeliverableId { get; set; }
        public ActivityType ActivityTpe { get; set; }
    }

    public class UpdateFieldPlacementActivityCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string UpdatedBy { get; set; }
        public int [] DeliverableId { get; set; }
        public List<SelectListItem> Deliverables { get; set; }
        public ActivityType ActivityType { get; set; }

    }
}
