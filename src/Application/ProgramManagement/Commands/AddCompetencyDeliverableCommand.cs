﻿using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddCompetencyDeliverableCommand : IRequest<List<int>>
    {
        public int CompetencyId { get; set; }
        public List<int> DeliverableId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Status { get; set; }
       
    }
}
