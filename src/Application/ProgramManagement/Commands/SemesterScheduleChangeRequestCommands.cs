﻿
using MediatR;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class InitiateSemesterScheduleChangeRequest : IRequest<SemesterScheduleChangeResponse>
    {
        public int SemesterScheduleId { get; set; }
        public int ProgramOfferId { get; set; }
        public string ProgramOffer { get; set; }
        public string Semester { get; set; }
        public DateTime DateRequested { get; set; }
        public string Comment { get; set; }
        public string RequestedBy { get; set; }
        public string ScheduleName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string OfferStartDate { get; set; }
        public string OfferEndDate { get; set; }
        public int Duration { get; set; }
        public ProgressStatus Status { get; set; }
    }

    public class ApproveSemesterChangeRequest : IRequest<SemesterScheduleChangeResponse>
    {
        public int ChangeRequestId { get; set; }
        public int SemesterScheduleId { get; set; }
        public string ScheduleName { get; set; }
        public string OriginalStartDate { get; set; }
        public string OriginalEndDate { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        [Required]
        public string Comment { get; set; }
        [Required]
        public ApprovalStatus ApprovalStatus { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovalReason { get; set; }
        public string RequestedBy { get; set; }
        
    }


    public class SemesterScheduleChangeResponse
    {
        public int SemesterScheduleId { get; set; }
        public string Message { get; set; }
    }


}