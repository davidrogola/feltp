﻿using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddFieldPlacementActivityDeliverableCommand : IRequest<List<int>>
    {
        public int Id { get; set; }
        public List<int> DeliverableId { get; set; }
        public int FieldPlacementActivityId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
    }
}
