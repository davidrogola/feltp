﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddResourceFileCommand : IRequest<int>
    {
        public int Id { get; set; }
        public int ResourceId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public IFormFile UploadedFile { get; set; }
        public byte[] FileContent { get; set; }
    }

    public class EditResourceFileCommand : IRequest<AddResourceFileResult>
    {
        public int Id { get; set; }
        public int ResourceId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public IFormFile UploadedFile { get; set; }
        public byte[] FileContent { get; set; }
    }

    public class AddResourceFileResult
    {
        public string Message { get; set; }
        public int ResourceId { get; set; }
    }
}
