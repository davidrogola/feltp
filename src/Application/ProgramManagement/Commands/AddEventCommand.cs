﻿using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;
using ProgramManagement.Domain.Event;

namespace Application.ProgramManagement.Commands
{
    public class AddEventCommand : IRequest<int>
   {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Theme { get; set; }
        public EventType EventTypeId { get; set; }
        public List<int> ProgramId { get; set; }
        public SelectList ProgramSelectList { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
    }

    public class UpdateEventCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Theme { get; set; }
        public EventType EventTypeId { get; set; }
        public string Program { get; set; }
        public List<int> ProgramId { get; set; }
        public DateTime DateCreated { get; set; }
        public string UpdatedBy { get; set; }
        public List<SelectListItem> ProgramSelectList { get; set; }
    }
}
