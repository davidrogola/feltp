﻿using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddCollaboratorCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public CollaboratorType CollaboratorTypeId { get; set; }
        public List<int> ProgramId { get; set; }
        public SelectList ProgramSelectList { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
    }

    public class UpdateCollaboratorCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public CollaboratorType CollaboratorTypeId { get; set; }
        public string Program { get; set; }
        public DateTime DateCreated { get; set; }
        public string UpdatedBy { get; set; }
        public int[] ProgramId { get; set; }
        public List<SelectListItem> Programs { get; set; }

    }
}
