﻿using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddProgramCourseCommand : IRequest<int>
    {
        public int ProgramId { get; set; }
        public int CourseTypeId { get; set; }
        public int CourseId { get; set; }
        public int ProgramTierId { get; set; }
        public int SemesterId { get; set; }
        public int CategoryId { get; set; }
        public int DurationInWeeks { get; set; }
        public DateTime  DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public SelectList ProgramSelectList { get; set; }
        public SelectList CategorySelectList { get; set; }
        public SelectList SemesterTitleSelectList { get; set; }

    }

    public class DeactivateProgramCourseCommand : IRequest<int>
    {
        public int Id { get; set; }
        public int ProgramId { get; set; }
        public int CourseTypeId { get; set; }
        public int CourseId { get; set; }
        public int ProgramTierId { get; set; }
        public int SemesterId { get; set; }
        public int CategoryId { get; set; }
        public int DurationInWeeks { get; set; }
        public DateTime DateDeactivatead { get; set; }
        public string DeactivatedBy { get; set; }
        public SelectList ProgramSelectList { get; set; }
        public SelectList CategorySelectList { get; set; }
        public SelectList SemesterTitleSelectList { get; set; }

    }
}
