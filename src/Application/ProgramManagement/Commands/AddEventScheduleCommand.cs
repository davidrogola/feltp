﻿using FacultyManagement.Domain;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProgramManagement.Domain.Event;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddEventScheduleCommand : IRequest<int>
    {
        public int ProgramEventId { get; set; }
        public int ProgramId { get; set; }
        public PresentationType PresentationType { get; set; }
        public string Location { get; set; }
        public string StrStartDate { get; set; }
        public DateTime StartDate { get; set; }
        public string StrEndDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsElapsed { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public SelectList ProgramSelectList { get; set; }
        public List<SelectListItem> ProgramEvents { get; set; }
        public int EventId { get; set; }
    }
}
