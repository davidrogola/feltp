﻿using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddCompetencyCommand : IRequest<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int DurationInWeeks { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public List<int> DeliverableId { get; set; }
    }

    public class UpdateCompetencyCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int DurationInWeeks { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public int [] DeliverableId { get; set; }
        public List<SelectListItem> Deliverables { get; set; }

    }
}
