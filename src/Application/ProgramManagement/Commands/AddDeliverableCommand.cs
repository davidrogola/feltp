﻿using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddDeliverableCommand : IRequest<int>
    {
        public string Code { get; set; }
        public string Name { get; set; }    
        public string Description { get; set; }
        public int DurationInWeeks { get; set; }
        public DateTime DateCreated { get; set; }
        public bool HasThesisDefence { get; set; }
        public string CreatedBy { get; set; }
    }

    public class UpdateDeliverableCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int DurationInWeeks { get; set; }
        public bool HasThesisDefence { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
    }


    public class DeactivateDeliverableCommand : IRequest<bool>
    {
        public int Id { get; set; }
        
        public string DeactivatedBy { get; set; }
        
    }


}
