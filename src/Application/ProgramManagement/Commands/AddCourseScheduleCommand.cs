﻿using FacultyManagement.Domain;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddCourseScheduleCommand : IRequest<int>
    {
        public string Name { get; set; }
        public int ProgramOfferId { get; set; }
        public int ProgramCourseId { get; set; }
        public int ProgramId { get; set; }
        public int ProgramTierId { get; set; }
        public string StrStartDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int FacultyId { get; set; }
        public string Location { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public SelectList ProgramSelectList { get; set; }

    }
}
