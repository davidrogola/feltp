﻿using Common.Domain.Person;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddProgramCommand : IRequest<int>
    {
        public string Name { get; set; }
        public int ProgramTierId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int DurationInMonths { get; set; }
        public bool HasMonthlySeminors { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public List<SelectListItem> ProgramTiers { get; set; }     
    }

    public class UpdateProgramCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ProgramTier { get; set; }
        public int ProgramTierId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int DurationInMonths { get; set; }
        public bool HasMonthlySeminors { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public List<SelectListItem> ProgramTiers { get; set; }
    }
}
