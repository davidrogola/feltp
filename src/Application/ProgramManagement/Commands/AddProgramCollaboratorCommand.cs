﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Commands
{
   public class AddProgramCollaboratorCommand : IRequest<List<int>>
    {
        public int CollaboratorId { get; set; }
        public List<int>ProgramId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Status { get; set; }
    }
}
