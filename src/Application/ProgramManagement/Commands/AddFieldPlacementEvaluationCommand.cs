using System;
using MediatR;

namespace Application.ProgramManagement.Commands
{
    public class AddFieldPlacementEvaluationCommand : IRequest<FieldPlacementEvaluationResponse>
    {
        public long ApplicantId { get; set; }
        public long FieldPlacementSiteId { get; set; }
        public string SiteName {get; set; }
        public DateTime DateEvaluated { get; set; }
        public FeedbackResults Feedback { get; set; }
        public int Rating { get; set; }
    }

    public class FeedbackResults
    {
        public string ChallengesEncountered { get; set; }
        public string ConduciveForLearning { get; set; }
        public string RecommendSiteToOthers { get; set; }
        public string BestExperience { get; set; }
        public string WhatToImprove { get; set; }
        
    }

    public class FieldPlacementEvaluationResponse
    {
        public string Message { get; set; }
        public bool Status { get; set; }
    }
}