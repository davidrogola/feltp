﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Application.ProgramManagement.Commands
{
    public class AddResourceCommand : IRequest<int>
    {
        public string ResourceTitle { get; set; }
        public string ResourceDescription { get; set; }
        public string Source { get; set; }
        public string SourceContactDetails { get; set; }
        public int ResourceTypeId { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public List<SelectListItem> ResourseTypes { get; set; }
        public List<int> ProgramId { get; set; }
        public SelectList ProgramSelectList { get; set; }
        public IFormFile [] UploadedFile { get; set; }
    }

    public class EditResourceCommand : IRequest<EditResourceResult>
    {
        public int Id { get; set; }
        
        public int ResourceId { get; set; }
        public string ResourceTitle { get; set; }
        public string ResourceDescription { get; set; }
        public string Source { get; set; }
        public string SourceContactDetails { get; set; }
        public int ResourceTypeId { get; set; }
        public DateTime DateCreated { get; set; }
        public string UpdatedBy { get; set; }
        public string CreatedBy { get; set; }
        public List<SelectListItem> ResourseTypes { get; set; }
        public List<int> ProgramId { get; set; }
        public SelectList ProgramSelectList { get; set; }
        public IFormFile[] UploadedFile { get; set; }
    }

    public class EditResourceResult
    {
        public string Message { get; set; }
        public int ResourceId { get; set; }
    }

}
