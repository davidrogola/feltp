﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Notification
{
    public class ResidentEnrolledNotification : INotification 
    {
        public long ResidentId { get; set; }
        public int ProgramId { get; set; }
        public DateTime DateEnrolled { get; set; }
        public int EnrollmentId { get; set; }
        public string EnrolledBy { get; set; }
        public int ProgramOfferId { get; set; }
    }

    public class ResidentEnrolledTemplateModel
    {
        public string Name { get; set; }
        public string DateEnrolled { get; set; }
        public string Program { get; set; }
        public string ProgramOffer { get; set; }
    }
}
