﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ProgramManagement.Notification
{
    public class ProgramCourseAddedNotification : INotification
    {
        public int ProgramCourseId { get; set; }


    }
}
