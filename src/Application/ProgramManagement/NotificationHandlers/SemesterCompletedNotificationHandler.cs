﻿using Application.ProgramManagement.Services;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.NotificationHandlers
{

    public class SemesterCompletedNotification : INotification
    {
        public int ProgramOfferId { get; set; }
        public int SemesterId { get; set; }

    }
    public class SemesterCompletedNotificationHandler : INotificationHandler<SemesterCompletedNotification>
    {

        IGenerateGraduandsList graduandsListGenerator;
        public SemesterCompletedNotificationHandler(IGenerateGraduandsList _graduandsListGenerator)
        {
            graduandsListGenerator = _graduandsListGenerator;
        }

        public Task Handle(SemesterCompletedNotification notification, CancellationToken cancellationToken)
        {

            graduandsListGenerator.GenerateGraduands(notification.ProgramOfferId);

            return Task.FromResult(0);
        }
    }



}
