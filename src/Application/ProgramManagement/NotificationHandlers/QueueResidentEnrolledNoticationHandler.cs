﻿using Application.Common.Notifications;
using Application.Common.Services;
using Application.ProgramManagement.Notification;
using Application.ResidentManagement.CommandHandlers;
using Common.Domain.Messaging;
using MediatR;
using Persistance.LookUpRepo.UnitOfWork;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.NotificationHandlers
{
    public class QueueResidentEnrolledNoticationHandler : INotificationHandler<ResidentEnrolledNotification>
    {
        ILookUpUnitOfWork unitOfWork;
        IProgramManagementUnitOfWork programUnitOfWork;
        IEmailTemplateBuilder emailTemplateBuilder;
        ILogger logger = Log.ForContext<ApplicantsShortListedNoticationHandler>();
        IMediator mediator;

        public QueueResidentEnrolledNoticationHandler(ILookUpUnitOfWork _unitOfWork, IProgramManagementUnitOfWork _programUnitOfWork,
        IEmailTemplateBuilder _emailTemplateBuilder, IMediator _mediator)
        {
            unitOfWork = _unitOfWork;
            programUnitOfWork = _programUnitOfWork;
            emailTemplateBuilder = _emailTemplateBuilder;
            mediator = _mediator;

        }
        public async Task Handle(ResidentEnrolledNotification notification, CancellationToken cancellationToken)
        {
            var messageTemplate = unitOfWork.LookUpRepository.FindBy<MessageTemplate>
                (x => x.MessageTypeId == MessageTypeEnum.ApplicantEnrolled).FirstOrDefault();

            if (messageTemplate == null)
            {
                logger.Error($"Message template not found for message type {MessageTypeEnum.ApplicantEnrolled.ToString()}");
                return;
            }

            var programOfferApplicant = programUnitOfWork.ProgramManagementRepository
                .FindBy<ProgramOfferApplicants>(x => x.ResidentId == notification.ResidentId && x.ProgramOfferId == notification.ProgramOfferId).FirstOrDefault();
            if (programOfferApplicant == null)
            {
                logger.Error($"Resident not found with Id {notification.ResidentId} and program offer Id {notification.ProgramOfferId}");
                return;
            }

            var body = await emailTemplateBuilder.BuildFromRazorTemplate(MessageTypeEnum.ApplicantEnrolled.ToString(), messageTemplate.Body,
                new ResidentEnrolledTemplateModel
                {
                    ProgramOffer = programOfferApplicant.ProgramOfferName,
                    Program = programOfferApplicant.Program,
                    DateEnrolled = notification.DateEnrolled.ToShortDateString(),
                    Name = $"{programOfferApplicant.FirstName} {programOfferApplicant.LastName}"
                });

            await mediator.Publish(new SendEmailNotification
            {
                Body = body,
                EmailAddresses = new []{programOfferApplicant.EmailAddress},
                Subject = messageTemplate.Subject
            });

        }
    }
}
