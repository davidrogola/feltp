﻿using Application.ProgramManagement.Notification;
using Application.ProgramManagement.Queries;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.NotificationHandlers
{
    public class ProgramCourseAddedNotificationHandler : INotificationHandler<ProgramCourseAddedNotification>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMediator mediator;
        public ProgramCourseAddedNotificationHandler(IProgramManagementUnitOfWork _unitOfWork, IMediator _mediator)
        {
            unitOfWork = _unitOfWork;
            mediator = _mediator;
        }
        public async Task Handle(ProgramCourseAddedNotification notification, CancellationToken cancellationToken)
        {

            var programCourse = unitOfWork.ProgramManagementRepository.Get<ProgramCourse>(notification.ProgramCourseId);

            if (programCourse == null)
                return;

            var courseUnits = unitOfWork.ProgramManagementRepository.FindByWithInclude<CourseUnit>(x => x.CourseId == programCourse.CourseId, x => x.Unit).ToList();

            if (!courseUnits.Any())
                return;

            int[] unitIds = courseUnits.Where(x => x.Unit.HasFieldPlacementActivity == true)
                .Select(x => x.UnitId).ToArray();

            List<int> deliverableIds = new List<int>();

            foreach (var unitId in unitIds)
            {
                var activities = await mediator.Send(new GetUnitFieldPlacementActivityWithDeliverables
                {
                    UnitId = unitId
                });
                foreach (var activity in activities.Activities)
                {
                    foreach (var deliverable in activity.Deliverables)
                    {
                        if (!deliverableIds.Contains(deliverable.Id))
                            deliverableIds.Add(deliverable.Id);
                    }
                }
            }

            var competencyIds = unitOfWork.ProgramManagementRepository.FindBy<CompetencyDeliverable>(x => deliverableIds.Contains(x.DeliverableId)).Select(x => x.CompetencyId).Distinct();

            List<ProgramCompetency> competencies = new List<ProgramCompetency>();

            foreach(var competencyId in competencyIds)
            {
                var programCompetency = new ProgramCompetency(programCourse.ProgramId, competencyId, programCourse.CreatedBy);
                competencies.Add(programCompetency);
            }

            if(competencies.Any())
            {
                unitOfWork.ProgramManagementRepository.AddRange(competencies);
                unitOfWork.SaveChanges();
            }

        }
    }
}
