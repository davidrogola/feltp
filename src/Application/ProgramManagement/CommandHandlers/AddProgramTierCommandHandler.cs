﻿using Application.ProgramManagement.Commands;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddProgramTierCommandHandler : IRequestHandler<AddProgramTierCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IMapper mapper;
        public AddProgramTierCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork, IMapper _mapper)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            mapper = _mapper;
        }
        public Task<int> Handle(AddProgramTierCommand request, CancellationToken cancellationToken)
        {
            request.DateCreated = DateTime.Now;
            request.CreatedBy = "System";

            var programTier = mapper.Map<ProgramTier>(request);

            programManagementUnitOfWork.ProgramManagementRepository.Add(programTier);
            programManagementUnitOfWork.SaveChanges();

            return Task.FromResult(programTier.Id);
        }

    }
    public class UpdateProgramTierCommandHandler : IRequestHandler<UpdateProgramTierCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IMapper mapper;
        public UpdateProgramTierCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork, IMapper _mapper)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            mapper = _mapper;
        }
        public Task<int> Handle(UpdateProgramTierCommand request, CancellationToken cancellationToken)
        {
            ProgramTier tier = mapper.Map<ProgramTier>(request);
            programManagementUnitOfWork.ProgramManagementRepository.Update(tier);
            programManagementUnitOfWork.SaveChanges();

            return Task.FromResult(tier.Id);
        }
    }

    public class DeactivateProgramTierCommandHandler : IRequestHandler<DeactivateProgramTierCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IMapper mapper;
        public DeactivateProgramTierCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork, IMapper _mapper)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            mapper = _mapper;
        }
        public Task<int> Handle(DeactivateProgramTierCommand request, CancellationToken cancellationToken)
        {
            var tier = programManagementUnitOfWork.ProgramManagementRepository.Get<ProgramTier>(request.Id);
            if (tier == null)
            {
                return Task.FromResult(0);
            }
            tier.Deactivate(request.UpdatedBy);
            programManagementUnitOfWork.ProgramManagementRepository.Update(tier);

            programManagementUnitOfWork.SaveChanges();
            return Task.FromResult(tier.Id);
        }
    }
}
