﻿using Application.MapperProfiles.ProgramManagement;
using Application.ProgramManagement.Commands;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddResourceTypeCommandHandler : IRequestHandler<AddResourceTypeCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        public AddResourceTypeCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
        }
        public Task<int> Handle(AddResourceTypeCommand request, CancellationToken cancellationToken)
        {
            request.DateCreated = DateTime.Now;
            request.CreatedBy = "System";

            var ResourceType = Mapper.Map<ResourceType>(request);

            programManagementUnitOfWork.ProgramManagementRepository.Add(ResourceType);
            programManagementUnitOfWork.SaveChanges();

            return Task.FromResult(ResourceType.Id);
        }
    }

    public class UpdateResourceTypeCommandHandler : IRequestHandler<UpdateResourceTypeCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IMapper mapper;
        public UpdateResourceTypeCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork, IMapper _mapper)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            mapper = _mapper;
        }
        public Task<int> Handle(UpdateResourceTypeCommand request, CancellationToken cancellationToken)
        {
            ResourceType role = mapper.Map<ResourceType>(request);
            programManagementUnitOfWork.ProgramManagementRepository.Update(role);
            programManagementUnitOfWork.SaveChanges();

            return Task.FromResult(role.Id);
        }
    }

    public class DeactivateResourceTypeCommandHandler : IRequestHandler<DeactivateResourceTypeCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IMapper mapper;
        public DeactivateResourceTypeCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork, IMapper _mapper)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            mapper = _mapper;
        }
        public Task<int> Handle(DeactivateResourceTypeCommand request, CancellationToken cancellationToken)
        {
            var type = programManagementUnitOfWork.ProgramManagementRepository.Get<ResourceType>(request.Id);
            if (type == null)
            {
                return Task.FromResult(0);
            }
            type.Deactivate(request.UpdatedBy);
            programManagementUnitOfWork.ProgramManagementRepository.Update(type);

            programManagementUnitOfWork.SaveChanges();
            return Task.FromResult(type.Id);
        }
    }


}

