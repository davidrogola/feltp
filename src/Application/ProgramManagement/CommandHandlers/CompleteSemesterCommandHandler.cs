﻿using Application.ProgramManagement.Commands;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class CompleteSemesterCommandHandler : IRequestHandler<CompleteSemesterCommand, ScheduleResponse>
    {
        IProgramManagementUnitOfWork unitOfWork;
        ILogger logger = Log.ForContext<CompleteSemesterCommandHandler>();
        public CompleteSemesterCommandHandler(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<ScheduleResponse> Handle(CompleteSemesterCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var semesterSchedule = unitOfWork.ProgramManagementRepository.Get<SemesterSchedule>(request.SemesterScheduleId);

                semesterSchedule.InitiateSemesterCompletionRequest(request.RequestedBy, request.CompletionReason);

                unitOfWork.ProgramManagementRepository.Update(semesterSchedule);

                unitOfWork.SaveChanges();

                return Task.FromResult(new ScheduleResponse
                {
                    Message = "Semester Completion request initiated successfully. It will be completed upon approval",
                    CreatedSuccesfully = true,
                    ScheduleId = request.SemesterScheduleId

                });
            }
            catch (Exception ex)
            {
                logger.Error(ex, "An error occured while initiating semster completion request");

                return Task.FromResult(new ScheduleResponse
                {
                    Message = "An error occured while initiating semster completion request",
                    CreatedSuccesfully = false,
                    ScheduleId = request.SemesterScheduleId

                });
            }
        }
    }
}
