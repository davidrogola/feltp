﻿using Application.ProgramManagement.Commands;
using Application.ResidentManagement.Notifications;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class ApproveSemesterCompletionCommandHandler : IRequestHandler<ApproveSemesterCompletionCommand, ScheduleResponse>
    {
        IProgramManagementUnitOfWork programUnitOfWork;
        IMediator mediator;
        public ApproveSemesterCompletionCommandHandler(IProgramManagementUnitOfWork _programUnitOfWork, IMediator _mediator)
        {
            programUnitOfWork = _programUnitOfWork;
            mediator = _mediator;
        }
        public Task<ScheduleResponse> Handle(ApproveSemesterCompletionCommand request, CancellationToken cancellationToken)
        {

            var semesterSchedule = programUnitOfWork.ProgramManagementRepository.FindBy<SemesterSchedule>(x => x.Id == request.SemesterScheduleId).SingleOrDefault();

            if (semesterSchedule == null)
                return Task.FromResult(new ScheduleResponse
                {
                    Message = "Semester schedule details not found"
                });

            semesterSchedule.ApproveSemesterCompletionRequest(request.Status, request.Comment, request.ApprovedBy);
            programUnitOfWork.ProgramManagementRepository.Update(semesterSchedule);
            programUnitOfWork.SaveChanges();


            if(request.Status == ApprovalStatus.Approved)
            mediator.Publish(new SemesterCompletionApprovedNotification
            {
                ProgramOfferId = semesterSchedule.ProgramOfferId,
                SemesterId = semesterSchedule.SemesterId,
                SemesterScheduleId = semesterSchedule.Id
            });

            return Task.FromResult(new ScheduleResponse
            {
                CreatedSuccesfully = true,
                Message = "Semester schedule completion request updated successfully",
                ScheduleId = request.SemesterScheduleId
            });

        }
    }
}
