﻿using Application.Common;
using Application.ProgramManagement.Commands;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddDeliverableCommandHandler : IRequestHandler<AddDeliverableCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IMapper mapper;
        ICodeGenerator codeGenerator;
        public AddDeliverableCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork,
            IMapper _mapper, ICodeGenerator _codeGenerator)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            codeGenerator = _codeGenerator;
            mapper = _mapper;
        }
        public Task<int> Handle(AddDeliverableCommand request, CancellationToken cancellationToken)
        {
            var deliverableCode = codeGenerator.Generate("deliverablecode");

            var deliverable = mapper.Map<Deliverable>(request);
            deliverable.SetAuditInfomation(request.CreatedBy);
            deliverable.SetCode(deliverableCode);

            programManagementUnitOfWork.ProgramManagementRepository.Add(deliverable);
            programManagementUnitOfWork.SaveChanges();

            return Task.FromResult(deliverable.Id);
        }
    }

    public class UpdateDeliverableCommandHandler : IRequestHandler<UpdateDeliverableCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IMapper mapper;
        public UpdateDeliverableCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork, IMapper _mapper)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            mapper = _mapper;
        }
        public Task<int> Handle(UpdateDeliverableCommand request, CancellationToken cancellationToken)
        {
            var deliverable = programManagementUnitOfWork.ProgramManagementRepository.Get<Deliverable>(request.Id);

            deliverable.Update(request.Code, request.Name, request.Description, request.DurationInWeeks, request.HasThesisDefence);

            programManagementUnitOfWork.ProgramManagementRepository.Update(deliverable);
            programManagementUnitOfWork.SaveChanges();

            return Task.FromResult(deliverable.Id);
        }
    }

    public class DeactivateDeliverableCommandHandler : IRequestHandler<DeactivateDeliverableCommand, bool>
    {
        private IProgramManagementUnitOfWork _unitOfWork;

        public DeactivateDeliverableCommandHandler(IProgramManagementUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public Task<bool> Handle(DeactivateDeliverableCommand request, CancellationToken cancellationToken)
        {
            var deliverable = _unitOfWork.ProgramManagementRepository
                .FindBy<Deliverable>(x => x.Id == request.Id).SingleOrDefault();
            if (deliverable == null)
            {
                return Task.FromResult(false);
            }
            deliverable.Deactivate(request.DeactivatedBy);
            _unitOfWork.ProgramManagementRepository.Update(deliverable);
            _unitOfWork.SaveChanges();
            return Task.FromResult(true);
        }
    }
}
