﻿using Application.MapperProfiles.ProgramManagement;
using Application.ProgramManagement.Commands;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddCompetencyDeliverableCommandHandler : IRequestHandler<AddCompetencyDeliverableCommand, List<int>>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        public AddCompetencyDeliverableCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
        }
        public Task<List<int>> Handle(AddCompetencyDeliverableCommand request, CancellationToken cancellationToken)
        {
            var competencyDeliverableList = new List<CompetencyDeliverable>();

            foreach (var deliverableId in request.DeliverableId)
            {
                var competencyDeliverable = new CompetencyDeliverable(request.CompetencyId, deliverableId, "System");
                competencyDeliverableList.Add(competencyDeliverable);
            }

            if(competencyDeliverableList.Any())
            {
                programManagementUnitOfWork.ProgramManagementRepository.AddRange(competencyDeliverableList);
                programManagementUnitOfWork.SaveChanges();
            }

            return Task.FromResult(competencyDeliverableList.Select(x=>x.Id).ToList());
        }
    }

}

