﻿using Application.Common.Services;
using Application.ProgramManagement.Commands;
using MediatR;
using Microsoft.Extensions.Configuration;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using Serilog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddSemesterScheduleItemCommandHandler : IRequestHandler<AddSemesterScheduleItemCommand, ScheduleResponse>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IConfiguration config;
        ILogger logger = Log.ForContext<AddSemesterScheduleItemCommandHandler>();

        public AddSemesterScheduleItemCommandHandler(IProgramManagementUnitOfWork _unitOfWork, IConfiguration _config)
        {
            unitOfWork = _unitOfWork;
            config = _config;
        }
        public Task<ScheduleResponse> Handle(AddSemesterScheduleItemCommand request, CancellationToken cancellationToken)
        {
            try
            {

                request.StartDate = request.StrStartDate.ToLocalDate();
                request.EndDate = request.StrEndDate.ToLocalDate();

                request.Status = request.StartDate.Date <= DateTime.Now.Date ? ProgressStatus.InProgress : ProgressStatus.NotStarted;
                

                if (request.ScheduleItemType == ScheduleItemType.Examination 
                    || request.ScheduleItemType == ScheduleItemType.Cat)
                {
                    var semester = unitOfWork.ProgramManagementRepository.FindBy<Semester>(x => x.Id == request.SemesterId)
                        .SingleOrDefault();
                    if (semester == null)
                        return Task.FromResult(new ScheduleResponse{Message = "Semester details not found"});    
                    request.Name = request.ScheduleItemType == ScheduleItemType.Examination ?
                        $"{semester.Name} Examination Schedule" :  $"{semester.Name} Cat Schedule";
                }
                else
                {
                    var programCourse = unitOfWork.ProgramManagementRepository
                        .FindByWithInclude<ProgramCourse>(x => x.Id == request.ProgramCourseId.Value, x => x.Course.Category.CourseType).SingleOrDefault();

                    if(programCourse == null)
                    {
                        logger.Information($"Program course not found during creation of {request.ScheduleItemType.ToString() } schedule for SemScheduleId {request.SemesterScheduleId}");

                        return Task.FromResult(new ScheduleResponse
                        {
                            Message = $"An error occured while creating {request.ScheduleItemType.ToString()} schedule"

                        });
                    }
                    request.Name = $"{ programCourse.Course.Name } ({programCourse.Course.Category.CourseType.Name} Schedule)";

                }        

                var semesterScheduleItem = new SemesterScheduleItem(request.ScheduleItemType, request.StartDate, 
                    request.EndDate, request.Venue,request.Name, request.ProgramCourseId, request.RevisionId, 
                    request.Status, request.CreatedBy, request.SemesterScheduleId);


                unitOfWork.ProgramManagementRepository.Add(semesterScheduleItem);
                unitOfWork.SaveChanges();

                return Task.FromResult(new ScheduleResponse
                {
                    CreatedSuccesfully = true,
                    Message = $"{request.ScheduleItemType.ToString()} schedule created successfully",
                    ScheduleId = request.SemesterScheduleId
                });
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while creating {request.ScheduleItemType.ToString()} for" +
                    $" SemesterScheduleId {request.SemesterScheduleId }");

                return Task.FromResult(new ScheduleResponse
                {
                    Message = $"An error occured while creating {request.ScheduleItemType.ToString()} schedule"
                  
                });
            }
        }

    }
}
