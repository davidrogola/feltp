﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;

using Application.ProgramManagement.Commands;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.FieldPlacement;
using System.Collections.Generic;
using System.Linq;

namespace Application.ProgramManagement.CommandHandlers
{
    public class FieldPlacementActivityDeliverableCommandHandler : IRequestHandler<AddFieldPlacementActivityDeliverableCommand, List<int>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public FieldPlacementActivityDeliverableCommandHandler(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }

        public Task<List<int>> Handle(AddFieldPlacementActivityDeliverableCommand request, CancellationToken cancellationToken)
        {
            var fieldPlacementActivityDeliverableList = new List<FieldPlacementActivityDeliverable>();

            foreach(var deliverable in request.DeliverableId)
            {
                var placementActivityDeliverable = new FieldPlacementActivityDeliverable(deliverable, request.FieldPlacementActivityId, "System");

                fieldPlacementActivityDeliverableList.Add(placementActivityDeliverable);
            }
            
            unitOfWork.ProgramManagementRepository.AddRange(fieldPlacementActivityDeliverableList);
            unitOfWork.SaveChanges();

            return Task.FromResult(fieldPlacementActivityDeliverableList.Select(x=>x.Id).ToList());
        }
    }
}
