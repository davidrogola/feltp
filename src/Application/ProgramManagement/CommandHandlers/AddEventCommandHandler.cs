﻿using Application.MapperProfiles.ProgramManagement;
using Application.ProgramManagement.Commands;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Event;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddEventCommandHandler : IRequestHandler<AddEventCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        public AddEventCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
        }
        public Task<int> Handle(AddEventCommand request, CancellationToken cancellationToken)
        {
            request.DateCreated = DateTime.Now;
            var Event = Mapper.Map<Event>(request);
            programManagementUnitOfWork.ProgramManagementRepository.Add(Event);

            var selectPrograms = Event.SelectPrograms(request.ProgramId.ToArray(), request.CreatedBy);
            programManagementUnitOfWork.ProgramManagementRepository.AddRange(selectPrograms);

            programManagementUnitOfWork.SaveChanges();

            return Task.FromResult(Event.Id);
        }
    }

    public class UpdateEventCommandHandler : IRequestHandler<UpdateEventCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IMapper mapper;
        public UpdateEventCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork, IMapper _mapper)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            mapper = _mapper;
        }
        public Task<int> Handle(UpdateEventCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var eventDetail = programManagementUnitOfWork.ProgramManagementRepository.Get<Event>(request.Id);

                eventDetail.UpdateEvent(request.Title, request.Theme, request.EventTypeId);
                programManagementUnitOfWork.ProgramManagementRepository.Update(eventDetail);

                var existingProgramEvents = programManagementUnitOfWork.ProgramManagementRepository.FindBy<ProgramEvent>(x => x.EventId == request.Id)
                    .Select(x => x.ProgramId).ToList();

                var deactivatedProgramEventIds = existingProgramEvents.Except(request.ProgramId);

                var newProgramEventIds = request.ProgramId.Except(existingProgramEvents);

                if (deactivatedProgramEventIds != null)
                {
                    var deactivatedProgramEvents = programManagementUnitOfWork.ProgramManagementRepository.FindBy<ProgramEvent>(x => deactivatedProgramEventIds.Contains(x.ProgramId) && x.EventId == request.Id).ToList();

                    foreach (var deactivatedEvent in deactivatedProgramEvents)
                    {
                        deactivatedEvent.Deactivate(request.UpdatedBy);
                        programManagementUnitOfWork.ProgramManagementRepository.Update(deactivatedEvent);
                    }
                }

                var newProgramEvents = new List<ProgramEvent>();

                if (newProgramEventIds.Any())
                {
                    foreach (var newEventId in newProgramEventIds)
                    {
                        var programEvent = new ProgramEvent(request.Id, newEventId, request.UpdatedBy);
                        newProgramEvents.Add(programEvent);
                    }

                    programManagementUnitOfWork.ProgramManagementRepository.AddRange(newProgramEvents);
                }



                programManagementUnitOfWork.SaveChanges();

                return Task.FromResult(eventDetail.Id);
            }
            catch (Exception ex)
            {

                return Task.FromResult(default(int));
            }
        }
    }
}

