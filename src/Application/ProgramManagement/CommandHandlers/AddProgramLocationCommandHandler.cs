﻿using Application.ProgramManagement.Commands;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddProgramLocationCommandHandler : IRequestHandler<AddProgramLocationCommand, int>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public AddProgramLocationCommandHandler(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }

        public Task<int> Handle(AddProgramLocationCommand request, CancellationToken cancellationToken)
        {
            var countyId = (request.CountyId.HasValue && request.CountyId.Value != 0) ? request.CountyId : null;
            var subcountyId = (request.SubCountyId.HasValue && request.SubCountyId.Value != 0) ? request.CountyId : null;

            var programLocation = new ProgramLocation(request.CountryId, countyId, subcountyId, request.PostalAddress, request.Location, request.Location, request.Road, "System");

            unitOfWork.ProgramManagementRepository.Add(programLocation);
            unitOfWork.SaveChanges();

            return Task.FromResult(programLocation.Id);
        }
    }
    public class UpdateProgramLocationCommandHandler : IRequestHandler<UpdateProgramLocationCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IMapper mapper;
        public UpdateProgramLocationCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork, IMapper _mapper)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            mapper = _mapper;
        }
        public Task<int> Handle(UpdateProgramLocationCommand request, CancellationToken cancellationToken)
        {

            var programLocation = programManagementUnitOfWork.ProgramManagementRepository.Get<ProgramLocation>(request.Id);
            programLocation.UpdateLocation(request.CountryId, request.CountyId, request.SubCountyId, request.PostalAddress, request.Building,
                request.Location, request.Road);

            programManagementUnitOfWork.ProgramManagementRepository.Update(programLocation);
            programManagementUnitOfWork.SaveChanges();

            return Task.FromResult(programLocation.Id);

        }
    }

    public class DeactivateProgramLocationCommandHandler : IRequestHandler<DeactivateProgramLocationCommand, int>
    {
        private IProgramManagementUnitOfWork _unitOfWork;

        public DeactivateProgramLocationCommandHandler(IProgramManagementUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public Task<int> Handle(DeactivateProgramLocationCommand request, CancellationToken cancellationToken)
        {
            Console.WriteLine(JsonConvert.SerializeObject(request));
            var location = _unitOfWork.ProgramManagementRepository.FindBy<ProgramLocation>(x => x.Id == request.Id)
                .SingleOrDefault();
            if (location == null)
            {
                Console.WriteLine("Not Found");
                return Task.FromResult(0);
            }
            
            location.Deactivate(request.DeactivatedBy);
            
            _unitOfWork.ProgramManagementRepository.Update(location);
            _unitOfWork.SaveChanges();
            
            return Task.FromResult(location.Id);
        }
    }


}
