﻿using Application.Common.Services;
using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Services;
using MediatR;
using Newtonsoft.Json;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class InitiateSemesterScheduleChangeRequestHandler : IRequestHandler<InitiateSemesterScheduleChangeRequest, SemesterScheduleChangeResponse>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public InitiateSemesterScheduleChangeRequestHandler(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<SemesterScheduleChangeResponse> Handle(InitiateSemesterScheduleChangeRequest request, CancellationToken cancellationToken)
        {
            var startDate = request.StartDate.ToLocalDate();


            var originalSemesterDates = unitOfWork.ProgramManagementRepository.FindBy<SemesterSchedule>(x => x.Id == request.SemesterScheduleId)
                .Select(x => new
                {
                    StartDate = x.EstimatedStartDate.ToShortDateString(),
                    EndDate = x.EstimatedEndDate.ToShortDateString()
                }).SingleOrDefault();

            var endDate = String.Equals(request.EndDate, originalSemesterDates.EndDate) ? 
                startDate.CalculateEndDate(request.Duration) : request.EndDate.ToLocalDate();

            var semesterChangeRequest = new SemesterScheduleChangeRequest(request.SemesterScheduleId, request.RequestedBy, startDate,
                endDate, request.Comment, JsonConvert.SerializeObject(originalSemesterDates));

            unitOfWork.ProgramManagementRepository.Add(semesterChangeRequest);
            unitOfWork.SaveChanges();

            return Task.FromResult(new SemesterScheduleChangeResponse
            {
                SemesterScheduleId = request.SemesterScheduleId,
                Message = "Semester schedule change request created successfully. It will be action upon approval"
            });
        }
    }

    public class ApproveSemesterChangeRequestHandler : IRequestHandler<ApproveSemesterChangeRequest, SemesterScheduleChangeResponse>
    {
        IProgramManagementUnitOfWork unitOfWork;
        ResidentCourseProgressUpdator residentCourseProgressUpdator;
        public ApproveSemesterChangeRequestHandler(IProgramManagementUnitOfWork _unitOfWork, 
            ResidentCourseProgressUpdator _residentCourseProgressUpdator)
        {
            unitOfWork = _unitOfWork;
            residentCourseProgressUpdator = _residentCourseProgressUpdator;
        }
        public Task<SemesterScheduleChangeResponse> Handle(ApproveSemesterChangeRequest request, CancellationToken cancellationToken)
        {
            var changeRequest = unitOfWork.ProgramManagementRepository.Get<SemesterScheduleChangeRequest>(request.ChangeRequestId);

            changeRequest.ActionRequest(request.ApprovalStatus, request.ApprovedBy, request.ApprovalReason);
            unitOfWork.ProgramManagementRepository.Update(changeRequest);

            if (request.ApprovalStatus == ApprovalStatus.Approved)
            {
                var semesterSchedule = unitOfWork.ProgramManagementRepository.Get<SemesterSchedule>(request.SemesterScheduleId);
                if (semesterSchedule == null)
                {
                    return Task.FromResult(new SemesterScheduleChangeResponse
                    {
                        SemesterScheduleId = request.SemesterScheduleId,
                        Message = "Semester schedule details not found"
                    });
                }

                semesterSchedule.AdjustSemesterDate(changeRequest.StartDate, changeRequest.EndDate);
                unitOfWork.ProgramManagementRepository.Update(semesterSchedule);

                if (DateTime.Now.Date >= semesterSchedule.ActualStartDate.Value.Date && DateTime.Now.Date <= semesterSchedule.ActualEndDate.Value.Date)
                {
                    residentCourseProgressUpdator.UpdateResidentCourseProgress(semesterSchedule.Id, semesterSchedule.SemesterId, semesterSchedule.ProgramOfferId, ProgressStatus.InProgress);
                }
                else if(DateTime.Now.Date > semesterSchedule.ActualEndDate.Value.Date)
                {
                    residentCourseProgressUpdator.UpdateResidentCourseProgress(semesterSchedule.Id, semesterSchedule.SemesterId, semesterSchedule.ProgramOfferId, ProgressStatus.Completed);
                }
                else
                {
                    residentCourseProgressUpdator.UpdateResidentCourseProgress(semesterSchedule.Id, semesterSchedule.SemesterId, semesterSchedule.ProgramOfferId, ProgressStatus.NotStarted);
                }

            }
            unitOfWork.SaveChanges();

            return Task.FromResult(new SemesterScheduleChangeResponse
            {
                SemesterScheduleId = request.SemesterScheduleId,
                Message = "Semester schedule details updated successfully"
            });
        }
    }
}
