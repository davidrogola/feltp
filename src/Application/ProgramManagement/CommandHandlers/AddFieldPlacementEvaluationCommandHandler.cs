using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.ProgramManagement.Commands;
using MediatR;
using Newtonsoft.Json;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.FieldPlacement;
using ResidentManagement.Domain;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddFieldPlacementEvaluationCommandHandler : IRequestHandler<AddFieldPlacementEvaluationCommand,FieldPlacementEvaluationResponse>
    {
        private readonly IProgramManagementUnitOfWork _programManagementUnitOfWork;
        private readonly IResidentManagementUnitOfWork _residentManagementUnitOfWork;

        public AddFieldPlacementEvaluationCommandHandler(IProgramManagementUnitOfWork programManagementUnitOfWork,
            IResidentManagementUnitOfWork residentManagementUnitOfWork)
        {
            _programManagementUnitOfWork = programManagementUnitOfWork;
            _residentManagementUnitOfWork = residentManagementUnitOfWork;
        }

        public Task<FieldPlacementEvaluationResponse> Handle(AddFieldPlacementEvaluationCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var residentId = _residentManagementUnitOfWork.ResidentManagementRepository
                    .FindBy<Resident>(x => x.ApplicantId == request.ApplicantId).SingleOrDefault()?.Id;
                if(!residentId.HasValue)
                  return Task.FromResult(new FieldPlacementEvaluationResponse{Message = "Resident information not found"});
               
                var placementEvaluation = new FieldPlacementSiteEvaluation(residentId.Value, request.FieldPlacementSiteId,
                    JsonConvert.SerializeObject(request.Feedback), request.Rating);
           
                _programManagementUnitOfWork.ProgramManagementRepository.Add(placementEvaluation);
                _programManagementUnitOfWork.SaveChanges();

                return Task.FromResult(new FieldPlacementEvaluationResponse
                {
                    Status = true,
                    Message = "Field Placement site evaluated successfully"
                 
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return Task.FromResult(new FieldPlacementEvaluationResponse()
                {
                    Message = "An error occured while reviewing field placement site"                              
                });
            }
        }
    }
}