﻿using Application.ProgramManagement.Commands;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Globalization;
using Application.Common.Services;
using System.Linq;
using Serilog.Core;
using Serilog;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddProgramOfferCommandHandler : IRequestHandler<AddProgramOfferCommand, int>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;

        public AddProgramOfferCommandHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;

        }
        public Task<int> Handle(AddProgramOfferCommand request, CancellationToken cancellationToken)
        {
            using (var transaction = unitOfWork.BeginTransaction(IsolationLevel.ReadCommitted))
            {

                var program = unitOfWork.ProgramManagementRepository.Get<Program>(request.ProgramId);
                if (program == null)
                    return Task.FromResult(0);


                request.StartDate = request.StrStartDate.ToLocalDate();

                var programOffer = new ProgramOffer(request.ProgramId, request.StartDate,
                     request.FacultyId, request.Name, request.CreatedBy, request.StrApplicationStartDate.ToLocalDate(), request.StrApplicationEndDate.ToLocalDate());

                programOffer.SetEndDate(program.Duration);

                unitOfWork.ProgramManagementRepository.Add(programOffer);

                List<ProgramOfferLocation> offerLocations = new List<ProgramOfferLocation>();

                foreach (var location in request.ProgramLocationId)
                {
                    var offerLocation = new ProgramOfferLocation(programOffer.Id, location, programOffer.CreatedBy);
                    offerLocations.Add(offerLocation);
                }

                unitOfWork.ProgramManagementRepository.AddRange(offerLocations);

                var programOfferGeneralRequirements = mapper.Map<ProgramOfferGeneralRequirement>(request.GeneralProgramQualificationRequirements);
                programOfferGeneralRequirements.SetAuditInfo(request.CreatedBy);
                programOfferGeneralRequirements.SetProgramOffer(programOffer.Id);
                unitOfWork.ProgramManagementRepository.Add(programOfferGeneralRequirements);

                var programOfferAcademicRequirements = BuildAcademicRequirementDbModel(programOffer.Id, request.CreatedBy, request.AcademicRequirementDetails);
                unitOfWork.ProgramManagementRepository.AddRange(programOfferAcademicRequirements);

                var programCompletionRequirement = BuildPreviousProgramCompletionModel(programOffer.Id, request.AcademicRequirementDetails.CompletedProgramIds, request.CreatedBy);
                unitOfWork.ProgramManagementRepository.AddRange(programCompletionRequirement);

                unitOfWork.SaveChanges();

                transaction.Commit();
                return Task.FromResult(programOffer.Id);
            }

        }


        private IEnumerable<ProgramOfferAcademicRequirement> BuildAcademicRequirementDbModel(int programOfferId, string createdBy,
            AddProgramAcademicRequirementCommand command)
        {
            var programAcademicRequirements = new List<ProgramOfferAcademicRequirement>();

            if (command.AcademicCourseToEducationLevelMapping == null || !command.AcademicCourseToEducationLevelMapping.Any())
                return programAcademicRequirements;

            foreach (var courseMapping in command.AcademicCourseToEducationLevelMapping)
            {
                var academicCourseId = Convert.ToInt16(courseMapping.Split(":")[1]);

                var courseGradeId = GetSelectedAcademicCourseGradeValue(command.CourseGradingToEducationLevelMapping,
                    courseMapping[0].ToString());
                if(courseGradeId != null)
                programAcademicRequirements.Add(new ProgramOfferAcademicRequirement(academicCourseId, programOfferId, courseGradeId, createdBy));
            }

            return programAcademicRequirements;
        }

        private IEnumerable<ProgramCompletionRequirement> BuildPreviousProgramCompletionModel(int programOfferId, 
            int[] requiredCompletedPrograms, string createdBy)
        {
            var completedProgramRequirements = new List<ProgramCompletionRequirement>();
            if (requiredCompletedPrograms == null)
                return completedProgramRequirements;
            if (!requiredCompletedPrograms.Any())
                return completedProgramRequirements;

            foreach (var completedProgramId in requiredCompletedPrograms)
            {
                completedProgramRequirements.Add(new ProgramCompletionRequirement(programOfferId, completedProgramId, createdBy));
            }
            return completedProgramRequirements;
        }

        private int? GetSelectedAcademicCourseGradeValue(List<string> academicGrades, string educationLevel)
        {
            if (academicGrades == null)
                return null;
            if (!academicGrades.Any())
                return null;

            var gradeToEducationLevelMapping = academicGrades.FirstOrDefault(x => x.Contains(educationLevel.ToString()));

            if (gradeToEducationLevelMapping == null)
                return null;

            var academicGradeId = Convert.ToInt16(gradeToEducationLevelMapping.Split(":")[1]);

            return academicGradeId;
        }

    }


    public class UpdateProgramOfferCommandHandler : IRequestHandler<UpdateProgramOfferCommand, int>
    {
        IProgramManagementUnitOfWork unitOfWork;
        ILogger logger = Log.ForContext<UpdateProgramOfferCommandHandler>();
        public UpdateProgramOfferCommandHandler(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<int> Handle(UpdateProgramOfferCommand request, CancellationToken cancellationToken)
        {
            using (var transaction = unitOfWork.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    var programOffer = unitOfWork.ProgramManagementRepository.FindByWithInclude<ProgramOffer>
                             (x => x.Id == request.Id, x => x.Program).SingleOrDefault();
                    if (programOffer == null)
                        return Task.FromResult(0);

                    programOffer.UpdateProgramOffer(request.ProgramId, request.StrStartDate.ToLocalDate(), request.FacultyId, request.Name,
                        request.StrApplicationStartDate.ToLocalDate(), request.StrApplicationEndDate.ToLocalDate());

                    programOffer.SetEndDate(programOffer.Program.Duration);

                    unitOfWork.ProgramManagementRepository.Update(programOffer);

                    if (request.ProgramLocationId != null)
                    {
                        var existingLocationIds = unitOfWork.ProgramManagementRepository.FindBy<ProgramOfferLocation>
                            (x => x.ProgramOfferId == programOffer.Id && x.DateDeactivated == null)
                            .Select(x => x.ProgramLocationId).Distinct().ToArray();

                        var addedLocations = request.ProgramLocationId.Except(existingLocationIds);

                        var removedLocations = existingLocationIds.Except(request.ProgramLocationId);

                        if (addedLocations.Any())
                        {
                            var programOfferLocations = new List<ProgramOfferLocation>();

                            foreach (var programLocationId in addedLocations)
                            {
                                var offerLocation = new ProgramOfferLocation(request.Id, programLocationId, request.CreatedBy);
                                programOfferLocations.Add(offerLocation);
                            }

                            unitOfWork.ProgramManagementRepository.AddRange(programOfferLocations);
                        }
                        if (removedLocations.Any())
                        {
                            List<int> trackedProgramLocationIds = new List<int>();
                            foreach (var programLocationId in removedLocations)
                            {
                                var programOfferLocation = unitOfWork.ProgramManagementRepository.FindBy<ProgramOfferLocation>
                                    (x => x.ProgramLocationId == programLocationId && x.ProgramOfferId == programOffer.Id).SingleOrDefault();

                                programOfferLocation.Deactivate(request.CreatedBy);

                                if (!trackedProgramLocationIds.Contains(programOfferLocation.Id))
                                {
                                    unitOfWork.ProgramManagementRepository.Update(programOfferLocation);
                                    trackedProgramLocationIds.Add(programOfferLocation.Id);

                                }
                            }
                        }
                    }

                    unitOfWork.SaveChanges();
                    transaction.Commit();
                    return Task.FromResult(programOffer.Id);
                }
                catch (Exception ex)
                {
                    logger.Error(ex, $"An error occurred while updating program offer details with Id: {request.Id }");
                    throw;
                }
            }


        }
    }

}
