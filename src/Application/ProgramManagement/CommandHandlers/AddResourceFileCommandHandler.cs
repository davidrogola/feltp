﻿using Application.ProgramManagement.Commands;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddResourceFileCommandHandler : IRequestHandler<AddResourceFileCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IMapper mapper;
        public AddResourceFileCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork, IMapper _mapper)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            mapper = _mapper;
        }
        public Task<int> Handle(AddResourceFileCommand request, CancellationToken cancellationToken)
        {
            request.DateCreated = DateTime.Now;
            var resourceFile = mapper.Map<ResourceFile>(request);
            programManagementUnitOfWork.ProgramManagementRepository.Add(resourceFile);

            programManagementUnitOfWork.SaveChanges();

            return Task.FromResult(resourceFile.Id);
        }
    }
}
