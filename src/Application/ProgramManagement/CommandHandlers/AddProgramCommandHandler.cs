﻿using Application.Common;
using Application.ProgramManagement.Commands;
using AutoMapper;
using Common.Domain.Person;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddProgramCommandHandler : IRequestHandler<AddProgramCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IMapper mapper;
        ICodeGenerator codeGenerator;
        public AddProgramCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork, IMapper _mapper,
            ICodeGenerator _codeGenerator)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            codeGenerator = _codeGenerator;
            mapper = _mapper;
        }

        public Task<int> Handle(AddProgramCommand request, CancellationToken cancellationToken)
        {

            var program = mapper.Map<Program>(request);
            program.SetAuditInfomation(request.CreatedBy);

            programManagementUnitOfWork.ProgramManagementRepository.Add(program);
            programManagementUnitOfWork.SaveChanges();

            return Task.FromResult(program.Id);
        }

        public class UpdateProgramCommandHandler : IRequestHandler<UpdateProgramCommand, int>
        {
            IProgramManagementUnitOfWork programManagementUnitOfWork;
            IMapper mapper;
            public UpdateProgramCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork, IMapper _mapper)
            {
                programManagementUnitOfWork = _programManagementUnitOfWork;
                mapper = _mapper;
            }
            public Task<int> Handle(UpdateProgramCommand request, CancellationToken cancellationToken)
            {

                var program = programManagementUnitOfWork.ProgramManagementRepository.Get<Program>(request.Id);
                program.Update(request.Name, request.Code, request.Description, request.ProgramTierId, request.HasMonthlySeminors);

                programManagementUnitOfWork.ProgramManagementRepository.Update(program);
                programManagementUnitOfWork.SaveChanges();

                return Task.FromResult(program.Id);

            }
        }
    }
}
