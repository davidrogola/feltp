﻿using Application.Common;
using Application.ProgramManagement.Commands;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddCompetencyCommandHandler : IRequestHandler<AddCompetencyCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IMapper mapper;
        ICodeGenerator codeGenerator;
        public AddCompetencyCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork,
            IMapper _mapper, ICodeGenerator _codeGenerator)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            codeGenerator = _codeGenerator;
            mapper = _mapper;
        }
        public Task<int> Handle(AddCompetencyCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var competencyCode = codeGenerator.Generate("competencycode");

                var deliverableDurations = programManagementUnitOfWork.ProgramManagementRepository
                    .FindBy<Deliverable>(x => request.DeliverableId.Contains(x.Id)).Select(x => x.DurationInWeeks).Sum();

                var competency = mapper.Map<Competency>(request);

                competency.SetCode(competencyCode);
                competency.SetAuditInfomation(request.CreatedBy);
                competency.SetDuration(deliverableDurations);

                programManagementUnitOfWork.ProgramManagementRepository.Add(competency);

                var competencyDeliverables = competency.AddDeliverables(request.DeliverableId.ToArray(), request.CreatedBy);
                programManagementUnitOfWork.ProgramManagementRepository.AddRange(competencyDeliverables);

                programManagementUnitOfWork.SaveChanges();

                return Task.FromResult(competency.Id);
            }
            catch (Exception ex)
            {
                return Task.FromResult(default(int));
            }
        }
    }

    public class UpdateComptencyCommandHandler : IRequestHandler<UpdateCompetencyCommand, int>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMediator mediator;
        ILogger logger = Log.ForContext<UpdateComptencyCommandHandler>();

        public UpdateComptencyCommandHandler(IProgramManagementUnitOfWork _unitOfWork, IMediator _mediator)
        {
            unitOfWork = _unitOfWork;
            mediator = _mediator;

        }
        public Task<int> Handle(UpdateCompetencyCommand request, CancellationToken cancellationToken)
        {
            using (var transaction = unitOfWork.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {

                    var competency = unitOfWork.ProgramManagementRepository.Get<Competency>(request.Id);

                    competency.UpdateCompetency(request.Name, request.Description);
                    unitOfWork.ProgramManagementRepository.Update(competency);

                    var existingDeliverableIds = unitOfWork.ProgramManagementRepository.FindBy<CompetencyDeliverable>
                        (x => x.CompetencyId == competency.Id && x.DateDeactivated == null).Select(x=>x.DeliverableId).Distinct().ToArray();

                    var addedDeliverables = request.DeliverableId.Except(existingDeliverableIds);

                    var removedDeliverables = existingDeliverableIds.Except(request.DeliverableId);

                    if (addedDeliverables.Any())
                    {
                        var competencyDeliverables = new List<CompetencyDeliverable>();

                        var deliverableDuration = unitOfWork.ProgramManagementRepository.FindBy<Deliverable>
                            (x => addedDeliverables.Contains(x.Id)).Sum(x => x.DurationInWeeks);
                        competency.SetDuration(deliverableDuration);

                        foreach (var deliverableId in addedDeliverables)
                        {
                            var activityDeliverable = new CompetencyDeliverable(request.Id, deliverableId, request.CreatedBy);
                            competencyDeliverables.Add(activityDeliverable);
                        }

                        unitOfWork.ProgramManagementRepository.AddRange(competencyDeliverables);
                    }

                    if (removedDeliverables.Any())
                    {
                        var deliverableDuration = unitOfWork.ProgramManagementRepository.FindBy<Deliverable>
                            (x => removedDeliverables.Contains(x.Id)).Sum(x=>x.DurationInWeeks);

                        competency.SubtractDuration(deliverableDuration);

                        List<int> trackedCompetencyDeliverableIds = new List<int>();
                        foreach (var deliverableId in removedDeliverables)
                        {
                            var competencyDeliverable = unitOfWork.ProgramManagementRepository.FindBy<CompetencyDeliverable>
                                (x => x.DeliverableId == deliverableId && x.CompetencyId == competency.Id).SingleOrDefault();

                            competencyDeliverable.Deactivate(request.CreatedBy);

                            if (!trackedCompetencyDeliverableIds.Contains(competencyDeliverable.Id))
                            {
                                unitOfWork.ProgramManagementRepository.Update(competencyDeliverable);
                                trackedCompetencyDeliverableIds.Add(competencyDeliverable.Id);

                            }
                        }
                    }

                    unitOfWork.SaveChanges();
                    transaction.Commit();
                    return Task.FromResult(competency.Id);
                }
                catch (Exception ex)
                {
                    logger.Error(ex, $"An error occurred while updating faculty with Id: {request.DeliverableId }");
                    throw;
                }
            }


        }

    }

}
