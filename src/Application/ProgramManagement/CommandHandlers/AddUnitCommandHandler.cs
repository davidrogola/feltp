﻿using Application.Common;
using Application.ProgramManagement.Commands;
using AutoMapper;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using ProgramManagement.Domain.Examination;
using ProgramManagement.Domain.FieldPlacement;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddUnitCommandHandler : MediatR.IRequestHandler<AddUnitCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IMapper mapper;
        public AddUnitCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork, IMapper _mapper)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            mapper = _mapper;
        }
        public Task<int> Handle(AddUnitCommand request, CancellationToken cancellationToken)
        {
            using (var transaction = programManagementUnitOfWork.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                var unit = new Unit(request.Name, request.Code, request.CreatedBy, request.Description, request.HasFieldPlacementActivity);
                programManagementUnitOfWork.ProgramManagementRepository.Add(unit);

                if (request.HasFieldPlacementActivity)
                {
                    var activities = unit.AddActivities(request.FieldPlacementActivities.ToArray());
                    programManagementUnitOfWork.ProgramManagementRepository.AddRange(activities);
                }

                var examination = new Examination(request.Name, unit.Id,null, request.CreatedBy);
                programManagementUnitOfWork.ProgramManagementRepository.Add(examination);

                programManagementUnitOfWork.SaveChanges();

                transaction.Commit();

                return Task.FromResult(unit.Id);

            }

        }
    }

    public class UpdateUnitCommandHandler : MediatR.IRequestHandler<UpdateUnitCommand, UpdateUnitResponse>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IMapper mapper;
        public UpdateUnitCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork, IMapper _mapper)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            mapper = _mapper;
        }
        public Task<UpdateUnitResponse> Handle(UpdateUnitCommand request, CancellationToken cancellationToken)
        {
            var unit = programManagementUnitOfWork.ProgramManagementRepository.Get<Unit>(request.Id);

            if (unit == null)
            {
                return Task.FromResult(new UpdateUnitResponse
                {
                    IsSuccessful = false,
                    Message = "Unit details not found"
                });
            }
            
            unit.Update(request.Name, request.Code, request.Description,request.UpdatedBy,request.HasFieldPlacementActivity);

            programManagementUnitOfWork.ProgramManagementRepository.Update(unit);

            var existingFieldPlacementActivyIds = programManagementUnitOfWork.ProgramManagementRepository.FindBy<UnitFieldPlacementActivity>
                    (x => x.UnitId == request.Id).Select(x => x.FieldPlacementActivityId).ToList();

            if (request.HasFieldPlacementActivity)
            {                
                var addedActivities = request.FieldPlacementActivityId.Except(existingFieldPlacementActivyIds);

                var removedActivities = existingFieldPlacementActivyIds.Except(request.FieldPlacementActivityId);

                if (addedActivities.Any())
                {
                    AddNewFieldPlacementActivities(addedActivities.ToArray(), request.Id, request.UpdatedBy);
                }
                if (removedActivities.Any())
                {
                    RemoveFieldPlacementActivities(removedActivities.ToArray(), request.Id, request.UpdatedBy);
                }

            }

            if(unit.HasFieldPlacementActivity && !request.HasFieldPlacementActivity)
            {
                RemoveFieldPlacementActivities(existingFieldPlacementActivyIds.ToArray(), request.Id, request.UpdatedBy); 
            }

            programManagementUnitOfWork.SaveChanges();

            return Task.FromResult(new UpdateUnitResponse
            {
                IsSuccessful = true,
                Message = "Unit Details updated succesfully"
            });

        }

        private void AddNewFieldPlacementActivities(int [] newActivityIds, int unitId,string updatedBy)
        {
            var newFieldPlacementActivities = new List<UnitFieldPlacementActivity>();

            foreach (var activityId in newActivityIds)
            {
                var fieldPlacementActivity = new UnitFieldPlacementActivity(unitId, activityId, updatedBy);
                newFieldPlacementActivities.Add(fieldPlacementActivity);

            }
            programManagementUnitOfWork.ProgramManagementRepository.AddRange(newFieldPlacementActivities);
        }

        private void RemoveFieldPlacementActivities(int [] activityIds,int unitId, string removedBy)
        {
            var deactivatedActivities = programManagementUnitOfWork.ProgramManagementRepository.FindBy<UnitFieldPlacementActivity>
                        (x => activityIds.Contains(x.FieldPlacementActivityId) && x.UnitId == unitId).ToList();

            foreach (var activity in deactivatedActivities)
            {
                activity.Deactivate(removedBy);
                programManagementUnitOfWork.ProgramManagementRepository.Update(activity);
            }
        }
    }
    
}
