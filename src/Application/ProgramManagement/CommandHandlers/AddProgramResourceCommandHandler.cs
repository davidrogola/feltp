﻿using Application.ProgramManagement.Commands;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddProgramResourceCommandHandler : IRequestHandler<AddProgramResourceCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        public AddProgramResourceCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
        }
        public Task<int> Handle(AddProgramResourceCommand request, CancellationToken cancellationToken)
        {
            request.DateCreated = DateTime.Now;
            request.CreatedBy = "System";
            var programresource = Mapper.Map<ProgramResource>(request);

            programManagementUnitOfWork.ProgramManagementRepository.Add(programresource);
            programManagementUnitOfWork.SaveChanges();
            return Task.FromResult(programresource.Id);
        }
    }
}
