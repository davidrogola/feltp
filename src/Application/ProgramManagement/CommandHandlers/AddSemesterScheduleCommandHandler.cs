﻿using Application.Common.Services;
using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Services;
using MediatR;
using Microsoft.Extensions.Configuration;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using Serilog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddSemesterScheduleCommandHandler : IRequestHandler<AddSemesterScheduleCommand, ScheduleResponse>
    {
        IProgramManagementUnitOfWork unitOfWork;
        ILogger logger = Log.ForContext<AddSemesterScheduleCommandHandler>();
        ResidentCourseProgressUpdator residentCourseProgressUpdator;
        public AddSemesterScheduleCommandHandler(IProgramManagementUnitOfWork _unitOfWork, ResidentCourseProgressUpdator _residentCourseProgressUpdator)
        {
            unitOfWork = _unitOfWork;
            residentCourseProgressUpdator = _residentCourseProgressUpdator;
        }
        public Task<ScheduleResponse> Handle(AddSemesterScheduleCommand request, CancellationToken cancellationToken)
        {
            try
            {
                request.EstimatedStartDate = request.StrEstimatedStartDate.ToLocalDate();
                request.EstimatedEndDate = request.StrEstimatedEndDate.ToLocalDate();

                var programOffer = unitOfWork.ProgramManagementRepository.Get<ProgramOffer>(request.ProgramOfferId);             

                if (programOffer == null)
                {
                    return Task.FromResult(new ScheduleResponse
                    {
                        Message = "Program offer details not found"
                    });
                }

                var semester = unitOfWork.ProgramManagementRepository.Get<Semester>(request.SemesterId);

                request.Status = request.EstimatedStartDate.Date <= DateTime.Now.Date ? ProgressStatus.InProgress : ProgressStatus.NotStarted;

                var semesterSchedule = new SemesterSchedule(request.ProgramOfferId,$"{programOffer.Name} ({semester.Name} Schedule)", request.SemesterId, request.EstimatedStartDate, request.EstimatedEndDate, request.CreatedBy, request.Status);

                unitOfWork.ProgramManagementRepository.Add(semesterSchedule);
                unitOfWork.SaveChanges();

                if (request.Status == ProgressStatus.InProgress)
                    residentCourseProgressUpdator.UpdateResidentCourseProgress(semesterSchedule.Id, request.SemesterId, request.ProgramOfferId, ProgressStatus.InProgress);

                return Task.FromResult(new ScheduleResponse
                {
                    CreatedSuccesfully = true,
                    Message = "Semester schedule created successfully",
                    ScheduleId = semesterSchedule.Id
                });
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while creating semester schedule for programofferId {request.ProgramOfferId} and semesterId {request.SemesterId }");

                return Task.FromResult(new ScheduleResponse
                {
                    Message = "An error occured while creating semester schedule",

                });
            }

        }

    }
}
