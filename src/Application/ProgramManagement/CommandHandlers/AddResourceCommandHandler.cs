﻿using Application.MapperProfiles.ProgramManagement;
using Application.ProgramManagement.Commands;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddResourceCommandHandler : IRequestHandler<AddResourceCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IMapper mapper;
        public AddResourceCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork, IMapper _mapper)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            mapper = _mapper;
        }
        public Task<int> Handle(AddResourceCommand request, CancellationToken cancellationToken)
        {
            request.DateCreated = DateTime.Now;
            var resource = mapper.Map<Resource>(request);
            programManagementUnitOfWork.ProgramManagementRepository.Add(resource);

            var selectedPrograms = resource.AddPrograms(request.ProgramId.ToArray());
            programManagementUnitOfWork.ProgramManagementRepository.AddRange(selectedPrograms);

            programManagementUnitOfWork.SaveChanges();

            return Task.FromResult(resource.Id);

        }
    }

    public class EditResourceCommandHandler : IRequestHandler<EditResourceCommand, EditResourceResult>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public EditResourceCommandHandler(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<EditResourceResult> Handle(EditResourceCommand request, CancellationToken cancellationToken)
        {
            var resource = unitOfWork.ProgramManagementRepository.Get<Resource>(request.Id);

            if (resource == null)
                return Task.FromResult(new EditResourceResult
                {
                    Message = "Selected resource details not found"
                });

            resource.UpdateResourceDetails(request.ResourceTypeId, request.ResourceTitle, request.Source, request.SourceContactDetails, request.ResourceDescription);

            unitOfWork.ProgramManagementRepository.Update(resource);

            var programResourceList = unitOfWork.ProgramManagementRepository
                  .FindBy<ProgramResource>(x => x.ResourceId == request.Id).ToList();


            var existingProgramIds = programResourceList.Select(x => x.ProgramId).ToList();
            
            var removedProgramIds = existingProgramIds.Except(request.ProgramId);

            var addedProgramIds = request.ProgramId.Except(existingProgramIds);

            var addedProgramsList = new List<ProgramResource>();
            if (addedProgramIds.Any())
            {
                foreach (var newProgramId in addedProgramIds)
                {
                    var addedProgram = new ProgramResource(request.Id, newProgramId, request.UpdatedBy);
                    addedProgramsList.Add(addedProgram);
                }
            }

            if (addedProgramsList.Any())
                unitOfWork.ProgramManagementRepository.AddRange(addedProgramsList);

            if (removedProgramIds.Any())
            {
                var removedProgramResources = programResourceList.Where(x => removedProgramIds.Contains(x.ProgramId)).ToList();

                foreach (var removedProgramResource in removedProgramResources)
                {
                    removedProgramResource.Deactivate(request.UpdatedBy);
                    unitOfWork.ProgramManagementRepository.Update(removedProgramResource);
                }
            }

            unitOfWork.SaveChanges();

            return Task.FromResult(new EditResourceResult { Message = $"{resource.ResourceTitle} details updated successfully" });
        }

    }

}

