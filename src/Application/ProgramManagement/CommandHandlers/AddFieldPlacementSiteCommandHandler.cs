﻿using Application.ProgramManagement.Commands;
using AutoMapper;
using Common.Domain.Address;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.FieldPlacement;
using Serilog;
using Serilog.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddFieldPlacementSiteCommandHandler : IRequestHandler<AddFieldPlacementSiteCommand, int>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public AddFieldPlacementSiteCommandHandler(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<int> Handle(AddFieldPlacementSiteCommand request, CancellationToken cancellationToken)
        {
            using (var transaction = unitOfWork.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    var countyId = (request.CountyId.HasValue && request.CountyId != 0) ? request.CountyId : null;
                    var subCountyId = (request.SubCountyId.HasValue && request.SubCountyId != 0) ? request.SubCountyId : null;

                    var filedPlacementSite = new FieldPlacementSite(request.Name, request.CountryId, countyId, subCountyId, request.PostalAddress, request.Building, request.Location,request.Level, request.CreatedBy);
                    unitOfWork.ProgramManagementRepository.Add(filedPlacementSite);

                    var fieldSiteSupervisors = filedPlacementSite.AddSupervisors(request.Faculty.ToArray(),request.CreatedBy);
                    unitOfWork.ProgramManagementRepository.AddRange(fieldSiteSupervisors);

                    unitOfWork.SaveChanges();
                    transaction.Commit();

                    return Task.FromResult(filedPlacementSite.Id);
                }
                catch (Exception ex)
                {

                    return Task.FromResult(default(int));
                }
            }


        }
    }

    public class UpdateFieldPlacementSiteCommandHandler : IRequestHandler<UpdateFieldPlacementSiteCommand, int>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMediator mediator;
        ILogger logger = Log.ForContext<UpdateFieldPlacementSiteCommandHandler>();

        public UpdateFieldPlacementSiteCommandHandler(IProgramManagementUnitOfWork _unitOfWork, IMediator _mediator)
        {
            unitOfWork = _unitOfWork;
            mediator = _mediator;

        }
        public Task<int> Handle(UpdateFieldPlacementSiteCommand request, CancellationToken cancellationToken)
        {
            using (var transaction = unitOfWork.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {

                    var fieldPlacementSite = unitOfWork.ProgramManagementRepository
                        .FindByWithInclude<FieldPlacementSite>(x => x.Id == request.Id, x => x.SiteSupervisors).SingleOrDefault();

                    fieldPlacementSite.UpdateFieldPlacementSite(request.Name,request.CountryId, request.CountyId, request.SubCountyId, request.PostalAddress, request.Building, request.Location, request.Level);
                    unitOfWork.ProgramManagementRepository.Update(fieldPlacementSite);

                    var existingFacultyIds = fieldPlacementSite.SiteSupervisors.Where(x => x.DateDeactivated ==null).Select(x => x.FacultyId).ToArray();

                    var addedFaculties = request.FacultyId.Except(existingFacultyIds);

                    var removedFaculties = existingFacultyIds.Except(request.FacultyId);

                    if (addedFaculties.Any())
                    {
                        var siteSupervisors = new List<FieldPlacementSiteSupervisor>();

                        foreach (var facultyId in addedFaculties)
                        {
                            var siteSupervisor = new FieldPlacementSiteSupervisor(facultyId, request.Id, request.CreatedBy);
                            siteSupervisors.Add(siteSupervisor);
                        }

                        unitOfWork.ProgramManagementRepository.AddRange(siteSupervisors);
                    }

                    if (removedFaculties.Any())
                    {
                        foreach (var facultyId in removedFaculties)
                        {
                            var supervisor = unitOfWork.ProgramManagementRepository.FindBy<FieldPlacementSiteSupervisor>
                                (x=>x.FacultyId == facultyId && x.FieldPlacementSiteId == fieldPlacementSite.Id && x.DateDeactivated == null)
                                .FirstOrDefault();

                            supervisor.Deactivate(request.CreatedBy);

                            unitOfWork.ProgramManagementRepository.Update(supervisor);
                        }
                    }
                    
                    unitOfWork.SaveChanges();
                    transaction.Commit();
                    return Task.FromResult(fieldPlacementSite.Id);
                }
                catch (Exception ex)
                {
                    logger.Error(ex, $"An error occurred while updating faculty with Id: {request.FacultyId }");
                    throw;
                }
            }


        }

    }

}