﻿using Application.MapperProfiles.ProgramManagement;
using Application.ProgramManagement.Commands;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddCollaboratorCommandHandler : IRequestHandler<AddCollaboratorCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        public AddCollaboratorCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
        }
        public Task<int> Handle(AddCollaboratorCommand request, CancellationToken cancellationToken)
        {
            request.DateCreated = DateTime.Now;
            var Collaborator = Mapper.Map<Collaborator>(request);
            programManagementUnitOfWork.ProgramManagementRepository.Add(Collaborator);

            var selectPrograms = Collaborator.SelectPrograms(request.ProgramId.ToArray());
            programManagementUnitOfWork.ProgramManagementRepository.AddRange(selectPrograms);

            programManagementUnitOfWork.SaveChanges();

            return Task.FromResult((int)Collaborator.Id);
        }
    }

    public class UpdateCollaboratorCommandHandler : IRequestHandler<UpdateCollaboratorCommand, int>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMediator mediator;
        ILogger logger = Log.ForContext<UpdateCollaboratorCommandHandler>();

        public UpdateCollaboratorCommandHandler(IProgramManagementUnitOfWork _unitOfWork, IMediator _mediator)
        {
            unitOfWork = _unitOfWork;
            mediator = _mediator;

        }
        public Task<int> Handle(UpdateCollaboratorCommand request, CancellationToken cancellationToken)
        {
            using (var transaction = unitOfWork.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {

                    var collaborator = unitOfWork.ProgramManagementRepository.Get<Collaborator>(request.Id);

                    collaborator.UpdateCollaborator(request.Name, request.CollaboratorTypeId);
                    unitOfWork.ProgramManagementRepository.Update(collaborator);

                    var existingProgramIds = unitOfWork.ProgramManagementRepository.FindBy<ProgramCollaborator>
                        (x => x.CollaboratorId == collaborator.Id && x.DateDeactivated == null).Select(x => x.ProgramId).Distinct().ToArray();

                    var addedPrograms = request.ProgramId.Except(existingProgramIds);

                    var removedprograms = existingProgramIds.Except(request.ProgramId);

                    if (addedPrograms.Any())
                    {
                        var programCollaborators = new List<ProgramCollaborator>();

                        foreach (var programId in addedPrograms)
                        {
                            var programCollaborator = new ProgramCollaborator(request.Id, programId, request.UpdatedBy);
                            programCollaborators.Add(programCollaborator);
                        }

                        unitOfWork.ProgramManagementRepository.AddRange(programCollaborators);
                    }

                    if (removedprograms.Any())
                    {

                        List<int> trackedProgramCollaboratorsIds = new List<int>();
                        foreach (var programId in removedprograms)
                        {
                            var programCollaborator = unitOfWork.ProgramManagementRepository.FindBy<ProgramCollaborator>
                                (x => x.ProgramId == programId && x.CollaboratorId == collaborator.Id).SingleOrDefault();

                            programCollaborator.Deactivate(request.UpdatedBy);

                            if (!trackedProgramCollaboratorsIds.Contains(programCollaborator.Id))
                            {
                                unitOfWork.ProgramManagementRepository.Update(programCollaborator);
                                trackedProgramCollaboratorsIds.Add(programCollaborator.Id);

                            }
                        }
                    }

                    unitOfWork.SaveChanges();
                    transaction.Commit();
                    return Task.FromResult(collaborator.Id);
                }
                catch (Exception ex)
                {
                    logger.Error(ex, $"An error occurred while updating faculty with Id: {request.ProgramId }");
                    throw;
                }
            }


        }

    }
}

