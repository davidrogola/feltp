﻿using Application.ProgramManagement.Commands;
using AutoMapper;
using MediatR;
using Persistance.Generic;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Event;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Globalization;
using Application.Common.Services;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddEventScheduleCommandHandler : IRequestHandler<AddEventScheduleCommand, int>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMediator mediator;
         IMapper mapper;
        public AddEventScheduleCommandHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper,
            IConfiguration _configiration,IMediator _mediator)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
            mediator = _mediator;
        }

        public Task<int> Handle(AddEventScheduleCommand request, CancellationToken cancellationToken)
        {

            var programEvent = unitOfWork.ProgramManagementRepository
                 .FindByWithInclude<ProgramEvent>(x => x.Id == request.ProgramEventId);
                
            if (programEvent == null)
                return Task.FromResult(0);

            request.StartDate = request.StrStartDate.ToLocalDate();
            request.EndDate = request.StrEndDate.ToLocalDate();

            var eventSchedule = new EventSchedule(request.ProgramEventId,request.PresentationType,
                    request.Location,request.StartDate,request.EndDate, request.IsElapsed, request.CreatedBy);

            unitOfWork.ProgramManagementRepository.Add(eventSchedule);
            unitOfWork.SaveChanges();

            return Task.FromResult(eventSchedule.Id);
        }
    }


}

