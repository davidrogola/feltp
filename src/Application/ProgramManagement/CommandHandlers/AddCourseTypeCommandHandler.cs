﻿using Application.MapperProfiles.ProgramManagement;
using Application.ProgramManagement.Commands;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddCourseTypeCommandHandler : IRequestHandler<AddCourseTypeCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        public AddCourseTypeCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
        }
        public Task<int> Handle(AddCourseTypeCommand request, CancellationToken cancellationToken)
        {
            request.DateCreated = DateTime.Now;
            request.CreatedBy = "System";

            var courseType = Mapper.Map<CourseType>(request);

            programManagementUnitOfWork.ProgramManagementRepository.Add(courseType);
            programManagementUnitOfWork.SaveChanges();

            return Task.FromResult(courseType.Id);
        }
    }

    public class UpdateCourseTypeCommandHandler : IRequestHandler<UpdateCourseTypeCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IMapper mapper;
        public UpdateCourseTypeCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork, IMapper _mapper)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            mapper = _mapper;
        }
        public Task<int> Handle(UpdateCourseTypeCommand request, CancellationToken cancellationToken)
        {
            CourseType type = mapper.Map<CourseType>(request);
            programManagementUnitOfWork.ProgramManagementRepository.Update(type);
            programManagementUnitOfWork.SaveChanges();

            return Task.FromResult(type.Id);
        }
    }
}

