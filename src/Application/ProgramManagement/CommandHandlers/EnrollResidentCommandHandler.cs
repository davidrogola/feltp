﻿using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Notification;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class EnrollResidentCommandHandler : IRequestHandler<EnrollResidentCommand, ResidentEnrollmentResult>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMediator mediator;
        ILogger logger = Log.ForContext<EnrollResidentCommandHandler>();
        public EnrollResidentCommandHandler(IProgramManagementUnitOfWork _unitOfWork, IMediator _mediator)
        {
            unitOfWork = _unitOfWork;
            mediator = _mediator;
        }

        public async Task<ResidentEnrollmentResult> Handle(EnrollResidentCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var isEnrolled = unitOfWork.ProgramManagementRepository.FindBy<ProgramEnrollment>(x => x.ResidentId == request.ResidentId && x.ProgramOfferApplicationId == request.ProgramOfferApplicationId).Any();
                if (isEnrolled)
                    return new ResidentEnrollmentResult
                    {
                        ApplicantId = request.ApplicantId,
                        ProgramOfferId = request.ProgramOfferId,
                        Message = "Applicant is already enrolled to the selected program"
                    };

                var programEnrollment = new ProgramEnrollment(request.ProgramOfferId, request.ResidentId, request.CreatedBy, request.ProgramOfferApplicationId);
                unitOfWork.ProgramManagementRepository.Add(programEnrollment);

                var offerApplication = unitOfWork.ProgramManagementRepository
                    .Get<ProgramOfferApplication>(request.ProgramOfferApplicationId);

                offerApplication.UpdateEnrollment();
                unitOfWork.ProgramManagementRepository.Update(offerApplication);
                unitOfWork.SaveChanges();

                await mediator.Publish(new ResidentEnrolledNotification()
                {
                    DateEnrolled = programEnrollment.DateCreated,
                    ProgramId = request.ProgramId,
                    ResidentId = request.ResidentId,
                    EnrollmentId = programEnrollment.Id,
                    EnrolledBy = programEnrollment.CreatedBy,
                    ProgramOfferId = request.ProgramOfferId
                });

                return new ResidentEnrollmentResult
                {
                    ApplicantId = request.ApplicantId,
                    Message = "Applicant has been succefully enrolled to the selected program",
                    ProgramOfferId = request.ProgramOfferId,
                    Successful = true
                };
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while enrolling applicantId {request.ApplicantId} to programofferId {request.ProgramOfferId}");

                return new ResidentEnrollmentResult
                {
                    ApplicantId = request.ApplicantId,
                    Message = "An error occured while enrolling the applicant to the program.Please try again later",
                    Successful = false
                };
            }
        }
    }


    public class GenerateResidentCourseWorkCommandHandler : IRequestHandler<GenerateResidentCourseWorkCommand, bool>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMediator mediator;
        public GenerateResidentCourseWorkCommandHandler(IProgramManagementUnitOfWork _unitOfWork,  IMediator _mediator)
        {
            unitOfWork = _unitOfWork;
            mediator = _mediator;
        }
        public async Task<bool> Handle(GenerateResidentCourseWorkCommand request, CancellationToken cancellationToken)
        {
            var programEnrollment = unitOfWork.ProgramManagementRepository.FindByWithInclude<ProgramEnrollment>(x => x.Id == request.ProgramEnrollmentId, x => x.ProgramOffer.Program).SingleOrDefault();
            if (programEnrollment == null)
                return false;



            await mediator.Publish(new ResidentEnrolledNotification()
            {
                DateEnrolled = programEnrollment.DateCreated,
                EnrolledBy = programEnrollment.CreatedBy,
                EnrollmentId = request.ProgramEnrollmentId,
                ProgramOfferId = request.ProgramOfferId,
                ResidentId = request.ResidentId,
                ProgramId = programEnrollment.ProgramOffer.ProgramId
            });

            return true;
        }
    }
}
