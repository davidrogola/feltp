﻿using Application.ProgramManagement.Commands;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.FieldPlacement;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddFieldPlacementActivityCommandHandler : IRequestHandler<AddFieldPlacementActivityCommand, int>
    {
        IProgramManagementUnitOfWork unitOfWork;

        public AddFieldPlacementActivityCommandHandler(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<int> Handle(AddFieldPlacementActivityCommand request, CancellationToken cancellationToken)
        {
            var fieldPlacementActivity = new FieldPlacementActivity(request.Name, "System",request.Description, request.ActivityTpe);
            unitOfWork.ProgramManagementRepository.Add(fieldPlacementActivity);

            var fieldPlacementActivityDeliverables = fieldPlacementActivity.AddDeliverables(request.DeliverableId.ToArray());
            unitOfWork.ProgramManagementRepository.AddRange(fieldPlacementActivityDeliverables);

            unitOfWork.SaveChanges();

            return Task.FromResult(fieldPlacementActivity.Id);
        }
    }

    public class UpdateFieldPlacementActivityCommandHandler : IRequestHandler<UpdateFieldPlacementActivityCommand, int>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMediator mediator;
        ILogger logger = Log.ForContext<UpdateFieldPlacementActivityCommandHandler>();

        public UpdateFieldPlacementActivityCommandHandler(IProgramManagementUnitOfWork _unitOfWork, IMediator _mediator)
        {
            unitOfWork = _unitOfWork;
            mediator = _mediator;

        }
        public Task<int> Handle(UpdateFieldPlacementActivityCommand request, CancellationToken cancellationToken)
        {
            using (var transaction = unitOfWork.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {

                    var fieldPlacementActivity = unitOfWork.ProgramManagementRepository
                        .FindByWithInclude<FieldPlacementActivity>(x => x.Id == request.Id).SingleOrDefault();

                    fieldPlacementActivity.UpdateFieldPlacementActivity(request.Name, request.Description, request.ActivityType);
                    unitOfWork.ProgramManagementRepository.Update(fieldPlacementActivity);

                    var fieldPlacementActivityDeliverables = unitOfWork.ProgramManagementRepository.FindBy<FieldPlacementActivityDeliverable>
                        (x => x.FieldPlacementActivityId == request.Id).ToList();

                    var existingDeliverableIds = fieldPlacementActivityDeliverables.Select(x => x.DeliverableId).ToArray();

                    var addedDeliverables = request.DeliverableId.Except(existingDeliverableIds);

                    var removedDeliverables = existingDeliverableIds.Except(request.DeliverableId);

                    if (addedDeliverables.Any())
                    {
                        var activityDeliverables = new List<FieldPlacementActivityDeliverable>();

                        foreach (var deliverableId in addedDeliverables)
                        {
                            var activityDeliverable = new FieldPlacementActivityDeliverable(request.Id, deliverableId, request.UpdatedBy);
                            activityDeliverables.Add(activityDeliverable);
                        }

                        unitOfWork.ProgramManagementRepository.AddRange(activityDeliverables);
                    }

                    if (removedDeliverables.Any())
                    {
                        foreach (var deliverableId in removedDeliverables)
                        {
                            var activityDeliverable = fieldPlacementActivityDeliverables.Where(x=>x.DeliverableId==deliverableId)
                                .SingleOrDefault();
                            activityDeliverable?.Deactivate(request.UpdatedBy);
                            unitOfWork.ProgramManagementRepository.Update(activityDeliverable);
                        }
                    }

                    unitOfWork.SaveChanges();
                    transaction.Commit();
                    return Task.FromResult(fieldPlacementActivity.Id);
                }
                catch (Exception ex)
                {
                    logger.Error(ex, $"An error occurred while updating field placement activity with Id: {request.Id }");
                    throw;
                }
            }


        }

    }

}

