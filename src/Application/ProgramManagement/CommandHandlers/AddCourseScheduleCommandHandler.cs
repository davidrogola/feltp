﻿using Application.ProgramManagement.Commands;
using AutoMapper;
using MediatR;
using Persistance.Generic;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddCourseScheduleCommandHandler : IRequestHandler<AddCourseScheduleCommand, int>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMediator mediator;
        public IMapper mapper;
        IConfiguration configuration;
        public AddCourseScheduleCommandHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper,
            IConfiguration _configiration, IMediator _mediator)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
            mediator = _mediator;
            configuration = _configiration;
        }

        public Task<int> Handle(AddCourseScheduleCommand request, CancellationToken cancellationToken)
        {
            var dateFormat = configuration["DateConversionFormat"];

            var programCourse = unitOfWork.ProgramManagementRepository
                .FindByWithInclude<ProgramCourse>(x => x.Id == request.ProgramCourseId, x => x.Semester)
                .Select(x => new
                {
                    Semester = x.Semester.Name,
                    Duration = x.DurationInWeeks
                }).SingleOrDefault();

            if (programCourse == null)
                return Task.FromResult(0);

            string scheduleName = $"{programCourse.Semester} - Schedule";

            int daysInWeek = 7;

            var durationInDays = programCourse.Duration * daysInWeek;
            request.StartDate = DateTime.ParseExact(request.StrStartDate, dateFormat, CultureInfo.InvariantCulture);

            var courseSchedule = new CourseSchedule(scheduleName, request.ProgramOfferId, request.ProgramCourseId, request.StartDate,
                request.StartDate.AddDays(durationInDays), request.FacultyId, request.Location, "System");

            unitOfWork.ProgramManagementRepository.Add(courseSchedule);
            unitOfWork.SaveChanges();

            return Task.FromResult(courseSchedule.Id);
        }
    }


}

