﻿using Application.ProgramManagement.Commands;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddWorkshopCommandHandler : IRequestHandler<AddWorkshopCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        public AddWorkshopCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
        }
        public Task<int> Handle(AddWorkshopCommand request, CancellationToken cancellationToken)
        {
            request.DateCreated = DateTime.Now;
            request.CreatedBy = "System";
            var workshop = Mapper.Map<Workshop>(request);

            programManagementUnitOfWork.ProgramManagementRepository.Add(workshop);
            programManagementUnitOfWork.SaveChanges();
            return Task.FromResult(workshop.Id);
        }
    }

    public class UpdateWorkshopCommandHandler : IRequestHandler<UpdateWorkshopCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IMapper mapper;
        public UpdateWorkshopCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork, IMapper _mapper)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            mapper = _mapper;
        }
        public Task<int> Handle(UpdateWorkshopCommand request, CancellationToken cancellationToken)
        {
            Workshop workshop = mapper.Map<Workshop>(request);
            programManagementUnitOfWork.ProgramManagementRepository.Update(workshop);
            programManagementUnitOfWork.SaveChanges();

            return Task.FromResult(workshop.Id);
        }
    }
}
