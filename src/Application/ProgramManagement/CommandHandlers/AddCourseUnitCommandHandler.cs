﻿using Application.ProgramManagement.Commands;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddCourseUnitCommandHandler : IRequestHandler<AddCourseUnitCommand,List<int>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public AddCourseUnitCommandHandler(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }


        Task<List<int>> IRequestHandler<AddCourseUnitCommand,List<int>>.Handle(AddCourseUnitCommand request, 
            CancellationToken cancellationToken)
        {
            List<CourseUnit> courseModules = new List<CourseUnit>();
            foreach(var moduleId in request.UnitId)
            {
                var courseModule = new CourseUnit(request.CourseId, moduleId, "System");
                courseModules.Add(courseModule);
            }
            unitOfWork.ProgramManagementRepository.AddRange(courseModules);
            unitOfWork.SaveChanges();

            return Task.FromResult(courseModules.Select(x=>x.Id).ToList());
        }
    }
}
