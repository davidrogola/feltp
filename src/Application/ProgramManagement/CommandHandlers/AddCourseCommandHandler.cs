﻿using Application.Common;
using Application.Common.SequenceGenerator;
using Application.ProgramManagement.Commands;
using AutoMapper;
using MediatR;
using Microsoft.Extensions.Configuration;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ProgramManagement.Domain.Examination;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddCourseCommandHandler : IRequestHandler<AddCourseCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IMapper mapper;
        ICodeGenerator codeGenerator;
        public AddCourseCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork, IMapper _mapper,
            ICodeGenerator _codeGenerator)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            codeGenerator = _codeGenerator;
            mapper = _mapper;
        }
        public Task<int> Handle(AddCourseCommand request, CancellationToken cancellationToken)
        {
            var courseCode = codeGenerator.Generate("coursecode");

            var course = new Course(request.CategoryId, request.Name, request.Code, request.CreatedBy);
            course.SetCode(courseCode);

            programManagementUnitOfWork.ProgramManagementRepository.Add(course);

            var courseUnits = course.AddUnits(request.UnitId.ToArray(),request.CreatedBy);
            programManagementUnitOfWork.ProgramManagementRepository.AddRange(courseUnits);
            programManagementUnitOfWork.SaveChanges();

            var examination = new Examination(course.Name,null,course.Id,course.CreatedBy);
            programManagementUnitOfWork.ProgramManagementRepository.Add(examination);

            programManagementUnitOfWork.SaveChanges();

            return Task.FromResult(course.Id);
        }
    }

    public class UpdateCourseCommandHandler : IRequestHandler<UpdateCourseCommand, int>
    {
        private readonly IProgramManagementUnitOfWork unitOfWork;

        public UpdateCourseCommandHandler(IProgramManagementUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public Task<int> Handle(UpdateCourseCommand request, CancellationToken cancellationToken)
        {
            var course = unitOfWork.ProgramManagementRepository.Get<Course>(request.Id);
            if(course == null)
            {
                return Task.FromResult(0);
            }
            course.Update(request.Name,request.CategoryId);
            unitOfWork.ProgramManagementRepository.Update(course);

            var courseUnits = unitOfWork.ProgramManagementRepository.FindBy<CourseUnit>
                (x => x.CourseId == request.Id && x.DateDeactivated == null);

            var existingUnitIds = courseUnits.Select(x => x.UnitId);

            var addedUnits = request.UnitId.Except(existingUnitIds);

            var removedUnitIds = existingUnitIds.Except(request.UnitId);

            if (removedUnitIds.Any())
            {
                var removedCourseUnits = courseUnits.Where(x => removedUnitIds.Contains(x.UnitId));

                foreach (var courseUnit in removedCourseUnits)
                {
                    courseUnit.Deactivate(request.UpdatedBy);
                    unitOfWork.ProgramManagementRepository.Update(courseUnit);
                }
            }
            var newCourseUnits = new List<CourseUnit>();
            if (addedUnits.Any())
            {
                foreach (var unit in addedUnits)
                {
                    var courseUnit = new CourseUnit(request.Id, unit, request.UpdatedBy);
                    newCourseUnits.Add(courseUnit);
                }

                unitOfWork.ProgramManagementRepository.AddRange(newCourseUnits);
            }

            unitOfWork.SaveChanges();

            return Task.FromResult(request.Id);
        }
    }

}
