﻿using Application.ProgramManagement.Commands;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddSeminarCommandHandler : IRequestHandler<AddSeminarCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        public AddSeminarCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
        }
        public Task<int> Handle(AddSeminarCommand request, CancellationToken cancellationToken)
        {
            request.DateCreated = DateTime.Now;
            request.CreatedBy = "System";
            var seminar = Mapper.Map<Seminar>(request);

            programManagementUnitOfWork.ProgramManagementRepository.Add(seminar);
            programManagementUnitOfWork.SaveChanges();
            return Task.FromResult(seminar.Id);
        }
    }

    public class UpdateSeminarCommandHandler : IRequestHandler<UpdateSeminarCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IMapper mapper;
        public UpdateSeminarCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork, IMapper _mapper)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            mapper = _mapper;
        }
        public Task<int> Handle(UpdateSeminarCommand request, CancellationToken cancellationToken)
        {
            Seminar seminar = mapper.Map<Seminar>(request);
            programManagementUnitOfWork.ProgramManagementRepository.Update(seminar);
            programManagementUnitOfWork.SaveChanges();

            return Task.FromResult(seminar.Id);
        }
    }
}
