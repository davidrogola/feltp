﻿using Application.ProgramManagement.Commands;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddGraduationRequirementCommandHandler : IRequestHandler<AddGraduationRequirementCommand, int>
    {
        IProgramManagementUnitOfWork unitOfWork;

        public AddGraduationRequirementCommandHandler(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<int> Handle(AddGraduationRequirementCommand request, CancellationToken cancellationToken)
        {

            var graduationRequirement = new GraduationRequirement(request.ProgramId, request.NumberOfDidacticUnitsRequired, request.NumberOfActivitiesRequired, request.PassingGrade,request.NumberOfThesisDefenceDeliverables,request.CreatedBy);

            unitOfWork.ProgramManagementRepository.Add(graduationRequirement);
            unitOfWork.SaveChanges();

            return Task.FromResult(graduationRequirement.Id);
        }
    }
}
