﻿using Application.ProgramManagement.Commands;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
   public class AddSemesterCommandHandler : IRequestHandler<AddSemesterCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        public AddSemesterCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
        }
        public Task<int> Handle(AddSemesterCommand request, CancellationToken cancellationToken)
        {
            request.DateCreated = DateTime.Now;
            request.CreatedBy = "System";
            var semester = Mapper.Map<Semester>(request);

            programManagementUnitOfWork.ProgramManagementRepository.Add(semester);
            programManagementUnitOfWork.SaveChanges();
            return Task.FromResult(semester.Id);
        }
    }

    public class UpdateSemesterCommandHandler : IRequestHandler<UpdateSemesterCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IMapper mapper;
        public UpdateSemesterCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork, IMapper _mapper)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            mapper = _mapper;
        }
        public Task<int> Handle(UpdateSemesterCommand request, CancellationToken cancellationToken)
        {
            var semester = programManagementUnitOfWork.ProgramManagementRepository.Get<Semester>(request.Id);
            semester.Update(request.Name);
            
            programManagementUnitOfWork.ProgramManagementRepository.Update(semester);
            programManagementUnitOfWork.SaveChanges();

            return Task.FromResult(semester.Id);
        }
    }

    public class DeactivateSemesetsrCommandHandler : IRequestHandler<DeactivateSemesterCommand, int>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IMapper mapper;
        public DeactivateSemesetsrCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork, IMapper _mapper)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            mapper = _mapper;
        }
        public Task<int> Handle(DeactivateSemesterCommand request, CancellationToken cancellationToken)
        {
            var semester = programManagementUnitOfWork.ProgramManagementRepository.Get<Semester>(request.Id);
            if (semester == null)
            {
                return Task.FromResult(0);
            }
            semester.Deactivate(request.DeactivatedBy);
            programManagementUnitOfWork.ProgramManagementRepository.Update(semester);

            programManagementUnitOfWork.SaveChanges();
            return Task.FromResult(semester.Id);
        }
    }
}
