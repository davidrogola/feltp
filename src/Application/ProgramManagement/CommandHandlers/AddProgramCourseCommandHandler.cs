﻿using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Notification;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddProgramCourseCommandHandler : IRequestHandler<AddProgramCourseCommand, int>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMediator mediator;
        public IMapper mapper;
        public AddProgramCourseCommandHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper,
            IMediator _mediator)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
            mediator = _mediator;
        }

        public Task<int> Handle(AddProgramCourseCommand request, CancellationToken cancellationToken)
        {
            request.DateCreated = DateTime.Now;

            var programCourse = mapper.Map<ProgramCourse>(request);
            unitOfWork.ProgramManagementRepository.Add(programCourse);

            var program = unitOfWork.ProgramManagementRepository.Get<Program>(request.ProgramId);
            program.AddDuration(request.DurationInWeeks);
            unitOfWork.ProgramManagementRepository.Update(program);

            unitOfWork.SaveChanges();

            mediator.Publish(new ProgramCourseAddedNotification { ProgramCourseId = programCourse.Id });

            return Task.FromResult(programCourse.Id);
        }

        public class DeactivateProgramCourseCommandHandler : IRequestHandler<DeactivateProgramCourseCommand, int>
        {
            IProgramManagementUnitOfWork programManagementUnitOfWork;
            IMapper mapper;
            public DeactivateProgramCourseCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork, IMapper _mapper)
            {
                programManagementUnitOfWork = _programManagementUnitOfWork;
                mapper = _mapper;
            }
            public Task<int> Handle(DeactivateProgramCourseCommand request, CancellationToken cancellationToken)
            {
                using (var transaction = programManagementUnitOfWork.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                {
                    var course = programManagementUnitOfWork.ProgramManagementRepository.Get<ProgramCourse>(request.Id);
                    if (course == null)
                    {
                        return Task.FromResult(0);
                    }
                    course.Deactivate(request.DeactivatedBy);
                    programManagementUnitOfWork.ProgramManagementRepository.Update(course);

                    var program = programManagementUnitOfWork.ProgramManagementRepository.Get<Program>(request.ProgramId);
                    program.AdjustProgramDuration(course.DurationInWeeks);
                    programManagementUnitOfWork.ProgramManagementRepository.Update(program);

                    programManagementUnitOfWork.SaveChanges();
                    transaction.Commit();

                    return Task.FromResult(course.Id);

                }

            }
        }
    }
}
