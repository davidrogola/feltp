﻿using Application.Common.Services;
using Application.ProgramManagement.Commands;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddScheduleItemTimetableCommandHandler : IRequestHandler<AddScheduleItemTimetableCommand, AddTimetableResponse>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public AddScheduleItemTimetableCommandHandler(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<AddTimetableResponse> Handle(AddScheduleItemTimetableCommand request, CancellationToken cancellationToken)
        {

            request.StartDate = $"{ request.TimetableItem.Date } {request.TimetableItem.StartTime}";
            request.EndDate = $"{ request.TimetableItem.Date } {request.TimetableItem.EndTime}";

            if(request.TimetableItem.TimetableType == 0)
                request.TimetableItem.TimetableType = request.TimetableType;

            var timeTableItem = new Timetable(request.ScheduleItemId, request.StartDate.ToLocalDate(),
                    request.EndDate.ToLocalDate(), request.TimetableItem.TimetableType, request.TimetableItem.FacultyId, 
                    request.TimetableItem.ExaminationId, request.TimetableItem.UnitId, request.CreatedBy);          

            unitOfWork.ProgramManagementRepository.Add(timeTableItem);
            unitOfWork.SaveChanges();

            return Task.FromResult(new AddTimetableResponse
            {
                IsSuccessful = true,
                Message = "Timetable created successfully",
                ScheduleItemId = request.ScheduleItemId,
                SemesterScheduleId = request.SemesterScheduleId
            });

        }
    }

    public class UpdateScheduleItemTimetableCommandHandler : IRequestHandler<UpdateScheduleItemTimetableCommand, AddTimetableResponse>
    {
        private IProgramManagementUnitOfWork unitOfWork;

        public UpdateScheduleItemTimetableCommandHandler(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<AddTimetableResponse> Handle(UpdateScheduleItemTimetableCommand request, CancellationToken cancellationToken)
        {
            var timetableItem = unitOfWork.ProgramManagementRepository.Get<Timetable>(request.Id);
            
            if (timetableItem == null)
            {
                return Task.FromResult(new AddTimetableResponse
                {
                    IsSuccessful = false,
                    Message = "An error occured while editing timetable details",
                    ScheduleItemId = request.ScheduleItemId,
                    SemesterScheduleId = request.SemesterScheduleId
                });
            }
            
            string startDate = $"{ request.Date } {request.StartTime}";
            string endDate = $"{ request.Date } {request.EndTime}";

            timetableItem.Update(startDate.ToLocalDate(), endDate.ToLocalDate(), request.FacultyId, request.ExaminationId, request.UnitId);

            unitOfWork.ProgramManagementRepository.Update(timetableItem);
            unitOfWork.SaveChanges();

            return Task.FromResult(new AddTimetableResponse
            {
                IsSuccessful = true,
                Message = "Timetable details updated succesfully",
                ScheduleItemId = request.ScheduleItemId,
                SemesterScheduleId = request.SemesterScheduleId
            });
        }
    }
}
