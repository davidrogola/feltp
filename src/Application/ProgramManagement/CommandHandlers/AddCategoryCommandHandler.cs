﻿using Application.ProgramManagement.Commands;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddCategoryCommandHandler : IRequestHandler<AddCategoryCommand, int>
    {
        readonly IProgramManagementUnitOfWork programManagementUnitOfWork;
        public AddCategoryCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
        }
        public Task<int> Handle(AddCategoryCommand request, CancellationToken cancellationToken)
        {   
            request.DateCreated = DateTime.Now;
            request.CreatedBy = "System";
            var category = Mapper.Map<Category>(request);

            programManagementUnitOfWork.ProgramManagementRepository.Add(category);
            programManagementUnitOfWork.SaveChanges();
            return Task.FromResult(category.Id);
        }
    }

    public class UpdateCategoryCommandHandler : IRequestHandler<UpdateCategoryCommand, int>
    {
        readonly IProgramManagementUnitOfWork _programManagementUnitOfWork;
        IMapper _mapper;
        public UpdateCategoryCommandHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork, IMapper _mapper)
        {
            this._programManagementUnitOfWork = _programManagementUnitOfWork;
            this._mapper = _mapper;
        }
        public Task<int> Handle(UpdateCategoryCommand request, CancellationToken cancellationToken)
        {
            var category = _programManagementUnitOfWork.ProgramManagementRepository.Get<Category>(request.Id);

            category.Update(request.Name, request.CourseTypeId);

            _programManagementUnitOfWork.ProgramManagementRepository.Update(category);
            _programManagementUnitOfWork.SaveChanges();

            return Task.FromResult(category.Id);
        }
    }
}
