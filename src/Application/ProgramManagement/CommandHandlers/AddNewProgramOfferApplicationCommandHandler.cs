﻿using Application.ProgramManagement.Commands;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;
using Serilog;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddNewProgramOfferApplicationCommandHandler : IRequestHandler<AddNewProgramOfferApplicationCommand, AddProgramOfferApplicationResponse>
    {
        IProgramManagementUnitOfWork programUnitOfWork;
        IResidentManagementUnitOfWork residentUnitOfWork;
        ILogger logger = Log.ForContext<AddNewProgramOfferApplicationCommandHandler>();
        public AddNewProgramOfferApplicationCommandHandler(IProgramManagementUnitOfWork _programUnitOfWork,
        IResidentManagementUnitOfWork _residentUnitOfWork)
        {
            programUnitOfWork = _programUnitOfWork;
            residentUnitOfWork = _residentUnitOfWork;
        }
        public Task<AddProgramOfferApplicationResponse> Handle(AddNewProgramOfferApplicationCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var existingApplicant = request.ApplicantId == default(long) ?  residentUnitOfWork.ResidentManagementRepository
                    .FindBy<Applicant>(x => x.PersonId == request.PersonId).Any() : true;
                if (!existingApplicant)
                {
                    var applicant = new Applicant(request.PersonId, "System");
                    residentUnitOfWork.ResidentManagementRepository.Add(applicant);
                    residentUnitOfWork.SaveChanges();
                    request.ApplicantId = applicant.Id;
                }

                var programOfferApplication = new ProgramOfferApplication(request.ProgramOfferId, request.ApplicantId, ProgramQualificationStatus.Pending_Evaluation, DateTime.Now, request.CreatedBy ?? "System", request.ApprovedByCounty, request.PersonalStatementSubmitted, ApplicationStatus.Pending);

                programUnitOfWork.ProgramManagementRepository.Add(programOfferApplication);
                programUnitOfWork.SaveChanges();

                return Task.FromResult(new AddProgramOfferApplicationResponse
                {
                    ApplicantId = request.ApplicantId,
                    ProgramOfferApplicationId = programOfferApplication.Id,
                    ProgramOfferId = request.ProgramOfferId,
                    PersonId = request.PersonId,
                    Success = true,
                    Message = "Program offer application added successfully"
                });
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while adding a new program offer application for Applicant Id {request.ApplicantId}");
                return Task.FromResult(new AddProgramOfferApplicationResponse
                {
                    Message = "An error occured while adding a new program offer application",
                    Success = false,
                    ProgramOfferId = request.ProgramOfferId,
                    ApplicantId = request.ProgramOfferId
                });
            }
        }
    }

    public class AddPersonalStatementFileCommandHandler : IRequestHandler<AddPersonalStatementFileCommand>
    {
        IResidentManagementUnitOfWork residentUnitOfWork;
        public AddPersonalStatementFileCommandHandler(IResidentManagementUnitOfWork _residentUnitOfWork)
        {
            residentUnitOfWork = _residentUnitOfWork;
        }
        public Task Handle(AddPersonalStatementFileCommand message, CancellationToken cancellationToken)
        {
            using (var memoryStream = new MemoryStream())
            {
                message.StatementFile.CopyTo(memoryStream);

                var personalStatementFile = new PersonalStatementFile(message.ProgramOfferApplicationId,
                    memoryStream.ToArray(), message.StatementFile.FileName, message.StatementFile.ContentType);

                residentUnitOfWork.ResidentManagementRepository.Add(personalStatementFile);
                residentUnitOfWork.SaveChanges();
            }

            return Task.FromResult(0);
        }
    }
}
