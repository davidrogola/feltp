﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.LookUp.Queries
{
    public class GetCountries : IRequest<List<CountryViewModel>>
    {
        public int Id { get; set; }
    }

    public class CountryViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
