﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.LookUp.Queries
{
    public class GetSubCounties : IRequest<List<SubCountyViewModel>>
    {
        public int ? CountyId { get; set; }
    }

    public class SubCountyViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

}
