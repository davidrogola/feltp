﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.LookUp.Queries
{
    public class GetMinistries : IRequest<List<MinistryViewModel>>
    {
        public int? Id { get; set; }
    }

    public class MinistryViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
