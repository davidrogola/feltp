﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.LookUp.Queries
{
    public class GetUniversities : IRequest<List<UniversityViewModel>>
    {
        public int? Id { get; set; }
    }

    public class UniversityViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
