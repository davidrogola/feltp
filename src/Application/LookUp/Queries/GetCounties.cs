﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.LookUp.Queries
{
    public class GetCounties : IRequest<List<CountyViewModel>>
    {
        public int ? CountryId { get; set; }
    }

    public class CountyViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
