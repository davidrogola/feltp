﻿using Application.LookUp.Queries;
using Common.Models;
using MediatR;
using Persistance.LookUpRepo.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.LookUp.QueryHandlers
{
    public class GetCountriesQueryHandler : IRequestHandler<GetCountries, List<CountryViewModel>>
    {
        ILookUpUnitOfWork unitOfWork;
        public GetCountriesQueryHandler(ILookUpUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<List<CountryViewModel>> Handle(GetCountries request, CancellationToken cancellationToken)
        {
            var countries = unitOfWork.LookUpRepository.GetAll<Country>();

            var countryViewModel = countries.
                Select(x => new CountryViewModel
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();
            return Task.FromResult(countryViewModel);
        }
    }
}
