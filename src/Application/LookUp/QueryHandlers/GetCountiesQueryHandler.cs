﻿using Application.LookUp.Queries;
using Common.Models;
using MediatR;
using Persistance.LookUpRepo.Repository;
using Persistance.LookUpRepo.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.LookUp.QueryHandlers
{
    public class GetCountiesQueryHandler : IRequestHandler<GetCounties, List<CountyViewModel>>
    {
        ILookUpUnitOfWork unitOfWork;
        public GetCountiesQueryHandler(ILookUpUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<List<CountyViewModel>> Handle(GetCounties request, CancellationToken cancellationToken)
        {
            IEnumerable<County> county = null;
            county = request.CountryId.HasValue ? unitOfWork.LookUpRepository.FindBy<County>(x => x.CountryId == request.CountryId) 
                                                  : unitOfWork.LookUpRepository.GetAll<County>();

            var countyViewModel = county.Select(x => new CountyViewModel
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            return Task.FromResult(countyViewModel);
        }
    }
}
