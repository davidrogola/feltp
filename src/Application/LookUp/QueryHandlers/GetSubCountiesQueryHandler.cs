﻿using Application.LookUp.Queries;
using Common.Models;
using MediatR;
using Persistance.LookUpRepo.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.LookUp.QueryHandlers
{
    public class GetSubCountiesQueryHandler : IRequestHandler<GetSubCounties, List<SubCountyViewModel>>
    {
        ILookUpUnitOfWork unitOfWork;
        public GetSubCountiesQueryHandler(ILookUpUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }

        public Task<List<SubCountyViewModel>> Handle(GetSubCounties request, CancellationToken cancellationToken)
        {
            IEnumerable<SubCounty> subCounties = null;
            subCounties = request.CountyId.HasValue ? unitOfWork.LookUpRepository
                                .FindBy<SubCounty>(x => x.CountyId == request.CountyId):
                             unitOfWork.LookUpRepository.GetAll<SubCounty>();

            var subCountiesViewModel = subCounties.Select(x => new SubCountyViewModel
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            return Task.FromResult(subCountiesViewModel);
        }
    }
}
