﻿using Application.LookUp.Queries;
using Common.Domain.Person;
using MediatR;
using Persistance.LookUpRepo.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.LookUp.QueryHandlers
{
    public class GetUniversitiesQueryHandler : IRequestHandler<GetUniversities, List<UniversityViewModel>>
    {
        ILookUpUnitOfWork unitOfWork;
        public GetUniversitiesQueryHandler(ILookUpUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<List<UniversityViewModel>> Handle(GetUniversities request, CancellationToken cancellationToken)
        {
            var universities = unitOfWork.LookUpRepository.GetAll<University>();

            var universityViewModel = universities.
                Select(x => new UniversityViewModel
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();
            return Task.FromResult(universityViewModel);
        }
    }
}
