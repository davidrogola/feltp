﻿using Application.LookUp.Queries;
using Common.Domain.Person;
using MediatR;
using Persistance.LookUpRepo.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.LookUp.QueryHandlers
{
    public class GetMinistriesQueryHandler : IRequestHandler<GetMinistries, List<MinistryViewModel>>
    {
        ILookUpUnitOfWork unitOfWork;
        public GetMinistriesQueryHandler(ILookUpUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<List<MinistryViewModel>> Handle(GetMinistries request, CancellationToken cancellationToken)
        {
            var ministries = unitOfWork.LookUpRepository.GetAll<Ministry>();

            var ministryViewModel = ministries.
                Select(x => new MinistryViewModel
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();
            return Task.FromResult(ministryViewModel);
        }
    }
}
