﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.Models
{
    public class PersonViewModel
    {

        public BioDataInformation BioDataInfo { get; set; }
        public ContactInfo ContactInfo { get; set; }
        public AddressInfo AddressInfo { get; set; }
        public List<EmploymentDetail> EmploymentDetail { get; set; }
        public List<AcademicDetail> AcademicDetail { get; set; }
        public string Cadre { get; set; }
        public string EmploymentInformation{ get; set; }
        public string AcademicInformation { get; set; }
    }
}
