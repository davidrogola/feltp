﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Application.Common.Models
{
    public class ContactInfo
    {
        public int Id { get; set; }
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }
        public string PrimaryMobileNumber { get; set; }
        public string AlternateMobileNumber { get; set; }
    }
}
