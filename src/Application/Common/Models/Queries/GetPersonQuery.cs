﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.Models.Queries
{
    public class GetPersonQuery : IRequest<PersonViewModel>
    {
        public long PersonId { get; set; }
        public string IdNumber { get; set; }

    }
}
