﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.Models.Queries
{
    public class GetPersonEmploymentHistory : IRequest<List<EmploymentDetail>>
    {
        public long PersonId { get; set; }

    }

    public class GetPersonAcademicDetail : IRequest<List<AcademicDetail>>
    {

        public long  PersonId { get; set; }

    }

}
