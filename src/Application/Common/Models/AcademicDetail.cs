﻿using Common.Domain.Person;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.Models
{
   public class AcademicDetail
    {   
        public string CourseName { get; set; }
        public string University { get; set; }
        public int? UniversityId { get; set; }
        public string Other { get; set; }
        public string YearOfGraduation { get; set; }
        public EducationLevel HighestEducationLevel { get; set; }
        public SelectList UniversityCoursesSelectList { get; set; }
        public List<SelectListItem> UniversitySelectList { get; set; }
        public int  ? UniversityCourseId { get; set; }
        public string StrEducationLevel { get; set; }
        public string OtherQualifications { get; set; }
        public bool GraduateofFELTP { get; set; }
        public bool GraduateofEIS { get; set; }
        public long PersonId { get; set; }
        public bool UniversityNotFound { get; set; }
        public bool CourseNotFound { get; set; }
        public bool PersonalStatementSubmitted { get; set; }
    }
}
