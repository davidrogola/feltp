﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.Models
{
    public class ApiResponse
    {
        public ApiResponse(string message, string responseInfo, bool success,List<Error> errors = null)
        {
            Message = message;
            ResponseInfo = responseInfo;
            Success = success;
            Errors = errors;
        }
        public string Message { get; set; }
        public string ResponseInfo { get; set; }
        public bool Success { get; set; }
        public List<Error> Errors { get; set; }

        public void SetErrors(List<Error> errors)
        {
            Errors = errors;
        }
    }

   

    public class Error
    {
        public string ErrorMessage { get; set; }
        public string ErrorCode { get; set; }

    }
}
