﻿using Common.Domain.Person;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.Models
{
    public class BioDataInformation  : IRequest<long>
    {
        public long Id { get; set; }
        public string Name { get { return FirstName + " " + LastName; } set { } }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OtherName { get; set; }
        public Gender Gender { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string StrDateOfBirth { get; set; }
        public string IdentificationNumber { get; set; }
        public IdentificationType IdentificationType { get; set; }
        public int ? CadreId { get; set; }

    }
}
