﻿using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.Models
{
    public class AddPersonCommand : IRequest<long>
    {
        public BioDataInformation BioDataInfo { get; set; }
        public ContactInfo ContactInfo { get; set; }
        public AddressInfo AddressInfo { get; set; }
        public EmploymentDetail EmploymentDetail { get; set; }
        public AcademicDetail AcademicDetail { get; set; }
    }

    public class UpdatePersonBioDataCommand : IRequest<long>
    {
       public BioDataInformation BioDataInfo { get; set; }
       public List<SelectListItem> Cadres { get; set; }

    }

    public class UpdatePersonContactInfoCommand : IRequest<long>
    {
        public ContactInfo ContactInfo { get; set; }
        public AddressInfo AddressInfo { get; set; }
        public List<SelectListItem> Countries { get; set; }
        public List<SelectListItem> Counties { get; set; }
        public List<SelectListItem> Subcounties { get; set; }
        public long PersonId { get; set; }
        public long CorrelationId { get; set; }
        public int ProgramOfferId { get; set; }
    }

    public class AddPersonAcademicInfoCommand : IRequest<long>
    {
        public AcademicDetail AcademicInfo { get; set; }
    }

    public class AddPersonEmploymentInfoCommand : IRequest<long>
    {
        public EmploymentDetail EmploymentInfo { get; set; }
    }
}
