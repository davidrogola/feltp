﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.Models
{
    public class EmploymentDetail
    {
        public bool EmployedByMinistry { get; set; }
        public bool IsCurrentEmployer { get; set; }
        public string WorkStation { get; set; }
        public string PersonnelNumber { get; set; }
        public string Designation { get; set; }
        public string DurationInPosition { get; set; }
        public string EmployerName { get; set; }
        public string EmploymentStartDate { get; set; }
        public string EmploymentEndDate { get; set; }
        public string DateOfEmployment { get; set; }
        public string YearOfEmployment { get; set; }
        public string CurrentPosition { get; set; }
        public int? MinistryId { get; set; }
        public long  PersonId { get; set; }
        public List<SelectListItem> MinistrySelectListItems { get; set; }
    }
}
