﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.Models
{
    public class AddressInfo
    {
        public int Id { get; set; }
        public string Country { get; set; }
        public string County { get; set; }
        public string SubCounty { get; set; }
        public int CountryId { get; set; }
        public int? CountyId { get; set; }
        public int? SubCountyId { get; set; }
        public string PostalAddress { get; set; }
    }
}
