﻿using Application.Common.Notifications;
using Application.Common.Services;
using Common.Domain.Messaging;
using MediatR;
using Persistance.LookUpRepo.UnitOfWork;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Common.Handlers
{

    public interface IUserNotificationService
    {
        Task NotifyUser(IdentityNotification notification);
    }


    public class UserNotificationService : IUserNotificationService
    {
        IEmailTemplateBuilder emailTemplateBuilder;
        ILookUpUnitOfWork unitOfWork;
        IMediator mediator;
        ILogger logger = Log.ForContext<UserNotificationService>();

        public UserNotificationService(IEmailTemplateBuilder _emailTemplateBuilder, ILookUpUnitOfWork _unitOfWork, IMediator _mediator)
        {
            emailTemplateBuilder = _emailTemplateBuilder;
            unitOfWork = _unitOfWork;
            mediator = _mediator;
        }

        public async Task NotifyUser(IdentityNotification notification)
        {
            try
            {
                var emailTemplate = unitOfWork.LookUpRepository.FindBy<MessageTemplate>(x => x.MessageTypeId ==notification.MessageType)
                    .FirstOrDefault();

                if (emailTemplate == null)
                {
                    logger.Error($"{notification.MessageType.ToString()} template not found");
                    return;
                }
                var body = await emailTemplateBuilder.BuildFromRazorTemplate(notification.MessageType.ToString(), emailTemplate.Body,
                    new DefaultEmailTemplate { Link = notification.CallBackUrl, Name = notification.UserName });

                await mediator.Publish(new SendEmailNotification
                {
                    Body = body,
                    EmailAddresses = new []{notification.EmailAddress},
                    Subject = emailTemplate.Subject
                });
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while processing notification type for email address {notification.EmailAddress}");
            }
        }
    }
}
