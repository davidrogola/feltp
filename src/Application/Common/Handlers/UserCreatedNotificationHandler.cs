﻿using Application.Common.Notifications;
using Application.Common.Services;
using Common.Domain.Messaging;
using MediatR;
using Persistance.LookUpRepo.UnitOfWork;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UserManagement.Domain;

namespace Application.Common.Handlers
{
    public class UserCreatedNotificationHandler : INotificationHandler<UserCreatedNotification>
    {
        IUserNotificationService userNotificationService;
        public UserCreatedNotificationHandler(IUserNotificationService _userNotificationService)
        {
            userNotificationService = _userNotificationService;
        }
        public async Task Handle(UserCreatedNotification notification, CancellationToken cancellationToken)
        {
            await userNotificationService.NotifyUser(notification);
        }
    }

    public class ForgotPasswordNotificationHandler : INotificationHandler<ForgotPasswordNotification>
    {
        IUserNotificationService userNotificationService;

        public ForgotPasswordNotificationHandler(IUserNotificationService _userNotificationService)
        {
            userNotificationService = _userNotificationService;
        }

        public async Task Handle (ForgotPasswordNotification notification, CancellationToken cancellationToken)
        {
            await userNotificationService.NotifyUser(notification);
        }
    }

    public class ResetPasswordNotificationHandler : INotificationHandler<ResetPasswordNotification>
    {
        IUserNotificationService userNotificationService;

        public ResetPasswordNotificationHandler(IUserNotificationService _userNotificationService)
        {
            userNotificationService = _userNotificationService;
        }

        public async Task Handle(ResetPasswordNotification notification, CancellationToken cancellationToken)
        {
            await userNotificationService.NotifyUser(notification);
        }
    }

    public class EmailChangeNotificationHandler : INotificationHandler<EmailChangeNotification>
    {
        IUserNotificationService userNotificationService;


        public EmailChangeNotificationHandler(IUserNotificationService _userNotificationService)
        {
            userNotificationService = _userNotificationService;

        }

        public async Task Handle(EmailChangeNotification notification, CancellationToken cancellationToken)
        {
            await userNotificationService.NotifyUser(notification);
        }
    }

    public class DefaultEmailTemplate
    {
        public string Name { get; set; }
        public string Link { get; set; }

    }
}
