﻿using Application.Common.Models;
using Application.Common.Models.Queries;
using AutoMapper;
using Common.Domain.Person;
using MediatR;
using Newtonsoft.Json;
using Persistance.LookUpRepo.UnitOfWork;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Common.Handlers
{
    public class GetPersonQueryHandler : IRequestHandler<GetPersonQuery, PersonViewModel>
    {
        ILookUpUnitOfWork unitOfWork;
        IMapper mapper;
        ILogger logger = Log.ForContext<GetPersonQueryHandler>();
        public GetPersonQueryHandler(ILookUpUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }

        public Task<PersonViewModel> Handle(GetPersonQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var includes = new Expression<Func<Person, object>>[]
                {
                    x =>x.ContactInformation,
                    x =>x.AddressInformation.Country,
                    x =>x.AddressInformation.County,
                    x =>x.AddressInformation.SubCounty,
                    x=>x.Cadre
                };

                var personData = !String.IsNullOrEmpty(request.IdNumber) ?
                           unitOfWork.LookUpRepository.FindByWithInclude(x => x.IdentificationNumber == request.IdNumber, includes) :
                           unitOfWork.LookUpRepository.FindByWithInclude(x => x.Id == request.PersonId, includes);

                if (!personData.Any())
                    return Task.FromResult(default(PersonViewModel));


                var person = personData.SingleOrDefault();

                var personViewModel = new PersonViewModel()
                {
                    ContactInfo = mapper.Map<ContactInfo>(person.ContactInformation),
                    AddressInfo = mapper.Map<AddressInfo>(person.AddressInformation),
                    BioDataInfo = mapper.Map<BioDataInformation>(person),
                    Cadre = person.Cadre?.Name
                };

                return Task.FromResult(personViewModel);
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while fetching person details with Id: { request.PersonId.ToString() }");
                throw;
            }
        }
    }

    public class GetPersonEmploymentHistoryRequestHandler : IRequestHandler<GetPersonEmploymentHistory, List<EmploymentDetail>>
    {
        ILookUpUnitOfWork unitOfWork;
        IMapper mapper;
        public GetPersonEmploymentHistoryRequestHandler(ILookUpUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }

        public Task<List<EmploymentDetail>> Handle(GetPersonEmploymentHistory request, CancellationToken cancellationToken)
        {
            var employmentHistoryDetails = unitOfWork.LookUpRepository.FindByWithInclude<EmploymentHistory>(x => x.PersonId == request.PersonId,
                x => x.Ministry).OrderByDescending(x=>x.Id).AsEnumerable();

            var employmentViewModel = mapper.Map<List<EmploymentDetail>>(employmentHistoryDetails);

            return Task.FromResult(employmentViewModel);
        }
    }

    public class GetPersonAcademicDetailQueryHandler : IRequestHandler<GetPersonAcademicDetail, List<AcademicDetail>>
    {
        ILookUpUnitOfWork unitOfWork;
        IMapper mapper;
        public GetPersonAcademicDetailQueryHandler(ILookUpUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<AcademicDetail>> Handle(GetPersonAcademicDetail request, CancellationToken cancellationToken)
        {

            var personAcademicHistory = unitOfWork.LookUpRepository.FindBy<AcademicHistoryView>(x => x.PersonId == request.PersonId).OrderByDescending(x=>x.Id).AsEnumerable();

            var academicHistoryModel = mapper.Map<List<AcademicDetail>>(personAcademicHistory);

            return Task.FromResult(academicHistoryModel);
        }
    }
}
