﻿using MediatR;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using Application.Common.Interface;
using Application.Common.Models;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Common.Handlers
{
    public class AddPersonCommandHandler : IRequestHandler<AddPersonCommand, long>
    {
        IPersonService personService;
        ILogger logger = Log.ForContext<AddPersonCommandHandler>();
        public AddPersonCommandHandler(IPersonService _personService)
        {
            personService = _personService;
        }
        public Task<long> Handle(AddPersonCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var personId = personService.CreatePerson(request);
                return Task.FromResult(personId);
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while creating person with IdNumber: { request.BioDataInfo.IdentificationNumber }");
                throw;
            }
        }
    }

    public class UpdatePersonBioDataCommandHandler : IRequestHandler<UpdatePersonBioDataCommand, long>
    {
        IPersonService personService;
        ILogger logger = Log.ForContext<AddPersonCommandHandler>();
        public UpdatePersonBioDataCommandHandler(IPersonService _personService)
        {
            personService = _personService;
        }
        public Task<long> Handle(UpdatePersonBioDataCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var personId = personService.UpdatePersonBioData(request);
                return Task.FromResult(personId);
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while creating person with IdNumber: { request.BioDataInfo.IdentificationNumber }");
                throw;
            }
        }
    }

    public class UpdatePersonContactInfoCommandHandler : IRequestHandler<UpdatePersonContactInfoCommand, long>
    {
        IPersonService personService;
        ILogger logger = Log.ForContext<AddPersonCommandHandler>();
        public UpdatePersonContactInfoCommandHandler(IPersonService _personService)
        {
            personService = _personService;
        }
        public Task<long> Handle(UpdatePersonContactInfoCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var personId = personService.UpdatePersonContactInfo(request);
                return Task.FromResult(personId);
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while creating person with MobileNumber: { request.ContactInfo.PrimaryMobileNumber }");
                throw;
            }
        }
    }

    public class AddPersionBioDataCommandHandler : IRequestHandler<BioDataInformation, long>
    {
        IPersonService personService;
        public AddPersionBioDataCommandHandler(IPersonService _personService)
        {
            personService = _personService;
        }
        public Task<long> Handle(BioDataInformation request, CancellationToken cancellationToken)
        {
            var personId = personService.AddPersonBioDataInformation(request);

            return Task.FromResult(personId);

        }
    }

    public class AddPersonAcademicDetailCommandHandler : IRequestHandler<AddPersonAcademicInfoCommand, long>
    {
        IPersonService personService;
        public AddPersonAcademicDetailCommandHandler(IPersonService _personService)
        {
            personService = _personService;
        }

        public Task<long> Handle(AddPersonAcademicInfoCommand request, CancellationToken cancellationToken)
        {
            var result = personService.AddPersonAcademicInformation(request.AcademicInfo);

            return Task.FromResult(result);
        }
    }

    public class AddPersonPersonEmploymentDetailCommandHandler : IRequestHandler<AddPersonEmploymentInfoCommand, long>
    {
        IPersonService personService;
        public AddPersonPersonEmploymentDetailCommandHandler(IPersonService _personService)
        {
            personService = _personService;
        }
        public Task<long> Handle(AddPersonEmploymentInfoCommand request, CancellationToken cancellationToken)
        {
            var result = personService.AddPersonEmploymentInformation(request.EmploymentInfo);

            return Task.FromResult(result);
        }
    }



}
