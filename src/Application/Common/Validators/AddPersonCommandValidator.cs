﻿using Application.Common.Models;
using Application.ResidentManagement;
using Common.Domain.Person;
using FluentValidation;
using Microsoft.Extensions.Configuration;
using Persistance.LookUpRepo.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Common.Validators
{
    public class AddPersonCommandValidator : AbstractValidator<AddPersonCommand>
    {
        ILookUpUnitOfWork lookUpUnitOfWork;
        public AddPersonCommandValidator(ILookUpUnitOfWork _lookUpUnitOfWork)
        {
            lookUpUnitOfWork = _lookUpUnitOfWork;

            RuleFor(x => x.BioDataInfo).Must((BioDataInformation info) =>
            {
                if (info == null)
                    return false;
                return true;
            }).WithMessage(ResidentManagementResource.BioDataInfoRequired);

            RuleFor(x => x.BioDataInfo.IdentificationNumber)
                .NotNull().WithMessage(ResidentManagementResource.IdNumberRequired);

            RuleFor(x => x.BioDataInfo.FirstName)
               .NotNull().WithMessage("Enter first name");

            RuleFor(x => x.BioDataInfo.LastName)
               .NotNull().WithMessage("Enter last name");

            RuleFor(x => x.BioDataInfo.CadreId)
               .NotEqual(0).WithMessage(ResidentManagementResource.CadreRequired);

            RuleFor(x => x.BioDataInfo.IdentificationNumber).Must((string idNo) =>
            {
                if (idNo == null)
                    return true;
                bool exists = lookUpUnitOfWork.LookUpRepository
                .FindBy<Person>(x => x.IdentificationNumber == idNo).Any();
                return !exists;
            }).WithMessage(ResidentManagementResource.DuplicateIdNumber);

            RuleFor(x => x.ContactInfo).Must((ContactInfo info) =>
            {
                if (info == null)
                    return false;
                return true;
            }).WithMessage(ResidentManagementResource.ContactInformationRequired);

            RuleFor(x => x.ContactInfo.PrimaryMobileNumber)
                .NotNull().WithMessage(ResidentManagementResource.PhoneNumberRequired);

            RuleFor(x => x.ContactInfo.PrimaryMobileNumber).Must((string phoneNo) =>
            {
                if (phoneNo == null)
                    return true;
                bool exists = lookUpUnitOfWork.LookUpRepository
                .FindBy<ContactInformation>(x => x.PrimaryMobileNumber == phoneNo).Any();
                return !exists;
            }).WithMessage(ResidentManagementResource.DuplicatePhoneNumber);

            RuleFor(x => x.ContactInfo.EmailAddress).Must((string email) =>
            {
                if (email == null)
                    return true;
                bool exists = lookUpUnitOfWork.LookUpRepository
                .FindBy<ContactInformation>(x => x.EmailAddress == email).Any();
                return !exists;
            }).WithMessage(ResidentManagementResource.DuplicateEmailAddress);


        }
    }
}
