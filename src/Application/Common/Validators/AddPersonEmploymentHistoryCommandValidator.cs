﻿using Application.Common.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.Validators
{
    public class AddPersonEmploymentHistoryCommandValidator : AbstractValidator<AddPersonEmploymentInfoCommand>
    {
        public AddPersonEmploymentHistoryCommandValidator()
        {
            RuleFor(x => x.EmploymentInfo).Custom((model, context) => {
                if (!model.MinistryId.HasValue && model.EmployedByMinistry)
                    context.AddFailure("Please select the ministry from the dropdown");

                if(!model.EmployedByMinistry && string.IsNullOrEmpty(model.EmployerName))
                    context.AddFailure("Please enter employer name");

                if(String.IsNullOrEmpty(model.EmploymentStartDate))
                    context.AddFailure("Enter the employment start year");

                if(String.IsNullOrEmpty(model.EmploymentEndDate) && !model.IsCurrentEmployer)
                    context.AddFailure("Enter the employment end year");

            });
            RuleFor(x => x.EmploymentInfo.CurrentPosition).NotEmpty().WithMessage("Postion name is required");
            RuleFor(x => x.EmploymentInfo.Designation).NotEmpty().WithMessage("Designation is required");
            RuleFor(x => x.EmploymentInfo.WorkStation).NotEmpty().WithMessage("Enter workstation name");
            RuleFor(x => x.EmploymentInfo.PersonnelNumber).NotEmpty().WithMessage("Enter your personel Number");
        }
    }
}
