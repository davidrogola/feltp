﻿using Application.Common.Models;
using Common.Domain.Person;
using FluentValidation;
using Persistance.LookUpRepo.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Common.Validators
{
    public class AddPersonAcademicInfoCommandValidator : AbstractValidator<AddPersonAcademicInfoCommand>
    {
        ILookUpUnitOfWork lookUpUnitOfWork;
        public AddPersonAcademicInfoCommandValidator(ILookUpUnitOfWork _lookUpUnitOfWork)
        {
            lookUpUnitOfWork = _lookUpUnitOfWork;

            RuleFor(x => x.AcademicInfo).Custom((model, context) =>
            {
                if (model.CourseNotFound && String.IsNullOrEmpty(model.CourseName))
                    context.AddFailure("Please enter the course name");

                if (model.UniversityNotFound && String.IsNullOrEmpty(model.University))
                    context.AddFailure("Please enter the university name");

                if (!model.UniversityNotFound && !model.UniversityId.HasValue)
                    context.AddFailure("Please select university");

                if (!model.CourseNotFound && !model.UniversityCourseId.HasValue)
                    context.AddFailure("Please select course");

                if (String.IsNullOrEmpty(model.YearOfGraduation))
                    context.AddFailure("Enter the year of graduation");

                if (!String.IsNullOrEmpty(model.CourseName))
                {
                    var courseExists = lookUpUnitOfWork.LookUpRepository
                      .FindBy<AcademicHistory>(x => x.CourseName == model.CourseName && x.PersonId == model.PersonId).Any();
                    if (courseExists)
                        context.AddFailure("Duplicate course already exists");
                }

                if (model.UniversityCourseId.HasValue)
                {
                    var universityCourseExists = lookUpUnitOfWork.LookUpRepository
                    .FindBy<AcademicHistory>(x => x.UniversityCourseId == model.UniversityCourseId.Value && x.PersonId == model.PersonId).Any();
                    if (universityCourseExists)
                        context.AddFailure("Duplicate course already exists");
                }

            });
        }

    }
}
