﻿using Common.Domain.Messaging;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.Notifications
{
    public class UserCreatedNotification : IdentityNotification, INotification
    {
        
    }

    public class ForgotPasswordNotification : IdentityNotification, INotification
    {

    }

    public class ResetPasswordNotification : IdentityNotification, INotification
    {

    }

    public class EmailChangeNotification : IdentityNotification, INotification
    {

    }

    public abstract class IdentityNotification
    {
        public string EmailAddress { get; set; }
        public string CallBackUrl { get; set; }
        public string Subject { get; set; }
        public string UserName { get; set; }
        public string EmailTemplateBody { get; set; }
        public MessageTypeEnum MessageType { get; set; }
    }


}
