﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.Notifications
{
    public class SendEmailNotification : INotification
    {
        public string[] EmailAddresses { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

    }
}
