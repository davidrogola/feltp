﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.SequenceGenerator
{
    public class SequenceTableInfo
    {
        public string SchemaQualifiedName { get; }
        public string ValueColumnName { get; }
        public string NameColumnName { get; }

        public SequenceTableInfo(string schemaQualifiedName, string nameColumnName, string valueColumnName)
        {
            SchemaQualifiedName = schemaQualifiedName;
            NameColumnName = nameColumnName;
            ValueColumnName = valueColumnName;
        }
    }
}
