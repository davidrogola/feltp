﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.SequenceGenerator
{
    public class SqlServerTables
    {
        public static readonly SequenceTableInfo HiSequenceInfo = 
            new SequenceTableInfo("feltp.hiloconfiguration", "Name", "Hi");

        public static readonly SequenceTableInfo LoSequenceInfo =
            new SequenceTableInfo("feltp.hiloconfiguration", "Name", "Lo");


        public static SequenceLoResolver CreateLoResolver(Func<MySqlConnection> connectionFactory)
        {
            if (connectionFactory == null)
                throw new ArgumentNullException(nameof(connectionFactory));
            return name =>
            {
                string query = "SELECT Lo FROM feltp.hiloconfiguration WHERE Name = @name";
                using (var con = connectionFactory())
                using (var command = new MySqlCommand(query, con))
                {
                    command.CommandType = System.Data.CommandType.Text;
                    command.Parameters.AddWithValue("@name", name);
                    command.Connection.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();
                            return reader.GetInt32(0);
                        }
                    }
                }
                throw new InvalidOperationException($"Sequence {name} Lo not found using query {query}");
            };

        }
    }
}
