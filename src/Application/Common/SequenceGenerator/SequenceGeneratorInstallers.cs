﻿using Microsoft.Extensions.DependencyInjection;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.SequenceGenerator
{
    public static class SequenceGeneratorInstallers
    {
        public static void AddServerHiloSequenceGenerator(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton(provider => 
            {
                return SqlServerTables.CreateLoResolver(provider.GetService<Func<MySqlConnection>>());
            });

            serviceCollection.AddSingleton<IDistributedSequenceGenerator>(provider =>
            {
                var sequenceHiGenerator = new SqlServerDistributedSequenceGenerator(provider.GetService<Func<MySqlConnection>>(),SqlServerTables.HiSequenceInfo);

                var sequenceLoResolver = provider.GetService<SequenceLoResolver>();
                return new HiLoDistributedSequenceGenerator(sequenceHiGenerator, sequenceLoResolver);
            });

           
        }
        public static void AddSequenceCodeGenerator(this IServiceCollection collection)
        {
            collection.AddSingleton<ICodeGenerator, SequenceBasedCodeGenerator>();
        }
    }
}
