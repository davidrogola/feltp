﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Common.SequenceGenerator
{
    /// <summary>
    /// I've defined this for DI purposes since I don't want to define an interface.
    /// A method I can use to get the Lo value of a sequence
    /// </summary>
    /// <param name="sequenceName"></param>
    /// <returns></returns>
    public delegate int SequenceLoResolver(string sequenceName);
    public class HiLoDistributedSequenceGenerator : IDistributedSequenceGenerator
    {
        readonly IDistributedSequenceGenerator hiGenerator;
        readonly ConcurrentDictionary<string, HiLoGenerator> generators = new ConcurrentDictionary<string, HiLoGenerator>();
        readonly Func<string, HiLoGenerator> generatorFactory;
        readonly SequenceLoResolver sequenceLoSource;

        public HiLoDistributedSequenceGenerator(IDistributedSequenceGenerator hiGenerator, SequenceLoResolver sequenceLoSource)
        {
            this.hiGenerator = hiGenerator ?? throw new ArgumentNullException(nameof(hiGenerator));
            this.sequenceLoSource = sequenceLoSource ?? throw new ArgumentNullException(nameof(sequenceLoSource));
            generatorFactory = AddGenerator;
        }

        public long Next(string sequenceName)
        {
            if (string.IsNullOrWhiteSpace(sequenceName))
                throw new ArgumentNullException(nameof(sequenceName));
            var generator = generators.GetOrAdd(sequenceName, generatorFactory);
            return generator.Next();
        }


        HiLoGenerator AddGenerator(string sequenceName)
        {
            return new HiLoGenerator(hiGenerator, sequenceName, sequenceLoSource(sequenceName));
        }


        private class HiLoGenerator
        {
            IDistributedSequenceGenerator hiGenerator;
            string sequenceName;
            long hi;
            int lo, accumulate;

            public HiLoGenerator(IDistributedSequenceGenerator hiGenerator, string sequenceName, int lo)
            {
                this.hiGenerator = hiGenerator;
                this.sequenceName = sequenceName;
                this.lo = lo;
                accumulate = lo + 1;
            }

            public long Next()
            {
                lock(this)
                {
                    if(accumulate > lo)
                    {
                        //if we have used up all the lo values then we
                        //request a new value and then we set the accumulator
                        //to 1
                        hi = hiGenerator.Next(sequenceName);
                        accumulate = 1;
                    }
                    //(hi * lo) + [1-lo] i.e. for lo = 5 we do
                    //hi == 0 => 0 + 1, 0 + 2, 0 + 3, 0 + 4, 0 + 5
                    //hi == 1 => 5 + 1, 5 + 2, 5 + 3, 5 + 4, 5 + 5
                    var value = (hi * lo) + accumulate;
                    accumulate++;
                    return value;
                }
            }
        }
    }
}
