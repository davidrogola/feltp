﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.SequenceGenerator
{
    public interface IDistributedSequenceGenerator
    {
        long Next(string sequenceName);
    }
}
