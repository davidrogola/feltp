﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Application.Common.SequenceGenerator
{
    public class SqlServerDistributedSequenceGenerator : IDistributedSequenceGenerator
    {
        SequenceTableInfo tableInfo;
        Func<MySqlConnection> connectionFactory;
        const string query = @"update_hilo_highvalue";

        public SqlServerDistributedSequenceGenerator(Func<MySqlConnection> connectionFactory, 
            SequenceTableInfo tableInfo)
        {
            this.tableInfo = tableInfo;
            this.connectionFactory = connectionFactory;
        }
        public long Next(string sequenceName)
        {
            using (var con = connectionFactory())
            using (var command = new MySqlCommand(query, con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@sequencename", sequenceName);
                command.Parameters["@sequencename"].Direction = ParameterDirection.Input;

                command.Connection.Open();

                using (var reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        return reader.GetInt64(0);
                    }
                }
            }
            return default(int);
        }
    }
}
