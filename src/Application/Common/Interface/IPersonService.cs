﻿using Application.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.Interface
{
    public interface IPersonService
    {
        long CreatePerson(AddPersonCommand addPersonCommand);
        long UpdatePersonBioData(UpdatePersonBioDataCommand updatePersonBioDataCommand);
        long UpdatePersonContactInfo(UpdatePersonContactInfoCommand updatePersonContactInfoCommand);
        long AddPersonBioDataInformation(BioDataInformation bioDataInformation);

        long AddPersonAcademicInformation(AcademicDetail academicInfo);

        long AddPersonEmploymentInformation(EmploymentDetail employmentInfo);
    }
}
