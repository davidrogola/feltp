﻿using Application.Common.SequenceGenerator;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common
{
    public class SequenceBasedCodeGenerator : ICodeGenerator
    {
        readonly IDistributedSequenceGenerator sequenceGenerator;
        IConfiguration configuration;

        public SequenceBasedCodeGenerator(IDistributedSequenceGenerator sequenceGenerator, 
            IConfiguration configuration)
        {
            this.configuration = configuration;
            this.sequenceGenerator = sequenceGenerator ?? throw new ArgumentNullException(nameof(sequenceGenerator));
        }

        public string Generate(string codeType)
        {
            var codeFormat = configuration[$"CodeTypes:{codeType}"];

            long sequenceNumber = sequenceGenerator.Next(codeType);

            return String.Format(codeFormat,sequenceNumber.ToString());

        }
    }
}
