﻿using Application.ProgramManagement.Services;
using MySql.Data.MySqlClient;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Application.Common.ActivityMonitor
{
    public class SemesterScheduleMonitoringService : IProgramActivityMonitoringService
    {
        IProgramManagementUnitOfWork unitOfWork;
        ILogger logger = Log.ForContext<SemesterScheduleMonitoringService>();
        ResidentCourseProgressUpdator residentCourseProgressUpdator;
        public SemesterScheduleMonitoringService(IProgramManagementUnitOfWork _unitOfWork, ResidentCourseProgressUpdator _residentCourseProgressUpdator)
        {
            unitOfWork = _unitOfWork;
            residentCourseProgressUpdator = _residentCourseProgressUpdator;
        }
        public void MonitorActivityElapsedPeriod()
        {
            try
            {
                var semesterSchedulesPendingStart = unitOfWork.ProgramManagementRepository.FindBy<SemesterSchedule>(x => x.Status == ProgressStatus.NotStarted &&  DateTime.Now.Date >= x.EstimatedStartDate.Date).ToList();


                foreach (var semesterSchedule in semesterSchedulesPendingStart)
                {
                    semesterSchedule.StartSemester();

                    unitOfWork.ProgramManagementRepository.Update(semesterSchedule);
                    unitOfWork.SaveChanges();

                    residentCourseProgressUpdator.UpdateResidentCourseProgress(semesterSchedule.Id, semesterSchedule.SemesterId, semesterSchedule.ProgramOfferId, ProgressStatus.InProgress);

                }

                var semesterSchedulesPendingCompletion = unitOfWork.ProgramManagementRepository.FindBy<SemesterSchedule>(x => x.Status == ProgressStatus.InProgress &&  DateTime.Now.Date >= x.EstimatedEndDate.Date && !x.ApprovalDate.HasValue).ToList();

                foreach (var schedule in semesterSchedulesPendingCompletion)
                {
                    schedule.InitiateSemesterCompletionRequest("System", "Semester end date elapsed");

                    if(!semesterSchedulesPendingStart.Contains(schedule))
                    unitOfWork.ProgramManagementRepository.Update(schedule);
                }
                unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

                logger.Error(ex,"An Error occured while processing semester schedule activity monitoring");
            }
        }

        
    }
}
