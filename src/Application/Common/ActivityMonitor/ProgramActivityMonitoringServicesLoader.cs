﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.ActivityMonitor
{
    public class ProgramActivityMonitoringServicesLoader : IProgamActivityMonitoringServiceLoader
    {
        IEnumerable<IProgramActivityMonitoringService> programActivityMonitoringServices;
        public ProgramActivityMonitoringServicesLoader(IEnumerable<IProgramActivityMonitoringService> _programActivityMonitoringServices)
        {
            programActivityMonitoringServices = _programActivityMonitoringServices;
        }

        public void Execute()
        {
            foreach (var monitoringService in programActivityMonitoringServices)
            {
                monitoringService.MonitorActivityElapsedPeriod();
            }
        }
    }

    public interface IProgamActivityMonitoringServiceLoader
    {
        void Execute();
    }
}
