﻿using Application.ProgramManagement.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.ActivityMonitor
{
    public class ProgramCompletionMonitoringService : IProgramActivityMonitoringService
    {
        IGenerateGraduandsList graduandsListGenerator;
       
        public ProgramCompletionMonitoringService(IGenerateGraduandsList _graduandsListGenerator)
        {
            graduandsListGenerator = _graduandsListGenerator;
        }

        public void MonitorActivityElapsedPeriod()
        {
            graduandsListGenerator.GenerateGraduands();
        }
    }
}
