﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Application.Common.ActivityMonitor.Installers
{
    public static class ProgramActivityMonitorInstallers
    {
        public static void AddProgramActivityMonitors(this IServiceCollection service)
        {
            var type = typeof(IProgramActivityMonitoringService);
            var programMonitors = Assembly.GetAssembly(typeof(IProgramActivityMonitoringService)).GetTypes()
                .Where(x => type.IsAssignableFrom(x) && !x.IsInterface).ToList();

            foreach (var loader in programMonitors)
            {
                service.AddScoped(type, loader);
            }
        }
    }
}
