﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.ActivityMonitor
{
    public interface IProgramActivityMonitoringService
    {
        void MonitorActivityElapsedPeriod(); // start or stop of an activity

    }
}
