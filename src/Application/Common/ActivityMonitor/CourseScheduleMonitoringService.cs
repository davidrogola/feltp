﻿using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Common.ActivityMonitor
{
    public class SemesterScheduleItemsMonitoringService : IProgramActivityMonitoringService
    {
        IProgramManagementUnitOfWork unitOfWork;
        ILogger logger = Log.ForContext<SemesterScheduleItemsMonitoringService>();
        public SemesterScheduleItemsMonitoringService(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public void MonitorActivityElapsedPeriod()
        {
            try
            {
                var scheduleItemsNotStarted = unitOfWork.ProgramManagementRepository.FindBy<SemesterScheduleItem>(x => x.Status == ProgressStatus.NotStarted && DateTime.Now.Date >= x.StartDate.Date).ToList();

                foreach (var scheduleItem in scheduleItemsNotStarted)
                {
                    scheduleItem.Start();
                    unitOfWork.ProgramManagementRepository.Update(scheduleItem);
                }

                var scheduleItemsPendingCompletion = unitOfWork.ProgramManagementRepository.FindBy<SemesterScheduleItem>(x => x.Status == ProgressStatus.InProgress &&  DateTime.Now.Date >= x.EndDate.Date ).ToList();

                foreach (var item in scheduleItemsPendingCompletion)
                {
                    item.Stop();
                    if (!scheduleItemsNotStarted.Contains(item))
                        unitOfWork.ProgramManagementRepository.Update(item);
                }

                unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

                logger.Error(ex, "An error occured while processing semester schedule items activity monitoring");
            }
        }
    }
}
