﻿using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Common.ActivityMonitor
{
    public class FieldPlacementActivityMonitoringService : IProgramActivityMonitoringService
    {
        IResidentManagementUnitOfWork unitOfWork;
        IProgramManagementUnitOfWork programUnitOfWork;
        public FieldPlacementActivityMonitoringService(IResidentManagementUnitOfWork _unitOfWork,
            IProgramManagementUnitOfWork _programUnitOfWork)
        {
            unitOfWork = _unitOfWork;
            programUnitOfWork = _programUnitOfWork;
        }
        public void MonitorActivityElapsedPeriod()
        {
            var fieldPlacementActiviesPendingStart = unitOfWork.ResidentManagementRepository.FindBy<FieldPlacement>(x => x.ProgressStatus == PlacementActivityStatus.NotStarted && DateTime.Now.Date >= x.StartDate.Date).ToList();

            var trackedEligiblityItemIds = new List<long>();
            var trackedPlacementActivityIds = new List<long>();
            var trackedResidentActivityIds = new List<long>();
            var trackedResidentDeliverableIds = new List<long>();

            foreach (var placementActivity in fieldPlacementActiviesPendingStart)
            {
                placementActivity.SetProgressStatus(PlacementActivityStatus.Ongoing);

                var residentFieldPlacementActivity = unitOfWork.ResidentManagementRepository.Get<ResidentFieldPlacementActivity>(placementActivity.ResidentFieldPlacementActivityId);

                residentFieldPlacementActivity.SetProgressStatus(PlacementActivityStatus.Ongoing);

                if (!trackedPlacementActivityIds.Contains(placementActivity.Id))
                {
                    unitOfWork.ResidentManagementRepository.Update(placementActivity);
                    trackedPlacementActivityIds.Add(placementActivity.Id);
                }
                if (!trackedResidentActivityIds.Contains(residentFieldPlacementActivity.Id))
                {
                    unitOfWork.ResidentManagementRepository.Update(residentFieldPlacementActivity);
                    trackedResidentActivityIds.Add(placementActivity.Id);

                }
            }

            var fieldPlacementActivitiesPendingCompletion = unitOfWork.ResidentManagementRepository
                  .FindBy<FieldPlacement>(x => (x.ProgressStatus == PlacementActivityStatus.Ongoing || x.ProgressStatus == PlacementActivityStatus.DeliverablePendingSubmission) && DateTime.Now.Date >= x.EndDate.Date).ToList();

            foreach (var placementActivity in fieldPlacementActivitiesPendingCompletion)
            {
                var activityDeliverables = unitOfWork.ResidentManagementRepository.FindBy<ResidentDeliverable>
                    (x => x.ResidentFieldPlacementActivityId == placementActivity.ResidentFieldPlacementActivityId &&
                    x.ProgressStatus == DeliverableStatus.Pending || x.ProgressStatus == DeliverableStatus.Overdue).ToList();

                var placementActivityStatus = activityDeliverables.Any() == true ? PlacementActivityStatus.DeliverablePendingSubmission : PlacementActivityStatus.Completed;

                var residentFieldPlacementActivity = unitOfWork.ResidentManagementRepository.Get<ResidentFieldPlacementActivity>(placementActivity.ResidentFieldPlacementActivityId);
                placementActivity.SetProgressStatus(placementActivityStatus);
                residentFieldPlacementActivity.SetProgressStatus(placementActivityStatus);


                foreach (var deliverable in activityDeliverables)
                {
                    if (!trackedResidentDeliverableIds.Contains(deliverable.Id))
                    {
                        unitOfWork.ResidentManagementRepository.Update(deliverable);
                        trackedResidentDeliverableIds.Add(deliverable.Id);
                    }

                }

                if (placementActivityStatus == PlacementActivityStatus.Completed)
                {
                    var enrollmentId = unitOfWork.ResidentManagementRepository.FindBy<ResidentFieldPlacementActivityView>(x => x.Id == placementActivity.ResidentFieldPlacementActivityId).SingleOrDefault().ProgramEnrollmentId;

                    var programEnrollment = programUnitOfWork.ProgramManagementRepository.Get<ProgramEnrollment>(enrollmentId);

                    if (programEnrollment == null)
                        continue;

                    var residentGraduationInfo = unitOfWork.ResidentManagementRepository.FindBy<ResidentGraduationInfoView>(x => x.ResidentId == placementActivity.ResidentId && x.ProgramOfferId == programEnrollment.ProgramOfferId).SingleOrDefault();

                    if (residentGraduationInfo == null)
                        continue;

                    var fieldPlacementEligiblityItem = unitOfWork.ResidentManagementRepository.FindBy<GraduateEligibilityItem>(x => x.GraduateId == residentGraduationInfo.Id && x.EligibiltyType == EligibilityType.FieldActivitiesParticipated).SingleOrDefault();

                    if (fieldPlacementEligiblityItem == null)
                        continue;

                    fieldPlacementEligiblityItem.EvaluateNonPassmarkEligiblityItem();


                    if (!trackedEligiblityItemIds.Contains(fieldPlacementEligiblityItem.Id))
                    {
                        unitOfWork.ResidentManagementRepository.Update(fieldPlacementEligiblityItem);
                        trackedEligiblityItemIds.Add(fieldPlacementEligiblityItem.Id);
                    }

                }

                if (!trackedPlacementActivityIds.Contains(placementActivity.Id))
                {
                    unitOfWork.ResidentManagementRepository.Update(placementActivity);
                    trackedPlacementActivityIds.Add(placementActivity.Id);
                }
                if (!trackedResidentActivityIds.Contains(residentFieldPlacementActivity.Id))
                {
                    unitOfWork.ResidentManagementRepository.Update(residentFieldPlacementActivity);
                    trackedResidentActivityIds.Add(placementActivity.Id);

                }

                unitOfWork.SaveChanges();

            }
        }
    }
}
