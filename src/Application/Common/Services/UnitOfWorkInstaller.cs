﻿using Microsoft.Extensions.DependencyInjection;
using Persistance.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Application.Common
{
    public static class UnitOfWorkInstaller
    {
        public static void AddUnitOfWorkImplementations(this IServiceCollection services)
        {
            var unitOfWorkType = typeof(IUnitOfWork);

            var unitOfWorkInterfaceImplementations = Assembly.GetAssembly(unitOfWorkType).GetTypes()
                .Where(x =>x.IsInterface)
                .Where(x=> unitOfWorkType.IsAssignableFrom(x))
                .Where(x=>x.Name != unitOfWorkType.Name)
                .ToList();

            foreach (var unitOfWorkImplementation in unitOfWorkInterfaceImplementations)
            {
                var unitOfWorkImplementingClass = Assembly.GetAssembly(unitOfWorkImplementation).GetTypes()
                .Where(x => !x.IsInterface)
                .Where(x => unitOfWorkImplementation.IsAssignableFrom(x)).ToList();

                foreach (var implementingClass in unitOfWorkImplementingClass)
                {
                    services.AddScoped(unitOfWorkImplementation, implementingClass);
                }
            }
        }
    }
}
