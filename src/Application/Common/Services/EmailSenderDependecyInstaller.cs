﻿using Application.Common.Handlers;
using Microsoft.Extensions.DependencyInjection;

namespace Application.Common.Services
{
    public static class EmailSenderDependecyInstaller
    {
        public static void AddEmailSender(this IServiceCollection services)
        {
            services.AddSingleton<IEmailSender, EmailSender>();
            services.AddRazorEngine();
            services.AddScoped<IEmailTemplateBuilder, EmailTemplateBuilder>();
        }
    }


    public static class UserNotifcationHandlerSevice
    {
        public static void AddUserNotifierService(this IServiceCollection services)
        {
            services.AddScoped<IUserNotificationService, UserNotificationService>();
        }
    }
}
