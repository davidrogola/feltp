﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Application.Common.Services
{
    public static class DateTimeParser
    {
        public static DateTime ToLocalDate(this string strDate)
        {
            if (String.IsNullOrEmpty(strDate))
                return new DateTime();

            var systemDateFormat = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;

            var ci = new CultureInfo("fr-FR");
            //format differs on mac and pc
            var formats = new[] { "dd/MM/yyyy" , systemDateFormat }
                    .Union(ci.DateTimeFormat.GetAllDateTimePatterns()).ToArray();

            return DateTime.ParseExact(strDate, formats, ci, DateTimeStyles.AssumeLocal);
 
        }

        public static DateTime CalculateEndDate(this DateTime startDate, int durationInWeeks)
        {
            var endDate = startDate.AddDays(GetDaysInWeekDuration(durationInWeeks));
            return endDate;
        }

        public static int GetDaysInWeekDuration(int durationInWeeks)
        {
            int daysInWeek = 7;
           return durationInWeeks * daysInWeek;
        }
    }
}
