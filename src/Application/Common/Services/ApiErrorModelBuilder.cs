﻿using Application.Common.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Common.Services
{
    public static class ApiErrorModelBuilder
    {
        public static List<Error> BuildErrorModelFromIdentityErrors(IEnumerable<IdentityError> identityErrors)
        {
            return identityErrors.Select(x => new Error
            {
                ErrorCode = x.Code,
                ErrorMessage = x.Description
            }).ToList();
        }

        public  static List<Error> BuildModelFromModelStateErrors(IEnumerable<ModelError> modelStateErrors)
        {
            return modelStateErrors.Select(x => new Error
            {
                ErrorCode = x.ErrorMessage,
                ErrorMessage = x.ErrorMessage
            }).ToList();
        }
    }
}
