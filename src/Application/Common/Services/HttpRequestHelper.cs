﻿using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using UserManagement.Services;

namespace Application.Common.Services
{
    public class HttpRequestHelper
    {
        ILogger logger = Log.ForContext<HttpRequestHelper>();
        IConfiguration configuration;
        AccessTokenGenerator accessTokenGenerator;
        int numberOfRetries = 0;
        IConfigurationSection apiResourcesConfigSection;
        public HttpRequestHelper(IConfiguration _configuration, AccessTokenGenerator _accessTokenGenerator)
        {
            configuration = _configuration;
            accessTokenGenerator = _accessTokenGenerator;
            numberOfRetries = Convert.ToInt16(configuration["IdentityConfiguration:NumberofRetries"]);
            apiResourcesConfigSection = configuration.GetSection("IdentityConfiguration:ApiResources");
        }

        public async Task<HttpResponseMessage> SendGetRequestAsync(string resourceName, string requestUri,TokenGenerationMode  tokenGenerationMode = TokenGenerationMode.Client_Credentials, string token = null)
        {
            int retryCounter = 1;
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.InternalServerError);

            var resourceEndpoint = apiResourcesConfigSection[$"{resourceName}-EndPoint"];

            var accessToken = tokenGenerationMode == TokenGenerationMode.Client_Credentials ? await accessTokenGenerator.GenerateAccessTokenUsingClientCredentails(resourceName) : token;

            var uri = new Uri(resourceEndpoint);

            while (retryCounter <= numberOfRetries && !httpResponseMessage.IsSuccessStatusCode)
            {
                try
                {
                    var httpClient = new HttpClient()
                    {
                        BaseAddress = uri
                    };
                    httpClient.SetBearerToken(accessToken);
                    httpResponseMessage = await httpClient.GetAsync(requestUri).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    logger.Error(ex, $"An error occured while sending a request to {uri.ToString()}/{requestUri} on retry number {retryCounter}");
                }
                retryCounter++;
            }
            return httpResponseMessage;
        }

        public async Task<HttpResponseMessage> SendPostJsonRequestAsync(string resourceName, string requestUri,
            string jsonContent, TokenGenerationMode tokenGenerationMode = TokenGenerationMode.Client_Credentials, string token = null)
        {
            int retryCounter = 1;
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.InternalServerError);

            var numberOfRetries = Convert.ToInt16(configuration["IdentityConfiguration:NumberofRetries"]);

            var accessToken = tokenGenerationMode == TokenGenerationMode.Client_Credentials ? await accessTokenGenerator.GenerateAccessTokenUsingClientCredentails(resourceName) : token;

            var resourceEndpoint = apiResourcesConfigSection[$"{resourceName}-EndPoint"];
            var uri = new Uri(resourceEndpoint);

            while (retryCounter <= numberOfRetries && !httpResponseMessage.IsSuccessStatusCode)
            {
                try
                {
                    var httpClient = new HttpClient()
                    {
                        BaseAddress = uri
                    };

                    httpClient.SetBearerToken(accessToken);
                    httpResponseMessage = await httpClient.PostAsync(requestUri,
                       new StringContent(jsonContent, Encoding.UTF8, "application/json")).ConfigureAwait(false);

                    return httpResponseMessage;
                }
                catch (Exception ex)
                {
                    logger.Error(ex, $"An error occured while sending a request to {uri.ToString()}/{requestUri} on retry number {retryCounter}");
                }
                retryCounter++;
            }
            return httpResponseMessage;
        }

        public async Task<HttpResponseMessage> SendPostRequestAsync(string resourceName, string requestUri,
            HttpContent httpContent, TokenGenerationMode tokenGenerationMode = TokenGenerationMode.Client_Credentials, string token = null)
        {
            int retryCounter = 1;
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.InternalServerError);

            var numberOfRetries = Convert.ToInt16(configuration["IdentityConfiguration:NumberofRetries"]);

            var accessToken = tokenGenerationMode == TokenGenerationMode.Client_Credentials ? await accessTokenGenerator.GenerateAccessTokenUsingClientCredentails(resourceName) : token;

            var resourceEndpoint = apiResourcesConfigSection[$"{resourceName}-EndPoint"];
            var uri = new Uri(resourceEndpoint);

            while (retryCounter <= numberOfRetries && !httpResponseMessage.IsSuccessStatusCode)
            {
                try
                {
                    var httpClient = new HttpClient()
                    {
                        BaseAddress = uri
                    };

                    httpClient.SetBearerToken(accessToken);
                    httpResponseMessage = await httpClient.PostAsync(requestUri, httpContent).ConfigureAwait(false); ;

                    return httpResponseMessage;
                }
                catch (Exception ex)
                {
                    logger.Error(ex, $"An error occured while sending a request to {uri.ToString()}/{requestUri} on retry number {retryCounter}");
                }
                retryCounter++;
            }
            return httpResponseMessage;
        }


    }
}
