﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using RazorLight;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using UserManagement.Domain;
using UserManagement.Services;
using System.Text.Encodings.Web;
using Serilog;

namespace Application.Common.Services
{

    public interface IEmailSender
    {
        void SendEmail(string body, string subject, string []recipients);

    }

    public interface IEmailTemplateBuilder
    {
        Task<string> BuildFromRazorTemplate<T>(string templateKey, string emailTemplate, T model) where T : class;
    }

    public class EmailTemplateBuilder : IEmailTemplateBuilder
    {
        RazorLightEngine razorLightEngine;

        public EmailTemplateBuilder(RazorLightEngine _razorLightEngine)
        {
            razorLightEngine = _razorLightEngine;
        }
        public async Task<string> BuildFromRazorTemplate<T>(string templateKey,string emailTemplate, T model) where T : class
        {
            string body = await razorLightEngine.CompileRenderAsync(templateKey, emailTemplate, model, typeof(T));
            return body;
        }
    }

    public class EmailSender : IEmailSender
    {
        IConfiguration configuration;
        ILogger logger = Log.ForContext<EmailSender>();
        public EmailSender(IConfiguration _configuration)
        {
            configuration = _configuration;
        }

        public async void SendEmail(string body, string subject, string [] recipients)
        {
            var emailConfig = configuration.GetSection("EmailSender");

            using (var smtpClient = new SmtpClient(emailConfig["Host"], Convert.ToInt16(emailConfig["Port"])))
            {
                smtpClient.UseDefaultCredentials = false;
                smtpClient.EnableSsl = true;
                smtpClient.Credentials = new NetworkCredential(emailConfig["Email"], emailConfig["Password"]);

                MailMessage mailMessage = new MailMessage
                {
                    From = new MailAddress(emailConfig["Email"])
                };
                foreach (var recipient in recipients)
                    mailMessage.To.Add(recipient);
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.Subject = subject;
                try
                {
                    await smtpClient.SendMailAsync(mailMessage);
                }
                catch (Exception ex)
                {
                    Console.Error.Write(ex);
                }
            }
        }
    }

    public class EmailTemplateFinder
    {
        ConcurrentDictionary<EmailType, EmailTemplateViewModel> emailTemplatesDict;
        public EmailTemplateFinder(ConcurrentDictionary<EmailType, EmailTemplateViewModel> _emailTemplatesDict)
        {
            emailTemplatesDict = _emailTemplatesDict;
        }

        public EmailTemplateViewModel GetEmailTemplate(EmailType emailType)
        {
            return emailTemplatesDict.SingleOrDefault(x => x.Key == emailType).Value;
        }
    }

}
