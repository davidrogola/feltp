﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Application.Common.Services
{
    public static class DispalyAttributeFetcher
    {
        public static string GetDisplayAttributeValue(Type type, string memberName)
        {
            var attributeInfo = type.GetMember(memberName).SingleOrDefault().GetCustomAttributes(typeof(DisplayAttribute), false)
                                 .Cast<DisplayAttribute>().FirstOrDefault();
            if (attributeInfo == null)
                return memberName;
            return attributeInfo.Name;
        }



    }
}
