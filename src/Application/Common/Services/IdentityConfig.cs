﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.Services
{
    public static class IdentityConfig
    {
        public static string GetIdentityServerEndPoint(IConfiguration configuration)
        {
            return configuration["IdentityConfiguration:IdentityProvider-Endpoint"];
        }
    }
}
