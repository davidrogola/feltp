﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Persistance.FacultyManagement.Database;
using Persistance.LookUpRepo.Database;
using Persistance.LookUpRepo.UnitOfWork;
using Persistance.ProgramManagement.Database;
using Persistance.ResidentManagement.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.Services
{
    public static class DatabaseContextInstallerService
    {
        public static void AddFeltpPersistanceStores(this IServiceCollection services, IConfiguration configuration)
        {
            var feltpConnectionString = configuration.GetConnectionString("feltp");

            services.AddDbContext<ProgramManagementDbContext>(
                options => options.UseMySql(feltpConnectionString));

            services.AddDbContext<ResidentManagementDbContext>(
                options => options.UseMySql(feltpConnectionString));

            services.AddDbContext<FacultyManagementDbContext>(
                options => options.UseMySql(feltpConnectionString));

            services.AddDbContext<LookUpDbContext>(
               options => options.UseMySql(feltpConnectionString));
        }
    }


    public static class LookupDbContextInstaller
    {
        public static void AddLookUpDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            var connString = configuration.GetConnectionString("feltp");
            services.AddDbContext<LookUpDbContext>(
              options => options.UseMySql(connString));

            services.AddScoped<ILookUpUnitOfWork, LookUpUnitOfWork>();
        }
    }
}
