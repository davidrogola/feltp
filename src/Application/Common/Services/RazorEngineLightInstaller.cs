﻿using Microsoft.Extensions.DependencyInjection;
using RazorLight;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.Services
{

    public static class RazorEngineLightInstaller
    {
        public static void AddRazorEngine(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton(provider =>
            {
                return new RazorLightEngineBuilder().UseMemoryCachingProvider()
                 .Build();

            });
        }
    }
}
