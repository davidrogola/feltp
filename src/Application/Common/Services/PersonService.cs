﻿using Application.Common.Interface;
using Application.Common.Models;
using AutoMapper;
using Common.Domain.Address;
using Common.Domain.Person;
using Microsoft.Extensions.Configuration;
using Persistance.LookUpRepo.UnitOfWork;
using Serilog;
using System;
using System.Linq;

namespace Application.Common.Services
{
    public class PersonService : IPersonService
    {
        ILookUpUnitOfWork unitOfWork;
        IMapper mapper;
        ILogger logger = Log.ForContext<PersonService>();

        public PersonService(ILookUpUnitOfWork _unitOfWork, IMapper _mapper, IConfiguration _configiration)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }

        public long CreatePerson(AddPersonCommand addPersonCommand)
        {
            try
            {
                var person = mapper.Map<Person>(addPersonCommand.BioDataInfo);

                person.DateOfBirth = addPersonCommand.BioDataInfo.StrDateOfBirth.ToLocalDate();

                unitOfWork.LookUpRepository.Add(person);

                var contactInfo = mapper.Map<ContactInformation>(addPersonCommand.ContactInfo);
                contactInfo.SetPerson(person.Id);
                unitOfWork.LookUpRepository.Add(contactInfo);

                var countyId = (addPersonCommand.AddressInfo.CountyId.HasValue && addPersonCommand.AddressInfo.CountyId != 0) ? addPersonCommand.AddressInfo.CountyId : null;
                var subCountyId = (addPersonCommand.AddressInfo.SubCountyId.HasValue && addPersonCommand.AddressInfo.SubCountyId != 0) ? addPersonCommand.AddressInfo.SubCountyId : null;

                var addressInfo = new AddressInformation(addPersonCommand.AddressInfo.CountryId, countyId, subCountyId, addPersonCommand.AddressInfo.PostalAddress, person.Id);
                unitOfWork.LookUpRepository.Add(addressInfo);


                var academicHistory = mapper.Map<AcademicHistory>(addPersonCommand.AcademicDetail);
                if (academicHistory != null)
                {
                    academicHistory.SetPerson(person.Id);
                    unitOfWork.LookUpRepository.Add(academicHistory);
                }

                var employmentHistory = mapper.Map<EmploymentHistory>(addPersonCommand.EmploymentDetail);
                if (employmentHistory != null)
                {
                    employmentHistory.SetPerson(person.Id);
                    unitOfWork.LookUpRepository.Add(employmentHistory);
                }

                unitOfWork.SaveChanges();

                return person.Id;
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while creating person with IdNumber {addPersonCommand.BioDataInfo.IdentificationNumber}");
                throw;
            }
        }

        public long UpdatePersonBioData(UpdatePersonBioDataCommand updatePersonBioDataCommand)
        {
            try
            {
                var person = mapper.Map<Person>(updatePersonBioDataCommand.BioDataInfo);

                person.DateOfBirth = updatePersonBioDataCommand.BioDataInfo.StrDateOfBirth.ToLocalDate();

                unitOfWork.LookUpRepository.Update(person);

                unitOfWork.SaveChanges();

                return person.Id;
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while creating person with IdNumber {updatePersonBioDataCommand.BioDataInfo.IdentificationNumber}");
                throw;
            }
        }

        public long UpdatePersonContactInfo(UpdatePersonContactInfoCommand updatePersonContactInfoCommand)
        {
            try
            {

                var contactInfo = unitOfWork.LookUpRepository
                .FindBy<ContactInformation>(x => x.Id == updatePersonContactInfoCommand.ContactInfo.Id).SingleOrDefault();

                contactInfo.UpdateContactInfo(updatePersonContactInfoCommand.ContactInfo.EmailAddress,
                    updatePersonContactInfoCommand.ContactInfo.PrimaryMobileNumber,
                    updatePersonContactInfoCommand.ContactInfo.AlternateMobileNumber);

                unitOfWork.LookUpRepository.Update(contactInfo);

                var addressInfo = unitOfWork.LookUpRepository
                    .FindBy<AddressInformation>(x => x.Id == updatePersonContactInfoCommand.AddressInfo.Id).SingleOrDefault();

                addressInfo.UpdateAddressInfo(updatePersonContactInfoCommand.AddressInfo.CountryId,
                    updatePersonContactInfoCommand.AddressInfo.CountyId,
                    updatePersonContactInfoCommand.AddressInfo.SubCountyId,
                    updatePersonContactInfoCommand.AddressInfo.PostalAddress);

                unitOfWork.LookUpRepository.Update(addressInfo);

                unitOfWork.SaveChanges();

                return addressInfo.PersonId;

            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while Updating person Contact Info with MobileNumber {updatePersonContactInfoCommand.ContactInfo.PrimaryMobileNumber}");
                throw;
            }
        }

        public long AddPersonBioDataInformation(BioDataInformation bioDataInformation)
        {
            try
            {
                var person = mapper.Map<Person>(bioDataInformation);

                person.DateOfBirth = bioDataInformation.StrDateOfBirth.ToLocalDate();

                unitOfWork.LookUpRepository.Add(person);

                unitOfWork.SaveChanges();

                return person.Id;
            }
            catch (Exception ex)
            {

                logger.Error(ex, $"An error occured while adding person bio data information with IdNumber: {bioDataInformation.IdentificationNumber}");
                return default(long);
            }

        }

        public long AddPersonAcademicInformation(AcademicDetail academicInfo)
        {
            try
            {
                var academicHistory = new AcademicHistory();

                academicHistory.SetPerson(academicInfo.PersonId);
                academicHistory.SetUniversityInfo(academicInfo.UniversityId, academicInfo.University);

                var educationLevel = academicInfo.CourseNotFound ? academicInfo.HighestEducationLevel : Enum.Parse(typeof(EducationLevel), academicInfo.StrEducationLevel);

                academicHistory.SetCourseName(academicInfo.UniversityCourseId, academicInfo.CourseName, (EducationLevel)educationLevel, academicInfo.YearOfGraduation);

                unitOfWork.LookUpRepository.Add(academicHistory);
                unitOfWork.SaveChanges();

                return academicHistory.Id;
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while adding  academic information for person Id: {academicInfo.PersonId}");
                return default(long);
            }
        }

        public long AddPersonEmploymentInformation(EmploymentDetail employmentInfo)
        {
            try
            {
                var employmentHistory = mapper.Map<EmploymentHistory>(employmentInfo);

                employmentHistory.SetEmploymentInformation(employmentInfo.EmploymentStartDate, employmentInfo.EmploymentEndDate, employmentInfo.IsCurrentEmployer);

                unitOfWork.LookUpRepository.Add(employmentHistory);

                unitOfWork.SaveChanges();
                return employmentHistory.Id;
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while adding  academic information for person Id: {employmentInfo.PersonId}");
                return default(long);
            }

        }
    }
}
