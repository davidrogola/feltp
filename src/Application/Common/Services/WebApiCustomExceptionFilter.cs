﻿using Application.Common.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Application.Common.Services
{
    public class WebApiCustomExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            String message = String.Empty;
            var statusCode = HttpStatusCode.InternalServerError;

            HttpResponse response = context.HttpContext.Response;
            response.ContentType = "application/json";

            var error = new Error
            {
                ErrorCode = statusCode.ToString(),
                ErrorMessage = context.Exception.Message
            };

            var apiResponse = new ApiResponse(message, null, false)
            {
                Errors = new List<Error>()
            };
            apiResponse.Errors.Add(error);

            response.WriteAsync(JsonConvert.SerializeObject(apiResponse));
        }
    }
}
