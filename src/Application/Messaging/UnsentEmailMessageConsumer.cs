﻿using Application.Common.Services;
using Common.Domain.Messaging;
using FELTP.Infrastructure.Task;
using Persistance.LookUpRepo.UnitOfWork;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Messaging
{
    public class UnsentEmailMessageConsumer : IConsumer<UnSentEmailItem>
    {
        IEmailSender emailSender;
        IReserveItemForAction<UnSentEmailItem, EmailReserveResult> unsentEmailReserver;
        ILookUpUnitOfWork unitOfWork;
        ILogger logger = Log.ForContext<UnsentEmailMessageConsumer>();

        public UnsentEmailMessageConsumer(IEmailSender _emailSender,
            IReserveItemForAction<UnSentEmailItem, EmailReserveResult> _unsentEmailReserver, ILookUpUnitOfWork _unitOfWork)
        {
            emailSender = _emailSender;
            unsentEmailReserver = _unsentEmailReserver;
            unitOfWork = _unitOfWork; 
        }

        public void Consume(UnSentEmailItem task)
        {
            try
            {
                var emailReservationResult = unsentEmailReserver.Reserve(task);

                if (!emailReservationResult.WasSuccesfull())
                    return;

                emailSender.SendEmail(emailReservationResult.Body, emailReservationResult.Subject, new []{ emailReservationResult.Email});

                var outboundMail = unitOfWork.LookUpRepository.Get<OutgoingMailMessage>(emailReservationResult.OutGoingEmailMessageId);
                outboundMail.MarkAsCompleted(MessageStatus.Sent);
                unsentEmailReserver.MarkAsComplete(emailReservationResult, false);

                unitOfWork.LookUpRepository.Update(outboundMail);
                unitOfWork.SaveChanges();

                return;
            }
            catch (Exception ex)
            {
                logger.Error(ex,$"An error occured while sending email with Id {task.OutGoingMailMessageId}");
            }
        }


    }
}
