﻿using Application.Common.Notifications;
using Application.Common.Services;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Messaging
{
    public class SendEmailNotificationHandler : INotificationHandler<SendEmailNotification>
    {
        IEmailSender emailSender;
        public SendEmailNotificationHandler(IEmailSender _emailSender)
        {
            emailSender = _emailSender;
        }
        public Task Handle(SendEmailNotification notification, CancellationToken cancellationToken)
        {
            emailSender.SendEmail(notification.Body, notification.Subject, notification.EmailAddresses);
            return Task.FromResult(0);
        }
    }
}
