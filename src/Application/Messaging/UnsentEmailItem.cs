﻿using FELTP.Infrastructure.Task;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Messaging
{
    public class UnSentEmailItem
    {
        public long OutGoingMailMessageId { get; set; }
    }

    public class EmailReserveResult  : IReservationResult
    {
        public bool Successful { get; set; }
        public long OutGoingEmailMessageId { get; set; }
        public string Body { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        public DateTime ReservedDate { get; set; }
        public DateTime ReservedToDate { get; set; }

        public bool WasSuccesfull()
        {
            return Successful;
        }
    }
}
