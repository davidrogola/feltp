﻿using FELTP.Infrastructure.Task;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Messaging
{
    public static class TasksProducerInstaller
    {
        public static void AddTaskProducerService(this IServiceCollection services)
        {
            services.AddScoped<ITasksProducer<UnSentEmailItem>, TasksProducer<UnSentEmailItem>>();
        }
    }

    public static class UnsentEmailReserverInstaller
    {
        public static void AddEmailReseverService(this IServiceCollection services)
        {
            services.AddScoped<IReserveItemForAction<UnSentEmailItem, EmailReserveResult>, UnsentEmailReserverService>();

        }
    }


    public static class TasksLoaderInstaller
    {
        public static void AddTasksLoaderService(this IServiceCollection services)
        {
            services.AddScoped<ITaskLoader<UnSentEmailItem>, UnsentEmailTaskLoaderService>(); ;
        }

    }


    public static class TasksQueueInstaller
    {
        public static void AddTasksQueue(this IServiceCollection services)
        {
            services.AddSingleton<ITaskQueue<UnSentEmailItem>, TasksQueue<UnSentEmailItem>>();

        }
    }

    public static class TasksWorkerInstaller
    {
        public static void AddTaskWorker(this IServiceCollection services)
        {
            services.AddScoped<ITasksWorker<UnSentEmailItem>, TasksWorker<UnSentEmailItem>>();
        }
    }

    public static class UnsentMailQueueConsumerInstaller
    {
        public static void AddUnsentMailConsumer(this IServiceCollection services)
        {
            services.AddScoped<IConsumer<UnSentEmailItem>, UnsentEmailMessageConsumer>();
        }
    }




}
