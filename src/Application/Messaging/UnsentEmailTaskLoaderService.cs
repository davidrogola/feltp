﻿using FELTP.Infrastructure;
using FELTP.Infrastructure.Task;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Application.Messaging
{
    public class UnsentEmailTaskLoaderService : ITaskLoader<UnSentEmailItem>
    {
        Func<MySqlConnection> mySqlConnectionFactory;
        string sproc = "peek_unsent_email";
        IConfiguration configuration;
        public UnsentEmailTaskLoaderService(Func<MySqlConnection> _mySqlConnectionFactory, IConfiguration _configuration)
        {
            mySqlConnectionFactory = _mySqlConnectionFactory;
            configuration = _configuration;
        }
        public IEnumerable<UnSentEmailItem> LoadTasks(int totalTasks)
        {
            if (totalTasks == 0)
                return null;
            var pendingTasks = new List<UnSentEmailItem>();

            var peekTimeInSeconds = Convert.ToInt16(configuration["PeekLockInterval"]);

            using (var connection = mySqlConnectionFactory())
            using (var command = new MySqlCommand(sproc, connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("peekedBy", Util.GetProcessName());
                command.Parameters.AddWithValue("peekTimeInSeconds",peekTimeInSeconds);
                command.Parameters["peekedBy"].Direction = ParameterDirection.Input;
                command.Parameters["peekTimeInSeconds"].Direction = ParameterDirection.Input;

                command.Connection.Open();

                using (var reader = command.ExecuteReader())
                {
                    while (reader.HasRows)
                    {
                        if (!reader.Read())
                        return pendingTasks;
                        
                        pendingTasks.Add(new UnSentEmailItem
                        {
                            OutGoingMailMessageId = reader.GetInt64(0)
                        });
                    }
                }
            }

            return pendingTasks;
        }
    }
}
