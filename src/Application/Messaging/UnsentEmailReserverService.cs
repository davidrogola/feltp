﻿using FELTP.Infrastructure;
using FELTP.Infrastructure.Task;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Application.Messaging
{
    public class UnsentEmailReserverService : IReserveItemForAction<UnSentEmailItem, EmailReserveResult>
    {
        string reserveEmailSproc = "try_reserve_and_get_unsent_mail";
        string markMailAsProcessedSproc = "mark_mail_as_processed";
        Func<MySqlConnection> mySqlFactory;
        IConfiguration configuration;
        public UnsentEmailReserverService(Func<MySqlConnection> _mySqlFactory, IConfiguration _configuration)
        {
            mySqlFactory = _mySqlFactory;
            configuration = _configuration;
        }

        public void MarkAsComplete(EmailReserveResult result, bool operationFailed)
        {
            using (var connection = mySqlFactory())
            using (var command = new MySqlCommand(markMailAsProcessedSproc, connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("emailId", result.OutGoingEmailMessageId);
                command.Parameters.AddWithValue("operationFailed", operationFailed);
                command.Parameters["emailId"].Direction = ParameterDirection.Input;
                command.Parameters["operationFailed"].Direction = ParameterDirection.Input;
                command.Connection.Open();
                command.ExecuteReader();
            }
        }


        public EmailReserveResult Reserve(UnSentEmailItem reservationObject)
        {
            var pendingTasks = new List<UnSentEmailItem>();

            var peekTimeInSeconds = Convert.ToInt16(configuration["PeekLockInterval"]);

            using (var connection = mySqlFactory())
            using (var command = new MySqlCommand(reserveEmailSproc, connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("emailId", reservationObject.OutGoingMailMessageId);
                command.Parameters.AddWithValue("shouldBePeekedBy", Util.GetProcessName());
                command.Parameters.AddWithValue("secondsToHold", peekTimeInSeconds);

                command.Parameters["emailId"].Direction = ParameterDirection.Input;
                command.Parameters["shouldBePeekedBy"].Direction = ParameterDirection.Input;
                command.Parameters["secondsToHold"].Direction = ParameterDirection.Input;

                command.Connection.Open();

                var reserveResult = new EmailReserveResult();

                using (var reader = command.ExecuteReader())
                {
                    var hasRows = reader.HasRows;
                    while (hasRows)
                    {
                        if (!reader.Read())
                            return reserveResult;
                        reserveResult.OutGoingEmailMessageId = reader.GetInt64(0);
                        reserveResult.Body = reader.GetString(1);
                        reserveResult.Subject = reader.GetString(2);
                        reserveResult.Email = reader.GetString(3);
                        reserveResult.Successful = true;
                        reserveResult.ReservedDate = reader.GetDateTime(4);
                        reserveResult.ReservedToDate = reader.GetDateTime(5);
                    }
                }
                return reserveResult;
            }

        }
    }
}
