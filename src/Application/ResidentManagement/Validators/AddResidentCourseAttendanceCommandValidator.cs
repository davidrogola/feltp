﻿using Application.ResidentManagement.Commands;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Validators
{
    public class AddResidentCourseAttendanceCommandValidator : AbstractValidator<AddResidentCourseAttendanceCommand>
    {
        public AddResidentCourseAttendanceCommandValidator()
        {
            RuleFor(x => x.AttendanceDate).NotNull().WithMessage("Please enter the attendance date");
            RuleFor(x => x.SemesterScheduleId).NotEqual(0).WithMessage("Please select a semester schedule");
            RuleFor(x => x.CourseScheduleId).NotEqual(0).WithMessage("Please select a the course schedule");
            RuleFor(x => x.ProgramOfferId).NotEqual(0).WithMessage("Please select a program offer");
            RuleFor(x => x.UnitId).NotEqual(0).WithMessage("Please a unit");
            RuleFor(x => x.StrAttendanceDate).NotNull().WithMessage("Please select the attendance date");
            RuleFor(x => x.StrFacultyTimetableItem).NotNull().WithMessage("Please select the faculty and time slot");
            RuleFor(x => x.Status).NotNull().WithMessage("Please select the faculty and time slot");

        }
    }
}
