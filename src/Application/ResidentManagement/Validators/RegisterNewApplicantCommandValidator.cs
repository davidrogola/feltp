﻿using Application.Common.Models;
using Application.ResidentManagement.Commands;
using Common.Domain.Address;
using Common.Domain.Person;
using FluentValidation;
using Persistance.ResidentManagement.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ResidentManagement.Validators
{
    public class RegisterNewApplicantCommandValidator : AbstractValidator<RegisterNewApplicantCommand>
    {
        IResidentManagementUnitOfWork unitOfWork;
        public RegisterNewApplicantCommandValidator(IResidentManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;

            RuleFor(x => x.Person.BioDataInfo).Must((BioDataInformation info) =>
              {
                  if (info == null)
                      return false;
                  return true;
              }).WithMessage(ResidentManagementResource.BioDataInfoRequired);

            RuleFor(x => x.Person.BioDataInfo.IdentificationNumber)
                .NotNull().WithMessage(ResidentManagementResource.IdNumberRequired);

            RuleFor(x => x.Person.BioDataInfo.CadreId)
               .NotEqual(0).WithMessage(ResidentManagementResource.CadreRequired);

            RuleFor(x => x.Person.BioDataInfo.IdentificationNumber).Must((string idNo)=> 
            {
                if (idNo == null)
                    return true;
                bool exists = unitOfWork.ResidentManagementRepository
                .FindBy<Person>(x => x.IdentificationNumber == idNo).Any();
                return !exists;
            }).WithMessage(ResidentManagementResource.DuplicateIdNumber);

            RuleFor(x => x.Person.ContactInfo).Must((ContactInfo info) =>
            {
                if (info == null)
                    return false;
                return true;
            }).WithMessage(ResidentManagementResource.ContactInformationRequired);

            RuleFor(x => x.Person.ContactInfo.PrimaryMobileNumber)
                .NotNull().WithMessage(ResidentManagementResource.PhoneNumberRequired);

            RuleFor(x => x.Person.ContactInfo.PrimaryMobileNumber).Must((string phoneNo) =>
            {
                if (phoneNo == null)
                    return true;
                bool exists = unitOfWork.ResidentManagementRepository
                .FindBy<ContactInformation>(x => x.PrimaryMobileNumber == phoneNo).Any();
                return !exists;
            }).WithMessage(ResidentManagementResource.DuplicatePhoneNumber);

            RuleFor(x => x.Person.ContactInfo.EmailAddress).Must((string email) =>
            {
                if (email == null)
                    return true;
                bool exists = unitOfWork.ResidentManagementRepository
                .FindBy<ContactInformation>(x => x.EmailAddress == email).Any();
                return !exists;
            }).WithMessage(ResidentManagementResource.DuplicateEmailAddress);




        }
    }

}
