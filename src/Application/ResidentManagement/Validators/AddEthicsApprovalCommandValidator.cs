﻿using Application.ResidentManagement.Commands;
using FluentValidation;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.Event;
using ResidentManagement.Domain.Ethics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ResidentManagement.Validators
{
    public class AddEthicsApprovalCommandValidator : AbstractValidator<AddEthicsApprovalCommand>
    {
        IResidentManagementUnitOfWork unitOfWork;
        IProgramManagementUnitOfWork programUnitOfWork;
        public AddEthicsApprovalCommandValidator(IResidentManagementUnitOfWork unitOfWork,
            IProgramManagementUnitOfWork programUnitOfWork)
        {
            this.unitOfWork = unitOfWork;
            this.programUnitOfWork = programUnitOfWork;

            RuleFor(x => x.ProgramId).NotEqual(0).WithMessage(ResidentManagementResource.ProgramRequired);
            RuleFor(x => x.PaperCategory).NotEmpty().WithMessage(ResidentManagementResource.PaperCategoryRequired);
            RuleFor(x => x.ProgramOfferId).NotEqual(0).WithMessage(ResidentManagementResource.ProgramOfferRequired);
            RuleFor(x => x.ProgramEnrollmentId).NotEqual(0).WithMessage(ResidentManagementResource.ResidentRequired);
            RuleFor(x => x.ProgramEventId).NotEqual(0).WithMessage(ResidentManagementResource.ProgramEventRequired);
            RuleFor(x => x.ResidentDeliverableId).NotEqual(0)
                .WithMessage(ResidentManagementResource.ResidentDeliverableRequired);
            RuleFor(x => x.FormFile).NotEmpty().WithMessage(ResidentManagementResource.FileRequired);

            RuleFor(x => x).Must((AddEthicsApprovalCommand model) =>
              {
                  if (model == null)
                      return true;
                  var ethicsExists = unitOfWork.ResidentManagementRepository.FindBy<EthicsApproval>(x => x.ProgramEventId == model.ProgramEventId && x.ResidentDeliverableId == model.ResidentDeliverableId).Any();
                  return !ethicsExists;
              }).WithMessage(ResidentManagementResource.DuplicateEthicsApprovalForm);

            RuleFor(x => x.ProgramEventId).Must((int eventId) =>
              {
                  if (eventId == 0)
                      return true;
                  var exists = programUnitOfWork.ProgramManagementRepository.FindBy<EventSchedule>(x => x.ProgramEventId == eventId).Any();
                  return exists;
              }).WithMessage(ResidentManagementResource.ScheduleRequired);

            RuleFor(x => x.ProgramEventId).Must((int eventId) =>
            {
                if (eventId == 0)
                    return true;
                var progamEventSchedule = programUnitOfWork.ProgramManagementRepository.FindBy<EventSchedule>(x => x.ProgramEventId == eventId).Any();
                return progamEventSchedule;
            }).WithMessage(ResidentManagementResource.ScheduleRequired);
        }
    }
}
