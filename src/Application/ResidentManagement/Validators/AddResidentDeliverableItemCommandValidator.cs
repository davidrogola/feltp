﻿using Application.Common.Services;
using Application.ProgramManagement.Queries;
using Application.ResidentManagement.Commands;
using FluentValidation;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Threading.Tasks;

namespace Application.ResidentManagement.Validators
{
    public class AddResidentDeliverableItemCommandValidator : AbstractValidator<AddResidentDeliverableItemCommand>
    {
        IResidentManagementUnitOfWork unitOfWork;
        IMediator mediator;
        public AddResidentDeliverableItemCommandValidator(IResidentManagementUnitOfWork unitOfWork, IMediator mediator)
        {
            this.unitOfWork = unitOfWork;
            this.mediator = mediator;

            RuleFor(x => x.ResidentDeliverableId).NotEqual(0).WithMessage("Please select a deliverable");
            RuleFor(x => x.Title).NotNull().WithMessage("Please enter title");
            RuleFor(x => x.Description).NotNull().WithMessage("Please enter the description");
            RuleFor(x => x.StrDateSubmitted).NotNull().WithMessage("Please select the submission date");
            RuleFor(x => x.SubmissionType).NotNull().WithMessage("Please select the submission type");

            RuleFor(x => x.ResidentDeliverableId).Must((long deliverableId) =>
              {
                  var finalSubmissionExists = unitOfWork.ResidentManagementRepository.FindBy<ResidentDeliverableItem>(x => x.ResidentDeliverableId == deliverableId && x.SubmissionType == SubmissionType.Final).Any();
                  return !finalSubmissionExists;
              }).WithMessage("Final submission for this deliverable has already been submitted");

            RuleFor(x => x).Custom(async (model, context) =>
            {
                var semesterScheduleDates = await GetSemesterScheduleDates(model.ProgramOfferId, model.SemesterId);
                if (semesterScheduleDates == null)
                {
                    context.AddFailure(
                        "Please ensure the semester schedule for the select semester has been configured");
                    return;
                }

                var semStartDate = semesterScheduleDates.Item1.ToLocalDate();
                var semEndDate = semesterScheduleDates.Item2.ToLocalDate();

                var submissionDate = model.StrDateSubmitted.ToLocalDate().Date;
                if (submissionDate > semEndDate)
                    context.AddFailure($"Deliverable cannot be submitted after semester end date of " +
                                       $"{semEndDate.ToShortDateString()}");

                if (submissionDate < semStartDate)
                    context.AddFailure($"Deliverable cannot be submitted before the semester start date of " +
                                       $"{semStartDate.ToShortDateString()}");
            });
        }

        private async Task<Tuple<string, string>> GetSemesterScheduleDates(int programOfferId, int semesterId)
        {
            var programOfferSemesterSchedules = await mediator.Send(
                new GetProgramOfferSemesterSchedule { ProgramOfferId = programOfferId });

            var selectedSemesterSchedule = programOfferSemesterSchedules?.FirstOrDefault(x => x.SemesterId == semesterId);
            if (selectedSemesterSchedule == null)
            {
                return null;
            }

            return new Tuple<string, string>(selectedSemesterSchedule?.EstimatedStartDate, 
                selectedSemesterSchedule?.EstimatedEndDate);
        }

    }
}
