﻿using Application.Common.Services;
using Application.ResidentManagement.Commands;
using FluentValidation;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.FieldPlacement;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ResidentManagement.Validators
{
    public class AddFieldPlacementCommandValidator : AbstractValidator<AddResidentFieldPlacementCommand>
    {
        IResidentManagementUnitOfWork unitOfWork;
        public AddFieldPlacementCommandValidator(IResidentManagementUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;

            
            RuleFor(x => x).Must((AddResidentFieldPlacementCommand model) =>
              {
                  if (model == null)
                      return true;
                  if (model.ActivityType == ActivityType.Field.ToString() && model.SupervisorId == 0)
                      return false;
                  else return true;
              }).WithMessage("Select the site supervisor");

            RuleFor(x => x).Must((AddResidentFieldPlacementCommand model) =>
            {
                if (model == null)
                    return true;
                if (model.ActivityType == ActivityType.NonField.ToString() && model.Faculty == 0)
                    return false;
                else return true;
            }).WithMessage("Select faculty");

            RuleFor(x => x.Description).NotNull().WithMessage("Enter the description for the activity");
            RuleFor(x => x.StrStartDate).NotNull().WithMessage("Enter the start date");
            RuleFor(x => x.StrEndDate).NotNull().WithMessage("Enter the end date");
            RuleFor(x => x.ResidentFieldPlacementActivityId).NotNull().WithMessage("Please select the activity");
            RuleFor(x => x.ResidentFieldPlacementActivityId).Must((long residentActivityId) =>
              {
                  var activityExists = unitOfWork.ResidentManagementRepository.FindBy<FieldPlacement>
                  (x => x.ResidentFieldPlacementActivityId == residentActivityId).Any();

                  return !activityExists;
             }).WithMessage("Field placement for this activity already created");

            RuleFor(x => x).Custom((model, context) =>
            {
                if(String.IsNullOrEmpty(model.StrStartDate) || String.IsNullOrEmpty(model.StrEndDate))
                {
                    return;
                }

                if (model.StrStartDate.ToLocalDate() < model.SemesterStartDate.ToLocalDate())
                    context.AddFailure("Field placement start date cannot be less than the semester start date");
               
                if(model.StrEndDate.ToLocalDate() > model.SemesterEndDate.ToLocalDate())
                    context.AddFailure("Field placement end date cannot be greater than the semester end date");

            });
        }
    }
}
