﻿using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using FluentValidation;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ResidentManagement.Validators
{
    public class AddNewProgramOfferApplicationCommandValidator :AbstractValidator<AddNewProgramOfferApplicationCommand>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IResidentManagementUnitOfWork residentUnitOfWork;
        IMediator mediator;
        public AddNewProgramOfferApplicationCommandValidator(IProgramManagementUnitOfWork unitOfWork, 
            IResidentManagementUnitOfWork _residentUnitOfWork, IMediator mediator)
        {
            this.unitOfWork = unitOfWork;
            residentUnitOfWork = _residentUnitOfWork;
            this.mediator = mediator;

            RuleFor(x => x.ProgramId).NotEqual(0).WithMessage("Please select a program");
            RuleFor(x => x).Must((AddNewProgramOfferApplicationCommand model) =>
              {
                  var exists = unitOfWork.ProgramManagementRepository.FindBy<ProgramOfferApplicants>(x => x.ApplicantId == model.ApplicantId && x.ProgramOfferId == model.ProgramOfferId).Any();

                  return !exists;
              }).WithMessage("An existing application to the selected program exists");

            RuleFor(x => x).Custom((model, context) =>
            {
                var unwantedProgramStatus = new[]
                {
                    CompletionStatus.Discontinued,
                    CompletionStatus.NotCompleted,
                    CompletionStatus.Pending,
                    CompletionStatus.Leave
                }.ToList();

                if (model.ProgramOfferId == 0)
                {
                    context.AddFailure("Please select a program offer");
                    return;
                }
                var residentId = residentUnitOfWork.ResidentManagementRepository.FindBy<Resident>(x => x.ApplicantId == model.ApplicantId).SingleOrDefault()?.Id;
                if (residentId.HasValue)
                {
                    var coursePendingCompletion = residentUnitOfWork.ResidentManagementRepository.FindBy<Graduate>(x => x.ResidentId == residentId.Value && unwantedProgramStatus.Contains(x.CompletionStatus)).Any();
                    if (coursePendingCompletion)
                        context.AddFailure("The applicant cannot apply for another program before completion of the current program");
                }
            });


            RuleFor(x => x).Custom((model, context) =>
            {
                var programOfferDetail = mediator.Send(new GetProgramOfferById { OfferId = model.ProgramOfferId }).Result;
                if(programOfferDetail != null)
                {
                   var validationResponse = programOfferDetail.ValidateProgramOfferApplicationDuration();
                    if (validationResponse.ApplicationsClosed)
                    {
                        context.AddFailure(validationResponse.Message);
                        return;
                    }

                    if (!validationResponse.ApplicationsOpen)
                    {
                        context.AddFailure(validationResponse.Message);
                        return;
                    }
                }

                var hasContinuingCourse = residentUnitOfWork.ResidentManagementRepository.FindBy<Graduate>(x => x.ResidentId == model.ResidentId && x.CompletionStatus != CompletionStatus.Completed).Any();

                if (hasContinuingCourse)
                    context.AddFailure("The applicant cannot cannot apply for another program before completion of the ongoing program");

            });
        }
    }
}
