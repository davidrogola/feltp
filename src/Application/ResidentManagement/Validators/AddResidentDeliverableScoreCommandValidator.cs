﻿using Application.ResidentManagement.Commands;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ResidentManagement.Validators
{
    public class AddResidentDeliverableScoreCommandValidator : AbstractValidator<AddResidentDeliverableScoreCommand>
    {
        public AddResidentDeliverableScoreCommandValidator()
        {
            RuleFor(x => x).Custom((command, context) =>
            {
                if (command == null)
                {
                    context.AddFailure("Please select atleast one deliverable before submitting scores");
                    return;
                }

                if (!command.ResidentDeliverables.Any(x => x.Score.HasValue == true))
                    context.AddFailure("Please submit the score of atleast one deliverable");

                foreach (var item in command.ResidentDeliverables)
                {

                    if (item.Score.HasValue)
                    {
                        if (item.Score.Value < 0 || item.Score.Value > 100)
                            context.AddFailure($"Scores for deliverable {item.Deliverable} should be between 0-100");
                    }
                    
                }
                return;
            });
        }
    }
}
