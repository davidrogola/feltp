﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Notifications
{
    public class ResidentCourseWorkCreatedNotification : INotification
    {
        public long ResidentId { get; set; }
        public int EnrollmentId { get; set; }
        public List<ResidentUnitNotification> ResidentUnits { get; set; }
        public string CreatedBy { get; set; }
    }

    public class ResidentUnitNotification
    {
        public long ResidentUnitId { get; set; }
        public int UnitId { get; set; }
    }

}
