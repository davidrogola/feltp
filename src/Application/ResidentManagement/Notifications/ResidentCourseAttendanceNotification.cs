﻿using MediatR;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Notifications
{
    public class ResidentCourseAttendanceNotification : INotification
    {
        public int UnitId { get; set; }
        public long ResidentId { get; set; }
        public int FacultyId { get; set; }
        public AttendanceStatus AttendanceStatus { get; set; }
        public DateTime AttendanceDate { get; set; }
        public string CreatedBy { get; set; }
        public int TimetableId { get; set; }

    }
}
