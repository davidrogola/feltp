﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Notifications
{
    public class ResidentExaminationScoreEvaluatedNotification : INotification
    {
        public long ResidentId { get; set; }
        public int ProgramEnrollmentId { get; set; }
        public List<ExaminationScoreDetails> ExaminationScores { get; set; }
    }

    public class ExaminationScoreDetails
    {
        public int ExaminationId { get; set; }
        public Decimal Score { get; set; }
        public long ResidentUnitId { get; set; }
        
        public long ResidentCourseWorkId { get; set; }
    }


}
