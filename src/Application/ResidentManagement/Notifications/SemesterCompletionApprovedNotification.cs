﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Notifications
{
    public class SemesterCompletionApprovedNotification : INotification
    {
        public int SemesterScheduleId { get; set; }
        public int ProgramOfferId { get; set; }
        public int SemesterId { get; set; }

    }
}
