using System;
using System.Collections.Generic;
using System.Linq;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Examination;
using Serilog;

namespace Application.ResidentManagement.Services
{
    public class ExamScoreConsolidationService
    {
        private readonly IProgramManagementUnitOfWork _programManagementUnitOfWork;
        private readonly ILogger _logger = Log.ForContext<ExamScoreConsolidationService>();
        public ExamScoreConsolidationService(IProgramManagementUnitOfWork programManagementUnitOfWork)
        {
            _programManagementUnitOfWork = programManagementUnitOfWork;
        }

        public decimal  ConsolidateScores(long residentCourseWorkId, decimal examScore)
        {
            const  int examMarksBase = 70;
            var catExamTypes = new[]
            {
                ExamType.CatOne, 
                ExamType.CatTwo, 
                ExamType.CatThree
            };
            
                var catScores = _programManagementUnitOfWork.ProgramManagementRepository
                    .FindBy<ExaminationScore>(x => x.ResidentCourseWorkId == residentCourseWorkId
                                                   && catExamTypes.Contains(x.ExamType)).ToList();
                if (catScores.Count == 0 || catScores.Count != catExamTypes.Length)
                {
                    _logger.Warning($"All the required cat scores not found for resident Course Id {residentCourseWorkId}");
                    return examScore;
                }

                var averageCatScore = catScores.Average(x => x.Score);
                var averageExamScore =examScore / 100 * examMarksBase;
                
               return averageCatScore + averageExamScore;      
        }
            
    }
}