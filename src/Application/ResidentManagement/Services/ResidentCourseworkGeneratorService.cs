﻿using Application.ProgramManagement.Notification;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ResidentManagement.Services
{

    public interface IResidentCourseworkGenerator
    {
        void GenerateResidentCoursework();
    }

    public class ResidentCourseworkGeneratorService : IResidentCourseworkGenerator
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMediator mediator;
        public ResidentCourseworkGeneratorService(IProgramManagementUnitOfWork _unitOfWork, IMediator _mediator)
        {
            unitOfWork = _unitOfWork;
            mediator = _mediator;
        }
        public void GenerateResidentCoursework()
        {
            var enrolledResidentsWithMissingCourseWork = unitOfWork.ProgramManagementRepository.
                FindBy<ProgramOfferApplicants>(x => x.EnrollmentStatus == EnrollmentStatus.Enrolled 
                                                    && x.ResidentCourseWorkId == null).Select(x => new
            {
                EnrollmentId = Convert.ToInt32(x.EnrollmentId),
                x.ResidentId,
                x.ProgramId,
                x.ProgramOfferId,
                DateEnrolled = Convert.ToDateTime(x.DateEnrolled),
                x.EnrolledBy
            }).ToList();


            enrolledResidentsWithMissingCourseWork.ForEach(async (enrollment) =>
            {
                if(enrollment.EnrollmentId != 0)
                await mediator.Publish(new ResidentEnrolledNotification
                {
                    DateEnrolled = Convert.ToDateTime(enrollment.DateEnrolled),
                    EnrolledBy = enrollment.EnrolledBy,
                    EnrollmentId = enrollment.EnrollmentId,
                    ProgramId = enrollment.ProgramId,
                    ProgramOfferId = enrollment.ProgramOfferId,
                    ResidentId = Convert.ToInt64(enrollment.ResidentId)
                });
            });

            return;
        }
    }


    public static class ResidentCourseWorkGeneratorInstaller
    {
        public static void AddResidentCourseGeneratorService(this IServiceCollection services)
        {
            services.AddScoped<IResidentCourseworkGenerator, ResidentCourseworkGeneratorService>();
        }
    }



}
