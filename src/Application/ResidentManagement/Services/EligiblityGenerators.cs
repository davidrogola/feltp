﻿using Application.ResidentManagement.Interface;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Services
{

    public class GraduationElibilityGenerator : IGraduateEligibityGenerator
    {

        public List<GraduateEligibilityItem> GenerateEligibityItems(EligibityDetails eligibityDetails)
        {
            var eligibityItems = new List<GraduateEligibilityItem>();

            foreach (var eligibityType in eligibityDetails.EligibilityTypes)
            {
                GraduateEligibilityItem graduateEligiblityItem = null;

                switch (eligibityType)
                {
                    case EligibilityType.DidacticUnitsAttendance:

                        graduateEligiblityItem = new GraduateEligibilityItem(eligibityDetails.GraduateId, 
                            eligibityDetails.NumberOfDidacticModulesRequired, 0,
                            OutcomeStatus.PendingEvaluation, eligibityType, FailureReason.None, eligibityDetails.CreatedBy,
                            null,null);
                        eligibityItems.Add(graduateEligiblityItem);

                        break;
                    case EligibilityType.FieldActivitiesParticipated:
                        graduateEligiblityItem = new GraduateEligibilityItem(eligibityDetails.GraduateId, 
                            eligibityDetails.NumberOfActivitiesRequired, 0, OutcomeStatus.PendingEvaluation,
                            eligibityType, FailureReason.None, eligibityDetails.CreatedBy, null,null);

                        eligibityItems.Add(graduateEligiblityItem);

                        break;
                    case EligibilityType.PassmarkAttained:
                        foreach (var residentCourseWork in eligibityDetails.ResidentCourseWorkIds)
                        {
                            var residentUnitEligiblity = new GraduateEligibilityItem(eligibityDetails.GraduateId, 
                                eligibityDetails.PassingGrade,
                      0, OutcomeStatus.PendingEvaluation, eligibityType, FailureReason.None, 
                                eligibityDetails.CreatedBy, null,
                                residentCourseWork);

                            eligibityItems.Add(residentUnitEligiblity);
                        }

                        break;
                    case EligibilityType.ThesisDefenceSuccesful:
                        graduateEligiblityItem = new GraduateEligibilityItem(eligibityDetails.GraduateId, 
                            eligibityDetails.NumberOfThesisDefenceDeliverables, 0, OutcomeStatus.PendingEvaluation, 
                            eligibityType, FailureReason.None, eligibityDetails.CreatedBy, null,null);

                        eligibityItems.Add(graduateEligiblityItem);

                        break;
   
                }
              
            }

            return eligibityItems;
        }
    }


}
