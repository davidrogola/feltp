﻿using Application.ResidentManagement.Interface;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.ResidentManagement.Services
{
    public static class GraduateEligibilityGeneratorServicesInstaller
    {
        public static void AddEligiltyGeneratorServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped(typeof(IGraduateEligibityGenerator), typeof(GraduationElibilityGenerator));

        }

    }


}
