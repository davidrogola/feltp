﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;
using ProgramManagement.Domain.Program;
using Application.ProgramManagement.Notification;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain;
using System.Collections.Generic;
using Application.ResidentManagement.Interface;
using Serilog;
using System;

namespace Application.ResidentManagement.NotificationHandlers
{
    public class ResidentGraduationInformationGenerator : INotificationHandler<ResidentEnrolledNotification>
    {
        readonly IResidentManagementUnitOfWork residentManagementUnitOfWork;
        readonly IProgramManagementUnitOfWork programManagementUnitOfWork;
        readonly IGraduateEligibityGenerator graduateEligibityGenerator;

        readonly ILogger logger = Log.ForContext<ResidentGraduationInformationGenerator>();

        public ResidentGraduationInformationGenerator(IResidentManagementUnitOfWork _residentManagementUnitOfWork,
        IProgramManagementUnitOfWork _programManagementUnitOfWork, IGraduateEligibityGenerator _graduateEligibityGenerator)
        {
            residentManagementUnitOfWork = _residentManagementUnitOfWork;
            programManagementUnitOfWork = _programManagementUnitOfWork;
            graduateEligibityGenerator = _graduateEligibityGenerator;
        }
        public Task Handle(ResidentEnrolledNotification notification, CancellationToken cancellationToken)
        {
            try
            {
                var graduationRequirement = programManagementUnitOfWork.ProgramManagementRepository
                 .FindBy<GraduationRequirement>(x => x.ProgramId == notification.ProgramId).SingleOrDefault();

                if (graduationRequirement == null)
                {
                    logger.Information($"Graduation requirement not found for programId {notification.ProgramId} and OfferId {notification.ProgramOfferId} ");
                    return Task.FromResult(0);
                }

                var programOffer = programManagementUnitOfWork.ProgramManagementRepository
                    .Get<ProgramOffer>(notification.ProgramOfferId);
                if (programOffer == null)
                {
                    logger.Information($"Program Offer not found for OfferId  {notification.ProgramOfferId} ");
                    return Task.FromResult(0);
                }

                var residentCourseWorkIds = residentManagementUnitOfWork.ResidentManagementRepository
                    .FindBy<ResidentCourseWork>(x => x.ProgramEnrollmentId == notification.EnrollmentId)
                    .Select(x => x.Id).ToList();

                var graduationInfoExists = residentManagementUnitOfWork.ResidentManagementRepository.
                    FindBy<Graduate>(x => x.ProgramEnrollmentId == notification.EnrollmentId).Any();
                if(graduationInfoExists)
                {
                    logger.Information($"Graduation information for program enrollment Id {notification.EnrollmentId} already exists");
                    return Task.FromResult(0);
                }

                var graduate = new Graduate(graduationRequirement.Id, notification.EnrolledBy, notification.ResidentId, notification.EnrollmentId, QualificationStatus.PendingEvaluation, programOffer.EndDate);

                residentManagementUnitOfWork.ResidentManagementRepository.Add(graduate);

                var eligibityTypes = new List<EligibilityType>()
                    {
                       EligibilityType.DidacticUnitsAttendance,
                       EligibilityType.FieldActivitiesParticipated,
                       EligibilityType.PassmarkAttained,
                       EligibilityType.ThesisDefenceSuccesful
                    };

                var eligibilityDetails = new EligibityDetails
                {
                    ResidentCourseWorkIds = residentCourseWorkIds,
                    CreatedBy = notification.EnrolledBy,
                    EligibilityTypes = eligibityTypes,
                    GraduateId = graduate.Id,
                    NumberOfActivitiesRequired = graduationRequirement.NumberOfActivitiesRequired,
                    NumberOfDidacticModulesRequired = graduationRequirement.NumberOfDidacticModulesRequired,
                    NumberOfThesisDefenceDeliverables = 0,
                    PassingGrade = graduationRequirement.PassingGrade
                };

                var eligiblityItemsCollection = graduateEligibityGenerator.GenerateEligibityItems(eligibilityDetails);

                residentManagementUnitOfWork.ResidentManagementRepository.AddRange(eligiblityItemsCollection);
                residentManagementUnitOfWork.SaveChanges();

                return Task.FromResult(0);
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while generating graduation eligiblity items for residentId {notification.ResidentId} and EnrollmentId { notification.EnrollmentId }");
                return Task.FromResult(0);
            }

        }
    }
}
