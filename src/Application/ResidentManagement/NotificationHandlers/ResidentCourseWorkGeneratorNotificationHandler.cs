﻿using Application.ProgramManagement.Notification;
using Application.ResidentManagement.Notifications;
using MediatR;
using Newtonsoft.Json;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.NotificationHandlers
{
    public class ResidentCourseWorkGeneratorNotificationHandler : INotificationHandler<ResidentEnrolledNotification>
    {
        readonly IMediator mediator;
        readonly IResidentManagementUnitOfWork residentManagementUnitOfWork;
        readonly IProgramManagementUnitOfWork programManagementUnitOfWork;
        readonly ILogger logger = Log.ForContext<ResidentCourseWorkGeneratorNotificationHandler>();

        public ResidentCourseWorkGeneratorNotificationHandler(IResidentManagementUnitOfWork _residentManagementUnitOfWork,
            IProgramManagementUnitOfWork _unitOfWork, IMediator _mediator)
        {
            residentManagementUnitOfWork = _residentManagementUnitOfWork;
            programManagementUnitOfWork = _unitOfWork;
            mediator = _mediator;

        }
        public Task Handle(ResidentEnrolledNotification notification, CancellationToken cancellationToken)
        {
            try
            {
                using (var transaction = residentManagementUnitOfWork.BeginTransaction(IsolationLevel.ReadCommitted))
                {
                    var programCourses = programManagementUnitOfWork.ProgramManagementRepository
                        .FindBy<ProgramCourse>(x => x.ProgramId == notification.ProgramId && x.DateDeactivated == null)
                            .Select(x => new
                            {
                                x.Id,
                                x.CourseId
                            }).ToList();

                    if (!programCourses.Any())
                    {
                        logger.Information($"Progam courses not found for programId: {notification.ProgramId} during enrollment of ResidentId: {notification.ResidentId}");
                        return Task.FromResult(0);
                    }



                    var courseWorkExists = residentManagementUnitOfWork.ResidentManagementRepository.FindBy<ResidentCourseWork>(x => x.ProgramEnrollmentId == notification.EnrollmentId && programCourses.Select(p => p.Id).Contains(x.ProgramCourseId)).Any();
                    if (courseWorkExists)
                    {
                        logger.Information($"Resident coursework already generated for enrollmentId : {notification.EnrollmentId}");
                        return Task.FromResult(0);

                    }

                    var residentCourses = new List<ResidentCourseWork>();

                    foreach (var programCourse in programCourses)
                    {
                        var residentCourse = new ResidentCourseWork(notification.EnrollmentId, programCourse.Id, CourseProgessStatus.NotStarted, notification.EnrolledBy, notification.ResidentId);
                        residentCourses.Add(residentCourse);
                    }

                    residentManagementUnitOfWork.ResidentManagementRepository.AddRange(residentCourses);
                    residentManagementUnitOfWork.SaveChanges();

                    var courseIds = programCourses.Select(course => course.CourseId).ToList();

                    var courseUnits = programManagementUnitOfWork.ProgramManagementRepository.FindByWithInclude<CourseUnit>(x =>
                    courseIds.Contains(x.CourseId)).Select(x => new
                        {
                            x.CourseId,
                            x.UnitId
                        }).ToList();

                    if (!courseUnits.Any())
                    {
                        logger.Information($"Course units not found for program Courses: {JsonConvert.SerializeObject(programCourses)} during enrollment of ResidentId: {notification.ResidentId}");
                        return Task.FromResult(0);
                    }
                    var residentUnits = new List<ResidentUnit>();

                    foreach (var courseUnit in courseUnits)
                    {
                        var programCourseId = programCourses.FirstOrDefault(x => x.CourseId == courseUnit.CourseId)?.Id;
                        if (!programCourseId.HasValue)
                            continue;
                        var residentCourseWorkId = residentCourses.FirstOrDefault(x => x.ProgramCourseId == programCourseId.Value)?.Id;
                        if (!residentCourseWorkId.HasValue)
                            continue;
                        var residentUnit = new ResidentUnit(residentCourseWorkId.Value, courseUnit.UnitId, notification.EnrolledBy);

                        if (!residentUnits.Contains(residentUnit))
                           residentUnits.Add(residentUnit);

                    }

                    residentManagementUnitOfWork.ResidentManagementRepository.AddRange(residentUnits);
                    residentManagementUnitOfWork.SaveChanges();

                    var courseWorkCreatedNotification = new ResidentCourseWorkCreatedNotification()
                    {
                        CreatedBy = notification.EnrolledBy,
                        EnrollmentId = notification.EnrollmentId,
                        ResidentId = notification.ResidentId,
                        ResidentUnits = residentUnits.Select(x => new ResidentUnitNotification
                        {
                            UnitId = x.UnitId,
                            ResidentUnitId = x.Id
                        }).ToList()
                    };

                    mediator.Publish(courseWorkCreatedNotification);

                    transaction.Commit();
                }
                return Task.FromResult(0);
            }
            catch (Exception ex)
            {
                logger.Error(ex,$"An error occured during resident coursework generation for residentId: {notification.ResidentId} and enrollmentId: { notification.EnrollmentId }");
                throw;
            }
        }
    }
}
