﻿using Application.ResidentManagement.Notifications;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.NotificationHandlers
{
    public class ResidentExaminationScoreEvaluatedNotificationHandler : INotificationHandler<ResidentExaminationScoreEvaluatedNotification>
    {
        readonly IResidentManagementUnitOfWork unitOfWork;
        readonly ILogger logger = Log.ForContext<ResidentExaminationScoreEvaluatedNotificationHandler>();
        public ResidentExaminationScoreEvaluatedNotificationHandler(IResidentManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task Handle(ResidentExaminationScoreEvaluatedNotification notification, CancellationToken cancellationToken)
        {
            try
            {
                var graduate = unitOfWork.ResidentManagementRepository.FindBy<Graduate>(x => x.ProgramEnrollmentId == notification.ProgramEnrollmentId).SingleOrDefault();
                if (graduate == null)
                {
                    logger.Information($"Graduation information not found for ResidentId { notification.ResidentId } and ProgramEnrollmentId { notification.ProgramEnrollmentId }");

                    return Task.FromResult(0);
                }
                var residentCourseWorkIds = notification.ExaminationScores.Select(x => x.ResidentCourseWorkId).ToList();
                if (!residentCourseWorkIds.Any())
                    return Task.FromResult(0);
                var graduationEligibityItems = unitOfWork.ResidentManagementRepository
                    .FindBy<GraduateEligibilityItem>(x => residentCourseWorkIds.Contains(x.ResidentCourseWorkId.Value)).ToList();


                foreach (var graduationEligibityItem in graduationEligibityItems)
                {
                    var score = notification.ExaminationScores.SingleOrDefault(x => x.ResidentCourseWorkId == graduationEligibityItem.ResidentCourseWorkId);
                    if (score != null)
                    {
                        graduationEligibityItem.Evaluate(score.Score);
                        unitOfWork.ResidentManagementRepository.Update(graduationEligibityItem);
                    }
                }

                unitOfWork.SaveChanges();

                return Task.FromResult(0);
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while evaluating resident graduation criteria for ResidentId { notification.ResidentId } and ProgramEnrollmentId {notification.ProgramEnrollmentId}");

                return Task.FromResult(0);
            }
        }
    }
}
