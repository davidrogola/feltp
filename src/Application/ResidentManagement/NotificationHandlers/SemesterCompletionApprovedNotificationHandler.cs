﻿using Application.ProgramManagement.NotificationHandlers;
using Application.ResidentManagement.Notifications;
using MediatR;
using MySql.Data.MySqlClient;
using ProgramManagement.Domain.Program;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.NotificationHandlers
{
    public class SemesterCompletionApprovedNotificationHandler : INotificationHandler<SemesterCompletionApprovedNotification>
    {
        Func<MySqlConnection> connectionFactory;
        IMediator mediator;
        const string query = @"update_semester_and_residentcoursework_info";
        

        ILogger logger = Log.ForContext<SemesterCompletionApprovedNotificationHandler>();
        public SemesterCompletionApprovedNotificationHandler(Func<MySqlConnection> _connectionFactory, 
            IMediator _mediator)
        {
            connectionFactory = _connectionFactory;
            mediator = _mediator;
        }

        public Task Handle(SemesterCompletionApprovedNotification notification, CancellationToken cancellationToken)
        {

            try
            {
                using (var con = connectionFactory())
                using (var command = new MySqlCommand(query, con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@semesterScheduleId", notification.SemesterScheduleId);
                    command.Parameters.AddWithValue("@semesterId", notification.SemesterId);
                    command.Parameters.AddWithValue("@programOfferId", notification.ProgramOfferId);
                    command.Parameters.AddWithValue("@newStatus", ProgressStatus.Completed);

                    command.Parameters["@semesterScheduleId"].Direction = ParameterDirection.Input;
                    command.Parameters["@semesterId"].Direction = ParameterDirection.Input;
                    command.Parameters["@programOfferId"].Direction = ParameterDirection.Input;
                    command.Parameters["@newStatus"].Direction = ParameterDirection.Input;


                    command.Connection.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            var result = reader.GetBoolean(0);
                        }

                    }
                }

                mediator.Publish(new SemesterCompletedNotification
                {
                    ProgramOfferId = notification.ProgramOfferId,
                    SemesterId = notification.SemesterId
                });

                return Task.FromResult(0);
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while updating semeseter schedule details for semesterscheduleId {notification.SemesterScheduleId} and programOfferId {notification.ProgramOfferId}");

                return Task.FromResult(0);
            }
        }
    }
}
