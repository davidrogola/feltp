﻿using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.NotificationHandlers
{

    public class ThesisDeliverableSubmittedNotification : INotification
    {
        public long ResidentDeliverableId { get; set; }
        public ThesisDefenceStatus ThesisDefenceStatus { get; set; }
        public long ResidentId { get; set; }
        public int ProgramEnrollemntId { get; set; }
    }
    public class ThesisDeliverableSubmittedNotificationHandler : INotificationHandler<ThesisDeliverableSubmittedNotification>
    {
        IResidentManagementUnitOfWork residentManagementUnitOfWork;
        IProgramManagementUnitOfWork programUnitOfWork;

        ILogger logger = Log.ForContext<ThesisDeliverableSubmittedNotificationHandler>();

        public ThesisDeliverableSubmittedNotificationHandler(IResidentManagementUnitOfWork _residentManagementUnitOfWork,
            IProgramManagementUnitOfWork _programUnitOfWork)
        {
            residentManagementUnitOfWork = _residentManagementUnitOfWork;
            programUnitOfWork = _programUnitOfWork;
        }
        public Task Handle(ThesisDeliverableSubmittedNotification notification, CancellationToken cancellationToken)
        {
            try
            {
                var programEnrollment = programUnitOfWork.ProgramManagementRepository.Get<ProgramEnrollment>(notification.ProgramEnrollemntId);

                if (programEnrollment == null)
                    return Task.FromResult(0);

                var residentGraduationInfo = residentManagementUnitOfWork.ResidentManagementRepository.FindBy<ResidentGraduationInfoView>
                    (x => x.ResidentId == notification.ResidentId && x.ProgramOfferId == programEnrollment.ProgramOfferId).SingleOrDefault();

                if (residentGraduationInfo == null)
                    return Task.FromResult(0);

                var thesisDefenceEligiblityItem = residentManagementUnitOfWork.ResidentManagementRepository.FindBy<GraduateEligibilityItem>(x => x.GraduateId == residentGraduationInfo.Id && x.EligibiltyType == EligibilityType.ThesisDefenceSuccesful).SingleOrDefault();

                if (thesisDefenceEligiblityItem == null)
                    return Task.FromResult(0);

                thesisDefenceEligiblityItem.EvaluateNonPassmarkEligiblityItem();

                residentManagementUnitOfWork.ResidentManagementRepository.Update(thesisDefenceEligiblityItem);

                residentManagementUnitOfWork.SaveChanges();

                return Task.FromResult(0);
            }
            catch (Exception ex)
            {

                logger.Error(ex, $"An error occured while evaluating resident thesis graduation eligibity for resident Id {notification.ResidentId} ");
                return Task.FromResult(0);
            }
        }
    }
}
