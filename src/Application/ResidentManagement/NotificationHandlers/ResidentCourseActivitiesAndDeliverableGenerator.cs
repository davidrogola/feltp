﻿using Application.ResidentManagement.Notifications;
using MediatR;
using Newtonsoft.Json;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.FieldPlacement;
using ResidentManagement.Domain;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.NotificationHandlers
{
    public class ResidentCourseActivitiesAndDeliverableGenerator : INotificationHandler<ResidentCourseWorkCreatedNotification>
    {
        IResidentManagementUnitOfWork residentManagementUnitOfWork;
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        ILogger logger = Log.ForContext<ResidentCourseActivitiesAndDeliverableGenerator>();

        public ResidentCourseActivitiesAndDeliverableGenerator(IResidentManagementUnitOfWork residentManagementUnitOfWork,
        IProgramManagementUnitOfWork programManagementUnitOfWork)
        {
            this.residentManagementUnitOfWork = residentManagementUnitOfWork;
            this.programManagementUnitOfWork = programManagementUnitOfWork;
        }
        public Task Handle(ResidentCourseWorkCreatedNotification notification, CancellationToken cancellationToken)
        {

            try
            {
                if (!notification.ResidentUnits.Any())
                {
                    logger.Information($"Resident units not found during deliverable generation for residentId {notification.ResidentId} and EnrollmentId {notification.EnrollmentId}");
                    return Task.FromResult(0);
                }

                var unitIds = notification.ResidentUnits.Select(x => x.UnitId).Distinct();

                var unitfieldPlacementActivityIds = programManagementUnitOfWork.ProgramManagementRepository
                    .FindBy<UnitFieldPlacementActivity>(x => unitIds.Contains(x.UnitId))
                    .Select(x => new
                    {
                        x.FieldPlacementActivityId,
                        x.UnitId
                    }).Distinct();

                if (!unitfieldPlacementActivityIds.Any())
                {
                    logger.Information($"Unit field placement activities not found for Resident {notification.ResidentId} and EnrollmentId {notification.EnrollmentId}");
                    return Task.FromResult(0);
                }

                var residentActivities = new List<ResidentFieldPlacementActivity>();

                foreach (var residentUnit in notification.ResidentUnits)
                {
                    var fieldPlacement = unitfieldPlacementActivityIds.FirstOrDefault(x => x.UnitId == residentUnit.UnitId);
                    if (fieldPlacement == null)
                        continue;
                    var residentFieldPlacementActivity = new ResidentFieldPlacementActivity(residentUnit.ResidentUnitId, fieldPlacement.FieldPlacementActivityId, notification.CreatedBy, PlacementActivityStatus.NotStarted);

                    if (!residentActivities.Contains(residentFieldPlacementActivity))
                        residentActivities.Add(residentFieldPlacementActivity);
                }

                residentManagementUnitOfWork.ResidentManagementRepository.AddRange(residentActivities);
                residentManagementUnitOfWork.SaveChanges();

                var fieldPlacementActivityIds = unitfieldPlacementActivityIds.Select(x => x.FieldPlacementActivityId).ToList();

                var placementActivityDeliverables = programManagementUnitOfWork.ProgramManagementRepository.FindBy<FieldPlacementActivityDeliverable>(x => fieldPlacementActivityIds.Contains(x.FieldPlacementActivityId))
                    .Select(x => new
                    {
                        x.DeliverableId,
                        x.FieldPlacementActivityId
                    }).ToList();

                if(!placementActivityDeliverables.Any())
                {
                    logger.Information($"Deliverables not found for Resident {notification.ResidentId} and EnrollmentId {notification.EnrollmentId} and Field placement activity Ids {JsonConvert.SerializeObject(fieldPlacementActivityIds)}");
                    return Task.FromResult(0);
                }
                var residentDeliverables = new List<ResidentDeliverable>();

                foreach (var placementDeliverable in placementActivityDeliverables)
                {
                    var residentActivityId = residentActivities.FirstOrDefault(x => x.FieldPlacementActivityId == placementDeliverable.FieldPlacementActivityId)?.Id;

                    if (!residentActivityId.HasValue)
                        continue;
                    var residentDeliverable = new ResidentDeliverable(residentActivityId.Value, placementDeliverable.DeliverableId, DeliverableStatus.Pending, notification.CreatedBy);

                    if (!residentDeliverables.Contains(residentDeliverable))
                        residentDeliverables.Add(residentDeliverable);

                }

                residentManagementUnitOfWork.ResidentManagementRepository.AddRange(residentDeliverables);
                residentManagementUnitOfWork.SaveChanges();

                return Task.FromResult(0);
            }
            catch (Exception ex)
            {

                logger.Error(ex, $"An error occured while generating resident deliverables and activities for ResidentId: {notification.ResidentId} and EnrollmentId: {notification.EnrollmentId}");
                throw;
            }
        }


    }
}
