﻿using Application.ResidentManagement.Notifications;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.NotificationHandlers
{
    public class ResidentCourseAttendanceNotificationHandler : INotificationHandler<ResidentCourseAttendanceNotification>
    {
        IResidentManagementUnitOfWork unitOfWork;
        ILogger logger = Log.ForContext<ResidentCourseAttendanceNotificationHandler>();

        public ResidentCourseAttendanceNotificationHandler(IResidentManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task Handle(ResidentCourseAttendanceNotification notification, CancellationToken cancellationToken)
        {
            using (var transaction = unitOfWork.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    var residentUnitId = unitOfWork.ResidentManagementRepository
                        .FindBy<ResidentUnitView>(x => x.UnitId == notification.UnitId && x.ResidentId == notification.ResidentId)
                        .Select(x => x.ResidentUnitId)?.SingleOrDefault();
                    if (!residentUnitId.HasValue)
                        return Task.FromResult(0);

                    var unitAttendance = new ResidentUnitAttendance(residentUnitId.Value, notification.FacultyId, notification.AttendanceDate, notification.CreatedBy, notification.TimetableId, notification.AttendanceStatus);

                    unitOfWork.ResidentManagementRepository.Add(unitAttendance);
                    unitOfWork.SaveChanges();

                    transaction.Commit();

                    return Task.FromResult(0);
                }
                catch (Exception ex)
                {
                    logger.Error(ex, $"Error occured during Attendance creation for ResidentId: {notification.ResidentId} and" +
                        $" UnitID: {notification.UnitId }");
                    throw;
                }
            }

        }
    }
}
