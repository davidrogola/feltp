﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Commands
{
    public class AddResidentCommand : IRequest<long>
    {
        public long ApplicantId { get; set; }
        public string CreatedBy { get; set; }
    }
}
