﻿using MediatR;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain.Ethics;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Application.ResidentManagement.Commands
{
    public class OverrideEthicsApprovalOutcomeCommand : IRequest<long>
    {
        public string Deliverable { get; set; }
        public string Title { get; set; }
        public string EventTypeId { get; set; }
        public long EthicsApprovalId { get; set; }
        public string OverridenBy { get; set; }
        [Required(AllowEmptyStrings =false,ErrorMessage ="Please enter override reason")]
        public string OverrideReason { get; set; }
        [Required(AllowEmptyStrings =false,ErrorMessage ="Please select an approval status")]
        public EthicsApprovalStatus ApprovalStatus { get; set; }
        public string Message { get; set; }
    }
}
