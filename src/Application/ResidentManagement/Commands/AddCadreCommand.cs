﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Commands
{
    public class AddCadreCommand : IRequest<int>
    {
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
    }

    public class UpdateCadreCommand : IRequest<int>

    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
    }

    public class DeactivateCadreCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateDeactivatead { get; set; }
        public string DeactivatedBy { get; set; }
        public string UpdatedBy { get; set; }

    }
}
