﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using ResidentManagement.Domain.Ethics;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Application.ResidentManagement.Commands
{
    public class AddEthicsApprovalCommand : IRequest<long>
    {
        public PaperCategory PaperCategory { get; set; }
        public long ResidentId { get; set; }
        public long ResidentDeliverableId { get; set; }
        public int ProgramEventId { get; set; }
        public int ProgramId { get; set; }
        public int ProgramOfferId { get; set; }
        public SelectList ProgramSelectList { get; set; }
        public DateTime SubmissionExpiryDate { get; set; }
        public DocumentVersion DocumentVersion { get; set; }
        public bool SmsNotification { get; set; }
        public bool EmailNotification { get; set; }
        public string CreatedBy { get; set; }
        public int ProgramEnrollmentId { get; set; }
        public IFormFile FormFile { get; set; }
        public string StrSubmissionExpiryDate { get; set; }
        public List<EthicsApprovalItemCommand> EthicsApprovalItems { get; set; }

    }

    public class EthicsApprovalItemCommand
    {
        public int CommitteeId { get; set; }
        public string  CommitteeName { get; set; }
        public DateTime DateSubmitted { get; set; }               
        public string StrDateSubmitted { get; set; }
        public EthicsApprovalStatus ApprovalStatus { get; set; }
        public string Comment { get; set; }
        public string Author { get; set; }
        public bool Selected { get; set; }
    }

    
}
