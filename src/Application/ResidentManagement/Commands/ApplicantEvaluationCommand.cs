﻿using MediatR;
using Application.ResidentManagement.Models;
using ProgramManagement.Domain.Program;

namespace Application.ResidentManagement.Commands
{
    public class ApplicantEvaluationCommand : IRequest<ApplicantEvaluationResult>
    {
        public long Id { get; set; }
        public ProgramQualificationStatus EvaluationStatus { get; set; }
        public bool ApprovedByCounty { get; set; }
        public string Comment { get; set; }
        public string EvaluatedBy { get; set; }
        public long ApplicantId { get; set; }
        public int ProgramOfferId { get; set; }
        public InterviewScoreModel InterviewScore { get; set; }
    }

    public class ApplicantEvaluationResult
    {
        public long ResidentId { get; set; }
        public ProgramQualificationStatus EvaluationOutCome { get; set; }
        public long ApplicantId { get; set; }
        public int ProgramOfferId { get; set; }
        public string Comment { get; set; }
        public long ProgramOfferApplicationId { get; set; }

    }

    public class ApplicantEvaluatedNotification : INotification
    {
        public long ApplicantId { get; set; }
        public string EmailAddress { get; set; }
        public ProgramQualificationStatus EvaluationOutCome { get; set; }
        public ApplicantEvaluationTemplate EvaluationTemplate { get; set; }
    }

    public class ApplicantEvaluationTemplate
    {
        public string Comment { get; set; }
        public string Name { get; set; }
        public string Program { get; set; }
        public string ProgramOffer { get; set; }

    }
}
