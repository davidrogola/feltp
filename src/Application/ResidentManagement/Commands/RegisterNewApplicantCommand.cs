﻿using Application.Common.Models;
using Application.ResidentManagement.Models;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Application.ResidentManagement.Commands
{
    public class RegisterNewApplicantCommand : IRequest<long>
    {
        public AddPersonCommand  Person { get; set; }
        public StatementFile File { get; set; }
        public bool PersonalStatementSubmitted { get; set; }
        public bool ApprovedByCounty { get; set; }
        public int ProgramOfferId { get; set; }
        public string OfferName { get; set; }
        public string ProgramName { get; set; }
        public bool UniversityFound { get; set; }
        public string CreatedBy { get; set; }

    }

    public class StatementFile
    {
        public StatementFile(byte [] content, string contentType, string fileName)
        {
            File = content;
            ContentType = contentType;
            FileName = fileName;
        }
        public byte[] File { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }
    }

    public class UpdateApplicantBioDataCommand : IRequest<long>
    {
        public UpdatePersonBioDataCommand UpdatePersonBioData { get; set; }
        public long ApplicantId { get; set; }
        public int ProgramOfferId { get; set; }


    }
}
