﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Commands
{
    public class AddResidentDeliverableItemCommand : IRequest<SubmitDeliverableResponse>
    {
        public string Description { get; set; }
        public string Title { get; set; }
        public byte[] File { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateSubmitted { get; set; }
        public string StrDateSubmitted { get; set; }
        public string CreatedBy { get; set; }
        public long ResidentId { get; set; }
        public int ProgramOfferId { get; set; }
        public SubmissionType SubmissionType { get; set; }
        public ProgrammaticArea ProgrammaticArea { get; set; }
        public long ResidentDeliverableId { get; set; }
        public int ProgramEnrollmentId { get; set; }
        public List<SelectListItem> ResidentDeliverables { get; set; }
        public List<AddCoauthorCommand> CoAuthors { get; set; }
        public string Other { get; set; }
        public bool ProgrammaticAreaFound { get; set; }
        public bool HasEthicalApproval { get; set; }
        public int SemesterId { get; set; }
        public bool HasThesisDefence { get; set; }
        public ThesisDefenceStatus ThesisDefenceStatus { get; set; }
        public AddDeliverableItemFileRequest DeliverableItemFile { get; set; }
    }


    public class AddDeliverableItemFileRequest : IRequest<DeliverableFileResponse>
    {
        public long DeliverableItemId { get; set; }
        public IFormFile DeliverableFile { get; set; }

    }


    public class DeliverableFileResponse
    {
        public bool IsSuccesful { get; set; }
        public long DeliverableItemId { get; set; }
        public string Message { get; set; }
        public int ProgramOfferId { get; set; }
        public string SemesterStartDate { get; set; }
        public string SemesterEndDate { get; set; }
        
    }

    public class AddCoauthorCommand
    {
        public string Committee { get; set; }
        public int CommitteeId { get; set; }
        public string AuthorName { get; set; }
        public bool Selected { get; set; }
    }

    public class SubmitDeliverableResponse
    {
        public string Message { get; set; }
        public bool IsSuccessful { get; set; }
        public long ResidentId { get; set; }
        public long DeliverableItemId { get; set; }

    }

    public class SubmitDeliverableTemplate
    {
        public String DeliverableName { get; set; }
        public string DateSubmitted { get; set; }
        public String ResidentName { get; set; }
        public String RegistrationNumber { get; set; }
        public String FieldActivity { get; set; }
        public String SubmissionType { get; set; }
    }
}
