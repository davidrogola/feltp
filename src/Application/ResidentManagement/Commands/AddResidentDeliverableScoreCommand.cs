﻿using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using ProgramManagement.Domain.Examination;

namespace Application.ResidentManagement.Commands
{
    public class AddResidentDeliverableScoreCommand : IRequest<InputResidentExaminationScoreResult>
    {
        public int ProgramEnrollmentId { get; set; }
        public long ResidentId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public int SemesterId { get; set; }
        
        public ExamType ExamType { get; set; }
        public List<ResidentDeliverableDetail> ResidentDeliverables { get; set; }
    }

    public class ResidentDeliverableDetail
    {       
        public long ResidentFieldPlacementActivityId { get; set; }
        public string UnitName { get; set; }
        public string ActivityName { get; set; }
        public long ResidentDeliverableId { get; set; }
        public string Deliverable { get; set; }
        public string Code { get; set; }
        public long ResidentUnitId { get; set; }
        public bool Submitted { get; set; } 
        public decimal ? Score { get; set; }
        public string StrScore { get; set; }
        
        public int CourseId { get; set; }

    }

}
