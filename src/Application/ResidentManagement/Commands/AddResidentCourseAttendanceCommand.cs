﻿using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Commands
{
  public  class AddResidentCourseAttendanceCommand : IRequest<ResidentCourseAttendanceResponse>
    {
        public int FacultyId { get; set; }
        public int TimetableId { get; set; }
        public string StrFacultyTimetableItem { get; set; }
        public DateTime AttendanceDate { get; set; }
        public string CreatedBy { get; set; }
        public int ProgramOfferId { get; set; }
        public int ProgramId { get; set; }
        public List<SelectListItem> AttendanceDates { get; set; }
        public string Status { get; set; }
        public int UnitId { get; set; }
        public SelectList Programs { get; set; }
        public long SemesterScheduleId { get; set; }
        public long CourseScheduleId { get; set; }
        public string StrAttendanceDate { get; set; }
        public List<SelectListItem> FacultyTimeSlots { get; set; }
        public List<SelectListItem> SemeseterScheduleSelectList { get; set; }

    }

    public class ResidentCourseAttendanceResponse
    {
        public string Message { get; set; }
        public long SemesterScheduleItemId { get; set; }
        public int UnitId { get; set; }
        public bool Success { get; set; }
        public string UnitName { get; set; }
        public string Code { get; set; }

    }
}
