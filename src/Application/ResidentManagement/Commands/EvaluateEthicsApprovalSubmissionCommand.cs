﻿using MediatR;
using ResidentManagement.Domain.Ethics;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Application.ResidentManagement.Commands
{
    public class EvaluateEthicsApprovalSubmissionCommand : IRequest<long>
    {
        public long EthicsApprovalItemId { get; set; }
        [Required(AllowEmptyStrings =false,ErrorMessage ="Please select an approval status")]
        public EthicsApprovalStatus ApprovalStatus { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter comment")]
        public string Comment { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime DateEvaluated { get; set; }
        public string EvaluatedBy { get; set; }
        public long EthicsApprovalCommitteeId { get; set; }
        public long EthicsApprovalId { get; set; }
    }
}
