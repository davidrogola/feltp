﻿using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProgramManagement.Domain.FieldPlacement;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Commands
{
    public class AddResidentFieldPlacementCommand : IRequest<AddFieldPlacementResponse>
    {
        public long ResidentFieldPlacementActivityId { get; set; }
        public long ResidentId { get; set; }
        public int ProgramEnrollmentId { get; set; }
        public string StrStartDate { get; set; }
        public string StrEndDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int? FieldPlacementSiteId { get; set; }
        public string Description { get; set; }
        public int? SupervisorId { get; set; }
        public string SiteName { get; set; } 
        public PlacementActivityStatus ProgressStatus { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public SelectList SiteSupervisors { get; set; }
        public SelectList ResidentFieldPlacementActivities { get; set; }
        public string ActivityType { get; set; }
        public int? Faculty { get; set; }
        public int SemesterId { get; set; }
        public int ProgramOfferId { get; set; }
        public string SemesterStartDate { get; set; }
        public string  SemesterEndDate { get; set; }

    }

    public class AddFieldPlacementResponse
    {
        public string Message { get; set; }
        public bool IsSuccessful { get; set; }
        public long ResidentId { get; set; }

    }

}
