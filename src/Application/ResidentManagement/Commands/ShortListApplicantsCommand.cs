﻿using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Application.ResidentManagement.Commands
{
    public class ShortListApplicantCommand : IRequest<ShortListApplicantResult>
    {
        public List<long> ProgramOfferApplicationIds { get; set; }
        public int ProgramId { get; set; }
        public string ProgramOfferApplicationIdsJson { get; set; }
        public string ShortListedBy { get; set; }
        public DateTime DateShortListed { get; set; }
        public int ProgramOfferId { get; set; }
        public ApplicationStatus ApplicationStatus { get; set; }
        public SelectList ProgramSelectList { get; set; }
        public string ProgramName { get; set; }
        public string ProgramOffer { get; set; }
        public long [] Id { get; set; }

    }


    public class ShortListApplicantResult
    {
        public bool CompletedSuccessfully { get; set; }
        public string Message { get; set; }

    }

    public class ApplicantsShortListedNotification : INotification
    {
        public List<long> ProgramOfferApplicationIds { get; set; }
        public string DateShortListed { get; set; }
        public string ProgramName { get; set; }
        public string ProgramOffer { get; set; }

    }

    public class ShortListApplicantTemplate
    {
        public string DateShortListed { get; set; }
        public string Program { get; set; }
        public string ProgramOffer { get; set; }
        public string Name { get; set; }

    }



}
