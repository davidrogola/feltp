﻿using Application.ResidentManagement.QueryHandlers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using ProgramManagement.Domain.Examination;

namespace Application.ResidentManagement.Commands
{
    public class InputResidentExaminationScoreCommand : IRequest<InputResidentExaminationScoreResult>
    {
        public string ExamScheduleDescription{ get; set; }
        public long ResidentId { get; set; }
        
        public int SemesterId { get; set; }

        public int ProgramOfferId { get; set; }
        public int EnrollmentId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public int SemesterScheduleItemId { get; set; }
        public ResidentExamDetailResponse ExamDetailResponse { get; set; }
        public ExamType ExamType { get; set; }
        public string ResidentName { get; set; }
        public string RegistrationNumber { get; set; }
        public bool IsCat { get; set; }
    }

    public class InputResidentExaminationScoreResult
    {

        public string Message { get; set; }
        public bool IsSuccessful { get; set; }
        public long ResidentId { get; set; }

    }


    public class ResidentExamDetail
    {
        public string StrExamType { get; set; }
        public ExamType ExamType { get; set; }
        public long ResidentUnitId { get; set; }

        public long ResidentCourseWorkId { get; set; }
        public int ExaminationId { get; set; }
        public int UnitId { get; set; }
        public int TimetableId { get; set; }
        public Decimal ? Score { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool Submitted { get; set; }
        public string StrScore { get; set; }

    }
}
