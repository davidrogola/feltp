using MediatR;

namespace Application.ResidentManagement.Commands
{
    public class SubmitDeliverableEvaluationCommand : IRequest<string>
    {
        public int Id { get; set; }
        public long ResidentId { get; set; }
        public string DeliverableName { get; set; }
        public string Message { get; set; }
        public string Faculty { get; set; }
    }
}