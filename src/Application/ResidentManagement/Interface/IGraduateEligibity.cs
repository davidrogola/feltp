﻿using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;


namespace Application.ResidentManagement.Interface
{
    public interface IGraduateEligibityGenerator
    {
        List<GraduateEligibilityItem> GenerateEligibityItems(EligibityDetails eligibityDetails);

    }

    public class EligibityDetails
    {
        public IEnumerable<EligibilityType> EligibilityTypes { get; set; }
        public long GraduateId { get; set; }
        public int NumberOfDidacticModulesRequired { get; set; }
        public int NumberOfActivitiesRequired { get; set; }
        public int NumberOfThesisDefenceDeliverables { get; set; }
        public int PassingGrade { get; set; }
        public List<long> ResidentUnitIds { get; set; }
        public string CreatedBy { get; set; }
        
        public List<long> ResidentCourseWorkIds { get; set; }

    }

}
