﻿using Application.ProgramManagement.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace Application.ResidentManagement.Queries
{
    public class GetApplicantListQuery : IRequest<List<ProgramOfferApplicationViewModel>>
    {
        public object Id { get; set; }
        public GetBy GetApplicantBy { get; set; }
        public bool IsResidentList { get; set; }
    }

    public enum GetBy
    {
        All,
        Program,
        ProgramOffer,
        ResidentId
    }
    public class GetResidentListQuery : IRequest<List<ProgramOfferApplicationViewModel>>
    {
        public GetBy GetResidentBy { get; set; }
        public object Id { get; set; }


    }


    public class FilterResident
    {
        public int ProgramOfferId { get; set; }
        public int ProgramId { get; set; }
        public SelectList ProgramSelectList { get; set; }

    }

}
