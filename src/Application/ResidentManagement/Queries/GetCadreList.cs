﻿using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Application.ResidentManagement.Queries
{
   public class GetCadreList : IRequest<List<CadreViewModel>>
    {
        public int Id { get; set; }
    }
    public class CadreViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; private set; }
        public string Status { get; set; }

    }

}
