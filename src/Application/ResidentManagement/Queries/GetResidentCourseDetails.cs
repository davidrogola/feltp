﻿using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProgramManagement.Domain.FieldPlacement;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Queries
{
    public class GetResidentCourseDetails : IRequest<ResidentCourseDetailViewModel>
    {
        public long ResidentId { get; set; }
        public int ProgramOfferId { get; set; }
    }

    public class ResidentCourseDetailViewModel
    {
        public string Program { get; set; }
        public string ProgramTier { get; set; }
        public string ProgramOffer { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Duration { get; set; }
        public string DateEnrolled { get; set; }
        public string EnrolledBy { get; set; }
        public int ProgramEnrollemntId { get; set; }
        public long ResidentId { get; set; }
        public string RegistrationNumber { get; set; }
        public string ResidentName { get; set; }
        public int ProgramOfferId { get; set; }
        public long ApplicantId { get; set; }
        public int ProgramId { get; set; }
        public int SemesterId { get; set; }
        public List<SelectListItem> SemesterDropDown { get; set; }
        public ExamDetails ExaminationDetails { get; set; }
    }

    public class ExamDetails
    {
        public int? SemesterScheduleId { get; set; }
        public bool ExamCompleted { get; set; }
        public int? ExaminationScheduleId { get; set; }
        public bool AnyExamPendingSubmission { get; set; }
        public bool AnyDeliverablePendingSubmission { get; set; }
    }


    public class GetResidentCourseWork : IRequest<List<ResidentCourseWorkViewModel>>
    {
        public int EnrollmentId { get; set; }
        public int? SemesterId { get; set; }
    }

    public class ResidentCourseWorkViewModel
    {
        public int ProgramEnrollmentId { get; set; }
        public string Course { get; set; }
        public string ProgressStatus { get; set; }
        public int? TotalScore { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string CourseType { get; set; }
        public long ResidentCourseWorkId { get; set; }
        public string Semester { get; set; }
    }

    public class GetResidentFieldPlacementActivities : IRequest<List<ResidentFieldPlacementActivityViewModel>>
    {
        public int ProgramEnrollmentId { get; set; }
        public int? SemesterId { get; set; }
    }

    public class ResidentFieldPlacementActivityViewModel
    {
        public long Id { get; set; }
        public string ActivityType { get; set; }
        public string Activity { get; set; }
        public string ProgressStatus { get; set; }
        public string EvaluatedBy { get; set; }
        public string DateCreated { get; set; }
        public int SemesterId { get; set; }
        public string Semester { get; set; }
        public long ResidentId { get; set; }
    }

    public class GetResidentUnits : IRequest<List<ResidentUnitViewModel>>
    {
        public long ResidentCourseWorkId { get; set; }
    }

    public class ResidentUnitViewModel
    {
        public long Id { get; set; }
        public string Unit { get; set; }
        public int UnitId { get; set; }
        public long ResidentCourseWorkId { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Course { get; set; }
        public string Code { get; set; }
    }

    public class GetResidentDeliverables : IRequest<List<ResidentDeliverableViewModel>>
    {
        public long? ResidentFieldPlacementActivityId { get; set; }

        public int? ProgramEnrollmentId { get; set; }

        public int? SemesterId { get; set; }
    }

    public class GetResidentDeliverable : IRequest<ResidentDeliverableViewModel>
    {
        public long Id { get; set; }
    }

    public class ResidentDeliverableViewModel
    {
        public long ResidentFieldPlacementActivityId { get; set; }
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ProgressStatus { get; set; }
        public string EvaluatedBy { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Activity { get; set; }
        public bool HasThesisDefence { get; set; }
        public long ResidentId { get; set; }
        public ThesisDefenceStatus? ThesisDefenceStatus { get; set; }
        public int CourseId { get; set; }
    }
}