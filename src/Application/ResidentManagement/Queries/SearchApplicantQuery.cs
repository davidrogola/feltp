﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Queries
{
    public class SearchApplicantQuery : IRequest<List<ApplicantSearchResult>>
    {
        public string IdentificationNumber { get; set; }
        public int OfferId { get; set; }
    }

    public class ApplicantSearchResult
    {
        
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Cadre { get; set; }
        public string IdentificationNumber { get; set; }
        public string CreatedBy { get; set; }
        public long ApplicantId { get; set; }
        public string Name { get { return FirstName + " " + LastName; } set { } }
    }
}
