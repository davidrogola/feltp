﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Queries
{
    public class GetResidentPlacementInformation : IRequest<ResidentFieldPlacementInfoViewModel>
    {
        public long ResidentFieldPlacementActivityId { get; set; }
    }

    public class GetResidentFieldPlacementSites : IRequest<List<ResidentFieldPlacementInfoViewModel>>
    {
        public long ResidentId { get; set; }
        public long ApplicantId { get; set; }
    }

    public class ResidentFieldPlacementInfoViewModel
    {
        public long Id { get; set; }
        public long  ResidentFieldPlacementActivityId { get; set; }
        public long ResidentId { get; set; }   
        public string ResidentName { get; set; }
        public string SiteName { get; set; }
        public string Supervisor { get; set; }
        public string ActivityName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Description { get; set; }
        public string ProgressStatus { get; set; }
        public string PlacedBy { get; set; }
        public string ActivityType { get; set; }
        public string Faculty { get; set; }
    }
}
