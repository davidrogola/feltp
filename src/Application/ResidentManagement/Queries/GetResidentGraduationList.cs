﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Queries
{
    public class GetResidentGraduationList : IRequest<List<ResidentGraduationViewModel>>
    {
        public int ProgramOfferId { get; set; }

    }

    public class GetGraduate : IRequest<ResidentGraduationViewModel>
    {
        public long GraduateId { get; set; }

    }
   

    public class ResidentGraduationViewModel
    {
        public long Id { get; set; }
        public string ResidentName { get; set; }
        public string ProgramName { get; set; }
        public string ProgramOffer { get; set; }
        public int ProgramOfferId { get; set; }
        public int GraduationRequirementId { get; set; }
        public long ResidentId { get; set; }
        public long ApplicantId { get; set; }
        public string RegistrationNumber { get; set; }
        public int ProgramEnrollmentId { get; set; }
        public string QualificationStatus { get; set; }
        public string CompletionStatus { get; set; }
        public string ExpectedProgramEndDate { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string EvaluatedBy { get; set; }
    }
}
