﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;

namespace Application.ResidentManagement.Queries
{
    public class GetApplicantCourseInformation : IRequest<ResidentCourseDetailViewModel>
    {
        public long ApplicantId { get; set; }

    }

    public class GetApplicantCourseInformationQueryHandler : IRequestHandler<GetApplicantCourseInformation, ResidentCourseDetailViewModel>
    {
        IProgramManagementUnitOfWork programUnitOfWork;
        IResidentManagementUnitOfWork residentUnitOfWork;
        public GetApplicantCourseInformationQueryHandler(IProgramManagementUnitOfWork _programUnitOfWork, 
            IResidentManagementUnitOfWork _residentUnitOfWork)
        {
            programUnitOfWork = _programUnitOfWork;
            residentUnitOfWork = _residentUnitOfWork;
        }
        public Task<ResidentCourseDetailViewModel> Handle(GetApplicantCourseInformation request, CancellationToken cancellationToken)
        {
            ProgramOfferApplicants validOffer = null;

            var programOfferApplications = programUnitOfWork.ProgramManagementRepository.FindBy<ProgramOfferApplicants>(x => x.ApplicantId == request.ApplicantId && x.EnrollmentStatus == EnrollmentStatus.Enrolled).OrderBy(x=>x.Id).ToList();

            if (!programOfferApplications.Any())
                return Task.FromResult(new ResidentCourseDetailViewModel { });

            foreach (var offer in programOfferApplications)
            {
                var completionInfo = residentUnitOfWork.ResidentManagementRepository.FindBy<Graduate>(x => x.ResidentId == offer.ResidentId && x.ProgramEnrollmentId == offer.EnrollmentId).FirstOrDefault();
                if (completionInfo == null)
                    continue;

                if(completionInfo.CompletionStatus != CompletionStatus.Completed)
                {
                    validOffer = offer;
                    break;
                }
            }

            return Task.FromResult(BuildCourseDetailsModel(validOffer));

        }

        private ResidentCourseDetailViewModel BuildCourseDetailsModel(ProgramOfferApplicants offerApplication)
        {
            if (offerApplication == null)
                return new ResidentCourseDetailViewModel();

            var programOffer = programUnitOfWork.ProgramManagementRepository
                .FindByWithInclude<ProgramOffer>(x => x.Id == offerApplication.ProgramOfferId, x => x.Program.ProgramTier).SingleOrDefault();

            var semestersDropdown = programUnitOfWork.ProgramManagementRepository
                .FindByWithInclude<ProgramCourse>(x => x.ProgramId == offerApplication.ProgramId, x => x.Semester)
                .GroupBy(x => x.SemesterId)
                .Select(x => x.FirstOrDefault())
                .Select(x => new SelectListItem
                {
                    Value = x.SemesterId.ToString(),
                    Text = x.Semester.Name
                }).ToList();

            var courseDetails = new ResidentCourseDetailViewModel
            {
                SemesterDropDown = semestersDropdown,
                ApplicantId = offerApplication.ApplicantId,
                DateEnrolled = offerApplication.DateEvaluated.Value.ToShortDateString(),
                Duration = programOffer.Program.ConvertDurationToMonths(),
                Program = offerApplication.Program,
                ProgramOffer = offerApplication.ProgramOfferName,
                ProgramOfferId = offerApplication.ProgramOfferId,
                ProgramId = offerApplication.ProgramId,
                ProgramTier = programOffer.Program.ProgramTier.Name,
                StartDate = programOffer.StartDate.ToShortDateString(),
                EndDate = programOffer.EndDate.ToShortDateString(),
                EnrolledBy = offerApplication.EvaluatedBy,
                RegistrationNumber = offerApplication.RegistrationNumber,
                ResidentName = $"{offerApplication.FirstName} {offerApplication.LastName}",
                ProgramEnrollemntId = offerApplication.EnrollmentId.Value,
                ResidentId = offerApplication.ResidentId.Value
            };

            return courseDetails;
        }
    }
}
