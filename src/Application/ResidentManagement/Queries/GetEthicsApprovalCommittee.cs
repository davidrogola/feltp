﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Queries
{
    public class GetEthicsApprovalCommittee : IRequest<List<EthicsApprovalCommitteeViewModel>>
    {
        public int ?  Id { get; set; }
    }

    public class EthicsApprovalCommitteeViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }
}
