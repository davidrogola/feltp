﻿using MediatR;
using System;

using Application.Common.Models;
using Application.ResidentManagement.Models;
using Application.ProgramManagement.Queries;

namespace Application.ResidentManagement.Queries
{
    public class GetApplicantQuery : IRequest<ApplicantViewModel>
    {
        public long Id { get; set; }
        public int ? ProgramOfferId { get; set; }
    }

    public class GetApplicantByIdNumber : IRequest<ProgramOfferApplicationViewModel>
    {
        public string IdentificationNumber { get; set; }
    }

    public class ApplicantViewModel
    {
        public long Id { get; set; }
        public string Cadre { get; set; }
        public virtual PersonViewModel Person { get; set; }
        public InterviewScoreModel InterViewScore { get; set; }
        public string  DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public bool IsResident { get; set; }
        public long ResidentId { get; set; }
        public int ProgramEnrollmentId { get; set; }
        public string RegistrationNumber { get; set; }
        public bool IsEnrolled { get; set; }
        public string DateRegistered { get; set; }
        public string RegisteredBy { get; set; }
        public bool IsApplicantPartial { get; set; }
        public int ProgramOfferId { get; set; }
        public bool CourseWorkGenerated { get; set; }


    }

   
}
