﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Queries
{
    public class GetEthicsApprovalQuery : IRequest<List<EthicsApprovalViewModel>>
    {
        public int ? ProgramEnrollmentId { get; set; }
        public long ? ResidentId { get; set; }
        public long ? Id { get; set; }
    }
    public class EthicsApprovalViewModel
    {
        public long Id { get; set; }
        public long ResidentId { get; set; }
        public long  ResidentDeliverableId { get; set; }
        public string PaperCategory { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string ResidentName { get; set; }
        public string SubmissionDueDate { get; set; }
        public string EnableEmailNotification { get; set; }
        public string EnableSmsNotification { get; set; }
        public string ApprovalStatus { get; set; }
        public int ProgramEnrollmentId { get; set; }
        public string DeliverableName { get; set; }
        public string Title { get; set; }
        public string EventTypeId { get; set; }
    }

    public class EthicsSubmissionViewModel
    {
        public string DeliverableName { get; set; }
        public string Title { get; set; }
        public string EventTypeId { get; set; }
        public string SubmissionDueDate { get; set; }
        public string Committee { get; set; }
        public int CommitteeId { get; set; }
        public long EthicsApprovalId { get; set; }
    }
}
