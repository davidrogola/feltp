﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Queries
{
    public class GetResidentDeliverableCoauthors : IRequest<List<ResidentDeliverableCoAuthorsViewModel>>
    {
        public long ? ResidentDeliverableId { get; set; }
    }

    public class ResidentDeliverableCoAuthorsViewModel
    {
        public string AuthorName { get; set; }
        public string CommitteeName { get; set; }
        public int CommitteeId { get; set; }
    }
}
