﻿using MediatR;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Queries
{
    public class GetResidentGraduateEligibiltyItems : IRequest<ResidentGraduationEligibityItems>
    {
        public long GraduateId { get; set; }

    }

    public class GraduateEligibityItemsViewModel
    {
        public long Id { get; set; }
        public long GraduateId { get; set; }
        public decimal ExpectedValue { get; set; }
        public decimal ActualValue { get; set; }
        public string OutcomeStatus { get; set; }
        public string StrEligibiltyType { get; set; }
        public EligibilityType EligibiltyType { get; set; }
        public string FailureReason { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public long? ResidentUnitId { get; set; }
        
        public long ? ResidentCourseWorkId { get; set; }
        public string CourseName { get; set; }
        public string Code { get; set; }
        public string Semester { get; set; }
        public int? SemesterId { get; set; }
    }

    public class ResidentGraduationEligibityItems
    {
        public ResidentGraduationViewModel ResidentGraduationView { get; set; }

        public List<GraduateEligibityItemsViewModel> GraduateEligibiltyItems { get; set; }

    }
}
