﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Queries
{
    public class GetEthicsApprovalItems : IRequest<List<EthicsApprovalItemViewModel>>
    {
        public  long ? EthicsApprovalId { get; set; }
        public long ? EthicsApprovalItemId { get; set; }
    }
    public class EthicsApprovalItemViewModel
    {
        public int Id { get; set; }
        public long EthicsApprovalId { get; set; }
        public int EthicsApprovalCommitteeId { get; set; }
        public long EthicsApprovalDocumentId { get; set; }
        public string SubmissionDate { get; set; }
        public string SubmittedBy { get; set; }
        public string ApprovalStatus { get; set; }
        public string DateCreated { get; set; }
        public string Comment { get; set; }
        public int CommitteeId { get; set; }
        public string Committee { get; set; }
        public string DocumentVersion { get; set; }
  

    }
}
