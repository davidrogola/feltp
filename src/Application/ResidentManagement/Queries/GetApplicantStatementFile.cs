﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Queries
{
    public class GetApplicantStatementFile : IRequest<PersonalStatementFileViewModel>
    {
        public long ProgramOfferApplicationId { get; set; }
    }

    public class PersonalStatementFileViewModel
    {
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte [] FileContents { get; set; }

    }
}
