﻿using MediatR;
using ResidentManagement.Domain.Ethics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Queries
{
    public class GetEthicsApprovalDocumentQuery : IRequest<List<EthicsApprovalDocumentViewModel>>
    {
        public long ?  Id { get; set; }
    }

    public class EthicsApprovalDocumentViewModel
    {
        public long Id { get; set; }
        public byte[] Document { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public DateTime SubmissionDate { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DocumentVersion DocumentVersion { get; set; }
    }
}
