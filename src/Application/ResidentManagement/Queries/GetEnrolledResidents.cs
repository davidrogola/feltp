﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Queries
{
    public class GetEnrolledResidents : IRequest<List<EnrolledResidentsViewModel>>
    {
        public int ProgramOfferId { get; set; }
    }

    public class EnrolledResidentsViewModel
    {
        public int ProgramOfferId { get; set; }
        public string RegistrationNumber { get; set; }
        public long ? ResidentId { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string ResidentName { get; set; }
        public int ? EnrollmentId { get; set; }

    }

}
