﻿using MediatR;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Queries
{
    public class GetResidentUnitActivityDeliverableQuery : IRequest<List<ResidentUnitActivityDeliverableViewModel>>
    {
        public int ProgramEnrollmentId { get; set; }
        public int SemesterId { get; set; }

    }

    public class GetResidentUnitDeliverables : IRequest<List<ResidentUnitActivityDeliverableViewModel>>
    {
        public long ResidentUnitId { get; set; }

    }

    public class ResidentUnitActivityDeliverableViewModel
    {
        public long ResidentId { get; set; }
        public long ResidentFieldPlacementActivityId { get; set; }
        public int ProgramEnrollmentId { get; set; }
        public string UnitName { get; set; }
        public string ActivityName { get; set; }
        public long ResidentDeliverableId { get; set; }
        public string Deliverable { get; set; }
        public string Code { get; set; }
        public long ResidentUnitId { get; set; }
        public int SemesterId { get; set; }
        public string Semester { get; set; }
        public decimal ? Score { get; set; }
        public string ProgressStatus { get; set; }

    }
}
