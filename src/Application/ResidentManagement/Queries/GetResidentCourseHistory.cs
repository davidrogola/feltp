﻿using MediatR;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Queries
{
    public class GetResidentCourseHistory : IRequest<List<CourseHistoryViewModel>>
    {
        public long  ResidentId { get; set; }
    }

    public class CourseHistoryViewModel
    {
        public long Id { get; set; }
        public long ResidentId { get; set; }
        public int ProgramEnrollmentId { get; set; }
        public string QualificationStatus { get; set; }
        public string CompletionStatus { get; set; }
        public string ExpectedProgramEndDate { get; set; }
        public string EnrollmentDate { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Program { get; set; }
        public string Tier { get; set; }
    }
}
