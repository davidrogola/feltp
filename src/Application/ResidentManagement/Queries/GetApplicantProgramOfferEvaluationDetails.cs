﻿using Application.ProgramManagement.Queries;
using Application.ResidentManagement.Models;
using MediatR;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Queries
{
    public class GetApplicantProgramOfferEvaluationDetails : IRequest<ApplicantEvaluationViewModel>
    {
        public long ProgramOfferApplicationId { get; set; }
        public long ApplicantId { get; set; }

    }

    public class ApplicantEvaluationViewModel
    {
        public bool ReadMode { get; set; }
        public ProgramOfferApplicationViewModel ProgramOfferApplicationDetail { get; set; }
        public ApplicationStatus EvaluationStatus { get; set; }
        public string Comment { get; set; }
        public InterviewScoreModel InterviewScore { get; set; }
        public bool ApprovedByCounty { get; set; }
        
    }
}
