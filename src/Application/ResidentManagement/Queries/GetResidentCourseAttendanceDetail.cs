﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Queries
{

    public enum GetTimetableBy
    {
        ResidentId,
        CourseWorkId,
        SemesterScheduleItemId

    }
    public class GetResidentCourseAttendance : IRequest<List<ResidentUnitAttendanceViewModel>>
    {
        public long Id { get; set; }
        public GetTimetableBy GetBy { get; set; }
    }
    public class ResidentUnitAttendanceViewModel
    {
        public string Trainer { get; set; }
        public string AttendanceDate { get; set; }
        public string Status { get; set; }
        public long ResidentId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Course { get; set; }
        public string ResidentName { get; set; }
        public string RegistrationNumber { get; set; }
        public string Unit { get; set; }
        public string Code { get; set; }
        public int UnitId { get; set; }
        public long ResidentCourseWorkId { get; set; }
    }

}
