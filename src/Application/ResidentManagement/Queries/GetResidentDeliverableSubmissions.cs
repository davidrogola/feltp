﻿using MediatR;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Queries
{
    public class GetResidentDeliverableSubmissions : IRequest<List<ResidentDeliverableSubmissionViewModel>>
    {
        public long ResidentDeliverableId { get; set; }
       
    }

    public class GetDeliverableSubmission : IRequest<ResidentDeliverableSubmissionViewModel>
    {
        public long SubmissionId { get; set; }
    }

    public class ResidentDeliverableSubmissionViewModel
    {
        public long Id { get; set; }
        public string Description { get; set; }
        public string DeliverableName { get; set; }
        public string Title { get; set; }
        public byte [] File { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public string DateCreated { get; set; }
        public string DateSubmitted { get; set; }
        public string CreatedBy { get; set; }
        public long ResidentId { get; set; }
        public string SubmissionType { get; set; }
        public long ResidentDeliverableId { get; set; }
        public string ProgrammaticArea { get; set; }
    }
}
