﻿using Application.ProgramManagement.Commands;
using Application.ResidentManagement.Commands;
using Application.ResidentManagement.Notifications;
using AutoMapper;
using MediatR;
using Newtonsoft.Json;
using Persistance.Generic;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.Course;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Globalization;
using Application.Common.Services;
using Serilog;
using Application.ProgramManagement.Queries;

namespace Application.ProgramManagement.CommandHandlers
{
    public class AddResidentCourseAttendanceCommandHandler : IRequestHandler<AddResidentCourseAttendanceCommand, ResidentCourseAttendanceResponse>
    {       
        IMediator mediator;
        ILogger logger = Log.ForContext<AddResidentCourseAttendanceCommandHandler>();

        public AddResidentCourseAttendanceCommandHandler(IMediator _mediator)
        {
           
            mediator = _mediator;
        }

        public async Task<ResidentCourseAttendanceResponse> Handle(AddResidentCourseAttendanceCommand request, CancellationToken cancellationToken)
        {

            try
            {
                var attendanceStatus = JsonConvert.DeserializeObject<Dictionary<long, AttendanceStatus>>(request.Status);

                var facultyTimetableItemSplit = request.StrFacultyTimetableItem.Split('-');

                foreach (var attendance in attendanceStatus)
                {
                   await mediator.Publish(new ResidentCourseAttendanceNotification
                    {
                        AttendanceStatus = attendance.Value,
                        ResidentId = attendance.Key,
                        AttendanceDate = request.StrAttendanceDate.ToLocalDate(),
                        TimetableId = Convert.ToInt16(facultyTimetableItemSplit[1]),
                        CreatedBy = request.CreatedBy,
                        FacultyId = Convert.ToInt16(facultyTimetableItemSplit[0]),
                        UnitId = request.UnitId,
                    });
                }



                return new ResidentCourseAttendanceResponse
                {
                    Message = "Residents attendance information updated sucessfully",
                    SemesterScheduleItemId = request.CourseScheduleId,
                    Success = true,
                    UnitId = request.UnitId
                };

            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while updating residents attendance details for " +
                    $"course schedule{request.CourseScheduleId} and unit Id {request.UnitId}");

                return new ResidentCourseAttendanceResponse
                {
                    UnitId = request.UnitId,
                    Message = "An error occured while proccessing your request. Please try again later",
                    SemesterScheduleItemId = request.CourseScheduleId
                };
            }
        }
    }


}

