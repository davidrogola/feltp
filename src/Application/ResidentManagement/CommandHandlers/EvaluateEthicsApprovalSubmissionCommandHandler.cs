﻿using Application.ResidentManagement.Commands;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain.Ethics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.CommandHandlers
{
    public class EvaluateEthicsApprovalSubmissionCommandHandler : IRequestHandler<EvaluateEthicsApprovalSubmissionCommand, long>
    {
        IResidentManagementUnitOfWork unitOfWork;
        public EvaluateEthicsApprovalSubmissionCommandHandler(IResidentManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<long> Handle(EvaluateEthicsApprovalSubmissionCommand request, CancellationToken cancellationToken)
        {
            var ethicsApprovalItem = unitOfWork.ResidentManagementRepository
                .Get<EthicsApprovalItem>(request.EthicsApprovalItemId);

            if (ethicsApprovalItem == null)
                return Task.FromResult(default(long));

            ethicsApprovalItem.Evaluate(request.EvaluatedBy, request.Comment, request.EvaluatedBy, request.ApprovalStatus);

            unitOfWork.ResidentManagementRepository.Update(ethicsApprovalItem);
            unitOfWork.SaveChanges();

            var anyItemPendingApproval = unitOfWork.ResidentManagementRepository
                .FindBy<EthicsApprovalItem>(x => x.EthicsApprovalId == ethicsApprovalItem.EthicsApprovalId
                && x.ApprovalStatus != EthicsApprovalStatus.Approved).Any();

            if (!anyItemPendingApproval)
            {
                var ethicsApproval = unitOfWork.ResidentManagementRepository
                    .Get<EthicsApproval>(ethicsApprovalItem.EthicsApprovalId);

                ethicsApproval.UpdateApproval(EthicsApprovalStatus.Approved);

                unitOfWork.ResidentManagementRepository.Update(ethicsApproval);
                unitOfWork.SaveChanges();
            }

            return Task.FromResult(request.EthicsApprovalItemId);
        }
    }
}
