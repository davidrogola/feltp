﻿using Application.Common.Notifications;
using Application.Common.Services;
using Application.ResidentManagement.Commands;
using Application.ResidentManagement.Models;
using Common.Domain.Messaging;
using MediatR;
using Persistance.LookUpRepo.UnitOfWork;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.CommandHandlers
{
    public class ApplicantEvaluationCommandHandler : IRequestHandler<ApplicantEvaluationCommand, ApplicantEvaluationResult>
    {
        IResidentManagementUnitOfWork unitOfWork;
        IProgramManagementUnitOfWork programUnitOfWork;
        IMediator mediator;
        public ApplicantEvaluationCommandHandler(IResidentManagementUnitOfWork _unitOfWork,
            IMediator _mediator, IProgramManagementUnitOfWork _programUnitOfWork)
        {
            unitOfWork = _unitOfWork;
            programUnitOfWork = _programUnitOfWork;
            mediator = _mediator;
        }
        public async Task<ApplicantEvaluationResult> Handle(ApplicantEvaluationCommand request, CancellationToken cancellationToken)
        {
            ApplicantEvaluationResult evaluationResult = null;

            var programOfferApplication = programUnitOfWork.ProgramManagementRepository.Get<ProgramOfferApplication>(request.Id);

            switch (request.EvaluationStatus)
            {
                case ProgramQualificationStatus.Pending_Evaluation:
                    {

                        if (programOfferApplication != null)
                        {
                            PersistInterviewScore(request.InterviewScore, programOfferApplication.Id);
                        }

                        evaluationResult = new ApplicantEvaluationResult()
                        {
                            ApplicantId = request.ApplicantId,
                            EvaluationOutCome = ProgramQualificationStatus.Pending_Evaluation,
                            ProgramOfferId = programOfferApplication.ProgramOfferId,
                            ProgramOfferApplicationId = programOfferApplication.ProgramOfferId
                        };
                        break;
                    }
                case ProgramQualificationStatus.Qualified:
                    {
                        long residentId = 0;
                        var resident = unitOfWork.ResidentManagementRepository.FindBy<Resident>(x => x.ApplicantId == request.ApplicantId).SingleOrDefault();

                        if (resident == null)
                        {
                            residentId = await mediator.Send(new AddResidentCommand
                            {
                                ApplicantId = request.ApplicantId,
                                CreatedBy = request.EvaluatedBy
                            });
                        }
                        else
                        {
                            residentId = resident.Id;
                        }

                        if (programOfferApplication != null)
                        {
                            PersistInterviewScore(request.InterviewScore, programOfferApplication.Id);

                            programOfferApplication.Evaluate(ProgramQualificationStatus.Qualified, request.EvaluatedBy, request.Comment, request.ApprovedByCounty);
                            programUnitOfWork.ProgramManagementRepository.Update(programOfferApplication);
                            programUnitOfWork.SaveChanges();
                        }

                        evaluationResult = new ApplicantEvaluationResult()
                        {
                            ApplicantId = request.ApplicantId,
                            EvaluationOutCome = ProgramQualificationStatus.Qualified,
                            ResidentId = residentId,
                            ProgramOfferId = programOfferApplication.ProgramOfferId,
                            ProgramOfferApplicationId = programOfferApplication.Id
                        };
                        break;
                    }
                case ProgramQualificationStatus.NotQualified:
                    {

                        if (programOfferApplication != null)
                        {
                            PersistInterviewScore(request.InterviewScore, programOfferApplication.Id);

                            programOfferApplication.Evaluate(ProgramQualificationStatus.NotQualified, request.EvaluatedBy, request.Comment, request.ApprovedByCounty);
                            programUnitOfWork.ProgramManagementRepository.Update(programOfferApplication);
                            programUnitOfWork.SaveChanges();
                        }

                        evaluationResult = new ApplicantEvaluationResult()
                        {
                            ApplicantId = request.ApplicantId,
                            EvaluationOutCome = ProgramQualificationStatus.NotQualified,
                            ProgramOfferId = programOfferApplication.ProgramOfferId,
                            ProgramOfferApplicationId = programOfferApplication.Id
                        };
                        break;
                    }
                default:
                    break;
            }
            if (evaluationResult.EvaluationOutCome != ProgramQualificationStatus.Pending_Evaluation)
                await PublishApplicantEvaluatedNotification(evaluationResult);

            return evaluationResult;
        }

        private async Task PublishApplicantEvaluatedNotification(ApplicantEvaluationResult evaluationResult)
        {
            var programOfferApplicant = programUnitOfWork.ProgramManagementRepository.FindBy<ProgramOfferApplicants>
                (x => x.Id == evaluationResult.ProgramOfferApplicationId).FirstOrDefault();
            if (programOfferApplicant == null)
                return;

            var evaluatedNotification = new ApplicantEvaluatedNotification
            {
                ApplicantId = evaluationResult.ApplicantId,
                EmailAddress = programOfferApplicant.EmailAddress,
                EvaluationOutCome = evaluationResult.EvaluationOutCome,
                EvaluationTemplate = new ApplicantEvaluationTemplate
                {
                    Comment = evaluationResult.Comment,
                    Name = $"{programOfferApplicant.FirstName} {programOfferApplicant.LastName}",
                    Program = programOfferApplicant.Program,
                    ProgramOffer = programOfferApplicant.ProgramOfferName
                }
            };

            await mediator.Publish(evaluatedNotification);
        }

        private void PersistInterviewScore(InterviewScoreModel interviewScore, long offerApplicationId)
        {
            InterviewScore score = new InterviewScore(offerApplicationId, interviewScore.OralInterview, interviewScore.WrittenInterview);

            unitOfWork.ResidentManagementRepository.Add(score);
            unitOfWork.SaveChanges();
        }
    }


    public class ApplicantEvaluatedNotificationHandler : INotificationHandler<ApplicantEvaluatedNotification>
    {
        ILookUpUnitOfWork unitOfWork;
        IEmailTemplateBuilder emailTemplateBuilder;
        ILogger logger = Log.ForContext<ApplicantsShortListedNoticationHandler>();
        IMediator mediator;
        public ApplicantEvaluatedNotificationHandler(ILookUpUnitOfWork _unitOfWork,
            IEmailTemplateBuilder _emailTemplateBuilder, IMediator _mediator)
        {
            unitOfWork = _unitOfWork;
            emailTemplateBuilder = _emailTemplateBuilder;
            mediator = _mediator;
        }
        public async Task Handle(ApplicantEvaluatedNotification notification, CancellationToken cancellationToken)
        {
            try
            {
                MessageTypeEnum messageType = MessageTypeEnum.ApplicantFailedEvaluation;

                switch (notification.EvaluationOutCome)
                {
                    case ProgramQualificationStatus.Qualified:
                        messageType = MessageTypeEnum.ApplicantSuccessfullyEvaluated;
                        break;
                    case ProgramQualificationStatus.NotQualified:
                        messageType = MessageTypeEnum.ApplicantFailedEvaluation;
                        break;
                    case ProgramQualificationStatus.Waiting_List:
                        messageType = MessageTypeEnum.ApplicantWaitingList;
                        break;
                    default:
                        break;
                }
                var messageTemplate = unitOfWork.LookUpRepository.FindBy<MessageTemplate>(x => x.MessageTypeId == messageType).FirstOrDefault();

                if (messageTemplate == null)
                {
                    logger.Error($"Mesage Template not found for message type {MessageTypeEnum.ApplicantShortListed.ToString()}");
                    return;
                }

                var template = await emailTemplateBuilder
                    .BuildFromRazorTemplate(messageType.ToString(), messageTemplate.Body, notification.EvaluationTemplate);

                await mediator.Publish(new SendEmailNotification
                {
                    Body = template,
                    EmailAddresses = new []{notification.EmailAddress},
                    Subject = messageTemplate.Subject
                });
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while sending applicant evaluated notifcation for applicant Id {notification.ApplicantId}");
            }
        }
    }
}
