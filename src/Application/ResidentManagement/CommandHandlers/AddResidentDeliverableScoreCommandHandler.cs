﻿using Application.ResidentManagement.Commands;
using Application.ResidentManagement.Notifications;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.Examination;
using ResidentManagement.Domain;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.CommandHandlers
{
    public class AddResidentDeliverableScoreCommandHandler : IRequestHandler<AddResidentDeliverableScoreCommand, InputResidentExaminationScoreResult>
    {
        IResidentManagementUnitOfWork residentUnitOfWork;
        IProgramManagementUnitOfWork programUnitOfWork;
        IMediator mediator;
        ILogger logger = Log.ForContext<AddResidentDeliverableScoreCommandHandler>();
        public AddResidentDeliverableScoreCommandHandler(IResidentManagementUnitOfWork _residentUnitOfWork,
            IProgramManagementUnitOfWork _programUnitOfWork, IMediator _mediator)
        {
            residentUnitOfWork = _residentUnitOfWork;
            programUnitOfWork = _programUnitOfWork;
            mediator = _mediator;
        }
        public Task<InputResidentExaminationScoreResult> Handle(AddResidentDeliverableScoreCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var submittedScores = request.ResidentDeliverables.Where(x => x.Score.HasValue);

                var courseIds = request.ResidentDeliverables.Select(x => x.CourseId).Distinct();

                var residentDeliverableDetails = submittedScores as ResidentDeliverableDetail[] ?? submittedScores.ToArray();
                var residentDeliverableIds = residentDeliverableDetails.Select(x => x.ResidentDeliverableId).ToList();

                var residentDeliverables = residentUnitOfWork.ResidentManagementRepository
                    .FindByWithInclude<ResidentDeliverable>(x => residentDeliverableIds.Contains(x.Id) && x.Score.HasValue == false,
                    x=>x.ResidentFieldPlacementActivity.ResidentUnit).ToList();

                foreach (var residentDeliverable in residentDeliverables)
                {
                    var score = residentDeliverableDetails.SingleOrDefault(x => x.ResidentDeliverableId == residentDeliverable.Id);
                    if (score != null)
                    {
                        residentDeliverable.InputScore(score.Score);
                        residentUnitOfWork.ResidentManagementRepository.Update(residentDeliverable);
                    }
                }

                residentUnitOfWork.SaveChanges();

                var residentUnits = residentDeliverables.
                    Select(x => x.ResidentFieldPlacementActivity.ResidentUnit)
                    .ToList();

                var examinations = programUnitOfWork.ProgramManagementRepository
                    .FindBy<Examination>(x => courseIds.Contains(x.CourseId.Value)).ToList();

                var examScores = new List<ExaminationScore>();
                foreach (var residentUnit in residentUnits)
                {
                    var residentActivities = residentUnitOfWork.ResidentManagementRepository.FindBy<ResidentFieldPlacementActivity>
                        (x => x.ResidentUnitId == residentUnit.Id).Select(x => x.Id).ToList();

                    var activityDeliverables = residentUnitOfWork.ResidentManagementRepository.FindBy<ResidentDeliverable>
                        (x => residentActivities.Contains(x.ResidentFieldPlacementActivityId)).ToList();

                    var anyDeliverableScorePending = activityDeliverables.Any(x => x.Score.HasValue == false);

                    if (!anyDeliverableScorePending)
                    {
                        var totalScore = activityDeliverables.Sum(x => x.Score.Value);

                        var averageScore = Math.Floor(totalScore / activityDeliverables.Count);
                        var examinationId = examinations.SingleOrDefault(x => x.UnitId == residentUnit.UnitId)?.Id;
                        if(!examinationId.HasValue)
                            continue;

                        var examScore = new ExaminationScore(examinationId.Value, null, request.CreatedBy,
                            averageScore, null, residentUnit.ResidentCourseWorkId,request.ExamType);
                        examScores.Add(examScore);

                    }
                }

                if (examScores.Any())
                {
                    programUnitOfWork.ProgramManagementRepository.AddRange(examScores);
                    programUnitOfWork.SaveChanges();

                    var examScoreNotification = new ResidentExaminationScoreEvaluatedNotification()
                    {
                        ResidentId = request.ResidentId,
                        ProgramEnrollmentId = request.ProgramEnrollmentId,
                        ExaminationScores = examScores.Select(x => new ExaminationScoreDetails
                        {
                            ExaminationId = x.ExaminationId,
                            ResidentCourseWorkId = x.ResidentCourseWorkId ?? 0,
                            Score = x.Score
                        }).ToList()
                    };

                    mediator.Publish(examScoreNotification);
                }

                return Task.FromResult(new InputResidentExaminationScoreResult
                {
                    IsSuccessful = true,
                    Message = "Resident deliverable scores updated successfully",
                    ResidentId = request.ResidentId
                });
            }
            catch (Exception ex)
            {

                logger.Error(ex, $"An error occurred while submitting resident deliverable scores for residentId {request.ResidentId} and EnrollmentId {request.ProgramEnrollmentId}");

                return Task.FromResult(new InputResidentExaminationScoreResult
                {
                    IsSuccessful = true,
                    Message = "An error occurred while submitting resident deliverable scores",
                    ResidentId = request.ResidentId
                });
            }
        }
    }
}
