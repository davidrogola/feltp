﻿using Application.ResidentManagement.Commands;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain.Ethics;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.CommandHandlers
{
    public class OverrideEthicsApprovalOutcomeCommandHandler : 
        IRequestHandler<OverrideEthicsApprovalOutcomeCommand, long>
    {
        IResidentManagementUnitOfWork unitOfWork;
        public OverrideEthicsApprovalOutcomeCommandHandler(IResidentManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<long> Handle(OverrideEthicsApprovalOutcomeCommand request, CancellationToken cancellationToken)
        {
            var ethicsApproval = unitOfWork.ResidentManagementRepository.Get<EthicsApproval>(request.EthicsApprovalId);
            if (ethicsApproval == null)
                return Task.FromResult(default(long));

            if (request.ApprovalStatus == ethicsApproval.ApprovalStatus)
                return Task.FromResult(default(long));

            ethicsApproval.OverideApplicationStatus(request.OverridenBy, ethicsApproval.ApprovalStatus, request.OverrideReason, request.ApprovalStatus);

            unitOfWork.ResidentManagementRepository.Update(ethicsApproval);
            unitOfWork.SaveChanges();

            return Task.FromResult(request.EthicsApprovalId);

        }
    }
}
