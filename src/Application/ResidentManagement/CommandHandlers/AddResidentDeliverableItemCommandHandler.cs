﻿using Application.Common.Services;
using Application.ResidentManagement.Commands;
using Application.ResidentManagement.NotificationHandlers;
using AutoMapper;
using MediatR;
using Microsoft.Extensions.Configuration;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain;
using Serilog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Notifications;
using Common.Domain.Messaging;
using FacultyManagement.Domain;
using Persistance.FacultyManagement.UnitOfWork;
using Persistance.LookUpRepo.UnitOfWork;

namespace Application.ResidentManagement.CommandHandlers
{
    public class AddResidentDeliverableItemCommandHandler : IRequestHandler<AddResidentDeliverableItemCommand, SubmitDeliverableResponse>
    {
        private IMapper mapper;
        private IResidentManagementUnitOfWork unitOfWork;
        private IFacultyManagementUnitOfWork facultyUnitOfWork;
        private IMediator mediator;
        private ILookUpUnitOfWork lookUpUnitOfWork;
        private ILogger logger = Log.ForContext<AddResidentDeliverableItemCommandHandler>();
        IEmailTemplateBuilder emailTemplateBuilder;

        public AddResidentDeliverableItemCommandHandler(
            IResidentManagementUnitOfWork unitOfWork, 
            ILookUpUnitOfWork lookUpUnitOfWork, 
            IFacultyManagementUnitOfWork facultyUnitOfWork ,
            IMapper mapper,
            IEmailTemplateBuilder templateBuilder,
             IMediator mediator)
        {
            this.unitOfWork = unitOfWork;
            this.facultyUnitOfWork = facultyUnitOfWork;
            this.lookUpUnitOfWork = lookUpUnitOfWork;
            this.mediator = mediator;
            this.mapper = mapper;
            emailTemplateBuilder = templateBuilder;
        }

        public async Task<SubmitDeliverableResponse> Handle(AddResidentDeliverableItemCommand request, CancellationToken cancellationToken)
        {

            try
            {
                request.DateSubmitted = request.StrDateSubmitted.ToLocalDate();

                var residentDeliverableItem = mapper.Map<ResidentDeliverableItem>(request);

                unitOfWork.ResidentManagementRepository.Add(residentDeliverableItem);

                var residentDeliverable = unitOfWork.ResidentManagementRepository
                    .FindByWithInclude<ResidentDeliverable>(x=>x.Id == request.ResidentDeliverableId, 
                        x=>x.Deliverable, x=>x.ResidentFieldPlacementActivity.FieldPlacementActivity).SingleOrDefault();
                
                if (residentDeliverable == null) // Log error message and return.
                    return new SubmitDeliverableResponse
                    {
                        IsSuccessful = false,
                        Message = "Resident deliverable info not found",
                        ResidentId = request.ResidentId
                    };

                if (request.SubmissionType == SubmissionType.Final)
                {
                    residentDeliverable.Evaluate(request.CreatedBy);
                    if(request.CoAuthors != null)
                    {
                        var coAuthors = request.CoAuthors.Where(x => x.Selected == true)
                        .Select(x => new ResidentDeliverableCoauthor
                        {
                            AuthorName = x.AuthorName,
                            CreatedBy = request.CreatedBy,
                            DateCreated = DateTime.Now,
                            EthicsApprovalCommitteeId = x.CommitteeId,
                            ResidentDeliverableId = request.ResidentDeliverableId,
                            IsActive = true

                        }).ToList();

                        unitOfWork.ResidentManagementRepository.AddRange(coAuthors);
                    }
                                    
                    if (request.HasThesisDefence)
                    {
                        residentDeliverable.SetThesisDefenceStatus(request.ThesisDefenceStatus);

                        if (request.ThesisDefenceStatus == ThesisDefenceStatus.SuccesfullyDefended)
                         await mediator.Publish(new ThesisDeliverableSubmittedNotification
                            {
                                ProgramEnrollemntId = request.ProgramEnrollmentId,
                                ResidentDeliverableId = request.ResidentDeliverableId,
                                ResidentId = request.ResidentId,
                                ThesisDefenceStatus = request.ThesisDefenceStatus
                            },cancellationToken);
                    }

                }
                else
                {
                    residentDeliverable.SetProgressStatus(DeliverableStatus.Submitted);
                }                
                
                unitOfWork.ResidentManagementRepository.Update(residentDeliverable);
                unitOfWork.SaveChanges();

                var resident = unitOfWork.ResidentManagementRepository
                    .FindByWithInclude<Resident>(x => x.Id == request.ResidentId,
                        x => x.Applicant.Person).SingleOrDefault();

                await NotifyFacultyOfSubmittedDeliverable(new SubmitDeliverableTemplate
                {
                    DeliverableName = residentDeliverable.Deliverable.Name,
                    ResidentName = resident != null ? resident.Applicant.Person.FirstName + " " + resident.Applicant.Person.LastName : null,
                    DateSubmitted = request.DateSubmitted.ToShortDateString(),
                    FieldActivity = residentDeliverable.ResidentFieldPlacementActivity.FieldPlacementActivity.Name,
                    RegistrationNumber = resident.RegistrationNumber,
                    SubmissionType = request.SubmissionType.ToString()

                });

                return new SubmitDeliverableResponse
                {
                    IsSuccessful = true,
                    Message = "Resident deliverable successfully submitted",
                    ResidentId = request.ResidentId,
                    DeliverableItemId = residentDeliverableItem.Id
                };
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while submitting resident deliverable for residentId {request.ResidentId }" +
                    $" and Resident DeliverableId {request.ResidentDeliverableId}");

                return new SubmitDeliverableResponse
                {
                    IsSuccessful = false,
                    Message = "An error occured while submitting resident deliverable",
                    ResidentId = request.ResidentId
                };

            }
        }
        
        private async Task NotifyFacultyOfSubmittedDeliverable(SubmitDeliverableTemplate template)
        {

            var msgTemplate = lookUpUnitOfWork.LookUpRepository
                .FindBy<MessageTemplate>(m => m.MessageTypeId == MessageTypeEnum.SubmittedDeliverable)
                .SingleOrDefault();
            if (msgTemplate == null) // Log and then return
               return;
            var body = await emailTemplateBuilder.BuildFromRazorTemplate( MessageTypeEnum.SubmittedDeliverable.ToString(),
                msgTemplate.Body,template);
            if(string.IsNullOrEmpty(body)) // Log error msg and return
                return;
            
            var facultyEmailAddresses = facultyUnitOfWork.FacultyManagementRepository.FindByWithInclude<Faculty>(null,
                x => x.Person,
                x => x.Person.ContactInformation).Select(x => x.Person.ContactInformation.EmailAddress).Distinct().ToArray();

           await mediator.Publish(new SendEmailNotification
            {
                Body = body,
                EmailAddresses = facultyEmailAddresses,
                Subject = msgTemplate.Subject
            });
           
           
        }

    }
    
  
    public class AddDeliverableItemFileRequestHandler : IRequestHandler<AddDeliverableItemFileRequest, DeliverableFileResponse>
    {
        IResidentManagementUnitOfWork unitOfWork;
        ILogger logger = Log.ForContext<AddDeliverableItemFileRequestHandler>();

        public AddDeliverableItemFileRequestHandler(IResidentManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<DeliverableFileResponse> Handle(AddDeliverableItemFileRequest request, CancellationToken cancellationToken)
        {
            try
            {
                var residentDeliverableItem = unitOfWork.ResidentManagementRepository.Get<ResidentDeliverableItem>(request.DeliverableItemId);
                if (residentDeliverableItem == null)
                    return Task.FromResult(new DeliverableFileResponse
                    {
                        DeliverableItemId = request.DeliverableItemId,
                        Message = "Resident deliverable item not found"
                    });

                using (var memoryStream = new MemoryStream())
                {
                    request.DeliverableFile.CopyTo(memoryStream);
                    residentDeliverableItem.AddDeliverableFile(memoryStream.ToArray(), request.DeliverableFile.FileName, request.DeliverableFile.ContentType);

                    unitOfWork.ResidentManagementRepository.Update(residentDeliverableItem);
                    unitOfWork.SaveChanges();

                    return Task.FromResult(new DeliverableFileResponse
                    {
                        DeliverableItemId = request.DeliverableItemId,
                        IsSuccesful = true,
                        Message = "Deliverable file added succesfully"
                    });
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while adding file for resident deliverable item {request.DeliverableItemId}");
                return Task.FromResult(new DeliverableFileResponse
                {
                    DeliverableItemId = request.DeliverableItemId,
                    Message = "An error occured while adding file for deliverable item"
                });

            }

        }
    }
}
