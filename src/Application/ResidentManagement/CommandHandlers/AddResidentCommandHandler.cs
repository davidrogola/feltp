﻿using Application.Common;
using Application.ResidentManagement.Commands;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.CommandHandlers
{
    public class AddResidentCommandHandler : IRequestHandler<AddResidentCommand, long>
    {
        IResidentManagementUnitOfWork unitOfWork;
        ICodeGenerator codeGenerator;
       
        public AddResidentCommandHandler(IResidentManagementUnitOfWork _unitOfWork, ICodeGenerator _codeGenerator)
        {
            unitOfWork = _unitOfWork;
            codeGenerator = _codeGenerator;
        }
        public Task<long> Handle(AddResidentCommand request, CancellationToken cancellationToken)
        {
            var registrationNumber = codeGenerator.Generate("residentcode");

            var resident = new Resident(request.ApplicantId, registrationNumber, request.CreatedBy);

            unitOfWork.ResidentManagementRepository.Add(resident);
            unitOfWork.SaveChanges();
            return Task.FromResult(resident.Id);
        }
    }

}
