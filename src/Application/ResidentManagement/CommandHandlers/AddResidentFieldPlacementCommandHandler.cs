﻿using Application.ResidentManagement.Commands;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.FieldPlacement;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Globalization;
using Application.Common.Services;
using Serilog;

namespace Application.ResidentManagement.CommandHandlers
{
    public class AddResidentFieldPlacementCommandHandler : IRequestHandler<AddResidentFieldPlacementCommand, AddFieldPlacementResponse>
    {
        IMapper mapper;
        IResidentManagementUnitOfWork unitOfWork;
        IProgramManagementUnitOfWork programUnitOfWork;
        ILogger logger = Log.ForContext<AddResidentFieldPlacementCommandHandler>();
        public AddResidentFieldPlacementCommandHandler(IResidentManagementUnitOfWork _unitOfWork, IProgramManagementUnitOfWork _programUnitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            programUnitOfWork = _programUnitOfWork;
            mapper = _mapper;

        }
        public Task<AddFieldPlacementResponse> Handle(AddResidentFieldPlacementCommand request, CancellationToken cancellationToken)
        {
            try
            {
                request.DateCreated = DateTime.Now;
                request.StartDate = request.StrStartDate.ToLocalDate();
                request.EndDate = request.StrEndDate.ToLocalDate();

                var fieldSite = programUnitOfWork.ProgramManagementRepository.FindBy<FieldPlacementSite>
                    (x => x.Name == request.SiteName).SingleOrDefault();

                var fieldPlacement = mapper.Map<FieldPlacement>(request);
                fieldPlacement.SetProgressStatus(PlacementActivityStatus.NotStarted);

                if (request.StartDate.Date <= DateTime.Now.Date)
                    fieldPlacement.SetProgressStatus(PlacementActivityStatus.Ongoing);

                if (request.EndDate.Date <= DateTime.Now.Date)
                {
                    var deliverablePendingSubmission = unitOfWork.ResidentManagementRepository.FindBy<ResidentDeliverable>(x => x.ResidentFieldPlacementActivityId == request.ResidentFieldPlacementActivityId
                    && x.ProgressStatus == DeliverableStatus.Pending).ToList();

                    var completionStatus = deliverablePendingSubmission.Any() == true ? PlacementActivityStatus.DeliverablePendingSubmission : PlacementActivityStatus.Completed;

                    fieldPlacement.SetProgressStatus(completionStatus);

                    if(completionStatus == PlacementActivityStatus.DeliverablePendingSubmission)
                    {
                        foreach (var deliverable in deliverablePendingSubmission)
                        {
                            deliverable.SetProgressStatus(DeliverableStatus.Overdue);
                            unitOfWork.ResidentManagementRepository.Update(deliverable);
                        }
                    }
                }
                   
                if (fieldSite != null)
                    fieldPlacement.SetSite(fieldSite.Id);

                var residentFieldPlacementActivity = unitOfWork.ResidentManagementRepository.Get<ResidentFieldPlacementActivity>(request.ResidentFieldPlacementActivityId);

                if (residentFieldPlacementActivity != null)
                {
                    residentFieldPlacementActivity.SetProgressStatus(fieldPlacement.ProgressStatus);
                    unitOfWork.ResidentManagementRepository.Update(residentFieldPlacementActivity);
                }

                unitOfWork.ResidentManagementRepository.Add(fieldPlacement);
                unitOfWork.SaveChanges();

                return Task.FromResult(new AddFieldPlacementResponse
                {
                    IsSuccessful = true,
                    ResidentId = request.ResidentId,
                    Message = "Resident field placement successfully initiated"
                });
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while initiating resident field placement for residentId { request.ResidentId } and field placementId {request.ResidentFieldPlacementActivityId}");

                return Task.FromResult(new AddFieldPlacementResponse
                {
                    IsSuccessful = false,
                    ResidentId = request.ResidentId,
                    Message = "An error occured while initiating resident field placement activity"
                });

            }
        }
    }
}
