﻿using Application.ResidentManagement.Commands;
using AutoMapper;
using Common.Domain.Person;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.CommandHandlers
{
   public class AddCadreCommandHandler : IRequestHandler<AddCadreCommand, int>
    {
        IResidentManagementUnitOfWork residentManagementUnitOfWork;
        public AddCadreCommandHandler(IResidentManagementUnitOfWork _residentManagementUnitOfWork)
        {
            residentManagementUnitOfWork = _residentManagementUnitOfWork;
        }
        public Task<int> Handle(AddCadreCommand request, CancellationToken cancellationToken)
        {
            request.DateCreated = DateTime.Now;
            request.CreatedBy = "System";
            var cadre = Mapper.Map<Cadre>(request);

            residentManagementUnitOfWork.ResidentManagementRepository.Add(cadre);
            residentManagementUnitOfWork.SaveChanges();
            return Task.FromResult(cadre.Id);
        }
    }

    public class UpdateCadreCommandHandler : IRequestHandler<UpdateCadreCommand, int>
    {
        IResidentManagementUnitOfWork residentManagementUnitOfWork;
        IMapper mapper;
        public UpdateCadreCommandHandler(IResidentManagementUnitOfWork _residentManagementUnitOfWork, IMapper _mapper)
        {
            residentManagementUnitOfWork = _residentManagementUnitOfWork;
            mapper = _mapper;
        }
        public Task<int> Handle(UpdateCadreCommand request, CancellationToken cancellationToken)
        {
            Cadre cadre = mapper.Map<Cadre>(request);
            residentManagementUnitOfWork.ResidentManagementRepository.Update(cadre);
            residentManagementUnitOfWork.SaveChanges();

            return Task.FromResult(cadre.Id);
        }
    }
}
