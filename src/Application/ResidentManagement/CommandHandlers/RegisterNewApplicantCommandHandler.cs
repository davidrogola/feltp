﻿using System.Data;
using System.Threading;
using System.Threading.Tasks;

using MediatR;
using AutoMapper;

using ResidentManagement.Domain;
using Common.Domain.Address;
using Common.Domain.Person;
using Persistance.ResidentManagement.UnitOfWork;
using Application.ResidentManagement.Commands;
using System;
using ProgramManagement.Domain.Program;
using Persistance.ProgramManagement.UnitOfWork;
using Application.Common.Interface;
using Serilog;
using System.Linq;

namespace Application.ResidentManagement.CommandHandlers
{
    public class RegisterNewApplicantCommandHandler : IRequestHandler<RegisterNewApplicantCommand, long>
    {
        IResidentManagementUnitOfWork unitOfWork;
        IProgramManagementUnitOfWork programUnitOfWork;
        IPersonService personService;
        ILogger logger = Log.ForContext<RegisterNewApplicantCommandHandler>();

        public RegisterNewApplicantCommandHandler(IResidentManagementUnitOfWork _unitOfWork,
            IProgramManagementUnitOfWork _programUnitOfWork, IPersonService _personService)
        {
            unitOfWork = _unitOfWork;
            personService = _personService;
            programUnitOfWork = _programUnitOfWork;

        }
        public Task<long> Handle(RegisterNewApplicantCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var personId = personService.CreatePerson(request.Person);

                if (personId == default(long))
                {
                    logger.Information($"Applicant registration failed for person with IdNumber: {request.Person.BioDataInfo.IdentificationNumber}");

                    return Task.FromResult(default(long));
                }

                var applicant = new Applicant(personId, request.CreatedBy);
                unitOfWork.ResidentManagementRepository.Add(applicant);
                unitOfWork.SaveChanges();

                var offerApplication = new ProgramOfferApplication(request.ProgramOfferId, applicant.Id, ProgramQualificationStatus.Pending_Evaluation, DateTime.Now, request.CreatedBy, request.ApprovedByCounty, request.PersonalStatementSubmitted, ApplicationStatus.Pending);

                programUnitOfWork.ProgramManagementRepository.Add(offerApplication);
                programUnitOfWork.SaveChanges();

                if (request.PersonalStatementSubmitted)
                {
                    var personalStatementFile = new PersonalStatementFile(offerApplication.Id, request.File.File, request.File.FileName, request.File.ContentType);

                    unitOfWork.ResidentManagementRepository.Add(personalStatementFile);

                }

                unitOfWork.SaveChanges();

                return Task.FromResult(applicant.Id);
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"Applicant registration failed for person with IdNumber: {request.Person.BioDataInfo.IdentificationNumber}");
                throw;
            }
        }

        public class UpdateApplicantBioDataCommandHandler : IRequestHandler<UpdateApplicantBioDataCommand, long>
        {
            IResidentManagementUnitOfWork unitOfWork;
            IMediator mediator;
            ILogger logger = Log.ForContext<RegisterNewApplicantCommandHandler>();

            public UpdateApplicantBioDataCommandHandler(IResidentManagementUnitOfWork _unitOfWork,
             IPersonService _personService, IMediator _mediator)

            {
                unitOfWork = _unitOfWork;
                mediator = _mediator;

            }
            public async Task<long> Handle(UpdateApplicantBioDataCommand request, CancellationToken cancellationToken)
            {
                using (var transaction = unitOfWork.BeginTransaction(IsolationLevel.ReadCommitted))
                {
                    try
                    {
                        var personId = await mediator.Send(request.UpdatePersonBioData);

                        var applicant = unitOfWork.ResidentManagementRepository
                            .FindBy<Applicant>(x => x.Id == request.ApplicantId).SingleOrDefault();

                        unitOfWork.ResidentManagementRepository.Update(applicant);

                        unitOfWork.SaveChanges();

                        return applicant.Id;
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex, $"An error occurred while updating applicant with Id: {request.ApplicantId }");
                        throw;
                    }
                }


            }

        }


    }
}
