﻿using Application.ResidentManagement.Commands;
using Application.ResidentManagement.Notifications;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Examination;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Application.ResidentManagement.Services;
using Newtonsoft.Json.Serialization;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain;

namespace Application.ResidentManagement.CommandHandlers
{
    public class InputResidentExaminationScoreCommandHandler : IRequestHandler<InputResidentExaminationScoreCommand, InputResidentExaminationScoreResult>
    {
        IProgramManagementUnitOfWork _unitOfWork;
        private readonly IResidentManagementUnitOfWork _residentUnitOfWork;
        IMediator _mediator;
        ILogger _logger = Log.ForContext<InputResidentExaminationScoreCommandHandler>();
        private readonly ExamScoreConsolidationService _examScoreConsolidationService;

        public InputResidentExaminationScoreCommandHandler(IProgramManagementUnitOfWork unitOfWork,
            IResidentManagementUnitOfWork residentUnitOfWork,
            ExamScoreConsolidationService examScoreConsolidationService,
            IMediator mediator)
        {
            _unitOfWork = unitOfWork;
            _residentUnitOfWork = residentUnitOfWork;
            _mediator = mediator;
            _examScoreConsolidationService = examScoreConsolidationService;
        }
        public Task<InputResidentExaminationScoreResult> Handle(InputResidentExaminationScoreCommand request, CancellationToken cancellationToken)
        {
            using (var transaction = _unitOfWork.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    var examParticipations = new List<ExaminationParticipation>();
                    var examTypes = request.IsCat
                        ? new[] {ExamType.CatOne, ExamType.CatTwo, ExamType.CatThree}
                        : new[] {ExamType.Final};

                    var existingDuplicateResidentIds = _unitOfWork.ProgramManagementRepository
                        .FindBy<ExaminationScore>(x => request.ExamDetailResponse.ResidentExams
                    .Select(r => r.ResidentCourseWorkId).Contains(x.ResidentCourseWorkId ?? 0) && examTypes.Contains(x.ExamType)).Select(x => x.ResidentCourseWorkId);

                    var submittedExams = request.ExamDetailResponse.ResidentExams.Where(x => (x.Submitted || x.Score.HasValue)
                    && !existingDuplicateResidentIds.Contains(x.ResidentCourseWorkId));

                    foreach (var exam in submittedExams)
                    {
                        var examParticipation = new ExaminationParticipation(exam.TimetableId, request.EnrollmentId,
                            request.CreatedBy);
                        examParticipations.Add(examParticipation);
                    }

                    if (examParticipations.Any())
                    {
                        _unitOfWork.ProgramManagementRepository.AddRange(examParticipations);
                        _unitOfWork.SaveChanges();
                    }

                    var examScores = new List<ExaminationScore>();

                    foreach (var item in examParticipations)
                    {
                        var inputtedScore = request.ExamDetailResponse.ResidentExams.SingleOrDefault(x => x.TimetableId == item.TimetableId);
                        if (inputtedScore?.Score == null) continue;
                        var consolidatedExamScore = request.IsCat ? inputtedScore.Score.Value:
                           _examScoreConsolidationService.ConsolidateScores(inputtedScore.ResidentCourseWorkId,
                                inputtedScore.Score.Value);
                        var examScore = new ExaminationScore(inputtedScore.ExaminationId, item.Id, request.CreatedBy, Math.Round(consolidatedExamScore), 
                            null,inputtedScore.ResidentCourseWorkId,inputtedScore.ExamType);
                        examScores.Add(examScore);
                    }

                    if (examScores.Any())
                    {
                        _unitOfWork.ProgramManagementRepository.AddRange(examScores);
                        _unitOfWork.SaveChanges();
                        if (!request.IsCat)
                        {
                            var examScoreNotification = new ResidentExaminationScoreEvaluatedNotification()
                            {
                                ResidentId = request.ResidentId,
                                ProgramEnrollmentId = request.EnrollmentId,
                                ExaminationScores = examScores.Select(x => new ExaminationScoreDetails
                                {
                                    ExaminationId = x.ExaminationId,
                                    Score = x.Score,
                                    ResidentCourseWorkId =  x.ResidentCourseWorkId ?? 0
                                }).ToList()
                            };

                            _mediator.Publish(examScoreNotification, cancellationToken);
                        }
                    }
                    _residentUnitOfWork.SaveChanges();

                    transaction.Commit();
                    return Task.FromResult(new InputResidentExaminationScoreResult
                    {
                        IsSuccessful = true,
                        Message = "Resident examination score updated successfully",
                        ResidentId = request.ResidentId
                    });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, $"An error occured while inputting examination scores for residentId {request.ResidentId} and EnrollmentId {request.EnrollmentId}");

                    return Task.FromResult(new InputResidentExaminationScoreResult
                    {
                        IsSuccessful = false,
                        Message = "An error occured while inputting resident examination scores",
                        ResidentId = request.ResidentId
                    });
                }
            }

        }
    }
}
