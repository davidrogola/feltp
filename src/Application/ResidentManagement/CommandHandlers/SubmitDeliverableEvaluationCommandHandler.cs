using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Notifications;
using Application.Common.Services;
using Application.ResidentManagement.Commands;
using Common.Domain.Messaging;
using FacultyManagement.Domain;
using MediatR;
using Persistance.LookUpRepo.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain;

namespace Application.ResidentManagement.CommandHandlers
{
    public class SubmitDeliverableEvaluationCommandHandler : IRequestHandler<SubmitDeliverableEvaluationCommand, string>
    {
        private readonly IResidentManagementUnitOfWork _residentManagementUnitOfWork;
        private readonly IMediator _mediator;
        private readonly ILookUpUnitOfWork _lookUpUnitOfWork;
        private readonly IEmailTemplateBuilder _emailTemplateBuilder;

        public SubmitDeliverableEvaluationCommandHandler(IResidentManagementUnitOfWork residentManagementUnitOfWork,
            ILookUpUnitOfWork lookUpUnitOfWork, IEmailTemplateBuilder emailTemplateBuilder, IMediator mediator)
        {
            _mediator = mediator;
            _lookUpUnitOfWork = lookUpUnitOfWork;
            _residentManagementUnitOfWork = residentManagementUnitOfWork;
            _emailTemplateBuilder = emailTemplateBuilder;
        }

        public async Task<string> Handle(SubmitDeliverableEvaluationCommand request, CancellationToken cancellationToken)
        {
            var residentDeliverable = _residentManagementUnitOfWork.ResidentManagementRepository
                .FindByWithInclude<ResidentDeliverable>(x => x.Id == request.Id,
                    x => x.ResidentFieldPlacementActivity.FieldPlacementActivity, x=>x.Deliverable).SingleOrDefault();
            if (residentDeliverable == null)
                return "Resident deliverable info not found";
            var residentInfo = _residentManagementUnitOfWork.ResidentManagementRepository
                .FindByWithInclude<Resident>(x => x.Id == request.ResidentId,
                    x => x.Applicant.Person.ContactInformation).SingleOrDefault();
            if (residentInfo == null)
                return "Resident info not found";

            var deliverableEvaluationInfo = new DeliverableEvaluationTemplate
            {
                ResidentName = residentInfo.Applicant.Person.FirstName + " " + residentInfo.Applicant.Person.LastName,
                Email = residentInfo.Applicant.Person.ContactInformation.EmailAddress,
                Message = request.Message,
                Deliverable = residentDeliverable.Deliverable.Name,
                Faculty = request.Faculty,
                DateEvaluated = DateTime.Now.ToShortDateString(),
                FieldActivity = residentDeliverable.ResidentFieldPlacementActivity.FieldPlacementActivity.Name
            };
            await NotifyResidentOfDeliverableEvaluation(deliverableEvaluationInfo);
            return "Resident evaluation information sent successfully";
        }

        private async Task NotifyResidentOfDeliverableEvaluation(DeliverableEvaluationTemplate template)
        {
            var msgTemplate = _lookUpUnitOfWork.LookUpRepository
                .FindBy<MessageTemplate>(m => m.MessageTypeId == MessageTypeEnum.DeliverableEvaluated)
                .SingleOrDefault();
            if (msgTemplate == null) // Log and then return
                return;
            var body = await _emailTemplateBuilder.BuildFromRazorTemplate(
                MessageTypeEnum.DeliverableEvaluated.ToString(),
                msgTemplate.Body, template);
            if (string.IsNullOrEmpty(body)) // Log error msg and return
                return;

            await _mediator.Publish(new SendEmailNotification
            {
                Body = body,
                EmailAddresses = new[] {template.Email},
                Subject = msgTemplate.Subject
            });
        }
    }
}

public class DeliverableEvaluationTemplate
{
    public string ResidentName { get; set; }
    public string Email { get; set; }
    public string Faculty { get; set; }
    public string Deliverable { get; set; }
    public string Message { get; set; }
    
    public string FieldActivity { get; set; }
    public string DateEvaluated { get; set; }
}

