﻿using Application.ResidentManagement.Commands;
using MediatR;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Persistance.Generic;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using Microsoft.EntityFrameworkCore;
using Serilog;
using Newtonsoft.Json;
using Persistance.LookUpRepo.UnitOfWork;
using Common.Domain.Messaging;
using System.Linq;
using Application.Common.Services;
using Application.Common.Notifications;

namespace Application.ResidentManagement.CommandHandlers
{
    public class ShortListApplicantCommandHandler : IRequestHandler<ShortListApplicantCommand, ShortListApplicantResult>
    {
        IProgramManagementUnitOfWork unitOfWork;
        ILogger logger = Log.ForContext<ShortListApplicantCommandHandler>();
        IMediator mediator;


        public ShortListApplicantCommandHandler(IProgramManagementUnitOfWork unitOfWork, IMediator _mediator)
        {
            this.unitOfWork = unitOfWork;
            mediator = _mediator;

        }
        public Task<ShortListApplicantResult> Handle(ShortListApplicantCommand request, CancellationToken cancellationToken)
        {

            try
            {
                //used this because in mysql there is no generic way to pass a datatable as a parameter in a sproc
                //and did not want to loop through the collection of applicants 

                var offerApplicationIds = String.Join(",", request.ProgramOfferApplicationIds.ToArray());

                string query = $"UPDATE programofferapplication set DateShortListed = NOW(),ShortListedBy ='{request.ShortListedBy}',ApplicationStatus ={1} WHERE Id in ({offerApplicationIds})";

                var result = unitOfWork.ProgramManagementRepository.ExecuteSql<ProgramOfferApplication>(query);

                mediator.Publish(new ApplicantsShortListedNotification
                {
                    DateShortListed = DateTime.Now.ToShortDateString(),
                    ProgramName = request.ProgramName,
                    ProgramOffer = request.ProgramOffer,
                    ProgramOfferApplicationIds = request.ProgramOfferApplicationIds
                });

                return Task.FromResult(new ShortListApplicantResult
                {
                    CompletedSuccessfully = true,
                    Message = "Applicants shortlisted successfully"
                });
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"An error occured while shortlisting applicants with ProgramOfferApplicationIds {JsonConvert.SerializeObject(request.ProgramOfferApplicationIds)}");

                return Task.FromResult(new ShortListApplicantResult
                {
                    CompletedSuccessfully = false,
                    Message = "An error occured while shortlisting applicants"
                });
            }
        }
    }

    public class ApplicantsShortListedNoticationHandler : INotificationHandler<ApplicantsShortListedNotification>
    {
        ILookUpUnitOfWork unitOfWork;
        IProgramManagementUnitOfWork programUnitOfWork;
        IEmailTemplateBuilder emailTemplateBuilder;
        IMediator mediator;
        ILogger logger = Log.ForContext<ApplicantsShortListedNoticationHandler>();

        public ApplicantsShortListedNoticationHandler(ILookUpUnitOfWork _unitofWork,
            IProgramManagementUnitOfWork _programUnitOfWork, IEmailTemplateBuilder _emailTemplateBuilder, IMediator _mediator)
        {
            unitOfWork = _unitofWork;
            programUnitOfWork = _programUnitOfWork;
            emailTemplateBuilder = _emailTemplateBuilder;
            mediator = _mediator;
        }
        public async Task Handle(ApplicantsShortListedNotification notification, CancellationToken cancellationToken)
        {
            try
            {
                var messageTemplate = unitOfWork.LookUpRepository.FindBy<MessageTemplate>(x => x.MessageTypeId == MessageTypeEnum.ApplicantShortListed).FirstOrDefault();
                if (messageTemplate == null)
                {
                    logger.Error($"Mesage Template not found for message type {MessageTypeEnum.ApplicantShortListed.ToString()}");
                    return;
                }

                var shortListedApplicants = programUnitOfWork.ProgramManagementRepository.FindBy<ProgramOfferApplicants>(x => notification.ProgramOfferApplicationIds.Contains(x.Id)).Select(x => new { Name = $"{x.FirstName} {x.LastName}", x.EmailAddress, x.ApplicantId });

                foreach (var applicant in shortListedApplicants)
                {
                    var template = await emailTemplateBuilder.BuildFromRazorTemplate
                        (MessageTypeEnum.ApplicantShortListed.ToString(), messageTemplate.Body, new ShortListApplicantTemplate { DateShortListed = notification.DateShortListed, Name = applicant.Name, Program = notification.ProgramName, ProgramOffer = notification.ProgramOffer });

                    await mediator.Publish(new SendEmailNotification
                    {
                        Body = template,
                        EmailAddresses = new[] {applicant.EmailAddress},
                        Subject = messageTemplate.Subject
                    },cancellationToken);
                }
                
            }
            catch (Exception ex)
            {

                logger.Error(ex, $"An error occured while processing shortlist applicant notication for Ids{JsonConvert.SerializeObject(notification.ProgramOfferApplicationIds)}");
            }
        }
    }
}
