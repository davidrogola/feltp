﻿using Application.ResidentManagement.Commands;
using MediatR;
using Microsoft.Extensions.Configuration;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain.Ethics;
using Serilog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.CommandHandlers
{
    public class AddEthicsApprovalCommandHandler : IRequestHandler<AddEthicsApprovalCommand, long>
    {
        IResidentManagementUnitOfWork unitOfWork;
        IProgramManagementUnitOfWork programUnitOfWork;
        IConfiguration configuration;
        ILogger logger = Log.ForContext<AddEthicsApprovalCommandHandler>();
        public AddEthicsApprovalCommandHandler(IResidentManagementUnitOfWork _unitOfWork, IProgramManagementUnitOfWork _programUnitOfWork, IConfiguration _configuration)
        {
            unitOfWork = _unitOfWork;
            programUnitOfWork = _programUnitOfWork;
            configuration = _configuration;
        }
        public Task<long> Handle(AddEthicsApprovalCommand request, CancellationToken cancellationToken)
        {
            using (var transaction = unitOfWork.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
            {
                try
                {
                    request.SubmissionExpiryDate = Convert.ToDateTime(request.StrSubmissionExpiryDate);

                    var enrollment = programUnitOfWork.ProgramManagementRepository.Get<ProgramEnrollment>(request.ProgramEnrollmentId);
                    if (enrollment == null)
                    {
                        logger.Information($"Program enrollment with Id {request.ProgramEnrollmentId } not found during creating of an ethics approval form");
                        return Task.FromResult(default(long));
                    }
                    EthicsApprovalStatus overAllApprovalStatus = EthicsApprovalStatus.Approved;

                    if (request.EthicsApprovalItems.Any(x => x.ApprovalStatus != EthicsApprovalStatus.Approved))
                        overAllApprovalStatus = EthicsApprovalStatus.Under_Review;

                    var ethicsApproval = new EthicsApproval(request.ResidentDeliverableId, enrollment.ResidentId, enrollment.Id, request.ProgramEventId, request.PaperCategory, request.SubmissionExpiryDate, request.CreatedBy, request.EmailNotification, request.SmsNotification, overAllApprovalStatus);

                    unitOfWork.ResidentManagementRepository.Add(ethicsApproval);
                    unitOfWork.SaveChanges();

                    EthicsApprovalDocument approvalDocument = null;
                    using (var memoryStream = new MemoryStream())
                    {
                        request.FormFile.CopyTo(memoryStream);

                        approvalDocument = new EthicsApprovalDocument(memoryStream.ToArray(), request.FormFile.FileName, request.FormFile.ContentType, request.CreatedBy, request.DocumentVersion, DateTime.Now);

                        unitOfWork.ResidentManagementRepository.Add(approvalDocument);
                        unitOfWork.SaveChanges();
                    }

                    var dateFormat = configuration["DateConversionFormat"];

                    var ethicsApprovalItems = request.EthicsApprovalItems.Where(x => x.Selected == true).Select(x => new EthicsApprovalItem
                    {
                        EthicsApprovalCommitteeId = x.CommitteeId,
                        EthicsApprovalDocumentId = approvalDocument.Id,
                        EthicsApprovalId = ethicsApproval.Id,
                        SubmissionDate = DateTime.ParseExact(x.StrDateSubmitted, dateFormat, CultureInfo.InvariantCulture),
                        SubmittedBy = request.CreatedBy,
                        DateCreated = DateTime.Now,
                        ApprovalStatus = x.ApprovalStatus,
                        Comment = x.Comment,
                    }).ToList();

                    unitOfWork.ResidentManagementRepository.AddRange(ethicsApprovalItems);

                    unitOfWork.SaveChanges();

                    transaction.Commit();
                    return Task.FromResult(ethicsApproval.Id);
                }
                catch (Exception ex)
                {
                    logger.Error(ex,$"An error occured during Ethics Approval form creation for EnrollemntId {request.ProgramEnrollmentId} and ResidentDeliverableId {request.ResidentDeliverableId}");

                    throw;
                }
            }
        }
    }
}
