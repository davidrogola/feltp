﻿using Application.ResidentManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain.Ethics;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetEthicsApprovalItemsQueryHandler : IRequestHandler<GetEthicsApprovalItems, List<EthicsApprovalItemViewModel>>
    {
        IResidentManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetEthicsApprovalItemsQueryHandler(IResidentManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;

        }
        public Task<List<EthicsApprovalItemViewModel>> Handle(GetEthicsApprovalItems request, CancellationToken cancellationToken)
        {
            IEnumerable<EthicsApprovalItem> ethicsApprovalItems = null;

            if (request.EthicsApprovalId.HasValue)
            {
                ethicsApprovalItems = unitOfWork.ResidentManagementRepository
               .FindByWithInclude<EthicsApprovalItem>(x => x.EthicsApprovalId == request.EthicsApprovalId.Value,
               x => x.EthicsApprovalCommittee, x => x.EthicsApprovalDocument);
            }
            else
            {
                ethicsApprovalItems = unitOfWork.ResidentManagementRepository
              .FindByWithInclude<EthicsApprovalItem>(x => x.Id == request.EthicsApprovalItemId.Value,
              x => x.EthicsApprovalCommittee, x => x.EthicsApprovalDocument);
            }


            var ethicsApprovalModel = mapper.Map<List<EthicsApprovalItemViewModel>>(ethicsApprovalItems);

            return Task.FromResult(ethicsApprovalModel);
        }
    }
}
