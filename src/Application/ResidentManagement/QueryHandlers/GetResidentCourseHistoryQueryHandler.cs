﻿using Application.ResidentManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetResidentCourseHistoryQueryHandler : IRequestHandler<GetResidentCourseHistory, List<CourseHistoryViewModel>>
    {
        IResidentManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetResidentCourseHistoryQueryHandler(IResidentManagementUnitOfWork _unitOfWork,IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<CourseHistoryViewModel>> Handle(GetResidentCourseHistory request, CancellationToken cancellationToken)
        {
            var courseHistory = unitOfWork.ResidentManagementRepository.FindByWithInclude<Graduate>(x => x.ResidentId == request.ResidentId, x => x.ProgramEnrollment.ProgramOffer.Program.ProgramTier);

            var courseHistoryViewModel = mapper.Map<List<CourseHistoryViewModel>>(courseHistory);

            return Task.FromResult(courseHistoryViewModel);
        }
    }
}
