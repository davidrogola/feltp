﻿using Application.ResidentManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain.Ethics;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetEthicsApprovalQueryHandler : IRequestHandler<GetEthicsApprovalQuery, List<EthicsApprovalViewModel>>
    {
        IResidentManagementUnitOfWork unitOfWork;
        IMapper mapper;
        ILogger logger = Log.ForContext<GetEthicsApprovalQueryHandler>();
        public GetEthicsApprovalQueryHandler(IResidentManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<EthicsApprovalViewModel>> Handle(GetEthicsApprovalQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var ethicsApprovals = request.Id.HasValue ? unitOfWork.ResidentManagementRepository
                        .FindBy<EthicsApprovalView>(x => x.Id == request.Id.Value) :
                        unitOfWork.ResidentManagementRepository.GetAll<EthicsApprovalView>();

                var ethicsModel = mapper.Map<List<EthicsApprovalViewModel>>(ethicsApprovals);
                return Task.FromResult(ethicsModel);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "An error occurred while fetching ethics approval list");
                throw;
            }
        }
    }
}
