﻿using Application.ResidentManagement.Commands;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.Examination;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Services;
using Application.ProgramManagement.QueryHandlers;
using IdentityModel;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetResidentExamDetailQueryHandler : IRequestHandler<GetResidentExamDetail, ResidentExamDetailResponse>
    {
        private readonly IResidentManagementUnitOfWork residentUnitOfWork;
        private readonly IProgramManagementUnitOfWork programManagementUnitOfWork;

        public GetResidentExamDetailQueryHandler(IResidentManagementUnitOfWork _residentUnitOfWork,
            IProgramManagementUnitOfWork _programManagementUnitOfWork, IMapper _mapper)
        {
            residentUnitOfWork = _residentUnitOfWork;
            programManagementUnitOfWork = _programManagementUnitOfWork;
        }

        public Task<ResidentExamDetailResponse> Handle(GetResidentExamDetail request,
            CancellationToken cancellationToken)
        {
            var semesterSchedule = programManagementUnitOfWork.ProgramManagementRepository
                .FindByWithInclude<SemesterSchedule>(x => x.SemesterId == request.SemesterId &&
                                                          x.ProgramOfferId == request.ProgramOfferId).SingleOrDefault();

            if (semesterSchedule == null)
                return Task.FromResult(new ResidentExamDetailResponse
                {
                    IsSuccessful = false,
                    Message = "Please ensure that the schedule for the selected semester exists",
                    ResidentExams = null
                });
            var scheduleItemType = ScheduleItemType.Examination;

            var examinationSchedule = programManagementUnitOfWork.ProgramManagementRepository
                .FindBy<SemesterScheduleItem>(x =>
                    x.SemesterScheduleId == semesterSchedule.Id && x.ScheduleItemType == scheduleItemType)
                .FirstOrDefault();
            if (examinationSchedule == null)
                return Task.FromResult(new ResidentExamDetailResponse
                {
                    IsSuccessful = false,
                    Message = "Please ensure that an examination schedule exists for the semester",
                    ResidentExams = null
                });

              var timeTableInfo = programManagementUnitOfWork.ProgramManagementRepository
                            .FindByWithInclude<Timetable>(x => x.SemesterScheduleItemId == examinationSchedule.Id, 
                                x=>x.Examination.Course).ToList();
                        if(!timeTableInfo.Any())
                            return Task.FromResult(new ResidentExamDetailResponse
                            {
                                IsSuccessful = false,
                                Message = "Examination timetable for the selected semester does not exist",
                                ResidentExams = null
                            });
                        
               var residentCourseWorks = residentUnitOfWork.ResidentManagementRepository
                         .FindByWithInclude<ResidentCourseWork>(x=>x.ProgramEnrollmentId == request.ProgramEnrollmentId
                                                         && x.ProgramCourse.SemesterId == request.SemesterId, 
                                                    x=>x.ProgramCourse).ToList();
            if(!residentCourseWorks.Any())
                return Task.FromResult(new ResidentExamDetailResponse
                {
                    IsSuccessful = false,
                    Message = "Resident course information not found.",
                    ResidentExams = null
                });

            var examScheduleDetails = timeTableInfo.Select(x => new {x.ExaminationId}).ToList();
            if (!examScheduleDetails.Any())
                return Task.FromResult(new ResidentExamDetailResponse
                {
                    IsSuccessful = false,
                    Message = "Resident exam details not found.",
                    ResidentExams = null
                });
            var courseWorkIds = residentCourseWorks.Select(x => x.Id).ToList();

            var examinationScores = programManagementUnitOfWork.ProgramManagementRepository
                .FindBy<ExaminationScore>(x => courseWorkIds.Contains(x.ResidentCourseWorkId.Value) 
                                               && x.ExamType == ExamType.Final).ToList();
            
            var residentExamDetailList = new List<ResidentExamDetail>();
            foreach (var residentCourse in residentCourseWorks)
            {
                var timetableInfo = timeTableInfo.
                    FirstOrDefault(x => x.Examination.CourseId == residentCourse.ProgramCourse.CourseId);
                if(timetableInfo == null)
                    continue;
                var examScore = examinationScores
                    .FirstOrDefault(x => x.ResidentCourseWorkId == residentCourse.Id)?.Score;
                
                var examDetail = new ResidentExamDetail
                {
                    Code = timetableInfo.Examination.Course.Code,
                    ExaminationId = timetableInfo.ExaminationId ?? 0,
                    Name = timetableInfo.Examination.Name,
                    ResidentCourseWorkId =  residentCourse.Id,
                    Score = examScore.HasValue ? Math.Floor(examScore.Value) : (decimal?) null,
                    Submitted = examScore.HasValue,
                    TimetableId = timetableInfo.Id,
                    StrScore = examScore.HasValue ? examScore.ToString() : null,
                    ExamType = ExamType.Final,   
                };
               residentExamDetailList.Add(examDetail);
            }

            return Task.FromResult(new ResidentExamDetailResponse
            {
                IsSuccessful = true,
                Message = "Exam details fetched successfully",
                ResidentExams = residentExamDetailList,
                ExamScheduleDescription = examinationSchedule.Name,
                SemesterScheduleItemId = examinationSchedule.Id
            });
        }
    }


    public class GetResidentExamDetail : IRequest<ResidentExamDetailResponse>
    {
        public int ProgramEnrollmentId { get; set; }
        public int SemesterId { get; set; }
        public int ProgramOfferId { get; set; }
    }

    public class GetResidentCatScoreDetails : IRequest<ResidentExamDetailResponse>
    {
        public int ProgramEnrollmentId { get; set; }
        public int SemesterId { get; set; }
        public int ProgramOfferId { get; set; }
    }

    public class GetResidentCatScoreDetailsQueryHandler : IRequestHandler<GetResidentCatScoreDetails, ResidentExamDetailResponse>
    {
        readonly IResidentManagementUnitOfWork _residentUnitOfWork;
        readonly IProgramManagementUnitOfWork _programManagementUnitOfWork;
        readonly IMapper _mapper;

        public GetResidentCatScoreDetailsQueryHandler(IResidentManagementUnitOfWork residentUnitOfWork,
            IProgramManagementUnitOfWork programManagementUnitOfWork, IMapper mapper)
        {
            _residentUnitOfWork = residentUnitOfWork;
            _programManagementUnitOfWork = programManagementUnitOfWork;
            _mapper = mapper;
        }

        public Task<ResidentExamDetailResponse> Handle(GetResidentCatScoreDetails request,
            CancellationToken cancellationToken)
        {
            var semesterSchedule = _programManagementUnitOfWork.ProgramManagementRepository
                .FindByWithInclude<SemesterSchedule>(x => x.SemesterId == request.SemesterId &&
                                                          x.ProgramOfferId == request.ProgramOfferId).SingleOrDefault();

            if (semesterSchedule == null)
                return Task.FromResult(new ResidentExamDetailResponse
                {
                    IsSuccessful = false,
                    Message = "Please ensure that the schedule for the selected semester exists",
                    ResidentExams = null
                });
            var catSchedule = _programManagementUnitOfWork.ProgramManagementRepository
                .FindByWithInclude<SemesterScheduleItem>(x =>
                    x.SemesterScheduleId == semesterSchedule.Id && x.ScheduleItemType == ScheduleItemType.Cat)
                .FirstOrDefault();
            if (catSchedule == null)
                return Task.FromResult(new ResidentExamDetailResponse
                {
                    IsSuccessful = false,
                    Message = "Please ensure that an cat schedule exists for the semester",
                    ResidentExams = null
                });
            var timeTableInfo = _programManagementUnitOfWork.ProgramManagementRepository
                .FindByWithInclude<Timetable>(x => x.SemesterScheduleItemId == catSchedule.Id, 
                    x=>x.Examination.Course).ToList();
            if(!timeTableInfo.Any())
                return Task.FromResult(new ResidentExamDetailResponse
                {
                    IsSuccessful = false,
                    Message = "Examination timetable for the selected semester does not exist",
                    ResidentExams = null
                });
            
            var residentCourseWorks = _residentUnitOfWork.ResidentManagementRepository
                .FindByWithInclude<ResidentCourseWork>(x=>x.ProgramEnrollmentId == request.ProgramEnrollmentId
                                             && x.ProgramCourse.SemesterId == request.SemesterId, 
                    x=>x.ProgramCourse).ToList();

            var residentCatDetails = new List<ResidentExamDetail>();
            foreach (var timetable in timeTableInfo)
            {     
                foreach (var residentCourseWork in residentCourseWorks)
                {
                    if(timetable.Examination.CourseId != residentCourseWork.ProgramCourse.CourseId)
                        continue;
                    var examType = ExamTypeHelper.GetExamTypeEnum(timetable.TimeTableType);
                    var examScore = _programManagementUnitOfWork.ProgramManagementRepository
                        .FindBy<ExaminationScore>(x =>
                            x.ResidentCourseWorkId == residentCourseWork.Id
                            && x.ExamType == examType).SingleOrDefault();
                    
                    var residentExamDetail = new ResidentExamDetail
                    {
                         Code = timetable.Examination.Course.Code,
                         ExamType =  examType,
                         Name = timetable.Examination.Name,
                         StrExamType = DispalyAttributeFetcher.GetDisplayAttributeValue(typeof(ExamType),examType.ToString()),
                         ResidentCourseWorkId =  residentCourseWork.Id,
                         ExaminationId = timetable.ExaminationId ?? 0,
                         Score = examScore?.Score,
                         StrScore = examScore?.Score.ToString(CultureInfo.InvariantCulture),
                         TimetableId = timetable?.Id ?? 0,
                         Submitted = examScore != null
                    };
                    
                    residentCatDetails.Add(residentExamDetail);
                }
            }
            
           
           
            return Task.FromResult(new ResidentExamDetailResponse
            {
                IsSuccessful = true,
                Message = "Cat details fetched successfully",
                ResidentExams = residentCatDetails,
                ExamScheduleDescription = catSchedule.Name,
                SemesterScheduleItemId = catSchedule.Id,
              
            });
        }
    }

    public class ResidentExamDetailResponse
    {
        public List<ResidentExamDetail> ResidentExams { get; set; }
        public string Message { get; set; }
        public bool IsSuccessful { get; set; }
        public string ExamScheduleDescription { get; set; }
        public int SemesterScheduleItemId { get; set; }
    }
}