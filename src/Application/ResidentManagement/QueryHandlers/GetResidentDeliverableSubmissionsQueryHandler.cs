﻿using Application.ResidentManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetResidentDeliverableSubmissionsQueryHandler : IRequestHandler<GetResidentDeliverableSubmissions, List<ResidentDeliverableSubmissionViewModel>>
    {
        IResidentManagementUnitOfWork unitOfWork;
        IMapper mapper;

        public GetResidentDeliverableSubmissionsQueryHandler(IResidentManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }

        public Task<List<ResidentDeliverableSubmissionViewModel>> Handle(GetResidentDeliverableSubmissions request, CancellationToken cancellationToken)
        {
            var residentSubmissions = unitOfWork.ResidentManagementRepository.FindBy<ResidentDeliverableItem>(x => x.ResidentDeliverableId == request.ResidentDeliverableId);

            var submissionViewModel = mapper.Map<List<ResidentDeliverableSubmissionViewModel>>(residentSubmissions);

            return Task.FromResult(submissionViewModel);
        }
    }      
}

public class GetResidentSubmissionHandler : IRequestHandler<GetDeliverableSubmission, ResidentDeliverableSubmissionViewModel>
{
    IResidentManagementUnitOfWork unitOfWork;
    IMapper mapper;
    public GetResidentSubmissionHandler(IResidentManagementUnitOfWork _unitOfWork, IMapper _mapper)
    {
        unitOfWork = _unitOfWork;
        mapper = _mapper;
    }
    public Task<ResidentDeliverableSubmissionViewModel> Handle(GetDeliverableSubmission request, CancellationToken cancellationToken)
    {
        var residentSubmission= unitOfWork.ResidentManagementRepository.Get<ResidentDeliverableItem>(request.SubmissionId);

        var submissionViewModel = mapper.Map<ResidentDeliverableSubmissionViewModel>(residentSubmission);

        return Task.FromResult(submissionViewModel);
    }
}
