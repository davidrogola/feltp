﻿using AutoMapper;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.QueryHandlers
{

    public class GetResidentExaminationScore : IRequest<List<ResidentExaminationScoreViewModel>>
    {
        public int EnrollmentId { get; set; }
        public int ? SemesterId { get; set; }

    }

    public class ResidentExaminationScoreViewModel
    {
        public int UnitId { get; set; }
        public long ResidentUnitId { get; set; }
        public string Code { get; set; }
        public string CreatedBy { get; set; }
        public string DateCreated { get; set; }
        public int ProgramEnrollmentId { get; set; }
        public long ResidentId { get; set; }
        public long ResidentCourseWorkId { get; set; }
        public string Name { get; set; }
        public int SemesterId { get; set; }
        public string Semester { get; set; }
        public int ExaminationId { get; set; }
        public decimal? Score { get; set; }
        public bool HasFieldPlacementActivity { get; set; }

    }

    public class GetResidentExaminationScoreQueryHandler : IRequestHandler<GetResidentExaminationScore, List<ResidentExaminationScoreViewModel>>
    {
        IResidentManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetResidentExaminationScoreQueryHandler(IResidentManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<ResidentExaminationScoreViewModel>> Handle(GetResidentExaminationScore request, CancellationToken cancellationToken)
        {
            var residentExaminations = request.SemesterId.HasValue ? unitOfWork.ResidentManagementRepository.FindBy<ResidentExaminationView>
                (x => x.ProgramEnrollmentId == request.EnrollmentId && x.SemesterId == request.SemesterId.Value)
                : unitOfWork.ResidentManagementRepository.FindBy<ResidentExaminationView>(x => x.ProgramEnrollmentId == request.EnrollmentId);

            var examViewModel = mapper.Map<List<ResidentExaminationScoreViewModel>>(residentExaminations.OrderBy(x => x.SemesterId).ToList());

            return Task.FromResult(examViewModel);
        }
    }
}
