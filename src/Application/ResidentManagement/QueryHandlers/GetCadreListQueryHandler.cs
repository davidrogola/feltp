﻿using Application.ResidentManagement.Queries;
using AutoMapper;
using Common.Domain.Person;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.QueryHandlers
{
   public class GetCadreListQueryHandler : IRequestHandler<GetCadreList, List<CadreViewModel>>
    {
        IResidentManagementUnitOfWork unitOfWork;
        public GetCadreListQueryHandler(IResidentManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<List<CadreViewModel>> Handle(GetCadreList request, CancellationToken cancellationToken)
        {
            var cadre = unitOfWork.ResidentManagementRepository.GetAll<Cadre>();
            var CadreViewModel = Mapper.Map<List<CadreViewModel>>(cadre);

            return Task.FromResult(CadreViewModel);
        }
    }
}