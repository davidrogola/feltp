﻿using Application.ResidentManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using IdentityModel;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetResidentPlacementInformationHandler : IRequestHandler<GetResidentPlacementInformation, ResidentFieldPlacementInfoViewModel>
    {
        IResidentManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetResidentPlacementInformationHandler(IResidentManagementUnitOfWork _unitOfWork,
        IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<ResidentFieldPlacementInfoViewModel> Handle(GetResidentPlacementInformation request, CancellationToken cancellationToken)
        {
            var fieldPlacement = unitOfWork.ResidentManagementRepository.FindBy<ResidentFieldPlacement>
                (x => x.ResidentFieldPlacementActivityId == request.ResidentFieldPlacementActivityId).SingleOrDefault();

            var placementViewModel = mapper.Map<ResidentFieldPlacementInfoViewModel>(fieldPlacement);

            return Task.FromResult(placementViewModel);
        }
    }

    public class GetResidentFieldPlacementSitesHandler : IRequestHandler<GetResidentFieldPlacementSites,
        List<ResidentFieldPlacementInfoViewModel>>
    {
        private readonly IMapper _mapper;
        private readonly IResidentManagementUnitOfWork _unitOfWork;
        public GetResidentFieldPlacementSitesHandler(IMapper mapper, IResidentManagementUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public Task<List<ResidentFieldPlacementInfoViewModel>> Handle(GetResidentFieldPlacementSites request, CancellationToken cancellationToken)
        {
            var residentId = _unitOfWork.ResidentManagementRepository
                .FindBy<Resident>(x => x.ApplicantId == request.ApplicantId).SingleOrDefault()?.Id;
            if(!residentId.HasValue)
                 return Task.FromResult(new List<ResidentFieldPlacementInfoViewModel>());
            var fieldPlacementSites = _unitOfWork.ResidentManagementRepository
                .FindBy<ResidentFieldPlacement>(x => x.ResidentId == residentId.Value).Distinct().ToList();

            var placementViewModel = _mapper.Map<List<ResidentFieldPlacementInfoViewModel>>(fieldPlacementSites);
            
            return Task.FromResult(placementViewModel);
        }
    }
    
    
    
}
