﻿using Application.ResidentManagement.Queries;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetApplicantFileQueryHandler : IRequestHandler<GetApplicantStatementFile, PersonalStatementFileViewModel>
    {
        IResidentManagementUnitOfWork unitOfWork;
        public GetApplicantFileQueryHandler(IResidentManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }

        public Task<PersonalStatementFileViewModel> Handle(GetApplicantStatementFile request, CancellationToken cancellationToken)
        {
            var statementFile = unitOfWork.ResidentManagementRepository.FindBy<PersonalStatementFile>
                (x => x.ProgramOfferApplicationId == request.ProgramOfferApplicationId).SingleOrDefault();

            if (statementFile == null)
                return Task.FromResult(new PersonalStatementFileViewModel());

            var statementFileViewModel = new PersonalStatementFileViewModel()
            {
                ContentType = statementFile.ContentType,
                FileContents = statementFile.File,
                FileName = statementFile.FileName
            };

            return Task.FromResult(statementFileViewModel);

        }
    }
}
