﻿using Application.ResidentManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetResidentUnitsQueryHandler : IRequestHandler<GetResidentUnits, List<ResidentUnitViewModel>>
    {
        IResidentManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetResidentUnitsQueryHandler(IResidentManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {

            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<ResidentUnitViewModel>> Handle(GetResidentUnits request, CancellationToken cancellationToken)
        {

            var residentUnits = unitOfWork.ResidentManagementRepository.FindBy<ResidentUnitView>
                (x => x.ResidentCourseWorkId == request.ResidentCourseWorkId);

            var residentUnitsViewModel = mapper.Map<List<ResidentUnitViewModel>>(residentUnits);

            return Task.FromResult(residentUnitsViewModel);

        }
    }
}
