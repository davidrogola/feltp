﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;
using MediatR;

using ResidentManagement.Domain;
using Application.Common.Models;
using Application.ResidentManagement.Queries;
using Persistance.ResidentManagement.UnitOfWork;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using Application.ProgramManagement.Queries;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetResidentListQueryHandler : IRequestHandler<GetResidentListQuery, List<ProgramOfferApplicationViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetResidentListQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }

        public Task<List<ProgramOfferApplicationViewModel>> Handle(GetResidentListQuery request, CancellationToken cancellationToken)
        {
            List<ProgramOfferApplicants> residents = null;
            
            switch (request.GetResidentBy)
            {
                case GetBy.All:

                    residents = unitOfWork.ProgramManagementRepository.FindBy<ProgramOfferApplicants>(x => x.ResidentId.HasValue)
                   .GroupBy(x => x.ResidentId).Select(x => x.FirstOrDefault()).ToList();
                    break;
                case GetBy.Program:
                    residents = unitOfWork.ProgramManagementRepository.FindBy<ProgramOfferApplicants>(x => x.ResidentId.HasValue && x.ProgramId == 
                    (int)request.Id).GroupBy(x => x.ResidentId).Select(x => x.FirstOrDefault()).ToList();

                    break;
                case GetBy.ProgramOffer:
                    residents = unitOfWork.ProgramManagementRepository.FindBy<ProgramOfferApplicants>(x => x.ResidentId.HasValue && 
                    x.ProgramOfferId == (int)request.Id).GroupBy(x => x.ResidentId).Select(x => x.FirstOrDefault()).ToList();
                    break;
                case GetBy.ResidentId:
                    residents = unitOfWork.ProgramManagementRepository.FindBy<ProgramOfferApplicants>(x => x.ResidentId == (long)request.Id)
                        .GroupBy(x => x.ResidentId).Select(x => x.FirstOrDefault()).ToList();
                    break;
            }

            var residentViewModel = mapper.Map<List<ProgramOfferApplicationViewModel>>(residents);

            return Task.FromResult(residentViewModel);
        }
    }
}
