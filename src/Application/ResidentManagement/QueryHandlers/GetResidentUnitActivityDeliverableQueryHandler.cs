﻿using Application.ResidentManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetResidentUnitActivityDeliverableQueryHandler : IRequestHandler<GetResidentUnitActivityDeliverableQuery, List<ResidentUnitActivityDeliverableViewModel>>
    {
        IResidentManagementUnitOfWork unitOfWork;
        IProgramManagementUnitOfWork programUnitOfWork;
        IMapper mapper;
        public GetResidentUnitActivityDeliverableQueryHandler(IResidentManagementUnitOfWork _unitOfWork, IProgramManagementUnitOfWork _programUnitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            programUnitOfWork = _programUnitOfWork;
            mapper = _mapper;
        }
        public Task<List<ResidentUnitActivityDeliverableViewModel>> Handle(GetResidentUnitActivityDeliverableQuery request, CancellationToken cancellationToken)
        {
            var residentUnitDeliverables = unitOfWork.ResidentManagementRepository
                .FindBy<ResidentUnitActivityDeliverableView>(x => x.ProgramEnrollmentId == request.ProgramEnrollmentId
                && x.SemesterId == request.SemesterId);

            var deliverableModel = mapper.Map<List<ResidentUnitActivityDeliverableViewModel>>(residentUnitDeliverables);

            return Task.FromResult(deliverableModel);
        }
    }

    public class GetResidentUnitDeliverablesQueryHandler : IRequestHandler<GetResidentUnitDeliverables, List<ResidentUnitActivityDeliverableViewModel>>
    {
        IResidentManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetResidentUnitDeliverablesQueryHandler(IResidentManagementUnitOfWork _unitOfWork,IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<ResidentUnitActivityDeliverableViewModel>> Handle(GetResidentUnitDeliverables request, CancellationToken cancellationToken)
        {
           var unitDeliverables = unitOfWork.ResidentManagementRepository
                .FindBy<ResidentUnitActivityDeliverableView>(x => x.ResidentUnitId == request.ResidentUnitId);

            var unitDeliverablesViewModel = mapper.Map<List<ResidentUnitActivityDeliverableViewModel>>(unitDeliverables);

            return Task.FromResult(unitDeliverablesViewModel);
        }
    }
}
