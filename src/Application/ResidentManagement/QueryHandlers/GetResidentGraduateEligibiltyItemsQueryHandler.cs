﻿using Application.ResidentManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetResidentGraduateEligibiltyItemsQueryHandler : IRequestHandler<GetResidentGraduateEligibiltyItems, ResidentGraduationEligibityItems>
    {
        IResidentManagementUnitOfWork unitOfWork;
        IMapper mapper;
        IMediator mediator;
        public GetResidentGraduateEligibiltyItemsQueryHandler(IResidentManagementUnitOfWork _unitOfWork, IMapper _mapper, IMediator _mediator)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
            mediator = _mediator;
        }
        public async Task<ResidentGraduationEligibityItems> Handle(GetResidentGraduateEligibiltyItems request, CancellationToken cancellationToken)
        {
            var graduationEligibityItems = unitOfWork.ResidentManagementRepository.FindBy<GraduationEligibityItemsView>(x => x.GraduateId == request.GraduateId);

            var graduateEligibityViewModel = mapper.Map<List<GraduateEligibityItemsViewModel>>(graduationEligibityItems);

            var residentGraduationDetails = await mediator.Send(new GetGraduate { GraduateId = request.GraduateId });

            return new ResidentGraduationEligibityItems
            {
                ResidentGraduationView = residentGraduationDetails,
                GraduateEligibiltyItems = graduateEligibityViewModel
            };
        }
    }
}
