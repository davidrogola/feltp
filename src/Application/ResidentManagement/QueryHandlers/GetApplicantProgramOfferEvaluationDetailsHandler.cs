﻿using Application.ProgramManagement.Queries;
using Application.ResidentManagement.Models;
using Application.ResidentManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetApplicantProgramOfferEvaluationDetailsHandler : IRequestHandler<GetApplicantProgramOfferEvaluationDetails, ApplicantEvaluationViewModel>
    {
        IResidentManagementUnitOfWork unitOfWork;
        IMediator mediator;
        public GetApplicantProgramOfferEvaluationDetailsHandler(IResidentManagementUnitOfWork _unitOfWork
            , IMediator _mediator)
        {
            unitOfWork = _unitOfWork;
            mediator = _mediator;
        }
        public async Task<ApplicantEvaluationViewModel> Handle(GetApplicantProgramOfferEvaluationDetails request,
            CancellationToken cancellationToken)
        {
            var programOfferApplications = await mediator.Send(new GetProgramOfferApplicationByApplicantId { ApplicantId = request.ApplicantId });

            var programOffer = programOfferApplications.Find(x => x.Id == request.ProgramOfferApplicationId);
            if (programOffer == null)
                return new ApplicantEvaluationViewModel();

            var interviewScore = unitOfWork.ResidentManagementRepository.FindBy<InterviewScore>
                (x => x.ProgramOfferApplicationId == programOffer.Id).LastOrDefault();

            var evaluationModel = new ApplicantEvaluationViewModel()
            {
                ProgramOfferApplicationDetail = programOffer
            };

            if (interviewScore != null)
                evaluationModel.InterviewScore =new InterviewScoreModel(interviewScore.OralInterview, interviewScore.WrittenInterview);

            return evaluationModel;
        }
    }
}
