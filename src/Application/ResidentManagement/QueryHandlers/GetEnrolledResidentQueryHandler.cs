﻿using Application.ResidentManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetEnrolledResidentsQueryHandler : IRequestHandler<GetEnrolledResidents, List<EnrolledResidentsViewModel>>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IResidentManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetEnrolledResidentsQueryHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork,
        IResidentManagementUnitOfWork _residentManagementUnitOfWork, IMapper _mapper)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            unitOfWork = _residentManagementUnitOfWork;
            mapper = _mapper;
        }
        public Task<List<EnrolledResidentsViewModel>> Handle(GetEnrolledResidents request, CancellationToken cancellationToken)
        {

            var enrolledResidents = programManagementUnitOfWork.ProgramManagementRepository
                 .FindBy<ProgramOfferApplicants>(x => x.ProgramOfferId == request.ProgramOfferId && x.EnrollmentStatus == EnrollmentStatus.Enrolled).ToList();

            var enrolledResidentsVewModel = enrolledResidents.Select(x => new EnrolledResidentsViewModel
            {
                ResidentId = x.ResidentId,
                CreatedBy = x.ShortListedBy,
                ProgramOfferId = request.ProgramOfferId,
                ResidentName = $"{ x.FirstName} {x.LastName}",
                RegistrationNumber = x.RegistrationNumber,
                EnrollmentId = x.EnrollmentId
            }).ToList();

            return Task.FromResult(enrolledResidentsVewModel);
        }
    }
}
