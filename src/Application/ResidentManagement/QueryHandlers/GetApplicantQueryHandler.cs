﻿using Application.Common.Models;
using Application.Common.Models.Queries;
using Application.ProgramManagement.Queries;
using Application.ResidentManagement.Queries;
using AutoMapper;
using Common.Domain.Address;
using Common.Domain.Person;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetApplicantQueryHandler : IRequestHandler<GetApplicantQuery, ApplicantViewModel>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IMediator mediator;
        IMapper mapper;
        
        public GetApplicantQueryHandler(IProgramManagementUnitOfWork _programUnitOfWork, IMapper _mapper, IMediator _mediator)
        {
            programManagementUnitOfWork = _programUnitOfWork;
            mediator = _mediator;
            mapper = _mapper;
        }
        public async Task<ApplicantViewModel> Handle(GetApplicantQuery request, CancellationToken cancellationToken)
        {
            var applicantProgramApplications = programManagementUnitOfWork.ProgramManagementRepository
                .FindBy<ProgramOfferApplicants>(x => x.ApplicantId == request.Id).AsEnumerable();

            var programOfferApplications = programManagementUnitOfWork.ProgramManagementRepository.FindBy<ProgramOfferApplicants>
                (x => x.ApplicantId == request.Id);


            if (!programOfferApplications.Any())
                return new ApplicantViewModel();

            var applicant = request.ProgramOfferId.HasValue ? programOfferApplications.FirstOrDefault(x => x.ProgramOfferId == request.ProgramOfferId.Value) : programOfferApplications.FirstOrDefault();

            var personDetails = await mediator.Send(new GetPersonQuery { PersonId = applicant.PersonId });

            List<ResidentCourseWorkViewModel> residentCourseWork = new List<ResidentCourseWorkViewModel>();
            if (applicant.EnrollmentId.HasValue)
                residentCourseWork = await mediator.Send(new GetResidentCourseWork { EnrollmentId = applicant.EnrollmentId.Value });


            var applicantViewModel = new ApplicantViewModel()
            {
                Cadre = applicant.Cadre,
                CreatedBy = applicant.ShortListedBy,
                DateCreated = applicant.DateCreated.ToShortDateString(),
                Person = personDetails,
                ResidentId = applicant.ResidentId ?? 0,
                IsEnrolled = applicant.EnrollmentId.HasValue,
                RegistrationNumber = applicant.RegistrationNumber,
                RegisteredBy = applicant.EvaluatedBy,
                DateRegistered = applicant.DateEvaluated.HasValue ? applicant.DateEvaluated.Value.ToShortDateString() : null,
                IsResident = applicant.ResidentId.HasValue,
                Id = applicant.ApplicantId,
                ProgramOfferId = applicant.ProgramOfferId,
                CourseWorkGenerated = residentCourseWork.Any(),
                ProgramEnrollmentId = applicant.EnrollmentId ?? 0
            };

            return applicantViewModel;
        }
    }

    public class GetApplicantByIdNumberQueryHandler : IRequestHandler<GetApplicantByIdNumber, ProgramOfferApplicationViewModel>
    {
        IProgramManagementUnitOfWork programUnitOfWork;
        IMapper mapper;
        public GetApplicantByIdNumberQueryHandler(IProgramManagementUnitOfWork _programUnitOfWork, IMapper _mapper)
        {
            programUnitOfWork = _programUnitOfWork;
            mapper = _mapper;
        }
        public Task<ProgramOfferApplicationViewModel> Handle(GetApplicantByIdNumber request, CancellationToken cancellationToken)
        {
            var programOfferApplicant = programUnitOfWork.ProgramManagementRepository.FindBy<ProgramOfferApplicants>(x => x.IdentificationNumber == request.IdentificationNumber).FirstOrDefault();

            var model = mapper.Map<ProgramOfferApplicationViewModel>(programOfferApplicant);

            return Task.FromResult(model);
        }
    }
}
