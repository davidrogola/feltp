﻿using Application.ResidentManagement.Queries;
using MediatR;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.QueryHandlers
{
    public class SeachApplicantsQueryHander : IRequestHandler<SearchApplicantQuery, List<ApplicantSearchResult>>
    {
        Func<MySqlConnection> mySqlConnectionFactory;
        const string query = "search_applicant_query";
        public SeachApplicantsQueryHander(Func<MySqlConnection> _mySqlConnectionFactory)
        {
            mySqlConnectionFactory = _mySqlConnectionFactory;
        }
        public Task<List<ApplicantSearchResult>> Handle(SearchApplicantQuery request, CancellationToken cancellationToken)
        {
            var searchResults = new List<ApplicantSearchResult>();

            using (var con = mySqlConnectionFactory())
            using (var command = new MySqlCommand(query, con))
            {
                //append wildcards to be used in boolean mode fulltext search
                var identificationNumber = $"*{request.IdentificationNumber}*"; 

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("identificationNumber",identificationNumber);
                command.Parameters["identificationNumber"].Direction = ParameterDirection.Input;

                command.Connection.Open();

                using (var reader = command.ExecuteReader())
                {
                    while(reader.Read())
                    {
                        searchResults.Add(new ApplicantSearchResult
                        {
                            FirstName = reader.GetString(0),
                            LastName = reader.GetString(1),
                            IdentificationNumber = reader.GetString(2),
                            CreatedBy = reader.GetString(3),
                            Cadre = reader.GetString(4),
                            ApplicantId = reader.GetInt64(5)
                        });
                                         
                  }
                }
                return Task.FromResult(searchResults);
            }
        }
    }
}
