﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;
using MediatR;

using ResidentManagement.Domain;
using Application.Common.Models;
using Application.ResidentManagement.Queries;
using Persistance.ResidentManagement.UnitOfWork;
using System;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using Application.ProgramManagement.Queries;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetApplicantListQueryHandler : IRequestHandler<GetApplicantListQuery, List<ProgramOfferApplicationViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetApplicantListQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<ProgramOfferApplicationViewModel>> Handle(GetApplicantListQuery request, CancellationToken cancellationToken)
        {
            try
            {
                List<ProgramOfferApplicants> applicants = null;

                switch (request.GetApplicantBy)
                {
                    case GetBy.All:

                        applicants = unitOfWork.ProgramManagementRepository.FindBy<ProgramOfferApplicants>(null)
                       .GroupBy(x => x.ApplicantId).Select(x => x.FirstOrDefault()).ToList();
                        break;
                    case GetBy.Program:
                        applicants = unitOfWork.ProgramManagementRepository.FindBy<ProgramOfferApplicants>(x=>x.ProgramId ==(int)request.Id)
                            .GroupBy(x => x.ApplicantId).Select(x => x.FirstOrDefault()).ToList();

                        break;
                    case GetBy.ProgramOffer:

                        var applicantsEnumerable = request.IsResidentList ? unitOfWork.ProgramManagementRepository.FindBy<ProgramOfferApplicants>(x => x.ResidentId.HasValue && x.ProgramOfferId == (int)request.Id)
                             : unitOfWork.ProgramManagementRepository.FindBy<ProgramOfferApplicants>(x =>x.ProgramOfferId == (int)request.Id);

                        applicants = applicantsEnumerable
                            .GroupBy(x => x.ApplicantId)
                            .Select(x => x.FirstOrDefault())
                            .ToList();

                        break;
                    case GetBy.ResidentId:
                        applicants = unitOfWork.ProgramManagementRepository.FindBy<ProgramOfferApplicants>(x => x.ResidentId == 
                        (long) request.Id).GroupBy(x => x.ResidentId).Select(x => x.FirstOrDefault()).ToList();
                        break;
                }

                var applicantViewModel = mapper.Map<List<ProgramOfferApplicationViewModel>>(applicants);

                return Task.FromResult(applicantViewModel);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
