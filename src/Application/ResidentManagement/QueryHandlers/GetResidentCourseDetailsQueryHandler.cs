﻿using Application.ProgramManagement.Queries;
using Application.ResidentManagement.Queries;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetResidentCourseDetailsQueryHandler : IRequestHandler<GetResidentCourseDetails, ResidentCourseDetailViewModel>
    {
        IProgramManagementUnitOfWork programManagementUnitOfWork;
        IResidentManagementUnitOfWork residentManagementUnitOfWork;
        IMediator mediator;
        IMapper mapper;
        public GetResidentCourseDetailsQueryHandler(IProgramManagementUnitOfWork _programManagementUnitOfWork,
        IResidentManagementUnitOfWork _residentManagementUnitOfWork, IMapper _mapper, IMediator _mediator)
        {
            programManagementUnitOfWork = _programManagementUnitOfWork;
            residentManagementUnitOfWork = _residentManagementUnitOfWork;
            mapper = _mapper;
            mediator = _mediator;
        }
        public async Task<ResidentCourseDetailViewModel> Handle(GetResidentCourseDetails request, CancellationToken cancellationToken)
        {
            var enrollment = programManagementUnitOfWork.ProgramManagementRepository.FindByWithInclude<ProgramEnrollment>
                (x => x.ResidentId == request.ResidentId && x.ProgramOfferId == request.ProgramOfferId, 
                x => x.ProgramOffer.Program.ProgramTier).FirstOrDefault();

            if (enrollment == null)
                return new ResidentCourseDetailViewModel();

          

            var residentDetails = await mediator.Send(new GetResidentListQuery
            {
                Id = request.ResidentId,
                GetResidentBy = GetBy.ResidentId
            });

            var detail = residentDetails.FirstOrDefault();

            var courseDetails = mapper.Map<ResidentCourseDetailViewModel>(enrollment);

            courseDetails.RegistrationNumber = detail.RegistrationNumber;
            courseDetails.ResidentName = detail.Name;
            courseDetails.ProgramOfferId = request.ProgramOfferId;
            courseDetails.ApplicantId = detail.ApplicantId;

            var semesters = programManagementUnitOfWork.ProgramManagementRepository
                .FindByWithInclude<ProgramCourse>(x => x.ProgramId == enrollment.ProgramOffer.ProgramId, x => x.Semester)
                .GroupBy(x=>x.SemesterId)
                .Select(x=>x.FirstOrDefault())
                .Select(x => new SelectListItem
                {
                    Value = x.SemesterId.ToString(),
                    Text = x.Semester.Name
                }).ToList();

            courseDetails.SemesterDropDown = semesters;

            return courseDetails;
        }

    }
}
