﻿using Application.ResidentManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain.Ethics;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetEthicsApprovalDocumentQueryHandler : IRequestHandler<GetEthicsApprovalDocumentQuery, List<EthicsApprovalDocumentViewModel>>
    {
        IResidentManagementUnitOfWork unitOfWork;
        IMapper mapper;
       
        public GetEthicsApprovalDocumentQueryHandler(IResidentManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<EthicsApprovalDocumentViewModel>> Handle(GetEthicsApprovalDocumentQuery request, CancellationToken cancellationToken)
        {
            var ethicsApprovalDocument = request.Id.HasValue ?
                unitOfWork.ResidentManagementRepository.FindBy<EthicsApprovalDocument>(x => x.Id == request.Id.Value)
                : unitOfWork.ResidentManagementRepository.GetAll<EthicsApprovalDocument>();

            var ethicsApprovalModel = mapper.Map<List<EthicsApprovalDocumentViewModel>>(ethicsApprovalDocument);

            return Task.FromResult(ethicsApprovalModel);
        }
    }
}
