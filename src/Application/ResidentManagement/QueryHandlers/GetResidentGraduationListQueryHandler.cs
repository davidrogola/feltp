﻿using Application.ResidentManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetResidentGraduationListQueryHandler : IRequestHandler<GetResidentGraduationList, List<ResidentGraduationViewModel>>
    {
        IResidentManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetResidentGraduationListQueryHandler(IResidentManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }

        public Task<List<ResidentGraduationViewModel>> Handle(GetResidentGraduationList request, CancellationToken cancellationToken)
        {
            var residentGraduationInfo = unitOfWork.ResidentManagementRepository.FindBy<ResidentGraduationInfoView>(x => x.ProgramOfferId == request.ProgramOfferId);

            var graduationInfoModel = mapper.Map<List<ResidentGraduationViewModel>>(residentGraduationInfo);

            return Task.FromResult(graduationInfoModel);

        }
  }

    public class GetGraduateQueryHandler : IRequestHandler<GetGraduate, ResidentGraduationViewModel>
    {
        IResidentManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetGraduateQueryHandler(IResidentManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<ResidentGraduationViewModel> Handle(GetGraduate request, CancellationToken cancellationToken)
        {
            var graduateInfo = unitOfWork.ResidentManagementRepository.Get<ResidentGraduationInfoView>(request.GraduateId);

            var graduationInfoModel = mapper.Map<ResidentGraduationViewModel>(graduateInfo);

            return Task.FromResult(graduationInfoModel);
        }
    }
}
