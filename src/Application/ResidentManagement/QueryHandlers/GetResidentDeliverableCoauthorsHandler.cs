﻿using Application.ResidentManagement.Queries;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetResidentDeliverableCoauthorsHandler : IRequestHandler<GetResidentDeliverableCoauthors, List<ResidentDeliverableCoAuthorsViewModel>>
    {
        IResidentManagementUnitOfWork unitOfWork;
        public GetResidentDeliverableCoauthorsHandler(IResidentManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }

        public Task<List<ResidentDeliverableCoAuthorsViewModel>> Handle(GetResidentDeliverableCoauthors request, CancellationToken cancellationToken)
        {
            var coAuthors = unitOfWork.ResidentManagementRepository.FindByWithInclude<ResidentDeliverableCoauthor>(x => x.ResidentDeliverableId == request.ResidentDeliverableId.Value, x => x.EthicsApprovalCommittee);

            var coAuthorViewModel = coAuthors.Select(x => new ResidentDeliverableCoAuthorsViewModel
            {
                AuthorName = x.AuthorName,
                CommitteeId = x.EthicsApprovalCommittee.Id,
                CommitteeName = x.EthicsApprovalCommittee.Name
            }).ToList();

            return Task.FromResult(coAuthorViewModel);
        }
    }
}
