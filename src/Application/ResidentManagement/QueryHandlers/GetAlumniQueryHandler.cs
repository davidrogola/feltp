﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.ProgramManagement.Queries;
using Application.ResidentManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetAlumniQueryHandler : IRequestHandler<GetAlumniQuery, List<ResidentGraduationViewModel>>
    {
        private readonly IResidentManagementUnitOfWork _residentManagementUnitOfWork;
        private readonly IMapper _mapper;

        public GetAlumniQueryHandler(IResidentManagementUnitOfWork residentManagementUnitOfWork,
            IMapper mapper)
        {
            _residentManagementUnitOfWork = residentManagementUnitOfWork;
            _mapper = mapper;
        }

        public Task<List<ResidentGraduationViewModel>> Handle(GetAlumniQuery request, CancellationToken cancellationToken)
        {
            var alumniIds = _residentManagementUnitOfWork.ResidentManagementRepository.
                FindBy<Resident>(x => x.IsAlumni).Select(x => x.Id).ToList();

            List<ResidentGraduationInfoView> graduatedResidents = _residentManagementUnitOfWork.ResidentManagementRepository
                .FindBy<ResidentGraduationInfoView>(x=> alumniIds.Contains(x.ResidentId)).ToList();

            var graduateInfoViewModel = _mapper.Map<List<ResidentGraduationViewModel>>(graduatedResidents);
            return Task.FromResult(graduateInfoViewModel);
        }
    }
}
