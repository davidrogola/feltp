﻿using Application.ResidentManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetResidentFieldPlacementActivitiesQueryHandler : IRequestHandler<GetResidentFieldPlacementActivities, List<ResidentFieldPlacementActivityViewModel>>
    {
        IResidentManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetResidentFieldPlacementActivitiesQueryHandler(IResidentManagementUnitOfWork _unitOfWork,
            IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }

        public Task<List<ResidentFieldPlacementActivityViewModel>> Handle(GetResidentFieldPlacementActivities request, CancellationToken cancellationToken)
        {
            var residentActivities = request.SemesterId.HasValue ? unitOfWork.ResidentManagementRepository.FindBy<ResidentFieldPlacementActivityView>(x => x.ProgramEnrollmentId == request.ProgramEnrollmentId && x.SemesterId == request.SemesterId.Value)
                : unitOfWork.ResidentManagementRepository.FindBy<ResidentFieldPlacementActivityView>(x => x.ProgramEnrollmentId == request.ProgramEnrollmentId);

            var residentActivityModel = mapper.Map<List<ResidentFieldPlacementActivityViewModel>>(residentActivities);

            return Task.FromResult(residentActivityModel);
        }
    }
}
