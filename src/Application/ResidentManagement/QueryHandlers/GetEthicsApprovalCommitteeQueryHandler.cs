﻿using Application.ResidentManagement.Queries;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain.Ethics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetEthicsApprovalCommitteeQueryHandler : IRequestHandler<GetEthicsApprovalCommittee, List<EthicsApprovalCommitteeViewModel>>
    {
        IResidentManagementUnitOfWork residentUnitOfWork;
        public GetEthicsApprovalCommitteeQueryHandler(IResidentManagementUnitOfWork _residentUnitOfWork)
        {
            residentUnitOfWork = _residentUnitOfWork;
        }
        public Task<List<EthicsApprovalCommitteeViewModel>> Handle(GetEthicsApprovalCommittee request, CancellationToken cancellationToken)
        {
            var approvalCommittes = request.Id.HasValue ?
                residentUnitOfWork.ResidentManagementRepository.FindBy<EthicsApprovalCommittee>(x => x.Id == request.Id.Value)
                :residentUnitOfWork.ResidentManagementRepository.GetAll<EthicsApprovalCommittee>();

            var approvalViewModel = approvalCommittes.Select(x => new EthicsApprovalCommitteeViewModel
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            return Task.FromResult(approvalViewModel);
        }
    }
}
