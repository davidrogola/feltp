﻿using Application.ResidentManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ResidentManagement.UnitOfWork;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetResidentDeliverablesQueryHandler : IRequestHandler<GetResidentDeliverables, List<ResidentDeliverableViewModel>>
    {
        IResidentManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetResidentDeliverablesQueryHandler(IResidentManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<ResidentDeliverableViewModel>> Handle(GetResidentDeliverables request, CancellationToken cancellationToken)
        {
            IEnumerable<ResidentDeliverableView> residentDeliverables = null;
            if(request.ProgramEnrollmentId.HasValue)
            {
                residentDeliverables = request.SemesterId.HasValue ?
                    unitOfWork.ResidentManagementRepository
                    .FindByWithInclude<ResidentDeliverableView>(x => x.ProgramEnrollmentId == request.ProgramEnrollmentId.Value && x.SemesterId == request.SemesterId.Value) : unitOfWork.ResidentManagementRepository
                    .FindByWithInclude<ResidentDeliverableView>(x => x.ProgramEnrollmentId == request.ProgramEnrollmentId.Value);
            }
            else
            {
                residentDeliverables = unitOfWork.ResidentManagementRepository.FindByWithInclude<ResidentDeliverableView>
                    (x => x.ResidentFieldPlacementActivityId == request.ResidentFieldPlacementActivityId);
            }
            
            var residentDeliverableModel = mapper.Map<List<ResidentDeliverableViewModel>>(residentDeliverables);

            return Task.FromResult(residentDeliverableModel);
        }
    }

    public class GetResidentDeliverableQueryHandler : IRequestHandler<GetResidentDeliverable, ResidentDeliverableViewModel>
    {
        IResidentManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetResidentDeliverableQueryHandler(IResidentManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<ResidentDeliverableViewModel> Handle(GetResidentDeliverable request, CancellationToken cancellationToken)
        {
            var residentDeliverable = unitOfWork.ResidentManagementRepository.FindBy<ResidentDeliverableView>(x => x.ResidentDeliverableId == request.Id).SingleOrDefault();

            var deliverableViewModel = mapper.Map<ResidentDeliverableViewModel>(residentDeliverable);

            return Task.FromResult(deliverableViewModel);
        }
    }
}
