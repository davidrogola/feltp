﻿using Application.ResidentManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using Persistance.ResidentManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ResidentManagement.QueryHandlers
{
    public class GetResidentCourseAttendanceQueryHandler : IRequestHandler<GetResidentCourseAttendance, List<ResidentUnitAttendanceViewModel>>
    {
        IResidentManagementUnitOfWork residentManagementUnitOfWork;
        IMapper mapper;

        public GetResidentCourseAttendanceQueryHandler(IResidentManagementUnitOfWork _residentManagementUnitOfWork, IMapper _mapper)
        {
            residentManagementUnitOfWork = _residentManagementUnitOfWork;
            mapper = _mapper;

        }
        public Task<List<ResidentUnitAttendanceViewModel>> Handle(GetResidentCourseAttendance request, CancellationToken cancellationToken)
        {

            IEnumerable<ResidentUnitAttendanceView> residentUnitAttendance = null;
          
            switch (request.GetBy)
            {
                case GetTimetableBy.ResidentId:

                    residentUnitAttendance = residentManagementUnitOfWork.ResidentManagementRepository.FindByWithInclude<ResidentUnitAttendanceView>
                        (x => x.ResidentId == request.Id);
                    break;
                case GetTimetableBy.SemesterScheduleItemId:
                    residentUnitAttendance = residentManagementUnitOfWork.ResidentManagementRepository.FindByWithInclude<ResidentUnitAttendanceView>
                        (x => x.SemesterScheduleItemId == request.Id);
                    break;

                default:
                    residentUnitAttendance = residentManagementUnitOfWork.ResidentManagementRepository.FindByWithInclude<ResidentUnitAttendanceView>
                         (x => x.ResidentCourseWorkId == request.Id);
                    break;
            }


            var attendanceDetails = mapper.Map<List<ResidentUnitAttendanceViewModel>>(residentUnitAttendance);

            return Task.FromResult(attendanceDetails);
        }
    }
}
