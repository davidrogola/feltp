﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Models
{
    public class CourseHistory
    {
        public string EnrollmentNumber { get; set; }
        public string ProgramName { get; set; }
        public int ProgramTierId { get; set; }
        public string Cohort { get; set; }
        public string RegistrationNumber { get; set; }
        public bool ApprovedByCounty { get; set; }
    }
}
