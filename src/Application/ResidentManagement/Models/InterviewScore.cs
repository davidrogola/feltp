﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ResidentManagement.Models
{
    public class InterviewScoreModel
    {
        public InterviewScoreModel()
        {
                
        }
        public InterviewScoreModel(int oralScore, int writtenScore)
        {
            WrittenInterview = writtenScore;
            OralInterview = oralScore;
            Total = oralScore + writtenScore;
        }
       
        public int OralInterview { get; set; }
        public int WrittenInterview { get; set; }
        public int Total { get; set; }      
    }

}
