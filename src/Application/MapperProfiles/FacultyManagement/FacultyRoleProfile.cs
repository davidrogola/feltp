﻿using Application.FacultyManagement.Commands;
using Application.FacultyManagement.Queries;
using AutoMapper;
using FacultyManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.FacultyManagement
{
  public  class FacultyRoleProfile : Profile
    {
        public FacultyRoleProfile()
        {
            CreateMap<FacultyRoleViewModel, FacultyRole>().ReverseMap()
                .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue ? 
                Status.InActive.ToString() : Status.Active.ToString())).ReverseMap();

            CreateMap<UpdateFacultyRoleCommand, FacultyRole>().ReverseMap();
            CreateMap<DeactivateFacultyRoleCommand, FacultyRole>().ReverseMap();
        }
    }
}
