﻿using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using Application.ProgramManagement.QueryHandlers;
using AutoMapper;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.MapperProfiles.ProgramManagement
{
    public class ProgramOfferProfile : Profile
    {
        public ProgramOfferProfile()
        {
            String dateFormat = "dd/MM/yyyy";
            CreateMap<ProgramOffer, ProgramOfferViewModel>()
              .ForMember(x => x.Faculty, src => src.MapFrom(x => x.Faculty.Person.FirstName + " " + x.Faculty.Person.LastName))
              .ForMember(x => x.StrStartDate, src => src.MapFrom(x => x.StartDate.ToString(dateFormat)))
              .ForMember(x => x.StrEndDate, src => src.MapFrom(x => x.EndDate.ToString(dateFormat)))
              .ForMember(x => x.Duration, src=> src.MapFrom(x => x.Program.ConvertDurationToMonths()))
              .ForMember(x => x.StrApplicationStartDate, src => src.MapFrom(x => x.ApplicationStartDate.ToString(dateFormat)))
              .ForMember(x => x.StrApplicationEndDate, src => src.MapFrom(x => x.ApplicationEndDate.ToString(dateFormat)))
              .ForMember(dest => dest.ProgramLocationId, src => src.MapFrom(x => x.ProgramOfferLocations.Where(c => c.DateDeactivated == null)
              .Select(d => d.ProgramLocationId).ToArray()))
              .ReverseMap();
        }
    }

    public class ProgramLocationProfile : Profile
    {
        public ProgramLocationProfile()
        {
            CreateMap<ProgramLocation, ProgramLocationViewModel>()
            .ForMember(x => x.Country, src => src.MapFrom(x => x.SubCounty.County.Country.Name))
            .ForMember(x => x.County, src => src.MapFrom(x => x.SubCounty.County.Name))
            .ForMember(x => x.SubCounty, src => src.MapFrom(x => x.SubCounty.Name))
            .ReverseMap();
        }
    }

    public class ProgramOfferGeneralRequirementProfile : Profile
    {
        public ProgramOfferGeneralRequirementProfile()
        {
            CreateMap<AddProgramGeneralQualificationRequirement, ProgramOfferGeneralRequirement>()
                .ForMember(dest => dest.DateCreated, src => src.MapFrom(x => DateTime.Now))
                .ReverseMap();

            CreateMap<GeneralRequirementsViewModel, ProgramOfferGeneralRequirement>().ReverseMap();
        }
    }

    public class ProgramOfferAcademicRequirementProfile : Profile
    {
        public ProgramOfferAcademicRequirementProfile()
        {
            CreateMap<AddProgramAcademicRequirementCommand, ProgramOfferAcademicRequirement>()
                .ForMember(dest => dest.DateCreated, src => src.MapFrom(x => DateTime.Now))
                .ReverseMap();

            CreateMap<ProgramOfferAcademicRequirement, AcademicRequirementsViewModel>()
                .ForMember(dest => dest.EducationLevel, src => src.MapFrom(x => x.AcademicCourse.EducationLevel.ToString()))
                .ForMember(dest => dest.CourseName, src => src.MapFrom(x => x.AcademicCourse.CourseName))
                .ForMember(dest => dest.MinimunGrade, src => src.MapFrom(x => x.MinimumCourseGradingId.HasValue ?
                x.MinimumCourseGrading.GradeName : "Not Specified")).ReverseMap();             
        }
    }

    public class ProgramCompletionRequirementProfile : Profile
    {
        public ProgramCompletionRequirementProfile()
        {
            CreateMap<ProgramCompletionRequirement, ProgramCompletionViewModel>()
                .ForMember(dest => dest.CompletedProgramName, src => src.MapFrom(x => x.CompletedProgram.Name))
                .ForMember(dest => dest.ProgramName, src => src.MapFrom(x => x.ProgramOffer.Program.Name))
                .ReverseMap();
        }
    }
}
