﻿using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using AutoMapper;
using ProgramManagement.Domain.Course;
using ProgramManagement.Domain.Event;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.ProgramManagement
{
    public class EventScheduleProfile : Profile
    {
        public EventScheduleProfile()
        {
                CreateMap<EventSchedule, EventScheduleViewModel>()
                .ForMember(src => src.ProgramEvent, dest => dest.MapFrom(x => x.ProgramEvent.Event.Title))
                .ForMember(src => src.ProgramName, dest => dest.MapFrom(x => x.ProgramEvent.Program.Name))
                .ForMember(src => src.StartDate, dest => dest.MapFrom(x => x.StartDate.ToShortDateString()))
                .ForMember(src => src.EndDate, dest => dest.MapFrom(x => x.EndDate.ToShortDateString()))
                .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue ? Status.InActive.ToString() : Status.Active.ToString()));

            }
        }
    }

