﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.ProgramManagement
{
    public class GraduationRequirementProfile : Profile
    {
        public GraduationRequirementProfile()
        {
            CreateMap<GraduationRequirement, GraduationRequirementViewModel>()
                .ForMember(x => x.Program, src => src.MapFrom(x => x.Program.Name))
                .ReverseMap();
        }
    }
}
