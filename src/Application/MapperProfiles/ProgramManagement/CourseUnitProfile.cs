﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using ProgramManagement.Domain.Course;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.ProgramManagement
{
    public class CourseUnitProfile : Profile
    {
        public CourseUnitProfile()
        {
            CreateMap<CourseUnit, CourseUnitViewModel>()
                .ForMember(dest => dest.Course, src => src.MapFrom(x => x.Course.Name))
                .ForMember(dest => dest.Unit, src => src.MapFrom(x => x.Unit.Name))
                .ForMember(dest => dest.Code, src => src.MapFrom(x => x.Unit.Code))
                .ForMember(dest => dest.HasActivity, src => src.MapFrom(x => x.Unit.HasFieldPlacementActivity))
                .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue ? Status.InActive.ToString() : Status.Active.ToString())).ReverseMap();

        }
    }
}
