﻿using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using AutoMapper;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.ProgramManagement
{
    public class SemesterProfile : Profile
    {
        public SemesterProfile()
        {
            CreateMap<AddSemesterCommand, Semester>().ReverseMap();
            CreateMap<SemesterViewModel, Semester>().ReverseMap()
                .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue ? 
                Status.InActive.ToString() : Status.Active.ToString())).ReverseMap();

            CreateMap<UpdateSemesterCommand, Semester>().ReverseMap();
            CreateMap<DeactivateSemesterCommand, Semester>().ReverseMap();
        }
    }
}
