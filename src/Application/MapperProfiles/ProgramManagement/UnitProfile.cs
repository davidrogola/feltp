﻿using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using AutoMapper;
using ProgramManagement.Domain.Course;
using ProgramManagement.Domain.Examination;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.MapperProfiles.ProgramManagement
{
    public class UnitProfile : Profile
    {
        public UnitProfile()
        {
            CreateMap<Unit, UnitViewModel>()
            .ForMember(x=>x.HasActivity,src=>src.MapFrom(x=>x.HasFieldPlacementActivity))
            .ForMember(x=>x.FieldPlacementActivityIds,src=>src.MapFrom(x=>x.FieldPlacementActivities.Where(r=>r.DateDeactivated == null)
            .Select(f=>f.FieldPlacementActivityId).ToArray()))
            .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue ? Status.InActive.ToString() : Status.Active.ToString()))
            .ReverseMap();

            CreateMap<Examination, ExaminationViewModel>().ReverseMap();
        }
    }
}
