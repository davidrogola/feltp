﻿using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using AutoMapper;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.ProgramManagement
{
    class DeliverableProfile : Profile
    {
        public DeliverableProfile()
        {
            CreateMap<AddDeliverableCommand, Deliverable>().ReverseMap();
            CreateMap<DeliverableViewModel, Deliverable>().ReverseMap()
             .ForMember(x=>x.StrHasThesisDefence,src=>src.MapFrom(x=>x.HasThesisDefence.ToString()))
             .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue ? 
             Status.InActive.ToString() : Status.Active.ToString()));

            CreateMap<UpdateDeliverableCommand, Deliverable>().ReverseMap();
        }

    }
}
