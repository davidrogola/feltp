﻿using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using AutoMapper;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.ProgramManagement
{
    public class CompetencyDeliverableProfile : Profile
    {
        public CompetencyDeliverableProfile()
        {

            CreateMap<CompetencyDeliverable, CompetencyDeliverableViewModel>()
            .ForMember(src => src.Competency, dest => dest.MapFrom(x => x.Competency.Name))
            .ForMember(src => src.Deliverable, dest => dest.MapFrom(x => x.Deliverable.Name))
            .ForMember(src => src.Duration, dest => dest.MapFrom(x => x.Deliverable.DurationInWeeks))
             .ForMember(src => src.Description, dest => dest.MapFrom(x => x.Deliverable.Description))
            .ForMember(src => src.Code, dest => dest.MapFrom(x => x.Deliverable.Code))
            .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue ? Status.InActive.ToString() : Status.Active.ToString())).ReverseMap();
        }

    }
}

