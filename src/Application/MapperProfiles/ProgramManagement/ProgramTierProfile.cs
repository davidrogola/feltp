﻿using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using AutoMapper;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.ProgramManagement
{
    public class ProgramTierProfile : Profile
    {
        public ProgramTierProfile()
        {
            CreateMap<AddProgramTierCommand, ProgramTier>().ReverseMap();
            CreateMap<ProgramTierViewModel, ProgramTier>().ReverseMap()
                .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue ? 
                Status.InActive.ToString() : Status.Active.ToString())).ReverseMap();
            CreateMap<UpdateProgramTierCommand, ProgramTier>().ReverseMap();
            CreateMap<DeactivateProgramTierCommand, ProgramTier>().ReverseMap();
        }
    }
}
