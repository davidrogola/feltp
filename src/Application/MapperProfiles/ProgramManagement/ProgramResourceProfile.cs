﻿using Application.ProgramManagement.Commands;
using ProgramManagement.Domain.Program;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Application.ProgramManagement.Queries;
using System.Linq;

namespace Application.MapperProfiles.ProgramManagement
{
    public class ProgramResourceProfile : Profile
    {
        public ProgramResourceProfile()
        {
            CreateMap<AddProgramResourceCommand, ProgramResource>()
                .ForMember(src => src.ProgramId, dest => dest.MapFrom(x => x.ProgramId))
                .ForMember(src => src.ResourceId, dest => dest.MapFrom(x => x.ResourceId)).ReverseMap();

            CreateMap<ProgramResource, ProgramResourcesViewModel>()
               .ForMember(src => src.ResourceTypeId, dest => dest.MapFrom(x => x.Resource.ResourceTypeId))
               .ForMember(src => src.ResourceId, dest => dest.MapFrom(x => x.Resource.Id))
                .ForMember(src => src.ProgramName, dest => dest.MapFrom(x => x.Program.Name))
                .ForMember(src => src.ResourceName, dest => dest.MapFrom(x => x.Resource.ResourceTitle))
                .ForMember(src => src.ResourceType, dest => dest.MapFrom(x => x.Resource.ResourceType.Name))
                .ForMember(src => src.Source, dest => dest.MapFrom(x => x.Resource.Source))
                .ForMember(src => src.SourceContactDetails, dest => dest.MapFrom(x => x.Resource.SourceContactDetails))
                .ForMember(src => src.ResourceDescription, dest => dest.MapFrom(x => x.Resource.ResourceDescription))
                .ForMember(src => src.ResourceType, dest => dest.MapFrom(x => x.Resource.ResourceType.Name))
                .ForMember(src => src.ProgramTier, dest => dest.MapFrom(x => x.Program.ProgramTier.Name))
                .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue ? Status.InActive.ToString() : Status.Active.ToString()));

        }
    }
}
