﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using ProgramManagement.Domain.FieldPlacement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.ProgramManagement
{
    public class FieldPlacementActivityDeliverableMapper : Profile
    {
        public FieldPlacementActivityDeliverableMapper()
        {
            CreateMap<FieldPlacementActivityDeliverable, FieldPlacementActivityDeliverablesViewModel>()
                .ForMember(dest => dest.Deliverable, src => src.MapFrom(x => x.Deliverable.Name))
                .ForMember(dest=>dest.Status, src=>src.MapFrom(x=>x.DateDeactivated.HasValue ? Status.InActive.ToString() : Status.Active.ToString()))
               .ForMember(dest => dest.FieldPlacementActivity, src => src.MapFrom(x => x.FieldPlacementActivity.Name)).ReverseMap();
        }
    }
}
