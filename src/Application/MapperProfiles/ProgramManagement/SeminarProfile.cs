﻿using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using AutoMapper;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.ProgramManagement
{
    public class SeminarProfile : Profile
    {
        public SeminarProfile()
        {
            CreateMap<AddSeminarCommand, Seminar>().ReverseMap();
            CreateMap<SeminarViewModel, Seminar>().ReverseMap()
                .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue ? Status.InActive.ToString() : Status.Active.ToString())).ReverseMap();

            CreateMap<UpdateSeminarCommand, Seminar>().ReverseMap();
        }
    }
}
