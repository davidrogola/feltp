﻿using Application.ProgramManagement.Commands;
using AutoMapper;
using FluentValidation;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.MapperProfiles.ProgramManagement
{
    public class EnrollResidentCommandValidator : AbstractValidator<EnrollResidentModel>
    {
        IProgramManagementUnitOfWork unitOfWork;
        public EnrollResidentCommandValidator(IProgramManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
            RuleFor(x => x.EnrollResidentCommand).Custom((model, context) =>
            {
                var applicantProgramApplications = unitOfWork.ProgramManagementRepository.FindBy<ProgramOfferApplicants>(x => x.ApplicantId == model.ApplicantId).ToList();

                var duplicateProgramEnrollment = applicantProgramApplications.FirstOrDefault(x => x.ProgramId == model.ProgramId && 
                x.EnrollmentStatus == EnrollmentStatus.Enrolled);

                if (duplicateProgramEnrollment != null)
                    context.AddFailure($"Applicant is already enrolled to the program {duplicateProgramEnrollment.Program} with offer {duplicateProgramEnrollment.ProgramOfferName}");

                var duplicateOfferEnrollment = applicantProgramApplications.FirstOrDefault(x => x.ProgramOfferId == model.ProgramOfferId && x.EnrollmentStatus == EnrollmentStatus.Enrolled);
                if (duplicateOfferEnrollment != null)
                    context.AddFailure($"Applicant is already registered to program offer {duplicateOfferEnrollment.ProgramOfferName}");
            });
        }
    }



}
