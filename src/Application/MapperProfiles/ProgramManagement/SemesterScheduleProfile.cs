﻿using Application.ProgramManagement.Queries;
using Application.ProgramManagement.QueryHandlers;
using AutoMapper;
using MediatR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Application.Common.Services;

namespace Application.MapperProfiles.ProgramManagement
{
    public class SemesterScheduleProfile : Profile
    {
        public SemesterScheduleProfile()
        {
            String dateFormat = "dd/MM/yyyy";
            CreateMap<SemesterSchedule, SemesterScheduleViewModel>()
                .ForMember(x => x.Status, src => src.MapFrom(x => x.Status.ToString()))
                .ForMember(x => x.EstimatedStartDate, src => src.MapFrom(x => x.EstimatedStartDate.ToString(dateFormat)))
                .ForMember(x => x.EstimatedEndDate, src => src.MapFrom(x => x.EstimatedEndDate.ToString(dateFormat)))
                .ForMember(x => x.Program, src => src.MapFrom(x => x.ProgramOffer.Program.Name))
                .ForMember(x => x.ProgramOffer, src => src.MapFrom(x => x.ProgramOffer.Name))
                .ForMember(x => x.Semester, src => src.MapFrom(x => x.Semester.Name))
                .ForMember(x => x.ProgramId, src => src.MapFrom(x => x.ProgramOffer.ProgramId))
                .ForMember(x => x.CourseScheduleCreated, src => src.MapFrom(x => x.SemesterScheduleItems
                .Any(s => s.ScheduleItemType == ScheduleItemType.Course)))
                .ForMember(x => x.ExaminationScheduleCreated, src => src.MapFrom(x => x.SemesterScheduleItems
                .Any(s => s.ScheduleItemType == ScheduleItemType.Examination)))
                .ForMember(x => x.CatScheduleCreated, src => src.MapFrom(x => x.SemesterScheduleItems
                    .Any(s => s.ScheduleItemType == ScheduleItemType.Cat)))
                .ForMember(x => x.SemesterCompletionRequestPending, src => src.MapFrom(x => !x.CompletionRequestDate.HasValue &&
                x.Status != ProgressStatus.Completed))
                .ReverseMap();

            CreateMap<SemesterScheduleItem, SemesterScheduleItemViewModel>()
                .ForMember(x => x.StartDate, src => src.MapFrom(x => x.StartDate.ToString(dateFormat)))
                .ForMember(x => x.EndDate, src => src.MapFrom(x => x.EndDate.ToString(dateFormat)))
                .ForMember(x=>x.CourseType,src=>src.MapFrom(x=>x.ProgramCourse.Course.Category.CourseType.Name))
                .ForMember(x => x.ScheduleItemType, src => src.MapFrom(x => x.ScheduleItemType.ToString()))
                .ForMember(x => x.TimetableCreated, src => src.MapFrom(x => x.TimetableCollection.Any()))
                .ForMember(x=>x.SemesterId, src =>src.MapFrom(x=>x.SemesterSchedule.SemesterId))
                 .ForMember(x => x.Status, src => src.MapFrom(x => x.Status.ToString()))
                .ReverseMap();
        }
    }

    public class TimetableProfile : Profile
    {
        public TimetableProfile()
        {
            CreateMap<Timetable, TimetableViewModel>()
                .ForMember(x => x.Faculty, src => src.MapFrom(x => ($"{ x.Faculty.Person.FirstName} { x.Faculty.Person.LastName}")))
                .ForMember(x => x.ExaminationName, src => src.MapFrom(x => x.Examination.Name))
                .ForMember(x => x.UnitName, src => src.MapFrom(x => x.Unit.Name))
                .ForMember(x => x.Duration, src => src.MapFrom(x => x.CalculateDuration()))
                .ForMember(x => x.StartDate, src => src.MapFrom(x => x.StartDate.ToShortDateString()))
                .ForMember(x => x.StartTime, src => src.MapFrom(x => x.StartDate.ToShortTimeString()))
                .ForMember(x => x.EndTime, src => src.MapFrom(x => x.EndDate.ToShortTimeString()))
                .ForMember(x => x.Code, src => src.MapFrom(x => x.UnitId.HasValue ? x.Unit.Code : x.Examination.Course.Code))
                .ForMember(x => x.ScheduleStartDate, src => src.MapFrom(x => x.SemesterScheduleItem.StartDate))
                .ForMember(x => x.ScheduleEndDate, src => src.MapFrom(x => x.SemesterScheduleItem.EndDate))
                .ForMember(x=>x.Semester, src=>src.MapFrom(x=>x.SemesterScheduleItem.SemesterSchedule.Semester.Name))
                .ForMember(x => x.TimeTableType,src => src.MapFrom(x => DispalyAttributeFetcher.GetDisplayAttributeValue(typeof(TimetableType),
                    x.TimeTableType.ToString())))
                .ForMember(x => x.Semester, src => src.MapFrom(x => x.SemesterScheduleItem.SemesterSchedule.SemesterId))
                .ReverseMap();
        }
    }
    public class SemesterScheduleChangeRequestProfile : Profile
    {
        public SemesterScheduleChangeRequestProfile()
        {
            CreateMap<SemesterScheduleChangeRequest, SemesterScheduleChangeRequestViewModel>()
                .ForMember(x => x.ProgramOffer, src => src.MapFrom(x => x.SemesterSchedule.ProgramOffer.Program.Name))
                .ForMember(x => x.Program, src => src.MapFrom(x => x.SemesterSchedule.ProgramOffer.Name))
                .ForMember(x => x.Semester, src => src.MapFrom(x => x.SemesterSchedule.Semester))
                .ForMember(x => x.ScheduleName, src => src.MapFrom(x => x.SemesterSchedule.Description))
                .ForMember(x => x.StartDate, src => src.MapFrom(x => x.StartDate.ToShortDateString()))
                .ForMember(x => x.EndDate, src => src.MapFrom(x => x.EndDate.ToShortDateString()))
                .ForMember(x => x.DateRequested, src => src.MapFrom(x => x.DateRequested.ToShortDateString()))
                .ForMember(x => x.Duration, src => src.MapFrom(x =>
                CalculateSemesterDuration(x.SemesterSchedule.ProgramOffer.Program.Courses.ToList(),x.SemesterSchedule.SemesterId)))
                .ForMember(x => x.OriginalStartDate, src => src.MapFrom(x => GetOriginalDates(x.OriginalValues).Item1))
                .ForMember(x => x.OriginalEndDate, src => src.MapFrom(x => GetOriginalDates(x.OriginalValues).Item2));
        }

        private Tuple<string, string> GetOriginalDates(string json)
        {
            dynamic originalValues = JObject.Parse(json);
            return new Tuple<string, string>(originalValues.StartDate.ToString(), originalValues.EndDate.ToString());
        }

        private int CalculateSemesterDuration(List<ProgramCourse> courses, int semesterId)
        {
            var semesterDuration = courses.Where(x => x.SemesterId == semesterId && x.DateDeactivated == null)
                .Sum(x => x.DurationInWeeks);
            return semesterDuration;
        }

    }
}
