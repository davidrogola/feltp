﻿using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using AutoMapper;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.MapperProfiles.ProgramManagement
{
    public class CollaboratorProfile : Profile
    {
        public CollaboratorProfile()
        {
            CreateMap<AddCollaboratorCommand, Collaborator>().ReverseMap();
            CreateMap<CollaboratorViewModel, Collaborator>().ReverseMap()
                .ForMember(x => x.CollaboratorType, src => src.MapFrom(x => x.CollaboratorTypeId))
                .ForMember(dest => dest.ProgramId, src => src.MapFrom(x => x.ProgramCollaborators.Where(c => c.DateDeactivated == null)
                .Select(d => d.ProgramId).ToArray()))
                .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue ? Status.InActive.ToString() : Status.Active.ToString())).ReverseMap();

            CreateMap<UpdateCollaboratorCommand, Collaborator>().ReverseMap();
        }
    }
}
