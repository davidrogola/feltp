﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.ProgramManagement
{
    public class ProgramCompetencyProfile : Profile
    {
        public ProgramCompetencyProfile()
        {
            CreateMap<ProgramCompetency, ProgramCompetencyViewModel>()
                .ForMember(x => x.Competency, src => src.MapFrom(x => x.Competency.Name))
                .ForMember(x => x.Program, src => src.MapFrom(x => x.Program.Name))
                .ReverseMap();
        }
    }
}
