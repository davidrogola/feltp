﻿using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using AutoMapper;
using ProgramManagement.Domain.FieldPlacement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.MapperProfiles.ProgramManagement
{
  public  class FieldPlacementSiteProfile : Profile
    {
        public FieldPlacementSiteProfile()
        {
            CreateMap<FieldPlacementSite,FieldPlacementSiteViewModel>()
                .ForMember(src => src.Country, dest => dest.MapFrom(x => x.Country.Name))
                .ForMember(src => src.County, dest => dest.MapFrom(x => x.County.Name))
                .ForMember(src => src.SubCounty, dest => dest.MapFrom(x => x.SubCounty.Name))
                .ForMember(src => src.FacultyId, dest => dest.MapFrom(x => x.SiteSupervisors.Where(f =>f.DateDeactivated == null)
                .Select(f=>f.FacultyId).Distinct().ToArray()))
                .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue ?
                Status.InActive.ToString() : Status.Active.ToString())).ReverseMap();

            CreateMap<UpdateFieldPlacementSiteCommand, FieldPlacementSite>().ReverseMap();
        }
    }
}
