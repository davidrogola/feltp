﻿using Application.ProgramManagement.Commands;
using ProgramManagement.Domain.Program;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Application.ProgramManagement.Queries;

namespace Application.MapperProfiles.ProgramManagement
{
    public class ProgramCollaboratorProfile : Profile
    {
        public ProgramCollaboratorProfile()
        {
            CreateMap<AddProgramCollaboratorCommand, ProgramCollaborator>()
                 .ForMember(src => src.ProgramId, dest => dest.MapFrom(x => x.ProgramId))
                .ForMember(src => src.CollaboratorId, dest => dest.MapFrom(x => x.CollaboratorId)).ReverseMap();

            CreateMap<ProgramCollaborator, ProgramCollaboratorViewModel>()
            .ForMember(src => src.ProgramName, dest => dest.MapFrom(x => x.Program.Name))
            .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue ? Status.InActive.ToString() : Status.Active.ToString()));

        }
    }
}
