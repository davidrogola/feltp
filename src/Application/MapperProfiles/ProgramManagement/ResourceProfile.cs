﻿using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using AutoMapper;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.MapperProfiles.ProgramManagement
{
    public class ResourseProfile : Profile
    {
        public ResourseProfile()
        {
            CreateMap<AddResourceCommand, Resource>().ReverseMap();
            CreateMap<ResourceViewModel, Resource>().ReverseMap()
                .ForMember(x => x.ResourceType, x => x.MapFrom(src => src.ResourceType.Name))
                .ForMember(x => x.ProgramId, x => x.MapFrom(src => src.LinkedPrograms.Where(p=>p.DateDeactivated == null)
                .Select(p=>p.ProgramId).ToList()))
                .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue ? 
                Status.InActive.ToString() : Status.Active.ToString()));

        }
    }
}
