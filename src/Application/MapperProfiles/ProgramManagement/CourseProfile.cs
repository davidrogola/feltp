﻿using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using AutoMapper;
using ProgramManagement.Domain.Course;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.ProgramManagement
{
    public class CourseProfile : Profile
    {
        public CourseProfile()
        {
            CreateMap<AddCourseCommand, Course>().ReverseMap();
            CreateMap<CourseViewModel, Course>().ReverseMap()
                .ForMember(x=>x.Category, x => x.MapFrom(src => src.Category.Name))
                .ForMember(x=>x.CourseType, x => x.MapFrom(src => src.Category.CourseType.Name))
                .ForMember(x=>x.CourseTypeId,x=>x.MapFrom(src=>src.Category.CourseTypeId))
                .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue ?
                Status.InActive.ToString() : Status.Active.ToString()));

            CreateMap<UpdateCourseCommand, Course>().ReverseMap();
        }
    }
}
