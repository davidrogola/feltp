﻿using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using Application.ProgramManagement.QueryHandlers;
using AutoMapper;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.ProgramManagement
{
    public class ProgramProfile : Profile 
    {
        public ProgramProfile()
        { 
            CreateMap<AddProgramCommand, Program>().ReverseMap();

            CreateMap<ProgramViewModel, Program>().ReverseMap()
             .ForMember(x => x.ProgramTier, x => x.MapFrom(src => src.ProgramTier.Name))
             .ForMember(x=>x.Duration,x=>x.MapFrom(src=>src.ConvertDurationToMonths()))
             .ForMember(dest => dest.Status, src=>src.MapFrom(x=>x.DateDeactivated.HasValue? 
             Status.InActive.ToString(): Status.Active.ToString()));

            CreateMap<UpdateProgramCommand, Program>().ReverseMap();
        }
    }

}
