﻿using Application.ProgramManagement.Queries;
using Application.ResidentManagement.Queries;
using AutoMapper;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;
using static Application.Common.Services.DispalyAttributeFetcher;

namespace Application.MapperProfiles.ProgramManagement
{
    public class ProgramOfferApplicantProfile : Profile
    {
        public ProgramOfferApplicantProfile()
        {
            CreateMap<ProgramOfferApplicants, ProgramOfferApplicationViewModel>()
                .ForMember(x => x.DateApplied, src => src.MapFrom(x => x.DateApplied.ToShortDateString()))
                .ForMember(x => x.ApplicationStatus, src => src.MapFrom(x => x.ApplicationStatus.ToString()))
                .ForMember(x => x.ApprovedByCounty, src => src.MapFrom(x => x.ApprovedByCounty.ToString()))
                .ForMember(x =>x.RegistrationNumber, src=>src.MapFrom(x=> string.IsNullOrEmpty(x.RegistrationNumber) ? "N/A": x.RegistrationNumber))
                .ForMember(x => x.EvaluatedBy, src => src.MapFrom(x => string.IsNullOrEmpty(x.EvaluatedBy) ? "N/A" : x.EvaluatedBy))
                .ForMember(x=>x.ProgressIndicator, src=>src.MapFrom(x=>x.GenerateProgressIndicator()))
                .ForMember(x => x.QualificationStatus, src => src.MapFrom(x => x.QualificationStatus.ToString()))
                .ForMember(x => x.StrApplicationStatus, src => src.MapFrom(x => SetOfferApplicationStatus(x.EnrollmentStatus, x.ApplicationStatus,x.QualificationStatus)))
                .ReverseMap();
        }

        private string SetOfferApplicationStatus(EnrollmentStatus enrollmentStatus, ApplicationStatus applicationStatus,
            ProgramQualificationStatus qualificationStatus )
        {
            if (qualificationStatus == ProgramQualificationStatus.NotQualified)
                return "Failed Evaluation";
            if (enrollmentStatus == EnrollmentStatus.Enrolled && applicationStatus == ApplicationStatus.Shortlisted)
                return EnrollmentStatus.Enrolled.ToString();
            if (applicationStatus == ApplicationStatus.Shortlisted)
                return "Pending Enrollment";
            else return "Pending Shortlisting";
        }
    }


}
