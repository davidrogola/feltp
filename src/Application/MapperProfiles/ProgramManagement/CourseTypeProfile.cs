﻿using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using AutoMapper;
using ProgramManagement.Domain.Course;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.ProgramManagement
{
    public class CourseTypeProfile  : Profile
    {
        public CourseTypeProfile()
        {
            CreateMap<AddCourseTypeCommand, CourseType>().ReverseMap();
            CreateMap<CourseTypeViewModel, CourseType>().ReverseMap()
                .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue ?
                Status.InActive.ToString() : Status.Active.ToString())).ReverseMap();
            CreateMap<UpdateCourseTypeCommand, CourseType>().ReverseMap();
        }
    }
}
