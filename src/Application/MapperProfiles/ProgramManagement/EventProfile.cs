﻿using System;
using AutoMapper;
using ProgramManagement.Domain.Event;
using System.Collections.Generic;
using System.Text;
using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;

namespace Application.MapperProfiles.ProgramManagement
{
    public class EventProfile : Profile
    {
        public EventProfile()
        {
            CreateMap<AddEventCommand, Event>().ReverseMap();
            CreateMap<EventViewModel, Event>().ReverseMap()
                .ForMember(x => x.EventType, src => src.MapFrom(x => x.EventTypeId))
                .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue ? Status.InActive.ToString() : Status.Active.ToString())).ReverseMap();

            CreateMap<UpdateEventCommand, Event>().ReverseMap();
        }
    }
}
