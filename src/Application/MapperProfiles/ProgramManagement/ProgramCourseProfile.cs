﻿using Application.ProgramManagement.Commands;
using ProgramManagement.Domain.Program;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Application.ProgramManagement.Queries;

namespace Application.MapperProfiles.ProgramManagement
{
    public class ProgramCourseProfile : Profile
    {
        public ProgramCourseProfile()
        {
            CreateMap<AddProgramCourseCommand, ProgramCourse>()
                .ForMember(src => src.SemesterId, dest => dest.MapFrom(x => x.SemesterId))
                .ForMember(src => src.ProgramId, dest => dest.MapFrom(x => x.ProgramId))
                .ForMember(src => src.CourseId, dest => dest.MapFrom(x => x.CourseId)).ReverseMap();

            CreateMap<ProgramCourse, ProgramCoursesViewModel>()
            .ForMember(src => src.Semester, dest => dest.MapFrom(x => x.Semester.Name))
            .ForMember(src => src.ProgramName, dest => dest.MapFrom(x => x.Program.Name))
            .ForMember(src => src.CourseName, dest => dest.MapFrom(x => x.Course.Name))
            .ForMember(src => src.Category, dest => dest.MapFrom(x => x.Course.Category.Name))
            .ForMember(src => src.CourseType, dest => dest.MapFrom(x => x.Course.Category.CourseType.Name))
            .ForMember(src => src.ProgramTier, dest => dest.MapFrom(x => x.Program.ProgramTier.Name))
            .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue ? Status.InActive.ToString() : Status.Active.ToString()));

            CreateMap<DeactivateProgramCourseCommand, ProgramCourse>().ReverseMap();
        }
    }
}
