﻿using Application.ProgramManagement.Commands;
using ProgramManagement.Domain.Program;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Application.ProgramManagement.Queries;
using ProgramManagement.Domain.Event;

namespace Application.MapperProfiles.ProgramManagement
{
    public class ProgramEventProfile : Profile
    {
        public ProgramEventProfile()
        {
            CreateMap<AddProgramEventCommand, ProgramEvent>()
                 .ForMember(src => src.ProgramId, dest => dest.MapFrom(x => x.ProgramId))
                .ForMember(src => src.EventId, dest => dest.MapFrom(x => x.EventId)).ReverseMap();

            CreateMap<ProgramEvent, ProgramEventViewModel>()
            .ForMember(src => src.ProgramName, dest => dest.MapFrom(x => x.Program.Name))
            .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue ? Status.InActive.ToString() : Status.Active.ToString()));

        }
    }
}
