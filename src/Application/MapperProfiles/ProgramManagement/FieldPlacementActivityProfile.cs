﻿using Application.ProgramManagement.Queries;
using AutoMapper;
using ProgramManagement.Domain.FieldPlacement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.MapperProfiles.ProgramManagement
{
    public class FieldPlacementActivityProfile : Profile
    {
        public FieldPlacementActivityProfile()
        {
            CreateMap<FieldPlacementActivity, FieldPlacementActivityViewModel>()
                .ForMember(x => x.StrActivityType, src => src.MapFrom(x => x.ActivityType.ToString()))
                .ForMember(dest => dest.DeliverableId, src => src.MapFrom(x => x.ActivityDeliverables.Where(d => d.DateDeactivated == null)
                     .Select(d => d.DeliverableId).ToArray()))
                .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue
                ? Status.InActive.ToString() : Status.Active.ToString())).ReverseMap();
        }
    }

    public class UnitFieldPlacementActivityProfile : Profile
    {
        public UnitFieldPlacementActivityProfile()
        {
            CreateMap<UnitFieldPlacementActivity, FieldPlacementActivityViewModel>()
               .ForMember(dest => dest.Name, src => src.MapFrom(x => x.FieldPlacementActivity.Name))
               .ForMember(dest => dest.Description, src => src.MapFrom(x => x.FieldPlacementActivity.Description))
               .ForMember(dest => dest.StrActivityType, src => src.MapFrom(x => x.FieldPlacementActivity.ActivityType.ToString()))
               .ForMember(dest => dest.DeliverableId, src => src.MapFrom(x => x.FieldPlacementActivity.ActivityDeliverables.
               Select(d => d.DeliverableId).ToArray()))
               .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue
               ? Status.InActive.ToString() : Status.Active.ToString())).ReverseMap();
        }
    }
}
