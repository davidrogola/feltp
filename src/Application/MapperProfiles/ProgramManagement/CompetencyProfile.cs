﻿using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using AutoMapper;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.MapperProfiles.ProgramManagement
{
    public class CompetencyProfile : Profile
    {
        public CompetencyProfile()
        {
            CreateMap<AddCompetencyCommand, Competency>().ReverseMap();
            CreateMap<Competency,CompetencyViewModel > ()
             .ForMember(dest => dest.DeliverableId, src => src.MapFrom(x => x.CompetencyDeliverables.Where(c=>c.DateDeactivated == null)
             .Select(d => d.DeliverableId).ToArray()))
             .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue ? Status.InActive.ToString() : Status.Active.ToString()))
            .ReverseMap();
        }
        
    }
}
