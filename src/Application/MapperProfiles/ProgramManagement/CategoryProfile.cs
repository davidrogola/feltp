﻿using AutoMapper;
using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using ProgramManagement.Domain.Course;

namespace Application.MapperProfiles.ProgramManagement
{
    public class CategoryProfile : Profile
    {
        public CategoryProfile()
        {
            CreateMap<AddCategoryCommand, Category>()
                 .ForMember(src => src.CourseTypeId, dest => dest.MapFrom(x => x.CourseTypeId)).ReverseMap();

            CreateMap<CategoryViewModel, Category>().ReverseMap()
                .ForMember(dest => dest.CourseType, x => x.MapFrom(src => src.CourseType.Name))
                .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue ?
                Status.InActive.ToString() : Status.Active.ToString()));

            CreateMap<UpdateCategoryCommand, Category>().ReverseMap();
        }
    }
}
