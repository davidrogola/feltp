﻿using Application.ProgramManagement.Commands;
using Application.ProgramManagement.Queries;
using AutoMapper;
using ProgramManagement.Domain.Course;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.ProgramManagement
{
    public class CourseScheduleProfile : Profile
    {
        public CourseScheduleProfile()
        {

                CreateMap<CourseSchedule, CourseScheduleViewModel>()
                .ForMember(src => src.ProgramCourse, dest => dest.MapFrom(x => x.ProgramCourse.Course.Name))
                .ForMember(src => src.ProgramOffer, dest => dest.MapFrom(x => x.ProgramOffer.Name))
                .ForMember(src => src.Faculty, dest => dest.MapFrom(x => x.Faculty.Person.FirstName + " " + x.Faculty.Person.LastName))
                .ForMember(src => src.ProgramTier, dest => dest.MapFrom(x => x.ProgramOffer.Program.ProgramTier.Name))
                .ForMember(src => src.Program, dest => dest.MapFrom(x => x.ProgramOffer.Program.Name))
                .ForMember(src => src.StartDate, dest => dest.MapFrom(x => x.StartDate.ToShortDateString()))
                .ForMember(src => src.EndDate, dest => dest.MapFrom(x => x.EndDate.ToShortDateString()))
                .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue ? Status.InActive.ToString() : Status.Active.ToString()));

            }
        }
    }

