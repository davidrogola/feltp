﻿using Application.Common.Models;
using AutoMapper;
using Common.Domain.Person;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.Common
{
    public class ContactProfile : Profile
    {
        public ContactProfile()
        {
            CreateMap<ContactInformation, ContactInfo>().ReverseMap();
        }
    }
}
