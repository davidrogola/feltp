﻿using Application.Common.Models;
using AutoMapper;
using Common.Domain.Address;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.Common
{
    class AddessInfoProfile : Profile
    {
        public AddessInfoProfile()
        {
            CreateMap<AddressInformation, AddressInfo>()
                .ForMember(x => x.CountyId, src =>
                src.MapFrom(x => (x.CountyId.HasValue && x.CountyId.Value != 0) ? x.CountyId : null))
                .ForMember(x => x.SubCountyId, src =>
                src.MapFrom(x => (x.SubCountyId.HasValue && x.SubCountyId != 0) ? x.SubCountyId : null))
                 .ForMember(x => x.Country, src => src.MapFrom(x => x.Country.Name))
                 .ForMember(x => x.County, src => src.MapFrom(x => x.County.Name))
                 .ForMember(x => x.SubCounty, src => src.MapFrom(x => x.SubCounty.Name))
                .ReverseMap();
        }
    }
}
