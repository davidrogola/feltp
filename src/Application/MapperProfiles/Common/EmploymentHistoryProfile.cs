﻿using Application.Common.Models;
using AutoMapper;
using Common.Domain.Person;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.Common
{
    public class EmploymentHistoryProfile : Profile
    {
        public EmploymentHistoryProfile()
        {
            CreateMap<EmploymentHistory, EmploymentDetail>()
                .ForMember(src => src.EmployerName, dest => dest.MapFrom(x => x.MinistryId.HasValue ? x.Ministry.Name : x.EmployerName))
                .ForMember(src => src.DurationInPosition, dest => dest.MapFrom(x => x.IsCurrentEmployer ? CalculateDurationInPosition(x.YearOfEmployment) : x.DurationInPosition))
                .ReverseMap();
        }

        private int CalculateDurationInPosition(string startYear)
        {
            var duration = DateTime.Now.Year - Convert.ToInt16(startYear);
            duration = duration < 1 ? 1 : duration;
            return duration;
        }
    }
}
