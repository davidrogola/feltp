﻿using AutoMapper;
using Application.Common.Models;
using Common.Domain.Person;
using Application.Common.Services;
using Application.ProgramManagement.QueryHandlers;

namespace Application.MapperProfiles.Common
{
    public class AcademicHistoryProfile : Profile
    {
        public AcademicHistoryProfile()
        {
            CreateMap<AcademicHistoryView, AcademicDetail>()
                .ForMember(x => x.StrEducationLevel,
                src => src.MapFrom(x => DispalyAttributeFetcher.GetDisplayAttributeValue(typeof(EducationLevel),
                     x.HighestEducationLevel.ToString())))
                 .ForMember(x=>x.CourseName,src=>src.MapFrom(x=>x.UniversityCourseId.HasValue ? x.UniversityCourse : x.CourseName)) 
                .ForMember(x => x.University, src => src.MapFrom(x => x.UniversityId.HasValue ? x.University : x.Other)).ReverseMap();

             CreateMap<AcademicDetail, AcademicHistory>()
                .ForMember(x=>x.UniversityId,src=>src.MapFrom(x=>(x.UniversityId.HasValue && x.UniversityId != 0) ? x.UniversityId : null))
                .ForMember(x=>x.CourseName, src=>src.MapFrom(x=>x.CourseName))
                .ReverseMap();
        }
    }

    public class AcademicCourseProfile : Profile
    {
        public AcademicCourseProfile()
        {
            CreateMap<AcademicCourse, AcademicCourseViewModel>()
                .ForMember(dest => dest.DateCreated, src => src.MapFrom(x => x.DateCreated.ToShortDateString()))
                .ForMember(dest => dest.EducationLevel, src => src.MapFrom(x => x.EducationLevel.ToString()))
                .ReverseMap();
        }
    }

    public class AcademicCourseGradingProfile : Profile
    {
        public AcademicCourseGradingProfile()
        {
            CreateMap<AcademicCourseGrading, CourseGradingViewModel>()
                .ForMember(dest => dest.EducationLevel, src => src.MapFrom(x => x.EducationLevel.ToString()))
                .ReverseMap();
        }
    }
}
