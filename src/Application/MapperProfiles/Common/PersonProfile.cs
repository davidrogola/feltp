﻿using Application.Common.Models;
using AutoMapper;
using Common.Domain.Person;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.Common
{
    public class PersonProfile : Profile
    {
        public PersonProfile()
        {
            CreateMap<Person, BioDataInformation>()
              .ForMember(x => x.StrDateOfBirth, src => src.MapFrom(x => x.DateOfBirth.ToShortDateString()))
              .ReverseMap();
        }
    }
}
