﻿using Application.ResidentManagement.Commands;
using Application.ResidentManagement.Queries;
using AutoMapper;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.ResidentManagement
{
    public class ResidentDeliverableItemProfile : Profile
    {
        public ResidentDeliverableItemProfile()
        {
            CreateMap<AddResidentDeliverableItemCommand, ResidentDeliverableItem>().ReverseMap();

            CreateMap<ResidentDeliverableItem, ResidentDeliverableSubmissionViewModel>()
                .ForMember(x => x.ProgrammaticArea, src => src.MapFrom(x => x.ProgrammaticArea.ToString()))
                .ForMember(x => x.DateSubmitted, src => src.MapFrom(x => x.DateSubmitted.ToShortDateString())).ReverseMap();

        }
    }
}
