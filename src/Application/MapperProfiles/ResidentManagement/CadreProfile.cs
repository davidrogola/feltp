﻿using Application.ResidentManagement.Commands;
using Application.ResidentManagement.Queries;
using AutoMapper;
using Common.Domain.Person;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.ResidentManagement
{
   public class CadreProfile : Profile
    {
        public CadreProfile()
    {
        CreateMap<AddCadreCommand, Cadre>().ReverseMap();
        CreateMap<CadreViewModel, Cadre>().ReverseMap()
            .ForMember(dest => dest.Status, src => src.MapFrom(x => x.DateDeactivated.HasValue ? Status.InActive.ToString() : Status.Active.ToString())).ReverseMap();

            CreateMap<UpdateCadreCommand, Cadre>().ReverseMap();
            CreateMap<DeactivateCadreCommand, Cadre>().ReverseMap();
        }
}
}

