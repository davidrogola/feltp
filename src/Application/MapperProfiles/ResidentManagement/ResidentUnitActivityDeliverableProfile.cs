﻿using Application.ResidentManagement.Queries;
using AutoMapper;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.ResidentManagement
{
    public class ResidentUnitActivityDeliverableProfile : Profile
    {
        public ResidentUnitActivityDeliverableProfile()
        {
            CreateMap<ResidentUnitActivityDeliverableView, ResidentUnitActivityDeliverableViewModel>().ReverseMap();
        }
    }
}
