﻿using Application.Common.Services;
using Application.ResidentManagement.Queries;
using AutoMapper;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.ResidentManagement
{
    public class ResidentGraduationDetailsProfile : Profile
    {
        public ResidentGraduationDetailsProfile()
        {
            CreateMap<ResidentGraduationInfoView, ResidentGraduationViewModel>()
                .ForMember(dest=>dest.DateCreated, src=>src.MapFrom(x=>x.DateCreated.ToShortDateString()))
                .ForMember(dest => dest.ExpectedProgramEndDate, src => src.MapFrom(x => x.ExpectedProgramEndDate.ToShortDateString()))
                .ReverseMap();
        }
    }

    public class GraduationEligibityItemsProfile : Profile
    {
        public GraduationEligibityItemsProfile()
        {
            CreateMap<GraduationEligibityItemsView, GraduateEligibityItemsViewModel>()
                .ForMember(dest => dest.DateCreated, src => src.MapFrom(x => x.DateCreated.ToShortDateString()))
                .ForMember(dest => dest.ActualValue, src => src.MapFrom(x =>Math.Floor(x.ActualValue)))
                .ForMember(dest => dest.ExpectedValue, src => src.MapFrom(x => Math.Floor(x.ExpectedValue)))
                .ForMember(x => x.StrEligibiltyType,src => src.MapFrom(x => DispalyAttributeFetcher.GetDisplayAttributeValue(typeof(EligibilityType),
                     x.EligibiltyType.ToString())))
                 .ForMember(x => x.FailureReason, src => src.MapFrom(x => DispalyAttributeFetcher.GetDisplayAttributeValue(typeof(FailureReason),
                      x.FailureReason.ToString())))
                 .ForMember(x => x.OutcomeStatus, src => src.MapFrom(x => DispalyAttributeFetcher.GetDisplayAttributeValue(typeof(OutcomeStatus),
                      x.OutcomeStatus.ToString())))
                .ReverseMap();

        }
    }
}