﻿using AutoMapper;
using Application.ResidentManagement.Queries;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;
using Application.Common.Services;
using ProgramManagement.Domain.FieldPlacement;
using Application.ResidentManagement.Commands;
using Application.ResidentManagement.QueryHandlers;

namespace Application.MapperProfiles.ResidentManagement
{
    public class ResidentCourseDetailsProfile : Profile
    {
        public ResidentCourseDetailsProfile()
        {
            CreateMap<ProgramEnrollment, ResidentCourseDetailViewModel>()
            .ForMember(x => x.Program, src => src.MapFrom(x => x.ProgramOffer.Program.Name))
            .ForMember(x => x.ProgramId, src => src.MapFrom(x => x.ProgramOffer.ProgramId))
            .ForMember(x => x.ProgramEnrollemntId, src => src.MapFrom(x => x.Id))
            .ForMember(x => x.ProgramOffer, src => src.MapFrom(x => x.ProgramOffer.Name))
            .ForMember(x => x.ProgramTier, src => src.MapFrom(x => x.ProgramOffer.Program.ProgramTier.Name))
            .ForMember(x => x.DateEnrolled, src => src.MapFrom(x => x.DateCreated.ToShortDateString()))
            .ForMember(x => x.Duration, src => src.MapFrom(x => x.ProgramOffer.Program.ConvertDurationToMonths()))
            .ForMember(x => x.StartDate, src => src.MapFrom(x => x.ProgramOffer.StartDate.ToShortDateString()))
            .ForMember(x => x.EndDate, src => src.MapFrom(x => x.ProgramOffer.EndDate.ToShortDateString()))
            .ForMember(x => x.EnrolledBy, src => src.MapFrom(x => x.CreatedBy))
            .ReverseMap();
        }
    }

    public class ResidentCourseWorkProfile : Profile
    {
        public ResidentCourseWorkProfile()
        {
            CreateMap<ResidentCourseWork, ResidentCourseWorkViewModel>()
            .ForMember(x => x.Course, src => src.MapFrom(x => x.ProgramCourse.Course.Name))
            .ForMember(x => x.CourseType, src => src.MapFrom(x => x.ProgramCourse.Course.Category.CourseType.Name))
            .ForMember(x => x.ResidentCourseWorkId, src => src.MapFrom(x => x.Id))
            .ForMember(x => x.Semester, src => src.MapFrom(x => x.ProgramCourse.Semester.Name))
            .ReverseMap();

        }


    }

    public class ResidentFieldPlacementActivityProfile : Profile
    {
        public ResidentFieldPlacementActivityProfile()
        {
            CreateMap<ResidentFieldPlacementActivityView, ResidentFieldPlacementActivityViewModel>()
                .ForMember(x => x.Activity, src => src.MapFrom(x => x.ActivityName))
                .ForMember(x => x.ActivityType,
                src => src.MapFrom(x => DispalyAttributeFetcher.GetDisplayAttributeValue(typeof(ActivityType),
                     x.ActivityType.ToString())))
                .ForMember(x => x.ProgressStatus,
                 src => src.MapFrom(x => DispalyAttributeFetcher.GetDisplayAttributeValue(typeof(PlacementActivityStatus),
                     x.ProgressStatus.ToString())))
                .ForMember(x => x.DateCreated, src => src.MapFrom(x => x.DateCreated.ToShortDateString()))
                .ReverseMap();
        }
    }

    public class ResidentUnitProfile : Profile
    {
        public ResidentUnitProfile()
        {
            CreateMap<ResidentUnit, ResidentUnitViewModel>()
                .ForMember(x => x.Unit, src => src.MapFrom(x => x.Unit.Name))
                .ForMember(x => x.Code, src => src.MapFrom(x => x.Unit.Code))
                .ReverseMap();

            CreateMap<ResidentUnitView, ResidentUnitViewModel>()
               .ForMember(x => x.Unit, src => src.MapFrom(x => x.Name))
               .ForMember(x => x.Id, src => src.MapFrom(x => x.ResidentUnitId))
               .ReverseMap();
        }
    }

    public class ResidentDeliverableProfile : Profile
    {

        public ResidentDeliverableProfile()
        {
            CreateMap<ResidentDeliverableView, ResidentDeliverableViewModel>()
                .ForMember(x => x.Id, src => src.MapFrom(x => x.ResidentDeliverableId))
                .ForMember(x => x.DateCreated, src => src.MapFrom(x => x.DateCreated.ToShortDateString()))
                .ForMember(x => x.Name, src => src.MapFrom(x => x.DeliverableName)).ReverseMap();

        }
    }

    public class ResidentExaminationProfile : Profile
    {
        public ResidentExaminationProfile()
        {
            CreateMap<ResidentExaminationView, ResidentExamDetail>()
                .ForMember(x => x.StrScore, src => src.MapFrom(x => x.Score.HasValue ? x.Score.Value.ToString() : null))
                .ReverseMap();

            CreateMap<ResidentExaminationView, ResidentExaminationScoreViewModel>()
             .ForMember(x => x.DateCreated, src => src.MapFrom(x => x.DateCreated.HasValue ? x.DateCreated.Value.ToShortDateString() : null))
              .ReverseMap();
        }
    }

    public class ResidentUnitAttendanceProfile : Profile
    {
        public ResidentUnitAttendanceProfile()
        {
            CreateMap<ResidentUnitAttendance, ResidentUnitAttendanceViewModel>()
                .ForMember(x => x.AttendanceDate, src => src.MapFrom(x => x.AttendanceDate.ToShortDateString()))
                .ForMember(x => x.StartTime, src => src.MapFrom(x => x.Timetable.StartDate.ToShortTimeString()))
                .ForMember(x => x.Status, src => src.MapFrom(x => x.AttendanceStatus.ToString()))
                .ForMember(x => x.EndTime, src => src.MapFrom(x => x.Timetable.EndDate.ToShortTimeString()))
                .ForMember(x => x.Trainer, src => src.MapFrom(x => x.Faculty.Person.FirstName +" "+ x.Faculty.Person.LastName))
                .ForMember(x => x.Unit, src => src.MapFrom(x => x.ResidentUnit.Unit.Name))
                .ForMember(x => x.Code, src => src.MapFrom(x => x.ResidentUnit.Unit.Code))
                .ReverseMap();

            CreateMap<ResidentUnitAttendanceView, ResidentUnitAttendanceViewModel>()
                .ForMember(x => x.AttendanceDate, src => src.MapFrom(x => x.AttendanceDate.ToShortDateString()))
                .ForMember(x => x.StartTime, src => src.MapFrom(x => x.StartTime.ToShortTimeString()))
                 .ForMember(x => x.Status, src => src.MapFrom(x => x.AttendanceStatus.ToString()))
                .ForMember(x => x.EndTime, src => src.MapFrom(x => x.EndTime.ToShortTimeString()));


        }
    }


}
