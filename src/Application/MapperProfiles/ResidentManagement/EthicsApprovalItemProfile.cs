﻿using Application.ResidentManagement.Queries;
using AutoMapper;
using ResidentManagement.Domain.Ethics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.ResidentManagement
{
    public class EthicsApprovalItemProfile : Profile
    {
        public EthicsApprovalItemProfile()
        {
            CreateMap<EthicsApprovalItem, EthicsApprovalItemViewModel>()
                .ForMember(x => x.DateCreated, src => src.MapFrom(x => x.DateCreated.ToShortDateString()))
                .ForMember(x => x.SubmissionDate, src => src.MapFrom(x => x.SubmissionDate.ToShortDateString()))
                .ForMember(x => x.Committee, src => src.MapFrom(x => x.EthicsApprovalCommittee.Name))
                .ForMember(x => x.CommitteeId, src => src.MapFrom(x => x.EthicsApprovalCommittee.Id))
                .ForMember(x => x.DocumentVersion, src => src.MapFrom(x => x.EthicsApprovalDocument.DocumentVersion))
                .ReverseMap();

        }
    }
}
