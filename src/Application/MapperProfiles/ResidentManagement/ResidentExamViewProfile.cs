using System.Collections.Generic;
using Application.Common.Services;
using Application.ProgramManagement.QueryHandlers;
using Application.ResidentManagement.Commands;
using AutoMapper;
using IdentityModel;
using ProgramManagement.Domain.Examination;
using ProgramManagement.Domain.Program;
using ResidentManagement.Domain;

namespace Application.MapperProfiles.ResidentManagement
{
    public class ResidentExamViewProfile : Profile
    {
      public ResidentExamViewProfile()
        {
            CreateMap<ResidentExaminationView, ResidentExamDetail>().ReverseMap();
            CreateMap<ResidentCatScoreView, ResidentExamDetail>()
                .ForMember(x=>x.Submitted, src=>
                    src.MapFrom(x=> x.Score.HasValue))
                .ForMember(x=>x.ExamType, src=>
                    src.MapFrom(x=>ExamTypeHelper.GetExamTypeEnum(x.TimeTableType)))
        
                .ForMember(x=>x.StrExamType,
                    src=>src.MapFrom(x=>ExamTypeHelper.GetExamTypeStr(x.TimeTableType))).ReverseMap();
        }
        
    }
}