﻿using Application.ResidentManagement.Queries;
using AutoMapper;
using ResidentManagement.Domain.Ethics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.ResidentManagement
{
    public class EthicsApprovalProfile : Profile
    {
        public EthicsApprovalProfile()
        {
            CreateMap<EthicsApprovalView, EthicsApprovalViewModel>()
                .ForMember(x => x.ApprovalStatus, src => src.MapFrom(x => x.ApprovalStatus.ToString()))
                .ForMember(x => x.PaperCategory, src => src.MapFrom(x => x.PaperCategory.ToString()))
                .ReverseMap();

        }
    }
}
