﻿using Application.ResidentManagement.Queries;
using AutoMapper;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.ResidentManagement
{
    public class CourseHistoryProfile : Profile
    {
        public CourseHistoryProfile()
        {
            CreateMap<Graduate, CourseHistoryViewModel>()
                .ForMember(x => x.Tier,src => src.MapFrom(x => x.ProgramEnrollment.ProgramOffer.Program.ProgramTier.Name))
                 .ForMember(x => x.Program, src => src.MapFrom(x => x.ProgramEnrollment.ProgramOffer.Program.Name))
                 .ForMember(x => x.EnrollmentDate,src => src.MapFrom(x => x.ProgramEnrollment.DateCreated))
                 .ForMember(x => x.ExpectedProgramEndDate, src => src.MapFrom(x => x.ExpectedProgramEndDate
                 .ToShortDateString()));
        }
    }
}
