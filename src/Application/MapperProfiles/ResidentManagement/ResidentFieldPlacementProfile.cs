﻿using Application.ResidentManagement.Commands;
using Application.ResidentManagement.Queries;
using AutoMapper;
using ResidentManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MapperProfiles.ResidentManagement
{
    public class ResidentFieldPlacementProfile : Profile
    {
        public ResidentFieldPlacementProfile()
        {
            CreateMap<AddResidentFieldPlacementCommand, FieldPlacement>()
            .ForMember(x => x.FacultyId, src => src.MapFrom(x => x.Faculty.HasValue && x.Faculty.Value != 0 ? x.Faculty.Value :(int ?) null ))
            .ForMember(x => x.SupervisorId, src => src.MapFrom(x => x.SupervisorId.HasValue && x.SupervisorId.Value != 0 ? x.SupervisorId.Value : (int?)null))
            .ForMember(x => x.FieldPlacementSiteId, src => src.MapFrom(x => x.FieldPlacementSiteId.HasValue && x.FieldPlacementSiteId.Value != 0 ? x.FieldPlacementSiteId.Value : (int?)null))
            .ReverseMap();

            CreateMap<ResidentFieldPlacement, ResidentFieldPlacementInfoViewModel>()
                .ForMember(x => x.StartDate, src => src.MapFrom(x => x.StartDate.ToShortDateString()))
                .ForMember(x => x.EndDate, src => src.MapFrom(x => x.EndDate.ToShortDateString())).ReverseMap();

        }
    }
}
