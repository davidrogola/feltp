﻿using Application.FacultyManagement.Commands;
using FacultyManagement.Domain;
using MediatR;
using Persistance.FacultyManagement.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.FacultyManagement.CommandHandlers
{
    public class DeactivateFacultyCommandHandler : IRequestHandler<DeactivateFacultyCommand, FacultyActivationDeactivationResult>
    {
        IFacultyManagementUnitOfWork unitOfWork;
        public DeactivateFacultyCommandHandler(IFacultyManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<FacultyActivationDeactivationResult> Handle(DeactivateFacultyCommand request, CancellationToken cancellationToken)
        {
            var faculty = unitOfWork.FacultyManagementRepository.Get<Faculty>(request.FacultyId);
            if (faculty == null)
                return Task.FromResult(new FacultyActivationDeactivationResult
                {
                    Id = request.FacultyId,
                    Message = "Faculty details not found"
                });

            faculty.Deactivate(request.DeactivatedBy);
            unitOfWork.FacultyManagementRepository.Update(faculty);

            unitOfWork.SaveChanges();

            return Task.FromResult(new FacultyActivationDeactivationResult
            {
                Id = request.FacultyId,
                Message = "Faculty deactivation successful"
            });
        }
    }

    public class ActivateFacultyCommandHandler : IRequestHandler<ActivateFacultyCommand, FacultyActivationDeactivationResult>
    {
        IFacultyManagementUnitOfWork unitOfWork;

        public ActivateFacultyCommandHandler(IFacultyManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<FacultyActivationDeactivationResult> Handle(ActivateFacultyCommand request, CancellationToken cancellationToken)
        {
            var faculty = unitOfWork.FacultyManagementRepository.Get<Faculty>(request.FacultyId);
            if (faculty == null)
               return Task.FromResult(new FacultyActivationDeactivationResult
                {
                    Id = request.FacultyId,
                    Message = "Faculty details not found"
                });

            faculty.Activate(request.ActivatedBy);
            unitOfWork.FacultyManagementRepository.Update(faculty);

            unitOfWork.SaveChanges();

            return Task.FromResult(new FacultyActivationDeactivationResult
            {
                Id = request.FacultyId,
                Message = "Faculty activation successful"
            });
        }
    }
}
