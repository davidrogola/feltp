﻿using Application.FacultyManagement.Commands;
using FacultyManagement.Domain;
using MediatR;
using Persistance.FacultyManagement.UnitOfWork;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.FacultyManagement.CommandHandlers
{
    public class AddFacultyCommandHandler : IRequestHandler<AddFacultyCommand, int>
    {
        IFacultyManagementUnitOfWork unitOfWork;
        IMediator mediator;
        ILogger logger = Log.ForContext<AddFacultyCommandHandler>();
        public AddFacultyCommandHandler(IFacultyManagementUnitOfWork _unitOfWork, IMediator _mediator)
        {
            unitOfWork = _unitOfWork;
            mediator = _mediator;

        }
        public async Task<int> Handle(AddFacultyCommand request, CancellationToken cancellationToken)
        {
            using (var transaction = unitOfWork.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    var personId = await mediator.Send(request.Person);

                    var faculty = new Faculty(personId, request.CreatedBy, request.StaffNumber);
                    unitOfWork.FacultyManagementRepository.Add(faculty);

                    var assignedRoles = faculty.AssignRoles(request.Roles.ToArray());
                    unitOfWork.FacultyManagementRepository.AddRange(assignedRoles);

                    unitOfWork.SaveChanges();
                    transaction.Commit();

                    return faculty.Id;
                }
                catch (Exception ex)
                {
                    logger.Error(ex, $"An error occurred while creating faculty with IdNumber: {request.Person.BioDataInfo.IdentificationNumber }");
                    throw;
                }
            }


        }


    }
    public class UpdateFacultyBioDataCommandHandler : IRequestHandler<UpdateFacultyBioDataCommand, int>
    {
        IFacultyManagementUnitOfWork unitOfWork;
        IMediator mediator;
        ILogger logger = Log.ForContext<AddFacultyCommandHandler>();
        public UpdateFacultyBioDataCommandHandler(IFacultyManagementUnitOfWork _unitOfWork, IMediator _mediator)
        {
            unitOfWork = _unitOfWork;
            mediator = _mediator;

        }
        public async Task<int> Handle(UpdateFacultyBioDataCommand request, CancellationToken cancellationToken)
        {
            using (var transaction = unitOfWork.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    var personId = await mediator.Send(request.UpdatePersonBioData);

                    var faculty = unitOfWork.FacultyManagementRepository
                        .FindByWithInclude<Faculty>(x=>x.Id == request.FacultyId,x=>x.FacultyRoleMap).SingleOrDefault();

                    faculty.UpdateStaffNumber(request.StaffNumber);

                    faculty.UpdateRoles(request.RoleId.ToArray(),faculty.FacultyRoleMap.Select(x=>x.FacultyRoleId).ToArray());

                    unitOfWork.FacultyManagementRepository.Update(faculty);

                    unitOfWork.SaveChanges();

                    return faculty.Id;
                }
                catch (Exception ex)
                {
                    logger.Error(ex, $"An error occurred while updating faculty with Id: {request.FacultyId }");
                    throw;
                }
            }


        }

    }
}
