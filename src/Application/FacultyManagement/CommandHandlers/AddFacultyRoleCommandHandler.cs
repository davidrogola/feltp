﻿using Application.FacultyManagement.Commands;
using AutoMapper;
using FacultyManagement.Domain;
using MediatR;
using Persistance.FacultyManagement.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.FacultyManagement.CommandHandlers
{
 public  class AddFacultyRoleCommandHandler : IRequestHandler<AddFacultyRoleCommand, int>
    {
        IFacultyManagementUnitOfWork facultyManagementUnitOfWork;
        public AddFacultyRoleCommandHandler(IFacultyManagementUnitOfWork _facultyManagementUnitOfWork)
        {
            facultyManagementUnitOfWork = _facultyManagementUnitOfWork;
        }
        public Task<int> Handle(AddFacultyRoleCommand request, CancellationToken cancellationToken)
        {
            FacultyRole role = new FacultyRole(request.Name, "System");
            facultyManagementUnitOfWork.FacultyManagementRepository.Add(role);
            facultyManagementUnitOfWork.SaveChanges();

            return Task.FromResult(role.Id);
        }
    }

    public class UpdateFacultyRoleCommandHandler : IRequestHandler<UpdateFacultyRoleCommand, int>
    {
        IFacultyManagementUnitOfWork facultyManagementUnitOfWork;
        IMapper mapper;
        public UpdateFacultyRoleCommandHandler(IFacultyManagementUnitOfWork _facultyManagementUnitOfWork,IMapper _mapper)
        {
            facultyManagementUnitOfWork = _facultyManagementUnitOfWork;
            mapper = _mapper;
        }
        public Task<int> Handle(UpdateFacultyRoleCommand request, CancellationToken cancellationToken)
        {
            FacultyRole role = mapper.Map<FacultyRole>(request);
            facultyManagementUnitOfWork.FacultyManagementRepository.Update(role);
            facultyManagementUnitOfWork.SaveChanges();

            return Task.FromResult(role.Id);
        }
    }


    public class DeactivateFacultyRoleCommandHandler : IRequestHandler<DeactivateFacultyRoleCommand, int>
    {
        IFacultyManagementUnitOfWork facultyManagementUnitOfWork;
        IMapper mapper;
        public DeactivateFacultyRoleCommandHandler(IFacultyManagementUnitOfWork _facultyManagementUnitOfWork, IMapper _mapper)
        {
            facultyManagementUnitOfWork = _facultyManagementUnitOfWork;
            mapper = _mapper;
        }
        public Task<int> Handle(DeactivateFacultyRoleCommand request, CancellationToken cancellationToken)
        {
            var role = facultyManagementUnitOfWork.FacultyManagementRepository.Get<FacultyRole>(request.Id);
            if (role == null)
            {
                return Task.FromResult(0);
            }
            role.Deactivate(request.UpdatedBy);
            facultyManagementUnitOfWork.FacultyManagementRepository.Update(role);

            facultyManagementUnitOfWork.SaveChanges();
            return Task.FromResult(role.Id);
        }
    }

}