﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.FacultyManagement.Commands
{
  public class AddFacultyRoleCommand : IRequest<int>
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }

    }

    public class UpdateFacultyRoleCommand: IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }

    }

    public class DeactivateFacultyRoleCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateDeactivatead { get; set; }
        public string DeactivatedBy { get; set; }
        public string UpdatedBy { get; set; }

    }
}