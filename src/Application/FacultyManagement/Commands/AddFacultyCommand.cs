﻿using Application.Common.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.FacultyManagement.Commands
{
 public class AddFacultyCommand : IRequest<int>
    {
        public AddPersonCommand Person { get; set; }
        public string CreatedBy { get; set; }
        public string StaffNumber { get; set; }
        public DateTime DateCreated { get; set; }
        public List<int> Roles { get; set; }
        public bool UniversityFound { get; set; }
    }

    public class UpdateFacultyBioDataCommand : IRequest<int>
    {
        public UpdatePersonBioDataCommand UpdatePersonBioData { get; set; }
        public List<int> RoleId { get; set; }
        public List<SelectListItem> Roles { get; set; }
        public int FacultyId { get; set; }
        public string StaffNumber { get; set; }

    }
}
