﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.FacultyManagement.Commands
{
    public class DeactivateFacultyCommand : FacultyDisplayInfo, IRequest<FacultyActivationDeactivationResult>
    {
        public string DeactivatedBy { get; set; }
    }
    public class FacultyDisplayInfo
    {
        public int FacultyId { get; set; }
        public string Name { get; set; }
        public string PersonnelNumber { get; set; }
    }

    public class ActivateFacultyCommand : FacultyDisplayInfo, IRequest<FacultyActivationDeactivationResult>
    {
        public string ActivatedBy { get; set; }
    }


    public class FacultyActivationDeactivationResult
    {
        public int Id { get; set; }
        public string Message { get; set; }
    }
}
