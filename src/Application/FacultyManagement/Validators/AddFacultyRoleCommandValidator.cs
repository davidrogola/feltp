﻿using Application.FacultyManagement.Commands;
using FacultyManagement.Domain;
using FluentValidation;
using Persistance.FacultyManagement.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.FacultyManagement.Validators
{
    public class AddFacultyRoleCommandValidator: AbstractValidator<AddFacultyRoleCommand>
    {
        IFacultyManagementUnitOfWork unitOfWork;
        public AddFacultyRoleCommandValidator(IFacultyManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;

            RuleFor(x => x.Name).NotNull().WithMessage(String.Format(FacultyManagementResource.NameIsRequired,"Role"));

            RuleFor(x => x.Name).Must((string name) =>
              {
                  if (String.IsNullOrEmpty(name))
                      return true;
                  bool exists = unitOfWork.FacultyManagementRepository.FindBy<FacultyRole>(x => x.Name == name).Any();

                  return !exists;

              }).WithMessage(FacultyManagementResource.NameExists);
        }
    }
}
