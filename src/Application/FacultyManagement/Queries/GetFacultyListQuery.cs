﻿using Application.Common.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.FacultyManagement.Queries
{
 public class GetFacultyListQuery : IRequest<List<FacultyViewModel>>
    {
        public int Id { get; set; }
    }

    public class FacultyViewModel
    {
        public int Id { get; set; }
        public string StaffNumber { get; set; }
        public string Status { get; set; }
        public PersonViewModel Person { get; set; }
        public List<FacultyRoleViewModel> Roles { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
    }
}
