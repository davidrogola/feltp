﻿using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.FacultyManagement.Queries
{
    public class GetFacultyForDropDownDisplayQuery : IRequest<List<SelectListItem>>
    {
        public string Role { get; set; }
    }


}
