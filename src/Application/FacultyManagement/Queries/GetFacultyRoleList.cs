﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.FacultyManagement.Queries
{
   public class GetFacultyRoleList : IRequest<List<FacultyRoleViewModel>>
    {
        public int ? Id { get; set; }
    }
    public class FacultyRoleViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; private set; }
        public string Status { get; set; }
    }

    public class GetAssignedFacultyRoles : IRequest<List<FacultyRoleViewModel>>
    {
        public int FacultyId { get; set; }
    }
}