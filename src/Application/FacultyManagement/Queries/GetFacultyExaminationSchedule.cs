﻿using Application.ProgramManagement.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.FacultyManagement.Queries
{
    public class GetFacultyExaminationSchedule : IRequest<List<TimetableViewModel>>
    {
        public int FacultyId { get; set; }

    }


}
