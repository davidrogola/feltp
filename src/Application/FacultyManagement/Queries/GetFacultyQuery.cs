﻿using Application.Common.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.FacultyManagement.Queries
{
    public class GetFacultyQuery : IRequest<FacultyViewModel>
    {
        public int Id { get; set; }
    }
}
