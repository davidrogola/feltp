﻿using Application.FacultyManagement.Queries;
using FacultyManagement.Domain;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using Persistance.FacultyManagement.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.FacultyManagement.QueryHandlers
{
    public class GetFacultyForDropDownDisplayQueryHandler : IRequestHandler<GetFacultyForDropDownDisplayQuery, List<SelectListItem>>
    {
        IFacultyManagementUnitOfWork unitOfWork;
        public GetFacultyForDropDownDisplayQueryHandler(IFacultyManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<List<SelectListItem>> Handle(GetFacultyForDropDownDisplayQuery request, CancellationToken cancellationToken)
        {
            var faculty = unitOfWork.FacultyManagementRepository.FindByWithInclude<Faculty>(x=>x.DateDeactivated == null, x => x.Person);

            var facultyDetails = faculty.Select(x => new SelectListItem
            {
                Text = x.Person.FirstName + " " + x.Person.LastName,
                Value = x.Id.ToString()
            }).ToList();

            return Task.FromResult(facultyDetails);
        }
    }
}
