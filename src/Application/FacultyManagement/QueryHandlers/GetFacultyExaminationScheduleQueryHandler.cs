﻿using Application.FacultyManagement.Queries;
using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Persistance.ProgramManagement.UnitOfWork;
using ProgramManagement.Domain.Program;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.FacultyManagement.QueryHandlers
{
    public class GetFacultyExaminationScheduleQueryHandler : IRequestHandler<GetFacultyExaminationSchedule, List<TimetableViewModel>>
    {
        IProgramManagementUnitOfWork unitOfWork;
        IMapper mapper;

        public GetFacultyExaminationScheduleQueryHandler(IProgramManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<TimetableViewModel>> Handle(GetFacultyExaminationSchedule request, CancellationToken cancellationToken)
        {
            var timetableDetails = unitOfWork.ProgramManagementRepository.FindByWithInclude<Timetable>(x => x.FacultyId == request.FacultyId,
                x => x.SemesterScheduleItem, x => x.Faculty.Person, x => x.Unit, x => x.Examination);

            var model = mapper.Map<List<TimetableViewModel>>(timetableDetails);

            return Task.FromResult(model);

        }
    }
}
