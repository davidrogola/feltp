﻿using Application.Common.Models;
using Application.FacultyManagement.Queries;
using AutoMapper;
using Common.Domain.Address;
using Common.Domain.Person;
using FacultyManagement.Domain;
using MediatR;
using Persistance.FacultyManagement.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.FacultyManagement.QueryHandlers
{
    public class GetFacultyListQueryHandler : IRequestHandler<GetFacultyListQuery, List<FacultyViewModel>>
    {
        IFacultyManagementUnitOfWork unitOfWork;
        IMapper mapper;
        public GetFacultyListQueryHandler(IFacultyManagementUnitOfWork _unitOfWork, IMapper _mapper)
        {
            unitOfWork = _unitOfWork;
            mapper = _mapper;
        }
        public Task<List<FacultyViewModel>> Handle(GetFacultyListQuery request, CancellationToken cancellationToken)
        {
            var faculties = unitOfWork.FacultyManagementRepository.FindByWithInclude<Faculty>(null, x => x.Person,
                x => x.Person.ContactInformation, x => x.Person.Cadre)
              .ToList();

            var facultyViewModel = faculties.Select(x => new FacultyViewModel
            {
                Id = x.Id,
                CreatedBy = x.CreatedBy,
                DateCreated = x.DateCreated.ToString(),
                StaffNumber = x.StaffNumber,
                Status = x.DateDeactivated.HasValue ? Status.InActive.ToString() : Status.Active.ToString(),
                Person = new PersonViewModel
                {
                    BioDataInfo = mapper.Map<BioDataInformation>(x.Person),
                    ContactInfo = mapper.Map<ContactInfo>(x.Person.ContactInformation),
                }
            }).OrderByDescending(x => x.DateCreated).ToList();

            return Task.FromResult(facultyViewModel);
        }
    }
}
