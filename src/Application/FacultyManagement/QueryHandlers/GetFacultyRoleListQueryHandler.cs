﻿using Application.FacultyManagement.Queries;
using AutoMapper;
using FacultyManagement.Domain;
using MediatR;
using Persistance.FacultyManagement.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.FacultyManagement.QueryHandlers
{
    public class GetFacultyRoleListQueryHandler : IRequestHandler<GetFacultyRoleList, List<FacultyRoleViewModel>>
    {
        IFacultyManagementUnitOfWork unitOfWork;
        public GetFacultyRoleListQueryHandler(IFacultyManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<List<FacultyRoleViewModel>> Handle(GetFacultyRoleList request, CancellationToken cancellationToken)
        {
            var facultyroles = request.Id.HasValue ? unitOfWork.FacultyManagementRepository.FindBy<FacultyRole>(x => x.Id == request.Id)
                : unitOfWork.FacultyManagementRepository.GetAll<FacultyRole>();
            var facultyRoleViewModel = Mapper.Map<List<FacultyRoleViewModel>>(facultyroles);

            return Task.FromResult(facultyRoleViewModel);
        }

    }

    public class GetAssignedFacultyRolesQueryHandler : IRequestHandler<GetAssignedFacultyRoles, List<FacultyRoleViewModel>>
    {

        IFacultyManagementUnitOfWork unitOfWork;
        public GetAssignedFacultyRolesQueryHandler(IFacultyManagementUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        public Task<List<FacultyRoleViewModel>> Handle(GetAssignedFacultyRoles request, CancellationToken cancellationToken)
        {
            var facultyRoleMap = unitOfWork.FacultyManagementRepository
                .FindByWithInclude<FacultyRoleMap>(x => x.FacultyId == request.FacultyId, x => x.FacultyRole);

            var facultyRoles = facultyRoleMap.Select(x => new FacultyRoleViewModel
            {
                Name = x.FacultyRole.Name,
                Id = x.FacultyRole.Id
            }).ToList();

            return Task.FromResult(facultyRoles);

        }
    }
}

