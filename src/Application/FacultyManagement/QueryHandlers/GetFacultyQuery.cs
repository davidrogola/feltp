﻿using Application.Common.Models;
using Application.FacultyManagement.Queries;
using FacultyManagement.Domain;
using AutoMapper;
using Common.Domain.Person;
using MediatR;
using Persistance.FacultyManagement.UnitOfWork;
using Persistance.ProgramManagement.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Application.Common.Models.Queries;

namespace Application.FacultyManagement.QueryHandlers
{
    public class GetFacultyQueryHandler : IRequestHandler<GetFacultyQuery, FacultyViewModel>
    {
        IFacultyManagementUnitOfWork facultyUnitOfWork;
        IMediator mediator;
        public GetFacultyQueryHandler(IFacultyManagementUnitOfWork _unitOfWork, IMediator _mediator)
        {
            facultyUnitOfWork = _unitOfWork;
            mediator = _mediator;
        }
        public async Task<FacultyViewModel> Handle(GetFacultyQuery request, CancellationToken cancellationToken)
        {

            var faculty = facultyUnitOfWork.FacultyManagementRepository.FindByWithInclude<Faculty>(x => x.Id == request.Id,
                x => x.FacultyRoleMap).SingleOrDefault();

            if (faculty == null)
                return new FacultyViewModel();

            var person = await mediator.Send(new GetPersonQuery() { PersonId = faculty.PersonId });

            if (person == null)
                return new FacultyViewModel();

            var roleIds = faculty.FacultyRoleMap.Select(f => f.FacultyRoleId);

            var roles = facultyUnitOfWork.FacultyManagementRepository.FindBy<FacultyRole>
                (x => roleIds.Contains(x.Id))
                .Select(x => new FacultyRoleViewModel
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();


            var facultyViewModel = new FacultyViewModel
            {
                Id = faculty.Id,
                Person = person,
                DateCreated = faculty.DateCreated.ToString(),
                CreatedBy = faculty.CreatedBy,
                Roles = roles,
                StaffNumber = faculty.StaffNumber,
                Status = faculty.DateDeactivated.HasValue ? Status.InActive.ToString() : Status.Active.ToString()
            };
            return facultyViewModel;
        }
    }
}
