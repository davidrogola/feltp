﻿using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Core;
using Serilog.Debugging;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FELTP.Infrastructure
{
    public static class SerilogConfiguration
    {
        public static void BootstrapLogger(IConfiguration config)
        {
            var section = config.GetSection("Serilog");
            var level = (LogEventLevel)Enum.Parse(typeof(LogEventLevel), section["serilog:minimum-level"], true);

            string logDirectory = section["serilog:dir"];
            logDirectory = CreateApplicationLogDir(logDirectory);
            string logFormat = Path.Combine(logDirectory, "log-{Date}.txt");

            Log.Logger = new LoggerConfiguration()
               .MinimumLevel.ControlledBy(new LoggingLevelSwitch(level))
               .WriteTo.RollingFile(logFormat, level,
                   "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level}] {Message}{NewLine}{Exception}", null, 300000000, 30)
               .CreateLogger();

            string internalSerilogFile = Path.Combine(logDirectory, "internalSerilog.txt");
            StreamWriter sWriter = new StreamWriter(File.Open(internalSerilogFile, FileMode.Append, FileAccess.Write, FileShare.ReadWrite))
            {
                AutoFlush = true
            };

            SelfLog.Enable(TextWriter.Synchronized(sWriter));
        }

        static string CreateApplicationLogDir(string configuredDir)
        {
            string dir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Logs");

            if (!string.IsNullOrEmpty(configuredDir))
                dir = Path.Combine(configuredDir, "ResidentManagementAPI");

            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            return dir;
        }
    }
}
