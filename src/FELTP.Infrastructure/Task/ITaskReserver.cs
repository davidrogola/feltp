﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FELTP.Infrastructure.Task
{
    public interface IReservationResult
    {
        bool WasSuccesfull();
    }

    public interface IReserveItemForAction<TSource, TResult> where TResult : IReservationResult
    {
        TResult Reserve(TSource reservationObject);

        void MarkAsComplete(TResult result, bool operationFailed);
    }
}
