﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FELTP.Infrastructure.Task
{
    public interface IConsumer<T> where T :class
    {
        void Consume(T task);
    }
}
