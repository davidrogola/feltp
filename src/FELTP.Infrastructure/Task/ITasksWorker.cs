﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FELTP.Infrastructure.Task
{
    public interface ITasksWorker<T> where T : class
    {
        void Process();
    }


    public class TasksWorker<T> : ITasksWorker<T> where T : class
    {
        IConsumer<T> consumer;
        ITaskQueue<T> tasksQueue;
        public TasksWorker(IConsumer<T> _consumer, ITaskQueue<T> _tasksQueue)
        {
            consumer = _consumer;
            tasksQueue = _tasksQueue;
        }

        public void Process()
        {
            var taskExists = tasksQueue.TryDequeue(out T task);
            if(taskExists)
            consumer.Consume(task);
        }
    }
}
