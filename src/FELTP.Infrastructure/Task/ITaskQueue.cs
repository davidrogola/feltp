﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace FELTP.Infrastructure.Task
{
    public interface ITaskQueue<T> where T : class
    {
        void Enqueue(T task);

        bool TryDequeue(out T task);

        void Enqueue(IEnumerable<T> tasks);

    }


    public class TasksQueue<T> : ITaskQueue<T> where T : class
    {
        ConcurrentQueue<T> queue;
        public TasksQueue()
        {
            queue = new ConcurrentQueue<T>();
        }
        public void Enqueue(T task)
        {
            queue.Enqueue(task);
        }

        public void Enqueue(IEnumerable<T> tasks)
        {
            if (tasks == null)
                return;

            foreach (var task in tasks)
                queue.Enqueue(task);
           
        }

        public bool TryDequeue(out T task)
        {
            return queue.TryDequeue(out task);
        }
    }
}
