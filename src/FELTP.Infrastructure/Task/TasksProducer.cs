﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FELTP.Infrastructure.Task
{
    public interface ITasksProducer<T> where T : class
    {
        void ProduceTasks();

    }


    public class TasksProducer<T> : ITasksProducer<T> where T : class
    {
        ITaskQueue<T> tasksQueue;
        ITaskLoader<T> taskLoader;
        IConfiguration configuration;
        public TasksProducer(ITaskQueue<T> _tasksQueue, ITaskLoader<T> _taskLoader, IConfiguration _configuration)
        {
            tasksQueue = _tasksQueue;
            taskLoader = _taskLoader;
            configuration = _configuration;
        }
        public void ProduceTasks()
        {
            var noOfTasks = Convert.ToInt32(configuration["TotalNumberOfTasks"]);

            var tasks = taskLoader.LoadTasks(noOfTasks);
            if (!tasks.Any())
                return;
            tasksQueue.Enqueue(tasks);
            
        }
    }


}
