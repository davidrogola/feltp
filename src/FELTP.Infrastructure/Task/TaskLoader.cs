﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FELTP.Infrastructure.Task
{
    public interface ITaskLoader<T> where T : class
    {
        IEnumerable<T> LoadTasks(int totalTasks);
    }
}
