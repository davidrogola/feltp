﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FELTP.Infrastructure
{
    public static class Util
    {
        public static string GetProcessName()
        {
            string processName = System.Diagnostics.Process.GetCurrentProcess()
               .ProcessName;
            return string.Concat(Environment.MachineName, "_", processName);
        }
    }
}
