﻿
using Application.Common;
using Application.Common.Services;
using Application.MapperProfiles.ProgramManagement;
using Application.ProgramManagement.Queries;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using static Application.Common.Services.IdentityConfig;


namespace LookUpManagementAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(Configuration);

            services.AddFeltpPersistanceStores(Configuration);
            services.AddUnitOfWorkImplementations();

            services.AddMvcCore(options => {

                options.Filters.Add(typeof(WebApiCustomExceptionFilter));
            })
            .AddAuthorization()
            .AddJsonFormatters();

            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = GetIdentityServerEndPoint(Configuration);
                    options.RequireHttpsMetadata = false;
                    options.ApiName = "LookUpManagementAPI";
                });

            services.AddAutoMapper(typeof(ProgramProfile).Assembly);
            services.AddMediatR(typeof(GetProgramList).Assembly);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
