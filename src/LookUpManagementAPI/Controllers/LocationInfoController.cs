﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.LookUp.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LookUpManagementAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [Authorize]
    public class LocationInfoController : Controller
    {
        IMediator mediator;
        public LocationInfoController(IMediator _mediator)
        {
            mediator = _mediator;
        }

        [HttpGet]
        public async Task<object> GetCountrySelectList()
        {
            var countries = await mediator.Send(new GetCountries());

            var countrySelectList = countries.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return Ok(countrySelectList);
        }

        [HttpGet]
        public async Task<object> GetCountySelectList(int countryId)
        {
            var counties = await mediator.Send(new GetCounties { CountryId = countryId });

            var countySelectList = counties.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return Ok(countySelectList);

        }

        [HttpGet]
        public async Task<object> GetSubCountySelectList(int countyId)
        {
            var subCounties = await mediator.Send(new GetSubCounties { CountyId = countyId });

            var subCountySelectList = subCounties.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return Ok(subCountySelectList);

        }
    }
}