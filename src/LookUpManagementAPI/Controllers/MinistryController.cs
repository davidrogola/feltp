﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.LookUp.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LookUpManagementAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [Authorize]
    public class MinistryController : Controller
    {
        IMediator mediator;
        public MinistryController(IMediator _medaitor)
        {
            mediator = _medaitor;
        }

        [HttpGet]
        public async Task<object> GetMinistrySelectList()
        {
            var ministries = await mediator.Send(new GetMinistries { });

            var selectList = ministries.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() });

            return Ok(selectList);
        }
    }
}