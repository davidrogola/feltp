﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.ResidentManagement.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LookUpManagementAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [Authorize]
    public class CadreController : Controller
    {
        IMediator mediator;
        public CadreController(IMediator _mediator)
        {
            mediator = _mediator;
        }

        [HttpGet]
        public async Task<object> GetCadreSelectList()
        {
            var cadres = await mediator.Send(new GetCadreList() { });
            var selectListItem = cadres.Select(x=>new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return Ok(selectListItem);
        }
    }
}