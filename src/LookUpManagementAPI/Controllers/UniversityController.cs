﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.LookUp.Queries;
using Application.ProgramManagement.QueryHandlers;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LookUpManagementAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [Authorize]
    public class UniversityController : Controller
    {
        IMediator mediator;
        public UniversityController(IMediator _mediator)
        {
            mediator = _mediator;
        }

        [HttpGet]
        public async Task<object> GetUniversitySelectList()
        {
            var universities = await mediator.Send(new GetUniversities { });

            var selectList = universities.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            return Ok(selectList);
        }

        public async Task<object> GetUniversityCourses()
        {
            var academicCourses = await mediator.Send(new GetAcademicCoursesQuery { });
            return Ok(academicCourses);
        }

    }
}