﻿using IdentityServer4.Models;
using IdentityServer4.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FELTP.Identity
{
    public static class IdentityConfiguration
    {
        public static List<TestUser> GetUsers()
        {
            return new List<TestUser>()
            {
                new TestUser
                {
                     SubjectId = "dbf5f010-d224-40cf-89a0-22fd9577fa6c",
                     Username = "davidrogola",
                     Password = "password",
                     Claims = new List<Claim>()
                      {
                            new Claim("given_name","David"),
                            new Claim("family_name","Ogola")
                      }

                },
                new TestUser
                {
                     SubjectId = "27a787c5-7b30-4c0d-bdf8-987ed63a2101",
                     Username = "johndoe",
                     Password = "password",
                     Claims = new List<Claim>()
                      {
                            new Claim("given_name","John"),
                            new Claim("family_name","Doe")
                      }

                }

            };

        }

        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
               new IdentityResources.OpenId(),
               new IdentityResources.Profile()
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>();
        }
    }
}
