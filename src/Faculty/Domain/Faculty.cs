﻿using Common.Domain.Person;
using Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FacultyManagement.Domain
{
    public class Faculty : IAuditItem
    {
        public Faculty()
        {

        }

        public Faculty(long personId, string createdBy, string staffnumber)
        {
            DateCreated = DateTime.Now;
            PersonId = personId;
            CreatedBy = createdBy;
            StaffNumber = staffnumber;

        }
        public int Id { get; set; }
        public long PersonId { get; set; }
        public string StaffNumber { get; set; }
        public virtual Person Person { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateDeactivated { get; set; }
        public string DeactivatedBy { get; set; }
        public virtual ICollection<FacultyRoleMap> FacultyRoleMap { get; set; }
        public void Deactivate(string deactivatedBy)
        {
            DateDeactivated = DateTime.Now;
            DeactivatedBy = deactivatedBy;
        }

        public void Activate(string activatedBy)
        {
            DateDeactivated = null;
            DeactivatedBy = null;
            CreatedBy = activatedBy;
        }

        public  List<FacultyRoleMap> AssignRoles(int [] roles)
        {
            var facultyRoles = new List<FacultyRoleMap>();
            foreach(var role in roles)
            {
                var facultyRole = new FacultyRoleMap(Id, role, CreatedBy);
                facultyRoles.Add(facultyRole);
            }
            return facultyRoles;
        }

        public void UpdateStaffNumber(string staffNumber)
        {
            StaffNumber = staffNumber;
        }

        public void UpdateRoles(int [] roles, int [] existingRoleIds)
        {
            var addedRoles = roles.Except(existingRoleIds);

            var removedRoles =  existingRoleIds.Except(roles);
        }
    }
}
