using System;

namespace FacultyManagement.Domain
{
    public class FacultyEvaluation
    {
        public FacultyEvaluation()
        {
            
        }
        
        public FacultyEvaluation(long facultyId, long residentId, int semesterId, int unitId, string feedback)
        {
            FacultyId = facultyId;
            ResidentId = residentId;
            SemesterId = semesterId;
            UnitId = unitId;
            Feedback = feedback;
            DateEvaluated = DateTime.Now;
        }
        public int Id { get; set; }
        public long FacultyId { get; set; }
        public long ResidentId { get; set; }
        public int SemesterId { get; set; }
        public int UnitId { get; set; }
        public string Feedback { get; set; }
        public DateTime DateEvaluated { get; set; }
    }
}