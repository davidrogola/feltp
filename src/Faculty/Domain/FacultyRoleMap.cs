﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FacultyManagement.Domain
{
    public class FacultyRoleMap
    {
        public FacultyRoleMap()
        {

        }

        public FacultyRoleMap(int facultyId, int roleId, string createdBy)
        {
            FacultyId = facultyId;
            FacultyRoleId = roleId;
            CreatedBy = createdBy;
            DateCreated = DateTime.Now;
        }
        public int Id { get; set; }
        public int FacultyId { get; set; }
        public int FacultyRoleId { get; set; }
        public DateTime DateCreated { get; private set; }
        public string CreatedBy { get; private set; }
        public DateTime? DateDeactivated { get; private set; }
        public string DeactivatedBy { get; private set; }
        public void Deactivate(string deactivatedBy)
        {
            DeactivatedBy = deactivatedBy;
            DateDeactivated = DateTime.Now;
        }
        public virtual FacultyRole FacultyRole { get; set; }

    }
}
